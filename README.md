InSANE Library
==============

InSANE is a multipurpose physics library which is mostly geared towards 
experimentalists and experiment minded theorists .

It provides some of the following :

* PDFs
* Structure Functions
* Form Factors
* GPDs
* Cross Sections
* Phase space samplers ( with cross section )
* Event Generators (a collection of phase space samplers)
* Composite target yield  and rate calculators
* Much more...



