void plot_Htilde()
{
   using namespace insane::physics;

   GK_GPDs*  gpds = new GK_GPDs();
   //auto  gpds = new GK_GPDs();

   int f = 2;
   int i = 1;
   double xi = 0.1;
   double t  = 0.0;
   double xB = 0.25;

   //auto func = new TF1("func",[&](double* x, double* p) {
   //      return( x[0]*gpds->Htilde_i(f,i, x[0],xi,0.0,4.0));
   //      }, -1.0,1.0,0);
   auto func = new TF1("func",[&](double* x, double* p) {
         return( gpds->H_i_integrand(f,i, xB,xi, t, 4.0,x[0]));
         }, -1.0,1.0,0);
   
   auto fgluon = new TF1("fgluon",[&](double* x, double* p) {
         return( gpds->H_g(x[0],xi, t, 4.0));
         }, 0.0001,1.0,0);
   auto fgluon2 = new TF1("fgluon",[&](double* x, double* p) {
         return( gpds->H_i_integrate_DD(0,0,x[0],xi, t, 4.0));
         }, 0.0001,1.0,0);

   auto fsea = new TF1("fsea",[&](double* x, double* p) {
         return( gpds->H_i(2,1, x[0],xi, t, 4.0));
         }, 0.0001,1.0,0);


   auto usea = new TF1("usea",[&](double* x, double* p) {
         //std::cout << " x = " << x[0] <<  std::endl;
         return( x[0]*gpds->H_i(2,1,x[0],xi, t,4.0));
         }, -0.5,0.5,0);
   auto usea2 = new TF1("usea2",[&](double* x, double* p) {
         //std::cout << " x = " << x[0] <<  std::endl;
         return( x[0]*gpds->H_i_integrate_DD(2,1,x[0],xi, t,4.0));
         }, -0.5,0.5,0);

   auto uval = new TF1("uval",[&](double* x, double* p) {
         //std::cout << " x = " << x[0] <<  std::endl;
         return( gpds->H_i(2,2,x[0],xi, t,4.0));
         }, -0.5,0.5,0);
   auto uval2 = new TF1("uval2",[&](double* x, double* p) {
         //std::cout << " x = " << x[0] << ", xi = " << xi <<   std::endl;
         return( gpds->H_i_integrate_DD(2,2,x[0],xi, t,4.0));
         }, -0.5,0.5,0);


   func->SetNpx(1500);

   TCanvas * c = new TCanvas();
   c->Divide(2,2);
   TGraph * gr = 0;
   TMultiGraph * mg =  0;

   //------------------------------------------
   c->cd(1);
   mg = new TMultiGraph();

   xB=0.1;
   xi=0.001;
   gr = new TGraph(func->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(1);
   mg->Add(gr,"l");

   xi=0.05;
   gr = new TGraph(func->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(4);
   mg->Add(gr,"l");

   xi=0.1;
   gr = new TGraph(func->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(2);
   mg->Add(gr,"l");

   xi=0.2;
   gr = new TGraph(func->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(3);
   //mg->Add(gr,"l");

   mg->Draw("a");
   mg->SetTitle("integrand d#beta");
   mg->Draw("a");

   //------------------------------------------
   c->cd(2);
   mg = new TMultiGraph();

   xi=0.001;
   //gr = new TGraph(fgluon->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(1);
   //mg->Add(gr,"l");
   //gr = new TGraph(fgluon2->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(1);
   //gr->SetLineStyle(2);
   //mg->Add(gr,"l");

   xi=0.05;
   //gr = new TGraph(fgluon->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(4);
   //mg->Add(gr,"l");
   gr = new TGraph(fgluon2->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(4);
   gr->SetLineStyle(2);
   mg->Add(gr,"l");

   xi=0.1;
   //gr = new TGraph(fgluon->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(2);
   //mg->Add(gr,"l");
   gr = new TGraph(fgluon2->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(2);
   gr->SetLineStyle(2);
   mg->Add(gr,"l");

   mg->Draw("a");
   mg->SetTitle("sea");
   gPad->SetLogx(true);
   mg->Draw("a");

   //------------------------------------------
   c->cd(3);
   mg = new TMultiGraph();

   xB = 0.1;
   xi=0.001;
   //gr = new TGraph(usea->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(1);
   //mg->Add(gr,"l");
   gr = new TGraph(usea2->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(1);
   gr->SetLineStyle(2);
   mg->Add(gr,"l");

   xi=0.05;
   //gr = new TGraph(usea->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(4);
   //mg->Add(gr,"l");
   gr = new TGraph(usea2->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(4);
   gr->SetLineStyle(2);
   mg->Add(gr,"l");

   xi=0.1;
   //gr = new TGraph(usea->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(2);
   //mg->Add(gr,"l");
   gr = new TGraph(usea2->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(2);
   gr->SetLineStyle(2);
   mg->Add(gr,"l");

   xi=0.2;
   //gr = new TGraph(usea->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(3);
   //mg->Add(gr,"l");
   gr = new TGraph(usea2->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(3);
   gr->SetLineStyle(2);
   mg->Add(gr,"l");

   mg->Draw("a");
   mg->SetTitle("sea");
   gPad->SetLogx(true);
   mg->Draw("a");

   //------------------------------------------
   c->cd(4);
   mg = new TMultiGraph();

   //xB = 0.1;
   //xi = 0.001;
   ////gr = new TGraph(uval->DrawCopy("goff")->GetHistogram());
   ////gr->SetLineColor(1);
   ////mg->Add(gr,"l");
   //gr = new TGraph(uval2->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(1);
   //gr->SetLineStyle(2);
   //mg->Add(gr,"l");

   //xi=0.05;
   ////gr = new TGraph(uval->DrawCopy("goff")->GetHistogram());
   ////gr->SetLineColor(4);
   ////mg->Add(gr,"l");
   //gr = new TGraph(uval2->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(4);
   //gr->SetLineStyle(2);
   //mg->Add(gr,"l");

   //xi=0.1;
   //gr = new TGraph(uval->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(2);
   //mg->Add(gr,"l");
   //gr = new TGraph(uval2->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(2);
   //gr->SetLineStyle(2);
   //mg->Add(gr,"l");

   //xi=0.2;
   ////gr = new TGraph(uval->DrawCopy("goff")->GetHistogram());
   ////gr->SetLineColor(3);
   ////mg->Add(gr,"l");
   //gr = new TGraph(uval2->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(3);
   //gr->SetLineStyle(2);
   //mg->Add(gr,"l");

   mg->Draw("a");
   mg->SetTitle("val");
   mg->Draw("a");


}

