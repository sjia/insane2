void plot_DD()
{
   using namespace insane::physics;

   DoubleDistributionGPDs*  gpds = new DoubleDistributionGPDs();
   //auto  gpds = new GK_GPDs();

   int f = 2;
   int i = 1;
   double t  = 0.0;
   double xB = 0.1;
   double xi = 0.001;
   double Q2 = 4.0;
   double alpha = 0.0;
   double beta = 0.0;

   auto func = new TF1("func",[&](double* x, double* p) {
         return( gpds->H_u_integrand(xB,xi,t,Q2,x[0] ));
         }, -0.5,0.5,0);
   auto func2 = new TF1("func2",[&](double* x, double* p) {
         return( gpds->ProfileFunction(1.0,beta,x[0] ));
         }, -1.0,1.0,0);
   auto func3 = new TF2("func3",[&](double* x, double* p) {
         return( gpds->ProfileFunction(1.0,x[0],x[1] ));
         }, -1.0,1.0,-1.0,1.0,0);

   auto udist = new TF1("udist",[&](double* x, double* p) {
         //std::cout << " x = " << x[0] <<  std::endl;
         return( x[0]*gpds->H_u(x[0],xi, t,Q2));
         }, -1.0,1.0,0);

   auto ubardist = new TF1("udist",[&](double* x, double* p) {
         //std::cout << " x = " << x[0] <<  std::endl;
         return( x[0]*gpds->H_ubar(x[0],xi, t,Q2));
         }, -1.0,1.0,0);

   auto uvaldist = new TF1("udist",[&](double* x, double* p) {
         //std::cout << " x = " << x[0] <<  std::endl;
         return( gpds->H_uval(x[0],xi, t,Q2));
         }, -1.0,1.0,0);

   auto gdist = new TF1("gdist",[&](double* x, double* p) {
         //std::cout << " x = " << x[0] <<  std::endl;
         return( gpds->H_g(x[0],xi, t,Q2));
         }, 0.0,1.0,0);

   func->SetNpx(1500);
   gdist->SetNpx(500);
   //udist->SetNpx(500);

   TLegend * leg = 0;
   TCanvas * c = new TCanvas();
   c->Divide(2,2);
   TGraph * gr = 0;
   TMultiGraph * mg =  0;

   //------------------------------------------
   c->cd(1);
   //leg = new TLegend (0.3,0.85,0.7,0.98);
   //mg = new TMultiGraph();

   //xB = 0.133;
   //xi = 0.001;
   //gr = new TGraph(func->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(1);
   //mg->Add(gr,"l");
   ////leg->AddEntry(gr,Form("#pi_{1}(#beta,#alpha=%.1f)",alpha),"l");

   //xi = 0.01;
   //gr = new TGraph(func->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(4);
   ////leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   //mg->Add(gr,"l");

   //xi = 0.1;
   //gr = new TGraph(func->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(2);
   ////leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   //mg->Add(gr,"l");

   //xi = 0.0;
   //gr = new TGraph(func->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(8);
   ////leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   //mg->Add(gr,"l");

   //mg->Draw("a");
   ////gPad->SetLogy(true);
   //mg->Draw("a");
   ////leg->Draw();
   
   //------------------------------------------
   c->cd(1);
   leg = new TLegend (0.3,0.85,0.7,0.98);
   mg = new TMultiGraph();

   xi = 0.001;
   gr = new TGraph(gdist->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(1);
   mg->Add(gr,"l");
   //leg->AddEntry(gr,Form("#pi_{1}(#beta,#alpha=%.1f)",alpha),"l");

   xi = 0.01;
   gr = new TGraph(gdist->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(4);
   //leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   mg->Add(gr,"l");

   xi = 0.1;
   gr = new TGraph(gdist->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(2);
   //leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   mg->Add(gr,"l");

   //xi = 0.15;
   //gr = new TGraph(gdist->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(8);
   ////leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   //mg->Add(gr,"l");

   //xi = 0.2;
   //gr = new TGraph(gdist->DrawCopy("goff")->GetHistogram());
   //gr->SetLineColor(7);
   ////leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   //mg->Add(gr,"l");

   mg->Draw("a");
   mg->SetTitle("gluon");
   mg->GetXaxis()->SetTitle("x");
   mg->GetXaxis()->CenterTitle(true);
   //leg->Draw();
   gPad->SetLogx(true);
   mg->Draw("a");


   //------------------------------------------
   c->cd(2);
   leg = new TLegend (0.3,0.85,0.7,0.98);
   mg = new TMultiGraph();

   xi = 0.001;
   gr = new TGraph(udist->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(1);
   mg->Add(gr,"l");
   //leg->AddEntry(gr,Form("#pi_{1}(#beta,#alpha=%.1f)",alpha),"l");

   xi = 0.01;
   gr = new TGraph(udist->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(4);
   //leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   mg->Add(gr,"l");

   xi = 0.05;
   gr = new TGraph(udist->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(2);
   //leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   mg->Add(gr,"l");

   xi = 0.1;
   gr = new TGraph(udist->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(8);
   //leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   mg->Add(gr,"l");

   xi = 0.2;
   gr = new TGraph(udist->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(7);
   //leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   mg->Add(gr,"l");

   mg->Draw("a");
   mg->SetTitle("quark");
   mg->GetXaxis()->SetTitle("x");
   mg->GetXaxis()->CenterTitle(true);
   //leg->Draw();
   //gPad->SetLogx(true);
   mg->Draw("a");

   //------------------------------------------
   c->cd(3);
   leg = new TLegend (0.3,0.85,0.7,0.98);
   mg = new TMultiGraph();

   xi = 0.001;
   gr = new TGraph(ubardist->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(1);
   mg->Add(gr,"l");
   //leg->AddEntry(gr,Form("#pi_{1}(#beta,#alpha=%.1f)",alpha),"l");

   xi = 0.01;
   gr = new TGraph(ubardist->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(4);
   //leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   mg->Add(gr,"l");

   xi = 0.05;
   gr = new TGraph(ubardist->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(2);
   //leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   mg->Add(gr,"l");

   xi = 0.1;
   gr = new TGraph(ubardist->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(8);
   //leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   mg->Add(gr,"l");

   xi = 0.2;
   gr = new TGraph(ubardist->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(7);
   //leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   mg->Add(gr,"l");

   mg->Draw("a");
   mg->SetTitle("sea");
   mg->GetXaxis()->SetTitle("x");
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle("xH^{sea}(x,#xi,t)");
   mg->GetYaxis()->CenterTitle(true);
   //leg->Draw();
   //gPad->SetLogx(true);
   mg->Draw("a");

   //------------------------------------------
   c->cd(4);
   leg = new TLegend (0.3,0.85,0.7,0.98);
   mg = new TMultiGraph();

   xi = 0.001;
   gr = new TGraph(uvaldist->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(1);
   mg->Add(gr,"l");
   //leg->AddEntry(gr,Form("#pi_{1}(#beta,#alpha=%.1f)",alpha),"l");

   xi = 0.01;
   gr = new TGraph(uvaldist->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(4);
   //leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   mg->Add(gr,"l");

   xi = 0.05;
   gr = new TGraph(uvaldist->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(2);
   //leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   mg->Add(gr,"l");

   xi = 0.1;
   gr = new TGraph(uvaldist->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(8);
   //leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   mg->Add(gr,"l");

   xi = 0.2;
   gr = new TGraph(uvaldist->DrawCopy("goff")->GetHistogram());
   gr->SetLineColor(7);
   //leg->AddEntry(gr,Form("#pi_{1}(#beta=%.1f,#alpha)",beta),"l");
   mg->Add(gr,"l");

   mg->Draw("a");
   mg->SetTitle("valence");
   mg->GetXaxis()->SetTitle("x");
   mg->GetXaxis()->CenterTitle(true);
   mg->GetYaxis()->SetTitle("H^{val}(x,#xi,t)");
   mg->GetYaxis()->CenterTitle(true);
   //leg->Draw();
   //gPad->SetLogx(true);
   mg->Draw("a");
}

