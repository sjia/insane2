void ideal_use()
{
  using namespace insane::physics;
  auto xsec = new DVCSDiffXSec();
  xsec->InitializePhaseSpaceVariables();
  xsec->Print();

}
