Int_t plot_pol_pdf_error_band(Int_t aNumber = 2){

  using namespace insane::physics;
   Double_t Q2 = 2.0;

   CJ12UnpolarizedPDFs * pdfs0 = new CJ12UnpolarizedPDFs();
   //BBPolarizedPDFs     * pdfs = new BBPolarizedPDFs();
   DSSVPolarizedPDFs     * pdfs = new DSSVPolarizedPDFs();

   Int_t Npoints = 100;

   TH1F * hu1     = new TH1F("Du","#Delta u",    Npoints ,0.01,0.99);
   TH1F * hubar1  = new TH1F("Dubar","#Delta u", Npoints ,0.01,0.99);
   TH1F * hd1     = new TH1F("Dd","#Delta d",    Npoints ,0.01,0.99);
   TH1F * hdbar1  = new TH1F("Ddbar","#Delta d", Npoints ,0.01,0.99);
   TH1F * hg1     = new TH1F("Dg","#Delta g",    Npoints ,0.01,0.99);

   TH1F * hDu1    = new TH1F("uErrorBand","u Error Band",    Npoints ,0.01,0.99);
   TH1F * hDubar1 = new TH1F("ubarErrorBand","u Error Band", Npoints ,0.01,0.99);
   TH1F * hDd1    = new TH1F("dErrorBand","d Error Band",    Npoints ,0.01,0.99);
   TH1F * hDdbar1 = new TH1F("dbarErrorBand","d Error Band", Npoints ,0.01,0.99);
   TH1F * hDg1    = new TH1F("gErrorBand","g Error Band",    Npoints ,0.01,0.99);

   pdfs->GetValues(hu1    , Q2 , PDFBase::kUP);
   pdfs->GetValues(hd1    , Q2 , PDFBase::kDOWN);
   pdfs->GetValues(hubar1 , Q2 , PDFBase::kANTIUP);
   pdfs->GetValues(hdbar1 , Q2 , PDFBase::kANTIDOWN);
   pdfs->GetValues(hg1    , Q2 , PDFBase::kGLUON);

   pdfs->GetErrorBand(hDu1    , Q2 , PDFBase::kUP);
   pdfs->GetErrorBand(hDd1    , Q2 , PDFBase::kDOWN);
   pdfs->GetErrorBand(hDubar1 , Q2 , PDFBase::kANTIUP);
   pdfs->GetErrorBand(hDdbar1 , Q2 , PDFBase::kANTIDOWN);
   pdfs->GetErrorBand(hDg1    , Q2 , PDFBase::kGLUON);

   TCanvas * c = new TCanvas();

   TColor *col46 = gROOT->GetColor(46);
   col46->SetAlpha(0.7);
   TColor *col41 = gROOT->GetColor(41);
   col41->SetAlpha(0.7);
   TColor *col36 = gROOT->GetColor(36);
   col36->SetAlpha(0.7);

   hDu1->SetFillColor(kRed);
   hDd1->SetFillColor(kBlue);
   hDubar1->SetFillColor(46);
   hDdbar1->SetFillColor(36);
   hDg1->SetFillColor(41);
   //hu1->SetFillColor(kRed);
   //hd1->SetFillColor(kBlue);
   //hg1->SetFillColor(kGreen);
   hu1->SetLineColor(1);
   hd1->SetLineColor(1);
   hg1->SetLineColor(1);
   hubar1->SetLineColor(1);
   hdbar1->SetLineColor(1);

   TMultiGraph * mg = new TMultiGraph();
   TGraph * gr = 0;

   TLegend * leg = new TLegend(0.65,0.75,0.88,0.88);
   leg->SetNColumns(2);
   leg->SetBorderSize(0);
   leg->SetBorderSize(0);
   leg->SetTextFont(gNiceFont);

   THStack * hs = new THStack("errorBands","");

   hs->Add(hDd1,"e3");
   hs->Add(hDu1,"e3");
   hs->Add(hDdbar1,"e3");
   hs->Add(hDubar1,"e3");
   hs->Add(hDg1,"e3");

   hs->Add(hg1,"C hist");
   hs->Add(hd1,"C hist");
   hs->Add(hu1,"C hist");
   hs->Add(hdbar1,"C hist");
   hs->Add(hubar1,"C hist");

   leg->AddEntry(hDu1,"#Deltau","lf");
   leg->AddEntry(hDd1,"#Deltad","lf");
   leg->AddEntry(hDubar1,"#Delta#bar{u}","lf");
   leg->AddEntry(hDdbar1,"#Delta#bar{d}","lf");
   leg->AddEntry(hDg1,"#Deltag","lf");

   hs->Draw("nostack");
   hs->GetXaxis()->CenterTitle(true);
   hs->GetXaxis()->SetTitle("x");
   hs->GetYaxis()->CenterTitle(true);
   hs->GetYaxis()->SetTitle("x f(x)");

   hs->SetMinimum(-0.19);
   hs->SetMaximum(0.39);

   leg->Draw();

   TLatex * t = new TLatex();
   t->SetNDC();
   t->SetTextFont(gNiceFont);
   t->SetTextColor(1);
   t->SetTextSize(0.05);
   t->SetTextAlign(12);
   t->DrawLatex(0.62,0.67,Form("Q^{2} = %d GeV^{2}",int(Q2)));

   c->SaveAs(Form("results/pdfs/plot_pol_pdf_error_band_%d.png",aNumber));
   c->SaveAs(Form("results/pdfs/plot_pol_pdf_error_band_%d.pdf",aNumber));

   return 0;
}
