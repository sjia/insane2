#ifndef DipoleFormFactors_HH
#define DipoleFormFactors_HH 1

#include "FormFactors.h"
#include "BonnWaveFunctions.h"

namespace insane {
  namespace physics {


    using namespace units;

    /** Simple Dipole Form Factors
     *
     * \ingroup formfactors
     */
    class DipoleFormFactors : public FormFactors {
      protected:
        double fDipoleFitParameter;


      public:
        DipoleFormFactors() {
          SetNameTitle("DipoleFormFactors","Dipole Form Factors");
          SetLabel("Dipole FFs");
          fDipoleFitParameter = 0.71; //GeV^2
        }

        virtual ~DipoleFormFactors() { }


        /** \f$ G_D(Q^2) = \frac{1}{(1+ \frac{Q^2}{0.71 GeV^2} )^2} \f$ */
        double DipoleFormFactor(double Qsquared) const {
          return(1.0 / TMath::Power(1.0 + Qsquared / fDipoleFitParameter , 2));
        }

        // proton
        /** \f$ G_{Ep}(Q^2) = G_D(Q^2) \f$ */
        virtual double GEp(double Qsq) const {
          return(DipoleFormFactor(Qsq));
        }

        /** \f$ \frac{G_{Mp}(Q^2)}{\mu_p} = G_D(Q^2) \f$ */
        virtual double GMp(double Qsq) const {
          return(Mu_p * DipoleFormFactor(Qsq));
        }

        // neutron
        virtual double GEn(double /*Qsq*/) {
          return(0.0);
        }
        virtual double GMn(double Qsq) {
          return(Mu_n * DipoleFormFactor(Qsq));
        }

      /** @name Deuteron Form Factors
       *
       *
       *  - F_{C0} : charge   monopole
       *  - F_{M1} : magnetic dipole
       *  - F_{C2} : charge   quadrupole
       *
       * \todo Add IA relations that use the deuteron WF to calculate form factors
       * from proton and neutron form factors.
       * 
       * References: 
       *
       *  1. https://inspirehep.net/record/565071
       *
       */
      //@{ 

        virtual double F_C0(double Qsq);
        virtual double F_M1(double Qsq);
        virtual double F_C2(double Qsq);

        struct WaveFunctionIAFormFactors {
          BonnDeuteronWaveFunction wf;
          FormFactors* ffs = 0;
          double F_l( double Qsq) const ;
          double F_s( double Qsq) const ;
          double G_es(FormFactors* ffs, double Qsq) const ;
          double G_ms(FormFactors* ffs, double Qsq) const ;
          double F_C0(FormFactors* ffs, double Qsq) const ;
          double F_M1(FormFactors* ffs, double Qsq) const ;
          double F_C2(FormFactors* ffs, double Qsq) const ;
        };

        WaveFunctionIAFormFactors WF_FFs;

        virtual double GEd(double Qsq) {
          return(DipoleFormFactor(Qsq));
        }
        virtual double GMd(double Qsq) {
          return(Mu_d * DipoleFormFactor(Qsq));
        }

        // Helium-3 
        virtual double GEHe3(double /*Qsq*/) {
          return(0.0);
        }
        virtual double GMHe3(double /*Qsq*/) {
          return(0.0);
        }
        //@}

        // Helium-4 
        virtual double FCHe4(double Qsq) {
          double a = 0.316;
          double b = 0.681;
          double q2 = Qsq/(hbarc_gev_fm*hbarc_gev_fm);
          double res = (1.0 - TMath::Power(a*a*q2,6.0))*TMath::Exp(-b*b*q2);
          return res;
        }
        ClassDef(DipoleFormFactors, 1)
    };

    using DefaultFormFactors = DipoleFormFactors;



  }
}
#endif

