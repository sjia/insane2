#ifndef insane_physics_fwd_FFs_HH
#define insane_physics_fwd_FFs_HH

#include "DipoleFormFactors.h"
#include "GalsterFormFactors.h"
#include "AHLYFormFactors.h"
#include "AMTFormFactors.h"
#include "AmrounFormFactors.h"
#include "BilenkayaFormFactors.h"
#include "BostedFormFactors.h"
//#include "F1F209QuasiElasticFormFactors.h"
#include "KellyFormFactors.h"
#include "MSWFormFactors.h"
#include "RiordanFormFactors.h"


namespace insane {
  namespace physics {

    using AMT_FFs_t   =  insane::physics::AMTFormFactors;
    using AHLY_FFs_t  =  insane::physics::AHLYFormFactors;
    using Kelly_FFs_t =  insane::physics::KellyFormFactors;

  }
}
#endif

