#ifndef InSANEFormFactors_H
#define InSANEFormFactors_H 1

#include <iostream> 
#include "TNamed.h"
#include "TMath.h"
#include "InSANEPhysicalConstants.h"
#include "TString.h"

/** ABC for calculating the form factors
 *
 * \ingroup formfactors
 */
class InSANEFormFactors : public TNamed {

   protected:
      TString fLabel;

   public:

      InSANEFormFactors();
      virtual ~InSANEFormFactors();

      void        SetLabel(const char * l){ fLabel = l;}
      const char* GetLabel(){ return fLabel; }

      virtual Double_t GEp(Double_t Qsq) = 0;
      virtual Double_t GMp(Double_t Qsq) = 0;
      virtual Double_t GEn(Double_t Qsq) = 0;
      virtual Double_t GMn(Double_t Qsq) = 0;

      virtual Double_t GEd(Double_t Qsq) = 0;
      virtual Double_t GCd(Double_t Qsq) { return (GEd(Qsq)); }
      virtual Double_t GMd(Double_t Qsq) = 0;
      virtual Double_t GQd(Double_t Qsq) { return 0.0 ; }
      virtual Double_t Ad(Double_t Qsq) {
         double eta = Qsq/(4.0*(insane::units::M_d*insane::units::M_d)/(GeV*GeV));
         double GC = GCd(Qsq);
         double GM = GMd(Qsq);
         double GQ = GQd(Qsq);
         return( GC*GC+(8.0/9.0)*eta*eta*GQ*GQ+(2.0/3.0)*eta*GM*GM );
      }
      virtual Double_t Bd(Double_t Qsq) {
         double eta = Qsq/(4.0*(insane::units::M_d*insane::units::M_d)/(GeV*GeV));
         double GM = GMd(Qsq);
         return( (4.0/3.0)*eta*(1.0+eta)*GM*GM );
      }
      //virtual Double_t T20d(Double_t Qsq) {
      //   double eta = Qsq/(4.0*(insane::units::M_d*insane::units::M_d)/(GeV*GeV));
      //   double GC = GCd(Qsq);
      //   double GM = GMd(Qsq);
      //   double GQ = GQd(Qsq);
      //   double T = -1.0/(TMath::Sqrt(2.0)*(Ad(Qsq)+Bd(Qsq)*TMath::Tan
      //   return( );
      //}
              

      virtual Double_t GEHe3(Double_t Qsq) = 0;
      virtual Double_t GMHe3(Double_t Qsq) = 0;

      virtual Double_t GEHe4(Double_t Qsq) { return FCHe4(Qsq); }
      virtual Double_t FCHe4(Double_t Qsq) { return 0.0; }

      Double_t g1p_el(Double_t Qsq) ;
      Double_t g2p_el(Double_t Qsq) ;

      Double_t g1n_el(Double_t Qsq) ;
      Double_t g2n_el(Double_t Qsq) ;

      Double_t d2p_el(Double_t Qsq) ;
      Double_t d2n_el(Double_t Qsq) ;

      Double_t d2p_el_Nachtmann(Double_t Qsq) ;
      Double_t d2n_el_Nachtmann(Double_t Qsq) ;

      virtual Double_t GENuclear(Double_t Q2, Double_t Z, Double_t A);
      virtual Double_t GMNuclear(Double_t Q2, Double_t Z, Double_t A);
      virtual Double_t GENuclear2(Double_t Q2, Double_t Z, Double_t A);
      virtual Double_t GMNuclear2(Double_t Q2, Double_t Z, Double_t A);

      // Quasi-elastic form factors 
      virtual Double_t GEdQE(Double_t   /*x*/,Double_t /*Qsq*/){return 0.0;}    
      virtual Double_t GMdQE(Double_t   /*x*/,Double_t /*Qsq*/){return 0.0;}    
      virtual Double_t GEHe3QE(Double_t /*x*/,Double_t /*Qsq*/){return 0.0;}    
      virtual Double_t GMHe3QE(Double_t /*x*/,Double_t /*Qsq*/){return 0.0;}    


      virtual Double_t GE_A(Double_t Z,Double_t A,Double_t Q2){
         // GE, GM for Z > 6 
         // Forms for GE and GM derived from expressions for W1 and W2  
         // from Stein et al, Phys Rev D 12, 1884 (1975)
         Double_t M_A   = A*M_p/GeV; 
         Double_t tau   = Q2/(4.*M_A*M_A);
         Double_t Q2_fm = Q2/(hbarc_gev_fm*hbarc_gev_fm);
         Double_t b     = 2.4;                         // fm 
         Double_t c     = 1.07*TMath::Power(A,-1./3.); // fm 
         Double_t F     = TMath::Exp( (-1.)*(Q2_fm*b*b/6.) )/(1. + Q2_fm*c*c/6.);
         Double_t GEA   = TMath::Sqrt(1.+tau)*Z*F;
         return GEA;
      }

      virtual Double_t GM_A(Double_t /*Z*/,Double_t /*A*/,Double_t /*Q2*/){
         return 0;
      }

      double Evaluated2p_el(double *x, double * /*p*/) {
         return(d2p_el(x[0]));
      }
      double Evaluated2n_el(double *x, double * /*p*/) {
         return(d2n_el(x[0]));
      }
      double EvaluateGEp(double *x, double * /*p*/) {
         return(GEp(x[0]));
      }
      double EvaluateGMp(double *x, double * /*p*/) {
         return(GMp(x[0]));
      }
      double EvaluateGMp_over_Mu(double *x, double * /*p*/) {
         return(GMp(x[0]) / Mu_p);
      }

      virtual Double_t FLp(Double_t Qsq) {
         Double_t tau = Qsq / (4.0 * M_p/GeV * M_p/GeV);
         return((1.0 + tau) * GEp(Qsq) / (TMath::Sqrt(4.0 * TMath::Pi())));
      }
      virtual Double_t FTp(Double_t Qsq) {
         Double_t tau = Qsq / (4.0 * M_p/GeV * M_p/GeV);
         return(-1.0 * TMath::Sqrt((2.0 * tau * (1.0 + tau)) / (4.0 * TMath::Pi())) * GMp(Qsq));
      }

      /** Sachs Form Factors.
       * F1 = (GE + GM*(Q^2/4M^2))/(1+(Q^2/4M^2))
       * F2 = (GM - GE)/(1+(Q^2/4M^2))
       */
      double F1p(double Q2) {
         double GE = GEp(Q2);
         double GM = GMp(Q2);
         double t  = Q2/(4.0*0.938*0.938);
         return (GE + GM*t)/(1+t);
      }
      double F2p(double Q2) {
         double GE = GEp(Q2);
         double GM = GMp(Q2);
         double t  = Q2/(4.0*0.938*0.938);
         return (GM - GE)/(1+t);
      }

      /** Sachs Form Factors. 
       * F1 = (GE + GM*(Q^2/4M^2))/(1+(Q^2/4M^2))
       * F2 = (GM - GE)/(1+(Q^2/4M^2))
       */
      double F1n(double Q2) {
         double GE = GEn(Q2);
         double GM = GMn(Q2);
         double t  = Q2/(4.0*0.938*0.938);
         return (GE + GM*t)/(1+t);
      }
      double F2n(double Q2) {
         double GE = GEn(Q2);
         double GM = GMn(Q2);
         double t  = Q2/(4.0*0.938*0.938);
         return (GM - GE)/(1+t);
      }


      ClassDef(InSANEFormFactors,2)
};


#endif



