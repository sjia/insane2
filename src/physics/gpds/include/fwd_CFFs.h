#ifndef insane_physics_fwd_CFFs_HH
#define insane_physics_fwd_CFFs_HH

#include "CFFsFromGPDs.h"
#include "DD_GPDs.h"
#include "GK_GPDs.h"

namespace insane {
  namespace physics {

    using DD_CFFs =  insane::physics::CFFsFromGPDs<insane::physics::DD_GPDs>;
    using GK_CFFs =  insane::physics::CFFsFromGPDs<insane::physics::GK_GPDs>;

  }
}

#endif

