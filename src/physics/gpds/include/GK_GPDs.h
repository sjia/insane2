#ifndef InSANE_physics_GK_GPDs

#define InSANE_physics_GK_GPDs
#include "GeneralizedPartonDistributions.h"

namespace insane {
  namespace physics {

    class GK_GPDs {

    protected:

      const double fn_g   = 2.0;
      const double fn_sea = 2.0;
      const double fn_val = 1.0;

      const std::vector<double> fn = { 2.0, 2.0, 1.0 };

      const std::vector<std::vector<double>> fdelta = {
        {0.10, 0.06, 0.0027},
        {1.10, 0.06, 0.0027},
        {0.48, 0.0,  0.0},
        {0.48, 0.0,  0.0}
      };
      const std::vector<std::vector<std::vector<double>>> fcij = {
        { {2.23,0.36}, {5.43, 7.00}, {34.0, 22.5},{40.6, 21.6} }, // gluon
        { {0.123, 0.0003}, {-0.327, -0.004}, {0.692, -0.068}, {-0.486,0.038} }, //strange
        { {1.52,0.248}, {2.88,-0.940}, {0.0,-0.095}, {0.0,0.0} }, // up
        { {0.76,0.248}, {3.11,-1.36}, {-3.99,1.15}, {0.0,0.0} }   // down
      };
      const std::vector<double> falpha_prime = { 0.15, 0.15, 0.9, 0.9 };

      double c(int i, int j, double Q2) const{
        using namespace TMath;
        double L = Log(Q2/4.0);
        return fcij.at(i).at(j).at(0) + fcij.at(i).at(j).at(1)*L;
      }

      double kappa_s(double Q2) const {
        using namespace TMath;
        double L = Log(Q2/4.0);
        return( 1.0 + 1.68/(1.0+0.52*L) );
      }


      // Htilde parameters from http://inspirehep.net/record/1193373
      //
      const std::vector<double> alpha_tilde_0     = {0.48,0.48}; // uval and dval
      const std::vector<double> alpha_prime_tilde = {0.45,0.45};
      const std::vector<double> b_h_tilde         = {0.0 ,0.0}; 
      const std::vector<std::vector<std::vector<double>>> fc_tilde = {
        {{0.170, 0.03}, {1.34, -0.02}, {0.12, -0.40} },
        {{-0.32, -0.04}, {-1.427, -0.176}, {0.692, -0.068} }
      };
      const std::vector<double> eta         = {0.926, -0.341}; 

      double Aq(int f, double Q2) const
      {
        using namespace TMath;
        double L = Log(Q2/4.0);
        double aht = alpha_tilde_0.at(f);;
        double T0 = Beta(1.0-aht,4.0);
        double c0 = fc_tilde.at(f).at(0).at(0)+ fc_tilde.at(f).at(0).at(1)*L;
        double c1 = fc_tilde.at(f).at(1).at(0)+ fc_tilde.at(f).at(1).at(1)*L;
        double c2 = fc_tilde.at(f).at(2).at(0)+ fc_tilde.at(f).at(2).at(1)*L;
        double T1 = (c0+c1*(1.0-aht)/(5.0-aht) + c2*(2.0-aht)*(1.0-aht)/((6.0-aht)*(5.0-aht)));
        return 1.0/(T0*T1);
      }

    public:

      GK_GPDs(){ }
      ~GK_GPDs(){ }
      double H_u(   double x, double xi, double t, double Q2) const ;
      double H_d(   double x, double xi, double t, double Q2) const ;
      double H_ubar(double x, double xi, double t, double Q2) const ;
      double H_dbar(double x, double xi, double t, double Q2) const ;

      double H_g(   double x, double xi, double t, double Q2) const ;
      double Htilde_g(   double x, double xi, double t, double Q2) const ;

      double delta(int f, int i, double Q2) const;

      double b(int i, double Q2) const;
      double n(int i) const ;
      double alpha_prime(int f) const;

      double Hij(int i, int j, int f, double x, double xi, double t, double Q2) const ;
      double h(int f, int i, double beta, double Q2) const;
      double f(int f, int i, double beta, double alpha, double t, double Q2) const;
      double H_i_integrand(int f, int i,double x, double xi, double t, double Q2,double bb) const;
      double H_i(int f, int i,double x, double xi, double t, double Q2) const;
      double Htilde_i_integrand(int f, int i,double x, double xi, double t, double Q2,double bb) const;
      double H_i_integrate_DD(int f, int i,double x, double xi, double t, double Q2) const;
      double Htilde_i(int f, int i,double x, double xi, double t, double Q2) const;


      ClassDef(GK_GPDs,1)
    };

  }
}
#endif

