#ifndef InSANE_physics_GeneralizedPartonDistributions
#define InSANE_physics_GeneralizedPartonDistributions

#include "TMath.h"
#include <vector>
#include "TF1.h"
#include "Math/WrappedTF1.h"
#include "Math/GaussIntegrator.h"
#include "Math/GSLIntegrator.h"

namespace insane {
   namespace physics {

      /**
       *  \f$ i = (0,1,2,3,4,5,6,7,8,9,10,11) = (u,d,s,c,b,t,g,\bar{u},\bar{d},\bar{s},\bar{c},\bar{b},\bar{t}) \f$
       */
      class GeneralizedPartonDistributions {
         
         public:
            GeneralizedPartonDistributions(){ }
            virtual ~GeneralizedPartonDistributions(){ }

            virtual double H(      int i, double x, double xi, double t, double Q2) const;
            virtual double E(      int i, double x, double xi, double t, double Q2) const;
            virtual double H_tilde(int i, double x, double xi, double t, double Q2) const;
            virtual double E_tilde(int i, double x, double xi, double t, double Q2) const;

            virtual double H_u(   double x, double xi, double t, double Q2) const ;
            virtual double H_d(   double x, double xi, double t, double Q2) const ;
            virtual double H_ubar(double x, double xi, double t, double Q2) const ;
            virtual double H_dbar(double x, double xi, double t, double Q2) const ;

            virtual double H_g(   double x, double xi, double t, double Q2) const ;
            virtual double Htilde_g(   double x, double xi, double t, double Q2) const ;

            virtual double Htilde_u(   double x, double xi, double t, double Q2) const ;
            virtual double Htilde_d(   double x, double xi, double t, double Q2) const ;
            virtual double Htilde_ubar(double x, double xi, double t, double Q2) const ;
            virtual double Htilde_dbar(double x, double xi, double t, double Q2) const ;

            ClassDef(GeneralizedPartonDistributions,1)
      };

   }
}

#endif
