#ifndef insane_physics_CFFsFromGPDs_HH
#define insane_physics_CFFsFromGPDs_HH

#include "SystemOfUnits.h"
#include "PhysicalConstants.h"
#include <algorithm>
#include "Math/Integrator.h"
#include "Math/GSLIntegrator.h"
#include <complex>
#include "Physics.h"

namespace insane {
  namespace physics {

    using namespace units;

    /** Compton Form Factors.
     *
     */
    template<typename GPDs>
    struct CFFsFromGPDs {

      using GPD_t = std::decay_t<GPDs>;

      GPD_t fGPDs;


      std::complex<double> H_p( double xi, double t, double Q2) const
      {
        std::complex<double> res = 0.0;
        //for( const auto& q : {Parton::u, Parton::d, Parton::s} ) {}
        res += PartonCharge2[kUP]*H_u(xi,t,Q2);
        res += PartonCharge2[kDOWN]*H_d(xi,t,Q2);
        return res;
      }

      std::complex<double> H_n( double xi, double t, double Q2) const
      {
        std::complex<double> res = 0.0;
        res += PartonCharge2[kUP]  *H_d(xi,t,Q2);
        res += PartonCharge2[kDOWN]*H_u(xi,t,Q2);
        return res;
      }

      std::complex<double> H_u( double xi, double t, double Q2) const
      {
        using namespace std::complex_literals;
        double  M    = M_p_GeV;
        double  tmin = -4.0*xi*xi*M*M/(1.0-xi*xi);
        if( t > tmin) { return 0.0; }
        auto func = [&](double x_var) { return fGPDs.H_u(x_var,xi,t,Q2);};
        ROOT::Math::IntegratorOneDim ig(func, ROOT::Math::IntegrationOneDim::kDEFAULT, 1.0e-2, 1.0e-2, 1e7); 
        double H_re = ig.IntegralCauchy( -1.0, 1.0, xi);
        if( ig.Status() ) {
          std::cout << "    xi : " << xi << "\n";
          std::cout << "  H_re : " << H_re << "\n";
          std::cout << "status : " << ig.Status() << "\n";
        }
        double H_im = pi*(fGPDs.H_u(xi, xi,t,Q2) - fGPDs.H_u(-xi, xi,t,Q2));
        if( std::isnan(H_re) ) { H_re = 0.0; }
        if( std::isnan(H_im) ) { H_im = 0.0; }
        return H_re + 1i*H_im;
      }

      std::complex<double> H_d( double xi, double t, double Q2) const
      {
        using namespace std::complex_literals;
        double M = M_p_GeV;
        auto tmin = -4.0*xi*xi*M*M/(1.0-xi*xi);
        if( t > tmin) { return 0.0; }
        auto func = [&](double var) { return fGPDs.H_d(var,xi,t,Q2);};
        ROOT::Math::IntegratorOneDim ig(func, ROOT::Math::IntegrationOneDim::kDEFAULT, 1.0e-3, 1.0e-4, 1e3); 
        double H_re = ig.IntegralCauchy( -1.0, 1.0, xi);
        if( std::isnan(H_re) ) { H_re = 0.0; }
        double H_im = pi*(fGPDs.H_d(xi, xi,t,Q2) - fGPDs.H_d(-xi, xi,t,Q2));
        if( std::isnan(H_im)) { H_im = 0.0; }
        return H_re + 1i*H_im;
      }

      std::complex<double> H_ubar( double xi, double t, double Q2) const
      {
        using namespace std::complex_literals;
        double M = M_p_GeV;
        auto tmin = -4.0*xi*xi*M*M/(1.0-xi*xi);
        if( t > tmin) { return 0.0; }
        auto func = [&](double var) { return fGPDs.H_ubar(var,xi,t,Q2);};
        ROOT::Math::IntegratorOneDim ig(func, ROOT::Math::IntegrationOneDim::kDEFAULT, 1.0e-3, 1.0e-4, 1e3); 
        double H_re = ig.IntegralCauchy( -1.0, 1.0, xi);
        if( std::isnan(H_re) ) { H_re = 0.0; }
        double H_im = pi*(fGPDs.H_ubar(xi, xi,t,Q2) - fGPDs.H_ubar(-xi, xi,t,Q2));
        if( std::isnan(H_im)) { H_im = 0.0; }
        return H_re + 1i*H_im;
      }

      std::complex<double> H_dbar( double xi, double t, double Q2) const
      {
        using namespace std::complex_literals;
        double M = M_p_GeV;
        auto tmin = -4.0*xi*xi*M*M/(1.0-xi*xi);
        if( t > tmin) { return 0.0; }
        auto func = [&](double var) { return fGPDs.H_dbar(var,xi,t,Q2);};
        ROOT::Math::IntegratorOneDim ig(func, ROOT::Math::IntegrationOneDim::kDEFAULT, 1.0e-3, 1.0e-4, 1e3); 
        double H_re = ig.IntegralCauchy( -1.0, 1.0, xi);
        if( std::isnan(H_re) ) { H_re = 0.0; }
        double H_im = pi*(fGPDs.H_dbar(xi, xi,t,Q2) - fGPDs.H_dbar(-xi, xi,t,Q2));
        if( std::isnan(H_im)) { H_im = 0.0; }
        return H_re + 1i*H_im;     // imaginary unit squared
      }

    };

  }
}
#endif

