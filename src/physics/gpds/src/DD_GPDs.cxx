#include "DD_GPDs.h"
#include "TMath.h"
#include "SystemOfUnits.h"
#include <algorithm>
#include "Math/Integrator.h"

namespace insane {
  namespace physics {

    double DD_GPDs::H_u( double x, double xi, double t, double Q2) const
    {
      return( H_ubar(x, xi, t, Q2) + H_uval(x, xi, t, Q2) );
    }

    double DD_GPDs::H_uval( double x, double xi, double t, double Q2) const
    {
      using namespace TMath;
      const DD_GPDs& gpds = *this;
      auto l = [&](double* v, double *p){
        double beta  = v[0];
        double alpha = (x-beta)/xi;
        if( ( alpha < (-1.0+Abs(beta)) ) 
           || ( alpha > (1.0-Abs(beta)) ) ) {
          return 0.0;
        }
        double piN = ProfileFunction(1.0,beta,alpha);
        double fq  = fPDFs.u(beta,Q2) - fPDFs.uBar(beta,Q2);
        return( piN*fq/TMath::Abs(xi) );
      };
      return H_DD(l, x, xi, t, Q2);
    }

    double DD_GPDs::H_ubar(double x, double xi, double t, double Q2) const
    {
      using namespace TMath;
      double sign = 1.0;
      if( x < 0.0) {
        sign = -1.0;
      }
      const DD_GPDs& gpds = *this;
      auto l = [&](double* v, double *p){
        double beta  = v[0];
        double alpha = (x-beta)/xi;
        if( ( alpha < (-1.0+Abs(beta)) ) 
           || ( alpha > (1.0-Abs(beta)) ) ) {
          return 0.0;
        }
        double piN = ProfileFunction(1.0,beta,alpha);
        double fq  = copysign(1.0,beta)*fPDFs.uBar(Abs(beta),Q2);
        //double fq  = fPDFs.u(beta,Q2) - fPDFs.uBar(beta,Q2);
        return( piN*fq/TMath::Abs(xi) );
      };
      return sign*H_DD(l, x, xi, t, Q2);
    }
    //______________________________________________________________________________

    double DD_GPDs::H_d( double x, double xi, double t, double Q2) const
    {
      return( H_dbar(x, xi, t, Q2) + H_dval(x, xi, t, Q2) );
    }
    //______________________________________________________________________________

    double DD_GPDs::H_dval( double x, double xi, double t, double Q2) const
    {
      using namespace TMath;
      const DD_GPDs& gpds = *this;
      auto l = [&](double* v, double *p){
        double beta  = v[0];
        double alpha = (x-beta)/xi;
        if( ( alpha < (-1.0+Abs(beta)) ) 
           || ( alpha > (1.0-Abs(beta)) ) ) {
          return 0.0;
        }
        double piN = ProfileFunction(1.0,beta,alpha);
        double fq  = fPDFs.d(beta,Q2) - fPDFs.dBar(beta,Q2);
        return( piN*fq/TMath::Abs(xi) );
      };
      return H_DD(l, x, xi, t, Q2);
    }
    //______________________________________________________________________________

    double DD_GPDs::H_dbar(double x, double xi, double t, double Q2) const
    {
      using namespace TMath;
      double sign = 1.0;
      if( x < 0.0) {
        sign = -1.0;
      }
      const DD_GPDs& gpds = *this;
      auto l = [&](double* v, double *p){
        double beta  = v[0];
        double alpha = (x-beta)/xi;
        if( ( alpha < (-1.0+Abs(beta)) ) 
           || ( alpha > (1.0-Abs(beta)) ) ) {
          return 0.0;
        }
        double piN = ProfileFunction(1.0,beta,alpha);
        double fq  = Abs(beta)*fPDFs.dBar(Abs(beta),Q2);
        return( piN*fq/TMath::Abs(xi) );
      };
      return sign*H_DD(l, x, xi, t, Q2);
    }
    //______________________________________________________________________________

    double DD_GPDs::H_g(   double x, double xi, double t, double Q2) const 
    {
      using namespace TMath;
      double sign = 1.0;
      if( x < 0.0) {
        sign = 1.0;
      }
      const DD_GPDs& gpds = *this;
      auto l = [&](double* v, double *p){
        double beta  = v[0];
        double alpha = (Abs(x)-beta)/xi;
        if( ( alpha < (-1.0+Abs(beta)) ) 
           || ( alpha > (1.0-Abs(beta)) ) ) {
          return 0.0;
        }
        double piN = ProfileFunction(1.0,beta,alpha);
        double fq  = Abs(beta)*fPDFs.g(Abs(beta),Q2);
        return( piN*fq/TMath::Abs(xi) );
      };
      return H_DD(l, x, xi, t, Q2);
    }

    //double DD_GPDs::Htilde_g(   double x, double xi, double t, double Q2) const 
    //{ return Htilde_i(0,0,x,xi,t,Q2);}
    //______________________________________________________________________________

    double DD_GPDs::ProfileFunction(double N, double beta, double alpha) const
    {
      // profile function \$f \pi_N(\beta,\alapha \$f
      using namespace TMath;
      using namespace insane::units;
      if( Abs(beta)+Abs(alpha) >= 1.0 ) {
        return 0.0;
      }
      double num = Gamma((3.0/2.0) +N)*Power(Power(1.0-Abs(beta),2.0)-alpha*alpha,N);
      double den = Sqrt(pi)*Gamma(1.0+N)*Power(1-Abs(beta),2.0*N+1.0);
      return num/den;
    }
    //______________________________________________________________________________

    double DD_GPDs::H_u_integrand(double x, double xi, double t, double Q2,double bb) const
    {
      using namespace TMath;
      // Create the function and wrap it
      const DD_GPDs& gpds = *this;
      auto l = [&](double v){//, double *p){
        double beta  = v;
        double alpha = (Abs(x)-beta)/xi;
        if( ( alpha < (-1.0+Abs(beta)) ) 
           || ( alpha > (1.0-Abs(beta)) ) ) {
          return 0.0;
        }
        double piN = ProfileFunction(1.0,beta,alpha);
        double fq  = Abs(beta)*fPDFs.g(beta,Q2);
        return( piN*fq/TMath::Abs(xi) );
      };
      //TF1 fDD("DD",l, -1.0, 1.0, 0);
      return l(bb);
    }
    //______________________________________________________________________________

    //double DD_GPDs::H_ubar( double x, double xi, double t, double Q2) const
    //{
    //   using namespace TMath;
    //   const DD_GPDs& gpds = *this;
    //   auto l = [&](double* v, double *p){
    //      double beta  = v[0];
    //      double alpha = (x-beta)/xi;
    //      if( ( alpha < (-1.0+Abs(beta)) ) 
    //            || ( alpha > (1.0-Abs(beta)) ) ) {
    //         return 0.0;
    //      }
    //      double piN = ProfileFunction(1.0,beta,alpha);
    //      double fq  = copysign(1.0,beta)*fPDFs.uBar(Abs(beta),Q2);
    //      //double fq  = fPDFs.u(beta,Q2) - fPDFs.uBar(beta,Q2);
    //      return( piN*fq/TMath::Abs(xi) );
    //   };
    //   double res = 0.0;
    //   double sign = 1.0;
    //   if( x < 0.0) {
    //      sign = -1.0;
    //   }
    //   if( x < -1.0*Abs(xi) ) {
    //      res = H_DDintegral_DGLAP_low(l, x, xi, t, Q2);
    //      //res =  H_u_DGLAP_low( x, xi, t, Q2);
    //   } else if( Abs(x) <= Abs(xi) ) {
    //      res = H_DDintegral_ERBL(l, x, xi, t, Q2);
    //      //res =  H_u_ERBL( x, xi, t, Q2);
    //   } else {
    //      res = H_DDintegral_DGLAP_high(l, x, xi, t, Q2);
    //      //res =  H_u_DGLAP_high( x, xi, t, Q2);
    //   }
    //   return sign*res;
    //}
    //______________________________________________________________________________


    double DD_GPDs::H_DD(std::function<double(double*,double*)> f, double x, double xi, double t, double Q2) const
    {
      using namespace TMath;
      double res = 0.0;
      if( x < -1.0*Abs(xi) ) {
        res = H_DDintegral_DGLAP_low(f, x, xi, t, Q2);
      } else if( Abs(x) <= Abs(xi) ) {
        res = H_DDintegral_ERBL(f, x, xi, t, Q2);
      } else {
        res = H_DDintegral_DGLAP_high(f, x, xi, t, Q2);
      }
      return res;
    }
    //______________________________________________________________________________

    double DD_GPDs::H_DDintegral_DGLAP_low( std::function<double(double*,double*)> f, double x, double xi, double t, double Q2) const
    {
        //{
        //  ROOT::Math::IntegratorMultiDim ig2(ROOT::Math::IntegrationMultiDim::kVEGAS); 
        //  ig2.SetFunction(ixs2);

        //  const auto& x_v0   = std::get<0>(ixs2.fXS.fPS.fDiff.fIndVars);
        //  const auto& Q2_v1  = std::get<1>(ixs2.fXS.fPS.fDiff.fIndVars);
        //  const auto& phi_v2 = std::get<2>(ixs2.fXS.fPS.fDiff.fIndVars);

        //  std::vector<double> a_min = {x_v0.Min(), Q2_v1.Min(), phi_v2.Min()};
        //  std::vector<double> b_max = {x_v0.Max(), Q2_v1.Max(), phi_v2.Max()};

        //  double val = ig2.Integral(a_min.data(), b_max.data() );
        //  std::cout << "integral result is " << val << std::endl;
        //  ixs2_val = val;
        //}
      using namespace TMath;
      //TF1 fDD("DD",f, -1.0, 1.0, 0);
      //ROOT::Math::WrappedTF1 wf1(fDD);
      ROOT::Math::IntegratorOneDim ig(ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR, 1.0e-8, 1.0e-8, 1e7); 
      auto func = [&](double var) { double xv, xp; return f(&xv,&xp);};
      ig.SetFunction(func);
      double eps = 0.01*Abs(xi)/(0.01 + Abs(xi));
      double eps0 = 0.001;
      double x0  = x;
      double temp  = 0;
      double min = x0-eps;
      double max = x0+eps;
      if( min < -1.0) {
        min = -1.0;
      }
      if( max > 1.0) {
        max = 1.0;
      }
      int nsplit = 5;
      std::vector<double> limits;
      for(int i = 0; i< nsplit;i++) {
        limits.push_back((double(i)-double(nsplit)/2.0)*2.0/double(nsplit));
      }
      limits.push_back(-eps0);
      limits.push_back(eps0);
      limits.push_back(min);
      limits.push_back(max);
      std::sort(limits.begin(), limits.end());
      double res = 0.0;
      int Nint = limits.size() -1;
      for(int i = 0; i<Nint; i++) {
        temp = ig.Integral( limits[i], limits[i+1]);
        res += temp;
        //std::cout << i << " (" << limits[i] << ", " << limits[i+1] << ") : " << temp << ", \n";
      }
      return res;
    }
    //______________________________________________________________________________

    double DD_GPDs::H_DDintegral_DGLAP_high(std::function<double(double*,double*)> f, double x, double xi, double t, double Q2) const
    {
      using namespace TMath;
      //TF1 fDD("DD",f, -1.0, 1.0, 0);
      //ROOT::Math::WrappedTF1 wf1(fDD);
      ////ROOT::Math::GSLIntegrator ig(ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR, 1.0e-8, 1.0e-8, 1e7); 
      //ROOT::Math::GSLIntegrator ig(ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR, 1.0e-7, 1.0e-9, 1e7); 
      ROOT::Math::IntegratorOneDim ig(ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR, 1.0e-8, 1.0e-8, 1e7); 
      auto func = [&](double var) { double xv, xp; return f(&xv,&xp);};
      ig.SetFunction(func);
      double eps = 0.01*Abs(xi)/(0.01 + Abs(xi));
      //double eps = 0.01*TMath::Abs(xi);//TMath::Abs((Abs(xi)+0.001)/(0.0001 + Abs(Abs(x)-Abs(xi))));
      double eps0 = 0.001;
      double x0  = x;
      double temp  = 0;
      double min = x0-eps;
      double max = x0+eps;
      if( min < -1.0) {
        min = -1.0;
      }
      if( max > 1.0) {
        max = 1.0;
      }
      int nsplit = 5;
      std::vector<double> limits;
      for(int i = 0; i< nsplit;i++) {
        limits.push_back((double(i)-double(nsplit)/2.0)*2.0/double(nsplit));
      }
      limits.push_back(-eps0);
      limits.push_back(eps0);
      limits.push_back(min);
      limits.push_back(max);
      std::sort(limits.begin(), limits.end());
      double res = 0.0;
      int Nint = limits.size() -1;
      for(int i = 0; i<Nint; i++) {
        temp = ig.Integral( limits[i], limits[i+1]);
        res += temp;
        //std::cout << i << " (" << limits[i] << ", " << limits[i+1] << ") : " << temp << ", \n";
      }
      return res;
    }
    //______________________________________________________________________________

    double DD_GPDs::H_DDintegral_ERBL(std::function<double(double*,double*)> f, double x, double xi, double t, double Q2) const
    {
      using namespace TMath;
      //TF1 fDD("DD",f, -1.0, 1.0, 0);
      //ROOT::Math::WrappedTF1 wf1(fDD);
      //ROOT::Math::GSLIntegrator ig(ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR, 1.0e-2, 1.0e-5, 1e7); 
      ROOT::Math::IntegratorOneDim ig(ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR, 1.0e-8, 1.0e-8, 1e7); 
      auto func = [&](double var) { double xv, xp; return f(&xv,&xp);};
      ig.SetFunction(func);
      double eps = TMath::Abs(xi)/(0.01 + Abs(xi));
      double eps0 = 0.0001;//TMath::Abs(x)/(0.01 + Abs(x));
      //double eps0 = 0.001;
      double x0  = x;
      double temp  = 0;
      int nsplit = 5;
      std::vector<double> limits;
      for(int i = 0; i< nsplit;i++) {
        limits.push_back((double(i)-double(nsplit)/2.0)*2.0/double(nsplit));
      }
      limits.push_back(-eps0);
      limits.push_back(eps0);
      std::sort(limits.begin(), limits.end());
      double res = 0.0;
      int Nint = limits.size() -1;
      for(int i = 0; i<Nint; i++) {
        temp = ig.Integral( limits[i], limits[i+1]);
        res += temp;
        //std::cout << i << " (" << limits[i] << ", " << limits[i+1] << ") : " << temp << ", \n";
      }
      return res;
    }
    //______________________________________________________________________________

    double DD_GPDs::H_u_DGLAP_low( double x, double xi, double t, double Q2) const
    {
      using namespace TMath;
      const DD_GPDs& gpds = *this;
      auto l = [&](double* v, double *p){
        double beta  = v[0];
        double alpha = (x-beta)/xi;
        if( ( alpha < (-1.0+Abs(beta)) ) 
           || ( alpha > (1.0-Abs(beta)) ) ) {
          return 0.0;
        }
        double piN = ProfileFunction(1.0,beta,alpha);
        double fq  = copysign(1.0,beta)*fPDFs.uBar(Abs(beta),Q2);
        //double fq  = fPDFs.u(beta,Q2) - fPDFs.uBar(beta,Q2);
        return( piN*fq/TMath::Abs(xi) );
      };
      return H_DDintegral_DGLAP_low(l, x, xi, t, Q2);
    }
    //______________________________________________________________________________

    double DD_GPDs::H_u_DGLAP_high(double x, double xi, double t, double Q2) const
    {
      using namespace TMath;
      const DD_GPDs& gpds = *this;
      auto l = [&](double* v, double *p){
        double beta  = v[0];
        double alpha = (x-beta)/xi;
        if( ( alpha < (-1.0+Abs(beta)) ) 
           || ( alpha > (1.0-Abs(beta)) ) ) {
          return 0.0;
        }
        double piN = ProfileFunction(1.0,beta,alpha);
        double fq  = copysign(1.0,beta)*fPDFs.uBar(Abs(beta),Q2);
        //double fq  = fPDFs.u(beta,Q2) - fPDFs.uBar(beta,Q2);
        return( piN*fq/TMath::Abs(xi) );
      };
      return H_DDintegral_DGLAP_high(l, x, xi, t, Q2);
    }
    //______________________________________________________________________________

    double DD_GPDs::H_u_ERBL(      double x, double xi, double t, double Q2) const
    {
      using namespace TMath;
      const DD_GPDs& gpds = *this;
      auto l = [&](double* v, double *p){
        double beta  = v[0];
        double alpha =  (x-beta)/xi;
        if( ( alpha < (-1.0+Abs(beta)) ) 
           || ( alpha > (1.0-Abs(beta)) ) ) {
          return 0.0;
        }
        double piN = ProfileFunction(1.0,beta,alpha);
        double fq  = copysign(1.0,beta)*fPDFs.uBar(Abs(beta),Q2);
        //double fq  = fPDFs.u(beta,Q2) - fPDFs.uBar(beta,Q2);
        return( piN*fq/TMath::Abs(xi) );
      };
      return H_DDintegral_ERBL(l, x, xi, t, Q2);
    }
    //______________________________________________________________________________

    //double DD_GPDs::H_i(int f, int i,double x, double xi, double t, double Q2) const
    //{
    //   using namespace TMath;
    //   double sign = 1.0;
    //   if( (x<0) && (i==1)) {
    //      //sea 
    //      x = Abs(x);
    //      sign = 1.0;
    //      if(f==2|| f==3) {
    //         sign = sign*kappa_s(Q2);
    //         f=1;
    //      }
    //   }
    //   double res = Exp( b(i,Q2)*t);
    //   double sum = 0.0;
    //   for(int j=0;j<=3;j++){
    //      sum += c(f,j,Q2)*Hij(i,j,f,x,xi,t,Q2);
    //   }
    //   return sign*res*sum;
    //}
    ////______________________________________________________________________________

    //double DD_GPDs::Htilde_i_integrand(int f, int i,double x, double xi, double t, double Q2,double bb) const
    //{
    //   using namespace TMath;
    //   // Create the function and wrap it
    //   const DD_GPDs& gpds = *this;
    //   auto l = [&](Double_t* v, Double_t *p){
    //      double beta  = v[0];
    //      double alpha =  (x-beta)/xi;
    //      if( ( alpha < (-1.0+Abs(beta)) ) ) {
    //         return 0.0;
    //      }
    //      if(( alpha > (1.0-Abs(beta)) ) ) {
    //         return 0.0;
    //      }
    //      return( gpds.f(f,i,beta,alpha,t,Q2)/TMath::Abs(xi) );
    //   };
    //   TF1 fDD("DD",l, -1.0, 1.0, 0);
    //   return fDD.Eval(bb);
    //}
    ////______________________________________________________________________________

    //double DD_GPDs::H_i_integrate_DD(int f, int i,double x, double xi, double t, double Q2) const
    //{
    //   using namespace TMath;
    //   double sign = 1.0;
    //   if( (i==2) && (x < -xi) && (-1.0 <= x)) {
    //      //val
    //      return 0.0;
    //   }
    //   if( (x<0) && (i==1)) {
    //      //sea 
    //      x = Abs(x);
    //      sign = -1.0;
    //      if(f==2|| f==3) {
    //         sign = sign*kappa_s(Q2);
    //         f=1;
    //      }
    //   } else if( (x<0) && (i==0) ) {
    //      //glue 
    //      x = Abs(x);
    //      sign = 1.0;
    //   }
    //   // Create the function and wrap it
    //   const DD_GPDs& gpds = *this;
    //   auto l = [&](Double_t* v, Double_t *p){
    //      double beta  = v[0];
    //      double alpha =  (x-beta)/xi;
    //      if( ( alpha < (-1.0+Abs(beta)) ) 
    //            || ( alpha > (1.0-Abs(beta)) ) ) {
    //         return 0.0;
    //      }
    //      return( gpds.f(f,i,beta,alpha,t,Q2)/TMath::Abs(xi) );
    //   };
    //   TF1 fDD("DD",l, -1.0, 1.0, 0);
    //   ROOT::Math::WrappedTF1 wf1(fDD);
    //   ROOT::Math::GSLIntegrator ig(ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR,1.0e-8, 1.0e-8, 1e7); 
    //   ig.SetFunction(wf1);
    //   double res = 0.0;
    //   double eps = TMath::Abs(xi)*((1.0+xi)/(1.0+x));
    //   double x0  = x;
    //   double temp  = 0;
    //   double min = x0-eps;
    //   double max = x0+eps;
    //   if(min > -1.0) {
    //      temp = ig.Integral(   -1.0, min);
    //      res += temp;
    //      std::cout << "0 : " << temp << ", \n";
    //   }
    //   if( min < -1.0) {
    //      min = -1.0;
    //   }
    //   if( max > 1.0) {
    //      max = 1.0;
    //   }
    //   temp = ig.Integral( min, max);
    //   res += temp;
    //   std::cout << "1 : " << temp << ", \n";

    //   if(max < 1.0) {
    //      temp = ig.Integral( max, 1.0);
    //      res += temp;
    //      std::cout << "2 : " << temp << std::endl;
    //   }
    //   //if( x < xi ) {
    //      temp = ig.IntegralCauchy( -1.0, 1.0, 0.0);
    //      std::cout << "PV : " << temp << std::endl;
    //      res = temp;
    //   //}
    //   return sign*res;
    //}

    //double DD_GPDs::Htilde_i(int f, int i,double x, double xi, double t, double Q2) const
    //{
    //   using namespace TMath;
    //   // Create the function and wrap it
    //   const DD_GPDs& gpds = *this;
    //   auto l = [&](Double_t* v, Double_t *p){
    //      double beta  = v[0];
    //      double alpha =  (x-beta)/xi;
    //      if( ( alpha < (-1.0+Abs(beta)) ) 
    //            || ( alpha > (1.0-Abs(beta)) ) ) {
    //         return 0.0;
    //      }
    //      return( gpds.f(f,i,beta,alpha,t,Q2)/xi );
    //   };
    //   TF1 fDD("DD",l, -1.0, 1.0, 0);
    //   ROOT::Math::WrappedTF1 wf1(fDD);
    //   //ROOT::Math::GaussIntegrator ig;
    //   ROOT::Math::GSLIntegrator ig(ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR,1.0e-12, 1.0e-12, 1e7); 
    //   ig.SetFunction(wf1);
    //   double res = 0.0;
    //   double eps = 0.01;//Abs(x);
    //   double temp  = 0;

    //   temp = ig.Integral(   -1.0, x-eps);
    //   //std::cout << "0 : " << temp << ", ";
    //   res += temp;//, Abs(x));

    //   temp = ig.Integral( x-eps, x+eps);//, Abs(x));
    //   //std::cout << "1 : " << temp << ", ";
    //   res += temp;//, Abs(x));

    //   temp = ig.Integral( x+eps,    1.0);//, Abs(x));
    //   //std::cout << "2 : " << temp << std::endl;
    //   res += temp;//, -Abs(x));

    //   return res;
    //}

  }
}
