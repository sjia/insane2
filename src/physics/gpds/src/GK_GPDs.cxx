#include "GK_GPDs.h"

namespace insane {
  namespace physics {

    double GK_GPDs::H_u(double x, double xi, double t, double Q2) const
    {
      return H_i(2,2,x,xi,t,Q2) + H_i(2,1,x,xi,t,Q2);
    }
    double GK_GPDs::H_d(double x, double xi, double t, double Q2) const
    { 
      return H_i(3,2,x,xi,t,Q2) + H_i(3,1,x,xi,t,Q2);
    }
    double GK_GPDs::H_ubar(double x, double xi, double t, double Q2) const
    {
      return H_i(2,1,x,xi,t,Q2);
    }
    double GK_GPDs::H_dbar(double x, double xi, double t, double Q2) const
    { 
      return H_i(3,1,x,xi,t,Q2);
    }

    double GK_GPDs::H_g(   double x, double xi, double t, double Q2) const 
    { return H_i(0,0,x,xi,t,Q2);}

    double GK_GPDs::Htilde_g(   double x, double xi, double t, double Q2) const 
    {
      return Htilde_i(0,0,x,xi,t,Q2);
    }

    double GK_GPDs::delta(int f, int i, double Q2) const
    {
      using namespace TMath;
      double L = Log(Q2/4.0);
      double del = fdelta[f][0]+fdelta[f][1]*L+fdelta[f][2]*L*L;
      if(i == 0) {
        // gluons
        if(f != 0 ) {
          std::cout << " error gluons : i=0 but f!=0" << std::endl;
        }
        f=0;
        return fdelta[f][0]+fdelta[f][1]*L+fdelta[f][2]*L*L;
      }
      if(i == 1) {
        // sea quarks
        // delta_sea = 1+delta_gluon
        if(f == 0 ) {
          std::cout << " error sea : i=1 but f=0" << std::endl;
        }
        f = 0;
        return 1.0 + fdelta[f][0]+fdelta[f][1]*L+fdelta[f][2]*L*L;
      }
      if(i == 2) {
        // valence quarks
        if(f == 0 ) {
          std::cout << " error valence : i=2 but f=0" << std::endl;
          f=2;
        }
        if(f == 1 ) {
          std::cout << " error valence : i=2 but f=1" << std::endl;
          f=2;
        }
        return fdelta[f][0]+fdelta[f][1]*L+fdelta[f][2]*L*L;
      }
    }
    //______________________________________________________________________________

    double GK_GPDs::b(int i, double Q2) const
    {
      using namespace TMath;
      if(i==2) {
        return 0.0;
      }
      return 2.58 + 0.25*Log(0.938*0.938/(0.938*0.938+Q2));
    }
    double GK_GPDs::n(int i) const
    {
      return fn.at(i);
    }

    double GK_GPDs::alpha_prime(int f) const
    {
      return falpha_prime.at(f);
    }
    //______________________________________________________________________________

    double GK_GPDs::Hij(int i, int j, int f, double x, double xi, double t, double Q2) const 
    {
      using namespace TMath;
      // i = g, sea, val
      // f = g, s, u, d
      // http://inspirehep.net/record/732489?ln=en
      double x1 = (x+xi)/(1.0+xi); 
      double x2 = (x-xi)/(1.0-xi); 
      double x3 = (x-xi)/(1.0+xi); 
      if(i!=2) {
        // sea and gluon
        double eps = 0.0; 
        double mij = 0;; 
        if( i==0 ) {
          //gluon
          eps = 1.0;
          mij = 3.0 + double(j)/2.0 - delta(f,i,Q2) - alpha_prime(f)*t; 
        } else {
          //sea
          eps = -1.0;
          mij = 3.0 + double(j)/2.0 - delta(f,i,Q2) - alpha_prime(f)*t - 1.0; 
        }
        double T0 = (15.0/(2.0*Power(xi,5.0)))*(Gamma(mij-2.0)/Gamma(mij+3.0));
        double T11 = 0;
        double T12 = 0;
        if(x>=xi) {
          T11 = ((mij*mij+2.0)*Power(xi*xi-x,2.0)-(mij*mij-1.0)*(1.0-xi*xi)*(x*x-xi*xi))*(Power(x1,mij)-Power(x2,mij));
          T12 = 3.0*mij*xi*(1.0-x)*(xi*xi-x)*(Power(x1,mij)+Power(x2,mij));
        } else if( (0 <= x) && (x < xi) ) {
          T11 = Power(x1,mij)*((mij*mij+2.0)*Power(xi*xi-x,2.0)+3.0*mij*xi*(xi*xi-x)*(1.0-x)-(mij*mij-1.0)*(1.0-xi*xi)*(x*x-xi*xi));
          x1 = (-x+xi)/(1.0+xi); 
          x2 = (-x-xi)/(1.0-xi); 
          x3 = (-x-xi)/(1.0+xi); 
          T12 = eps*Power(x1,mij)*((mij*mij+2.0)*Power(xi*xi+x,2.0)+3.0*mij*xi*(xi*xi+x)*(1.0+x)-(mij*mij-1.0)*(1.0-xi*xi)*(x*x-xi*xi));
        }
        return T0*(T11+T12);
      } else {
        // valence
        double mij = 2.0+double(j)/2.0 - delta(f,i,Q2)-alpha_prime(f)*t; 
        double T0 = (3.0/(2.0*xi*xi*xi))*(Gamma(mij-1.0)/Gamma(mij+2.0));
        double T1 = 0;
        if(x>=xi) {
          T1 = (xi*xi-x)*(Power(x1,mij)-Power(x2,mij))+mij*xi*(1.0-x)*(Power(x1,mij)+Power(x2,mij));
        } else if( (-xi <= x) && (x < xi) ) {
          T1 = Power(x1,mij)*(xi*xi-x+mij*xi*(1.0-x));
        } else if( x < -xi )  {
          T1 = 0.0;
        }

        return T0*T1;
      }
    }
    //______________________________________________________________________________

    double GK_GPDs::h(int f, int i, double beta, double Q2) const
    {
      using namespace TMath;
      // i = g, sea, val
      // f = g, s, u, d
      double sign = 1.0;
      if( (i==0) ){
        sign = Abs(beta);
        if( (beta < 0) ){
          sign *= 1.0;
        }
      }
      if( (i==1) ) {
        sign = 1.0;
        if( (beta < 0) ){
          sign = -1.0;
        }
      }
      if( (i==2) && (beta < 0) ){
        return 0.0;
      }
      beta = TMath::Abs(beta);
      double t1 = Power(beta, -1.0*delta(f,i,Q2));
      double t2 = Power(1.0-beta,2.0*n(i)+1.0);
      double t3 = 0;
      for(int j=0;j<=3;j++){
        t3 += c(f,j,Q2)*Power(beta,double(j)/2.0);
      }
      //std::cout << beta << std::endl;
      //std::cout << " t1 : " << t1 << std::endl;
      //std::cout << " t2 : " << t2 << std::endl;
      //std::cout << " t3 : " << t3 << std::endl;
      return sign*t1*t2*t3;
    }
    //______________________________________________________________________________

    double GK_GPDs::f(int f, int i, double beta, double alpha, double t, double Q2) const
    {
      using namespace TMath;
      using namespace std;

      double hfi = h(f,i,beta,Q2);
      double gam1   = Gamma(2.0*n(i)+2.0);
      double den1   = pow(2.0,2.0*n(i)+1.0)*pow(Gamma(n(i)+1.0),2.0);
      double num1   = 1.0/den1;
      double t1 = exp(b(i,Q2)*t)*pow(abs(beta),-alpha_prime(f)*t);
      double t2 = hfi*gam1*num1;

      double t30  = Power((1.0-Abs(beta))*(1.0-Abs(beta))-alpha*alpha,n(i));
      double den3 = (Power(1.0-Abs(beta),2.0*n(i)+1.0));
      double num3 = 1.0/den3;
      double t3   = t30*den3;
      //std::cout << " t1 : " << t1 << std::endl;
      //std::cout << " t2 : " << t2 << std::endl;
      //std::cout << " t3 : " << t3 << std::endl;
      return t1*t2*t3;
    }
    //______________________________________________________________________________

    double GK_GPDs::H_i_integrand(int f, int i,double x, double xi, double t, double Q2,double bb) const
    {
      using namespace TMath;
      // Create the function and wrap it
      const GK_GPDs& gpds = *this;
      auto l = [&](Double_t* v, Double_t *p){
        double beta  = v[0];
        double alpha =  (x-beta)/xi;
        if( ( alpha < (-1.0+Abs(beta)) ) 
           || ( alpha > (1.0-Abs(beta)) ) ) {
          return 0.0;
        }
        return( gpds.f(f,i,beta,alpha,t,Q2)/TMath::Abs(xi) );
      };
      TF1 fDD("DD",l, -1.0, 1.0, 0);
      return fDD.Eval(bb);
    }
    //______________________________________________________________________________

    double GK_GPDs::H_i(int f, int i,double x, double xi, double t, double Q2) const
    {
      using namespace TMath;
      double sign = 1.0;
      if( (x<0) && (i==1)) {
        //sea 
        x = Abs(x);
        sign = 1.0;
        if(f==2|| f==3) {
          sign = sign*kappa_s(Q2);
          f=1;
        }
      }
      double res = Exp( b(i,Q2)*t);
      double sum = 0.0;
      for(int j=0;j<=3;j++){
        sum += c(f,j,Q2)*Hij(i,j,f,x,xi,t,Q2);
      }
      double tot =  sign*res*sum;
      if(std::isnan(tot)) { return 0.0; }
      return tot;
    }
    //______________________________________________________________________________

    double GK_GPDs::Htilde_i_integrand(int f, int i,double x, double xi, double t, double Q2,double bb) const
    {
      using namespace TMath;
      // Create the function and wrap it
      const GK_GPDs& gpds = *this;
      auto l = [&](Double_t* v, Double_t *p){
        double beta  = v[0];
        double alpha =  (x-beta)/xi;
        if( ( alpha < (-1.0+Abs(beta)) ) ) {
          return 0.0;
        }
        if(( alpha > (1.0-Abs(beta)) ) ) {
          return 0.0;
        }
        return( gpds.f(f,i,beta,alpha,t,Q2)/TMath::Abs(xi) );
      };
      TF1 fDD("DD",l, -1.0, 1.0, 0);
      return fDD.Eval(bb);
    }
    //______________________________________________________________________________

    double GK_GPDs::H_i_integrate_DD(int f, int i,double x, double xi, double t, double Q2) const
    {
      using namespace TMath;
      double sign = 1.0;
      if( (i==2) && (x < -xi) && (-1.0 <= x)) {
        //val
        return 0.0;
      }
      if( (x<0) && (i==1)) {
        //sea 
        x = Abs(x);
        sign = -1.0;
        if(f==2|| f==3) {
          sign = sign*kappa_s(Q2);
          f=1;
        }
      } else if( (x<0) && (i==0) ) {
        //glue 
        x = Abs(x);
        sign = 1.0;
      }
      // Create the function and wrap it
      const GK_GPDs& gpds = *this;
      auto l = [&](Double_t* v, Double_t *p){
        double beta  = v[0];
        double alpha =  (x-beta)/xi;
        if( ( alpha < (-1.0+Abs(beta)) ) 
           || ( alpha > (1.0-Abs(beta)) ) ) {
          return 0.0;
        }
        return( gpds.f(f,i,beta,alpha,t,Q2)/TMath::Abs(xi) );
      };
      TF1 fDD("DD",l, -1.0, 1.0, 0);
      ROOT::Math::WrappedTF1 wf1(fDD);
      ROOT::Math::GSLIntegrator ig(ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR,1.0e-8, 1.0e-8, 1e7); 
      ig.SetFunction(wf1);
      double res = 0.0;
      double eps = TMath::Abs(xi)*((1.0+xi)/(1.0+x));
      double x0  = x;
      double temp  = 0;
      double min = x0-eps;
      double max = x0+eps;
      if(min > -1.0) {
        temp = ig.Integral(   -1.0, min);
        res += temp;
        std::cout << "0 : " << temp << ", \n";
      }
      if( min < -1.0) {
        min = -1.0;
      }
      if( max > 1.0) {
        max = 1.0;
      }
      temp = ig.Integral( min, max);
      res += temp;
      std::cout << "1 : " << temp << ", \n";

      if(max < 1.0) {
        temp = ig.Integral( max, 1.0);
        res += temp;
        std::cout << "2 : " << temp << std::endl;
      }
      //if( x < xi ) {
      temp = ig.IntegralCauchy( -1.0, 1.0, 0.0);
      std::cout << "PV : " << temp << std::endl;
      res = temp;
      //}
      return sign*res;
    }

    double GK_GPDs::Htilde_i(int f, int i,double x, double xi, double t, double Q2) const
    {
      using namespace TMath;
      // Create the function and wrap it
      const GK_GPDs& gpds = *this;
      auto l = [&](Double_t* v, Double_t *p){
        double beta  = v[0];
        double alpha =  (x-beta)/xi;
        if( ( alpha < (-1.0+Abs(beta)) ) 
           || ( alpha > (1.0-Abs(beta)) ) ) {
          return 0.0;
        }
        return( gpds.f(f,i,beta,alpha,t,Q2)/xi );
      };
      TF1 fDD("DD",l, -1.0, 1.0, 0);
      ROOT::Math::WrappedTF1 wf1(fDD);
      //ROOT::Math::GaussIntegrator ig;
      ROOT::Math::GSLIntegrator ig(ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR,1.0e-12, 1.0e-12, 1e7); 
      ig.SetFunction(wf1);
      double res = 0.0;
      double eps = 0.01;//Abs(x);
      double temp  = 0;

      temp = ig.Integral(   -1.0, x-eps);
      //std::cout << "0 : " << temp << ", ";
      res += temp;//, Abs(x));

      temp = ig.Integral( x-eps, x+eps);//, Abs(x));
      //std::cout << "1 : " << temp << ", ";
      res += temp;//, Abs(x));

      temp = ig.Integral( x+eps,    1.0);//, Abs(x));
      //std::cout << "2 : " << temp << std::endl;
      res += temp;//, -Abs(x));

      return res;
    }

  }
}

