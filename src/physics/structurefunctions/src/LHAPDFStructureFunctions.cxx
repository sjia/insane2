#include "LHAPDFStructureFunctions.h"
namespace insane {
namespace physics {
//______________________________________________________________________________
LHAPDFStructureFunctions::LHAPDFStructureFunctions(){
      fSubset = 0;
      const int SUBSET = 0;
      const std::string NAME = "MRST2004nlo";
      fLabel = NAME;
      LHAPDF::initPDFSet(NAME, LHAPDF::LHGRID, SUBSET);
      LHAPDF::initPDF(0);
}
//______________________________________________________________________________
LHAPDFStructureFunctions::~LHAPDFStructureFunctions(){

}
//______________________________________________________________________________
void LHAPDFStructureFunctions::SetPDFType(const char * pdfset, Int_t type) {
   if (!type) SetPDFDataSet(pdfset, LHAPDF::LHGRID, fSubset);
   else SetPDFDataSet(pdfset, LHAPDF::EVOLVE, fSubset);
}
//______________________________________________________________________________
void LHAPDFStructureFunctions::SetPDFDataSet(const char * pdfset, LHAPDF::SetType type , Int_t subset ) {
   fSubset = subset;
   //     int SUBSET = subset;
   fLabel = pdfset;
   //     const double  mz = 91.2;
   LHAPDF::initPDFSet(fLabel.Data(), type, subset);
   //   std::cout << "alphas(mz) = " << LHAPDF::alphasPDF(mz) << std::endl;
   //   std::cout << "qcdlam4    = " << LHAPDF::getLam4(SUBSET) << std::endl;
   //   std::cout << "qcdlam5    = " << LHAPDF::getLam5(SUBSET) << std::endl;
   //   std::cout << "orderPDF   = " << LHAPDF::getOrderPDF() << std::endl;
   //   std::cout << "xmin       = " << LHAPDF::getXmin(SUBSET) << std::endl;
   //   std::cout << "xmax       = " << LHAPDF::getXmax(SUBSET) << std::endl;
   //   std::cout << "q2min      = " << LHAPDF::getQ2min(SUBSET) << std::endl;
   //   std::cout << "q2max      = " << LHAPDF::getQ2max(SUBSET) << std::endl;
   //   std::cout << "orderalfas = " << LHAPDF::getOrderAlphaS() << std::endl;
   //   std::cout << "num flav   = " << LHAPDF::getNf(SUBSET) << std::endl;
   //   std::cout << "name       = " << fLabel.Data() << std::endl;
   //   std::cout << "number     = " << LHAPDF::numberPDF() << std::endl;
   //   std::cout << std::endl;

}
//______________________________________________________________________________
double LHAPDFStructureFunctions::F1p(double x, double Qsq) {
	/** F1p in the  parton model */
	std::vector<double> pdfvals =  LHAPDF::xfx(x, TMath::Sqrt(Qsq));
	double result = 0;
	result = (4.0 / 9.0) * (pdfvals[8] + pdfvals[4]) +
		(1.0 / 9.0) * (pdfvals[7] + pdfvals[5]) +
		(1.0 / 9.0) * (pdfvals[9] + pdfvals[3]);
	return(result / (2.0 * x));
}
//______________________________________________________________________________
double LHAPDFStructureFunctions::F2p(double x, double Qsq) {
	/** F2p in the  parton model */
	std::vector<double> pdfvals =  LHAPDF::xfx(x, TMath::Sqrt(Qsq));
	double result = 0;
	result = (4.0 / 9.0) * (pdfvals[8] + pdfvals[4]) +
		(1.0 / 9.0) * (pdfvals[7] + pdfvals[5]) +
		(1.0 / 9.0) * (pdfvals[9] + pdfvals[3]); //F2 =2xF1 = 2x sum e_q^2 (q(x) +qbar(x))/2
	return(result);
}
//______________________________________________________________________________
double LHAPDFStructureFunctions::F1n(double x, double Qsq)
{

   return(x * (1.0 / TMath::Power(x - 0.3, 2)));
}
//______________________________________________________________________________
double LHAPDFStructureFunctions::F2n(double x, double Qsq)
{

   return(x * (1.0 / TMath::Power(x - 0.3, 2)));
}
//______________________________________________________________________________
double LHAPDFStructureFunctions::F1d(double x, double Qsq)
{

   return(x * (1.0 / TMath::Power(x - 0.3, 2)));
}
//______________________________________________________________________________
double LHAPDFStructureFunctions::F2d(double x, double Qsq)
{

   return(x * (1.0 / TMath::Power(x - 0.3, 2)));
}
//______________________________________________________________________________
double LHAPDFStructureFunctions::F1He3(double x, double Qsq)
{
   return(0);
}
//______________________________________________________________________________
double LHAPDFStructureFunctions::F2He3(double x, double Qsq)
{

   return(0);
}
}}
