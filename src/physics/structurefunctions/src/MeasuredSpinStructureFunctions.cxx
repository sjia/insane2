#include "MeasuredSpinStructureFunctions.h"
#include "PhysicalConstants.h"
#include "TMath.h"
#include <cmath>
#include <iostream>

namespace insane {
namespace physics {

using namespace insane::units;
//______________________________________________________________________________
MeasuredSpinStructureFunctions::MeasuredSpinStructureFunctions()
{
   //SetNameTitle("Spin SFs from MAID07", "Spin SFs from MAID07");
   //fSFs   = fman->CreateSFs(9);
}
//______________________________________________________________________________
MeasuredSpinStructureFunctions::~MeasuredSpinStructureFunctions()
{
}
//______________________________________________________________________________

double MeasuredSpinStructureFunctions::g1()
{
   double M      = (M_p/GeV);
   double gamma2 = (4.0*M*M*fx*fx)/fQ2;
   double F1     = fF1;
   fg1           = (F1/(1.0+gamma2))*(fA1 + TMath::Sqrt(gamma2)*fA2);
   return fg1;
}
//______________________________________________________________________________

double MeasuredSpinStructureFunctions::g2()
{
   double M      = (M_p/GeV);
   double gamma2 = (4.0*M*M*fx*fx)/fQ2;
   double F1     = fF1;
   fg2           = (F1/(1.0+gamma2))*(fA2/TMath::Sqrt(gamma2) - fA1);
   return fg2;
}
//______________________________________________________________________________

double MeasuredSpinStructureFunctions::g1_unc()
{
   double M      = (M_p/GeV);
   double gamma2 = (4.0*M*M*fx*fx)/fQ2;
   double F1     = fF1;
   double a1     = (F1/(1.0+gamma2));
   double a2     = (F1/(1.0+gamma2))*TMath::Sqrt(gamma2);
   double eg1    = TMath::Sqrt( TMath::Power(a1*fA1_unc,2.0) + TMath::Power(a2*fA2_unc,2.0) );
   fg1_unc = eg1;
   return fg1_unc;
}
//______________________________________________________________________________
double MeasuredSpinStructureFunctions::g2_unc()
{
   double M      = (M_p/GeV);
   double gamma2 = (4.0*M*M*fx*fx)/fQ2;
   double F1     = fF1;
   double a1     = -1.0*(F1/(1.0+gamma2));
   double a2     = (F1/(1.0+gamma2))/TMath::Sqrt(gamma2);
   double eg2    = TMath::Sqrt( TMath::Power(a1*fA1_unc,2.0) + TMath::Power(a2*fA2_unc,2.0) );
   fg2_unc = eg2;
   return fg2_unc;
}
//______________________________________________________________________________
}}

