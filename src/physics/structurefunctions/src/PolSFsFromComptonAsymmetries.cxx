#include "PolSFsFromComptonAsymmetries.h"
#include "InSANEPhysics.h"

namespace insane {
  namespace physics {
    PolSFsFromComptonAsymmetries::PolSFsFromComptonAsymmetries()
    {
      fSFs = new DefaultStructureFunctions();
    }
    //__________________________________________________________________________
    //
    PolSFsFromComptonAsymmetries::~PolSFsFromComptonAsymmetries()
    {}
  }
}
