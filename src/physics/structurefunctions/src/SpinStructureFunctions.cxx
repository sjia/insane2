#include "SpinStructureFunctions.h"
#include "TMath.h"
#include "InSANEMathFunc.h"
#include "Math.h"


namespace insane {
  using namespace units;
  namespace physics {

    SpinStructureFunctions * SpinStructureFunctions::fgSpinStructureFunctions = nullptr;
    //______________________________________________________________________________

    SpinStructureFunctions::SpinStructureFunctions()
    { }
    //______________________________________________________________________________

    SpinStructureFunctions::~SpinStructureFunctions()
    { }
    //______________________________________________________________________________
 
    double SpinStructureFunctions::Get(double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t, OPELimit l) const
    {
      auto sftype = std::get<SF>(sf);
      auto target = std::get<Nuclei>(sf);

      switch(sftype) {

        case SF::g1 : 
          return g1(x,Q2,target);
          break;

        case SF::g2 : 
          return g2(x,Q2,target);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::g1(double x, double Q2, Nuclei target, Twist t) const
    {
      switch(target) {

        case Nuclei::p : 
          return g1p(x,Q2);
          break;

        case Nuclei::n : 
          return g1n(x,Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::g2(double x, double Q2, Nuclei target, Twist t) const
    {
      switch(target) {

        case Nuclei::p : 
          return g2p(x,Q2);
          break;

        case Nuclei::n : 
          return g2n(x,Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________
    
    double SpinStructureFunctions::g1_TMC(double x, double Q2, Nuclei target, Twist t) const
    {
      switch(target) {

        case Nuclei::p : 
          return g1p_TMC(x,Q2);
          break;

        case Nuclei::n : 
          return g1n_TMC(x,Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::g2_TMC(double x, double Q2, Nuclei target, Twist t) const
    {
      switch(target) {

        case Nuclei::p : 
          return g2p_TMC(x,Q2);
          break;

        case Nuclei::n : 
          return g2n_TMC(x,Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    //double SpinStructureFunctions::g1p_Twist2(double x, double Q2) const
    //{
    //  return Calculate(x, Q2, std::make_tuple(SF::g2, Nuclei::p), Twist::Two, OPELimit::Massless);
    //}
    ////______________________________________________________________________________

    //double SpinStructureFunctions::g2p_Twist2(double x, double Q2) const
    //{
    //  return Calculate(x, Q2, std::make_tuple(SF::g2, Nuclei::p), Twist::Two, OPELimit::Massless);
    //}
    ////______________________________________________________________________________

    //double SpinStructureFunctions::g1n_Twist2(double x, double Q2) const
    //{
    //  return Calculate(x, Q2, std::make_tuple(SF::g1, Nuclei::n), Twist::Two, OPELimit::Massless);
    //}
    ////______________________________________________________________________________

    //double SpinStructureFunctions::g2n_Twist2(double x, double Q2) const
    //{
    //  return Calculate(x, Q2, std::make_tuple(SF::g2, Nuclei::n), Twist::Two, OPELimit::Massless);
    //}
    ////______________________________________________________________________________

    double SpinStructureFunctions::g2_WW(double x, double Q2, Nuclei target) const
    {
      return 0.0; 
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::g1_BT(double x, double Q2, Nuclei target) const
    {
      return 0.0; 
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::Mellin_g1p(Int_t n, double Q2,double x1, double x2)
    {
      double integral_result =  insane::integrate::simple(
        [&](double xx) { return TMath::Power(xx, double(n)-1.0)*g1p(xx,Q2);},
        x1, x2, 200);
      return(integral_result);
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::Mellin_g2p(Int_t n, double Q2,double x1,double x2)
    {
      double integral_result =  insane::integrate::simple(
        [&](double xx) { return TMath::Power(xx, double(n)-1.0)*g2p(xx,Q2);},
        x1, x2, 200);
      return(integral_result);
    }

    double SpinStructureFunctions::Mellin_mn_p( int m, int n, double Q2, double x1, double x2)
    {
      switch(m) {
        case 1: return Mellin_g1p(n,Q2,x1,x2);
                break;
        case 2: return Mellin_g2p(n,Q2,x1,x2);
                break;
      }
      return 0.0;
    }

    double SpinStructureFunctions::Mellin_g1p_TMC(Int_t n, double Q2,double x1, double x2)
    {
      double integral_result =  insane::integrate::simple(
        [&](double xx) { return TMath::Power(xx, double(n)-1.0)*g1p_TMC(xx,Q2);},
        x1, x2, 200);
      return(integral_result);
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::Mellin_g2p_TMC(Int_t n, double Q2,double x1,double x2)
    {
      double integral_result =  insane::integrate::simple(
        [&](double xx) { return TMath::Power(xx, double(n)-1.0)*g2p_TMC(xx,Q2);},
        x1, x2, 200);
      return(integral_result);
    }

    double SpinStructureFunctions::Mellin_mn_TMC_p( int m, int n, double Q2, double x1, double x2)
    {
      switch(m) {
        case 1: return Mellin_g1p_TMC(n,Q2,x1,x2);
                break;
        case 2: return Mellin_g2p_TMC(n,Q2,x1,x2);
                break;
      }
      return 0.0;
    }

    //______________________________________________________________________________
    //double SpinStructureFunctions::d2p_WW(double Q2,double x1,double x2)
    //{
    //  // d2 tilde (no elastic contribution) 
    //  // Calculated using only the WW g2 and leading twist g1
    //  //double x1     = 0.1;
    //  //double x2     = 0.9;
    //  double result = 0.0;
    //  Int_t N         = 100;
    //  double dx     = (x2 - x1) / ((double)N);
    //  // quick simple integration
    //  for (int i = 0; i < N; i++) {
    //    double x = x1 + dx*double(i);
    //    result += (dx*(x*x)*( 2.0*g1p_Twist2(x,Q2) + 3.0*g2pWW(x,Q2) ));
    //  }
    //  return(result);
    //}
    ////_____________________________________________________________________________

    //double SpinStructureFunctions::d2p_Twist2_TMC(double Q2,double x1,double x2)
    //{
    //  // d2 tilde (no elastic contribution) 
    //  //double x1     = 0.1;
    //  //double x2     = 0.9;
    //  double result = 0.0;
    //  Int_t N         = 100;
    //  double dx     = (x2 - x1) / ((double)N);
    //  // quick simple integration
    //  for (int i = 0; i < N; i++) {
    //    double x = x1 + dx*double(i);
    //    result += (dx*(x*x)*( 2.0*g1p_Twist2_TMC(x,Q2) + 3.0*g2p_Twist2_TMC(x,Q2) ));
    //  }
    //  return(result);
    //}
    ////_____________________________________________________________________________

    //double SpinStructureFunctions::d2p_Twist3(double Q2,double x1,double x2)
    //{
    //  // d2 tilde (no elastic contribution) 
    //  // Calculated using only the WW g2 and leading twist g1
    //  //double x1     = 0.1;
    //  //double x2     = 0.9;
    //  double result = 0.0;
    //  Int_t N         = 100;
    //  double dx     = (x2 - x1) / ((double)N);
    //  // quick simple integration
    //  for (int i = 0; i < N; i++) {
    //    double x = x1 + dx*double(i);
    //    result += (dx*(x*x)*( 2.0*g1p_Twist3(x,Q2) + 3.0*g2p_Twist3(x,Q2) ));
    //  }
    //  return(result);
    //}
    ////_____________________________________________________________________________

    //double SpinStructureFunctions::d2p_Twist3_TMC(double Q2,double x1,double x2)
    //{
    //  // d2 tilde (no elastic contribution) 
    //  // Calculated using only the WW g2 and leading twist g1
    //  //double x1     = 0.1;
    //  //double x2     = 0.9;
    //  double result = 0.0;
    //  Int_t N         = 100;
    //  double dx     = (x2 - x1) / ((double)N);
    //  // quick simple integration
    //  for (int i = 0; i < N; i++) {
    //    double x = x1 + dx*double(i);
    //    result += (dx*(x*x)*( 2.0*g1p_Twist3_TMC(x,Q2) + 3.0*g2p_Twist3_TMC(x,Q2) ));
    //  }
    //  return(result);
    //}
    ////_____________________________________________________________________________

    double SpinStructureFunctions::d2p_tilde(double Q2,double x1,double x2)
    {
      // d2 tilde (no elastic contribution) 
      //double x1     = 0.1;
      //double x2     = 0.9;
      double integral_result =  insane::integrate::simple(
        [&](double xx) { return xx*xx*( 2.0*g1p(xx,Q2) + 3.0*g2p(xx,Q2));},
        x1,x2,200);
      return(integral_result);
    }
    //_____________________________________________________________________________

    double SpinStructureFunctions::d2n_tilde(double Q2,double x1,double x2)
    {
      // d2 tilde (no elastic contribution) 
      //double x1     = 0.1;
      //double x2     = 0.9;
      double integral_result =  insane::integrate::simple(
        [&](double xx) { return xx*xx*( 2.0*g1n(xx,Q2) + 3.0*g2n(xx,Q2));},
        x1,x2,200);
      return(integral_result);
    }
    //_____________________________________________________________________________

    double SpinStructureFunctions::d2p_tilde_TMC(double Q2,double x1,double x2)
    {
      // d2 tilde (no elastic contribution) 
      double integral_result =  insane::integrate::simple(
        [&](double xx) { return xx*xx*( 2.0*g1p_TMC(xx,Q2) + 3.0*g2p_TMC(xx,Q2));},
        x1,x2,200);
      return(integral_result);
    }
    //_____________________________________________________________________________
    
    double SpinStructureFunctions::d2n_tilde_TMC(double Q2,double x1,double x2)
    {
      // d2 tilde (no elastic contribution) 
      double integral_result =  insane::integrate::simple(
        [&](double xx) { return xx*xx*( 2.0*g1n_TMC(xx,Q2) + 3.0*g2n_TMC(xx,Q2));},
        x1,x2,200);
      return(integral_result);
    }
    //_____________________________________________________________________________

    double SpinStructureFunctions::M1n_p(Int_t n, double Q2, double x1, double x2)
    {
      // Nachtmann moment M_1^n. See Dong.PRC.77,015201.2008
      // proton
      if( (n%2 == 0) || n<1 ) {
        Error("M1n_p","First argument can be n = 3,5,7,...");
        return 0;
      }
      double integral_result =  insane::integrate::simple(
        [&](double xx) { return M1nIntegrand_p(n,xx,Q2);},
        x1,x2,200);
      return(integral_result);
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::M1nIntegrand_p(Int_t n, double x, double Q2)
    {
      // Nachtmann moment M_1^n. See Dong.PRC.77,015201.2008
      // proton Integrand
      double xi = insane::Kine::xi_Nachtmann(x,Q2);
      double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
      double c0 = TMath::Power(xi,double(n+1))/(x*x);
      double c1 = x/xi - TMath::Power(double(n*n)/double(n+2),2.0)*y2*x*xi;
      double c2 = -y2*x*x*double(4*n)/double(n+2);
      return( c0*(c1*g1p(x,Q2) + c2*g2p(x,Q2)) );
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::M2n_p(Int_t n, double Q2, double x1, double x2)
    {
      // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
      // proton
      if( (n%2 == 0) || n<3 ) { 
        Error("M2n_p","First argument can be n = 3,5,7,...");
        return 0;
      }
      double integral_result =  insane::integrate::simple(
        [&](double xx) { return M2nIntegrand_p(n,xx,Q2);},
        x1,x2,200);
      return(integral_result);
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::M2nIntegrand_p(Int_t n, double x, double Q2)
    {
      // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
      // proton Integrand
      double xi = insane::Kine::xi_Nachtmann(x,Q2);
      double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
      double c0 = TMath::Power(xi,double(n+1))/(x*x);
      double c1 = x/xi;
      double c2 = (x/xi)*(x/xi)*double(n)/double(n-1) - y2*x*x*double(n)/double(n+1);
      return( c0*(c1*g1p(x,Q2) + c2*g2p(x,Q2)) );
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::Mmn_p( int m, int n, double Q2, double x1, double x2 )
    {
      switch(m) {
        case 1: return M1n_p(n,Q2,x1,x2);
                break;
        case 2: return M2n_p(n,Q2,x1,x2);
                break;
      }
      return 0.0;
    }
    //______________________________________________________________________________
    
    double SpinStructureFunctions::M1n_n(Int_t n, double Q2, double x1, double x2)
    {
      // Nachtmann moment M_1^n. See Dong.PRC.77,015201.2008
      // neutron
      if( (n%2 == 0) || n<1 ) {
        Error("M1n_p","First argument can be n = 3,5,7,...");
        return 0;
      }
      double integral_result =  insane::integrate::simple(
        [&](double xx) { return M1nIntegrand_n(n,xx,Q2);},
        x1,x2,200);
      return(integral_result);
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::M1nIntegrand_n(Int_t n, double x, double Q2)
    {
      // Nachtmann moment M_1^n. See Dong.PRC.77,015201.2008
      // proton Integrand
      double xi = insane::Kine::xi_Nachtmann(x,Q2);
      double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
      double c0 = TMath::Power(xi,double(n+1))/(x*x);
      double c1 = x/xi - TMath::Power(double(n*n)/double(n+2),2.0)*y2*x*xi;
      double c2 = -y2*x*x*double(4*n)/double(n+2);
      return( c0*(c1*g1n(x,Q2) + c2*g2n(x,Q2)) );
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::M2n_n(Int_t n, double Q2, double x1, double x2)
    {
      // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
      // proton
      if( (n%2 == 0) || n<3 ) { 
        Error("M2n_n","First argument can be n = 3,5,7,...");
        return 0;
      }
      double integral_result =  insane::integrate::simple(
        [&](double xx) { return M2nIntegrand_n(n,xx,Q2);},
        x1,x2,200);
      return(integral_result);
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::M2nIntegrand_n(Int_t n, double x, double Q2)
    {
      // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
      // proton Integrand
      double xi = insane::Kine::xi_Nachtmann(x,Q2);
      double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
      double c0 = TMath::Power(xi,double(n+1))/(x*x);
      double c1 = x/xi;
      double c2 = (x/xi)*(x/xi)*double(n)/double(n-1) - y2*x*x*double(n)/double(n+1);
      return( c0*(c1*g1n(x,Q2) + c2*g2n(x,Q2)) );
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::Mmn_n( int m, int n, double Q2, double x1, double x2 )
    {
      switch(m) {
        case 1: return M1n_n(n,Q2,x1,x2);
                break;
        case 2: return M2n_n(n,Q2,x1,x2);
                break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::M1n_TMC_p(Int_t n, double Q2, double x1, double x2)
    {
      // Nachtmann moment M_1^n. See Dong.PRC.77,015201.2008
      // proton
      if( (n%2 == 0) || n<1 ) {
        Error("M1n_p","First argument can be n = 3,5,7,...");
        return 0;
      }
      double integral_result =  insane::integrate::simple(
        [&](double xx) { return M1nIntegrand_TMC_p(n,xx,Q2);},
        x1,x2,100);
      return(integral_result);
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::M1nIntegrand_TMC_p(Int_t n, double x, double Q2)
    {
      // Nachtmann moment M_1^n. See Dong.PRC.77,015201.2008
      // proton Integrand
      double xi = insane::Kine::xi_Nachtmann(x,Q2);
      double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
      double c0 = TMath::Power(xi,double(n+1))/(x*x);
      double c1 = x/xi - TMath::Power(double(n*n)/double(n+2),2.0)*y2*x*xi;
      double c2 = -y2*x*x*double(4*n)/double(n+2);
      return( c0*(c1*g1p_TMC(x,Q2) + c2*g2p_TMC(x,Q2)) );
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::M2n_TMC_p(Int_t n, double Q2, double x1, double x2)
    {
      // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
      // proton
      if( (n%2 == 0) || n<3 ) { 
        Error("M2n_p","First argument can be n = 3,5,7,...");
        return 0;
      }
      double integral_result =  insane::integrate::simple(
        [&](double xx) { return M2nIntegrand_TMC_p(n,xx,Q2);},
        x1,x2,100);
      return(integral_result);
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::M2nIntegrand_TMC_p(Int_t n, double x, double Q2)
    {
      // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
      // proton Integrand
      double xi = insane::Kine::xi_Nachtmann(x,Q2);
      double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
      double c0 = TMath::Power(xi,double(n)+1.0)/(x*x);
      double c1 = x/xi;
      double c2 = (x/xi)*(x/xi)*double(n)/(double(n)-1.0) - y2*x*x*double(n)/(double(n)+1.0);
      return( c0*(c1*g1p_TMC(x,Q2) + c2*g2p_TMC(x,Q2)) );
    }
    //______________________________________________________________________________
    
    double SpinStructureFunctions::Mmn_TMC_p( int m, int n, double Q2, double x1, double x2 )
    {
      switch(m) {
        case 1: return M1n_TMC_p(n,Q2,x1,x2);
                break;
        case 2: return M2n_TMC_p(n,Q2,x1,x2);
                break;
      }
      return 0.0;
    }
    //______________________________________________________________________________


    double SpinStructureFunctions::M1n_TMC_n(Int_t n, double Q2, double x1, double x2)
    {
      // Nachtmann moment M_1^n. See Dong.PRC.77,015201.2008
      // proton
      if( (n%2 == 0) || n<1 ) {
        Error("M1n_TMC_n","First argument can be n = 3,5,7,...");
        return 0;
      }
      double integral_result =  insane::integrate::simple(
        [&](double xx) { return M1nIntegrand_TMC_n(n,xx,Q2);},
        x1,x2,100);
      return(integral_result);
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::M1nIntegrand_TMC_n(Int_t n, double x, double Q2)
    {
      // Nachtmann moment M_1^n. See Dong.PRC.77,015201.2008
      // proton Integrand
      double xi = insane::Kine::xi_Nachtmann(x,Q2);
      double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
      double c0 = TMath::Power(xi,double(n+1))/(x*x);
      double c1 = x/xi - TMath::Power(double(n*n)/double(n+2),2.0)*y2*x*xi;
      double c2 = -y2*x*x*double(4*n)/double(n+2);
      return( c0*(c1*g1n_TMC(x,Q2) + c2*g2n_TMC(x,Q2)) );
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::M2n_TMC_n(Int_t n, double Q2, double x1, double x2)
    {
      // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
      // proton
      if( (n%2 == 0) || n<3 ) { 
        Error("M2n_TMC_n","First argument can be n = 3,5,7,...");
        return 0;
      }
      double integral_result =  insane::integrate::simple(
        [&](double xx) { return M2nIntegrand_TMC_n(n,xx,Q2);},
        x1,x2,100);
      return(integral_result);
    }
    //______________________________________________________________________________

    double SpinStructureFunctions::M2nIntegrand_TMC_n(Int_t n, double x, double Q2)
    {
      // Nachtmann moment M_2^n. See Dong.PRC.77,015201.2008
      // proton Integrand
      double xi = insane::Kine::xi_Nachtmann(x,Q2);
      double y2 = (M_p/GeV)*(M_p/GeV)/Q2;
      double c0 = TMath::Power(xi,double(n)+1.0)/(x*x);
      double c1 = x/xi;
      double c2 = (x/xi)*(x/xi)*double(n)/(double(n)-1.0) - y2*x*x*double(n)/(double(n)+1.0);
      return( c0*(c1*g1n_TMC(x,Q2) + c2*g2n_TMC(x,Q2)) );
    }
    //______________________________________________________________________________
    
    double SpinStructureFunctions::Mmn_TMC_n( int m, int n, double Q2, double x1, double x2 )
    {
      switch(m) {
        case 1: return M1n_TMC_n(n,Q2,x1,x2);
                break;
        case 2: return M2n_TMC_n(n,Q2,x1,x2);
                break;
      }
      return 0.0;
    }

    //double SpinStructureFunctions::Mmn_TMC_p( int m, int n, double Q2, double x1, double x2 )
    //{
    //  switch(m) {
    //    case 1: return M1n_TMC_p(n,Q2,x1,x2);
    //            break;
    //    case 2: return M2n_TMC_p(n,Q2,x1,x2);
    //            break;
    //  }
    //  return 0.0;
    //}
  }
}

