#include "CompositeSFs.h"

namespace insane {
  namespace physics {

    CompositeSFs::CompositeSFs()
    { }
    //______________________________________________________________________________

    CompositeSFs::~CompositeSFs()
    { }
    //______________________________________________________________________________

    double CompositeSFs::F2p(double x, double Qsq) const
    {
      double res = 0.0;

      double w = GetWeight(0,x,Qsq);
      if( w > 0.0 ) {
        res        +=  w*fAsf.F2p(x,Qsq);
      }
      w =  GetWeight(1,x,Qsq);
      if( w > 0.0 ) {
        res        +=  w*fBsf.F2p(x,Qsq);
      }
      return( res );
    }
    //______________________________________________________________________________
    
    double CompositeSFs::F1p(double x, double Qsq) const
    {
      double res = 0.0;

      double w = GetWeight(0,x,Qsq);
      if( w > 0.0 ) {
        res        +=  w*fAsf.F1p(x,Qsq);
      }
      w =  GetWeight(1,x,Qsq);
      if( w > 0.0 ) {
        res        +=  w*fBsf.F1p(x,Qsq);
      }
      return( res );
      //InSANEStructureFunctions * fAsf = (InSANEStructureFunctions *)fSFList.At( GetIndex(x,Qsq) );
      //return(fAsf.F1p(x,Qsq) );
    }
    //______________________________________________________________________________

    double CompositeSFs::F2n(double x, double Qsq) const
    {
      // Proton
      double res = GetWeight(0,x,Qsq)*fAsf.F2n(x,Qsq);
      res        +=  GetWeight(1,x,Qsq)*fBsf.F2n(x,Qsq);
      return( res );
      //InSANEStructureFunctions * fAsf = (InSANEStructureFunctions *)fSFList.At( GetIndex(x,Qsq) );
      //return(fAsf.F2n(x,Qsq) );
    }
    //______________________________________________________________________________
    
    double CompositeSFs::F1n(double x, double Qsq) const
    {
      double res  = GetWeight(0,x,Qsq)*fAsf.F1n(x,Qsq);
      res        +=  GetWeight(1,x,Qsq)*fBsf.F1n(x,Qsq);
      return( res );
      //InSANEStructureFunctions * fAsf = (InSANEStructureFunctions *)fSFList.At( GetIndex(x,Qsq) );
      //return(fAsf.F1n(x,Qsq) );
    }
    //______________________________________________________________________________

    double CompositeSFs::GetWeight(int iSF, double x, double Q2) const
    {
      // F1F209 is good for W<3 GeV and Q2<10 GeV^2
      // There are 3 overlap regions where the weights change for a smooth transfer
      // The width in W is dW and for Q2 dQ2.

      double W   = insane::Kine::W_xQsq(x,Q2);
      double dQ2 = 1.0;
      double dW  = 0.7;
      double W0  = 2.5;
      double Q20 = 10.0;
      //return 1.0;

      if( (W < W0 - dW) && (Q2 < Q20 - dQ2) ) {
        // F1F209 
        if( iSF == 0 ) { // DIS
          return 0.0;
        } else if( iSF == 1 ) { // F1F209
          return 1.0;
        }
      }

      if( (W >= W0 ) || (Q2 >= Q20 ) ) {
        // not F1F209 
        if( iSF == 0 ) { // DIS
          return 1.0;
        } else if( iSF == 1 ) { // F1F209
          return 0.0;
        }
      }

      // Over lap in W
      if( ( TMath::Abs(W - W0 + dW/2.0) <= dW/2.0 ) && (Q2 < Q20 - dQ2) ) {
        double W1 = (W - W0 + dW)/dW;
        if( iSF == 0 ) {
          return(W1);
        } else if( iSF == 1 ) {
          return(1.0 - W1);
        }

      }
      // Overlap in Q2 
      if( ( TMath::Abs(Q2 - Q20 + dQ2/2.0) <= dQ2/2.0 ) && (W < W0 - dW) ) {
        double W1 = (Q2 - Q20 + dQ2)/dQ2;
        if( iSF == 0 ) {
          return(W1);
        } else if( iSF == 1 ) {
          return(1.0 - W1);
        }

      }
      // Overlap in Q2 and W  
      if( ( TMath::Abs(W - W0 + dW/2.0) <= dW/2.0 ) && ( TMath::Abs(Q2 - Q20 + dQ2/2.0) <= dQ2/2.0 ) ) {
        double x1 = (Q2 - Q20 + dQ2)/dQ2;
        double y1 = (W - W0 + dW)/dW;
        double W1 = x1 + y1 - x1*y1;
        if( iSF == 0 ) {
          return(W1);
        } else if( iSF == 1 ) {
          return(1.0 - W1);
        }

      }

      std::cout << "W  " << W <<std::endl;
      std::cout << "Q2 " << Q2 <<std::endl;
      Error("GetWeight","Should not have made it here");
      return 0.0;
    }
    //______________________________________________________________________________

    double CompositeSFs::R(double x, double Q2, Nuclei target, Twist t) const
    {
      double res  = GetWeight(0,x,Q2)*fAsf.R(x,Q2,target,t);
      res        += GetWeight(1,x,Q2)*fBsf.R(x,Q2,target,t);
      return( res );
    }

  }
}

