#include "StructureFunctionsFromPDFs.h"
namespace insane {
namespace physics {

StructureFunctionsFromPDFs::StructureFunctionsFromPDFs() 
   : fUsesR(true), fTargetMassCorrections(true), fNintegrate(50)
{ }
//______________________________________________________________________________

StructureFunctionsFromPDFs::StructureFunctionsFromPDFs(
    PartonDistributionFunctions * pdfs) : 
  fUsesR(true), fTargetMassCorrections(true), fNintegrate(50), fUnpolarizedPDFs(pdfs)
{
   SetLabel(Form("%s",pdfs->GetLabel()));
   SetNameTitle(Form("SFs from %s",pdfs->GetName()),Form("%s",pdfs->GetName()));
}
//______________________________________________________________________________

StructureFunctionsFromPDFs::~StructureFunctionsFromPDFs(){
}
//______________________________________________________________________________
void StructureFunctionsFromPDFs::SetUnpolarizedPDFs(PartonDistributionFunctions * pdfs) {
   fUnpolarizedPDFs = pdfs;
   SetLabel(Form("%s",pdfs->GetLabel()));
   SetNameTitle(Form("SFs from %s",pdfs->GetName()),Form("%s",pdfs->GetName()));
}
//______________________________________________________________________________
PartonDistributionFunctions *  StructureFunctionsFromPDFs::GetUnpolarizedPDFs() { 
   return(fUnpolarizedPDFs);
}
//______________________________________________________________________________
double StructureFunctionsFromPDFs::h2_TMC(double xi, double Q2){
   // J. Phys. G 35, 053101 (2008)
   Int_t N         = fNintegrate;
   double du     = (1.0 - xi) / ((double)N);
   double result = 0.0;
   for (int i = 0; i < N; i++) {
      double u =  xi + du*double(i);
      result += (F2pNoTMC(u,Q2)*du/(u*u));
   }
   return(result);
}
//______________________________________________________________________________
double StructureFunctionsFromPDFs::g2_TMC(double xi, double Q2){
   // J. Phys. G 35, 053101 (2008)
   Int_t N         = fNintegrate;
   double du     = (1.0 - xi) / ((double)N);
   double result = 0.0;
   for (int i = 0; i < N; i++) {
      double u =  xi + du*double(i);
      result += ((u-xi)*F2pNoTMC(u,Q2)*du/(u*u));
   }
   return(result);
}
//______________________________________________________________________________
double StructureFunctionsFromPDFs::R(double x, double Qsq)
{
   if(fUsesR){
      return insane::Kine::R1998(x,Qsq);
   }
   //if(fTargetMassCorrections) {
   //   return( F2p(x,Qsq)/(2.0*x*F1p(x,Qsq))*(1.0+(4.0*(M_p/GeV)*(M_p/GeV)*x*x)/(Qsq))-1.0 );
   //}
   /*else*/return 0.0;
}
//______________________________________________________________________________
double StructureFunctionsFromPDFs::F2p(double x, double Qsq) {
   // including target mass effects
   // references: 
   //    Phys Rev D 84, 074008 (2011), Eq 9a  
   //    J. Phys. G 35, 053101 (2008)
   double result = 0.0;
   if( fTargetMassCorrections ) {
      // leading order in 1/Q2 
      double M         = M_p/GeV; 
      double rho       = TMath::Sqrt(1. + 4.*x*x*M*M/Qsq); 
      double xi        = 2.*x/(1. + rho); 
      double F2_0      = F2pNoTMC(xi,Qsq); 
      double h2        = h2_TMC(x,Qsq);
      double g2        = g2_TMC(x,Qsq);
      result    = (F2_0*x*x)/(xi*xi*rho*rho*rho) + 6.0*M*M*x*x*x*h2/(Qsq*rho*rho*rho*rho) + 12.0*M*M*M*M*x*x*x*x*g2/(Qsq*Qsq*rho*rho*rho*rho*rho);
      return(result);

      // Approximate form   Phys Rev D 84, 074008 (2011), Eq 9a  
      //double T1        = TMath::Power(1.+rho,2.)/( 4.*TMath::Power(rho,3.) ); 
      //double T2        = 1. + 3.*(rho*rho - 1.)*TMath::Power(1.-xi,2.)/( rho*(1.+rho) );
      //result             = T1*T2*f2_no_tmc; 
      //return(result);
   }
   if(!fTargetMassCorrections) {
      result = F2pNoTMC(x,Qsq); 
   }
   if(fUsesR) {
      //result *= (1.0+4.0*TMath::Power(x*M_p/GeV,2.0)/Qsq)/(R(x,Qsq)+1.0);
   }
   return result;
}
//______________________________________________________________________________
double StructureFunctionsFromPDFs::F1p( double x, double Qsq ){
   double result = 0.0;
   if( fTargetMassCorrections ) {
      // leading order in 1/Q2 
      double M         = M_p/GeV; 
      double rho       = TMath::Sqrt(1. + 4.*x*x*M*M/Qsq); 
      double xi        = 2.*x/(1. + rho); 
      double F1_0      = F1pNoTMC(xi,Qsq); 
      double h2        = h2_TMC(x,Qsq);
      double g2        = g2_TMC(x,Qsq);
      result    = (F1_0*x)/(xi*rho) + M*M*x*x*h2/(Qsq*rho*rho) + 2.0*M*M*M*M*x*x*x*g2/(Qsq*Qsq*rho*rho*rho);
      return(result);
   }
   result = F1pNoTMC(x, Qsq);
   if(fUsesR) {
      result *= (1.0+4.0*TMath::Power(x*M_p/GeV,2.0)/Qsq)/(R(x,Qsq)+1.0);
      return result;
   }
   return(result);
}
//______________________________________________________________________________
double StructureFunctionsFromPDFs::F2pNoTMC(double x, double Qsq ) {
   double result = 0.0;
   fUnpolarizedPDFs->GetPDFs(x, Qsq);
   result += (4.0 / 9.0) * fUnpolarizedPDFs->u();
   result += (1.0 / 9.0) * fUnpolarizedPDFs->d();
   result += (1.0 / 9.0) * fUnpolarizedPDFs->s();
   result += (4.0 / 9.0) * fUnpolarizedPDFs->ubar();
   result += (1.0 / 9.0) * fUnpolarizedPDFs->dbar();
   result += (1.0 / 9.0) * fUnpolarizedPDFs->sbar();
   return(result*x);
}
//______________________________________________________________________________
double StructureFunctionsFromPDFs::F1pNoTMC( double x, double Qsq ){
   double result = F2pNoTMC(x, Qsq) / (2.0*x);
   return(result);
}
//______________________________________________________________________________
double StructureFunctionsFromPDFs::F2n(double x, double Qsq) {
   // including target mass effects
   // reference: Phys Rev D 84, 074008 (2011), Eq 9a  
   if(!fTargetMassCorrections) {
      return F2nNoTMC(x,Qsq);
   }
   double M         = M_p/GeV;             // nucleon mass => proton mass 
   double rho       = TMath::Sqrt(1. + 4.*x*x*M*M/Qsq); 
   double xi        = 2.*x/(1. + rho); 
   double f2_no_tmc = F2nNoTMC(xi,Qsq); 
   double T1        = TMath::Power(1.+rho,2.)/( 4.*TMath::Power(rho,3.) ); 
   double T2        = 1. + 3.*(rho*rho - 1.)*TMath::Power(1.-xi,2.)/( rho*(1.+rho) );
   double result    = T1*T2*f2_no_tmc; 
   return result;
}
//______________________________________________________________________________
double StructureFunctionsFromPDFs::F2nNoTMC(double x, double Qsq) {
   // for the neutron we switch the charges on the up and down quarks in the sum.
   double result = 0.0;
   fUnpolarizedPDFs->GetPDFs(x, Qsq);
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->u();
   result +=  (4.0 / 9.0) * fUnpolarizedPDFs->d();
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->s();
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->ubar();
   result +=  (4.0 / 9.0) * fUnpolarizedPDFs->dbar();
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->sbar();
   return(result*x);
}
//______________________________________________________________________________
double StructureFunctionsFromPDFs::F1n(double x,double Qsq){
   double result = F2n(x, Qsq) / (2.0*x);
   if(!fTargetMassCorrections) {
      return result;
   }
   result *= (1.0+4.0*TMath::Power(x*M_p/GeV,2.0)/Qsq)/(R(x,Qsq)+1.0);
   return(result);
}
//______________________________________________________________________________
double StructureFunctionsFromPDFs::F1nNoTMC(double x,double Qsq){
   double result = F2nNoTMC(x, Qsq) / (2.0*x);
   result *= (1.0+4.0*TMath::Power(x*M_p/GeV,2.0)/Qsq)/(R(x,Qsq)+1.0);
   return(result);
}
//______________________________________________________________________________
double StructureFunctionsFromPDFs::F1d(double x,double Qsq){
   // F1d PER NUCLEON
   double wD     = 0.058;         // D-wave state probability 
   double result = 0.5*(1.-1.5*wD)*( F1n(x,Qsq) + F1p(x,Qsq) ); 
   return(result);
}
//______________________________________________________________________________
double StructureFunctionsFromPDFs::F2d(double x,double Qsq){
   // F2d PER NUCLEON 
   double wD     = 0.058;         // D-wave state probability 
   double result = 0.5*(1.-1.5*wD)*( F2n(x,Qsq) + F2p(x,Qsq) ); 
   return(result);
}
//______________________________________________________________________________
double StructureFunctionsFromPDFs::F1He3(double x,double Qsq){
   // F1He3 PER NUCLEUS
   double result = ( F1n(x,Qsq) + 2.*F1p(x,Qsq) ); 
   return(result);
} 
//______________________________________________________________________________
double StructureFunctionsFromPDFs::F2He3(double x,double Qsq){
   // F2He3 PER NUCLEUS
   double result = ( F2n(x,Qsq) + 2.*F2p(x,Qsq) ); 
   return(result);
}
//______________________________________________________________________________
double StructureFunctionsFromPDFs::F1p_Error(   double x, double Q2){
   return 0.0;
}
double StructureFunctionsFromPDFs::F2p_Error(   double x, double Q2){
   // including target mass effects
   // reference: Phys Rev D 84, 074008 (2011), Eq 9a  
   double M         = M_p/GeV; 
   double rho       = TMath::Sqrt(1. + 4.*x*x*M*M/Q2); 
   double xi        = 2.*x/(1. + rho); 
   double f2_no_tmc = F2pNoTMC_Error(xi,Q2); 
   double T1        = TMath::Power(1.+rho,2.)/( 4.*TMath::Power(rho,3.) ); 
   double T2        = 1. + 3.*(rho*rho - 1.)*TMath::Power(1.-xi,2.)/( rho*(1.+rho) );
   double result    = T1*T2*f2_no_tmc; 
   return result;
}
double StructureFunctionsFromPDFs::F1n_Error(   double x, double Q2){return 0.0;}
double StructureFunctionsFromPDFs::F2n_Error(   double x, double Q2){
   // including target mass effects
   // reference: Phys Rev D 84, 074008 (2011), Eq 9a  
   double M         = M_p/GeV;             // nucleon mass => proton mass 
   double rho       = TMath::Sqrt(1. + 4.*x*x*M*M/Q2); 
   double xi        = 2.*x/(1. + rho); 
   double f2_no_tmc = F2nNoTMC_Error(xi,Q2); 
   double T1        = TMath::Power(1.+rho,2.)/( 4.*TMath::Power(rho,3.) ); 
   double T2        = 1. + 3.*(rho*rho - 1.)*TMath::Power(1.-xi,2.)/( rho*(1.+rho) );
   double result    = T1*T2*f2_no_tmc; 
   return result;
}
double StructureFunctionsFromPDFs::F1d_Error(   double x, double Q2){return 0.0;}
double StructureFunctionsFromPDFs::F2d_Error(   double x, double Q2){return 0.0;}
double StructureFunctionsFromPDFs::F1He3_Error( double x, double Q2){return 0.0;}
double StructureFunctionsFromPDFs::F2He3_Error( double x, double Q2){return 0.0;}
//______________________________________________________________________________
double StructureFunctionsFromPDFs::F1pNoTMC_Error(   double x, double Q2){
   double result = 0.0;
   fUnpolarizedPDFs->GetPDFErrors(x, Q2);
   result += TMath::Power((4.0 / 9.0) * fUnpolarizedPDFs->uError(),2.0);
   result += TMath::Power((1.0 / 9.0) * fUnpolarizedPDFs->dError(),2.0);
   result += TMath::Power((1.0 / 9.0) * fUnpolarizedPDFs->sError(),2.0);
   result += TMath::Power((4.0 / 9.0) * fUnpolarizedPDFs->ubarError(),2.0);
   result += TMath::Power((1.0 / 9.0) * fUnpolarizedPDFs->dbarError(),2.0);
   result += TMath::Power((1.0 / 9.0) * fUnpolarizedPDFs->sbarError(),2.0);
   return( TMath::Sqrt(result)/2.0 );
}
double StructureFunctionsFromPDFs::F2pNoTMC_Error(   double x, double Q2){
   double result = 0.0;
   fUnpolarizedPDFs->GetPDFErrors(x, Q2);
   result += TMath::Power((4.0 / 9.0) * fUnpolarizedPDFs->uError(),2.0);
   result += TMath::Power((1.0 / 9.0) * fUnpolarizedPDFs->dError(),2.0);
   result += TMath::Power((1.0 / 9.0) * fUnpolarizedPDFs->sError(),2.0);
   result += TMath::Power((4.0 / 9.0) * fUnpolarizedPDFs->ubarError(),2.0);
   result += TMath::Power((1.0 / 9.0) * fUnpolarizedPDFs->dbarError(),2.0);
   result += TMath::Power((1.0 / 9.0) * fUnpolarizedPDFs->sbarError(),2.0);
   return( TMath::Sqrt(result)*x );
}
double StructureFunctionsFromPDFs::F1nNoTMC_Error(   double x, double Q2){
   // for the neutron we switch the charges on the up and down quarks in the sum.
   double result = 0.0;
   fUnpolarizedPDFs->GetPDFErrors(x, Q2);
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->uError();
   result +=  (4.0 / 9.0) * fUnpolarizedPDFs->dError();
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->sError();
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->ubarError();
   result +=  (4.0 / 9.0) * fUnpolarizedPDFs->dbarError();
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->sbarError();
   return(result/2.0);
}
double StructureFunctionsFromPDFs::F2nNoTMC_Error(   double x, double Q2){
   // for the neutron we switch the charges on the up and down quarks in the sum.
   double result = 0.0;
   fUnpolarizedPDFs->GetPDFErrors(x, Q2);
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->uError();
   result +=  (4.0 / 9.0) * fUnpolarizedPDFs->dError();
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->sError();
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->ubarError();
   result +=  (4.0 / 9.0) * fUnpolarizedPDFs->dbarError();
   result +=  (1.0 / 9.0) * fUnpolarizedPDFs->sbarError();
   return(result*x);
}
double StructureFunctionsFromPDFs::F1dNoTMC_Error(   double x, double Q2){return 0.0;}
double StructureFunctionsFromPDFs::F2dNoTMC_Error(   double x, double Q2){return 0.0;}
double StructureFunctionsFromPDFs::F1He3NoTMC_Error( double x, double Q2){return 0.0;}
double StructureFunctionsFromPDFs::F2He3NoTMC_Error( double x, double Q2){return 0.0;}
//______________________________________________________________________________
double StructureFunctionsFromPDFs::F2Nuclear(double x,double Qsq,double Z, double A){
   double res = (A/2.0)*2.0*F2d(x,Qsq)*EMC_Effect(&x,&A);
   return res; 
}
//______________________________________________________________________________
double StructureFunctionsFromPDFs::F1Nuclear(double x,double Qsq,double Z, double A){
   double res = (A/2.0)*2.0*F1d(x,Qsq)*EMC_Effect(&x,&A);
   return res; 
}
//______________________________________________________________________________
}}
