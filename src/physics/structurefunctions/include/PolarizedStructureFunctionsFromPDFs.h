#ifndef POLARIZEDSTRUCTUREFUNCTIONSFROMPDFS_h
#define POLARIZEDSTRUCTUREFUNCTIONSFROMPDFS_h 1

#include <cstdlib> 
#include <iostream>
#include <iomanip>
#include "PartonDistributionFunctions.h"
#include "StructureFunctions.h"
#include "PolarizedStructureFunctions.h"
#include "PolarizedPartonDistributionFunctions.h"

namespace insane {
namespace physics {
/** Base class for Polarized Structure Functions using Polarized Parton Distributions.
 *
 *  Only \f$ g_1 \f$ and \f$ g2_{WW} \f$ can be calculated from polarized parton distributions.
 *  
 *  \f$ g_1(x) = (1/2)\sum_q \Delta q(x) \f$
 *
 *  Note that because the the nuclear corrections, target mass and higher twist corrections
 *  effect the extraction of and evolve with the pdfs they are expected to be defined from the
 *  PDF class. In the future they should be decomposed into some type of quark distributions.
 * 
 * \ingroup partondistributions
 * \ingroup structurefunctions
 */
class PolarizedStructureFunctionsFromPDFs: public PolarizedStructureFunctions {

   protected:
      PolarizedPartonDistributionFunctions *fPolarizedPDFs;

   public: 
      PolarizedStructureFunctionsFromPDFs();  
      PolarizedStructureFunctionsFromPDFs(PolarizedPartonDistributionFunctions * ppdfs);  
      virtual ~PolarizedStructureFunctionsFromPDFs(); 

      void SetPolarizedPDFs(PolarizedPartonDistributionFunctions * ppdfs) ;
      PolarizedPartonDistributionFunctions * GetPolarizedPDFs() ;

      /**  g_1^{p}
       *  \f$ \lim_{Bjorken} M^2 \nu G_1(p.q,Q^2) = g_1(x)   \f$
       *  \f$ \nu = \frac{Q^2}{2Mx} \f$
       *
       *  \f$ g_1^{n} \f$ using isospin invariance of pdfs p->n (u->d,d->u) 
       *  \f$ g_1^{d}       \f$  
       *  \f$ g_1^{^{3}He}  \f$ 
       *
       * \f$ \lim_{Bjorken} M \nu^2 G_2(p.q,Q^2) = g_2(x)   \f$
       *
       *
       *  \f$ g_2^{WW} = \int_x^1 g_1(y)/y dy - g_1(x)\f$
       * 
       */        

      // -----------------------------
      // g1 Spin Structure Function
      virtual double g1p(   double x, double Q2);
      virtual double g1n(   double x, double Q2);
      virtual double g1d(   double x, double Q2);
      virtual double g1He3( double x, double Q2);

      virtual double g1p_Error(   double x, double Q2){ return g1pError(x,Q2);}
      virtual double g1n_Error(   double x, double Q2){ return g1nError(x,Q2);}
      virtual double g1d_Error(   double x, double Q2){ return 0.0;}
      virtual double g1He3_Error( double x, double Q2){ return g1He3Error(x,Q2);}
      // Deprecated:
      virtual double g1pError(   double x, double Q2);
      virtual double g1nError(   double x, double Q2);
      virtual double g1He3Error( double x, double Q2);

      // -----------------------------
      // g2 Spin Structure Function
      virtual double g2p(   double x, double Q2);
      virtual double g2n(   double x, double Q2);
      virtual double g2d(   double x, double Q2);
      virtual double g2He3( double x, double Q2);

      /// \todo Need to handle g2 errors 
      virtual double g2p_Error(   double x, double Q2){ return 0.0;}
      virtual double g2n_Error(   double x, double Q2){ return 0.0;}
      virtual double g2d_Error(   double x, double Q2){ return 0.0;}
      virtual double g2He3_Error( double x, double Q2){ return 0.0;}

      // -----------------------------
      //virtual double g2pWW_Error(   double x, double Q2){ return 0.0;}
      //virtual double g2nWW_Error(   double x, double Q2){ return 0.0;}
      //virtual double g2dWW_Error(   double x, double Q2){ return 0.0;}
      //virtual double g2He3WW_Error( double x, double Q2){ return 0.0;}

      // ------------------------------------
      // Twist expansion with target mass M=0
      // ------------------------------------
      // g1 twist-2  M=0 (no TMC)
      virtual double g1p_Twist2(   double x, double Q2){ return fPolarizedPDFs->g1p_Twist2(x , Q2); }
      virtual double g1n_Twist2(   double x, double Q2){ return fPolarizedPDFs->g1n_Twist2(x , Q2); }
      virtual double g1d_Twist2(   double x, double Q2){ return fPolarizedPDFs->g1d_Twist2(x , Q2); }
      virtual double g1He3_Twist2( double x, double Q2){ return fPolarizedPDFs->g1He3_Twist2(x   , Q2); }

      // g2 twist-2  M=0 (no TMC), i.e. g2_WW
      virtual double g2p_Twist2(   double x, double Q2){ return fPolarizedPDFs->g2p_Twist2(x , Q2); }
      virtual double g2n_Twist2(   double x, double Q2){ return fPolarizedPDFs->g2n_Twist2(x , Q2); }
      virtual double g2d_Twist2(   double x, double Q2){ return fPolarizedPDFs->g2d_Twist2(x , Q2); }
      virtual double g2He3_Twist2( double x, double Q2){ return fPolarizedPDFs->g2He3_Twist2(x   , Q2); }

      // -----------------------------
      // g2 twist-3  M=0 (no TMC)
      virtual double g1p_Twist3(double   x, double Q2) { return 0.0;}//fPolarizedPDFs->g1p_Twist3(x , Q2); }
      virtual double g1n_Twist3(double   x, double Q2) { return 0.0;}//fPolarizedPDFs->g1n_Twist3(x , Q2); }
      virtual double g1d_Twist3(double   x, double Q2) { return 0.0;}//fPolarizedPDFs->g1d_Twist3(x , Q2); }
      virtual double g1He3_Twist3(double x, double Q2) { return 0.0;}//fPolarizedPDFs->g1He3_Twist3(x   , Q2); }

      // g2 twist-3  M=0 (no TMC)
      virtual double g2p_Twist3(double   x, double Q2) { return fPolarizedPDFs->g2p_Twist3(x , Q2); }
      virtual double g2n_Twist3(double   x, double Q2) { return fPolarizedPDFs->g2n_Twist3(x , Q2); }
      virtual double g2d_Twist3(double   x, double Q2) { return fPolarizedPDFs->g2d_Twist3(x , Q2); }
      virtual double g2He3_Twist3(double x, double Q2) { return fPolarizedPDFs->g2He3_Twist3(x   , Q2); }

      // -----------------------------
      // g1 twist 4
      virtual double g1p_Twist4(double   x, double Q2) { return fPolarizedPDFs->g1p_Twist4(x , Q2); }
      virtual double g1n_Twist4(double   x, double Q2) { return fPolarizedPDFs->g1n_Twist4(x , Q2); }
      virtual double g1d_Twist4(double   x, double Q2) { return fPolarizedPDFs->g1d_Twist4(x , Q2); }
      virtual double g1He3_Twist4(double x, double Q2) { return fPolarizedPDFs->g1He3_Twist4(x   , Q2); }

      // g2 twist 4
      virtual double g2p_Twist4(double   x, double Q2) { return fPolarizedPDFs->g2p_Twist4(x , Q2); }
      virtual double g2n_Twist4(double   x, double Q2) { return fPolarizedPDFs->g2n_Twist4(x , Q2); }
      virtual double g2d_Twist4(double   x, double Q2) { return fPolarizedPDFs->g2d_Twist4(x , Q2); }
      virtual double g2He3_Twist4(double x, double Q2) { return fPolarizedPDFs->g2He3_Twist4(x   , Q2); }

      // ---------------------------------
      // SSFs with Target Mass Corrections
      // ---------------------------------
      //
      // g1 Spin Structure Function with TMC and all Twists <=4
      virtual double g1p_TMC(   double x, double Q2);
      virtual double g1n_TMC(   double x, double Q2);
      virtual double g1d_TMC(   double x, double Q2);
      virtual double g1He3_TMC( double x, double Q2);

      // g1 Spin Structure Function with TMC and all Twists <=4
      virtual double g2p_TMC(   double x, double Q2);
      virtual double g2n_TMC(   double x, double Q2);
      virtual double g2d_TMC(   double x, double Q2);
      virtual double g2He3_TMC( double x, double Q2);

      // g1_twist2 plus the twist-2 TMC
      virtual double g1p_Twist2_TMC  ( double x, double Q2){ return fPolarizedPDFs->g1p_Twist2_TMC(  x, Q2); }
      virtual double g1n_Twist2_TMC  ( double x, double Q2){ return fPolarizedPDFs->g1n_Twist2_TMC(  x, Q2); }
      virtual double g1d_Twist2_TMC  ( double x, double Q2){ return fPolarizedPDFs->g1d_Twist2_TMC(  x, Q2); }
      virtual double g1He3_Twist2_TMC( double x, double Q2){ return fPolarizedPDFs->g1He3_Twist2_TMC(x, Q2); }

      // g1_twist3 plus the twist-3 TMC
      virtual double g1p_Twist3_TMC  ( double x, double Q2){ return fPolarizedPDFs->g1p_Twist3_TMC(  x, Q2); }
      virtual double g1n_Twist3_TMC  ( double x, double Q2){ return fPolarizedPDFs->g1n_Twist3_TMC(  x, Q2); }
      virtual double g1d_Twist3_TMC  ( double x, double Q2){ return fPolarizedPDFs->g1d_Twist3_TMC(  x, Q2); }
      virtual double g1He3_Twist3_TMC( double x, double Q2){ return fPolarizedPDFs->g1He3_Twist3_TMC(x, Q2); }

      // -----------------------------
      // g2_twist2 plus the twist-2 TMC
      virtual double g2p_Twist2_TMC  ( double x, double Q2){ return fPolarizedPDFs->g2p_Twist2_TMC(  x, Q2); }
      virtual double g2n_Twist2_TMC  ( double x, double Q2){ return fPolarizedPDFs->g2n_Twist2_TMC(  x, Q2); }
      virtual double g2d_Twist2_TMC  ( double x, double Q2){ return fPolarizedPDFs->g2d_Twist2_TMC(  x, Q2); }
      virtual double g2He3_Twist2_TMC( double x, double Q2){ return fPolarizedPDFs->g2He3_Twist2_TMC(x, Q2); }

      // g2_twist3 plus the twist-3 TMC
      virtual double g2p_Twist3_TMC  ( double x, double Q2){ return fPolarizedPDFs->g2p_Twist3_TMC(  x, Q2); }
      virtual double g2n_Twist3_TMC  ( double x, double Q2){ return fPolarizedPDFs->g2n_Twist3_TMC(  x, Q2); }
      virtual double g2d_Twist3_TMC  ( double x, double Q2){ return fPolarizedPDFs->g2d_Twist3_TMC(  x, Q2); }
      virtual double g2He3_Twist3_TMC( double x, double Q2){ return fPolarizedPDFs->g2He3_Twist3_TMC(x, Q2); }

      // -----------------------------
      // g1 twist 4
      virtual double g1p_Twist4_TMC(double   x, double Q2) { return fPolarizedPDFs->g1p_Twist4_TMC(x , Q2); }
      virtual double g1n_Twist4_TMC(double   x, double Q2) { return fPolarizedPDFs->g1n_Twist4_TMC(x , Q2); }
      virtual double g1d_Twist4_TMC(double   x, double Q2) { return fPolarizedPDFs->g1d_Twist4_TMC(x , Q2); }
      virtual double g1He3_Twist4_TMC(double x, double Q2) { return fPolarizedPDFs->g1He3_Twist4_TMC(x   , Q2); }

      // g2 twist 4
      virtual double g2p_Twist4_TMC(double   x, double Q2) { return fPolarizedPDFs->g2p_Twist4_TMC(x , Q2); }
      virtual double g2n_Twist4_TMC(double   x, double Q2) { return fPolarizedPDFs->g2n_Twist4_TMC(x , Q2); }
      virtual double g2d_Twist4_TMC(double   x, double Q2) { return fPolarizedPDFs->g2d_Twist4_TMC(x , Q2); }
      virtual double g2He3_Twist4_TMC(double x, double Q2) { return fPolarizedPDFs->g2He3_Twist4_TMC(x   , Q2); }

      // -----------------------------
      // The wrong way of doing things (as a check):
      virtual double g2pWW_TMC(   double x, double Q2);
      virtual double g2pWW_TMC_t3(   double x, double Q2);


      ClassDef(PolarizedStructureFunctionsFromPDFs, 1)

}; 

using DefaultPolarizedStructureFunctions = PolarizedStructureFunctionsFromPDFs;

}
}

#endif 
