#ifndef PolSFsFromComptonAsymmetries_HH
#define PolSFsFromComptonAsymmetries_HH

#include "PolarizedStructureFunctions.h"
#include "VirtualComptonAsymmetries.h"
namespace insane {
namespace physics {

/** Uses A1 and A2 along with an instance of StructureFunctions (F1) to get g1 and g2.
 *
 * \f$ g1 = F1/(1+gamma^2)(A1 + gamma A2) \f$ 
 * \f$ g2 = F1/(1+gamma^2)(A2/gamma - A1) \f$ 
 * \f$ gamma^2  = 4M^2x^2/Q^2 \f$
 */
class PolSFsFromComptonAsymmetries : public PolarizedStructureFunctions {
   protected:
      StructureFunctions          * fSFs;
      VirtualComptonAsymmetries   * fA1A2;

   public:
      PolSFsFromComptonAsymmetries();
      virtual ~PolSFsFromComptonAsymmetries();

      void  SetVirtualComptonAsymmetries(VirtualComptonAsymmetries * v){ fA1A2 = v; }
      VirtualComptonAsymmetries * GetVirtualComptonAsymmetries(){ return fA1A2; }

      virtual double A1p(double x,double Q2){
         if(fA1A2)
            return fA1A2->A1p(x,Q2);
         /*else*/
         Error("A1p","No VirtualComptonAsymmetries defined");
         return 0.0;
      }
      virtual double A2p(double x,double Q2){
         if( fA1A2) 
            return fA1A2->A2p(x,Q2); 
         /*else*/
         Error("A1p","No VirtualComptonAsymmetries defined");
         return 0.0;
      }

      virtual double g1p(double x, double Q2){
         double  gamma   = 2.0*(M_p/GeV)*x/TMath::Sqrt(Q2);
         double  gamma2  = gamma*gamma;
         double  F1      = fSFs->F1p(x,Q2);
         return( (F1/(1.0+gamma2))*(A1p(x,Q2)+gamma*A2p(x,Q2)) );
      }
      virtual double g2p(double x, double Q2){
         double  gamma   = 2.0*(M_p/GeV)*x/TMath::Sqrt(Q2);
         double  gamma2  = gamma*gamma;
         double  F1      = fSFs->F1p(x,Q2);
         return( (F1/(1.0+gamma2))*(A2p(x,Q2)/gamma - A1p(x,Q2)) );
      }

      // neutron 
      virtual double g1n(double x,double Q2) {return 0 ; }
      // deuteron 
      virtual double g1d(double x,double Q2) {return 0 ; }
      // He3 
      virtual double g1He3(double x,double Q2) {return 0 ; }

      // neutron 
      virtual double g2n(double x,double Qsq) {return 0 ; } 
      // deuteron 
      virtual double g2d(double x,double Qsq) {return 0 ; }
      // 3He  
      virtual double g2He3(double x,double Qsq) {return 0 ; } 

ClassDef(PolSFsFromComptonAsymmetries,1)
};

}}

#endif

