#ifndef NMC95STRUCTUREFUNCTIONS_H
#define NMC95STRUCTUREFUNCTIONS_H 1

#include <cstdlib> 
#include <iostream> 
#include <iomanip> 
#include <fstream>
#include "TString.h" 
#include "FortranWrappers.h"
#include "StructureFunctions.h"
#include "FortranWrappers.h"
namespace insane {
namespace physics {

/** Concrete imp of NMC95 structure functions F1 and F2 for n, p, d and higher A nuclei  
 * 
 *  Coverage: 0.5<Q2<75 GeV^2, 0.006<x<0.9
 *
 *
 * \ingroup structurefunctions
 */

class NMC95StructureFunctions : public StructureFunctions {

   private: 
      double   fA,fZ,fN;
      double fF1,fF2,fR,fM;

      void GetSFs(Int_t,double,double); 

   public:
      NMC95StructureFunctions(); 
      virtual ~NMC95StructureFunctions(); 

      // Returns F2 per nucleus (not per nucleon)
      virtual double F2Nuclear(double x,double Qsq,double Z, double A);
      virtual double F1Nuclear(double x,double Qsq,double Z, double A);

      virtual double F1p(double,double); 
      virtual double F1n(double,double); 
      virtual double F1d(double,double); 
      virtual double F1He3(double,double); 
      virtual double F2p(double,double); 
      virtual double F2n(double,double); 
      virtual double F2d(double,double); 
      virtual double F2He3(double,double); 

      ClassDef(NMC95StructureFunctions,1)
};
}}

#endif 
