#ifndef insane_physics_SSFsFromPDFs_HH 
#define insane_physics_SSFsFromPDFs_HH 

#include "SpinStructureFunctions.h"
#include "Physics.h"
//#include "PDFBase.h"
#include <tuple>
#include <utility>


namespace insane {
  namespace physics {

    //template<class S, class T = Twist, class L = OPELimit> 
    //  struct SFCondition{ };

//#pragma GCC diagnostic push
//#pragma GCC diagnostic ignored "-Wc++17-extensions"
    /** Spin structure functions.
     *
     */
    template<class...T>
    double g1(const std::tuple<T...>& dfs, Nuclei target, double x, double Q2){
      return( std::get<T>(dfs).g1(x,Q2,target)+...);
    } 
    //template<class T, class HT>
    //double g1(const T&& pdfs, const HT&& ht, Nuclei target, double x, double Q2){
    //  //calls the function above
    //  return( g1(std::make_tuple(std::forward<T>(pdfs),std::forward<HT>(ht)),target,x,Q2) );
    //} 
    //template<class T>
    //double g1(const T&& pdfs, Nuclei target, double x, double Q2){
    //  //calls the function above
    //  return( g1(std::make_tuple(std::forward<T>(pdfs)),target,x,Q2) );
    //} 
    //______________________________________________________________________________

    template<class...T>
    double g2(const std::tuple<T...>& dfs, Nuclei target, double x, double Q2){
      return( std::get<T>(dfs).g2(x,Q2,target)+...);
    } 
    //template<class T, class HT>
    //double g2(const T&& pdfs, const HT&& ht, Nuclei target, double x, double Q2){
    //  return( g2(std::make_tuple(std::forward<T>(pdfs),std::forward<HT>(ht)),target,x,Q2) );
    //} 
    //template<class T>
    //double g2(const T&& pdfs, Nuclei target, double x, double Q2){
    //  return( g2(std::make_tuple(std::forward<T>(pdfs)),target,x,Q2) );
    //} 
    //______________________________________________________________________________

    /** Target Mass Corrections.
     */
    template<class...T>
    double g1_TMC(const std::tuple<T...>& dfs, Nuclei target, double x, double Q2){
      return( std::get<T>(dfs).g1_TMC(x,Q2,target)+...);
    } 
    //template<class T, class HT>
    //double g1_TMC(const T&& pdfs, const HT&& ht, Nuclei target, double x, double Q2){
    //  //return( pdfs.g2(target,x,Q2) + ht.g2(target,x,Q2) );
    //  return( g1_TMC(std::make_tuple(std::forward<T>(pdfs),std::forward<HT>(ht)),target,x,Q2) );
    //} 
    //template<class T>
    //double g1_TMC(const T&& pdfs, Nuclei target, double x, double Q2){
    //  //return( pdfs.g2(target,x,Q2) );
    //  return( g1_TMC(std::make_tuple(std::forward<T>(pdfs)),target,x,Q2) );
    //} 
    //______________________________________________________________________________

    template<class...T>
    double g2_TMC(const std::tuple<T...>& dfs, Nuclei target, double x, double Q2){
      return( std::get<T>(dfs).g2_TMC(x,Q2,target)+...);
    } 
    //template<class T, class HT>
    //double g2_TMC(const T&& pdfs, const HT&& ht, Nuclei target, double x, double Q2){
    //  //calls template above
    //  return( g2_TMC(std::make_tuple(std::forward<T>(pdfs),std::forward<HT>(ht)),target,x,Q2) );
    //} 
    //template<class T>
    //double g2_TMC(const T&& pdfs, Nuclei target, double x, double Q2){
    //  //calls template above
    //  return( g2_TMC(std::make_tuple(std::forward<T>(pdfs)),target,x,Q2) );
    //} 

//#pragma GCC diagnostic pop

    //______________________________________________________________________________

    /** SpinStructureFunction implementation for polarized PDFs.
     *  Although we don't need this class to calculate the SSFs, it provides
     *  some usefulness when using with other structure functions.
     */
    template <class PDF, class ...T>
    class SSFsFromPDFs : public SpinStructureFunctions {

      public:
        std::tuple<PDF, T...> fDFs;

      public:
        SSFsFromPDFs(){}
        virtual ~SSFsFromPDFs(){}

        PDF*          GetPDF_ptr() {return &std::get<0>(fDFs);}
        const PDF&    GetPDFs() const {return std::get<0>(fDFs);}
        const std::tuple<PDF, T...>&   GetDFs() const { return fDFs;}

        virtual double Calculate    (double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t = Twist::All, OPELimit l = OPELimit::MassiveTarget) const;
        virtual double Uncertainties(double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t = Twist::All, OPELimit l = OPELimit::MassiveTarget) const;

        // these are in SpinStructureFunctions
        //virtual double g1(double x, double Q2, Nuclei target) const { return Calculate(x,Q2,std::make_tuple(SF::g1,target)); }
        //virtual double g2(double x, double Q2, Nuclei target) const { return Calculate(x,Q2,std::make_tuple(SF::g2,target)); }
        //virtual double g1(double x, double Q2, Nuclei target) const { return Calculate(x,Q2,std::make_tuple(SF::g1,target)); }
        //virtual double g2(double x, double Q2, Nuclei target) const { return Calculate(x,Q2,std::make_tuple(SF::g2,target)); }
        //virtual double g2_WW(Nuclei target, double x, double Q2) const { return Calculate(x,Q2,std::make_tuple(SF::g2_WW,target)); }
        //virtual double g1_BT(Nuclei target, double x, double Q2) const { return Calculate(x,Q2,std::make_tuple(SF::g2_WW,target)); }

        virtual double g1p(double x, double Q2) const{ return Calculate(x,Q2,std::make_tuple(SF::g1,Nuclei::p),Twist::All, OPELimit::Massless); }
        virtual double g2p(double x, double Q2) const{ return Calculate(x,Q2,std::make_tuple(SF::g2,Nuclei::p),Twist::All, OPELimit::Massless); }
        virtual double g1n(double x, double Q2) const{ return Calculate(x,Q2,std::make_tuple(SF::g1,Nuclei::n),Twist::All, OPELimit::Massless); }
        virtual double g2n(double x, double Q2) const{ return Calculate(x,Q2,std::make_tuple(SF::g2,Nuclei::n),Twist::All, OPELimit::Massless); }
        virtual double g1p_TMC(double x, double Q2) const{ return Calculate(x,Q2,std::make_tuple(SF::g1,Nuclei::p)); }
        virtual double g2p_TMC(double x, double Q2) const{ return Calculate(x,Q2,std::make_tuple(SF::g2,Nuclei::p)); }
        virtual double g1n_TMC(double x, double Q2) const{ return Calculate(x,Q2,std::make_tuple(SF::g1,Nuclei::n)); }
        virtual double g2n_TMC(double x, double Q2) const{ return Calculate(x,Q2,std::make_tuple(SF::g2,Nuclei::n)); }

        ClassDef(SSFsFromPDFs,1)
    };


  }
}

#include "SSFsFromPDFs.hxx"


#endif

