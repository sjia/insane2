#ifndef BETAG4StructureFunctions_H
#define BETAG4StructureFunctions_H 1

#include "TMath.h"
#include "StructureFunctions.h"

namespace insane {
namespace physics {
/**
 */
class BETAG4StructureFunctions : public StructureFunctions {
public:

   BETAG4StructureFunctions();

   virtual ~BETAG4StructureFunctions();

   virtual double F1p(double x, double Qsq);

   virtual double F2p(double x, double Qsq);

   virtual double F1n(double x, double Qsq);

   virtual double F2n(double x, double Qsq);

   virtual double F1d(double x, double Qsq);

   virtual double F2d(double x, double Qsq);

   virtual double g1p(double x, double Qsq);

   virtual double g2p(double x, double Qsq);


   ClassDef(BETAG4StructureFunctions, 1)
};

}
}


#endif



