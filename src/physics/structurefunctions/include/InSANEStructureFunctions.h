#ifndef InSANEStructureFunctions_H
#define InSANEStructureFunctions_H 1

#include "TNamed.h"
#include "TMath.h"
#include "TString.h"
#include "TH1F.h"
#include "InSANEPhysicalConstants.h"
#include "InSANEMathFunc.h"
#include "InSANEFortranWrappers.h"


double EMC_Effect(double *x, double *p); 
double EMC_Effect(double x, double A); 


/** Base class for Structure Functions
 *
 * \ingroup structurefunctions
 */
class InSANEStructureFunctionBase : public TNamed {

   protected:
      TString  fLabel;
      TString  fComments;

   public:
      InSANEStructureFunctionBase();
      virtual ~InSANEStructureFunctionBase();
      
      void        SetLabel(const char * l){ fLabel = l;}
      const char* GetLabel(){ return fLabel; }

      virtual void Print(Option_t * opt = "") const;

      typedef enum  { 
         kF1p   ,
         kF1n   ,
         kF1d   ,
         kF1He3 ,

         kF2p ,
         kF2n ,
         kF2d ,
         kF2He3 
      } UnpolarizedSFType;

      typedef enum  { 
         kg1p   ,
         kg1n   ,
         kg1d   ,
         kg1He3 ,

         kg2p ,
         kg2n ,
         kg2d ,
         kg2He3 ,

         kg2pWW ,
         kg2nWW ,
         kg2dWW ,
         kg2He3WW 
      } PolarizedSFType;

      ClassDef(InSANEStructureFunctionBase, 1)
};


/** ABC for calculating the nucleon structure functions
 * 
 * \f$ \lim_{Bjorken} M W_1(p.q,Q^2) = F_1(x)  \f$  
 *
 * \f$ \lim_{Bjorken} \nu W_2(p.q,Q^2) = F_2(x)   \f$  
 *
 * \ingroup structurefunctions
 */
class InSANEStructureFunctions : public InSANEStructureFunctionBase {

   public:
      InSANEStructureFunctions();
      virtual ~InSANEStructureFunctions();

      virtual double F1p(   double x, double Q2) = 0;
      virtual double F2p(   double x, double Q2) = 0;
      virtual double F1n(   double x, double Q2) = 0;
      virtual double F2n(   double x, double Q2) = 0;
      virtual double F1d(   double x, double Q2) = 0;
      virtual double F2d(   double x, double Q2) = 0;
      virtual double F1He3( double x, double Q2) = 0;
      virtual double F2He3( double x, double Q2) = 0;

      virtual double F1p_Error(   double x, double Q2);
      virtual double F2p_Error(   double x, double Q2);
      virtual double F1n_Error(   double x, double Q2);
      virtual double F2n_Error(   double x, double Q2);
      virtual double F1d_Error(   double x, double Q2);
      virtual double F2d_Error(   double x, double Q2);
      virtual double F1He3_Error( double x, double Q2);
      virtual double F2He3_Error( double x, double Q2);

      virtual double xF1p(   double x, double Q2);
      virtual double xF2p(   double x, double Q2);
      virtual double xF1n(   double x, double Q2);
      virtual double xF2n(   double x, double Q2);
      virtual double xF1d(   double x, double Q2);
      virtual double xF2d(   double x, double Q2);
      virtual double xF1He3( double x, double Q2);
      virtual double xF2He3( double x, double Q2);

      // R=sigma_L/sigma_T
      virtual double R(double x, double Q2);

      // Rnp = F2n/F2p
      virtual double Rnp(double x, double Q2);
      virtual double Rnp_Error(double x, double Q2);

      virtual double W1p(double x, double Qsq);
      virtual double W2p(double x, double Qsq);
      virtual double W1n(double x, double Qsq);
      virtual double W2n(double x, double Qsq);

      // Returns F2 per nucleus (not per nucleon)
      virtual double F2Nuclear(  double x, double Qsq, double Z, double A);
      virtual double xF2Nuclear( double x, double Qsq, double Z, double A);
      virtual double F1Nuclear(  double x, double Qsq, double Z, double A);
      virtual double xF1Nuclear( double x, double Qsq, double Z, double A);

      /** @name Useful for using as ROOT functions, TF1 etc...
       *
       * Use with TF1. The arguments are x=x[0], Q^2=p[0] 
       * @{
       */
      double EvaluateF1p(double *x, double *p) { return(F1p(x[0], p[0])); }
      double EvaluateF2p(double *x, double *p) { return(F2p(x[0], p[0])); }
      double EvaluateF1n(double *x, double *p) { return(F1n(x[0], p[0])); }
      double EvaluateF2n(double *x, double *p) { return(F2n(x[0], p[0])); }
      double EvaluateF1d(double *x, double *p) { return(F1d(x[0], p[0])); }
      double EvaluateF2d(double *x, double *p) { return(F2d(x[0], p[0])); }
      double EvaluateF1He3(double *x, double *p) { return(F1He3(x[0], p[0])); }
      double EvaluateF2He3(double *x, double *p) { return(F2He3(x[0], p[0])); }
      double EvaluatexF1p(double *x, double *p) { return(xF1p(x[0], p[0])); }
      double EvaluatexF2p(double *x, double *p) { return(xF2p(x[0], p[0])); }
      double EvaluatexF1n(double *x, double *p) { return(xF1p(x[0], p[0])); }
      double EvaluatexF2n(double *x, double *p) { return(xF2p(x[0], p[0])); }
      double EvaluatexF1d(double *x, double *p) { return(xF1d(x[0], p[0])); }
      double EvaluatexF2d(double *x, double *p) { return(xF2d(x[0], p[0])); }
      double EvaluatexF1He3(double *x, double *p) { return(xF1He3(x[0], p[0])); }
      double EvaluatexF2He3(double *x, double *p) { return(xF2He3(x[0], p[0])); }
      double EvaluateR(double *x, double *p) { return(R(x[0], p[0])); }
      double EvaluateF2nOverF2d(double *x, double *p) { return( F2n(x[0], p[0])/F2d(x[0], p[0])); }
      double EvaluateF2nOverF2d_W(double *x, double *p) {
         // x[0] = W
         double xbj = InSANE::Kine::xBjorken_WQsq(x[0],p[0]);
         return( F2n(xbj, p[0])/F2d(xbj, p[0]));
      }
      double EvaluateF2Nuclear(double *x, double *p) { return( F2Nuclear(x[0], p[0],p[1],p[2]) ); }
      double EvaluateF1Nuclear(double *x, double *p) { return( F1Nuclear(x[0], p[0],p[1],p[2]) ); }
      double EvaluatexF2Nuclear(double *x, double *p) { return( xF2Nuclear(x[0], p[0],p[1],p[2]) ); }
      double EvaluatexF1Nuclear(double *x, double *p) { return( xF1Nuclear(x[0], p[0],p[1],p[2]) ); }
      double EvaluatedOveruRatio(double *x, double *p) { 
         double F2nOverF2p = F2n(x[0], p[0])/F2p(x[0], p[0]);
         return((4.0*F2nOverF2p-1.0)/(4.0-F2nOverF2p));
      }


      /** Use with TF2. The arguments are x=x[0], Q^2=x[1] */
      double Evaluate2DxF1p(double *x, double * /*p*/) { return(xF1p(x[0], x[1])); }
      double Evaluate2DxF2p(double *x, double * /*p*/) { return(xF2p(x[0], x[1])); }
      double Evaluate2DF1p(double *x, double * /*p*/) { return(F1p(x[0], x[1])); }
      double Evaluate2DF2p(double *x, double * /*p*/) { return(F2p(x[0], x[1])); }
      //@}

      /** Fills supplied histogram vs x.
       *
       */
      void GetValues(TObject *obj, double Q2, InSANEStructureFunctionBase::UnpolarizedSFType = InSANEStructureFunctionBase::kF2p );

      /** Get error band.
       *  Argument obj is assumed to be a TH1. 
       */ 
      void GetErrorBand(TObject *obj, double Q2, InSANEStructureFunctionBase::UnpolarizedSFType = InSANEStructureFunctionBase::kF2p );

      ClassDef(InSANEStructureFunctions, 2)
};

#endif

