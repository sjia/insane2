#ifndef StructureFunctionsFromVPCSs_HH
#define StructureFunctionsFromVPCSs_HH

#include "StructureFunctions.h"
#include "PhotoAbsorptionCrossSections.h"
namespace insane {
namespace physics {

class StructureFunctionsFromVPCSs : public StructureFunctions {

   protected:

      PhotoAbsorptionCrossSections * fVPCSs;

      double   f4piAlphaOverM;

   public:
      StructureFunctionsFromVPCSs();
      virtual ~StructureFunctionsFromVPCSs();

      virtual double F1p(   double x, double Q2);
      virtual double F2p(   double x, double Q2);
      virtual double F1n(   double x, double Q2);
      virtual double F2n(   double x, double Q2);
      virtual double F1d(   double x, double Q2);
      virtual double F2d(   double x, double Q2);
      virtual double F1He3( double x, double Q2);
      virtual double F2He3( double x, double Q2);

      void SetVPCSs(PhotoAbsorptionCrossSections * v) { fVPCSs = v; }
      PhotoAbsorptionCrossSections * GetVPCSs() { return fVPCSs; }

   ClassDef(StructureFunctionsFromVPCSs,1)
};
}}


#endif

