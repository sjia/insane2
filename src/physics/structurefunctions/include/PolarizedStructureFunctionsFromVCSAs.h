#ifndef PolarizedStructureFunctionsFromVCSAs_HH
#define PolarizedStructureFunctionsFromVCSAs_HH 1

#include "VirtualComptonAsymmetries.h"
#include "PolarizedStructureFunctions.h"
#include "StructureFunctions.h"
namespace insane {
namespace physics {

class PolarizedStructureFunctionsFromVCSAs: public PolarizedStructureFunctions {

   protected:

      VirtualComptonAsymmetries * fVCSAs;
      StructureFunctions        * fSFs;

   public:

      PolarizedStructureFunctionsFromVCSAs();
      virtual ~PolarizedStructureFunctionsFromVCSAs();

      VirtualComptonAsymmetries * GetVCSAs(){return fVCSAs;}
      void SetVCSAs(VirtualComptonAsymmetries * v) {fVCSAs = v;}

      StructureFunctions * GetSFs(){return fSFs;}
      void SetSFs(StructureFunctions * v) {fSFs = v;}
      

      virtual double g1p(   double x, double Q2);
      virtual double g1n(   double x, double Q2);
      virtual double g1d(   double x, double Q2);
      virtual double g1He3( double x, double Q2);

      virtual double g2p(   double x, double Q2);
      virtual double g2n(   double x, double Q2);
      virtual double g2d(   double x, double Q2);
      virtual double g2He3( double x, double Q2);

   ClassDef(PolarizedStructureFunctionsFromVCSAs,1)
};
}}


#endif
