#ifndef StructureFunctionsFromPDFs_H
#define StructureFunctionsFromPDFs_H 2 

#include "TNamed.h"
#include "TMath.h"
#include "TString.h"
#include "PhysicalConstants.h"
#include "StructureFunctions.h"
#include "PolarizedStructureFunctions.h"
#include "PartonDistributionFunctions.h"
#include "InSANEMathFunc.h"

namespace insane {
namespace physics {
/** ABC class for unpolarized structure functions using unpolarized parton distributions.
 *  Makes use of the naive parton model interpretation of PDFs following the convention:
 * 
 *  \f$ F_2(x) = \sum_q e_q^2 x q(x)  \f$
 *
 *  \f$ F_1(x) = \frac{F_2(x)(1+\gamma^2)}{2x(R+1)} \f$
 *
 * where \f$ R = \sigma_L/\sigma_T \f$
 * 
 * Target mass effects are included by default when calling the functions F2(x,Q2) or F1(x,Q2). 
 * They are calculated from the approximate form (Phys Rev D 84, 074008 (2011) eqn.9). If 
 * you want to just calculate the leading twist piece, use F1XNoTMC(x,Q2) instead.
 *
 * \ingroup structurefunctions
 */

class StructureFunctionsFromPDFs : public StructureFunctions {

   private:
      Bool_t fUsesR;
      Bool_t fTargetMassCorrections;
      Int_t  fNintegrate;

   protected:
      double h2_TMC(double xi, double Q2);
      double g2_TMC(double xi, double Q2);

   public:
      PartonDistributionFunctions * fUnpolarizedPDFs; //->

   public:
      StructureFunctionsFromPDFs(); 
      StructureFunctionsFromPDFs(PartonDistributionFunctions * pdfs);  
      virtual ~StructureFunctionsFromPDFs(); 

      /** Set whether or not to use R fit incalculating F1.
       *  By default it is false. Turning it off sets R=0.
       */
      void    SetUseR(Bool_t v = true){fUsesR=v; }//if(v) fTargetMassCorrections = (!v);}
      Bool_t  GetUseR() const {return fUsesR;}

      void    CalculateTMCs(Bool_t v = true){fTargetMassCorrections=v;}// if(v) fUsesR = (!v);}
      Bool_t  TargetMassCorrected() const {return fTargetMassCorrections;}

      virtual double R(double x, double Qsq);

      void SetUnpolarizedPDFs(PartonDistributionFunctions * pdfs);
      PartonDistributionFunctions *  GetUnpolarizedPDFs();

      virtual double F2p(      double x, double Qsq);
      virtual double F2pNoTMC( double x, double Qsq);
      virtual double F1p(      double x, double Qsq);
      virtual double F1pNoTMC( double x, double Qsq);

      virtual double F2n(      double x, double Qsq);
      virtual double F2nNoTMC( double x, double Qsq);
      virtual double F1n(      double x, double Qsq);
      virtual double F1nNoTMC( double x, double Qsq);

      virtual double F1d(   double x, double Qsq);
      virtual double F2d(   double x, double Qsq);

      virtual double F1He3( double x, double Qsq);
      virtual double F2He3( double x, double Qsq);

      virtual double F1p_Error(   double x, double Q2);
      virtual double F2p_Error(   double x, double Q2);
      virtual double F1n_Error(   double x, double Q2);
      virtual double F2n_Error(   double x, double Q2);
      virtual double F1d_Error(   double x, double Q2);
      virtual double F2d_Error(   double x, double Q2);
      virtual double F1He3_Error( double x, double Q2);
      virtual double F2He3_Error( double x, double Q2);

      virtual double F1pNoTMC_Error(   double x, double Q2);
      virtual double F2pNoTMC_Error(   double x, double Q2);
      virtual double F1nNoTMC_Error(   double x, double Q2);
      virtual double F2nNoTMC_Error(   double x, double Q2);
      virtual double F1dNoTMC_Error(   double x, double Q2);
      virtual double F2dNoTMC_Error(   double x, double Q2);
      virtual double F1He3NoTMC_Error( double x, double Q2);
      virtual double F2He3NoTMC_Error( double x, double Q2);

      virtual double xF1p(double x, double Qsq){ return( x*F1p(x,Qsq)); } 
      virtual double xF2p(double x, double Qsq){ return( x*F2p(x,Qsq)); }
      virtual double xF1n(double x, double Qsq) { return(x*F1n(x, Qsq)); }
      virtual double xF2n(double x, double Qsq) { return(x*F2n(x, Qsq)); }
      virtual double xF1d(double x, double Qsq) { return(x * F1d(x, Qsq)); }
      virtual double xF2d(double x, double Qsq) { return(x * F2d(x, Qsq)); }
      virtual double xF1He3(double x, double Qsq) { return(x * F1He3(x, Qsq)); }
      virtual double xF2He3(double x, double Qsq) { return(x * F2He3(x, Qsq)); }

      double  EvaluateF1pNoTMC(double *x, double *p) {  return(F1pNoTMC(x[0], p[0])); }
      double  EvaluatexF1pNoTMC(double *x, double *p) { return(x[0]*F1pNoTMC(x[0], p[0])); }

      virtual double F2Nuclear(  double x, double Qsq, double Z, double A);
      virtual double F1Nuclear(  double x, double Qsq, double Z, double A);
      
      ClassDef(StructureFunctionsFromPDFs,2)
};
}}


#endif
