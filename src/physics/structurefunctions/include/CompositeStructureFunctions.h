#ifndef CompositeStructureFunctions_HH
#define CompositeStructureFunctions_HH 1

//#include "LHAPDFUnpolarizedPDFs.h"
#include "CTEQ6UnpolarizedPDFs.h"
#include "StatisticalUnpolarizedPDFs.h"
#include "F1F209StructureFunctions.h"
#include "NMC95StructureFunctions.h"
#include "StructureFunctions.h"
#include "StructureFunctionsFromPDFs.h"
#include "TList.h"

namespace insane {
namespace physics {
/** ABC for a composite structure function which calls different structure functions depending on
 *  the kinematic variables, x and Q2.
 *  You must implement GetWeight(i,x,Q2)
 *  which computes the weight for the ith structure function. If it is zero, the function is not evaluate.
 * 
 *  Potential problem might be a jump in values at transition points and discontinuity of the derivative.
 *  Therefore, a smooth transition can be done with the weights.
 *  
 * \ingroup structurefunctions
 */ 
class CompositeStructureFunctions : public StructureFunctions {

   protected:
      TList fSFList;
      Int_t fNSFs;

   public:
      CompositeStructureFunctions();
      virtual ~CompositeStructureFunctions();

      void   Add(StructureFunctions * sf);

      // Returns F2 per nucleus (not per nucleon)
      virtual double F2Nuclear(double x,double Qsq,double Z, double A);
      virtual double F1Nuclear(double x,double Qsq,double Z, double A);

      // Pure virtual method which returns the index of the SF to use based on x and Q2. 
      //virtual Int_t GetIndex(double x, double Q2) = 0; 

      /** returns a weight for the ith SF. */
      virtual double GetWeight(Int_t iSF,double x, double Q2) = 0; 

      /// Here is where you implement the specifics
      // proton
      virtual double F2p(double x, double Qsq);
      virtual double F1p(double x, double Qsq);

      // neutron
      virtual double F2n(double x, double Qsq);
      virtual double F1n(double x, double Qsq);

      // Deuteron
      virtual double F2d(double x, double Qsq);
      virtual double F1d(double x, double Qsq);

      // He3 
      virtual double F2He3(double x, double Qsq);
      virtual double F1He3(double x, double Qsq);

      ClassDef(CompositeStructureFunctions,1)
};


/** Best structure function implementation at Low \f$ Q^2 \f$ over all of x.
 *  - CTEQ6 for low X 
 *  - F1F209 for high x
 */
class LowQ2StructureFunctions : public CompositeStructureFunctions {
   private:
      //CTEQ6UnpolarizedPDFs              fPDFs;
      //StatisticalUnpolarizedPDFs          fPDFs;
      //StructureFunctionsFromPDFs    fCTEQSFs;
      F1F209StructureFunctions            fF1F209SFs;
      NMC95StructureFunctions             fNMCSFs;
      
      //double xrange[5]; 

   public:
      LowQ2StructureFunctions();
      virtual ~LowQ2StructureFunctions();

      virtual double GetWeight(Int_t iSF, double x, double Q2);

      //virtual Int_t GetIndex(double x, double Q2){
      //  if( x>xrange[0] && x<xrange[1]) { // CTEQ
      //     return(0); // CTEQ
      //  } else if( x>=xrange[1] && x<xrange[2] ) {
      //     return(1); // F1F209
      //  } else { /// Error
      //     return(0); // CTEQ
      //  }
      //}


      ClassDef(LowQ2StructureFunctions,1)
};

}
}
#endif

