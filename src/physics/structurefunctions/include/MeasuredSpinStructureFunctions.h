#ifndef MeasuredSpinStructureFunctions_HH
#define MeasuredSpinStructureFunctions_HH 1

#include "TObject.h"

namespace insane {
namespace physics {

class MeasuredSpinStructureFunctions : public TObject {

   public:

      double    fx  = 0.0;
      double    fy  = 0.0;
      double    fQ2 = 0.0;
      double    fW  = 0.0;
      double    fF1 = 0.0;

      double    fA1     = 0.0;
      double    fA2     = 0.0;
      double    fA1_unc = 0.0;
      double    fA2_unc = 0.0;

      double    fg1     = 0.0;
      double    fg2     = 0.0;
      double    fg1_unc = 0.0;
      double    fg2_unc = 0.0;

   public:

      MeasuredSpinStructureFunctions();
      virtual ~MeasuredSpinStructureFunctions();

      double g1();
      double g2();

      double g1_unc();
      double g2_unc();

   ClassDef(MeasuredSpinStructureFunctions,1)
};
}}

#endif
