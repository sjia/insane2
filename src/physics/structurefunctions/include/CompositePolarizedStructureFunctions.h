#ifndef CompositePolarizedStructureFunctions_HH
#define CompositePolarizedStructureFunctions_HH 1

#include "PolarizedStructureFunctions.h"
#include "PolarizedStructureFunctionsFromPDFs.h"
#include "DSSVPolarizedPDFs.h"
#include "AAC08PolarizedPDFs.h"
#include "BBPolarizedPDFs.h"
#include "GSPolarizedPDFs.h"
#include "DNS2005PolarizedPDFs.h"
#include "LSS2006PolarizedPDFs.h"
#include "TList.h"
#include "DSSVPolarizedPDFs.h"

namespace insane {
namespace physics {
/** ABC for a composite structure function which calls different structure functions depending on
 *  the kinematic variables, x and Q2.
 *  Potential problem: 
 *  You must implement GetSFIndex(x,Q2)
 *
 * \ingroup structurefunctions
 */ 
class CompositePolarizedStructureFunctions : public PolarizedStructureFunctions {

   protected:
      TList fSFList;
      Int_t fNSFs;

   public:
      CompositePolarizedStructureFunctions();
      virtual ~CompositePolarizedStructureFunctions();

      void   Add(PolarizedStructureFunctions * sf);

      //virtual Int_t GetIndex(double x, double Q2) = 0; 
      virtual double GetWeight(Int_t iSF,double x, double Q2) = 0; 

      /// Here is where you implement the specifics
      virtual double g2p(double x, double Qsq);
      virtual double g1p(double x, double Qsq);
      virtual double g2n(double x, double Qsq);
      virtual double g1n(double x, double Qsq);
      virtual double g2d(double x, double Qsq);
      virtual double g1d(double x, double Qsq);
      virtual double g2He3(double x, double Qsq);
      virtual double g1He3(double x, double Qsq);
      



ClassDef(CompositePolarizedStructureFunctions,1)
};


/** Best structure function implementation at Low \f$ Q^2 \f$ over all of x.
 *  - CTEQ6 for low X 
 *  - F1F209 for high x
 */
class LowQ2PolarizedStructureFunctions : public CompositePolarizedStructureFunctions {

   private:
      DSSVPolarizedPDFs fDSSVPolarizedPDFs;
      PolarizedStructureFunctionsFromPDFs fDSSVSFs;
      AAC08PolarizedPDFs fAACPolarizedPDFs;
      PolarizedStructureFunctionsFromPDFs fAACSFs;
      
      double xrange[5]; 

   public:
      LowQ2PolarizedStructureFunctions();
      virtual ~LowQ2PolarizedStructureFunctions();

      virtual double GetWeight(Int_t iSF, double x, double Q2);

      ClassDef(LowQ2PolarizedStructureFunctions,1)
};

}}


#endif

