#ifndef PolarizedStructureFunctions_H
#define PolarizedStructureFunctions_H 2 
#include "TNamed.h"
#include "TMath.h"
#include "TString.h"
#include "TF1.h"
#include "TF2.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TLatex.h"
#include "TAxis.h"
#include "PhysicalConstants.h"
#include "StructureFunctions.h"
#include "TH1F.h"

namespace insane {
namespace physics {


/** ABC for polarized structure functions.
 *  
 *  How to implement:
 *   - Implement all 8 structure functions g1 and g2.
 *   - Do NOT reimplement any other functions.
 *   - Do NOT use calls to other functions in your specific implementation (e.g. g1p should not call xg1p). 
 *
 * \ingroup structurefunctions
 */
class PolarizedStructureFunctions : public StructureFunctionBase  {
   
   protected:
      Int_t fNintegrate;  // number of divisions for doing WW integrals

      //static PolarizedStructureFunctions*  fgPolarizedStructureFunctions;

   public:

      PolarizedStructureFunctions(); 
      virtual ~PolarizedStructureFunctions(); 

      //static PolarizedStructureFunctions* GetPolarizedStructureFunctions(){ return fgPolarizedStructureFunctions;}

      virtual double g1p(   double x, double Q2) = 0;
      virtual double g1n(   double x, double Q2) = 0;
      virtual double g1d(   double x, double Q2) = 0;
      virtual double g1He3( double x, double Q2) = 0;

      virtual double g1p_Error(   double x, double Q2){ return 0.0;}
      virtual double g1n_Error(   double x, double Q2){ return 0.0;}
      virtual double g1d_Error(   double x, double Q2){ return 0.0;}
      virtual double g1He3_Error( double x, double Q2){ return 0.0;}

      virtual double g2p(   double x, double Q2) = 0;
      virtual double g2n(   double x, double Q2) = 0;
      virtual double g2d(   double x, double Q2) = 0;
      virtual double g2He3( double x, double Q2) = 0;

      virtual double g2p_Error(   double x, double Q2);
      virtual double g2n_Error(   double x, double Q2);
      virtual double g2d_Error(   double x, double Q2);
      virtual double g2He3_Error( double x, double Q2);

      virtual double g2pWW(   double x, double Q2);
      virtual double g2nWW(   double x, double Q2);
      virtual double g2dWW(   double x, double Q2);
      virtual double g2He3WW( double x, double Q2);

      virtual double g2pWW_Error(   double x, double Q2);
      virtual double g2nWW_Error(   double x, double Q2);
      virtual double g2dWW_Error(   double x, double Q2);
      virtual double g2He3WW_Error( double x, double Q2);

      virtual double g2pWW_TMC(   double x, double Q2){return 0.0;}

      virtual double g2pWW_TMC_t3(   double x, double Q2){return 0.0;}

      // -----------------------------
      // Separated Twist contributions 
      // These maybe implemented separtely as needed.
      // g1 twist-2
      virtual double g1p_Twist2  ( double x, double Q2) ;
      virtual double g1n_Twist2  ( double x, double Q2) ;
      virtual double g1d_Twist2  ( double x, double Q2) ;
      virtual double g1He3_Twist2( double x, double Q2) ;

      // g2 twist 2 - g2WW
      virtual double g2p_Twist2(   double x, double Q2);
      virtual double g2n_Twist2(   double x, double Q2);
      virtual double g2d_Twist2(   double x, double Q2);
      virtual double g2He3_Twist2( double x, double Q2);

      // -----------------------------
      // g1 twist 3 - calculated as a TMC using g2_Twist3
      virtual double g1p_Twist3(   double x, double Q2);
      virtual double g1n_Twist3(   double x, double Q2);
      virtual double g1d_Twist3(   double x, double Q2);
      virtual double g1He3_Twist3( double x, double Q2);

      // g2 twist 3 
      virtual double g2p_Twist3(   double x, double Q2);
      virtual double g2n_Twist3(   double x, double Q2);
      virtual double g2d_Twist3(   double x, double Q2);
      virtual double g2He3_Twist3( double x, double Q2);

      // -----------------------------
      // g1 twist 4 - typically takes the form h(x)/Q^2
      virtual double g1p_Twist4(   double x, double Q2);
      virtual double g1n_Twist4(   double x, double Q2);
      virtual double g1d_Twist4(   double x, double Q2);
      virtual double g1He3_Twist4( double x, double Q2);

      // g2 twist 4 
      virtual double g2p_Twist4(   double x, double Q2);
      virtual double g2n_Twist4(   double x, double Q2);
      virtual double g2d_Twist4(   double x, double Q2);
      virtual double g2He3_Twist4( double x, double Q2);

      // ----------------------------
      // Target Mass Corrections
      // ----------------------------
      //
      virtual double g1p_TMC(   double x, double Q2);
      virtual double g1n_TMC(   double x, double Q2);
      virtual double g1d_TMC(   double x, double Q2);
      virtual double g1He3_TMC( double x, double Q2);

      virtual double g2p_TMC(   double x, double Q2);
      virtual double g2n_TMC(   double x, double Q2);
      virtual double g2d_TMC(   double x, double Q2);
      virtual double g2He3_TMC( double x, double Q2);

      // -----------------------------
      // Twist 2
      // g1_twist2 plus the twist-2 TMC
      virtual double g1p_Twist2_TMC  ( double x, double Q2){return g1p_Twist2(x,Q2);}
      virtual double g1n_Twist2_TMC  ( double x, double Q2){return g1n_Twist2(x,Q2);}
      virtual double g1d_Twist2_TMC  ( double x, double Q2){return g1d_Twist2(x,Q2);}
      virtual double g1He3_Twist2_TMC( double x, double Q2){return g1He3_Twist2(x,Q2);}

      // g1_twist2 plus the twist-2 TMC
      virtual double g2p_Twist2_TMC  ( double x, double Q2){return 0.0;}
      virtual double g2n_Twist2_TMC  ( double x, double Q2){return 0.0;}
      virtual double g2d_Twist2_TMC  ( double x, double Q2){return 0.0;}
      virtual double g2He3_Twist2_TMC( double x, double Q2){return 0.0;}

      // -----------------------------
      // Twist 3
      // g1_twist3 plus the twist-3 TMC
      virtual double g1p_Twist3_TMC  ( double x, double Q2){return 0.0;}
      virtual double g1n_Twist3_TMC  ( double x, double Q2){return 0.0;}
      virtual double g1d_Twist3_TMC  ( double x, double Q2){return 0.0;}
      virtual double g1He3_Twist3_TMC( double x, double Q2){return 0.0;}

      // g1_twist3 plus the twist-3 TMC
      virtual double g2p_Twist3_TMC  ( double x, double Q2){return 0.0;}
      virtual double g2n_Twist3_TMC  ( double x, double Q2){return 0.0;}
      virtual double g2d_Twist3_TMC  ( double x, double Q2){return 0.0;}
      virtual double g2He3_Twist3_TMC( double x, double Q2){return 0.0;}

      // -----------------------------
      // Twist 4
      // g1_twist3 plus the twist-3 TMC
      virtual double g1p_Twist4_TMC  ( double x, double Q2){return 0.0;}
      virtual double g1n_Twist4_TMC  ( double x, double Q2){return 0.0;}
      virtual double g1d_Twist4_TMC  ( double x, double Q2){return 0.0;}
      virtual double g1He3_Twist4_TMC( double x, double Q2){return 0.0;}

      // g1_Twist4 plus the twist-3 TMC
      virtual double g2p_Twist4_TMC  ( double x, double Q2){return 0.0;}
      virtual double g2n_Twist4_TMC  ( double x, double Q2){return 0.0;}
      virtual double g2d_Twist4_TMC  ( double x, double Q2){return 0.0;}
      virtual double g2He3_Twist4_TMC( double x, double Q2){return 0.0;}


      // g2-g2WW 
      virtual double g2pbar(   double x, double Q2);
      virtual double g2nbar(   double x, double Q2);
      virtual double g2dbar(   double x, double Q2);
      virtual double g2He3bar( double x, double Q2);

      //---------------------

      //@{
      /// with various Factors x and x^2
      /** \f$ \lim_{Bjorken} M^2 \nu G_1(p.q,Q^2) = g_1(x)   \f$
       *  \f$ \nu = \frac{Q^2}{2Mx} \f$
       */
      virtual double xg1p(    double x, double Q2);
      virtual double x2g1p(   double x, double Q2);
      virtual double xg1n(    double x, double Q2);
      virtual double x2g1n(   double x, double Q2);
      virtual double xg1d(    double x, double Q2);
      virtual double x2g1d(   double x, double Q2);
      virtual double xg1He3(  double x, double Q2);
      virtual double x2g1He3( double x, double Q2);

      /** \f$ \lim_{Bjorken} M \nu^2 G_2(p.q,Q^2) = g_2(x)   \f$
       *
       *  \f$ \nu = \frac{Q^2}{2Mx} \f$
       */
      virtual double  xg2p(  double x, double Q2);
      virtual double x2g2p(  double x, double Q2);
      virtual double  xg2pWW(double x, double Q2);
      virtual double x2g2pWW(double x, double Q2);

      virtual double xg2n(    double x, double Q2);
      virtual double x2g2n(   double x, double Q2);
      virtual double xg2nWW(  double x, double Q2);
      virtual double x2g2nWW( double x, double Q2);

      virtual double xg2d(    double x, double Q2);
      virtual double x2g2d(   double x, double Q2);
      virtual double xg2dWW(  double x, double Q2);
      virtual double x2g2dWW( double x, double Q2);

      virtual double xg2He3(    double x, double Q2);
      virtual double x2g2He3(   double x, double Q2);
      virtual double xg2He3WW(  double x, double Q2);
      virtual double x2g2He3WW( double x, double Q2);

      //@}

      /// Errors 
      /// \todo Need to handle g2 errors 
      virtual double g1pError(   double x, double Q2);
      virtual double g1dError(   double x, double Q2);
      virtual double g1nError(   double x, double Q2);
      virtual double g1He3Error( double x, double Q2);

      /** \f$ \lim_{Bjorken} M^2 \nu G_1(p.q,Q^2) = g_1(x)   \f$
       *  \f$ \lim_{Bjorken} M \nu^2 G_2(p.q,Q^2) = g_2(x)   \f$
       */
      virtual double G1p(double x, double Q2);
      virtual double G2p(double x, double Q2);


      /** @name Moments
       * Nachtmann and CN moments.
       *
       *  \f$ \displaystyle \Gamma_1^{(n)}(Q^2) = \int_0^1 x^{n-1} g_1(x,Q2)   \f$
       *
       *  \f$ \displaystyle \Gamma_2^{(n)}(Q^2) = \int_0^1 x^{n-1} g_2(x,Q2)  \f$
       *
       *  Nachtmann moments.
       *  Note here that n=1,3,5,...
       *
       *  \f$ \displaystyle M_1^{(n)}(Q^2) = \int_0^1 \frac{\xi^{n+1}}{x} \big[g_1(x,Q2)\big]   \f$
       *
       *  \f$ \displaystyle M_2^{(n)}(Q^2) = \int_0^1 x^{n-1} g_2(x,Q2)  \f$
       *
       *  \f$ \displaystyle I(Q^2) \f$
       *
       * @{
       */
      // Moments
      
      virtual double Mellin_g1p(Int_t n, double Q2,double x1 = 0.01,double x2 = 0.99);
      virtual double Mellin_g2p(Int_t n, double Q2,double x1 = 0.01,double x2 = 0.99);

      virtual double d2p_WW(        double Q2,double x1 = 0.01,double x2 = 0.99) ;
      virtual double d2p_Twist2_TMC(double Q2,double x1 = 0.01,double x2 = 0.99) ;
      virtual double d2p_Twist3(    double Q2,double x1 = 0.01,double x2 = 0.99) ;
      virtual double d2p_Twist3_TMC(double Q2,double x1 = 0.01,double x2 = 0.99) ;

      virtual double d2p_tilde(double Q2,double x1 = 0.01,double x2 = 0.99) ;
      virtual double d2n_tilde(double Q2,double x1 = 0.01,double x2 = 0.99) ;

      virtual double d2p_tilde_TMC(double Q2,double x1 = 0.01,double x2 = 0.99) ;

      double Gamma1_p(double Q2,double x1 = 0.01,double x2 = 0.99);
      double Gamma2_p(double Q2,double x1 = 0.01,double x2 = 0.99);

      double Gamma1_n(double Q2,double x1 = 0.01,double x2 = 0.99);
      double Gamma2_n(double Q2,double x1 = 0.01,double x2 = 0.99);

      double Gamma1_p_minus_n(double Q2,double x1 = 0.01,double x2 = 0.99);

      double M1n_p(             Int_t n, double Q2,double x1 = 0.01,double x2 = 0.99);
      double M1nIntegrand_p(    Int_t n, double x, double Q2);
      double M1n_TMC_p(         Int_t n, double Q2,double x1 = 0.01,double x2 = 0.99);
      double M1nIntegrand_TMC_p(Int_t n, double x, double Q2);

      double M2n_p(         Int_t n, double Q2,double x1 = 0.01,double x2 = 0.99);
      double M2nIntegrand_p(Int_t n, double x, double Q2);
      double M2n_TMC_p(         Int_t n, double Q2,double x1 = 0.01,double x2 = 0.99);
      double M2nIntegrand_TMC_p(Int_t n, double x, double Q2);
      double M2n_TMC_test_p(         Int_t n, double Q2,double x1 = 0.01,double x2 = 0.99);
      double M2nIntegrand_TMC_test_p(Int_t n, double x, double Q2);

      double Moment_g2p_Twist3(int n, double Q2, double x=0.01, double x2=0.99);

      double d2_Oscar(double Q2, double x1, double x2);
      //@}


      /** @name Useful for using as ROOT functions, TF1,TF2 etc...
       *
       * The arguments are x=x[0] and Q^2=p[0].
       *\code
       *    PolarizedStructureFunctionsFromPDFs * pSFs = new PolarizedStructureFunctionsFromPDFs();
       *    pSFs->SetPolarizedPDFs( new DNS2005PolarizedPDFs);
       *    Int_t npar=1;
       *    double Q2=5.0;
       *    TF1 * xg1 = new TF1("xg1p", pSFs, &PolarizedStructureFunctions::Evaluatexg1p,
       *                0, 1, npar,"PolarizedStructureFunctions","Evaluatexg1p");
       * \endcode
       * @{
       */

      double Evaluateg1p(double *x, double *p) { return(g1p(x[0], p[0])); }
      double Evaluatexg1p(double *x, double *p) { return(xg1p(x[0], p[0])); }
      double Evaluatex2g1p(double *x, double *p) { return(x2g1p(x[0], p[0])); }

      // --------------
      // proton
      double Evaluateg1p_Twist2(double *x, double *p) { return(g1p_Twist2(x[0], p[0])); }
      double Evaluatexg1p_Twist2(double *x, double *p) { return(x[0]*g1p_Twist2(x[0], p[0])); }
      double Evaluatex2g1p_Twist2(double *x, double *p) { return(x[0]*x[0]*g1p_Twist2(x[0], p[0])); }

      double Evaluateg1p_Twist3(double *x, double *p) { return(g1p_Twist3(x[0], p[0])); }
      double Evaluatexg1p_Twist3(double *x, double *p) { return(x[0]*g1p_Twist3(x[0], p[0])); }
      double Evaluatex2g1p_Twist3(double *x, double *p) { return(x[0]*x[0]*g1p_Twist3(x[0], p[0])); }

      double Evaluateg1p_Twist4(double *x, double *p) { return(g1p_Twist4(x[0], p[0])); }
      double Evaluatexg1p_Twist4(double *x, double *p) { return(x[0]*g1p_Twist4(x[0], p[0])); }
      double Evaluatex2g1p_Twist4(double *x, double *p) { return(x[0]*x[0]*g1p_Twist4(x[0], p[0])); }

      double Evaluatexg1p_Twist23(double *x, double *p) { return(x[0]*(g1p_Twist2(x[0], p[0])+g1p_Twist3(x[0], p[0]) )); }
      double Evaluatex2g1p_Twist23(double *x, double *p) { return(x[0]*x[0]*(g1p_Twist2(x[0], p[0])+g1p_Twist3(x[0], p[0]) )); }

      double Evaluatexg1p_Twist234(double *x, double *p) { return(x[0]*(g1p_Twist2(x[0], p[0])+g1p_Twist3(x[0], p[0])+g1p_Twist4(x[0], p[0]) )); }
      double Evaluatex2g1p_Twist234(double *x, double *p) { return(x[0]*x[0]*(g1p_Twist2(x[0], p[0])+g1p_Twist3(x[0], p[0])+g1p_Twist4(x[0], p[0]) )); }

      virtual double Evaluated2p_tilde(double *Q2, double *p){ return( d2p_tilde(Q2[0])); }

      double EvaluateGamma1_p(double *x, double *p) { return(Gamma1_p(x[0],p[0],p[1])); }
      double EvaluateGamma2_p(double *x, double *p) { return(Gamma2_p(x[0],p[0],p[1])); }


      // --------------
      // neutron
      double Evaluateg1n_Twist2(double *x, double *p) { return(g1n_Twist2(x[0], p[0])); }
      double Evaluatexg1n_Twist2(double *x, double *p) { return(x[0]*g1n_Twist2(x[0], p[0])); }
      double Evaluatex2g1n_Twist2(double *x, double *p) { return(x[0]*x[0]*g1n_Twist2(x[0], p[0])); }

      double Evaluateg1n_Twist3(double *x, double *p) { return(g1n_Twist3(x[0], p[0])); }
      double Evaluatexg1n_Twist3(double *x, double *p) { return(x[0]*g1n_Twist3(x[0], p[0])); }
      double Evaluatex2g1n_Twist3(double *x, double *p) { return(x[0]*x[0]*g1n_Twist3(x[0], p[0])); }

      double Evaluateg1n_Twist4(double *x, double *p) { return(g1n_Twist4(x[0], p[0])); }
      double Evaluatexg1n_Twist4(double *x, double *p) { return(x[0]*g1n_Twist4(x[0], p[0])); }
      double Evaluatex2g1n_Twist4(double *x, double *p) { return(x[0]*x[0]*g1n_Twist4(x[0], p[0])); }

      double Evaluatexg1n_Twist23(double *x, double *p) { return(x[0]*(g1n_Twist2(x[0], p[0])+g1n_Twist3(x[0], p[0]) )); }
      double Evaluatex2g1n_Twist23(double *x, double *p) { return(x[0]*x[0]*(g1n_Twist2(x[0], p[0])+g1n_Twist3(x[0], p[0]) )); }

      double Evaluatexg1n_Twist234(double *x, double *p) { return(x[0]*(g1n_Twist2(x[0], p[0])+g1n_Twist3(x[0], p[0])+g1n_Twist4(x[0], p[0]) )); }
      double Evaluatex2g1n_Twist234(double *x, double *p) { return(x[0]*x[0]*(g1n_Twist2(x[0], p[0])+g1n_Twist3(x[0], p[0])+g1n_Twist4(x[0], p[0]) )); }

      virtual double Evaluated2n_tilde(double *Q2, double *p){ return( d2n_tilde(Q2[0])); }

      double EvaluateGamma1_n(double *x, double *p) { return(Gamma1_n(x[0],p[0],p[1])); }
      double EvaluateGamma2_n(double *x, double *p) { return(Gamma2_n(x[0],p[0],p[1])); }

      double EvaluateGamma1_p_minus_n(double *x, double *p) { return(Gamma1_p_minus_n(x[0],p[0],p[1])); }

      // --------------
      // deuteron

      // --------------
      // 
      double Evaluateg2p(double *x, double *p) { return(g2p(x[0], p[0])); }
      double Evaluatexg2p(double *x, double *p) { return(xg2p(x[0], p[0])); }
      double Evaluatex2g2p(double *x, double *p) { return(x2g2p(x[0], p[0])); }

      double Evaluateg2pWW(double *x, double *p) { return(g2pWW(x[0], p[0])); }
      double Evaluatexg2pWW(double *x, double *p) { return(xg2pWW(x[0], p[0])); }
      double Evaluatex2g2pWW(double *x, double *p) { return(x2g2pWW(x[0], p[0])); }

      double Evaluateg2pbar(double *x, double *p) { return(g2pbar(x[0], p[0])); }
      double Evaluatexg2pbar(double *x, double *p) { return(x[0]*g2pbar(x[0], p[0])); }
      double Evaluatex2g2pbar(double *x, double *p) { return(x[0]*x[0]*g2pbar(x[0], p[0])); }

      double Evaluateg1n(double *x, double *p) { return(g1n(x[0], p[0])); }

      double Evaluateg2n(double *x, double *p) { return(g2n(x[0], p[0])); }

      double Evaluateg2nWW(double *x, double *p) { return(g2nWW(x[0], p[0])); }

      double Evaluateg1d(double *x, double *p) { return(g1d(x[0], p[0])); }
      double Evaluateg2d(double *x, double *p) { return(g2d(x[0], p[0])); }

      double Evaluateg1He3(double *x, double *p) { return(g1He3(x[0], p[0])); }
      double Evaluateg2He3(double *x, double *p) { return(g2He3(x[0], p[0])); }

      double Evaluatexg1n(double *x, double *p) { return(xg1n(x[0], p[0])); }
      double Evaluatexg2n(double *x, double *p) { return(xg2n(x[0], p[0])); }
      double Evaluatexg2nWW(double *x, double *p) { return(xg2nWW(x[0], p[0])); }
      double Evaluatexg1d(double *x, double *p) { return(xg1d(x[0], p[0])); }
      double Evaluatexg2d(double *x, double *p) { return(xg2d(x[0], p[0])); }
      double Evaluatexg1He3(double *x, double *p) { return(xg1He3(x[0], p[0])); }
      double Evaluatexg2He3(double *x, double *p) { return(xg2He3(x[0], p[0])); }
      double Evaluatex2g1n(double *x, double *p) { return(x2g1n(x[0], p[0])); }
      double Evaluatex2g2n(double *x, double *p) { return(x2g2n(x[0], p[0])); }
      double Evaluatex2g2nWW(double *x, double *p) { return(x2g2nWW(x[0], p[0])); }
      double Evaluatex2g1d(double *x, double *p) { return(x2g1d(x[0], p[0])); }
      double Evaluatex2g2d(double *x, double *p) { return(x2g2d(x[0], p[0])); }
      double Evaluatex2g1He3(double *x, double *p) { return(x2g1He3(x[0], p[0])); }
      double Evaluatex2g2He3(double *x, double *p) { return(x2g2He3(x[0], p[0])); }

      /** Use with TF2. The arguments are x=x[0], Q^2=x[1] */
      double Evaluate2Dxg1p(double *x, double * /*p*/) { return(xg1p(x[0], x[1])); }
      double Evaluate2Dxg2p(double *x, double * /*p*/) { return(xg2p(x[0], x[1])); }
      double Evaluate2Dg1p(double *x, double * /*p*/) { return(g1p(x[0], x[1])); }
      double Evaluate2Dg2p(double *x, double * /*p*/) { return(g2p(x[0], x[1])); }
      double Evaluate2Dg2pWW(double *x, double * /*p*/) { return(g2pWW(x[0], x[1])); }

      // becuase cint can be dumb:
      TF1 * GetFunction1();
      TF1 * GetFunction2();
      TF1 * GetFunction3();
      TF1 * GetFunction4();

      //@}

      /** Fills histogram vs X.
       *
       */
      void GetValues(TObject *obj, double Q2, StructureFunctionBase::PolarizedSFType = StructureFunctionBase::kg1p );
      /** Get error band.
       *  Argument obj is assumed to be a TH1. 
       */ 
      void GetErrorBand(TObject *obj, double Q2, StructureFunctionBase::PolarizedSFType = StructureFunctionBase::kg1p );

      ClassDef(PolarizedStructureFunctions, 2)
};
}}

#endif

