#ifndef INSANE_PHYSICS_SFBase_HH
#define INSANE_PHYSICS_SFBase_HH 1

#include "TNamed.h"
#include "TAttLine.h"
#include "TAttMarker.h"
#include "TAttFill.h"
#include <array>
#include <string>
#include <tuple>
#include "Physics.h"

namespace insane {
  using namespace units;
  namespace physics {

    class SFBase : public TNamed , public virtual TAttLine, public virtual TAttFill, public virtual TAttMarker {


      protected:
        std::string      fLabel;

        // Storage of the x and Q2 values last used to compute the structure function
        // indexed by enum class insane::physics::SF
        mutable std::array<std::array<double, NStructureFunctions>, NNuclei> fx_values;
        mutable std::array<std::array<double, NStructureFunctions>, NNuclei> fQ2_values;
        mutable std::array<std::array<double, NStructureFunctions>, NNuclei> fValues;
        mutable std::array<std::array<double, NStructureFunctions>, NNuclei> fUncertainties;


      public :

        SFBase();
        virtual ~SFBase();

        void        SetLabel(const char * l){ fLabel = l;}
        const char* GetLabel() const { return fLabel.c_str(); }

        double GetXBjorken(std::tuple<SF,Nuclei> sf) const;
        double GetQSquared(std::tuple<SF,Nuclei> sf) const;

        bool   IsComputed( std::tuple<SF,Nuclei> sf, double x, double Q2) const;

        void Reset();

        /** Calculate and return distribution value.
         * Returns the current value for flavor f but checks that the 
         * distributions have already been calculated at (x,Q2). 
         * It uses IsComputed(x,Q2) to do this check.
         */
        virtual double Get(double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t = Twist::All, OPELimit l = OPELimit::MassiveTarget) const ;
        //virtual double  Get(std::tuple<SF,Nuclei> sf, double x, double Q2) const ;

        /** Get current distribution value.
         * Note: this method should only be used to get the stored values 
         * after Calculate(x,Q2) or Get(f,x,Q2) has been used at the desired (x,Q2).
         */ 
        double  Get(std::tuple<SF,Nuclei> sf) const ;

        /** Virtual method should get all values of SFs. This also sets
         *  internal values of fx and fQsquared for use by IsComputed()
         */
        virtual double  Calculate    (double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t = Twist::All, OPELimit l = OPELimit::MassiveTarget) const {return 0.0;}
        virtual double  Uncertainties(double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t = Twist::All, OPELimit l = OPELimit::MassiveTarget) const {return 0.0;}

        ClassDef(SFBase,1)
    };

  }
}
#endif

