#ifndef F1F209STRUCTUREFUNCTIONS_H
#define F1F209STRUCTUREFUNCTIONS_H 2 

#include <cstdlib> 
#include <iostream> 
#include <iomanip> 
#include <fstream> 
#include "PhysicalConstants.h"
#include "FortranWrappers.h"
#include "StructureFunctions.h"
namespace insane {
  namespace physics {

    /** Concrete imp of F1F209 structure functions F1 and F2.
     *  By default it includes the contribution the quasi-elastic 
     *  peak. This can be turned off by SetQEPeak(false). 
     *
     * \ingroup structurefunctions
     */
    class F1F209StructureFunctions : public StructureFunctions {

      private: 
        char     fType;                ///< Deprecated  
        Bool_t   fModifiedVersion;     ///< modified version for 3He...
        double fEs;                  ///< Beam energy (GeV) ? is it really? Why? -whit
        Bool_t   fIsFull;              ///< QE + inelastic: supercedes IncludesQE  
        Bool_t   fIsQEOnly;            ///< QE only                 
        Bool_t   fIsInelasticOnly;     ///< Inelastic only                 

        /** Handy function. Note i=0->R, i=1->F1, i=2->F2.*/
        double GetNuclearSF(double x, double Q2, double z, double a, Int_t i =2); 

      public:
        F1F209StructureFunctions(); 
        virtual ~F1F209StructureFunctions(); 

        // Returns F2 per nucleus (not per nucleon)
        virtual double F2Nuclear(double x,double Qsq,double Z, double A);
        virtual double F1Nuclear(double x,double Qsq,double Z, double A);

        void    SetQEPeak(Bool_t q = true) { fIsFull = true;}
        void    DoQEOnly(Bool_t q = true)  { 
          fIsQEOnly = q;
          if(fIsQEOnly){ 
            fIsFull          = false; 
            fIsInelasticOnly = false; 
          }
          std::cout << "[F1F209StructureFunctions]: Only QE component will be used." << std::endl;
        }  
        void DoInelasticOnly(Bool_t q = true) { 
          fIsInelasticOnly = q; 
          if(fIsInelasticOnly){
            fIsFull          = false; 
            fIsQEOnly        = false; 
          }
          std::cout << "[F1F209StructureFunctions]: Only inelastic component will be used." << std::endl;
        }  
        void DoFullCalculation(Bool_t q = true) { 
          fIsFull = q;
          if(fIsFull){
            fIsQEOnly        = false;
            fIsInelasticOnly = false; 
          } 
          std::cout << "[F1F209StructureFunctions]: QE and inelastic component will be used." << std::endl;
        }  

        virtual double F1p(double,double); 
        virtual double F1n(double,double); 
        virtual double F1d(double,double); 
        virtual double F1He3(double,double); 
        virtual double F2p(double,double); 
        virtual double F2n(double,double); 
        virtual double F2d(double,double); 
        virtual double F2He3(double,double); 

        virtual double R(double, double); 

        void SetBeamEnergy(double E){fEs = E;} 

        /**Use the modified version of F1F209. 
         * Set fType to 'y' to use. 
         * NOTE: You MUST set the beam energy if you want to use this!  
         */
        void UseModifiedModel(char ans){
          fType = ans; 
          std::cout << "[F1F209StructureFunctions]: Now using the modified model." << std::endl; 
        }


        ClassDef(F1F209StructureFunctions,3)
    };

    using DefaultStructureFunctions = F1F209StructureFunctions;

    /** Concrete imp of F1F209 QUASI-ELASTIC structure functions F1 and F2.
     *
     *
     * \ingroup structurefunctions
     */

    class F1F209QuasiElasticStructureFunctions : public StructureFunctions {

      private: 
        char     fType;                /// Deprecated  
        double   fA,fZ,fN;             /// Deprecated 
        double fEs;                  /// Beam energy (GeV) 
        double fF1,fF2,fR,fM;

        void GetSFs(double,double); 

      public:
        F1F209QuasiElasticStructureFunctions(); 
        virtual ~F1F209QuasiElasticStructureFunctions(); 

        // Returns F2 per nucleus (not per nucleon)
        virtual double F2Nuclear(double x,double Qsq,double Z, double A);
        virtual double F1Nuclear(double x,double Qsq,double Z, double A);

        virtual double F1p(double,double); 
        virtual double F1n(double,double); 
        virtual double F1d(double,double); 
        virtual double F1He3(double,double); 
        virtual double F2p(double,double); 
        virtual double F2n(double,double); 
        virtual double F2d(double,double); 
        virtual double F2He3(double,double); 

        void SetBeamEnergy(double E){fEs = E;} 

        /**Use the modified version of F1F209. 
         * Set fType to 'y' to use. 
         * NOTE: You MUST set the beam energy if you want to use this!  
         */
        void UseModifiedModel(char ans){
          fType = ans; 
          std::cout << "[F1F209QuasiElasticStructureFunctions]: Now using the modified model." << std::endl; 
        }

        ClassDef(F1F209QuasiElasticStructureFunctions,1)
    };
  }
}
#endif 

