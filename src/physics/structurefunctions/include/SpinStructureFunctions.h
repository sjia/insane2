#ifndef insane_physics_SpinStructureFunctions_HH 
#define insane_physics_SpinStructureFunctions_HH 

#include "TNamed.h"
#include "TAttLine.h"
#include "TAttMarker.h"
#include "TAttFill.h"
#include "Physics.h"

namespace insane {
  using namespace units;
  namespace physics {

    /** Spin Structure Functions interface.
     *  At this level an implementation not involving the pdfs,
     *  e.g., an empircal fit, will ignore the twist argument.
     *  The implementation SSFsFromPDFs will not ignore the twist argument. 
     */
    class SpinStructureFunctions : public TNamed , public virtual TAttLine, public virtual TAttFill, public virtual TAttMarker {

      protected:
        std::string      fLabel;
        static SpinStructureFunctions* fgSpinStructureFunctions;

        // Storage of the x and Q2 values last used to compute the structure function
        // indexed by enum class insane::physics::SF
        mutable std::array<std::array<double, NStructureFunctions>, NNuclei> fx_values;
        mutable std::array<std::array<double, NStructureFunctions>, NNuclei> fQ2_values;
        mutable std::array<std::array<double, NStructureFunctions>, NNuclei> fValues;
        mutable std::array<std::array<double, NStructureFunctions>, NNuclei> fUncertainties;

      public:
        SpinStructureFunctions();
        virtual ~SpinStructureFunctions();

        static SpinStructureFunctions* GetSpinStructureFunctions() {return fgSpinStructureFunctions;}

        void        SetLabel(const char * l){ fLabel = l;}
        const char* GetLabel() const { return fLabel.c_str(); }

        double GetXBjorken(std::tuple<SF,Nuclei> sf) const;
        double GetQSquared(std::tuple<SF,Nuclei> sf) const;

        bool   IsComputed( std::tuple<SF,Nuclei> sf, double x, double Q2) const;

        void Reset();

        /** Calculate and return distribution value.
         * Returns the current value for flavor f but checks that the 
         * distributions have already been calculated at (x,Q2). 
         * It uses IsComputed(x,Q2) to do this check.
         */
        virtual double Get(double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t = Twist::All, OPELimit l = OPELimit::MassiveTarget) const ;

        /** Get current distribution value.
         * Note: this method should only be used to get the stored values 
         * after Calculate(x,Q2) or Get(f,x,Q2) has been used at the desired (x,Q2).
         */ 
        double  Get(std::tuple<SF,Nuclei> sf) const ;

        /** Virtual method should get all values of SFs. This also sets
         *  internal values of fx and fQsquared for use by IsComputed()
         */
        virtual double  Calculate    (double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t = Twist::All, OPELimit l = OPELimit::MassiveTarget) const {return 0.0;}
        virtual double  Uncertainties(double x, double Q2, std::tuple<SF,Nuclei> sf, Twist t = Twist::All, OPELimit l = OPELimit::MassiveTarget) const {return 0.0;}

        virtual double g1(    double x, double Q2, Nuclei target, Twist t = Twist::All) const;
        virtual double g2(    double x, double Q2, Nuclei target, Twist t = Twist::All) const;
        virtual double g1_TMC(double x, double Q2, Nuclei target, Twist t = Twist::All) const;
        virtual double g2_TMC(double x, double Q2, Nuclei target, Twist t = Twist::All) const;

        virtual double g2_WW(double x, double Q2, Nuclei target) const;
        virtual double g1_BT(double x, double Q2, Nuclei target) const;

        //virtual double g1p_Twist2(double x, double Q2) const;
        //virtual double g2p_Twist2(double x, double Q2) const;
        //virtual double g1n_Twist2(double x, double Q2) const;
        //virtual double g2n_Twist2(double x, double Q2) const;

        virtual double g1p(double x, double Q2) const     { return 0.0; }
        virtual double g2p(double x, double Q2) const     { return 0.0; }
        virtual double g1n(double x, double Q2) const     { return 0.0; }
        virtual double g2n(double x, double Q2) const     { return 0.0; }
        virtual double g1p_TMC(double x, double Q2) const { return 0.0; }
        virtual double g2p_TMC(double x, double Q2) const { return 0.0; }
        virtual double g1n_TMC(double x, double Q2) const { return 0.0; }
        virtual double g2n_TMC(double x, double Q2) const { return 0.0; }

        virtual double g2pWW(   double x, double Q2) { return 0.0; }

        /** Mellin moments.
         *  \f$ g_i^{(n)} = \int dx g_i^{n-1} \f$
         */
        double Mellin_g1p(Int_t n, double Q2,double x1 = 0.01,double x2 = 0.99);
        double Mellin_g2p(Int_t n, double Q2,double x1 = 0.01,double x2 = 0.99);
        double Mellin_g1p_TMC(Int_t n, double Q2,double x1 = 0.01,double x2 = 0.99);
        double Mellin_g2p_TMC(Int_t n, double Q2,double x1 = 0.01,double x2 = 0.99);

        double Mellin_mn_p( int m, int n, double Q2, double x1 = 0.01,double x2 = 0.99);
        double Mellin_mn_TMC_p( int m, int n, double Q2, double x1 = 0.01,double x2 = 0.99);

        //virtual double d2p_WW(        double Q2,double x1 = 0.01,double x2 = 0.99) ;
        //virtual double d2p_Twist2_TMC(double Q2,double x1 = 0.01,double x2 = 0.99) ;
        //virtual double d2p_Twist3(    double Q2,double x1 = 0.01,double x2 = 0.99) ;
        //virtual double d2p_Twist3_TMC(double Q2,double x1 = 0.01,double x2 = 0.99) ;

        double d2p_tilde(double Q2,double x1 = 0.01,double x2 = 0.99) ;
        double d2n_tilde(double Q2,double x1 = 0.01,double x2 = 0.99) ;

        double d2p_tilde_TMC(double Q2,double x1 = 0.01,double x2 = 0.99) ;
        double d2n_tilde_TMC(double Q2,double x1 = 0.01,double x2 = 0.99) ;

        double M1n_p(             Int_t n, double Q2,double x1 = 0.01,double x2 = 0.99);
        double M1nIntegrand_p(    Int_t n, double x, double Q2);
        double M2n_p(             Int_t n, double Q2,double x1 = 0.01,double x2 = 0.99);
        double M2nIntegrand_p(    Int_t n, double x, double Q2);

        double M1n_TMC_p(         Int_t n, double Q2,double x1 = 0.01,double x2 = 0.99);
        double M1nIntegrand_TMC_p(Int_t n, double x, double Q2);
        double M2n_TMC_p(         Int_t n, double Q2,double x1 = 0.01,double x2 = 0.99);
        double M2nIntegrand_TMC_p(Int_t n, double x, double Q2);

        double M1n_n(             Int_t n, double Q2,double x1 = 0.01,double x2 = 0.99);
        double M1nIntegrand_n(    Int_t n, double x, double Q2);
        double M2n_n(             Int_t n, double Q2,double x1 = 0.01,double x2 = 0.99);
        double M2nIntegrand_n(    Int_t n, double x, double Q2);

        double M1n_TMC_n(         Int_t n, double Q2,double x1 = 0.01,double x2 = 0.99);
        double M1nIntegrand_TMC_n(Int_t n, double x, double Q2);
        double M2n_TMC_n(         Int_t n, double Q2,double x1 = 0.01,double x2 = 0.99);
        double M2nIntegrand_TMC_n(Int_t n, double x, double Q2);

      //@{ 
      /** Nachtmann moments M_n^m */
      double Mmn_p( int m, int n, double Q2, double x1 = 0.01,double x2 = 0.99);
      double Mmn_n( int m, int n, double Q2, double x1 = 0.01,double x2 = 0.99 );

      double Mmn_TMC_p( int m, int n, double Q2, double x1 = 0.01,double x2 = 0.99);
      double Mmn_TMC_n( int m, int n, double Q2, double x1 = 0.01,double x2 = 0.99);
      //@} 


        ClassDef(SpinStructureFunctions,1)
    };
  }
}

#endif
