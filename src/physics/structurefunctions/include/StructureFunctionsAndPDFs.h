#ifndef PDFs_HH
#define PDFs_HH 1

/*! This header can be used to add all the PDF models in one shot.
 */

// unpolarized pdfs
//#include "LHAPDFUnpolarizedPDFs.h"
#include "CTEQ6UnpolarizedPDFs.h"

// polarized pdfs
#include "AAC08PolarizedPDFs.h"
#include "BBPolarizedPDFs.h"
#include "DSSVPolarizedPDFss.h"
#include "DNS2005PolarizedPDFs.h"
#include "GSPolarizedPDFs.h"
//#include "LSS2006PolarizedPDFs.h" // currently broken

// Unpolarized SFs
#include "StructureFunctions.h"
#include "F1F209StructureFunctions.h"
//#include "LHAPDFStructureFunctions.h"
#include "NMC95StructureFunctions.h"

// Polarized SFs
#include "StructureFunctionsFromPDFs.h"


#endif
