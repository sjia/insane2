#ifndef insane_physics_CompositeSSFs_HH
#define insane_physics_CompositeSSFs_HH 1

#include "SpinStructureFunctions.h"
#include "Physics.h"
//#include "MAID_VPACs.h"
#include "SSFsFromVPACs.h"
#include "SSFsFromPDFs.h"
#include "Stat2015_PPDFs.h"

namespace insane {
  namespace physics {

    class  CompositeSSFs : public SpinStructureFunctions { 
      protected:
        SSFsFromPDFs<Stat2015_PPDFs>   fAsf;
        SSFsFromPDFs<Stat2015_PPDFs>   fBsf;
        //SSFsFromVPACs<MAID_VPACs>      fBsf;

      public:
        CompositeSSFs();
        virtual ~CompositeSSFs();
       
        virtual double g1p(double x, double Q2) const ;
        virtual double g2p(double x, double Q2) const ;
        virtual double g1n(double x, double Q2) const ;
        virtual double g2n(double x, double Q2) const ;

        //virtual double R(double x, double Q2, Nuclei target, Twist t = Twist::All) const;

        virtual double GetWeight(int iSF, double x, double Q2) const;

      ClassDef(CompositeSSFs,1)
    };

  }
}

#endif

