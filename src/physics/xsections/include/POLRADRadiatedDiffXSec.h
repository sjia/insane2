#ifndef POLRADRadiatedDiffXSec_HH
#define POLRADRadiatedDiffXSec_HH 1

#include "POLRADBornDiffXSec.h"
namespace insane {
namespace physics {

/** Cross Section with radiative corrections.
 *  Full internal calculation. 
 *
 * \ingroup inclusiveXSec
 */
class POLRADRadiatedDiffXSec: public POLRADBornDiffXSec {
   public:
      POLRADRadiatedDiffXSec();
      virtual ~POLRADRadiatedDiffXSec();
      virtual POLRADRadiatedDiffXSec*  Clone(const char * newname) const ;
      virtual POLRADRadiatedDiffXSec*  Clone() const { return( Clone("") ); } 

      virtual Double_t EvaluateXSec(const Double_t *x) const ; 

      ClassDef(POLRADRadiatedDiffXSec,1)
};
}}

#endif

