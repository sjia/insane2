#ifndef insane_physics_DVCS_HH
#define insane_physics_DVCS_HH 1

#include <complex>

/** @brief Useful collection of DVCS and GPD related functions.
 *
 *  DVCS.h is a collection of useful functions for DVCS such as:
 *   - Bethe-Heitler (BH) Amplitudes
 *   - Fourier harmonics (BH, DVCS, I)
 *   - Kinmatic coefficents
 *   - Compton form factors (CFFs)
 *   - GPD relations
 *
 *
 *  References:
 *  1. http://inspirehep.net/record/537446
 *     "Twist three analysis of photon electroproduction off pion"
 *     "Belitsky, Mueller, Kirchner, Schafer"
 *  2. http://inspirehep.net/record/796841
 *     "Refined analysis of photon leptoproduction off spinless target"
 *  3. http://inspirehep.net/record/679716
 *     "Unraveling hadron structure with generalized parton distributions"
 *     "Belitsky, Radyushkin"
 */
namespace insane {

  namespace physics {

    /** DVCS variables. 
     *
     */
    struct DVCS_KinematicVariables {
      double xB;
      double Q2;
      double t;
      double phi;
      double xi;
      double E0;
      double M = 0.938;
      double y;
      double nu;
      double eps;
      double K;
      double J;
    };

    double epsilon(    double Q2, double xB, double M=0.938);
    double Delta2_min( double Q2, double xB, double M=0.938) ;
    double Delta2_min2(double Q2, double xB, double M=0.938);
    double Delta2_max( double Q2, double xB, double M=0.938);
    double Delta2_perp(double xi, double Delta2, double D2m);

    double y_max(  double eps);
    double xi_BKM( double xB, double Q2, double Delta2);
    double eta_BKM(double xi, double Q2, double Delta2);

    //double F3_plus(double x, double xi);
    //double F3_minus(double x, double xi);

    /** @brief Form Factors and Compton Form Factors. */
    struct DVCS_FormFactors {
      double               F1     = 0.0;
      double               F2     = 0.0;
      std::complex<double> H      = 0.0;
      std::complex<double> E      = 0.0;
      std::complex<double> Htilde = 0.0;
      std::complex<double> Etilde = 0.0;
      std::complex<double> HT;
      std::complex<double> ET;
      std::complex<double> HTtilde;
      std::complex<double> ETtilde;
      std::complex<double> Heff;
      std::complex<double> Eeff;
      std::complex<double> Hefftilde;
      std::complex<double> Eefftilde;
    };

    /** @brief Form Factors and Compton Form Factors. */
    struct DVCS_CFFs {
      double               F1     = 0.0;
      double               F2     = 0.0;
      std::complex<double> H      = 0.0;
      std::complex<double> E      = 0.0;
      std::complex<double> Htilde = 0.0;
      std::complex<double> Etilde = 0.0;
    };

    //------------------------------------------
    // Propagators
    //------------------------------------------
    double K_DVCS( double E0, double Q2, double xB, double Delta2, double M=0.938);
    double K_DVCS2(double E0, double Q2, double xB, double Delta2, double M=0.938);
    double J_DVCS( double E0, double Q2, double xB, double Delta2, double M=0.938);

    double K_DVCS( const DVCS_KinematicVariables& vars);
    double K_DVCS2(const DVCS_KinematicVariables& vars);
    double J_DVCS( const DVCS_KinematicVariables& vars);

    double P1(double E0, double Q2, double xB, double Delta2, double phi, double M=0.938);
    double P2(double E0, double Q2, double xB, double Delta2, double phi, double M=0.938);
    double P1(const DVCS_KinematicVariables& vars);
    double P2(const DVCS_KinematicVariables& vars);

    //------------------------------------------
    // Fourier Harmonics
    //------------------------------------------
    
    /** @name Spin-1/2 target
     *  Fourier Harmonics for spin-1/2 target.
     */
    ///@{
    double c0_BH_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);
    double c1_BH_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);
    double c2_BH_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);

    double c0_DVCS_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& F);
    double c1_DVCS_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& F);
    double s1_DVCS_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& F);
    double c2_DVCS_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& F);
    double s2_DVCS_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& F);

    double c0_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& F);
    double c1_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& F);
    double s1_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);
    double c2_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);
    double s2_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);
    double c3_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);
    double s3_I_unp(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);
    ///@}

    /** @name Spin-0 target
     *  Fourier Harmonics for spin-0 target.
     */
    ///@{
    double c0_BH_spin0(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);
    double c1_BH_spin0(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);
    double c2_BH_spin0(const DVCS_KinematicVariables& vars, const DVCS_FormFactors& ffs);
    ///@}

    //------------------------------------------
    // Angular Harmonics
    //------------------------------------------
    std::complex<double> C_angular_DVCS_unp(  const DVCS_KinematicVariables& vars, const DVCS_CFFs& F, const DVCS_CFFs& Fstar);
    std::complex<double> C_angular_I_unp(     const DVCS_KinematicVariables& vars, const DVCS_CFFs& F);
    std::complex<double> C_angular_I_T_unp(   const DVCS_KinematicVariables& vars, const DVCS_CFFs& F);
    std::complex<double> DeltaC_angular_I_unp(const DVCS_KinematicVariables& vars, const DVCS_CFFs& F);

  }
}

#endif

