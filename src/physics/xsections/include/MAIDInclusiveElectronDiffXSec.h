#ifndef MAIDInclusiveElectronDiffXSec_HH
#define MAIDInclusiveElectronDiffXSec_HH 1

#include "InclusiveDiffXSec.h"
#include "MAIDExclusivePionDiffXSec.h"
#include "TMath.h"
#include "Math/GSLIntegrator.h"
#include "Math/GaussIntegrator.h"
#include "Math/GaussLegendreIntegrator.h"
#include "Math/VirtualIntegrator.h"
#include "Math/Integrator.h"
#include "Math/IntegratorOptions.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/AllIntegrationTypes.h"
#include "Math/GaussIntegrator.h"
#include <Math/IFunction.h>
#include <Math/Functor.h>

namespace insane {
namespace physics {
/** MAID inclusive electron cross section
 *
 *
 * \ingroup inclusiveXSec
 */
class MAIDInclusiveElectronDiffXSec : public InclusiveDiffXSec {

   private:

       ROOT::Math::IntegrationMultiDim::Type fIntType;
       double fAbsErr        ;
       double fRelErr        ;
       unsigned int fNcalls  ;
       int fRule             ;

   public:

      MAIDInclusiveElectronDiffXSec(const char * nucleon = "p");
      virtual ~MAIDInclusiveElectronDiffXSec();
      virtual MAIDInclusiveElectronDiffXSec*  Clone(const char * newname) const {
         std::cout << "MAIDInclusiveElectronDiffXSec::Clone()\n";
         auto * copy = new MAIDInclusiveElectronDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual MAIDInclusiveElectronDiffXSec*  Clone() const { return( Clone("") ); } 

      Double_t   EvaluateXSec(const Double_t *x) const ;
      Double_t * GetDependentVariables(const Double_t * x) const ;
      void       InitializePhaseSpaceVariables();


      class MAIDXSec_5Fold_theta : public ROOT::Math::IBaseFunctionOneDim {
         public:
            mutable MAIDExclusivePionDiffXSec * fXSec;
            double DoEval(double x) const { return x*x; }
            unsigned int NDim() const { return 1; }
            ROOT::Math::IBaseFunctionOneDim* Clone() const { return new MAIDXSec_5Fold_theta(); }
            ClassDef(MAIDXSec_5Fold_theta,1)
      };
      class MAIDXSec_5Fold_phi : public ROOT::Math::IBaseFunctionOneDim {
         public:
            mutable MAIDExclusivePionDiffXSec * fXSec;
            double DoEval(double x) const { return x*x; }
            unsigned int NDim() const { return 1; }
            ROOT::Math::IBaseFunctionOneDim* Clone() const { return new MAIDXSec_5Fold_phi(); }
            ClassDef(MAIDXSec_5Fold_phi,1)
      };

      /** IRT 2 dimensional integrand in theta and phi. */
      class MAIDXSec_5Fold_2D_integrand : public ROOT::Math::IBaseFunctionMultiDim {
         public:
            mutable MAIDExclusivePionDiffXSec * fXSec;
            mutable Double_t fArgs[9]; 
            double DoEval(const double* x) const {
               fArgs[3] = x[0];
               fArgs[4] = x[1];
               Double_t res = fXSec->EvaluateXSec(fXSec->GetDependentVariables(fArgs));
               //std::cout << " res(5-fold)  = " << res << std::endl;
               //std::cout << " theta_pi     = " <<  x[0]/degree << std::endl;
               //std::cout << " phi_pi       = " <<  x[1]/degree << std::endl;
               return res;
            }
            unsigned int NDim() const { return 2; }
            ROOT::Math::IBaseFunctionMultiDim* Clone() const { return new MAIDXSec_5Fold_2D_integrand(); } 
            ClassDef(MAIDXSec_5Fold_2D_integrand,1)
      }; 

       MAIDXSec_5Fold_2D_integrand   fSig0;
       MAIDXSec_5Fold_2D_integrand   fSig1;
       MAIDExclusivePionDiffXSec    *fXSec0;
       MAIDExclusivePionDiffXSec    *fXSec1;

ClassDef(MAIDInclusiveElectronDiffXSec,1)
};
}}

#endif

