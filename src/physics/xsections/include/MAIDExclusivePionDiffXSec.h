#ifndef MAIDExclusivePionDiffXSec_H 
#define MAIDExclusivePionDiffXSec_H 1

#include <cstdlib> 
#include <iostream> 
#include <cmath> 
#include <vector> 
#include "TVector3.h"
#include "TRotation.h"
#include "TString.h"
#include "MAIDPolarizedKinematicKey.h"
#include "ExclusiveDiffXSec.h"

namespace insane {
namespace physics {

/** MAID cross section for (polarized target) threshold (exlusive) pion production.
 * 
 * Paper reference: J. Phys. G: Nucl. Part. Phys 18, 449 (1992)  
 * For the reaction e N --> e' N' pi, where the user specifies pi and N'. 
 * Uses data from the MAID 2007 calculations: polarized target.
 * 
 *
 * \ingroup exclusiveXSec
 */ 
class MAIDExclusivePionDiffXSec: public ExclusiveDiffXSec{

   private:
      mutable TVector3        fP_t_cm;
      mutable TVector3        fA_t;
      mutable TVector3        fA_et;

      mutable TVector3        fX_axis;
      mutable TVector3        fY_axis;
      mutable TVector3        fZ_axis;
      mutable TVector3        fYScat_axis; 
      mutable TVector3        fk1_CM;
      mutable TVector3        fk2_CM;
      mutable TVector3        fP1_CM;
      mutable TVector3        fP2_CM;
      mutable TVector3        fkpi_CM;
      mutable TVector3        fq_CM;
      mutable TVector3        fPT_CM;
      mutable TVector3        fBoostToCM;
      mutable TLorentzVector  f4Veck1_CM;
      mutable TLorentzVector  f4Veck2_CM;
      mutable TLorentzVector  f4Vecq_CM;
      mutable TLorentzVector  f4Veckpi_CM;
      mutable TLorentzVector  f4VecP1_CM;
      mutable TLorentzVector  f4VecP2_CM;
      mutable TLorentzVector  f4VecPT_CM;
      mutable TRotation       fRotateToq;

      mutable Double_t                                      x_eval[5];
      mutable std::vector<Double_t>                                      xd;
      mutable std::vector<std::vector<Double_t> >                             y_lim;
      mutable std::vector<std::vector<Int_t> >                             yi_lim;
      //std::vector<std::vector<std::vector<std::vector<std::vector<Double_t> > > > >  c_5;
      // work around
      mutable std::vector<std::vector<std::vector<std::vector<Double_t> > > >           c_5_4[5];
      mutable std::vector<std::vector<std::vector<std::vector<Double_t> > > >      *    c_5;
      mutable std::vector<std::vector<std::vector<std::vector<Double_t> > > >           c_4;
      mutable std::vector<std::vector<std::vector<Double_t> > >                    c_3;
      mutable std::vector<std::vector<Double_t> >                             c_2;
      mutable std::vector<Double_t>                                      c_1;
      mutable Double_t                                              c_0;

      TString  fFileName,fConventionName,fprefix; 
      TString  fPion,fNucleon,frxn; 
      bool     fDebug;
      Int_t    fConvention;
      Int_t    fNumberOfPoints;  
      Double_t fPx,fPz;

      mutable TOrdCollection fGridData;
      mutable MAIDPolarizedKinematicKey *fKineKey;
      mutable std::vector<Double_t> fEprime,fTheta,fPhi,fepsilon,fNu,fW,fQ2; 
      mutable std::vector<Double_t> fSigma_0,fA_e,fA_1,fA_2,fA_3,fA_e1,fA_e2,fA_e3,fXSf;;  
     
      void     FillHyperCubeValues(Double_t phi_e = 0.0) const ;
      Double_t GetInterpolation() const ;
      Double_t EvaluateVirtualPhotonFlux(Double_t,Double_t) const; 
      Double_t EvaluateFluxFactor(Double_t,Double_t,Double_t) const; 
      void     BinarySearch(std::vector<Double_t> *,Double_t,Int_t &,Int_t &) const;
      Int_t    GetGridIndex(Int_t ieps, Int_t iQ2, Int_t iW, Int_t iTh, Int_t iPh) const ;

   protected:

      mutable TRotation labToScat;
      mutable TRotation scatToLab;
      mutable TVector3  betaToCM; 
      mutable TVector3  betaToLAB;

      mutable TVector3  fPT_scat;  // Target Polarization vector in scat plane coords
      mutable TVector3  fA_target;
      mutable TVector3  fA_beam_target;

      mutable TLorentzVector f4Veck1_LAB;
      mutable TLorentzVector f4Veck2_LAB;
      mutable TLorentzVector f4Vecq_LAB;
      mutable TLorentzVector f4VecP1_LAB;
      mutable TLorentzVector f4VecP2_LAB;
      mutable TLorentzVector f4Veckpi_LAB;

      mutable TVector3 fk1_LAB;
      mutable TVector3 fk2_LAB;
      mutable TVector3 fq_LAB;
      mutable TVector3 fP1_LAB;
      mutable TVector3 fP2_LAB;
      mutable TVector3 fkpi_LAB;

      Double_t  fM_1;
      Double_t  fM_2;
      Double_t  fM_pi;

      mutable Double_t  fOmega_th_lab;
      mutable Double_t  fOmega_pi_lab; 
      mutable Double_t  fOmega_pi_cm; 
      mutable Double_t  fTheta_pi_cm; 
      mutable Double_t  fPhi_pi_cm; 
      mutable Double_t  fOmega_pi_cm_over_lab;

      mutable Double_t  fTheta_pi_q_lab; // Angle between pion and q momentum in the lab. 

      TVector3 GetTargetPolInCM(Double_t eps, Double_t Q2, Double_t W2, Double_t phi_e=0.0) const;
      Double_t GetVirtualPhotonCrossSection(MAIDPolarizedKinematicKey* key, Double_t phi_e) const;
 
   public: 

      MAIDExclusivePionDiffXSec(const char * pion = "pi0",const char * nucleon = "p"); 
      virtual ~MAIDExclusivePionDiffXSec();
      //virtual MAIDExclusivePionDiffXSec*  Clone(const char * newname) const {
      //   std::cout << "MAIDExclusivePionDiffXSec::Clone()\n";
      //   MAIDExclusivePionDiffXSec * copy = new MAIDExclusivePionDiffXSec();
      //   (*copy) = (*this);
      //   return copy;
      //}
      //virtual MAIDExclusivePionDiffXSec*  Clone() const { return( Clone("") ); } 

      virtual Double_t   EvaluateXSec(const Double_t *) const; 
      virtual Double_t * GetDependentVariables(const Double_t * ) const;
      virtual void       InitializePhaseSpaceVariables();

      void       ImportData();
      void       DebugMode(bool ans){fDebug = ans;} // doesn't do anything yet...  
      void       Print(Option_t * );  
      void       PrintData();  
      void       PrintParameters();  
      void       Clear(Option_t * ); 
      void       SetVirtualPhotonFluxConvention(Int_t i);
      void       SetReactionChannel(TString pion,TString nucleon);

      //Double_t   GetHelicity() const { return fh; }
      //void       SetHelicity(Double_t h) { 
      //   if(TMath::Abs(h) > 1.0){
      //      Error("SetHelicity","Argument too big: |h|<=1.");
      //   }else{ 
      //      fh = h;
      //   }
      //}
      //TVector3   GetTargetPolarization(){ return fPT; }
      //void       SetTargetPolarization(const TVector3 &P){
      //   fPT = P; 
      //   if(P.Mag() > 1.0) {
      //      Error("SetTargetPolarization","Magnitude too big. Setting to 1.");
      //      fPT.SetMag(1.0);
      //   }
      //}
      //TVector3   fPT;  // Target Polarization vector

      double EnergyDependentXSec(double *x, double *p) {
         Double_t y[5];
         y[0] = x[0];
         y[1] = p[0];
         y[2] = p[1];
         y[3] = p[2];
         y[4] = p[3];
         return(EvaluateXSec(GetDependentVariables(y)));
      }

      double AngleDependentXSec(double *x, double *p) {
         Double_t y[5];
         y[0] = p[0];
         y[1] = x[0];
         y[2] = p[1];
         y[3] = p[2];
         y[4] = p[3];
         return(EvaluateXSec(GetDependentVariables(y)));
      }
      double PhiAngleDependentXSec(double *x, double *p) {
         Double_t y[5];
         y[0] = p[0];
         y[1] = p[1];
         y[2] = x[0];
         y[3] = p[2];
         y[4] = p[3];
         return(EvaluateXSec(GetDependentVariables(y)));
      }
      double PionAngleDependentXSec(double *x, double *p) {
         Double_t y[5];
         y[0] = p[0];
         y[1] = p[1];
         y[2] = p[2];
         y[3] = x[0];
         y[4] = p[3];
         return(EvaluateXSec(GetDependentVariables(y)));
      }
      double PionPhiAngleDependentXSec(double *x, double *p) {
         Double_t y[5];
         y[0] = p[0];
         y[1] = p[1];
         y[2] = p[2];
         y[3] = p[3];
         y[4] = x[0];
         return(EvaluateXSec(GetDependentVariables(y)));
      }
      ClassDef(MAIDExclusivePionDiffXSec,1)
};

}}

#endif 
