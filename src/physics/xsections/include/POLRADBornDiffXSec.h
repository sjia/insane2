#ifndef POLRADBornDiffXSec_HH
#define POLRADBornDiffXSec_HH

#include "POLRADInternalPolarizedDiffXSec.h"
namespace insane {
namespace physics {

/** Born Cross Section.
 *  Used as the base class for all POLRAD cross-sections which use 
 *  the phase space variables E',theta, and phi.
 *
 * \ingroup inclusiveXSec
 */
class POLRADBornDiffXSec: public POLRADInternalPolarizedDiffXSec {

   public:
      POLRADBornDiffXSec();
      virtual ~POLRADBornDiffXSec();
      virtual POLRADBornDiffXSec*  Clone(const char * newname) const {
         std::cout << "POLRADBornDiffXSec::Clone()\n";
         auto * copy = new POLRADBornDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual POLRADBornDiffXSec*  Clone() const { return( Clone("") ); } 
      virtual void InitializePhaseSpaceVariables() ;
      virtual Double_t EvaluateXSec(const Double_t *x) const ; 

      virtual void SetTargetPolarization(const TVector3 &P){
         // make sure to add call to this in override
         GetPOLRAD()->SetPolarizationVectors(fTargetPol,GetHelicity());
         DiffXSec::SetTargetPolarization(P);
      }
      virtual void       SetHelicity(Double_t h) { 
         // make sure to add call to this in override
         GetPOLRAD()->SetHelicity(h);
         DiffXSec::SetHelicity(h);
      }
      virtual void SetPolarizations(Double_t pe, Double_t pt){
         // make sure to add call to this in override
         GetPOLRAD()->SetPolarizations(pe,pt,0.0);
         DiffXSec::SetPolarizations(pe,pt);
      } 

      ClassDef(POLRADBornDiffXSec,1)
};
}}
#endif

