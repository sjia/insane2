#ifndef WiserInclusivePhotoXSec_HH
#define WiserInclusivePhotoXSec_HH

#include "WiserXSection.h"

namespace insane {
namespace physics {
/**  Inclusive photo production of pions, kaons, and nucleons.
 *   This is from David Wiser's thesis in which he fit the data
 *   from a bremsstrahlung beam with this cross section convoluted
 *   with the bremsstrahlung photon spectrum.
 *
 * \ingroup inclusiveXSec
 */
class WiserInclusivePhotoXSec : public InclusiveWiserXSec {

   public:
      WiserInclusivePhotoXSec();
      virtual ~WiserInclusivePhotoXSec();
      virtual WiserInclusivePhotoXSec*  Clone(const char * newname) const {
         std::cout << "WiserInclusivePhotoXSec::Clone()\n";
         auto * copy = new WiserInclusivePhotoXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual WiserInclusivePhotoXSec*  Clone() const { return( Clone("") ); } 

      /** gamma+p --> pi + X */
      //virtual void InitializePhaseSpaceVariables();

      /** Evaluate Cross Section (nbarn/GeV*str).  */
      Double_t EvaluateXSec(const Double_t * x) const ;

   ClassDef(WiserInclusivePhotoXSec,1)
};

/**  Inclusive photo production of pions, kaons, and nucleons.
 *   This is from David Wiser's thesis in which he fit the data
 *   from a bremsstrahlung beam with this cross section convoluted
 *   with the bremsstrahlung photon spectrum.
 *
 * \ingroup inclusiveXSec
 */
class WiserInclusivePhotoXSec2 : public InclusiveWiserXSec {

   public:
      WiserInclusivePhotoXSec2();
      virtual ~WiserInclusivePhotoXSec2();
      virtual WiserInclusivePhotoXSec2*  Clone(const char * newname) const {
         std::cout << "WiserInclusivePhotoXSec2::Clone()\n";
         auto * copy = new WiserInclusivePhotoXSec2();
         (*copy) = (*this);
         return copy;
      }
      virtual WiserInclusivePhotoXSec2*  Clone() const { return( Clone("") ); } 

      /** gamma+p --> pi + X */
      //virtual void InitializePhaseSpaceVariables();

      /** Evaluate Cross Section (nbarn/GeV*str).  */
      Double_t EvaluateXSec(const Double_t * x) const ;

   ClassDef(WiserInclusivePhotoXSec2,1)
};

/** Wiser cross section for arbitrary nuclear target. 
 *
 * \ingroup inclusiveXSec
 */
class PhotoWiserDiffXSec : public CompositeDiffXSec {

   public:
      PhotoWiserDiffXSec();
      virtual ~PhotoWiserDiffXSec();
      virtual PhotoWiserDiffXSec*  Clone(const char * newname) const {
         std::cout << "PhotoWiserDiffXSec::Clone()\n";
         auto * copy = new PhotoWiserDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual PhotoWiserDiffXSec*  Clone() const { return( Clone("") ); } 

      //virtual void InitializePhaseSpaceVariables();

      TParticlePDG * GetParticlePDG() { return ((WiserInclusivePhotoXSec*)fProtonXSec)->GetParticlePDG(); } 
      Int_t          GetParticleType() { return ((WiserInclusivePhotoXSec*)fProtonXSec)->GetParticlePDG()->PdgCode(); }

      Double_t GetRadiationLength(){ 
         return( ((WiserInclusivePhotoXSec*)fProtonXSec)->GetRadiationLength());
      }
      void   SetRadiationLength(Double_t r) { 
         ((WiserInclusivePhotoXSec*)fProtonXSec )->SetRadiationLength(r);
         ((WiserInclusivePhotoXSec*)fNeutronXSec)->SetRadiationLength(r);
      }

      void SetProductionParticleType(Int_t PDGcode, Int_t part = 0) {
         SetParticleType(PDGcode);
         ((WiserInclusivePhotoXSec*)fProtonXSec)->SetProductionParticleType(PDGcode);
         ((WiserInclusivePhotoXSec*)fNeutronXSec)->SetProductionParticleType(PDGcode);
      }

      void SetParticlePDGEncoding(Int_t PDGcode) { SetProductionParticleType(PDGcode); }

      virtual Double_t  EvaluateXSec(const Double_t * x) const ;

   ClassDef(PhotoWiserDiffXSec,1)
};

/** Wiser cross section for arbitrary nuclear target. 
 *
 * \ingroup inclusiveXSec
 */
class PhotoWiserDiffXSec2 : public CompositeDiffXSec {

   public:
      PhotoWiserDiffXSec2();
      virtual ~PhotoWiserDiffXSec2();
      virtual PhotoWiserDiffXSec2*  Clone(const char * newname) const {
         std::cout << "PhotoWiserDiffXSec2::Clone()\n";
         auto * copy = new PhotoWiserDiffXSec2();
         (*copy) = (*this);
         return copy;
      }
      virtual PhotoWiserDiffXSec2*  Clone() const { return( Clone("") ); } 

      //virtual void InitializePhaseSpaceVariables();

      TParticlePDG * GetParticlePDG()  { return ((WiserInclusivePhotoXSec2*)fProtonXSec)->GetParticlePDG(); } 
      Int_t          GetParticleType() { return ((WiserInclusivePhotoXSec2*)fProtonXSec)->GetParticlePDG()->PdgCode(); }

      Double_t GetRadiationLength(){ 
         return( ((WiserInclusivePhotoXSec2*)fProtonXSec)->GetRadiationLength());
      }
      void   SetRadiationLength(Double_t r) { 
         ((WiserInclusivePhotoXSec2*)fProtonXSec )->SetRadiationLength(r);
         ((WiserInclusivePhotoXSec2*)fNeutronXSec)->SetRadiationLength(r);
      }

      virtual void SetProductionParticleType(Int_t PDGcode, Int_t part = 0) {
         SetParticleType(PDGcode);
         ((WiserInclusivePhotoXSec2*)fProtonXSec)->SetProductionParticleType(PDGcode);
         ((WiserInclusivePhotoXSec2*)fNeutronXSec)->SetProductionParticleType(PDGcode);
      }

      void SetParticlePDGEncoding(Int_t PDGcode) { SetProductionParticleType(PDGcode); }

      virtual Double_t  EvaluateXSec(const Double_t * x) const ;

   ClassDef(PhotoWiserDiffXSec2,1)
};
}}
#endif

