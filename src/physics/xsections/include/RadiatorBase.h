#ifndef RadiatorBase_H
#define RadiatorBase_H 1

#include "RADCOR.h"
#include "POLRAD.h"
#include "InclusiveDiffXSec.h"
#include "F1F209eInclusiveDiffXSec.h"
namespace insane {
namespace physics {

template <class T>
class RadiatorBase : public T {

   protected:
      mutable POLRAD * fPOLRAD;          ///< Used to get the (internal) polarized 
      mutable RADCOR * fRADCOR;          ///< Used to get the (internal and external)
      Double_t               fRadLen[2];       ///< [0] before and [1] after scattering

      void CreatePOLRAD() const ;
      void CreateRADCOR() const ;

      Bool_t   fAddRegion4;                                // adds 2d integral over region IV (see Mt 1969)
      Bool_t   fInternalOnly;                              //

   public:
      RadiatorBase();
      virtual ~RadiatorBase();

      POLRAD * GetPOLRAD() const { if(!fPOLRAD) CreatePOLRAD(); return fPOLRAD; }
      RADCOR * GetRADCOR() const { if(!fRADCOR) CreateRADCOR(); return fRADCOR; }

      void SetRegion4(bool v = true) { fAddRegion4 = v; }
      void SetInternalOnly(bool v = true) { fInternalOnly = v; }

      void SetPolarizations(Double_t pe, Double_t pt){
         GetPOLRAD()->SetPolarizations(pe,pt,0.0);
         TVector3 p = this->GetTargetPolarization();
         p.SetMag(pt);
         this->SetTargetPolarization(p);
      } 

      virtual Double_t EvaluateXSec(const Double_t *x) const ;

      virtual Double_t EvaluateBaseXSec(const Double_t *x) const {
         return T::EvaluateXSec(x);
      }

      ClassDef(RadiatorBase,1)
};
//______________________________________________________________________________

template <class T>
RadiatorBase<T>::RadiatorBase(){
   fRadLen[0] = 0.05; 
   fRadLen[1] = 0.05; 
   GetRADCOR()->SetUnpolarizedCrossSection(this); 
   GetRADCOR()->SetRadiationLengths(fRadLen);
}
//______________________________________________________________________________

template <class T>
RadiatorBase<T>::~RadiatorBase(){
}
//______________________________________________________________________________

template <class T>
Double_t RadiatorBase<T>::EvaluateXSec(const Double_t *x) const {
   if (!this->VariablesInPhaseSpace(this->fnDim, x)){
      //std::cout << "[POLRADElasticTailDiffXSec::EvaluateXSec]: Something is wrong!" << std::endl;
      return(0.0);
   }

   Double_t Eprime  = x[0];
   Double_t theta   = x[1];
   Double_t phi     = x[2];
   Double_t Ebeam   = this->GetBeamEnergy();
   Double_t sig_rad = 0.0;
   // Using the POLRAD IRT
   // sig_rad = fDiffXSec0->EvaluateXSec(x);
   // Using the Equiv. Rad. Method
   Double_t sig_rad2 = 0.0;
   if(fInternalOnly){
      sig_rad = GetRADCOR()->ContinuumStragglingStripApprox_InternalEquivRad(Ebeam,Eprime,theta,phi);
      if(fAddRegion4) sig_rad2 = GetRADCOR()->Internal2DEnergyIntegral(Ebeam,Eprime,theta,phi);
      sig_rad += sig_rad2;
   } else {
      sig_rad = GetRADCOR()->ContinuumStragglingStripApprox(Ebeam,Eprime,theta,phi);
      if(fAddRegion4) sig_rad2 = GetRADCOR()->Internal2DEnergyIntegral(Ebeam,Eprime,theta,phi);
      sig_rad += sig_rad2;
   }
   if( this->IncludeJacobian() ) sig_rad = sig_rad*TMath::Sin(theta);
   if(sig_rad <0.0 || TMath::IsNaN(sig_rad)) sig_rad = 0.0;
   return sig_rad;
} 
//______________________________________________________________________________

template <class T>
void RadiatorBase<T>::CreateRADCOR() const {
   fRADCOR = new RADCOR();
   //fRADCOR->Do(false); 
   fRADCOR->SetThreshold(2); 
   fRADCOR->UseMultiplePhoton();
   fRADCOR->UseInternal(false);  
   fRADCOR->UseExternal(true);  
   fRADCOR->SetPolarization(0); 
   fRADCOR->SetVerbosity(0); 
   //fRADCOR->SetCrossSection(fDiffXSec); 
   fRADCOR->SetTargetNucleus(Nucleus::Proton()); 
   fRADCOR->SetRadiationLengths(fRadLen);
}
//______________________________________________________________________________

template <class T>
void RadiatorBase<T>::CreatePOLRAD() const {
   fPOLRAD = new POLRAD();
   fPOLRAD->SetVerbosity(1);
   //fPOLRAD->DoQEFullCalc(false); 
   fPOLRAD->SetTargetNucleus(Nucleus::Proton());
   fPOLRAD->fErr   = 1E-1;   // integration error tolerance 
   fPOLRAD->fDepth = 3;     // number of iterations for integration 
   fPOLRAD->SetMultiPhoton(true); 
   //fPOLRAD->SetUltraRel   (false);
}
}}

#endif

