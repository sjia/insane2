#ifndef RADCOR_HH 
#define RADCOR_HH 1 

#include <cstdlib>
#include <iostream> 
#include "TSystem.h" 
#include "TMath.h"
#include "TStopwatch.h"
#include "TRandom3.h"
#include "TFile.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TF1.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "Math/Integrator.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/AllIntegrationTypes.h"
#include "Math/GaussIntegrator.h"
#include <Math/IFunction.h>
#include <Math/Functor.h>
#include "PhysicalConstants.h"
#include "InSANEMathFunc.h"
#include "Nucleus.h"
#include "InclusiveDiffXSec.h"
#include "PhysicalConstants.h"
#include "FormFactors.h"
#include "RADCORKinematics.h"
#include "MSWFormFactors.h"
#include "F1F209eInclusiveDiffXSec.h"
#include "AmrounFormFactors.h"
#include "StatisticalPolarizedPDFs.h"
#include "StructureFunctions.h"
#include "PolarizedCrossSectionDifference.h"

namespace insane {
namespace physics {
/** Class to handle radiative corrections to elastic and inelastic electron (and muon) scattering.
 *
 *
 * References: 
 *   Mo and Tsai, Rev.Mod.Phys. 1969
 *   Tsai, SLAC-PUB-848. 1971
 *   Stein, et.al.
 */
class RADCOR : public TObject{

   private:
      /** @name Switches  
       * Unnecessary switchs?
       * @{ 
       */
      Int_t       fUnits;                        ///< Energy units (0 => GeV, 1 => MeV) Why?!?  
      Int_t       fThreshold;                    ///< Integration threshold: 0 = elastic, 1 = quasielastic, 2 = pion 
      Int_t       fMultiPhoton;                  ///< 0 = off, 1 = on [soon to be deprecated] 
      bool        fIsElastic;                    ///< do elastic calculation (exact) 
      bool        fIsMultiPhoton;                ///< use multiple photon correction 
      bool        fIsInternal;                   ///< use internal correction 
      bool        fIsExternal;                   ///< use external correction 
      bool        fIsSmear;                      ///< use Fermi smearing factor 
      //@}  

      // Debugging variables 
      TFile      *fDebugFile;   //!
      TTree      *fDebugTree;   //!
      TString     fROOTFileName; 

      // internal  
      Double_t    rEs_int,rEp_int;
      Double_t    rCOSTHK_int,rOmega_int;
      Double_t    rQ2_int;                     // includes the real photon 
      Double_t    rEs_inel_int,rEp_inel_int;
      Double_t    rCOSTHK_inel_int,rOmega_inel_int;
      Double_t    rQ2_inel_int;                // includes the real photon 
      Double_t    rIntegrand_inel_int; 

      // external 
      Double_t    rEs_ext,rEp_ext,rEsPrime_ext,rEpPrime_ext,rtPrime_ext;
      Double_t    rEs_1_ext,rEp_1_ext,rEsPrime_1_ext,rEpPrime_1_ext,rtPrime_1_ext;
      Double_t    rEs_2_ext,rEp_2_ext,rEsPrime_2_ext,rEpPrime_2_ext,rtPrime_2_ext;
      Double_t    rT1_ext,rT2_ext,rT3_ext;
      Double_t    rT1_1_ext,rT2_1_ext,rT3_1_ext;
      Double_t    rT1_2_ext,rT2_2_ext,rT3_2_ext;

      /** @name Integration variables 
       *
       * @{ 
       */ 
      Int_t       fDepth;
      Double_t    fDeltaE,fDelta;
      Double_t    fEpsilon;
      Double_t    fthickness,fEsPrime,fEpPrime;         ///< Dummy variables?
      Double_t    fCOSTHK; 
      Double_t    fX0Eff;                               ///< Effective radiation length in g/cm^2  
      Double_t    fZEff,fAEff;                          ///< Effective Z and A of all materials 
      //@} 

      /** @name Target variables 
       *
       * @{ 
       */ 
      Nucleus fTargetNucleus;                           ///< Stores all nucleus information  
      TString fTargetName;                                    ///< Target name  
      Double_t fspin2;                                        ///< Target spin*2   
      //@}

      /** @name Kinematic variables.  
       *  Kinematics container variables [int, ext = internal and external tails (exact calc.)]
       *  @{ 
       */ 
      RADCORKinematics fKinematics;
      RADCORKinematics fKinematicsInt;
      RADCORKinematics fKinematicsExt;    
      Double_t fCFACT; 
      //@}  

      /** @name Models
       *
       * @{ 
       */ 
      FormFactors                  *fFF;                         //!   ///< Form factors for internally radiated xsec 
      FormFactors                  *fFFT;                        //!   ///< Form factors for internally radiated xsec 
      FormFactors                  *fFFQE;                       //!   ///< Form factors for internally radiated xsec 
      StructureFunctions           *fUnpolSFs;                   //!   ///< SFs used for internally radiated xsec calculations
      PolarizedStructureFunctions  *fPolSFs;                     //!   ///< SFs used for ... 

      InclusiveDiffXSec            *fUnpolXS;                    //!   ///< Born(?) unpolarized cross sections
      InclusiveDiffXSec            *fIntRadUnpolXS;              //!   ///< Internally radiated cross section (which will have straggling applied) 
      InclusiveDiffXSec            *fPolDiffXS;                  //!   ///< Polarized cross section difference 
      InclusiveDiffXSec            *fUnpolXSEl;                  //!   ///< Unpolarized ELASTIC cross section 
      InclusiveDiffXSec            *fPolDiffXSEl;                //!   ///< Polarized ELASTIC cross section difference  
      //@}

      // Miscellaneous variables 
      Double_t fhbar_c_sq;                                     ///< (h*c)^2 in the appropriate units                       
      Double_t fDeltaM;                                        ///< For computing Es or Ep limits at a given threshold  

   public: 
      RADCOR();
      virtual ~RADCOR();

      Int_t fPol;                                              ///< Polarization type [0 = unpolarized; 1 = Delta sigma (para); 2 = Delta sigma (perp)]   
      Int_t fDebug;                                            ///< Verbosity level
      Int_t fMCIntegration;                                    ///< Monte Carlo integration method (0 = Sample Mean; 1 = Accept/reject; 2 = Importance Sampling)  
      Int_t fNumMCEvents;                                      ///< Number of Monte Carlo events (for MC integration) 
      Int_t fMCBins;                                           ///< Number of Monte Carlo bins (for MC integration)  

      RADCORKinematics * GetKinematics(){ return &fKinematics ; }
      RADCORKinematics * GetKinematicsInt(){ return &fKinematicsInt ; }
      RADCORKinematics * GetKinematicsExt(){ return &fKinematicsExt ; }

      void InitializeVariables();
      void InitializeROOTFile();
      void WriteTheROOTFile();
      void SetUnits(Int_t); 
      void SetDeltaM(Double_t m){fDeltaM = m;} 

      void DoElastic(bool ans){
         fIsElastic=ans;
         fKinematics.DoElastic(ans); 
         fKinematicsInt.DoElastic(ans); 
         fKinematicsExt.DoElastic(ans); 
         if(fIsElastic){
            fThreshold = 0; 
         }else{
            fThreshold = 1;
         }
         //if(fIsElastic)  std::cout << "[RADCOR]: Will do ELASTIC calculation"   << std::endl;
         if(!fIsElastic){
            //std::cout << "[RADCOR]: Will do INELASTIC calculation" << std::endl;
            std::cout << "                Choose your integration threshold if doing the peaking approximation." << std::endl;
            std::cout << "                Default is the QE threshold." << std::endl;
         }
      }

      void UseInternal(bool ans = true){
         fIsInternal = ans;
         fKinematics.UseInternal(ans);  
         //if(fIsInternal)  std::cout << "[RADCOR]: Will do INTERNAL calculation." << std::endl;  
         //if(!fIsInternal) std::cout << "[RADCOR]: Will NOT do INTERNAL calculation." << std::endl;  
      }

      void UseExternal(bool ans = true){
         Double_t ZERO[2] = {0.,0.};  
         fIsExternal = ans;
         fKinematics.UseExternal(ans);  
         //if(fIsExternal)  std::cout << "[RADCOR]: Will do EXTERNAL calculation."     << std::endl;  
         if(!fIsExternal){
            //std::cout << "[RADCOR]: Will NOT do EXTERNAL calculation." << std::endl; 
            SetRadiationLengths(ZERO);
         }

      }

      void SetMonteCarloIntegrationMethod(Int_t a){
         fMCIntegration = a;
         TString method;
         switch(fMCIntegration){
            case 0: 
               method = Form("sample mean");
               break;
            case 1: 
               method = Form("accept/reject");
               break; 
            case 2: 
               method = Form("importance sampling");
               break; 
            default: 
               std::cout << "[RADCOR::SetMonteCarloIntegrationMethod]: ";
               std::cout << "Invalid integration method!  Exiting..." << std::endl;
               exit(1);
         }
         std::cout << "[RADCOR]: Using the Monte Carlo " << method << " integration method." << std::endl;
      }    

      void SetNumberOfMCEvents(Int_t n){fNumMCEvents = n;} 
      void SetNumberOfMCBins(Int_t n){fMCBins = n;} 

      void SetEffectiveZ(Double_t Z){fZEff = Z;}
      void SetEffectiveA(Double_t A){fAEff = A;}
      void SetEffectiveRadiationLength(Double_t x0){fX0Eff = x0;}

      void SetPolarization(Int_t p = 0){
         fPol = p; 
         fPolDiffXS->SetPolarizationType(p);
         //if(fPol==0) std::cout << "[RADCOR]: Will do UNPOLARIZED cross section calculation."      << std::endl;
         //if(fPol==1) std::cout << "[RADCOR]: Will do POLARIZED (para) cross section calculation." << std::endl;
         //if(fPol==2) std::cout << "[RADCOR]: Will do POLARIZED (perp) cross section calculation." << std::endl;
      }   
      void CalculateCFACT();
      void ProcessFormFactors(Double_t,Double_t *);                         
      void ProcessQuasiElasticFormFactors(Double_t,Double_t,Double_t *);                         
      void ProcessStructureFunctions(Double_t,Double_t,Double_t *);                         
      void SetVerbosity(int v){fDebug = v;} 
      void SetTargetNucleus(const Nucleus& n); 

      void UseFermiSmearing(bool ans = true){
         fIsSmear = ans; 
         //if(fIsSmear)  std::cout << "[RADCOR]: Will smear the form factors."     << std::endl;
         //if(!fIsSmear) std::cout << "[RADCOR]: Will not smear the form factors." << std::endl;
      }

      void SetScatteringAngle(Double_t th){
         fKinematics.SetTheta(th*degree); 
         fKinematicsInt.SetTheta(th*degree); 
         fKinematicsExt.SetTheta(th*degree);
      }

      void SetRadiationLengths(const Double_t *); 

      void SetFormFactors(FormFactors *f){fFF = f;}
      void SetTargetFormFactors(FormFactors *f){fFFT = f;}
      void SetQuasiElasticFormFactors(FormFactors *f){fFFQE = f;}

      void SetUnpolarizedStructureFunctions(StructureFunctions *f){fUnpolSFs = f;}
      void SetPolarizedStructureFunctions(PolarizedStructureFunctions *f){fPolSFs = f;}
      void SetUnpolSFs(StructureFunctions *f){SetUnpolarizedStructureFunctions(f);}

      void SetUnpolarizedCrossSection(InclusiveDiffXSec *f){fUnpolXS = f;}
      void SetUnpolXS(InclusiveDiffXSec *f){SetUnpolarizedCrossSection(f);}

      void SetElasticCrossSection(InclusiveDiffXSec *f){fUnpolXSEl = f;}

      void SetInternallyRadiatedCrossSection(InclusiveDiffXSec *f){ fIntRadUnpolXS = f;}

      void SetPolarizedCrossSectionDifference(InclusiveDiffXSec *f){fPolDiffXS = f; fPolDiffXS->InitializePhaseSpaceVariables();}
      void SetPolDiffXS(InclusiveDiffXSec *f){SetPolarizedCrossSectionDifference(f);}

      void SetKinematics(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi=0); 
      void SetKinematicsForExactInternal(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi=0); 
      void SetKinematicsForExactExternal(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi=0); 

      void SetThreshold(Int_t t){
         fThreshold = t;
         //if(fThreshold==0) std::cout << "[RADCOR]: Integrating from the ELASTIC threshold."         << std::endl;
         //if(fThreshold==1) std::cout << "[RADCOR]: Integrating from the QUASI-ELASTIC threshold."   << std::endl;
         //if(fThreshold==2) std::cout << "[RADCOR]: Integrating from the PION PRODUCTION threshold." << std::endl;
      }

      void SetMultiplePhoton(Int_t t){
         // deprecated method! 
         fMultiPhoton = t;
         if(fMultiPhoton==1) UseMultiplePhoton(true);
         if(fMultiPhoton==0) UseMultiplePhoton(false);
      }   
      void UseMultiplePhoton(Bool_t ans=true){ 
         fIsMultiPhoton = ans;
         if(fIsMultiPhoton){
            //std::cout << "[RADCOR]: Will use multiple photon correction." << std::endl;
            fMultiPhoton = 1; 
         }else{
            //std::cout << "[RADCOR]: Will NOT use multiple photon correction." << std::endl;
            fMultiPhoton = 0; 
         } 
      }

      TString GetTargetName(){return fTargetName;}

      Double_t GetOmegaMax(Double_t);                                   ///< omega_max from Mo & Tsai (Eq B10)  
      Double_t GetMfSq(Double_t,Double_t);                              ///< Wsq from Mo & Tsai (Eq B9)  
      Double_t F_soft(Double_t,Double_t,Double_t); 
      Double_t GetTr(Double_t);                                         ///< Tr(Q2): changes PER INTEGRATION BIN 
      Double_t GetFTilde(Double_t);                                     ///< FTilde(Q2): changes PER INTEGRATION BIN 
      Double_t GetFTildeExactInternal(Double_t);                        ///< Same as above, just using exact internal variables 
      Double_t GetPhi(Double_t) const ;                                 ///< shape of bremsstrahlung for small v (complete screening)
      Double_t GetEsMin(Double_t,Double_t);                             ///< EsMin(Ep,theta) 
      Double_t GetEpMax(Double_t,Double_t);                             ///< EpMax(Es,theta)                        
      Double_t GetSpence(Double_t);                                     ///< Spence(x), x = arbitrary value
      Double_t GetSmearFunc(Double_t);                                  ///< Fermi smearing function 

      /** @name For the peaking approximation
       *
       * @{
       */
      Double_t ElasticTail(Double_t,Double_t,Double_t); 
      Double_t EnergyPeakingApprox(Double_t,Double_t,Double_t);
      Double_t CalculateEsIntegral();
      Double_t CalculateEpIntegral();
      Double_t EsIntegrand(Double_t &);
      Double_t EpIntegrand(Double_t &);
      Double_t sigma_p();                                               ///< Elastic tail [internal] -- NO INTEGRAL  
      Double_t sigma_b();                                               ///< Elastic tail [external] -- NO INTEGRAL  
      Double_t sigma_qe_b();                                            ///< Quasi-Elastic tail [external] -- NO INTEGRAL  
      Double_t sigma_el(Double_t,Double_t); 
      Double_t sigma_qe(Double_t,Double_t); 
      Double_t sigma_el_tilde(Double_t,Double_t); 
      Double_t sigma_qe_tilde(Double_t,Double_t);
      //@} 

      /** @name For the exact integral. 
       * 
       * @{
       */
      /// Stuff we're using 
      Double_t Exact(Double_t,Double_t,Double_t);
      Double_t InternalIntegrand_MoTsai69(Double_t);                    ///< Elastic tail integrand  
      Double_t InternalIntegrand_Inelastic_MoTsai69(Double_t *);        ///< Inelastic tail integrand 
      Double_t EpExactIntegrand_4(Double_t *);
      Double_t ExternalOnly_ExactInelasticIntegrand(Double_t *);
      Double_t EpExactIntegrand_Inelastic_Term_1(Double_t *);
      Double_t EpExactIntegrand_Inelastic_Term_2(Double_t *);            
      Double_t Ib_2(Double_t,Double_t,Double_t);                        ///< Straggling function (Eq B43 of Tsai's SLAC-PUB-0848)  
      Double_t Delta_r(Double_t);                                       ///< Multiple photon term (from Mo & Tsai) 
      Double_t Delta_inf_1(Double_t);                                   ///< Multiple photon infrared divergent component (form INFERRED from Mo & Tsai)  
      Double_t Delta_inf_2();                                           ///< Multiple photon infrared divergent component (form from POLRAD) 
      Double_t Delta(Double_t,Double_t);                                ///< Energy loss applied before doing full RCs  
      Double_t Delta_0(Double_t,Double_t);                            
      /// New stuff  
      void     SetIntegrationParametersElastic(Int_t,Double_t,Double_t,Double_t,Int_t &,Double_t *,Double_t *); 
      void     SetIntegrationParametersInelastic(Int_t,Double_t,Double_t,Double_t,Int_t &,Double_t *,Double_t *,Int_t &,Double_t *,Double_t *,Int_t region = 0);
      Double_t ExactElasticInternalOnly(Double_t,Double_t,Double_t);   
      Double_t ExactElasticInternalExternal(Double_t,Double_t,Double_t);   
      Double_t ExactElasticExternalOnly(Double_t,Double_t,Double_t);   
      Double_t ExactInelasticInternalOnly(Double_t,Double_t,Double_t);   
      Double_t ExactInelasticInternalExternal(Double_t,Double_t,Double_t);   
      Double_t ExactInelasticExternalOnly(Double_t,Double_t,Double_t);   

      /// Depricated stuff 
      Double_t Ie(Double_t,Double_t,Double_t);
      Double_t Ie_2(Double_t,Double_t,Double_t);
      Double_t Ib(Double_t,Double_t,Double_t);
      Double_t W_b(Double_t,Double_t);  
      Double_t W_i(Double_t,Double_t); 
      /// Stuff we can get rid of soon  
      Double_t GetX0();
      Double_t GetInternalRadiatedXS(Double_t,Double_t,Double_t);
      Double_t GetInternalRadiatedXS_Inelastic(Double_t,Double_t,Double_t);
      Double_t InternalIntegrand(Double_t &);                           ///< Elastic tail (from Stein)  
      Double_t InternalIntegrand_costhk_Inelastic(Double_t &);          ///< Inelastic tail (from Mo & Tsai) 
      Double_t InternalIntegrand_omega_Inelastic(Double_t &);           ///< Inelastic tail (from Mo & Tsai) 
      Double_t CalculateTExactIntegral();
      Double_t CalculateEsExactIntegral();
      Double_t CalculateEpExactIntegral(Double_t);
      Double_t TExactIntegrand(Double_t &);
      Double_t EsExactIntegrand(Double_t &);
      Double_t EpExactIntegrand(Double_t &);
      Double_t EsExactIntegrand_2(Double_t &);
      Double_t EpExactIntegrand_2(Double_t &);
      Double_t EpExactIntegrand_3(Double_t &,Double_t &,Double_t &);
      //@} 

      Double_t ExternalOnly_ElasticPeak(Double_t Es,Double_t Ep,Double_t theta, Double_t phi=0.0);
      Double_t ExternalOnly_PeakPlot(double *x,double *p){
         Double_t Ep  = x[0];
         Double_t Es  = p[0];
         Double_t th  = p[1];
         if(Ep > Es) return(0.0);
         Double_t M  = M_p/GeV;
         th = 2.0*TMath::ASin(TMath::Sqrt((M/2.0)*(1.0/Ep - 1.0/Es) )); 
         Double_t res = ExternalOnly_ElasticPeak(Es,Ep,th); 
         if( TMath::IsNaN(res) ) res = 0.0;
         return res;  
      }
      Double_t ExternalOnly_ExactElasticTail(Double_t Es,Double_t Ep,Double_t theta, Double_t phi=0.0);
      Double_t ExternalOnly_Plot(double *x,double *p){
         Double_t Ep  = x[0];
         Double_t Es  = p[0];
         Double_t th  = p[1];
         Double_t res = ExternalOnly_ExactElasticTail(Es,Ep,th); 
         if( TMath::IsNaN(res) ) res = 0.0;
         return res;  
      }
      Double_t Internal2DEnergyIntegral(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi=0.0);
      Double_t External2DEnergyIntegral(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi=0.0);
      Double_t External2DEnergyIntegral_Integrand(Double_t Es,Double_t Ep,Double_t theta,Double_t phi,Double_t t1,Double_t t2,Double_t Esprime,Double_t EpPrime);
      class External2DEnergyIntegral_IntegrandWrap : public ROOT::Math::IBaseFunctionMultiDim {
         public:
            mutable Double_t Es;
            mutable Double_t Ep;
            mutable Double_t theta;
            mutable Double_t phi;
            mutable Double_t t1;
            mutable Double_t t2;
            mutable RADCOR * fRADCOR;
            double DoEval(const double* x) const {
               return fRADCOR->External2DEnergyIntegral_Integrand(Es,Ep,theta,phi,t1,t2,x[0],x[1]);
            }
            unsigned int NDim() const { return 2; }
            ROOT::Math::IBaseFunctionMultiDim* Clone() const { return new External2DEnergyIntegral_IntegrandWrap(); }
            ClassDef(External2DEnergyIntegral_IntegrandWrap,1)
      };
      Double_t ContinuumStragglingStripApprox_InternalEquivRad(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi=0.0);
      Double_t ContinuumStragglingStripApprox(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi=0.0);
      Double_t StripApproxIntegrand1(Double_t Es,  Double_t Ep, Double_t theta, Double_t T, Double_t EsPrime);
      Double_t StripApproxIntegrand1_withD(Double_t Es,  Double_t Ep, Double_t theta, Double_t T, Double_t EsPrime);
      Double_t StripApproxIntegrand2(Double_t Es,  Double_t Ep, Double_t theta, Double_t T, Double_t EpPrime);
      class StripApproxIntegrand1_withDWrap : public ROOT::Math::IBaseFunctionOneDim {
         public:
            mutable RADCOR * fRADCOR;
            mutable Double_t Es;
            mutable Double_t Ep;
            mutable Double_t T;
            mutable Double_t theta;
            double DoEval(double x) const {
               double res = fRADCOR->StripApproxIntegrand1_withD(Es,Ep,theta,T,x);
               //std::cout << " sig1 = " << res << std::endl;
               return res ;
            }
            unsigned int NDim() const { return 1; }
            ROOT::Math::IBaseFunctionOneDim* Clone() const { return new StripApproxIntegrand1_withDWrap(); }
            ClassDef(StripApproxIntegrand1_withDWrap,1)
      };
      class StripApproxIntegrand1Wrap : public ROOT::Math::IBaseFunctionOneDim {
         public:
            mutable RADCOR * fRADCOR;
            mutable Double_t Es;
            mutable Double_t Ep;
            mutable Double_t T;
            mutable Double_t theta;
            double DoEval(double x) const {
               double res = fRADCOR->StripApproxIntegrand1(Es,Ep,theta,T,x);
               //std::cout << " sig1 = " << res << std::endl;
               return res ;
            }
            unsigned int NDim() const { return 1; }
            ROOT::Math::IBaseFunctionOneDim* Clone() const { return new StripApproxIntegrand1Wrap(); }
            ClassDef(StripApproxIntegrand1Wrap,1)
      };
      class StripApproxIntegrand2Wrap : public ROOT::Math::IBaseFunctionOneDim {
         public:
            mutable RADCOR * fRADCOR;
            mutable Double_t Es;
            mutable Double_t Ep;
            mutable Double_t T;
            mutable Double_t theta;
            double DoEval(double x) const {
               double res = fRADCOR->StripApproxIntegrand2(Es,Ep,theta,T,x);
               //std::cout << " sig2 = " << res << std::endl;
               return res ;
            }
            unsigned int NDim() const { return 1; }
            ROOT::Math::IBaseFunctionOneDim* Clone() const { return new StripApproxIntegrand2Wrap(); }
            ClassDef(StripApproxIntegrand2Wrap,1)
      };
      class ExternalOnly_ExactInelasticIntegrandWrap : public ROOT::Math::IBaseFunctionMultiDim {
         public:
            mutable RADCOR * fRADCOR;
            double DoEval(const double* x) const {
               Double_t y[] = {x[0],x[1],x[2]};
               return fRADCOR->ExternalOnly_ExactInelasticIntegrand(y);
            }
            unsigned int NDim() const { return 3; }
            ROOT::Math::IBaseFunctionMultiDim* Clone() const { return new ExternalOnly_ExactInelasticIntegrandWrap(); }
            ClassDef(ExternalOnly_ExactInelasticIntegrandWrap,1)
      };

      /** @name Integration functions 
       * @{  
       */
      /// Adaptive Simpson  
      Double_t AdaptiveSimpson(Double_t (RADCOR::*)(Double_t &) ,Double_t,Double_t,Double_t,Int_t);
      Double_t AdaptiveSimpsonAux(Double_t (RADCOR::*)(Double_t &) ,Double_t,Double_t,Double_t,Double_t,Double_t,Double_t,Double_t,Int_t);
      /// Monte carlo integration in N dimensions
      // void MCIntegral(Int_t,const Int_t &,Double_t (RADCOR::*)(Double_t *),
      //                 Int_t (RADCOR::*)(Double_t *),
      //                 Double_t *,Double_t *,Double_t &,Double_t &);
      /// Boundary functions 
      Int_t Boundary(Double_t *); 
      Int_t Boundary_Inelastic_1(Double_t *);  
      Int_t Boundary_Inelastic_2(Double_t *);  
      Int_t Boundary_Internal(Double_t *);                      // returns 1 
      Int_t Boundary_Internal_Inelastic(Double_t *);  
      Double_t RandomVariable(Int_t,Double_t *,Double_t *);  
      Double_t ProbabilityDist(Double_t *,Double_t *,Double_t *);  
      /// Accept/reject method  
      void MCAcceptReject(const Int_t &,Double_t (RADCOR::*)(Double_t *),
            Double_t *,Double_t *,Int_t,Int_t,Double_t &,Double_t &);
      /// Importance sampling method
      void MCImportanceSampling(const Int_t &,Double_t (RADCOR::*)(Double_t *),
            Double_t (RADCOR::*)(Double_t *,Double_t *,Double_t *),
            Double_t (RADCOR::*)(Int_t,Double_t *,Double_t *),
            Double_t *,Double_t *,Int_t,Double_t &,Double_t &); 
      /// Sample mean method  
      void MCSampleMean(const Int_t &,Double_t (RADCOR::*)(Double_t *),
            Double_t *,Double_t *,Int_t,Double_t &,Double_t &); 
      void MCSampleMean(const Int_t &,Double_t (RADCOR::*)(Double_t),
            Double_t *,Double_t *,Int_t,Double_t &,Double_t &); 
      //@} 

      /** @name Interpolation functions. 
       * TERP and FTCS follow closely the form of the fortran RADCOR code.
       * BinarySearch, ImportData and SetUpMatrix are 'new' methods that 
       * have no counterpart in the original fortran.  
       * Details:  
       * - If you want to use a grid, be sure to set the path before calling the method UseGridData!
       * - The grid MUST be of the form: (Es,Ep,XS), with Es,Ep in GeV and XS in nb/GeV/sr, 
       *   with Es and Ep both being in ascending order  
       * - Be sure that the grid fills out the full phase space of integration; otherwise, the interpolation
       *   will use a linear extrapolation outside the region of the grid.  
       * - fEsGridData is a std::vector of type Double_t 
       * - fEpGridData and fXSGridData are doubly-indexed std::vector of type Double_t 
       * @{ 
       */
      bool fUseGridData,fUseGridBornData;
      TString fGridPath,fGridBornPath;
      std::vector<Double_t> fEsGridData,fEpGridBornData,fXSGridBornData; 
      std::vector< std::vector<Double_t> > fEpGridData,fXSGridData;
      void SetGridPath(TString path){fGridPath = path;}  
      void SetGridBornPath(TString path){fGridBornPath = path;}  
      void UseGridData(bool ans=true){
         fUseGridData = ans;
         if(fUseGridData){
            std::cout << "[RADCOR]: Will use data from grid file: " << fGridPath << std::endl;
            std::cout << "                when calculating the cross sections in the integrals." << std::endl;
            ImportGridData();
         }
      }
      void UseGridBornData(bool ans=true){
         fUseGridBornData = ans;
         if(fUseGridBornData){
            std::cout << "[RADCOR]: Will use BORN data from grid file: " << fGridBornPath << std::endl;
            std::cout << "                when calculating the BORN cross section." << std::endl;
            ImportGridBornData();
         }
      } 
      void ImportGridData(); 
      void ImportGridBornData(); 
      void SetUpMatrix(std::vector<Double_t>,std::vector<Double_t>,std::vector< std::vector<Double_t> > &); 
      void BinarySearch(std::vector<Double_t>,Double_t,Int_t &,Int_t &); 
      Double_t TERP(Int_t,Double_t);
      Double_t FTCS(Double_t,Double_t);
      //@}  

      /** @name Debugging stuff 
       *
       * @{ 
       */ 
      TStopwatch *fWatch; //->
      Double_t Ie_2_Type1_Plot(double *x,double *p){
         Double_t E  = x[0]; 
         Double_t E0 = p[0];
         Double_t t  = p[1]; 
         Double_t res = Ie_2(E0,E,t); 
         return res;
      }
      Double_t Ie_2_Type2_Plot(double *x,double *p){
         Double_t E0 = x[0]; 
         Double_t E  = p[0];
         Double_t t  = p[1]; 
         Double_t res = Ie_2(E0,E,t); 
         return res;
      }
      Double_t InternalIntegrand_omega_Inelastic_Plot(double *x,double *p){
         Double_t omega = x[0];
         fCOSTHK        = p[0];
         Double_t res   = InternalIntegrand_omega_Inelastic(omega); 
         return res;  
      }
      Double_t InternalIntegrand_costhk_Inelastic_Plot(double *x,double *p){
         Double_t costhk = x[0];
         Double_t res    = InternalIntegrand_costhk_Inelastic(costhk); 
         return res;  
      }
      Double_t EsExactIntegrand_Plot(double *x,double *p){
         Double_t EsPrime = x[0]; 
         // We have to set Es, Ep and theta 
         Double_t Es      = p[0]; 
         Double_t Ep      = p[1]; 
         Double_t th      = p[2]; 
         // Double_t ph      = p[3]; 
         Double_t t       = p[4]; 
         fthickness = t;
         fKinematicsExt.SetEs(Es); 
         fKinematicsExt.SetEp(Ep); 
         fKinematicsExt.SetTheta(th); 
         // get the integrand  
         // Double_t res     = EsExactIntegrand(EsPrime); 
         Double_t res     = EsExactIntegrand_2(EsPrime); 
         return res;
      }
      Double_t EpExactIntegrand_Plot(double *x,double *p){
         Double_t EpPrime = x[0]; 
         // We have to set Es, Ep and theta 
         Double_t Es      = p[0]; 
         Double_t Ep      = p[1]; 
         Double_t th      = p[2]; 
         // Double_t ph      = p[3]; 
         Double_t t       = p[4]; 
         Double_t EsPrime = p[5]; 
         fEsPrime         = EsPrime; 
         fthickness = t;
         fKinematicsExt.SetEs(Es); 
         fKinematicsExt.SetEp(Ep); 
         fKinematicsExt.SetTheta(th); 
         // get the integrand  
         // Double_t res     = EpExactIntegrand(EpPrime); 
         Double_t res     = EpExactIntegrand_2(EpPrime); 
         return res;
      }
      // 2D plots of the 4-dimensional integrand 
      Double_t EpExactIntegrand_4_EsPrimeEpPrime_Plot(double *x,double *p){
         Double_t EsPrime = x[0]; 
         Double_t EpPrime = x[1]; 
         // We have to set Es, Ep and theta 
         Double_t Es      = p[0]; 
         Double_t Ep      = p[1]; 
         Double_t th      = p[2]; 
         Double_t costhk  = p[3]; 
         Double_t t       = p[4]; 
         Double_t y[4]    = {EpPrime,EsPrime,t,costhk}; 
         fEsPrime         = EsPrime; 
         fthickness       = t;
         fKinematicsExt.SetEs(Es); 
         fKinematicsExt.SetEp(Ep); 
         fKinematicsExt.SetTheta(th); 
         // get the integrand  
         Double_t res     = EpExactIntegrand_4(y); 
         return res;
      }

      Double_t TExactIntegrand_Plot(double *x,double *p){
         Double_t t     = x[0]; 
         // We have to set Es, Ep and theta 
         Double_t Es    = p[0]; 
         Double_t Ep    = p[1]; 
         Double_t th    = p[2]; 
         fKinematicsExt.SetEs(Es); 
         fKinematicsExt.SetEp(Ep); 
         fKinematicsExt.SetTheta(th); 
         // computing the full tail will set all needed kinematics -- I don't think we need to do this step 
         // Double_t exact = Exact(Es,Ep,th); 
         // get the integrand 
         Double_t res   = TExactIntegrand(t); 
         return res;
      }
      Double_t TExactIntegrand_Snapshot_Plot(double *x,double *p){
         Double_t t       = x[0]; 
         // We have to set Es, Ep and theta 
         Double_t Es      = p[0]; 
         Double_t EsPrime = p[1]; 
         Double_t Ep      = p[2]; 
         Double_t EpPrime = p[3]; 
         Double_t theta   = p[4];
         Double_t t_b     = fKinematicsExt.Gett_b();  
         Double_t t_a     = fKinematicsExt.Gett_a();
         Double_t T       = t_b + t_a;   
         fKinematicsExt.SetEs(Es); 
         fKinematicsExt.SetEp(Ep); 
         fKinematicsExt.SetTheta(theta); 
         // Get the integrand
         // We're taking a 'snapshot' here, so the only integral done is the internal one 
         Double_t T1    = Ie_2(Es,EsPrime,t);  
         Double_t T2    = GetInternalRadiatedXS(EsPrime,EpPrime,theta);  
         Double_t T3    = Ie_2(EpPrime,Ep,T-t);  
         Double_t res   = T1*T2*T3;
         // std::cout << "---------------------------------" << std::endl; 
         // std::cout << "Es  = " << Es       << std::endl;
         // std::cout << "Es' = " << EsPrime  << std::endl;
         // std::cout << "Ep  = " << Ep       << std::endl;
         // std::cout << "Ep' = " << EpPrime  << std::endl;
         // std::cout << "th  = " << theta/degree  << std::endl;
         // std::cout << "t   = " << t        << std::endl;
         // std::cout << "T1 = " << T1        << std::endl;
         // std::cout << "T2 = " << T2        << std::endl;
         // std::cout << "T3 = " << T3        << std::endl;
         return res;
      }
      Double_t InternalIntegrand_Plot(double *x,double *p){
         Double_t costhk = x[0];
         // We have to set Es, Ep and theta 
         Double_t Es    = p[0]; 
         Double_t Ep    = p[1]; 
         Double_t th    = p[2]; 
         Double_t ph    = p[3]; 
         fKinematicsInt.SetEs(Es); 
         fKinematicsInt.SetEp(Ep); 
         fKinematicsInt.SetTheta(th); 
         fKinematicsInt.SetPhi(ph); 
         Double_t res   = InternalIntegrand_MoTsai69(costhk); 
         return res;  
      }
      Double_t InternalIntegrand_Inelastic_Plot(double *x,double *p){
         Double_t costhk = x[0];
         // We have to set Es, Ep and theta 
         Double_t Es    = p[0]; 
         Double_t Ep    = p[1]; 
         Double_t th    = p[2]; 
         Double_t ph    = p[3]; 
         Double_t omega = p[3]; 
         Double_t y[2]  = {costhk,omega}; 
         fKinematicsInt.SetEs(Es); 
         fKinematicsInt.SetEp(Ep); 
         fKinematicsInt.SetTheta(th); 
         fKinematicsInt.SetPhi(ph); 
         Double_t res   = InternalIntegrand_Inelastic_MoTsai69(y); 
         return res;  
      }
      Double_t InternalIntegrand_Alt_Plot(double *x,double *p){
         // try different forms of this integrand -- take the exp
         Double_t costhk  = x[0];
         // we have to set Es, Ep and theta
         Double_t Es      = p[0];
         Double_t Ep      = p[1];
         Double_t th      = p[2];
         Double_t ph      = p[3];
         fKinematicsInt.SetEs(Es); 
         fKinematicsInt.SetEp(Ep); 
         fKinematicsInt.SetTheta(th); 
         fKinematicsInt.SetPhi(ph); 
         Double_t arg     = InternalIntegrand(costhk); 
         Double_t res     = TMath::Exp(-arg); 
         // std::cout << arg << "\t" << res << std::endl;
         return res;  
      }
      Double_t GetInternalRadiatedXS_Plot(double *x,double *p){
         Double_t Ep  = x[0];
         Double_t Es  = p[0];
         Double_t th  = p[1];
         Double_t mp  = F_soft(Es,Ep,th); 
         Double_t sig = GetInternalRadiatedXS(Es,Ep,th); 
         Double_t res = hbarc2_gev_nb*sig; 
         if(fIsMultiPhoton) res *= mp; 
         return res;  
      }
      Double_t GetInternalRadiatedXS_Inelastic_Plot(double *x,double *p){
         Double_t Ep  = x[0];
         Double_t Es  = p[0];
         Double_t th  = p[1];
         Double_t res = GetInternalRadiatedXS_Inelastic(Es,Ep,th); 
         return res;  
      }
      Double_t Exact_Plot(double *x,double *p){
         Double_t Ep  = x[0];
         Double_t Es  = p[0];
         Double_t th  = p[1];
         Double_t res = Exact(Es,Ep,th); 
         return res;  
      }
      Double_t Approx_Plot(double *x,double *p){
         Double_t Ep  = x[0];
         Double_t Es  = p[0];
         Double_t th  = p[1];
         Double_t res = ContinuumStragglingStripApprox(Es,Ep,th); 
         return res;  
      }
      Double_t Ib_Es_Plot(double *x,double *p){
         Double_t EsPrime  = x[0]; 
         Double_t Es       = p[0]; 
         Double_t t        = p[1];
         Double_t res      = Ib(Es,EsPrime,t); 
         return res; 
      }
      Double_t Ib_Ep_Plot(double *x,double *p){
         Double_t EpPrime  = x[0]; 
         Double_t Ep       = p[0]; 
         Double_t t        = p[1];
         Double_t res      = Ib(EpPrime,Ep,t); 
         return res; 
      }
      Double_t Ib_Plot(double *x,double *p){
         Double_t E0       = x[0]; 
         Double_t E        = x[1];
         Double_t t        = x[2]; 
         Double_t res      = Ib(E0,E,t); 
         return res; 
      }
      Double_t GetOmegaMax_Plot(double *x,double *p){
         Double_t costhk = x[0];
         Double_t Es     = p[0];  
         Double_t Ep     = p[1];  
         Double_t th     = p[2]; 
         SetKinematicsForExactInternal(Es,Ep,th);  
         Double_t res    = GetOmegaMax(costhk); 
         return res;  
      }
      //@} 

      ClassDef(RADCOR,1) 
}; 


/** Wrapper class!!
 * IBaseFunctionOneDim
 */
class EpFuncWrap : public ROOT::Math::IBaseFunctionOneDim {
   public:
      EpFuncWrap(){}
      ~EpFuncWrap(){}

      mutable RADCOR * fRADCOR;

      double DoEval(double x) const
      {
         double EpPrime = x; 
         double res = fRADCOR->EpIntegrand(EpPrime);
         //std::cout << " res(" << tau << ") = "  << res << "\n";
         return res ;
      }

      unsigned int NDim() const
      {
         return 1;
      }

      ROOT::Math::IBaseFunctionOneDim* Clone() const
      {
         return new EpFuncWrap();
      }

      ClassDef(EpFuncWrap,1)
};

/** Wrapper class!!
 * IBaseFunctionOneDim
 */
class EsFuncWrap : public ROOT::Math::IBaseFunctionOneDim {
   public:
      EsFuncWrap(){}
      ~EsFuncWrap(){}
      mutable RADCOR * fRADCOR;

      double DoEval(double x) const {
         double EsPrime = x;
         double res = fRADCOR->EsIntegrand(EsPrime);
         return res ;
      }

      unsigned int NDim() const { return 1; }
      ROOT::Math::IBaseFunctionOneDim* Clone() const { return new EsFuncWrap(); }

      ClassDef(EsFuncWrap,1)
};

/** Wrapper class!!
 * IBaseFunctionOneDim
 */
class EpExactFuncWrap : public ROOT::Math::IBaseFunctionOneDim {
   public:
      EpExactFuncWrap(){}
      ~EpExactFuncWrap(){}

      mutable RADCOR * fRADCOR;

      double DoEval(double x) const
      {
         double EpPrime = x; 
         double res = fRADCOR->EpExactIntegrand(EpPrime);
         //std::cout << " res(" << tau << ") = "  << res << "\n";
         return res ;
      }

      unsigned int NDim() const
      {
         return 1;
      }

      ROOT::Math::IBaseFunctionOneDim* Clone() const
      {
         return new EpExactFuncWrap();
      }

      ClassDef(EpExactFuncWrap,1)
};

/** Wrapper class!!
 * IBaseFunctionOneDim
 */
class EpExactFunc2Wrap : public ROOT::Math::IBaseFunctionOneDim {
   public:
      EpExactFunc2Wrap(){}
      ~EpExactFunc2Wrap(){}

      mutable RADCOR * fRADCOR;

      double DoEval(double x) const
      {
         double EpPrime = x; 
         double res = fRADCOR->EpExactIntegrand_2(EpPrime);
         //std::cout << " res(" << tau << ") = "  << res << "\n";
         return res ;
      }

      unsigned int NDim() const
      {
         return 1;
      }

      ROOT::Math::IBaseFunctionOneDim* Clone() const
      {
         return new EpExactFunc2Wrap();
      }

      ClassDef(EpExactFunc2Wrap,1)
};

/** Wrapper class!!
 * IBaseFunctionOneDim
 */
class EsExactFuncWrap : public ROOT::Math::IBaseFunctionOneDim {
   public:
      EsExactFuncWrap(){}
      ~EsExactFuncWrap(){}

      mutable RADCOR * fRADCOR;

      double DoEval(double x) const
      {
         double EsPrime = x; 
         double res = fRADCOR->EsExactIntegrand(EsPrime);
         //std::cout << " res(" << tau << ") = "  << res << "\n";
         return res ;
      }

      unsigned int NDim() const
      {
         return 1;
      }

      ROOT::Math::IBaseFunctionOneDim* Clone() const
      {
         return new EsExactFuncWrap();
      }

      ClassDef(EsExactFuncWrap,1)
};

/** Wrapper class!!
 * IBaseFunctionOneDim
 */
class EsExactFunc2Wrap : public ROOT::Math::IBaseFunctionOneDim {
   public:
      EsExactFunc2Wrap(){}
      ~EsExactFunc2Wrap(){}

      mutable RADCOR * fRADCOR;

      double DoEval(double x) const
      {
         double EsPrime = x; 
         double res = fRADCOR->EsExactIntegrand_2(EsPrime);
         //std::cout << " res(" << tau << ") = "  << res << "\n";
         return res ;
      }

      unsigned int NDim() const
      {
         return 1;
      }

      ROOT::Math::IBaseFunctionOneDim* Clone() const
      {
         return new EsExactFunc2Wrap();
      }

      ClassDef(EsExactFunc2Wrap,1)
};

/** Wrapper class!!
 * IBaseFunctionOneDim
 */
class TExactFuncWrap : public ROOT::Math::IBaseFunctionOneDim {
   public:
      TExactFuncWrap(){}
      ~TExactFuncWrap(){}

      mutable RADCOR * fRADCOR;

      double DoEval(double x) const
      {
         double t = x; 
         double res = fRADCOR->TExactIntegrand(t);
         return res ;
      }

      unsigned int NDim() const
      {
         return 1;
      }

      ROOT::Math::IBaseFunctionOneDim* Clone() const
      {
         return new TExactFuncWrap();
      }

      ClassDef(TExactFuncWrap,1)
};

/** Wrapper class!!
 * IBaseFunctionOneDim
 */
class IntRadFuncWrap : public ROOT::Math::IBaseFunctionOneDim {
   public:
      IntRadFuncWrap(){}
      ~IntRadFuncWrap(){}

      mutable RADCOR * fRADCOR;

      double DoEval(double x) const
      {
         double COSTHK = x; 
         double res = fRADCOR->InternalIntegrand(COSTHK);
         //std::cout << " res(" << tau << ") = "  << res << "\n";
         return res ;
      }

      unsigned int NDim() const
      {
         return 1;
      }

      ROOT::Math::IBaseFunctionOneDim* Clone() const
      {
         return new IntRadFuncWrap();
      }

      ClassDef(IntRadFuncWrap,1)
};
/** Wrapper class!!
 * IBaseFunctionOneDim
 */
class IntRadOmegaFuncWrap : public ROOT::Math::IBaseFunctionOneDim {
   public:
      IntRadOmegaFuncWrap(){}
      ~IntRadOmegaFuncWrap(){}

      mutable RADCOR * fRADCOR;

      double DoEval(double x) const
      {
         double omega = x; 
         double res = fRADCOR->InternalIntegrand_omega_Inelastic(omega);
         //std::cout << " res(" << tau << ") = "  << res << "\n";
         return res ;
      }

      unsigned int NDim() const
      {
         return 1;
      }

      ROOT::Math::IBaseFunctionOneDim* Clone() const
      {
         return new IntRadOmegaFuncWrap();
      }

      ClassDef(IntRadOmegaFuncWrap,1)
};

/** Wrapper class!!
 * IBaseFunctionOneDim
 */
class IntRadCosThkFuncWrap : public ROOT::Math::IBaseFunctionOneDim {
   public:
      IntRadCosThkFuncWrap(){}
      ~IntRadCosThkFuncWrap(){}

      mutable RADCOR * fRADCOR;

      double DoEval(double x) const
      {
         double COSTHK = x; 
         double res = fRADCOR->InternalIntegrand_costhk_Inelastic(COSTHK);
         //std::cout << " res(" << tau << ") = "  << res << "\n";
         return res ;
      }

      unsigned int NDim() const
      {
         return 1;
      }

      ROOT::Math::IBaseFunctionOneDim* Clone() const
      {
         return new IntRadCosThkFuncWrap();
      }

      ClassDef(IntRadCosThkFuncWrap,1)
};

}
}

#endif 

