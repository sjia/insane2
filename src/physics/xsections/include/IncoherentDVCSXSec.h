#ifndef IncoherentDVCSXSec_H
#define IncoherentDVCSXSec_H 1

#include "DiffXSec.h"
#include "TLorentzVector.h"
#include "TLorentzRotation.h"
#include "TRotation.h"
#include "FermiMomentumDist.h"

namespace insane {
   namespace physics {

      /** Incoherent DVCS.
       * \ingroup exclusiveXSec
       */
      class IncoherentDVCSXSec : public DiffXSec {

         protected:

            struct IncoherentKinematics {
               TRotation         fR_phi;     // Rotation (in lab frame) around z to remove p1's y component
               TLorentzRotation  fLambda_p1; // Boost to the p1 rest frame (A)
               TRotation         fR_k1;      // Rotation (in the A frame) to align k1 with z (producing the A' coordinates).
               TRotation         fR_q1;      // Rotation (in the A' frame) to align q with z (producing the B frame).
               TVector3          fn_gamma;   // unit vector n_gamma = q x k1
               TLorentzVector    fp1;
               TLorentzVector    fp2;
               TLorentzVector    fq1;
               TLorentzVector    fq2;
               TLorentzVector    fk1;
               TLorentzVector    fk2;
            };
            mutable IncoherentKinematics fKine;
            FermiMomentumDist    fFermiDist;

            Nucleus     fActiveTargetFragment;
            Nucleus     fSpectatorTargetFragment;

            double            fOffShellMass            = 0.91897; // M(4He)-M(3H)

            struct dvcsvar_t { double Q2  , t    , x , phi; };
            struct dvcspar_t {
               double Q2_0   ;//= 1.0;
               double alpha  ;//= 2.5;
               double b      ;//= -11.0;
               double beta   ;//=  12.0;
               double xc     ;//=   0.2;
               double c      ;//=   0.2;
               double d      ;//=   0.4;
            };
            //______________________________________________________________________________
            //                           Q20  alpha      b    beta    xc   c     d
            dvcspar_t PAR_COHDVCS   = {  1.0,  2.5,  -11.0,    12.0, 0.2, 0.2,  0.4}; // b,beta from t-fit to tc
            dvcspar_t PAR_INCDVCS   = {  1.0,  1.5,  -1.408,   4.0,  0.2, 0.2,  0.4};
            //______________________________________________________________________________

            // What are the cross section units?
            double xsec_e1dvcs(const dvcsvar_t &x,const dvcspar_t &p) const;


         public:
            IncoherentDVCSXSec();
            virtual ~IncoherentDVCSXSec();
            IncoherentDVCSXSec(const IncoherentDVCSXSec& rhs)            = default;
            IncoherentDVCSXSec& operator=(const IncoherentDVCSXSec& rhs) = default;
            IncoherentDVCSXSec(IncoherentDVCSXSec&&)                     = default;
            IncoherentDVCSXSec& operator=(IncoherentDVCSXSec&&) &        = default;

            void    SetRecoilNucleus(const Nucleus& spectator);
            void    SetRecoilNucleus(const Nucleus& spectator, const Nucleus& active);
            virtual void    SetTargetNucleus(const Nucleus& target);

            virtual IncoherentDVCSXSec*  Clone(const char * newname) const {
               std::cout << "IncoherentDVCSXSec::Clone()\n";
               auto * copy = new IncoherentDVCSXSec();
               (*copy) = (*this);
               return copy;
            }
            virtual IncoherentDVCSXSec*  Clone() const { return( Clone("") ); } 

            virtual void       InitializePhaseSpaceVariables() ;
            virtual void       DefineEvent(Double_t * vars);
            virtual Double_t * GetDependentVariables(const Double_t * x) const ;
            virtual Double_t   EvaluateXSec(const Double_t * x) const;
            virtual Double_t   GetBeamSpinAsymmetry(const Double_t * x) const;

            /**  Returns the scattered electron energy using the angle. */
            Double_t GetEPrime(const Double_t theta) const ;

            virtual Double_t Jacobian(const Double_t * vars) const;

            //double DVCSKinematics::Gett_max() const 
            //{
            //   double m  = GetM1();
            //   double Q2 = GetQ2();
            //   double x  = Getx();
            //   double W2 = GetW2();
            //   double W  = TMath::Sqrt(W2);
            //   double v1LAB = fK1.E();
            //   double k1LAB = fK1.Vect().Mag();
            //   double k1 = k1LAB*(m/W);
            //   double v1 = W - TMath::Sqrt(k1*k1 + m*m);
            //   double tmax    = -Q2 - 2.0*(v1 + k1)*(W2 - m*m)/(2.0*W);
            //   double minus_s = -W2;
            //   double res = tmax;
            //   if( res < minus_s ) {
            //      res = minus_s;
            //   }
            //   return res;
            //}
            ClassDef(IncoherentDVCSXSec, 1)
      };

   }
}

#endif

