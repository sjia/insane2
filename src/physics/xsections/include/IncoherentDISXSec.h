#ifndef IncoherentDISXSec_H
#define IncoherentDISXSec_H 1

#include "DiffXSec.h"
#include "TLorentzVector.h"
#include "TLorentzRotation.h"
#include "TRotation.h"
#include "FermiMomentumDist.h"
#include "Nucleus.h"

namespace insane {
   namespace physics {

      /** Incoherent DIS.
       * \ingroup exclusiveXSec
       */
      class IncoherentDISXSec : public DiffXSec {

         protected:

            DiffXSec * fActiveXSec  = nullptr; 

            struct IncoherentKinematics {
               TRotation         fR_phi;     // Rotation (in lab frame) around z to remove p1's y component
               TLorentzRotation  fLambda_p1; // Boost to the p1 rest frame (A)
               TRotation         fR_k1;      // Rotation (in the A frame) to align k1 with z (producing the A' coordinates).
               TRotation         fR_q1;      // Rotation (in the A' frame) to align q with z (producing the B frame).
               TVector3          fn_gamma;   // unit vector n_gamma = q x k1
               TLorentzVector    fp1;
               TLorentzVector    fp2;
               TLorentzVector    fq1;
               TLorentzVector    fq2;
               TLorentzVector    fk1;
               TLorentzVector    fk2;
            };
            mutable IncoherentKinematics fKine;
            FermiMomentumDist            fFermiDist;

            Nucleus                fActiveTargetFragment;
            Nucleus                fSpectatorTargetFragment;

         public:
            IncoherentDISXSec();
            virtual ~IncoherentDISXSec();
            IncoherentDISXSec(const IncoherentDISXSec& rhs)            = default;
            IncoherentDISXSec& operator=(const IncoherentDISXSec& rhs) = default;
            IncoherentDISXSec(IncoherentDISXSec&&)                     = default;
            IncoherentDISXSec& operator=(IncoherentDISXSec&&) &        = default;

            virtual IncoherentDISXSec*  Clone(const char * newname) const {
               std::cout << "IncoherentDISXSec::Clone()\n";
               auto * copy = new IncoherentDISXSec();
               (*copy) = (*this);
               return copy;
            }
            virtual IncoherentDISXSec*  Clone() const { return( Clone("") ); } 

            void    SetRecoilNucleus(const Nucleus& spectator);
            void    SetRecoilNucleus(const Nucleus& spectator, const Nucleus& active);
            virtual void    SetTargetNucleus(const Nucleus& target);
            


            virtual void       InitializePhaseSpaceVariables() ;
            virtual void       DefineEvent(Double_t * vars);
            virtual Double_t * GetDependentVariables(const Double_t * x) const ;
            virtual Double_t   EvaluateXSec(const Double_t * x) const;

            ClassDef(IncoherentDISXSec, 1)
      };

   }
}

#endif

