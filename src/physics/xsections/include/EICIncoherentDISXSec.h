#ifndef EICIncoherentDISXSec_H
#define EICIncoherentDISXSec_H 1

#include "DiffXSec.h"
#include "TLorentzVector.h"
#include "TLorentzRotation.h"
#include "TRotation.h"
#include "FermiMomentumDist.h"
#include "Nucleus.h"

namespace insane {
  namespace physics {
    namespace eic {

      /** Incoherent DIS.
       * \ingroup exclusiveXSec
       */
      class EICIncoherentDISXSec : public DiffXSec {

        protected:

          DiffXSec * fActiveXSec  = nullptr; 

          struct IncoherentKinematics {
            TRotation         fR_phi;     // Rotation (in lab frame) around z to remove p1's y component
            TLorentzRotation  fLambda_p1; // Boost to the p1 rest frame (A)
            TRotation         fR_k1;      // Rotation (in the A frame) to align k1 with z (producing the A' coordinates).
            TRotation         fR_q1;      // Rotation (in the A' frame) to align q with z (producing the B frame).
            TVector3          fn_gamma;   // unit vector n_gamma = q x k1
            TLorentzVector    fP0;
            TLorentzVector    fp1;
            TLorentzVector    fp2;
            TLorentzVector    fq1;
            TLorentzVector    fq2;
            TLorentzVector    fk1;
            TLorentzVector    fk2;
            TLorentzRotation  fLambda_EIC_Lab; // from nucleon at rest to EIC lab frame
            TLorentzVector    fk1_Nrest;
            TLorentzVector    fk2_Nrest;
            TLorentzVector    fp1_Nrest;
            TLorentzVector    fk1_Arest;
            TLorentzVector    fk2_Arest;
            TLorentzVector    fp1_Arest;
          };
          mutable IncoherentKinematics fKine;
          FermiMomentumDist            fFermiDist;

          Nucleus                fActiveTargetFragment;
          Nucleus                fSpectatorTargetFragment;

        public:
          EICIncoherentDISXSec();
          virtual ~EICIncoherentDISXSec();
          EICIncoherentDISXSec(const EICIncoherentDISXSec& rhs)            = default;
          EICIncoherentDISXSec& operator=(const EICIncoherentDISXSec& rhs) = default;
          EICIncoherentDISXSec(EICIncoherentDISXSec&&)                     = default;
          EICIncoherentDISXSec& operator=(EICIncoherentDISXSec&&) &        = default;

          virtual EICIncoherentDISXSec*  Clone(const char * newname) const {
            std::cout << "EICIncoherentDISXSec::Clone()\n";
            auto * copy = new EICIncoherentDISXSec();
            (*copy) = (*this);
            return copy;
          }
          virtual EICIncoherentDISXSec*  Clone() const { return( Clone("") ); } 

          void    SetRecoilNucleus(const Nucleus& spectator);
          void    SetRecoilNucleus(const Nucleus& spectator, const Nucleus& active);
          virtual void    SetTargetNucleus(const Nucleus& target);
          virtual void    SetIonEnergy(double P0, double M0 = 0.938);

          const IncoherentKinematics& GetKine() const { return fKine; }


          virtual void       InitializePhaseSpaceVariables() ;
          virtual void       DefineEvent(Double_t * vars);
          virtual Double_t * GetDependentVariables(const Double_t * x) const ;
          virtual Double_t   EvaluateXSec(const Double_t * x) const;

          ClassDef(EICIncoherentDISXSec, 1)
      };

    }
  }
}

#endif

