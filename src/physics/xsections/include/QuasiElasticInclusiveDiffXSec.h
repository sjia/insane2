#ifndef QuasiElasticInclusiveDiffXSec_H
#define QuasiElasticInclusiveDiffXSec_H 1

#include "ExclusiveDiffXSec.h"
#include "InclusiveDiffXSec.h"
#include "TMath.h"
#include "FormFactors.h"
#include "TVector3.h"
#include "QuasiElasticInclusiveDiffXSec.h"


namespace insane {
   namespace physics {

/** Elastic scattering using the Dipole FFs.
 * \ingroup exclusiveXSec
 */
class QuasiElasticInclusiveDiffXSec : public ExclusiveDiffXSec {

   public:
      QuasiElasticInclusiveDiffXSec();
      virtual ~QuasiElasticInclusiveDiffXSec();
            QuasiElasticInclusiveDiffXSec(const QuasiElasticInclusiveDiffXSec& rhs)            = default;
            QuasiElasticInclusiveDiffXSec& operator=(const QuasiElasticInclusiveDiffXSec& rhs) = default;
            QuasiElasticInclusiveDiffXSec(QuasiElasticInclusiveDiffXSec&&)                     = default;
            QuasiElasticInclusiveDiffXSec& operator=(QuasiElasticInclusiveDiffXSec&&)          = default;

      virtual QuasiElasticInclusiveDiffXSec*  Clone(const char * newname) const {
         std::cout << "QuasiElasticInclusiveDiffXSec::Clone()\n";
         auto * copy = new QuasiElasticInclusiveDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual QuasiElasticInclusiveDiffXSec*  Clone() const { return( Clone("") ); } 

      virtual void InitializePhaseSpaceVariables();

      Double_t GetEPrime(const Double_t theta) const ;
      Double_t GetTheta(double ep) const ;

      virtual Double_t * GetDependentVariables(const Double_t * x) const ;

      virtual Double_t   EvaluateBaseXSec(const Double_t * x) const;
      virtual Double_t   EvaluateXSec(const Double_t * x) const;
      virtual void       DefineEvent(Double_t * vars);

      ClassDef(QuasiElasticInclusiveDiffXSec, 1)
};
}
}


#endif

