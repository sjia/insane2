#ifndef ElectronProtonElasticDiffXSec_H
#define ElectronProtonElasticDiffXSec_H 1

#include "TMath.h"
#include "FormFactors.h"
#include "TVector3.h"
#include "QuasiElasticInclusiveDiffXSec.h"
#include "ExternalRadiator.h"

namespace insane {
   namespace physics {
/** Elastic scattering using the Dipole FFs.
 * \ingroup exclusiveXSec
 */
class ElectronProtonElasticDiffXSec : public ExternalRadiator<QuasiElasticInclusiveDiffXSec> {
   protected:

   public:
      ElectronProtonElasticDiffXSec();
      virtual ~ElectronProtonElasticDiffXSec();
      ElectronProtonElasticDiffXSec(const ElectronProtonElasticDiffXSec& rhs)            = default;
      ElectronProtonElasticDiffXSec& operator=(const ElectronProtonElasticDiffXSec& rhs) = default;
      ElectronProtonElasticDiffXSec(ElectronProtonElasticDiffXSec&&)                     = default;
      ElectronProtonElasticDiffXSec& operator=(ElectronProtonElasticDiffXSec&&)          = default;

      virtual ElectronProtonElasticDiffXSec*  Clone(const char * newname) const {
         std::cout << "ElectronProtonElasticDiffXSec::Clone()\n";
         auto * copy = new ElectronProtonElasticDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual ElectronProtonElasticDiffXSec*  Clone() const { return( Clone("") ); } 

      virtual void InitializePhaseSpaceVariables();

      virtual Double_t * GetDependentVariables(const Double_t * x) const ;

      virtual Double_t   EvaluateXSec(const Double_t * x) const;
      //virtual void       DefineEvent(Double_t * vars);

      //DiffXSec * GetBornXSec() { return fElasticDiffXSec; }

      double GetTheta_e(double Es, double alpha) const;
      double GetPalpha(double Es, double alpha) const;
      double ElectronToAlphaJacobian(double Es, double alpha) const ;
      double Ib(Double_t E0,Double_t E,Double_t t) const ;
      double GetEs(double Ep, double theta) const ;

      double Sig_el(double Es, double Ep, double theta) const;

      ClassDef(ElectronProtonElasticDiffXSec, 1)
};
}
}


#endif

