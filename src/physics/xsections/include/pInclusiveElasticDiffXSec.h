#ifndef pInclusiveElasticDiffXSec_HH
#define pInclusiveElasticDiffXSec_HH

#include "InclusiveDiffXSec.h"
namespace insane {
namespace physics {

/** Elastic scattering using the Dipole FFs where only the proton is detected.
 *
 * \ingroup exclusiveXSec
 */
class pInclusiveElasticDiffXSec : public InclusiveDiffXSec {
   public:
      pInclusiveElasticDiffXSec();

      virtual ~pInclusiveElasticDiffXSec() ;
      virtual pInclusiveElasticDiffXSec*  Clone(const char * newname) const ;
      virtual pInclusiveElasticDiffXSec*  Clone() const { return( Clone("") ); } 

      void InitializePhaseSpaceVariables();

      /** Virtual method that returns the calculated values of dependent variables. This method
       *  should be overridden for exclusive cross sections in order to conserve momentum and energy!
       *
       *  For example, in mott scattering (elastic) there is really only two random variables,
       *  the the scattered angles theta and phi. The rest can be calculated from these two angles
       *  (assuming the beam energy is known).
       *
       */
      virtual Double_t * GetDependentVariables(const Double_t * x) const {
         TVector3 k1(0, 0, fBeamEnergy); // incident electron
         TVector3 p2(0, 0, 0);          // scattered electron
         p2.SetMagThetaPhi(x[0], x[1], x[2]);
         TVector3 k2 = k1 - p2; // recoil proton
         //       k1.Print();
         //       k2.Print();
         //       p2.Print();
         fDependentVariables[0] = k2.Mag();
         fDependentVariables[1] = k2.Theta();
         fDependentVariables[2] = k2.Phi();
         fDependentVariables[3] = p2.Mag();//TMath::Sqrt(p2.Mag2()+0.938*0.938);
         fDependentVariables[4] = p2.Theta();
         fDependentVariables[5] = p2.Phi();
         /*      if( p2.Phi() <0.0) fDependentVariables[5] = p2.Phi() + 2.0*TMath::Pi() ;*/
         return(fDependentVariables);
      }

      /**  Returns the scattered electron energy using the angle.
      */
      Double_t GetEPrime(const Double_t theta)const  {
         Double_t M  = fTargetNucleus.GetMass();//M_p/GeV;
         return (fBeamEnergy / (1.0 + 2.0 * fBeamEnergy / M * TMath::Power(TMath::Sin(theta / 2.0), 2)));
      }


      /** Evaluate Cross Section (mbarn/sr) */
      virtual Double_t EvaluateXSec(const Double_t * x) const;

      virtual Double_t Density(Int_t ndim, Double_t * x) {
         return(EvaluateXSec(GetDependentVariables(GetUnnormalizedVariables(x))));
      }

      //FormFactors * fDipoleFFs;

      /** Returns scattering angle as a function of energy. */
      Double_t GetTheta(const Double_t Ep) const {
         Double_t E0 = GetBeamEnergy();
         Double_t M  = fTargetNucleus.GetMass();//M_p/GeV;
        // Double_t M  = M_p/GeV;
         return( 2.0*TMath::ASin(TMath::Sqrt((M/2.0)*(1.0/Ep - 1.0/E0) )) );
      }
      /** Cross section as a function of energy.
       *  \param x[0] = eprime 
       *  \param p[0] = phi 
       */
      double EnergyDependentXSec(double *x, double *p) {
         Double_t y[3];
         y[0] = GetTheta(x[0]);
         y[1] = p[0];
         return(EvaluateXSec(GetDependentVariables(y)));
      }

      ClassDef(pInclusiveElasticDiffXSec, 1)
};
}}
#endif

