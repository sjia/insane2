#ifndef DVCSDiffXSec_H
#define DVCSDiffXSec_H 1

#include "ExclusiveDiffXSec.h"
#include "InclusiveDiffXSec.h"
#include "TMath.h"
#include "FormFactors.h"
#include "TVector3.h"


namespace insane {
   namespace physics {

      /** Elastic scattering using the Dipole FFs.
       * \ingroup exclusiveXSec
       */
      class DVCSDiffXSec : public ExclusiveDiffXSec {

         protected:

            struct dvcsvar_t { double Q2  , t    , x , phi; };
            struct dvcspar_t { double Q2_0, alpha, b , beta    , c , d; };
            //______________________________________________________________________________
            //                                 q20  alpha      b     beta    c     d
            //const dvcspar_t PAR_COHDVCS = {  1.0,  2.0,  -8.8,     7.3,   0.3,  0.4}; // b,beta from t-fit to tr
            dvcspar_t PAR_COHDVCS   = {  1.0,  2.0,  -0.667,  35.1,   0.3,  0.4}; // b,beta from t-fit to tc
            //const dvcspar_t PAR_INCDVCS   = {  1.0,  2.0,  -1.408,   4.0,   0.3,  0.4};
            dvcspar_t PAR_INCDVCS   = {  1.0,  1.5,  -1.408,   4.0,   0.2,  0.4};
            dvcspar_t PAR_COHPI0    = {  1.0,  3.0,  -8.8,     7.3,   0.3,  0.0};
            dvcspar_t PAR_INCPI0    = {  1.0,  3.0,  -1.408,   4.0,   0.3,  0.0};

            // What are the cross section units?
            double xsec_e1dvcs(const dvcsvar_t &x,const dvcspar_t &p) const;

         public:
            DVCSDiffXSec();
            virtual ~DVCSDiffXSec();
            DVCSDiffXSec(const DVCSDiffXSec& rhs) = default ;
            DVCSDiffXSec& operator=(const DVCSDiffXSec& rhs) = default ;
            DVCSDiffXSec(DVCSDiffXSec&&) = default;
            DVCSDiffXSec& operator=(DVCSDiffXSec&&) & = default; // Move assignment operator

            virtual DVCSDiffXSec*  Clone(const char * newname) const {
               std::cout << "DVCSDiffXSec::Clone()\n";
               auto * copy = new DVCSDiffXSec();
               (*copy) = (*this);
               return copy;
            }
            virtual DVCSDiffXSec*  Clone() const { return( Clone("") ); } 

            virtual void       InitializePhaseSpaceVariables() ;
            virtual void       DefineEvent(Double_t * vars);
            virtual Double_t * GetDependentVariables(const Double_t * x) const ;
            virtual Double_t   EvaluateXSec(const Double_t * x) const;
            virtual Double_t   Jacobian(const Double_t * vars) const;
            virtual Double_t   GetBeamSpinAsymmetry(const Double_t * x) const;

            /**  Returns the scattered electron energy using the angle. */
            Double_t GetEPrime(const Double_t theta) const ;

            ClassDef(DVCSDiffXSec, 1)
      };

   }
}

#endif

