#ifndef MAIDPolarizedKinematicKey_HH
#define MAIDPolarizedKinematicKey_HH 1

#include <iostream>
#include "TObject.h"
#include "TOrdCollection.h"
namespace insane {
namespace physics {

class MAIDPolarizedKinematicKey : public TObject {
   public:
      Int_t    fNum;
      Double_t fEbeam;
      Double_t fEprime;
      Double_t fTheta;
      Double_t fPhi;
      Double_t fepsilon; 
      Double_t fW; 
      Double_t fQ2;  
      Double_t fSigma_0;
      Double_t fA_e;
      Double_t fA_1;
      Double_t fA_2;
      Double_t fA_3;
      Double_t fA_e1;
      Double_t fA_e2;
      Double_t fA_e3;

      MAIDPolarizedKinematicKey(Int_t i = 0) {
         fNum     = i;
         fEbeam   = 0.0;
         fEprime  = 0.0;
         fTheta   = 0.0;
         fPhi     = 0.0;
         fW       = 0.0; 
         fQ2      = 0.0;
         fepsilon = 0.0;  
         fSigma_0 = 0.0; 
         fA_e     = 0.0; 
         fA_1     = 0.0; 
         fA_2     = 0.0; 
         fA_3     = 0.0; 
         fA_e1    = 0.0; 
         fA_e2    = 0.0; 
         fA_e3    = 0.0; 
      }

      virtual ~MAIDPolarizedKinematicKey() {}

      void Print() { 
         std::cout << fNum     << "\t" 
                   << fEbeam   << "\t"
                   << fEprime  << "\t"
                   << fepsilon << "\t"
                   << fTheta   << "\t"
                   << fPhi     << "\t" 
                   << fW       << "\t" 
                   << fQ2      << std::endl; 
      }
      //ULong_t Hash() const { return fNum; }
      Bool_t IsEqual(const TObject * obj) const { 
         auto * aKey = (MAIDPolarizedKinematicKey*)obj;
         if(aKey->fepsilon != fepsilon) return false;
         if(aKey->fQ2      != fQ2)      return false;
         if(aKey->fW       != fW)       return false;
         if(aKey->fTheta   != fTheta)   return false;
         if(aKey->fPhi     != fPhi)     return false;
         return(true);
      }

      Bool_t  IsSortable() const { return kTRUE; }

      // Int_t   Compare0(const TObject *obj) const {
      //    MAIDPolarizedKinematicKey * aKey = (MAIDPolarizedKinematicKey*)obj;
      //    // Order first by beam energy
      //    if(aKey->fEbeam  == fEbeam)  return 0;
      //    if(aKey->fEbeam  > fEbeam)  return -1;
      //    /*if(aKey->fEbeam  < fEbeam)*/  return  1;
      //    Error("Compare","Should not have made it here!");
      //    return -1;
      // }
      // Int_t   Compare1(const TObject *obj) const {
      //    MAIDPolarizedKinematicKey * aKey = (MAIDPolarizedKinematicKey*)obj;
      //    // Order first by beam energy
      //    if(aKey->fEprime  == fEprime)  return 0;
      //    if(aKey->fEprime   > fEprime)  return -1;
      //    /*if(aKey->fEprime  < fEprime)*/  return  1;
      //    Error("Compare","Should not have made it here!");
      //    return -1;
      // }
      // Int_t   Compare2(const TObject *obj) const {
      //    MAIDPolarizedKinematicKey * aKey = (MAIDPolarizedKinematicKey*)obj;
      //    // Order first by beam energy
      //    if(aKey->fTheta  == fTheta)  return 0;
      //    if(aKey->fTheta   > fTheta)  return -1;
      //    /*if(aKey->fEprime  < fEprime)*/  return  1;
      //    Error("Compare","Should not have made it here!");
      //    return -1;
      // }
      // Int_t   Compare3(const TObject *obj) const {
      //    MAIDPolarizedKinematicKey * aKey = (MAIDPolarizedKinematicKey*)obj;
      //    // Order first by beam energy
      //    if(aKey->fPhi  == fPhi)  return 0;
      //    if(aKey->fPhi   > fPhi)  return -1;
      //    /*if(aKey->fEprime  < fEprime)*/  return  1;
      //    Error("Compare","Should not have made it here!");
      //    return -1;
      // }

      Int_t   Compare(const TObject *obj) const {
         if( IsEqual(obj) ) return (0);
         auto * aKey = (MAIDPolarizedKinematicKey*)obj;
         // Order first by epsilon 
         if(aKey->fepsilon > fepsilon) return -1;
         if(aKey->fepsilon < fepsilon) return 1;
         // next Q2 
         if(aKey->fQ2  > fQ2)          return -1;
         if(aKey->fQ2  < fQ2)          return 1;
         // next by W 
         if(aKey->fW > fW)             return -1;
         if(aKey->fW < fW)             return 1;
         // next by theta  
         if(aKey->fTheta  > fTheta)    return -1;
         if(aKey->fTheta  < fTheta)    return 1;
         // next phi
         if(aKey->fPhi    > fPhi)      return -1;
         if(aKey->fPhi    < fPhi)      return 1;
         Error("Compare","Should not have made it here!");
         return -1;
      }

   ClassDef(MAIDPolarizedKinematicKey,1)
};
}}
#endif

