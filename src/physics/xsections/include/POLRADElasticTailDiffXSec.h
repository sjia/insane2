#ifndef POLRADElasticTailDiffXSec_HH
#define POLRADElasticTailDiffXSec_HH

#include "POLRADBornDiffXSec.h"
namespace insane {
namespace physics {

/** Elastic Raditaive Tail. 
 *
 *
 * \ingroup inclusiveXSec
 */
class POLRADElasticTailDiffXSec: public POLRADBornDiffXSec {
   public:
      POLRADElasticTailDiffXSec();
      virtual ~POLRADElasticTailDiffXSec();
      virtual POLRADElasticTailDiffXSec*  Clone(const char * newname) const {
         std::cout << "POLRADElasticTailDiffXSec::Clone()\n";
         auto * copy = new POLRADElasticTailDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual POLRADElasticTailDiffXSec*  Clone() const { return( Clone("") ); } 
      virtual Double_t EvaluateXSec(const Double_t *x) const ; 

      ClassDef(POLRADElasticTailDiffXSec,1)
};
}}

#endif

