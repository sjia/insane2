#ifndef MollerDiffXSec_H
#define MollerDiffXSec_H 1

#include "ExclusiveDiffXSec.h"
#include "InclusiveDiffXSec.h"
#include "TMath.h"
#include "FormFactors.h"
#include "TVector3.h"

namespace insane {
namespace physics {

/** Moller scattering using the Dipole FFs.
 * \ingroup exclusiveXSec
 */
class MollerDiffXSec : public ExclusiveDiffXSec {

   public:
      MollerDiffXSec();
      virtual ~MollerDiffXSec();

      virtual MollerDiffXSec*  Clone(const char * newname) const {
         std::cout << "MollerDiffXSec::Clone()\n";
         auto * copy = new MollerDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual MollerDiffXSec*  Clone() const { return( Clone("") ); } 

      /** Virtual method that returns the calculated values of dependent variables. This method
       *  should be overridden for exclusive cross sections in order to conserve momentum and energy!
       *
       *  For example, in mott scattering (elastic) there is really only two random variables,
       *  the the scattered angles theta and phi. The rest can be calculated from these two angles
       *  (assuming the beam energy is known).
       *
       */
      virtual Double_t * GetDependentVariables(const Double_t * x) const ;

      virtual void InitializePhaseSpaceVariables();

      double  MollerCrossSection_CM(double E0, double sin2th) const ;

      /** Evaluate Cross Section (mbarn/sr) */
      virtual Double_t EvaluateXSec(const Double_t * x) const;

      ClassDef(MollerDiffXSec, 1)
};


///** Moller scattering.
// * \ingroup inclusiveXSec
// */
//class InclusiveMollerDiffXSec : public InclusiveDiffXSec {
//
//   public:
//      InclusiveMollerDiffXSec();
//      virtual ~InclusiveMollerDiffXSec();
//
//      virtual InclusiveMollerDiffXSec*  Clone(const char * newname) const {
//         std::cout << "InclusiveMollerDiffXSec::Clone()\n";
//         MollerDiffXSec * copy = new MollerDiffXSec();
//         (*copy) = (*this);
//         return copy;
//      }
//      virtual InclusiveMollerDiffXSec*  Clone() const { return( Clone("") ); } 
//
//      /** Virtual method that returns the calculated values of dependent variables. This method
//       *  should be overridden for exclusive cross sections in order to conserve momentum and energy!
//       *
//       *  For example, in mott scattering (elastic) there is really only two random variables,
//       *  the the scattered angles theta and phi. The rest can be calculated from these two angles
//       *  (assuming the beam energy is known).
//       *
//       */
//      virtual Double_t * GetDependentVariables(const Double_t * x) const ;
//
//      virtual void InitializePhaseSpaceVariables();
//
//      double  MollerCrossSection_CM(double E0, double sin2th) const ;
//
//      /** Evaluate Cross Section (mbarn/sr) */
//      virtual Double_t EvaluateXSec(const Double_t * x) const;
//
//      ClassDef(InclusiveMollerDiffXSec, 1)
//};
}}

#endif

