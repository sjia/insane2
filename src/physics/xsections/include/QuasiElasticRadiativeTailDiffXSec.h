#ifndef QuasiElasticRadiativeTailDiffXSec_H
#define QuasiElasticRadiativeTailDiffXSec_H 1

#include "TMath.h"
#include "FormFactors.h"
#include "TVector3.h"
#include "QuasiElasticInclusiveDiffXSec.h"
#include "ExternalRadiator.h"

namespace insane {
   namespace physics {
/** Elastic scattering using the Dipole FFs.
 * \ingroup exclusiveXSec
 */
class QuasiElasticRadiativeTailDiffXSec : public ExternalRadiator<QuasiElasticInclusiveDiffXSec> {
   protected:

   public:
      QuasiElasticRadiativeTailDiffXSec();
      virtual ~QuasiElasticRadiativeTailDiffXSec();
      QuasiElasticRadiativeTailDiffXSec(const QuasiElasticRadiativeTailDiffXSec& rhs)            = default;
      QuasiElasticRadiativeTailDiffXSec& operator=(const QuasiElasticRadiativeTailDiffXSec& rhs) = default;
      QuasiElasticRadiativeTailDiffXSec(QuasiElasticRadiativeTailDiffXSec&&)                     = default;
      QuasiElasticRadiativeTailDiffXSec& operator=(QuasiElasticRadiativeTailDiffXSec&&)          = default;

      virtual QuasiElasticRadiativeTailDiffXSec*  Clone(const char * newname) const {
         std::cout << "QuasiElasticRadiativeTailDiffXSec::Clone()\n";
         auto * copy = new QuasiElasticRadiativeTailDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual QuasiElasticRadiativeTailDiffXSec*  Clone() const { return( Clone("") ); } 

      virtual void InitializePhaseSpaceVariables();

      virtual Double_t * GetDependentVariables(const Double_t * x) const ;

      virtual Double_t   EvaluateXSec(const Double_t * x) const;
      //virtual void       DefineEvent(Double_t * vars);

      //DiffXSec * GetBornXSec() { return fElasticDiffXSec; }

      double GetTheta_e(double Es, double alpha) const;
      double GetPalpha(double Es, double alpha) const;
      double ElectronToAlphaJacobian(double Es, double alpha) const ;
      double Ib(Double_t E0,Double_t E,Double_t t) const ;

      ClassDef(QuasiElasticRadiativeTailDiffXSec, 1)
};
}
}


#endif

