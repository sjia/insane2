#ifndef Helium4ElasticRadiativeTailDiffXSec_H
#define Helium4ElasticRadiativeTailDiffXSec_H 1

#include "TMath.h"
#include "FormFactors.h"
#include "TVector3.h"
#include "Helium4ElasticDiffXSec.h"
#include "ExternalRadiator.h"

namespace insane {
   namespace physics {
/** Elastic scattering using the Dipole FFs.
 * \ingroup exclusiveXSec
 */
class Helium4ElasticRadiativeTailDiffXSec : public ExternalRadiator<Helium4ElasticDiffXSec> {
   protected:

   public:
      Helium4ElasticRadiativeTailDiffXSec();
      virtual ~Helium4ElasticRadiativeTailDiffXSec();
      Helium4ElasticRadiativeTailDiffXSec(const Helium4ElasticRadiativeTailDiffXSec& rhs)            = default;
      Helium4ElasticRadiativeTailDiffXSec& operator=(const Helium4ElasticRadiativeTailDiffXSec& rhs) = default;
      Helium4ElasticRadiativeTailDiffXSec(Helium4ElasticRadiativeTailDiffXSec&&)                     = default;
      Helium4ElasticRadiativeTailDiffXSec& operator=(Helium4ElasticRadiativeTailDiffXSec&&)          = default;

      virtual Helium4ElasticRadiativeTailDiffXSec*  Clone(const char * newname) const {
         std::cout << "Helium4ElasticRadiativeTailDiffXSec::Clone()\n";
         auto * copy = new Helium4ElasticRadiativeTailDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual Helium4ElasticRadiativeTailDiffXSec*  Clone() const { return( Clone("") ); } 

      virtual void InitializePhaseSpaceVariables();

      virtual Double_t * GetDependentVariables(const Double_t * x) const ;

      virtual Double_t   EvaluateXSec(const Double_t * x) const;
      //virtual void       DefineEvent(Double_t * vars);

      //DiffXSec * GetBornXSec() { return fElasticDiffXSec; }

      double GetTheta_e(double Es, double alpha) const;
      double GetPalpha(double Es, double alpha) const;
      double ElectronToAlphaJacobian(double Es, double alpha) const ;
      double Ib(Double_t E0,Double_t E,Double_t t) const ;

      ClassDef(Helium4ElasticRadiativeTailDiffXSec, 1)
};
}
}


#endif

