#ifndef MAIDExclusivePionDiffXSec2_H 
#define MAIDExclusivePionDiffXSec2_H 1

#include <cstdlib> 
#include <iostream> 
#include <cmath> 
#include <vector> 
#include "TVector3.h"
#include "TRotation.h"
#include "TString.h"
#include "MAIDPolarizedKinematicKey.h"
#include "MAIDExclusivePionDiffXSec.h"


namespace insane {
namespace physics {
/** MAID cross section for (polarized target) threshold (exlusive) pion production.
 * 
 * Paper reference: J. Phys. G: Nucl. Part. Phys 18, 449 (1992)  
 * For the reaction e N --> e' N' pi, where the user specifies pi and N'. 
 * Uses data from the MAID 2007 calculations: polarized target.
 * 
 *
 * \ingroup exclusiveXSec
 */ 
class MAIDExclusivePionDiffXSec2: public MAIDExclusivePionDiffXSec {

   protected:

      Double_t JacobianEprimeOverEpion1(Double_t E1,Double_t w,Double_t the,Double_t thpi,Double_t M1,Double_t M2,Double_t Mpi ) const ;
      Double_t JacobianEprimeOverEpion2(Double_t E1,Double_t w,Double_t the,Double_t thpi,Double_t M1,Double_t M2,Double_t Mpi ) const ;
      Double_t Eprime1(Double_t E1,Double_t w,Double_t the,Double_t thpi,Double_t M1,Double_t M2,Double_t Mpi ) const ;
      Double_t Eprime2(Double_t E1,Double_t w,Double_t the,Double_t thpi,Double_t M1,Double_t M2,Double_t Mpi ) const ;

      Double_t EprimeLab(Double_t E1,Double_t w,Double_t theta_e,Double_t theta_pi, Double_t delta_phi,Double_t M1,Double_t M2,Double_t Mpi ) const {
         using namespace TMath;
         Double_t cosDeltaPhi= Cos(delta_phi);
         Double_t cosThetaPi = Cos(theta_pi);
         Double_t cosThetaE  = Cos(theta_e);
         Double_t sinThetaPi = Sin(theta_pi);
         Double_t sinThetaE  = Sin(theta_e);
         Double_t sin2the    = Power(Sin(theta_e/2.0),2.0);
         Double_t res = (Power(M1,2) - Power(M2,2) + Power(Mpi,2) - 2*M1*w + 2*E1*(M1 - w + cosThetaPi*Sqrt(Power(w,2)-Power(Mpi,2))))/
               (2.*(M1 + 2*E1*sin2the - w + cosThetaE*cosThetaPi*Sqrt(Power(w,2)-Power(Mpi,2)) - cosDeltaPhi*sinThetaE*sinThetaPi*Sqrt(Power(w,2)-Power(Mpi,2))));
         return res;
      }
      Double_t JacobianEprimeOverEpion_2(Double_t E1,Double_t w,Double_t theta_e,Double_t theta_pi, Double_t delta_phi,Double_t M1,Double_t M2,Double_t Mpi ) const {
         using namespace TMath;
         Double_t cosDeltaPhi= Cos(delta_phi);
         Double_t cosThetaPi = Cos(theta_pi);
         Double_t cosThetaE  = Cos(theta_e);
         Double_t sinThetaPi = Sin(theta_pi);
         Double_t sinThetaE  = Sin(theta_e);
         Double_t sin2the    = Power(Sin(theta_e/2.0),2.0);
         Double_t res = Abs(((M1 + 2*E1*sin2the - w + (cosThetaE*cosThetaPi - cosDeltaPhi*sinThetaE*sinThetaPi)*
                     Sqrt(-Power(Mpi,2) + Power(w,2)))*
                  (-2*M1 + E1*(-2 + (2*cosThetaPi*w)/Sqrt(-Power(Mpi,2) + Power(w,2)))) - 
                  (-1 + ((cosThetaE*cosThetaPi - cosDeltaPhi*sinThetaE*sinThetaPi)*w)/
                   Sqrt(-Power(Mpi,2) + Power(w,2)))*
                  (Power(M1,2) - Power(M2,2) + Power(Mpi,2) - 2*M1*w + 
                   2*E1*(M1 - w + cosThetaPi*Sqrt(-Power(Mpi,2) + Power(w,2)))))/
               Power(M1 + 2*E1*sin2the - w + (cosThetaE*cosThetaPi - cosDeltaPhi*sinThetaE*sinThetaPi)*
                  Sqrt(-Power(Mpi,2) + Power(w,2)),2))/2.0;
         //Double_t res = Abs(((-Power(M1,2) - Power(M2,2) + Power(Mpi,2) - 4*Power(E1,2)*sin2the - 4*E1*M1*sin2the)*Sqrt(Power(Mpi,2) + Power(w,2)) + 
         //                cosDeltaPhi*sinThetaE*sinThetaPi*(2*M1*Power(Mpi,2) + Power(M1,2)*w - Power(M2,2)*w + Power(Mpi,2)*w + 2*E1*(Power(Mpi,2) + M1*w)) + 
         //                       cosThetaPi*(4*Power(E1,2)*sin2the*w - 2*(-1 + cosThetaE)*E1*(Power(Mpi,2) + M1*w) - 
         //                                    cosThetaE*(2*M1*Power(Mpi,2) + Power(M1,2)*w - Power(M2,2)*w + Power(Mpi,2)*w)))/
         //           (Sqrt(Power(Mpi,2) + Power(w,2))*Power(M1 + 2*E1*sin2the - w + cosThetaE*cosThetaPi*Sqrt(Power(Mpi,2) + Power(w,2)) - 
         //                                                           cosDeltaPhi*sinThetaE*sinThetaPi*Sqrt(Power(Mpi,2) + Power(w,2)),2)))/2.;
         return res;
      }
 
   public: 

      MAIDExclusivePionDiffXSec2(const char * pion = "pi0",const char * nucleon = "p"); 
      ~MAIDExclusivePionDiffXSec2();

      Double_t   EvaluateXSec(const Double_t *) const; 
      Double_t * GetDependentVariables(const Double_t * ) const;
      void       InitializePhaseSpaceVariables();

      ClassDef(MAIDExclusivePionDiffXSec2,1)
};

}}
#endif 
