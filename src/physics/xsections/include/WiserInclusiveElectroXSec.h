#ifndef WiserInclusiveElectroXSec_HH
#define WiserInclusiveElectroXSec_HH 1

#include "TNamed.h"
#include "InclusiveDiffXSec.h"
#include "FortranWrappers.h"
#include "InSANEMathFunc.h"
#include "CompositeDiffXSec.h"
#include "WiserXSection.h"
#include "WiserInclusivePhotoXSec.h"
namespace insane {
namespace physics {

/**  Inclusive electro production of pions, kaons, and nucleons.
 *   Uses the fit from wiser and virtual photon spectrum from Traitor-Wright Nuc.Phys. A379.
 *   The recoil factor (eq13) is also included.
 *
 * \ingroup inclusiveXSec
 */
class WiserInclusiveElectroXSec : public InclusiveWiserXSec {

   public:
      WiserInclusiveElectroXSec();
      virtual ~WiserInclusiveElectroXSec();
      virtual WiserInclusiveElectroXSec*  Clone(const char * newname) const {
         std::cout << "WiserInclusiveElectroXSec::Clone()\n";
         auto * copy = new WiserInclusiveElectroXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual WiserInclusiveElectroXSec*  Clone() const { return( Clone("") ); } 

      /** gamma+p --> pi + X */
      //virtual void InitializePhaseSpaceVariables();

      /** Evaluate Cross Section (nbarn/GeV*str).  */
      Double_t EvaluateXSec(const Double_t * x) const ;

   ClassDef(WiserInclusiveElectroXSec,1)
};



/** Wiser cross section for arbitrary nuclear target. 
 *
 * \ingroup inclusiveXSec
 */
class ElectroWiserDiffXSec : public CompositeDiffXSec {

   public:
      ElectroWiserDiffXSec();
      virtual ~ElectroWiserDiffXSec();
      virtual ElectroWiserDiffXSec*  Clone(const char * newname) const {
         std::cout << "ElectroWiserDiffXSec::Clone()\n";
         auto * copy = new ElectroWiserDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual ElectroWiserDiffXSec*  Clone() const { return( Clone("") ); } 

      //virtual void InitializePhaseSpaceVariables();
      TParticlePDG * GetParticlePDG() { return ((WiserInclusivePhotoXSec*)fProtonXSec)->GetParticlePDG(); } 
      Int_t          GetParticleType() { return ((WiserInclusivePhotoXSec*)fProtonXSec)->GetParticlePDG()->PdgCode(); }

      void SetProductionParticleType(Int_t PDGcode, Int_t part = 0) {
         SetParticleType(PDGcode);
         ((WiserInclusivePhotoXSec*)fProtonXSec)->SetProductionParticleType(PDGcode);
         ((WiserInclusivePhotoXSec*)fNeutronXSec)->SetProductionParticleType(PDGcode);
      }

      void SetParticlePDGEncoding(Int_t PDGcode) { SetProductionParticleType(PDGcode); }

      virtual Double_t  EvaluateXSec(const Double_t * x) const ;

   ClassDef(ElectroWiserDiffXSec,1)
};

}}
#endif

