#ifndef insane_physics_ExternalRadiator_HXX
#define insane_physics_ExternalRadiator_HXX 1

namespace insane {
   namespace physics {
//___________________________________________________________________
template <class T>
ExternalRadiator<T>::ExternalRadiator()
{

   this->fID     += 30000000;
   fPOLRAD       = nullptr;
   fRADCOR       = nullptr;
   fRadLen[0]    = 0.05;
   fRadLen[1]    = 0.05;
   fAddRegion4   = false;
   fInternalOnly = false;
   fExternalOnly = true;
   fFullExternal = false;
   fUsePOLRAD    = false;
   fElasticOnly  = false;

   this->SetTitle(Form("Radiated - %s",T::GetTitle()));
   this->SetPlotTitle(Form("Radiated - %s",T::GetPlotTitle()));

   GetRADCOR()->SetCrossSection(this); 
   GetRADCOR()->SetRadiationLengths(fRadLen);
   //GetRADCOR()->SetDeltaM(0.001);
   //GetRADCOR()->SetIntegrationThreshold(0.001); 

}
//______________________________________________________________________________

template <class T>
ExternalRadiator<T>::~ExternalRadiator(){
}
//______________________________________________________________________________

template <class T>
Double_t ExternalRadiator<T>::EvaluateXSec(const Double_t *x) const {
   if (!this->VariablesInPhaseSpace(this->fnDim, x)){
      //std::cout << "[POLRADElasticTailDiffXSec::EvaluateXSec]: Something is wrong!" << std::endl;
      return(0.0);
   }
   Double_t Eprime  = x[0];
   Double_t theta   = x[1];
   Double_t phi     = x[2];
   Double_t Ebeam   = this->GetBeamEnergy();
   Double_t sig_rad = 0.0;
   Double_t sig_rad2 = 0.0;

   // Using the Equiv. Rad. Method
   if( !fUsePOLRAD ) {

      // Use RADCOR2

      if(fFullExternal){
         sig_rad                  = GetRADCOR()->EquivRad3DIntegral(Ebeam,Eprime,theta,phi);
      } else if( fElasticOnly ) {

         sig_rad    = GetRADCOR()->ExternalOnly_ElasticTail(Ebeam,Eprime,theta,phi);

      } else {

         if(fInternalOnly) {
            sig_rad                  = GetRADCOR()->InternalOnly_EquivRadContinuumStragglingStripApprox(Ebeam,Eprime,theta,phi);
         } if(fExternalOnly) {
            sig_rad                  = GetRADCOR()->ExternalOnly_EquivRadContinuumStragglingStripApprox(Ebeam,Eprime,theta,phi);
            //sig_rad                  = GetRADCOR()->ExternalOnly_ElasticTail(Ebeam,Eprime,theta,phi);
            //std::cout << sig_rad << std::endl;

         } else {
            sig_rad                  = GetRADCOR()->EquivRadContinuumStragglingStripApprox(Ebeam,Eprime,theta,phi);
         }

         if(fAddRegion4){
            sig_rad2 = GetRADCOR()->EquivRad2DEnergyIntegral_RegionIV(Ebeam,Eprime,theta,phi);
            // std::cout << sig_rad << "\t" << sig_rad2 << std::endl;
            sig_rad += sig_rad2;
         }
      }
   }
   if( this->IncludeJacobian() ) sig_rad = sig_rad*TMath::Sin(theta);
   if(sig_rad <0.0 || TMath::IsNaN(sig_rad)) sig_rad = 0.0;
   //this->SetBeamEnergy(Ebeam);
   return sig_rad;
} 
//______________________________________________________________________________

template <class T>
Int_t ExternalRadiator<T>::InitFromDisk() {
   // Set this because the born cross section pointer RADCOR2::fXS is not streamed
   GetRADCOR()->SetCrossSection(this); 
   return 0;
}
//______________________________________________________________________________

template <class T>
void ExternalRadiator<T>::CreateRADCOR() const {
   fRADCOR = new RADCOR2();
   fRADCOR->SetIntegrationThreshold(2);
}
//______________________________________________________________________________

template <class T>
void ExternalRadiator<T>::CreatePOLRAD() const {
   fPOLRAD = new POLRAD();
   fPOLRAD->SetVerbosity(1);
   //fPOLRAD->DoQEFullCalc(false); 
   fPOLRAD->SetTargetNucleus(Nucleus::Proton());
   fPOLRAD->fErr   = 1E-1;   // integration error tolerance 
   fPOLRAD->fDepth = 3;     // number of iterations for integration 
   fPOLRAD->SetMultiPhoton(true); 
   //fPOLRAD->SetUltraRel   (false);
}
//______________________________________________________________________________

}
}
#endif

