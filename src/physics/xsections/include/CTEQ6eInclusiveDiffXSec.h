#ifndef CTEQ6eInclusiveDiffXSec_H
#define CTEQ6eInclusiveDiffXSec_H 1 

#include "PhysicalConstants.h"
#include "InclusiveDiffXSec.h"
#include "StructureFunctionsFromPDFs.h"
#include "CTEQ6UnpolarizedPDFs.h"

namespace insane {
namespace physics {
/** Differential cross section using the CTEQ PDFs. 
 *  
 * @ingroup inclusiveXSec 
 */
class CTEQ6eInclusiveDiffXSec: public InclusiveDiffXSec{

   private: 
      //Double_t fA,fZ; 

      Double_t MottXSec(Double_t,Double_t); 

   public: 
      CTEQ6eInclusiveDiffXSec();
      virtual ~CTEQ6eInclusiveDiffXSec(); 
      virtual CTEQ6eInclusiveDiffXSec*  Clone(const char * newname) const {
         std::cout << "CTEQ6eInclusiveDiffXSec::Clone()\n";
         auto * copy = new CTEQ6eInclusiveDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual CTEQ6eInclusiveDiffXSec*  Clone() const { return( Clone("") ); } 

      // DON"T DO THIS
      //void SetA(Double_t a){fA = a;} 
      //void SetZ(Double_t z){fZ = z;}

      Double_t EvaluateXSec(const Double_t *) const; 

      ClassDef(CTEQ6eInclusiveDiffXSec,1)
};

}}
#endif 

