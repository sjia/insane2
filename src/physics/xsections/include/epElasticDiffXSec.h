#ifndef epElasticDiffXSec_H
#define epElasticDiffXSec_H 1

#include "ExclusiveDiffXSec.h"
#include "InclusiveDiffXSec.h"
#include "TMath.h"
#include "FormFactors.h"
#include "TVector3.h"


namespace insane {
namespace physics {
/** Elastic scattering using the Dipole FFs.
 * \ingroup exclusiveXSec
 */
class epElasticDiffXSec : public ExclusiveDiffXSec {

   public:
      epElasticDiffXSec();
      virtual ~epElasticDiffXSec();
      virtual epElasticDiffXSec*  Clone(const char * newname) const {
         std::cout << "epElasticDiffXSec::Clone()\n";
         auto * copy = new epElasticDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual epElasticDiffXSec*  Clone() const { return( Clone("") ); } 

      /** Virtual method that returns the calculated values of dependent variables. This method
       *  should be overridden for exclusive cross sections in order to conserve momentum and energy!
       *
       *  For example, in mott scattering (elastic) there is really only two random variables,
       *  the the scattered angles theta and phi. The rest can be calculated from these two angles
       *  (assuming the beam energy is known).
       *
       */
      virtual Double_t * GetDependentVariables(const Double_t * x) const ;

      /**  Returns the scattered electron energy using the angle. */
      Double_t GetEPrime(const Double_t theta) const ;

      /** Evaluate Cross Section (mbarn/sr) */
      virtual Double_t EvaluateXSec(const Double_t * x) const;

      ClassDef(epElasticDiffXSec, 1)
};
}
}

#endif

