#ifndef insane_physics_VirtualPhotoAbsorptionCrossSections_HH
#define insane_physics_VirtualPhotoAbsorptionCrossSections_HH 1

#include "TNamed.h"
#include <vector>

namespace insane {
  namespace physics {

    /** Virtual photo-absorption cross sections.
     *
     */
    template<class T>
    struct VPACs {
    public:
        double               fSig_T   = 0.0;
        double               fSig_L   = 0.0;
        double               fSig_LT  = 0.0;
        double               fSig_LTp = 0.0;
        double               fSig_TT  = 0.0;
        double               fSig_TTp = 0.0;
        double               fSig_L0  = 0.0;
        double               fSig_LT0 = 0.0;
        double               fSig_LT0p= 0.0;
        std::vector<double>  fSigs    ;

    public:
        double Sig_T()   const { return fSig_T; }
        double Sig_L()   const { return fSig_L; }
        double Sig_LT()  const { return fSig_LT; }
        double Sig_TT()  const { return fSig_TT; }
        double Sig_LTp() const { return fSig_LTp; }
        double Sig_TTp() const { return fSig_TTp; }

    private:
        VPACs(){}
        friend T;
    };

    /** VirtualPhotoAbsorptionCrossSections.
     *
     * \deprecated 
     */ 
    class VirtualPhotoAbsorptionCrossSections : public TNamed  {
      public:

        mutable double               fSig_T   = 0.0;
        mutable double               fSig_L   = 0.0;
        mutable double               fSig_LT  = 0.0;
        mutable double               fSig_LTp = 0.0;
        mutable double               fSig_TT  = 0.0;
        mutable double               fSig_TTp = 0.0;
        mutable double               fSig_L0  = 0.0;
        mutable double               fSig_LT0 = 0.0;
        mutable double               fSig_LT0p= 0.0;
        mutable std::vector<double>  fSigs    ;

      public:

        VirtualPhotoAbsorptionCrossSections(
            const char * n = "VirtualPhotoAbsorptionCrossSections",
            const char * t = "Total photo absorption cross sections");
        virtual ~VirtualPhotoAbsorptionCrossSections();

        virtual void CalculateProton( double x, double Q2) const { };
        virtual void CalculateNeutron(double x, double Q2) const { };

        double Sig_T()   const { return fSig_T; }
        double Sig_L()   const { return fSig_L; }
        double Sig_LT()  const { return fSig_LT; }
        double Sig_TT()  const { return fSig_TT; }
        double Sig_LTp() const { return fSig_LTp; }
        double Sig_TTp() const { return fSig_TTp; }

        ClassDef(VirtualPhotoAbsorptionCrossSections,1)
    };



  }
}

#endif

