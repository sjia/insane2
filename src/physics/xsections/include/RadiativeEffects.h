#ifndef InSANE_physics_RadiativeEffects_HH
#define InSANE_physics_RadiativeEffects_HH

namespace insane {
   namespace physics {

      double F_Tsai(double Es, double Ep, double theta, double T=0.05);
      double RCToPeak_Tsai(double Es, double EpPeak, double theta, double T=0.05, double Z=2.0, double A=4.0);
      double Ib_integral(double E0, double Eprime, double DeltaE, double T=0.05, double Z=2.0, double A=4.0);
      double tr_Tsai(double Q2);

      // Eq'ns C.11 and C12 of Tsai 1971, SLAC-PUB-848
      double omega_s_Tsai(double Es, double Ep, double theta, double M);
      double omega_p_Tsai(double Es, double Ep, double theta, double M);

   }
}

#endif
