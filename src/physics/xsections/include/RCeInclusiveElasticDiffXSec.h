#ifndef RCeInclusiveElasticDiffXSec_HH
#define RCeInclusiveElasticDiffXSec_HH 1

#include "InclusiveDiffXSec.h"
#include "RADCOR2.h"
#include "eInclusiveElasticDiffXSec.h"

namespace insane {
namespace physics {
/** Elastic scattering using. 
 *
 * \ingroup inclusiveXSec
 */
class RCeInclusiveElasticDiffXSec : public InclusiveDiffXSec {
   protected:

      mutable RADCOR2             fRADCOR;
      InclusiveDiffXSec * fXS;
   public:
      RCeInclusiveElasticDiffXSec();
      virtual ~RCeInclusiveElasticDiffXSec();
      virtual RCeInclusiveElasticDiffXSec*  Clone(const char * newname) const {
         std::cout << "RCeInclusiveElasticDiffXSec::Clone()\n";
         auto * copy = new RCeInclusiveElasticDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual RCeInclusiveElasticDiffXSec*  Clone() const { return( Clone("") ); } 

      virtual void InitializePhaseSpaceVariables();

      /** The variables used here are theta and phi. */
      virtual Double_t * GetDependentVariables(const Double_t * x) const ;

      /** Evaluate Cross Section (mbarn/sr) */
      virtual Double_t EvaluateXSec(const Double_t * x) const;
      virtual Double_t EvaluateBaseXSec(const Double_t * x) const;

      /** Returns scattering angle as a function of energy. */
      Double_t GetTheta(const Double_t Ep) const {
         Double_t E0 = GetBeamEnergy();
         if(Ep > E0) return(0.0);
         Double_t M  = fTargetNucleus.GetMass();//M_p/GeV;
         Double_t arg = TMath::Sqrt((M/2.0)*(1.0/Ep - 1.0/E0) );
         //std::cout << arg << std::endl;
         return( 2.0*TMath::ASin(arg) );
      }

      /**  Returns the scattered electron energy using the angle.  */
      Double_t GetEPrime(const Double_t theta)const  {
         Double_t M  = fTargetNucleus.GetMass();//M_p/GeV;
         return (GetBeamEnergy() / (1.0 + 2.0 * GetBeamEnergy() / (M) * TMath::Power(TMath::Sin(theta / 2.0), 2.0)));
      }

      /** Cross section as a function of W.
       *  Calulates E'. 
       *  \param x[0] = W
       *  \param p[0] = theta
       *  \param p[1] = phi
       */
      virtual double Vs_W(double *x, double *p) ;

      /** Cross section as a function of energy.
       *  \param x[0] = W
       *  \param p[0] = Q2 
       *  \param p[1] = phi
       *  \todo This function is incomplte. 
       */
      virtual double WDependentXSec(double *x, double *p) ;

      /** Cross section as a function of energy.
       *  \param x[0] = eprime 
       *  \param p[0] = phi 
       */
      virtual double EnergyDependentXSec(double *x, double *p) {
         Double_t y[2];
         y[0] = GetTheta(x[0]);
         y[1] = p[0];
         return(EvaluateXSec(GetDependentVariables(y)));
      }

      /** Cross section as a function of polar angle .
       *  \param x[0] = theta
       *  \param p[0] = phi
       */
      virtual double PolarAngleDependentXSec(double *x, double *p) {
         Double_t y[2];
         y[0] = x[0];
         y[1] = p[0];
         return(EvaluateXSec(GetDependentVariables(y)));
      }

      /** Cross section as a function of Photon energy.
       *  \param x[0] = photon energy
       *  \param p[0] = theta
       *  \param p[1] = phi
       */
      virtual double PhotonEnergyDependentXSec(double *x, double *p) {
         Double_t eprime = GetBeamEnergy() - x[0];
         Double_t y[2];
         y[0] = GetTheta(eprime);
         y[1] = p[0];
         return(EvaluateXSec(GetDependentVariables(y)));
      }


      ClassDef(RCeInclusiveElasticDiffXSec, 1)
};
}}
#endif
