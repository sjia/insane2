#include "RadiativeTail.h"
#include "InSANEPhysics.h"

namespace insane {
namespace physics {

RadiativeTail::RadiativeTail()
{
   fID = 100010011;

   SetTitle("RadiativeTail");//,"POLRAD Born cross-section");
   SetPlotTitle("Radiative Tail cross-section");

   fLabel = "#frac{d#sigma}{dEd#Omega}";
   fUnits = "nb/GeV/sr";

   fRadLen[0] = 0.025; 
   fRadLen[1] = 0.025; 

   fPOLRAD = nullptr;
   fRADCOR = nullptr;

   //POLRADElasticDiffXSec *fDiffXSec = new  POLRADElasticDiffXSec();
   //fDiffXSec->SetA(1);
   //fDiffXSec->SetZ(1);
   //fDiffXSec->InitializePhaseSpaceVariables();
   //fDiffXSec->InitializeFinalStateParticles();

   Nucleus::NucleusType Target = Nucleus::kProton; 


   // Structure functions F1,F2
   StructureFunctions * sf   = new DefaultStructureFunctions();
   SetUnpolarizedStructureFunctions(sf);

   // Structure functions g1,g2
   PolarizedStructureFunctions * psf   = new DefaultPolarizedStructureFunctions();
   SetPolarizedStructureFunctions(psf);

   // Quasi  structure functions
   auto * F1F209QESFs = new F1F209QuasiElasticStructureFunctions();
   SetQEStructureFunctions(F1F209QESFs);

   // Nucleon form factors 
   FormFactors * FFs = new DefaultFormFactors(); 
   SetFormFactors(FFs);

   // Nuclei form factors 
   //FormFactors * NFFs = new AmrounFormFactors();
   FormFactors * NFFs = new MSWFormFactors();
   SetTargetFormFactors(NFFs);
}
//________________________________________________________________________________
RadiativeTail::~RadiativeTail(){
}
//________________________________________________________________________________
void RadiativeTail::CreateRADCOR() const {
   fRADCOR = new RADCOR();
   //fRADCOR->Do(false); 
   fRADCOR->SetThreshold(1); 
   fRADCOR->UseMultiplePhoton();
   fRADCOR->UseInternal(true);  
   fRADCOR->UseExternal(true);  
   fRADCOR->SetPolarization(0); 
   fRADCOR->SetVerbosity(0); 
   //fRADCOR->SetCrossSection(fDiffXSec); 
   fRADCOR->SetTargetNucleus(Nucleus::Proton()); 
   fRADCOR->SetRadiationLengths(fRadLen);
}
//________________________________________________________________________________
void RadiativeTail::CreatePOLRAD() const {
   fPOLRAD = new POLRAD();
   fPOLRAD->SetVerbosity(1);
   //fPOLRAD->DoQEFullCalc(false); 
   fPOLRAD->SetTargetNucleus(Nucleus::Proton());
   fPOLRAD->fErr   = 1E-1;   // integration error tolerance 
   fPOLRAD->fDepth = 3;     // number of iterations for integration 
   fPOLRAD->SetMultiPhoton(true); 
   //fPOLRAD->SetUltraRel   (false);
}

//________________________________________________________________________________
//________________________________________________________________________________
//________________________________________________________________________________
}}
