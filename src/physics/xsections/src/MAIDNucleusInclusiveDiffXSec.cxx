#include "MAIDNucleusInclusiveDiffXSec.h"
//______________________________________________________________________________
namespace insane {
namespace physics {

MAIDNucleusInclusiveDiffXSec::MAIDNucleusInclusiveDiffXSec()
{
   fID         = 100104035;

   SetTitle("MAIDNucleusInclusiveDiffXSec");
   SetPlotTitle("MAID #vec{p}(#vec{e},e'#pi^{0})p");
   fLabel      = "#int #frac{d#sigma}{dEd#Omega#Omega_{#pi}} d#Omega_{#pi}";
   fUnits      = "nb/GeV/sr";
   fPolType    = 0;                              // cross section polarization type (0 = unpolarized)  
   fh          = 0.;                             // electron helicity (+/- 1) or 0 
   fP_t.SetXYZ(0.0,0.0,0.0);

   fPi0Neutron    = new MAIDInclusiveDiffXSec("pi0"    ,"n");
   fPiMinusProton = new MAIDInclusiveDiffXSec("piminus","p");
   fPi0Proton     = new MAIDInclusiveDiffXSec("pi0"    ,"p");
   fPiPlusNeutron = new MAIDInclusiveDiffXSec("piplus" ,"n");

   //Nucleus::NucleusType neutron = Nucleus::kNeutron; 
   //Nucleus::NucleusType proton  = Nucleus::kProton;
 
   /// neutron targets 
   fPi0Neutron->SetTargetNucleus(   Nucleus::Neutron()); 
   fPiMinusProton->SetTargetNucleus(Nucleus::Neutron()); 
   /// proton targets 
   fPi0Proton->SetTargetNucleus(    Nucleus::Proton());  
   fPiPlusNeutron->SetTargetNucleus(Nucleus::Proton()); 

   /// initialize everything 
   fPi0Neutron->InitializePhaseSpaceVariables();
   fPi0Neutron->InitializeFinalStateParticles();
   fPi0Neutron->Refresh();
   fPiMinusProton->InitializePhaseSpaceVariables();
   fPiMinusProton->InitializeFinalStateParticles();
   fPiMinusProton->Refresh();
   fPi0Proton->InitializePhaseSpaceVariables();
   fPi0Proton->InitializeFinalStateParticles();
   fPi0Proton->Refresh();
   fPiPlusNeutron->InitializePhaseSpaceVariables();
   fPiPlusNeutron->InitializeFinalStateParticles();
   fPiPlusNeutron->Refresh();

   SetHelicity(fh); 
   SetTargetPolarization(fP_t);

   fDebug      = false; 
   fIsModified = false;      // use scaled version of MAID (scaled to data from E94010... not totally good yet.)  

}
//______________________________________________________________________________
MAIDNucleusInclusiveDiffXSec::~MAIDNucleusInclusiveDiffXSec(){
   delete fPi0Neutron; 
   delete fPiMinusProton; 
   delete fPi0Proton; 
   delete fPiPlusNeutron; 
}
//______________________________________________________________________________
Double_t MAIDNucleusInclusiveDiffXSec::EvaluateXSec(const Double_t *par) const{

   /// arrays to store results 
   /// index 0 => h = -1, index 1 => h = +1
   /// subscript pt refers to the set target polarization, P_t  
   Double_t hel[2]          = {-1.,1.};
   Double_t pi0_n_pt[2]     = { 0.,0.}; 
   Double_t pi0_p_pt[2]     = { 0.,0.}; 
   Double_t piMinus_p_pt[2] = { 0.,0.}; 
   Double_t piPlus_n_pt[2]  = { 0.,0.}; 
   Double_t p_xs_pt[2]      = { 0.,0.}; 
   Double_t n_xs_pt[2]      = { 0.,0.}; 

   Double_t h=0; 
   for(int i=0;i<2;i++){
      h = hel[i]; 
      fPi0Neutron->SetHelicity(h);      
      fPiMinusProton->SetHelicity(h);      
      fPi0Proton->SetHelicity(h);      
      fPiPlusNeutron->SetHelicity(h);    
      /// individual reactions  
      pi0_n_pt[i]     = fPi0Neutron->EvaluateXSec(par); 
      piMinus_p_pt[i] = fPiMinusProton->EvaluateXSec(par);
      pi0_p_pt[i]     = fPi0Proton->EvaluateXSec(par);  
      piPlus_n_pt[i]  = fPiPlusNeutron->EvaluateXSec(par);
      /// sum for proton and neutron targets 
      p_xs_pt[i]      = pi0_p_pt[i] + piPlus_n_pt[i]; 
      n_xs_pt[i]      = pi0_n_pt[i] + piMinus_p_pt[i]; 
   }

   /// now for the opposite target polarization: (-1)*P_t (if P_t != 0)
   /// we can use the values we have calculated already: 
   /// We know that sig(-h,+pt) = sig(+h,-pt);  
   /// since we flip the target polarization sign already, the cross section 
   /// for a given helicity is just the xs for the opposite helicity  
   Double_t pi0_n_ptm[2]     = { 0.,0.}; 
   Double_t pi0_p_ptm[2]     = { 0.,0.}; 
   Double_t piMinus_p_ptm[2] = { 0.,0.}; 
   Double_t piPlus_n_ptm[2]  = { 0.,0.}; 
   Double_t p_xs_ptm[2]      = { 0.,0.}; 
   Double_t n_xs_ptm[2]      = { 0.,0.}; 

   pi0_n_ptm[0]     = pi0_n_pt[1]; 
   pi0_n_ptm[1]     = pi0_n_pt[0]; 
   piMinus_p_ptm[0] = piMinus_p_pt[1]; 
   piMinus_p_ptm[1] = piMinus_p_pt[0]; 
   pi0_p_ptm[0]     = pi0_p_pt[1]; 
   pi0_p_ptm[1]     = pi0_p_pt[0]; 
   piPlus_n_ptm[0]  = piPlus_n_pt[1]; 
   piPlus_n_ptm[1]  = piPlus_n_pt[0]; 

   for(int i=0;i<2;i++){
      /// sum for proton and neutron targets (P_t = -P_t)  
      p_xs_ptm[i] = pi0_p_ptm[i] + piPlus_n_ptm[i]; 
      n_xs_ptm[i] = pi0_n_ptm[i] + piMinus_p_ptm[i]; 
   }

   Double_t p_xs=0,n_xs=0; 

   /// combine helicity-dependent cross sections based on polarization type 
   /// default is unpolarized  
   Double_t PtMag = fP_t.Mag();
   if(PtMag!=0){
      /// since we have a non-zero target polarization, we need to average over that polarization too 
      p_xs = 0.25*(p_xs_pt[0] + p_xs_pt[1] + p_xs_ptm[0] + p_xs_ptm[1]); 
      n_xs = 0.25*(n_xs_pt[0] + n_xs_pt[1] + n_xs_ptm[0] + n_xs_ptm[1]); 
   }else{
      p_xs = 0.5*(p_xs_pt[0] + p_xs_pt[1]); 
      n_xs = 0.5*(n_xs_pt[0] + n_xs_pt[1]); 
   }

   if(fPolType==1||fPolType==2){
      /// if we want delta_sig_para or delta_sig_perp, we compute (h=-1) - (h=+1)    
      p_xs = p_xs_pt[0] - p_xs_pt[1]; 
      n_xs = n_xs_pt[0] - n_xs_pt[1]; 
   }else if(fPolType==3){
      /// if we want sig(-h,p_t)     
      p_xs = p_xs_pt[0]; 
      n_xs = n_xs_pt[0]; 
   }else if(fPolType==4){
      /// if we want sig(h,p_t)     
      p_xs = p_xs_pt[1]; 
      n_xs = n_xs_pt[1]; 
   }

   Double_t res=0; 
   Nucleus::NucleusType t = fTargetNucleus.GetType();
   Double_t A = fTargetNucleus.GetA();  
   Double_t Z = fTargetNucleus.GetZ();  

   // FIXME: The calculation below is only necessarily true for POLARIZED targets... 
   //        The overall normalization of unpolarized calculations seems way too big compared to data... 

   Double_t w_D     = 0.058;                    /// D-wave state probability  
   Double_t f       = 1. - 1.5*w_D;             /// effective polarization         [deuteron]   
   Double_t PnTilde = 0.879 + 0.056;            /// effective neutron polarization [3He] (including off-shell effects)
   Double_t PpTilde = 2.*(-0.021) - 0.014;      /// effective proton polarization  [3He] (including off-shell effects)

   if(fPolType==0){
      res = Z*p_xs + (A-Z)*n_xs;  
   }else{
      switch(t){
         case Nucleus::kProton: 
            res = 0.;
            break;
         case Nucleus::kNeutron:
            res = 0.;
            break;
         case Nucleus::kDeuteron: 
            res = f*(p_xs + n_xs); 
            break;
         case Nucleus::k3He: 
            res = PpTilde*p_xs + PnTilde*n_xs; 
            break; 
         default: 
            std::cout << "[MAIDNucleusInclusiveDiffXSec::EvaluateXSec]: Invalid target type!  Exiting..." << std::endl;
            exit(1);
      }
   } 

   /// Fudge factor to fit the data... 
   Double_t Es       = GetBeamEnergy(); 
   Double_t Ep       = par[0]; 
   Double_t th       = par[1]; 
   Double_t W        = insane::Kine::W_EEprimeTheta(Es,Ep,th); 
   Double_t Q2       = insane::Kine::Qsquared(Es,Ep,th); 
   Double_t MyPar[5] = {Es,Ep,th,W,Q2};
   Double_t F        = 1.; 
   if(fIsModified) F = ScaleFactor(MyPar); 
 
   if(fDebug){
      std::cout << "Es = " << Es << "\t" << "Ep = " << Ep << "\t" << "th = " << th << "\t" << "proton xs = " << p_xs << "\t" << "neutron xs = " << n_xs << std::endl;
   }

   res *= F;

   // if(IncludeJacobian()) return res*TMath::Sin(th);  // this is done in each separate MAID class... 
   return res;  

}
//______________________________________________________________________________
Double_t MAIDNucleusInclusiveDiffXSec::ScaleFactor(Double_t *x) const{
   /// Scale function to tweak the model to fit the data 
   Double_t Es = x[0]; 
   Double_t Ep = x[1]; 
   Double_t th = x[2]; 
   Double_t W  = x[3]; 
   Double_t Q2 = x[4]; 
   /// Lorentzian 
   // Double_t X     = W;
   // linear dependence of the width as a function of Es 
   // Double_t g_p0  = 0.02877;    
   // Double_t g_p1  = 0.05074; 
   // Double_t gamma = g_p0 + g_p1*Es;
   // parabolic dependence of the amplitude as a function of Es 
   // Double_t amp_p0 = -428.845;    
   // Double_t amp_p1 =  215.795;    
   // Double_t amp_p2 = -27.3571;
   // Double_t amp_p0 = -675.91;   
   // Double_t amp_p1 =  497.061;  
   // Double_t amp_p2 = -127.202;  
   // Double_t amp_p3 =  11.1683;  
   // Double_t Amp    = amp_p0 + amp_p1*Es + amp_p2*Es*Es + amp_p3*Es*Es*Es;   
   // Double_t X0     = 1.232;        // at the delta 
   // Double_t T      = 0.5*gamma;
   // Double_t lnum   = Amp; 
   // Double_t lden   = pi*( TMath::Power(X-X0,2.) + TMath::Power(T,2.) ); 
   // Double_t Lor    = lnum/lden;
   /// A pseudo-Lorentzian...
   // width: Es dependence  
   // Double_t g_p0   = 0.02877;    
   // Double_t g_p1   = 0.05074; 
   // Double_t Gamma  = 2.5*(g_p0 + g_p1*Es);
   // width: Q2 dependence  
   // Double_t g_p0   = -5.51494;   // rising expo    
   // Double_t g_p1   =  5.02202;   // rising expo 
   // Double_t Gamma  = TMath::Exp(g_p0 + g_p1*Q2);
   // offset: Es dependence  
   // Double_t w0_p0  =  1.08729;       
   // Double_t w0_p1  =  0.125327;       
   // Double_t w0_p2  = -0.0358354;       
   // Double_t w0_p3  =  0.00343136;  
   // Double_t W0     = w0_p0 + w0_p1*Es + w0_p2*Es*Es + w0_p3*Es*Es*Es; 
   // amplitude: Es dependence 
   // Double_t amp_p0 = -9.48906;      
   // Double_t amp_p1 =  2.15956; 
   // Double_t amp_p0 = -5.3056;     
   // Double_t amp_p1 =  0.888854;  
   // amplitude: Q2 dependence  
   // Double_t amp_p0 = -5.51494;   // rising expo    
   // Double_t amp_p1 =  5.02202;   // rising expo 
   // Double_t amp_p0 =  5.78742;   // falling expo  
   // Double_t amp_p1 = -7.50566;   // falling expo   
   // Double_t amp    = TMath::Exp(amp_p0 + amp_p1*Q2);  
   // Double_t amp    = 1.; 
   // Double_t num    = amp; 
   // Double_t den    = pi*( TMath::Power(W-W0,2.) + TMath::Power(Gamma,2.) );
   // Double_t Lor    = num/den;    
   // std::cout << "W = " << W << "\t" << "W0 = " << W0 << "\t" << "gamma = " << Gamma << "\t" << "f = " << InvLor << std::endl;
   /// a new lorentzian 
   // amplitude: Q2 dependence  
   Double_t amp_p0 =  0.417215;    
   Double_t amp_p1 = -0.0980862;    
   Double_t amp_p2 = -0.0369108;    
   Double_t amp_p3 =  0.00957322;    
   Double_t amp    = 3.3*(amp_p0 + amp_p1*Q2 + amp_p2*Q2*Q2 + amp_p3*Q2*Q2*Q2); 
   // Double_t amp_p3 =  0.00957322;   
   // Double_t amp_p0 =  0.104697;      
   // Double_t amp_p1 = -1.58881;   
   // Double_t amp    = TMath::Exp(amp_p0 + amp_p1*Q2);  
   // width: Es dependence  
   Double_t g_p0   = 0.02877;    
   Double_t g_p1   = 0.05074; 
   // Double_t g_p0   = 1.;    
   // Double_t g_p1   = .8; 
   Double_t Gamma  = 1.*(g_p0 + g_p1*Es);
   // offset: Es dependence 
   Double_t w0_p0  =  1.08729;       
   Double_t w0_p1  =  0.125327;       
   Double_t w0_p2  = -0.0358354;       
   Double_t w0_p3  =  0.00343136;  
   Double_t W0     = w0_p0 + w0_p1*Es + w0_p2*Es*Es + w0_p3*Es*Es*Es;
   Double_t T      = Gamma;  
   Double_t num    = amp; 
   Double_t den    = pi*( TMath::Power(W-W0,2.) + TMath::Power(T,2.) );
   Double_t Lor    = num/den; 
   // std::cout << "W = " << W << "\t" << "W0 = " << W0 << "\t" << "Q2 = " << Q2 << "\t" << "amp = " << amp << "\t" << "gamma = " << Gamma << "\t" << "f = " << Lor << std::endl;
   /// gaussian 
   // Double_t amp_p0 = -4693.11;     
   // Double_t amp_p1 =  3802.62;     
   // Double_t amp_p2 = -1043.57;     
   // Double_t amp_p3 =  95.9623;
   // Double_t Amp    = amp_p0 + amp_p1*Es + amp_p2*Es*Es + amp_p3*Es*Es*Es; 
   // Double_t sig_p0 = -0.0186434;  
   // Double_t sig_p1 =  0.0532744; 
   // Double_t sig_p2 = -0.0069487; 
   // Double_t sig    = sig_p0 + sig_p1*Es + sig_p2*Es*Es;  
   // Double_t mu_p0  = 1.18051;  
   // Double_t mu_p1  = 0.01835;
   // Double_t mu     = mu_p0 + mu_p1*Es;
   // Double_t arg    = -0.5*TMath::Power( (W-mu)/sig,2. );  
   // Double_t gaus   = 1.*TMath::Exp(arg);  
   /// polynomial
   /// for Es dependence  
   // Double_t p0     =   1.98124;     
   // Double_t p1     =  -0.76209;     
   // Double_t p2     = 0.0774737;
   /// for Q2 dependence  
   // Double_t p0   =  0.417215;    
   // Double_t p1   = -0.0980862;    
   // Double_t p2   = -0.0369108;    
   // Double_t p3   =  0.00957322;    
   // Double_t poly = 1.1*(p0 + p1*Q2 + p2*Q2*Q2 + p3*Q2*Q2*Q2); 
   /// exponential
   // Double_t p0 =  1.21824;     
   // Double_t p1 = -0.73971;  
   // Double_t p0 =  0.0526048;     
   // Double_t p1 = -1.31867;  
   // Double_t p0 =  0.104697;     
   // Double_t p1 = -1.58881;   
   // Double_t expo = TMath::Exp(p0 + p1*Q2);
   // function 
   Double_t func = 1./Lor; 
   return func;  

}
//______________________________________________________________________________

}}
