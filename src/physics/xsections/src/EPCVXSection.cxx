#include "EPCVXSection.h"
#include "FortranWrappers.h"

namespace insane {
namespace physics {

InclusiveEPCVXSec::InclusiveEPCVXSec()
{
   fID = 100004001;
   fTitle = "Inclusive Pi0";
   fPlotTitle = "#frac{d#sigma}{dE_{#pi} d#Omega_{#pi}} mb/GeV-Sr";
   fPIDs.clear();
   fEPCVParticle = "PI0";
   //fPIDs.push_back(2212);//+

   /*      fParticleName = "PI0"; // argument for wiser fortran code*/
   /*      fParticle = TDatabasePDG::Instance()->GetParticle(111);//pi0 */
   // pdg code for pi0 see http://www.slac.stanford.edu/BFROOT/www/Computing/Environment/NewUser/htmlbug/node51.html
   //SetProductionParticleType(2212);
   SetProductionParticleType(111);
}
//____________________________________________________________________

void InclusiveEPCVXSec::InitializePhaseSpaceVariables()
{
   PhaseSpace * ps = GetPhaseSpace();

   /// Production particle variables
   auto * varEnergy2 = new PhaseSpaceVariable();
   varEnergy2 = new PhaseSpaceVariable();
   varEnergy2->SetNameTitle("energy_pi", "E_{#pi}");
   varEnergy2->SetMinimum(0.1); //GeV
   varEnergy2->SetMaximum(4.9); //GeV
   /*      varEnergy2->SetParticleIndex(0);*/
   //fMomentum_pi = varEnergy2->GetCurrentValueAddress();
   //fMomentum = varEnergy2->GetCurrentValueAddress();
   ps->AddVariable(varEnergy2);

   auto *   varTheta2 = new PhaseSpaceVariable();
   varTheta2->SetNameTitle("theta_pi", "#theta_{#pi}"); // ROOT string latex
   varTheta2->SetMinimum(20.0 * TMath::Pi() / 180.0); //
   varTheta2->SetMaximum(50.0 * TMath::Pi() / 180.0); //
   /*      varTheta2->SetParticleIndex(1);*/
   //fTheta_pi = varTheta2->GetCurrentValueAddress();
   //fTheta = varTheta2->GetCurrentValueAddress();
   ps->AddVariable(varTheta2);

   auto *   varPhi2 = new PhaseSpaceVariable();
   varPhi2->SetNameTitle("phi_pi", "#phi_{#pi}"); // ROOT string latex
   varPhi2->SetMinimum(-50.0 * TMath::Pi() / 180.0); //
   varPhi2->SetMaximum(50.0 * TMath::Pi() / 180.0); //
   /*      varPhi2->SetParticleIndex(1);*/
   //fPhi_pi = varPhi2->GetCurrentValueAddress();
   //fPhi = varPhi2->GetCurrentValueAddress();
   ps->AddVariable(varPhi2);

}
//____________________________________________________________________

void InclusiveEPCVXSec::PrintPossibleParticles()
{
   std::cout << " Particle = PDGcode \n";
   std::cout << "      pi0 = 111   \n";
   std::cout << "      pi+ = 211   \n";
   std::cout << "      pi- = -211  \n";
   std::cout << "        p = 2212  \n";
   std::cout << "        n = 2112 \n";
}
//____________________________________________________________________

char * InclusiveEPCVXSec::GetEPCVParticleType(Int_t PDGcode)
{
   char * value = const_cast<char*>("PI0");
   if (PDGcode == 111) value = const_cast<char*>("PI0");
   else if (PDGcode == 211) value = const_cast<char*>("PI+");
   else if (PDGcode == -211) value = const_cast<char*>("PI-");
   else if (PDGcode == 2212) value = const_cast<char*>("P");
   else if (PDGcode == 2112) value = const_cast<char*>("N");
   else {
      std::cout << " Bad particle code, " << PDGcode
                << ",  for EPCV inclusive cross section.\n";
      std::cout << " Possible choices are:\n";
      PrintPossibleParticles();
   }
   return(value);
}
//____________________________________________________________________

Double_t InclusiveEPCVXSec::EvaluateXSec(const Double_t * x) const
{
   //if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);
   double RES = 0;
   auto * PART = (char *)fEPCVParticle.Data();
   double EBEAM = GetBeamEnergy() * 1000.0; //(converting to MeV )
   double Epart = x[0];
   double Ppart = TMath::Sqrt(Epart*Epart - (M_pion*M_pion/(GeV*GeV))) ;
   if( TMath::IsNaN( Ppart ) ) {
      // this is a check that the energy is not less than the mass at rest.
      // could be improved.
      return 0;
   }
   double PscatteredPart = Ppart * 1000.0; // TMath::Sqrt( epart*epart-fParticle->Mass()*fParticle->Mass() ) *1000.0;  //(to MeV??)
   double THETA = x[1] * 180.0 / TMath::Pi(); //(DEGREES)

   int len_string = strlen(PART);
   double Z = GetZ();
   double N = GetN();
   /// \todo Use Target instead of hard coding nuclei!!!!!!!

   //std::cout << "PART = " << PART << "\n";
   epcv_single_(PART , &len_string, &Z, &N, &EBEAM, &PscatteredPart, &THETA, &RES); // returns ub/MeV-sr
   //epcv_single_v3_(PART , &len_string, &Z, &N, &EBEAM, &PscatteredPart, &THETA, &RES); // returns ub/MeV-sr
   //gpc_single_v3_(PART , &len_string, &Z, &N, &EBEAM, &PscatteredPart, &THETA, &RES); // returns ub/MeV-sr

   if( TMath::IsNaN(RES) ) RES = 0.0;
   Double_t sigma = RES * 1.0e6; // *1000(MeV/GeV)*1000(nb/ub)
   sigma *= (Epart/Ppart); // jacobian (dP_pi/dE_pi)
   //std::cout << RES << std::endl;
   if( IncludeJacobian() ) return sigma*TMath::Sin(x[1]);
   return( sigma ); // converts ub/MeV-sr to nb/GeV-sr
}
//____________________________________________________________________



InclusiveEPCVXSec2::InclusiveEPCVXSec2()
{
   fID = 100004002;
}
//______________________________________________________________________________

InclusiveEPCVXSec2::~InclusiveEPCVXSec2()
{}
//______________________________________________________________________________

Double_t InclusiveEPCVXSec2::EvaluateXSec(const Double_t * x) const
{
   //if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);
   double RES = 0;
   auto * PART = (char *)fEPCVParticle.Data();
   double EBEAM = GetBeamEnergy() * 1000.0; //(converting to MeV )
   double Epart = x[0];
   double Ppart = TMath::Sqrt(Epart*Epart - (M_pion*M_pion/(GeV*GeV))) ;
   double PscatteredPart = Ppart * 1000.0; // TMath::Sqrt( epart*epart-fParticle->Mass()*fParticle->Mass() ) *1000.0;  //(to MeV??)
   double THETA = x[1] * 180.0 / TMath::Pi(); //(DEGREES)

   int len_string = strlen(PART);
   double Z = GetZ();
   double N = GetN();
   /// \todo Use Target instead of hard coding nuclei!!!!!!!

   //std::cout << "PART = " << PART << "\n";
   //epcv_single_(PART , &len_string, &Z, &N, &EBEAM, &PscatteredPart, &THETA, &RES); // returns ub/MeV-sr
   epcv_single_v3_(PART , &len_string, &Z, &N, &EBEAM, &PscatteredPart, &THETA, &RES); // returns ub/MeV-sr
   //gpc_single_v3_(PART , &len_string, &Z, &N, &EBEAM, &PscatteredPart, &THETA, &RES); // returns ub/MeV-sr

   if( TMath::IsNaN(RES) ) RES = 0.0;
   Double_t sigma = RES * 1.0e6; // *1000(MeV/GeV)*1000(nb/ub)
   sigma *= (Epart/Ppart); // jacobian (dP_pi/dE_pi)
   //std::cout << RES << std::endl;
   if( IncludeJacobian() ) return sigma*TMath::Sin(x[1]);
   return( sigma ); // converts ub/MeV-sr to nb/GeV-sr
}
//____________________________________________________________________
}}
