#include "RADCOR.h"

#include "InSANEPhysics.h"

namespace insane {
namespace physics {

//______________________________________________________________________________
RADCOR::RADCOR(){
   fDebugFile     = nullptr;
   fDebugTree     = nullptr;
   fFF            = nullptr;
   fFFT           = nullptr;
   fFFQE          = nullptr;
   fUnpolSFs      = nullptr;
   fPolSFs        = nullptr;
   fUnpolXS       = nullptr;
   fIntRadUnpolXS = nullptr;
   fPolDiffXS     = nullptr;
   fUnpolXSEl     = nullptr;
   fPolDiffXSEl   = nullptr;
      fROOTFileName  = Form("test.root");
      fGridPath      = Form("NONE"); 
      fCFACT         =  0;             /// factor for peaking approx in stein 
      fDeltaE        =  0.01;          /// \todo: 10 MeV (see Stein) isn't this the same as Delta?   
      fDelta         =  0.01;         /// 10 MeV: should be LESS THAN 15 MeV! For infrared cutoff  
      fDepth         =  5; 
      fEpsilon       =  1E-5;
      fEsPrime       =  0; 
      fEpPrime       =  0; 
      fZEff          =  1.;           /// Effective Z of all materials 
      fAEff          =  1.;           /// Effective A of all materials 
      fX0Eff         =  1.;           /// Effective X0 of all materials 
      fDeltaM        =  0.;           /// For computing Es and Ep thresholds 
      fThreshold     = -1; 
      fMultiPhoton   =  1;            /// deprecated (use the boolean fIsMultiPhoton from now on)  
      fPol           = -1;  
      fDebug         =  0;            /// lowest verbosity setting
      fUseGridData   = false; 
      fUseGridBornData = false; 
      fIsElastic     = false;
      fIsMultiPhoton = true;
      fIsInternal    = true;  
      fIsExternal    = true; 
      fIsSmear       = false;         /// Fermi smearing factor (default is false)  
      fNumMCEvents   = 1E+6; 
      fMCBins        = 100;
      fUnits         = 0;             /// 0 => GeV, 1 => MeV
      fhbar_c_sq     = hbarc2_gev_nb; /// default (hc)^2 units 
      fMCIntegration = 0;             /// MC Integration: Sample mean method  
      gRandom->SetSeed(0);            /// for Monte Carlo integration 
      // SetTargetType(0);               /// default is the proton target 

      //std::cout << "-------------------- RADCOR Constructor --------------------" << std::endl;

      /// default models [3He]  
      /// form factors 
      // AMTFormFactors *AMT             = new AMTFormFactors();
      // DipoleFormFactors *Dipole = new DipoleFormFactors();
      auto *Galster     = new GalsterFormFactors(); // this will give us a non-zero value for GEn; all others are dipole form. 
      auto *Amroun       = new AmrounFormFactors();
      SetFormFactors(Galster);
      SetTargetFormFactors(Amroun);
      /// structure functions 
      auto *F1F209 = new F1F209StructureFunctions();
      // NMC95StructureFunctions *NMC95   = new NMC95StructureFunctions();
      // CTEQ6UnpolarizedPDFs *CTEQ       = new CTEQ6UnpolarizedPDFs();
      auto *Stat   = new StatisticalPolarizedPDFs(); 
      Stat->UseQ2Interpolation();
      auto *polSFs = new PolarizedStructureFunctionsFromPDFs(); 
      polSFs->SetPolarizedPDFs(Stat);
      SetUnpolarizedStructureFunctions(F1F209);
      SetPolarizedStructureFunctions(polSFs);
      /// elastic cross section 
      // FIXME 
      // // unpolarized
      // InclusiveDiffXSec *UnpolXSEl = new InclusiveDiffXSec(); 
      // UnpolXSEl->SetFormFactors(Amroun); 
      // SetUnpolarizedCrossSectionElastic(UnpolXSEl); 
      // // polarized
      // InclusiveDiffXSec *PolDiffXSEl = new InclusiveDiffXSec(); 
      // PolDiffXSEl->SetFormFactors(Amroun); 
      // SetPolarizedCrossSectionDifferenceElastic(PXSPara); 
      /// inelastic cross section 
      // unpolarized 
      auto *F1F209XS = new F1F209eInclusiveDiffXSec(); 
      F1F209XS->UseModifiedModel('y');
      SetUnpolarizedCrossSection(F1F209XS); 
      // polarized  
      auto *PXS = new PolarizedCrossSectionDifference();
      PXS->SetPolarizationType(1); // parallel 
      PXS->SetPolarizedStructureFunctions(polSFs); 
      PXS->InitializePhaseSpaceVariables();
      PXS->InitializeFinalStateParticles();
      SetPolarizedCrossSectionDifference(PXS); 
      PhaseSpace *ps1 = PXS->GetPhaseSpace();
      //ps1->Print();

      fWatch = new TStopwatch(); 

      //std::cout << "------------------------------------------------------------------" << std::endl;
}
//______________________________________________________________________________
RADCOR::~RADCOR(){

   delete fWatch;

}
//______________________________________________________________________________
void RADCOR::InitializeVariables(){
   // internal (elastic)
   rEs_int                    = 0.;       
   rEp_int                    = 0.; 
   rCOSTHK_int                = 0.; 
   rOmega_int                 = 0.; 
   rQ2_int                    = 0.; 
   // external (elastic) 
   rEs_ext                    = 0.; 
   rEp_ext                    = 0.; 
   rEsPrime_ext               = 0.; 
   rEpPrime_ext               = 0.; 
   rtPrime_ext                = 0.; 
   rT1_ext                    = 0.; 
   rT2_ext                    = 0.; 
   rT3_ext                    = 0.; 
   // internal (inelastic) 
   rEs_inel_int               = 0.;
   rEp_inel_int               = 0.;  
   rCOSTHK_inel_int           = 0.;      
   rOmega_inel_int            = 0.;      
   rQ2_inel_int               = 0.;  
   rIntegrand_inel_int        = 0.;        
   // external (inelastic) 
   rEs_1_ext                  = 0.;            
   rEp_1_ext                  = 0.;        
   rEsPrime_1_ext             = 0.;           
   rEpPrime_1_ext             = 0.;       
   rtPrime_1_ext              = 0.;        
   rT1_1_ext                  = 0.;         
   rT2_1_ext                  = 0.;              
   rT3_1_ext                  = 0.;              
   rEs_2_ext                  = 0.;         
   rEp_2_ext                  = 0.;     
   rEsPrime_2_ext             = 0.;       
   rEpPrime_2_ext             = 0.;       
   rtPrime_2_ext              = 0.;     
   rT1_2_ext                  = 0.;            
   rT2_2_ext                  = 0.;       
   rT3_2_ext                  = 0.;        

}
//______________________________________________________________________________
void RADCOR::InitializeROOTFile(){

   TString cmd = Form("rm %s",fROOTFileName.Data());

   gSystem->Exec(cmd);

   fDebugFile = new TFile(fROOTFileName,"recreate");
   fDebugTree = new TTree("T","RADCOR Debug");

   InitializeVariables();

   // elastic 
   // internal 
   fDebugTree->Branch("Es_int"     ,&rEs_int     ,"rEs_int/D");
   fDebugTree->Branch("Ep_int"     ,&rEp_int     ,"rEp_int/D");
   fDebugTree->Branch("costhk_int" ,&rCOSTHK_int ,"rCOSTHK_int/D");
   fDebugTree->Branch("omega_int"  ,&rOmega_int  ,"rOmega_int/D");
   fDebugTree->Branch("Q2_int"     ,&rQ2_int     ,"rQ2_int/D");
   // external 
   fDebugTree->Branch("Es_ext"     ,&rEs_ext     ,"rEs_ext/D");
   fDebugTree->Branch("Ep_ext"     ,&rEp_ext     ,"rEp_ext/D");
   fDebugTree->Branch("EsPrime_ext",&rEsPrime_ext,"rEsPrime_ext/D");
   fDebugTree->Branch("EpPrime_ext",&rEpPrime_ext,"rEpPrime_ext/D");
   fDebugTree->Branch("tPrime_ext" ,&rtPrime_ext ,"rtPrime_ext/D");
   fDebugTree->Branch("T1_ext"     ,&rT1_ext     ,"rT1_ext/D");
   fDebugTree->Branch("T2_ext"     ,&rT2_ext     ,"rT2_ext/D");
   fDebugTree->Branch("T3_ext"     ,&rT3_ext     ,"rT3_ext/D");
   // inelastic
   // internal  
   fDebugTree->Branch("Es_inel_int"    ,&rEs_inel_int    ,"rEs_inel_int/D");
   fDebugTree->Branch("Ep_inel_int"    ,&rEp_inel_int    ,"rEp_inel_int/D");
   fDebugTree->Branch("costhk_inel_int",&rCOSTHK_inel_int,"rCOSTHK_inel_int/D");
   fDebugTree->Branch("omega_inel_int" ,&rOmega_inel_int ,"rOmega_inel_int/D");
   fDebugTree->Branch("Q2_inel_int"    ,&rQ2_inel_int    ,"rQ2_inel_int/D");
   fDebugTree->Branch("Integrand_inel_int",&rIntegrand_inel_int,"rIntegrand_inel_int/D");
   // external 
   fDebugTree->Branch("Es_1_ext"       ,&rEs_1_ext       ,"rEs_1_ext/D");
   fDebugTree->Branch("Ep_1_ext"       ,&rEp_1_ext       ,"rEp_1_ext/D");
   fDebugTree->Branch("EsPrime_1_ext"  ,&rEsPrime_1_ext  ,"rEsPrime_1_ext/D");
   fDebugTree->Branch("EpPrime_1_ext"  ,&rEpPrime_1_ext  ,"rEpPrime_1_ext/D");
   fDebugTree->Branch("tPrime_1_ext"   ,&rtPrime_1_ext   ,"rtPrime_1_ext/D");
   fDebugTree->Branch("T1_1_ext"       ,&rT1_1_ext       ,"rT1_1_ext/D");
   fDebugTree->Branch("T2_1_ext"       ,&rT2_1_ext       ,"rT2_1_ext/D");
   fDebugTree->Branch("T3_1_ext"       ,&rT3_1_ext       ,"rT3_1_ext/D");
   fDebugTree->Branch("Es_2_ext"       ,&rEs_2_ext       ,"rEs_2_ext/D");
   fDebugTree->Branch("Ep_2_ext"       ,&rEp_2_ext       ,"rEp_2_ext/D");
   fDebugTree->Branch("EsPrime_2_ext"  ,&rEsPrime_2_ext  ,"rEsPrime_2_ext/D");
   fDebugTree->Branch("EpPrime_2_ext"  ,&rEpPrime_2_ext  ,"rEpPrime_2_ext/D");
   fDebugTree->Branch("tPrime_2_ext"   ,&rtPrime_2_ext   ,"rtPrime_2_ext/D");
   fDebugTree->Branch("T1_2_ext"       ,&rT1_2_ext       ,"rT1_2_ext/D");
   fDebugTree->Branch("T2_2_ext"       ,&rT2_2_ext       ,"rT2_2_ext/D");
   fDebugTree->Branch("T3_2_ext"       ,&rT3_2_ext       ,"rT3_2_ext/D");

   std::cout << "[RADCOR]: ROOT file '" << fROOTFileName << "' initialized." << std::endl;

}
//______________________________________________________________________________
void RADCOR::WriteTheROOTFile(){
   fDebugFile->cd();
   fDebugTree->Write();
   fDebugFile->Close();
   // delete fDebugTree;
   // delete fDebugFile;
   std::cout << "[RADCOR]: The data has been written to the ROOT file: " << fROOTFileName << std::endl;
}

//______________________________________________________________________________
Double_t RADCOR::Internal2DEnergyIntegral(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi){

   SetKinematics(Ebeam,Eprime,theta,phi);
   SetKinematicsForExactExternal(Ebeam,Eprime,theta,phi);

   Double_t Es     = fKinematics.GetEs();
   Double_t Ep     = fKinematics.GetEp();
   Double_t M      = fKinematics.GetM();
   Double_t tr     = GetTr(fKinematics.GetQ2());
   //std::cout << " tr = " << tr << std::endl;

   fThreshold = 2;
   // First variable is Es
   Double_t min[2] = {GetEsMin(Ep,theta), Ep + fDelta};
   Double_t max[2] = {Es - fDelta,  GetEpMax(Es,theta)};

   if( min[0] > max[0] ||  min[1] > max[1] ){ 
      return(0.0);
   }

   ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kVEGAS;//,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kADAPTIVE;//,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kMISER;
   double absErr                           = 1.0e100;   /// desired absolute Error 
   double relErr                           = 1.0e-4;   /// desired relative Error 
   unsigned int ncalls                     = 1000; /// number of calls (MC only)
   int rule                                = 1;        /// Gauss-Kronrod integration rule (only for GSL kADAPTIVE type)    
   ROOT::Math::IntegratorMultiDim ig(type,absErr,relErr,ncalls); 

   External2DEnergyIntegral_IntegrandWrap f1;
   f1.fRADCOR = this;
   f1.Es    = Es;
   f1.Ep    = Ep;
   f1.theta = theta;
   f1.phi   = phi;
   f1.t1    = tr;
   f1.t2    = tr;
   ig.SetFunction(f1);
   
   Double_t res1 = ig.Integral(min,max);
   return res1; 
}
//______________________________________________________________________________
Double_t RADCOR::External2DEnergyIntegral(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi){

   SetKinematics(Ebeam,Eprime,theta,phi);
   SetKinematicsForExactExternal(Ebeam,Eprime,theta,phi);

   Double_t Es     = fKinematics.GetEs();
   Double_t Ep     = fKinematics.GetEp();
   Double_t M      = fKinematics.GetM();

   fThreshold = 2;
   // First variable is Es
   Double_t min[2] = {GetEsMin(Ep,theta), Ep /*+ fDelta*/};
   Double_t max[2] = {Es /*- fDelta*/,  GetEpMax(Es,theta)};

   if( min[0] > max[0] ||  min[1] > max[1] ){ 
      return(0.0);
   }

   ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kVEGAS;//,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kADAPTIVE;//,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kMISER;
   double absErr                           = 1.0e100;   /// desired absolute Error 
   double relErr                           = 1.0e-4;   /// desired relative Error 
   unsigned int ncalls                     = 1000; /// number of calls (MC only)
   int rule                                = 1;        /// Gauss-Kronrod integration rule (only for GSL kADAPTIVE type)    
   ROOT::Math::IntegratorMultiDim ig(type,absErr,relErr,ncalls); 

   External2DEnergyIntegral_IntegrandWrap f1;
   f1.fRADCOR = this;
   f1.Es    = Es;
   f1.Ep    = Ep;
   f1.theta = theta;
   f1.phi   = phi;
   f1.t1    = fKinematics.Gett_b();
   f1.t2    = fKinematics.Gett_a();
   ig.SetFunction(f1);
   
   Double_t res1 = ig.Integral(min,max);
   return res1; 
}
//______________________________________________________________________________
Double_t RADCOR::External2DEnergyIntegral_Integrand(Double_t Es,  Double_t Ep, Double_t theta, Double_t phi, Double_t t1, Double_t t2, Double_t EsPrime, Double_t EpPrime){
   if( GetEpMax(EsPrime,theta) < EpPrime) return(0.0);
   // This is the Es' and Ep' integrand
   //std::cout << "Es = " << Es << std::endl;
   //std::cout << "Ep = " << Ep << std::endl;
   //std::cout << "Es'= " << EsPrime << std::endl;
   //std::cout << "Ep'= " << EpPrime << std::endl;
   //std::cout << "t1 = " << t1 << std::endl;
   Double_t I1 = Ib_2(Es,EsPrime,t1); 
   Double_t I2 = Ib_2(EpPrime,Ep,t2); 
   Double_t par[] = {EpPrime,theta,phi};
   fUnpolXS->SetBeamEnergy(EsPrime);
   //std::cout << " I1 = " << I1 << std::endl;
   //std::cout << " I2 = " << I2 << std::endl;
   Double_t res = I1*fUnpolXS->EvaluateBaseXSec(par)*I2;
   //std::cout << " res = " << res << std::endl;
   return res;
}
//______________________________________________________________________________
Double_t RADCOR::ContinuumStragglingStripApprox_InternalEquivRad(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi){

   // Mo and Tsai 1971, C23

   SetKinematics(Ebeam,Eprime,theta,phi);
   SetKinematicsForExactExternal(Ebeam,Eprime,theta,phi);
   fThreshold = 2;

   Double_t Es     = fKinematics.GetEs();
   Double_t Ep     = fKinematics.GetEp();
   Double_t T      = 2.0*GetTr(fKinematics.GetQ2());
   Double_t delta_s= Delta(Ebeam, T/2.0); 
   Double_t delta_p= Delta(Eprime,T/2.0); 
   Double_t b      = fKinematics.Getb();
   Double_t gamma  = 1.0 + 0.5772*b*T;//TMath::Gamma(1.0 +b*T);
   Double_t M      = fKinematics.GetM();

   Double_t min_Ep = Ep + fDelta;
   Double_t max_Ep = GetEpMax(Es,theta);

   Double_t R      = (M + Es*(1.0 - TMath::Cos(theta)))/(M - max_Ep*(1.0 - TMath::Cos(theta))); 
   Double_t T0     = TMath::Power(R*fDelta/(Es),b*T/2.0)*TMath::Power(fDelta/(Ep),b*T/2.0);
 
   //std::cout << "-------------------------------------------" << std::endl;
   //std::cout << " R      = " << R << std::endl;
   //std::cout << " T      = " << T << std::endl;
   //std::cout << " delta  = " << fDelta << std::endl;
   //std::cout << " Es     = " << Es << std::endl;
   //std::cout << " Ep     = " << Ep << std::endl;
   //std::cout << " min_Ep = " << min_Ep << std::endl;
   //std::cout << " max_Ep = " << max_Ep << std::endl;

   Double_t min_Es = GetEsMin(Ep,theta);
   Double_t max_Es = Es - fDelta*R;

   if( min_Es > max_Es ||  min_Ep > max_Ep ){ 
      //std::cout << min_Ep << " < Ep' < " << max_Ep << std::endl;
      //std::cout << min_Es << " < Es' < " << max_Es << std::endl;
      //fKinematicsExt.Print();
      //fKinematics.Print();
      return(0.0);
   }

   //Double_t delta_s  = -0.5*fKinematics.Getb()*fKinematics.GetT()*TMath::Log(Es/fDelta);
   //Double_t delta_p  = -0.5*fKinematics.Getb()*fKinematics.GetT()*TMath::Log(Ep/fDelta);
   
   StripApproxIntegrand1Wrap f1;
   f1.fRADCOR = this;
   f1.Es     = Es;
   f1.Ep     = Ep;
   f1.T      = T;
   f1.theta  = theta;

   StripApproxIntegrand2Wrap f2;
   f2.fRADCOR = this;
   f2.Es     = Es;
   f2.Ep     = Ep;
   f2.T      = T;
   f2.theta  = theta;

   //ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR;
   ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVE; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   double absErr                           = 1.0e100;   /// desired absolute Error 
   double relErr                           = 1.0e-4;   /// desired relative Error 
   unsigned int ncalls                     = 1.0e4;  /// number of calls (MC only)
   unsigned int rule                       = 1;
   //ROOT::Math::IntegratorOneDim  ig(type,absErr,relErr,ncalls);//,rule);
   ROOT::Math::IntegratorOneDim  ig(type,absErr,relErr,ncalls,rule);

   ig.SetFunction( f1 );
   Double_t res1 = ig.Integral(min_Es,max_Es);

   ig.SetFunction( f2 );
   Double_t res2 = ig.Integral(min_Ep,max_Ep);

   Double_t par[] = {Eprime+delta_p,theta,0.0};
   fUnpolXS->SetBeamEnergy(Ebeam-delta_s);
   Double_t sig0 = fUnpolXS->EvaluateBaseXSec(par);
   Double_t sig_t = T0*sig0;
   sig_t         += res1;
   sig_t         += res2;
   sig_t         *= gamma;

   //std::cout << "-------------------------------------------" << std::endl;
   //std::cout << "gamma = " << gamma << std::endl;
   //std::cout << "res1  = " << res1 << std::endl;
   //std::cout << "res2  = " << res2 << std::endl;
   //std::cout << "T0    = " << T0 << std::endl;
   //std::cout << "sig_t = " << gamma << "(" << T0 << "*" << sig0 << "+" << res1 << "+" << res2 << ")" << std::endl;
   //std::cout << "sig_t = " << "(" << T0*gamma << "*" << sig0 << "+" << gamma*res1 << "+" << gamma*res2 << ")" << std::endl;
   //std::cout << "sig_t = " << "(" << T0*gamma*sig0 << "+" << gamma*res1 << "+" << gamma*res2 << ")" << std::endl;
   //std::cout << "sig_t = " << sig_t << std::endl;
   return sig_t; 
}
//______________________________________________________________________________
Double_t RADCOR::ContinuumStragglingStripApprox(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi){

   // Mo and Tsai 1971, C23

   SetKinematics(Ebeam,Eprime,theta,phi);
   SetKinematicsForExactExternal(Ebeam,Eprime,theta,phi);

   Double_t Es     = fKinematics.GetEs();
   Double_t Ep     = fKinematics.GetEp();
   Double_t T      = fKinematics.GetT() + 2.0*GetTr(fKinematics.GetQ2());
   Double_t b      = fKinematics.Getb();
   Double_t gamma  = 1.0 + 0.5772*b*T;//TMath::Gamma(1.0 +b*T);
   Double_t M      = fKinematics.GetM();

   Double_t min_Ep = Ep + fDelta;
   Double_t max_Ep = GetEpMax(Es,theta);

   Double_t R      = (M + Es*(1.0 - TMath::Cos(theta)))/(M - max_Ep*(1.0 - TMath::Cos(theta))); 
   Double_t T0     = TMath::Power(R*fDelta*fDelta/(Es*Ep),b*T/2.0);

   Double_t min_Es = GetEsMin(Ep,theta);
   Double_t max_Es = Es - fDelta*R;

   if( min_Es > max_Es ||  min_Ep > max_Ep ){ 
      //std::cout << min_Ep << " < Ep' < " << max_Ep << std::endl;
      //std::cout << min_Es << " < Es' < " << max_Es << std::endl;
      //fKinematicsExt.Print();
      //fKinematics.Print();
      return(0.0);
   }

   //Double_t delta_s  = -0.5*fKinematics.Getb()*fKinematics.GetT()*TMath::Log(Es/fDelta);
   //Double_t delta_p  = -0.5*fKinematics.Getb()*fKinematics.GetT()*TMath::Log(Ep/fDelta);
   
   StripApproxIntegrand1Wrap f1;
   f1.fRADCOR = this;
   f1.Es     = Es;
   f1.Ep     = Ep;
   f1.T      = T;
   f1.theta  = theta;

   StripApproxIntegrand2Wrap f2;
   f2.fRADCOR = this;
   f2.Es     = Es;
   f2.Ep     = Ep;
   f2.T      = T;
   f2.theta  = theta;

   ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVE; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   double absErr                           = 1.0e100;   /// desired absolute Error 
   double relErr                           = 1.0e-2;   /// desired relative Error 
   unsigned int ncalls                     = 1.0e2;  /// number of calls (MC only)
   unsigned int rule                       = 1;
   ROOT::Math::IntegratorOneDim  ig(type,absErr,relErr,ncalls,rule);

   ig.SetFunction( f1 );
   Double_t res1 = ig.Integral(min_Es,max_Es);

   ig.SetFunction( f2 );
   Double_t res2 = ig.Integral(min_Ep,max_Ep);

   Double_t par[] = {Ep,theta,0.0};
   fUnpolXS->SetBeamEnergy(Es);
   Double_t sig0 = fUnpolXS->EvaluateBaseXSec(par);
   Double_t sig_t = T0*sig0;
   sig_t         += res1;
   sig_t         += res2;
   sig_t         *= gamma;
   return sig_t; 
}
//______________________________________________________________________________
Double_t RADCOR::StripApproxIntegrand1_withD(Double_t Es,  Double_t Ep, Double_t theta, Double_t T, Double_t EsPrime){

   // Mo and Tsai 1971 C.23 
   // This is the Es' integrand
   Double_t b = fKinematics.Getb();
   Double_t M = fKinematics.GetM();
   //Double_t R = (M + Es*(1.0 - TMath::Cos(theta)))/(M - Ep*(1.0 - TMath::Cos(theta))); 
   Double_t max_Ep = GetEpMax(Es,theta);
   Double_t R      = (M + Es*(1.0 - TMath::Cos(theta)))/(M - max_Ep*(1.0 - TMath::Cos(theta))); 
   Double_t T1 = TMath::Power( (Es-EsPrime)*(Es-EsPrime)/(Ep*R*Es), b*T/2.0 )*b*T/(2.0*(Es-EsPrime));
   Double_t par[] = {Ep,theta,0.0};
   fUnpolXS->SetBeamEnergy(EsPrime);
   Double_t D = insane::Kine::BeamDepol(Es,EsPrime,1.0);
   return T1*fUnpolXS->EvaluateBaseXSec(par)*GetPhi((Es-EsPrime)/Es)*(1.0-D);
}
//______________________________________________________________________________
Double_t RADCOR::StripApproxIntegrand1(Double_t Es,  Double_t Ep, Double_t theta, Double_t T, Double_t EsPrime){

   // Mo and Tsai 1971 C.23 
   // This is the Es' integrand
   Double_t b = fKinematics.Getb();
   Double_t M = fKinematics.GetM();
   Double_t max_Ep = GetEpMax(Es,theta);
   Double_t R      = (M + Es*(1.0 - TMath::Cos(theta)))/(M - max_Ep*(1.0 - TMath::Cos(theta))); 
   //Double_t R = (M + Es*(1.0 - TMath::Cos(theta)))/(M - Ep*(1.0 - TMath::Cos(theta))); 
   Double_t T1 = TMath::Power( (Es-EsPrime)*(Es-EsPrime)/(Ep*R*Es), b*T/2.0);
   Double_t T2 = b*T/(2.0*(Es-EsPrime));
   Double_t par[] = {Ep,theta,0.0};
   fUnpolXS->SetBeamEnergy(EsPrime);
   Double_t f_tilde = insane::Kine::F_tilde_internal(EsPrime,Ep,theta);
   //std::cout << " T1 = " << T1 << std::endl;
   //std::cout << " T2 = " << T2 << std::endl;
   //std::cout << " R = " << R << std::endl;
   //std::cout << " sig = " << fUnpolXS->EvaluateBaseXSec(par) << std::endl;
   return T1*T2*f_tilde*fUnpolXS->EvaluateBaseXSec(par)*GetPhi((Es-EsPrime)/Es);
}
//______________________________________________________________________________
Double_t RADCOR::StripApproxIntegrand2(Double_t Es,  Double_t Ep, Double_t theta, Double_t T, Double_t EpPrime){

   // Mo and Tsai 1971 C.23 
   // This is the Ep' integrand
   Double_t b = fKinematics.Getb();
   Double_t M = fKinematics.GetM();
   Double_t max_Ep = GetEpMax(Es,theta);
   Double_t R      = (M + Es*(1.0 - TMath::Cos(theta)))/(M - max_Ep*(1.0 - TMath::Cos(theta))); 
   //Double_t R = (M + Es*(1.0 - TMath::Cos(theta)))/(M - Ep*(1.0 - TMath::Cos(theta))); 
   Double_t T1 = TMath::Power( (EpPrime-Ep)*(EpPrime-Ep)*R/(EpPrime*Es), b*T/2.0 );
   Double_t T2 = b*T/(2.0*(EpPrime-Ep));
   Double_t par[] = {EpPrime,theta,0.0};
   Double_t f_tilde = insane::Kine::F_tilde_internal(Es,EpPrime,theta);
   fUnpolXS->SetBeamEnergy(Es);
   return T1*T2*f_tilde*fUnpolXS->EvaluateBaseXSec(par)*GetPhi((EpPrime-Ep)/EpPrime);
}
//______________________________________________________________________________
Double_t RADCOR::Exact(Double_t Es,Double_t Ep,Double_t theta){

   // Set the kinematics
   SetKinematics(Es,Ep,theta);
   SetKinematicsForExactExternal(Es,Ep,theta);
   SetKinematicsForExactInternal(Es,Ep,theta);

   Double_t sig=0;

   if(fIsElastic){
      if( (fIsInternal)&&(!fIsExternal) ) sig = ExactElasticInternalOnly(Es,Ep,theta);  
      if( (fIsInternal)&&(fIsExternal)  ) sig = ExactElasticInternalExternal(Es,Ep,theta); 
      if( (!fIsInternal)&&(fIsExternal) ) sig = ExactElasticExternalOnly(Es,Ep,theta); 
   }else{
      if( (fIsInternal)&&(!fIsExternal) ) sig = ExactInelasticInternalOnly(Es,Ep,theta);  
      if( (fIsInternal)&&(fIsExternal)  ) sig = ExactInelasticInternalExternal(Es,Ep,theta); 
      if( (!fIsInternal)&&(fIsExternal) ) sig = ExactInelasticExternalOnly(Es,Ep,theta); 
   }

   // this should not be here. but further up the line. 
   //sig *= fhbar_c_sq; 

   return sig;
}
//______________________________________________________________________________
Double_t RADCOR::ExactElasticInternalOnly(Double_t Es,Double_t Ep,Double_t theta){
   /// Elastic radiative tail 
   /// Internal only
   Double_t M_A   = fKinematicsInt.GetM_A(); 
   Double_t alpha = fine_structure_const; 
   Double_t N_el  = (alpha*alpha*alpha/(4.*pi2))*(Ep/Es)*(1./M_A);   // Mo & Tsai 1969, for elastic XS  
   Double_t mp    = F_soft(Es,Ep,theta);

   Int_t Type = 0;  // 0 = internal; 1 = internal + external, 2 = external 
   Int_t ndim; 
   const Int_t MAXSIZE = 4; 
   Double_t Min[MAXSIZE],Max[MAXSIZE]; 
   SetIntegrationParametersElastic(Type,Es,Ep,theta,ndim,Min,Max); 

   const int NDim  = ndim; 
   Double_t sig=0,err=0; 

   Int_t MCEvents = fNumMCEvents; 
   Int_t Bin      = fMCBins;

   if(fMCIntegration==0){ 
      MCSampleMean(NDim,&RADCOR::InternalIntegrand_MoTsai69,&Min[0],&Max[0],MCEvents,sig,err);
   }else if(fMCIntegration==1){
      std::cout << "[RADCOR::Exact]: MC accept/reject not functional yet!  Exiting..." << std::endl;
      exit(1);
   }else if(fMCIntegration==2){
      std::cout << "[RADCOR::Exact]: MC importance sampling not functional yet!  Exiting..." << std::endl;
      exit(1);
   }else{
      sig = 0.;  
   }
   sig *= N_el; 

   if(fIsMultiPhoton) sig *= mp; 

   sig *= fhbar_c_sq; 

   return sig;

}
//______________________________________________________________________________
Double_t RADCOR::ExactElasticInternalExternal(Double_t Es,Double_t Ep,Double_t theta){
   /// Elastic radiative tail 
   /// Internal & external
   Double_t M_A   = fKinematicsInt.GetM_A(); 
   Double_t alpha = fine_structure_const; 
   Double_t T     = fKinematicsExt.GetT(); 
   Double_t mp    = F_soft(Es,Ep,theta);

   Int_t Type = 1;  // 0 = internal; 1 = internal + external, 2 = external 
   Int_t ndim; 
   const Int_t MAXSIZE = 4; 
   Double_t Min[MAXSIZE],Max[MAXSIZE]; 
   SetIntegrationParametersElastic(Type,Es,Ep,theta,ndim,Min,Max); 

   const int NDim  = ndim;

   Double_t sig=0,err=0; 

   Int_t MCEvents = fNumMCEvents; 
   Int_t Bin      = fMCBins;

   if(fMCIntegration==0){ 
      MCSampleMean(NDim,&RADCOR::EpExactIntegrand_4,Min,Max,MCEvents,sig,err); 
   }else if(fMCIntegration==1){
      std::cout << "[RADCOR::Exact]: MC accept/reject not functional yet!  Exiting..." << std::endl;
      exit(1);
   }else if(fMCIntegration==2){
      std::cout << "[RADCOR::Exact]: MC importance sampling not functional yet!  Exiting..." << std::endl;
      exit(1);
   }else{
      sig = 0.;  
   }
   sig *= 1./T; 
   
   if(fIsMultiPhoton) sig *= mp;        // FIXME: Is the multiple photon correction taken care of with the straggling functions?  

   sig *= fhbar_c_sq; 
   return sig; 

}
//______________________________________________________________________________
Double_t RADCOR::ExactElasticExternalOnly(Double_t Es,Double_t Ep,Double_t theta){
   /// Elastic radiative tail 
   /// External only  
   Double_t M_A   = fKinematicsInt.GetM_A(); 
   Double_t alpha = fine_structure_const; 
   Double_t N_el  = (alpha*alpha*alpha/(4.*pi2))*(Ep/Es)*(1./M_A);   // Mo & Tsai 1969, for elastic XS  
   Double_t T     = fKinematicsExt.GetT(); 
   Double_t mp    = F_soft(Es,Ep,theta);

   Int_t Type = 2;  // 0 = internal; 1 = internal + external, 2 = external 
   Int_t ndim; 
   const Int_t MAXSIZE = 4; 
   Double_t Min[MAXSIZE],Max[MAXSIZE]; 

   SetIntegrationParametersElastic(Type,Es,Ep,theta,ndim,Min,Max); 

   const int NDim  = ndim;

   Double_t sig=0,err=0; 

   Int_t MCEvents = fNumMCEvents; 
   Int_t Bin      = fMCBins;

   /// External only.
   if(fMCIntegration==0){ 
      MCSampleMean(NDim,&RADCOR::EpExactIntegrand_4,Min,Max,MCEvents,sig,err);
   }else if(fMCIntegration==1){
      std::cout << "[RADCOR::Exact]: MC accept/reject not functional yet!  Exiting..." << std::endl;
      exit(1);
   }else if(fMCIntegration==2){
      std::cout << "[RADCOR::Exact]: MC importance sampling not functional yet!  Exiting..." << std::endl;
      exit(1);
   }else{
      sig = 0.;  
   }
   sig *= N_el; 
   sig *= 1./T; 

   // if(fIsMultiPhoton) sig *= mp;        // FIXME: Is the multiple photon correction taken care of with the straggling functions?  

   return sig;  

}
//______________________________________________________________________________
Double_t RADCOR::ExactInelasticInternalOnly(Double_t Es,Double_t Ep,Double_t theta){
   /// Inelastic radiative tail  
   /// Internal only.  Different form from the other options 
   Double_t M           = fKinematicsInt.GetM(); 
   Double_t alpha       = fine_structure_const; 
   Double_t N_inel      = (alpha*alpha*alpha/(4.*pi2))*(Ep/Es)*(1./M);     // Mo & Tsai 1969, for inelastic XS  
   Double_t mp          = F_soft(Es,Ep,theta);
   Double_t delta_r     = Delta_r(fDelta); 
   Double_t delta_inf_1 = Delta_inf_1(fDelta);
   Double_t delta_inf_2 = Delta_inf_2();
   Double_t delta_inf   = delta_inf_1; 

   Int_t Type = 0;  // 0 = internal; 1 = internal + external, 2 = external 
   Int_t ndim1,ndim2; 
   const Int_t MAXSIZE = 5; 
   Double_t Min1[MAXSIZE],Max1[MAXSIZE]; 
   Double_t Min2[MAXSIZE],Max2[MAXSIZE]; 

   SetIntegrationParametersInelastic(Type,Es,Ep,theta,ndim1,Min1,Max1,ndim2,Min2,Max2); 

   const int NDim1  = ndim1;
   const int NDim2  = ndim2;

   Int_t MCEvents       = fNumMCEvents; 
   Int_t Bin            = fMCBins;

   Double_t sig_born_inel=0,sig1=0,sig2=0,err1=0,err2=0;
   Double_t par[3] = {Ep,theta,0.};  
   if(fPol==0){
      // unpolarized 
      sig_born_inel = fUnpolXS->EvaluateBaseXSec(par); 
   }else if(fPol>0){
      // polarized (para or perp) 
      sig_born_inel = fPolDiffXS->EvaluateBaseXSec(par); 
   }else{
      std::cout << "[RADCOR::ExactInelasticInternalOnly]: Error! Invalid cross section!  Exiting..." << std::endl;
      exit(1); 
   }

   sig_born_inel *= 1./fhbar_c_sq;  // make this unitless so it matches sig2.  

   // get sig1
   if(fIsMultiPhoton){ 
      // FIXME: What is the right way to do this? 
      // We mimic POLRAD here.  It seems that delta_v = delta_r = delta_inf + delta_vac + delta_vertex. 
      // the exponential term is inferred from looking at POLRAD...
      sig1    = TMath::Exp(delta_inf)*(1.+delta_r-delta_inf)*sig_born_inel; 
   }else{
      sig1    = (1.+delta_r)*sig_born_inel; // is this right?  
   }

   // get sig2 
   if(fMCIntegration==0){
      MCSampleMean(NDim2,&RADCOR::InternalIntegrand_Inelastic_MoTsai69,Min2,Max2,MCEvents,sig2,err2);
      sig2   *= N_inel;       // we need to scale this piece to the correct units!  
   }else if(fMCIntegration==1){
      std::cout << "[RADCOR::Exact]: MC accept/reject not functional yet!  Exiting..." << std::endl;
      exit(1);
   }else if(fMCIntegration==2){
      std::cout << "[RADCOR::Exact]: MC importance sampling not functional yet!  Exiting..." << std::endl;
      exit(1);
   }else{
      sig2 = 0.;  
   }

   Double_t sig  = sig1 + sig2;

   sig *= fhbar_c_sq; 
   return sig;

}
//______________________________________________________________________________
Double_t RADCOR::ExactInelasticInternalExternal(Double_t Es,Double_t Ep,Double_t theta){
   /// Inelastic radiative tail 
   /// Internal and external
   Double_t M           = fKinematicsInt.GetM(); 
   Double_t alpha       = fine_structure_const; 
   Double_t N_inel      = (alpha*alpha*alpha/(4.*pi2))*(Ep/Es)*(1./M);     // Mo & Tsai 1969, for inelastic XS  
   Double_t mp          = F_soft(Es,Ep,theta);
   Double_t T           = fKinematicsExt.GetT(); 
   Double_t delta_r     = Delta_r(fDelta); 
   Double_t delta_inf_1 = Delta_inf_1(fDelta);
   Double_t delta_inf_2 = Delta_inf_2();
   Double_t delta_inf   = delta_inf_1; 

   Int_t Type = 1;  // 0 = internal; 1 = internal + external, 2 = external 
   Int_t ndim1,ndim2; 
   const Int_t MAXSIZE = 5; 
   Double_t Min1[MAXSIZE],Max1[MAXSIZE]; 
   Double_t Min2[MAXSIZE],Max2[MAXSIZE]; 

   SetIntegrationParametersInelastic(Type,Es,Ep,theta,ndim1,Min1,Max1,ndim2,Min2,Max2); 

   const int NDim1  = ndim1;
   const int NDim2  = ndim2;

   Int_t MCEvents       = fNumMCEvents; 
   Int_t Bin            = fMCBins;

   Double_t sig=0,sig1=0,sig2=0,err1=0,err2=0; 

   if(fMCIntegration==0){ 
      MCSampleMean(NDim1,&RADCOR::EpExactIntegrand_Inelastic_Term_1,Min1,Max1,MCEvents,sig1,err1);
      MCSampleMean(NDim2,&RADCOR::EpExactIntegrand_Inelastic_Term_2,Min2,Max2,MCEvents,sig2,err2);	
   }else if(fMCIntegration==1){
      std::cout << "[RADCOR::Exact]: MC accept/reject not functional yet!  Exiting..." << std::endl;
      exit(1);
   }else if(fMCIntegration==2){
      std::cout << "[RADCOR::Exact]: MC importance sampling not functional yet!  Exiting..." << std::endl;
      exit(1);
   }else{
      sig = 0.;  
   }
   sig  = sig1 + sig2;
   sig *= 1./T; 

   // if(fIsMultiPhoton) sig *= mp;     // FIXME: Is the multiple photon correction *inside* the straggling functions?? 

   sig *= fhbar_c_sq; 
   return sig;

}
//______________________________________________________________________________
Double_t RADCOR::ExactInelasticExternalOnly(Double_t Es,Double_t Ep,Double_t theta){
   
   // Inelastic radiative tail 
   // External (straggling) only
   Double_t M           = fKinematicsInt.GetM(); 
   Double_t alpha       = fine_structure_const; 
   Double_t N_inel      = (alpha*alpha*alpha/(4.*pi2))*(Ep/Es)*(1./M);     // Mo & Tsai 1969, for inelastic XS  
   Double_t mp          = F_soft(Es,Ep,theta);
   Double_t T           = fKinematicsExt.GetT(); 
   //Double_t delta_r     = Delta_r(fDelta); 
   //Double_t delta_inf_1 = Delta_inf_1(fDelta);
   //Double_t delta_inf_2 = Delta_inf_2();
   //Double_t delta_inf   = delta_inf_1; 

   Int_t Type = 2;  // 0 = internal; 1 = internal + external, 2 = external 
   Int_t ndim1,ndim2; 
   const Int_t MAXSIZE = 5; 
   Int_t MCEvents   = fNumMCEvents; 
   Int_t Bin        = fMCBins;
   Double_t Min1[MAXSIZE],Max1[MAXSIZE]; 
   Double_t Min2[MAXSIZE],Max2[MAXSIZE]; 
   Double_t sig=0,sig1=0,err1=0,sig2=0,err2=0; 
   Double_t sig_total = 0.0;
   Double_t err_total = 0.0;

   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kVEGAS;//,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   //ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kADAPTIVE;//,kLEGENDRE,kADAPTIVESINGULAR
   ROOT::Math::IntegrationMultiDim::Type type = ROOT::Math::IntegrationMultiDim::kMISER;
   double absErr                           = 1.0e100;   /// desired absolute Error 
   double relErr                           = 1.0e-3;   /// desired relative Error 
   unsigned int ncalls                     = MCEvents; /// number of calls (MC only)
   int rule                                = 1;        /// Gauss-Kronrod integration rule (only for GSL kADAPTIVE type)    
   ROOT::Math::IntegratorMultiDim ig(type,absErr,relErr,ncalls); 

   ExternalOnly_ExactInelasticIntegrandWrap f1;
   f1.fRADCOR = this;
   ig.SetFunction(f1);


   SetIntegrationParametersInelastic(Type,Es,Ep,theta,ndim1,Min1,Max1,ndim2,Min2,Max2,1); 
   //MCSampleMean(ndim1,&RADCOR::ExternalOnly_ExactInelasticIntegrand,Min1,Max1,MCEvents,sig1,err1);
   sig1       = ig.Integral(Min1,Max1); 
   err1       = ig.Error();
   std::cout << " sig(1) = " << sig1 << std::endl;
   std::cout << " err(1) = " << err1 << std::endl;
   sig_total += sig1;
   err_total += err1;

   SetIntegrationParametersInelastic(Type,Es,Ep,theta,ndim1,Min1,Max1,ndim2,Min2,Max2,2); 
   //MCSampleMean(ndim1,&RADCOR::ExternalOnly_ExactInelasticIntegrand,Min1,Max1,MCEvents,sig1,err1);
   sig1       = ig.Integral(Min1,Max1); 
   err1       = ig.Error();
   std::cout << " sig(2) = " << sig1 << std::endl;
   std::cout << " err(2) = " << err1 << std::endl;
   sig_total += sig1;
   err_total += err1;

   SetIntegrationParametersInelastic(Type,Es,Ep,theta,ndim1,Min1,Max1,ndim2,Min2,Max2,3); 
   //MCSampleMean(ndim1,&RADCOR::ExternalOnly_ExactInelasticIntegrand,Min1,Max1,MCEvents,sig1,err1);
   sig1       = ig.Integral(Min1,Max1); 
   err1       = ig.Error();
   std::cout << " sig(3) = " << sig1 << std::endl;
   std::cout << " err(3) = " << err1 << std::endl;
   sig_total += sig1;
   err_total += err1;

   SetIntegrationParametersInelastic(Type,Es,Ep,theta,ndim1,Min1,Max1,ndim2,Min2,Max2,4); 
   //MCSampleMean(ndim1,&RADCOR::ExternalOnly_ExactInelasticIntegrand,Min1,Max1,MCEvents,sig1,err1);
   sig1       = ig.Integral(Min1,Max1); 
   err1       = ig.Error();
   std::cout << " sig(4) = " << sig1 << std::endl;
   std::cout << " err(4) = " << err1 << std::endl;
   sig_total += sig1;
   err_total += err1;

   //const int NDim1  = ndim1;
   //const int NDim2  = ndim2;


   //if(fMCIntegration==0){ 
   //   MCSampleMean(NDim1,&RADCOR::ExternalOnly_ExactInelasticIntegrand,Min1,Max1,MCEvents,sig1,err1);
   //   //MCSampleMean(NDim1,&RADCOR::EpExactIntegrand_Inelastic_Term_1,Min1,Max1,MCEvents,sig1,err1);
   //   // MCSampleMean(NDim2,&RADCOR::EpExactIntegrand_Inelastic_Term_2,&RADCOR::Boundary_Inelastic_2,Min2,Max2,MCEvents,sig2,err2);	
   //}else if(fMCIntegration==1){
   //   std::cout << "[RADCOR::Exact]: MC accept/reject not functional yet!  Exiting..." << std::endl;
   //   exit(1);
   //}else if(fMCIntegration==2){
   //   std::cout << "[RADCOR::Exact]: MC importance sampling not functional yet!  Exiting..." << std::endl;
   //   exit(1);
   //}else{
   //   sig = 0.;  
   //}
   sig = sig_total; // FIXME: I don't think we need the second term here, integrals over cos(thk) and omega are not done...  
   sig *= 1./T; 
   std::cout << "sig = "<<  sig << " +- " << err_total/T << std::endl;
   // if(fIsMultiPhoton) sig *= mp;     // FIXME: Is the multiple photon correction *inside* the straggling functions?? 

   return sig; 

}
//______________________________________________________________________________
void RADCOR::SetIntegrationParametersElastic(Int_t Type,Double_t Es,Double_t Ep,Double_t theta,
                                                   Int_t &ndim,Double_t *min ,Double_t *max){

   Double_t EsMin     =  GetEsMin(Ep,theta);
   Double_t EsMax     =  Es;  
   Double_t EpMin     =  Ep;
   Double_t EpMax     =  GetEpMax(EsMax,theta);  // the largest EpMax could ever be for this bin  
   Double_t T         =  fKinematicsExt.GetT(); 
   Double_t tMin      =  0.;
   Double_t tMax      =  T;
   Double_t costhkMin = -1.; 
   Double_t costhkMax =  1.; 

   switch(Type){
      case 0: // internal only 
         ndim   = 1; 
         min[0] = costhkMin; 
         max[0] = costhkMax; 
         break; 
       case 1: // internal + external 
         ndim   = 4; 
         min[0] = EpMin; 
         min[1] = EsMin; 
         min[2] = tMin; 
         min[3] = costhkMin; 
         max[0] = EpMax; 
         max[1] = EsMax; 
         max[2] = tMax; 
         max[3] = costhkMax; 
         break; 
       case 2: // external only 
         ndim   = 3; 
         min[0] = EpMin; 
         min[1] = EsMin; 
         min[2] = tMin; 
         max[0] = EpMax; 
         max[1] = EsMax; 
         max[2] = tMax; 
         break; 
       default: 
         std::cout << "[RADCOR::SetIntegrationParametersElastic]: Invalid choice!  Exiting..." << std::endl;
         exit(1); 
   }

}
//______________________________________________________________________________
void RADCOR::SetIntegrationParametersInelastic(Int_t Type,Double_t Es,Double_t Ep,Double_t theta,
                                                   Int_t &ndim1,Double_t *min1,Double_t *max1,
                                                   Int_t &ndim2,Double_t *min2,Double_t *max2, Int_t region){

   Double_t EsMin     =  GetEsMin(Ep,theta);
   Double_t EsMax     =  Es;  
   Double_t EpMin     =  Ep;
   Double_t EpMax     =  GetEpMax(EsMax,theta);  // the largest EpMax could ever be for this bin  
   Double_t T         =  fKinematics.GetT(); 
   Double_t tMin      =  0.;
   Double_t tMax      =  T;
   Double_t costhkMin = -1.; 
   Double_t costhkMax =  1.; 
   Double_t omegaMin  =  fDelta; 
   Double_t omegaMax  =  GetOmegaMax(costhkMax); 

   switch(Type){
      case 0: // internal only 
         ndim1   = 0;
         min1[0] = 0.;  
         max1[0] = 0.;  
         ndim2   = 2; 
         min2[0] = costhkMin; 
         min2[1] = omegaMin; 
         max2[0] = costhkMax; 
         max2[1] = omegaMax; 
         break; 
       case 1: // internal + external 
         ndim1   = 3; 
         min1[0] = EpMin; 
         min1[1] = EsMin; 
         min1[2] = tMin; 
         max1[0] = EpMax; 
         max1[1] = EsMax; 
         max1[2] = tMax; 
         ndim2   = 5; 
         min2[0] = EpMin; 
         min2[1] = EsMin; 
         min2[2] = tMin; 
         min2[3] = costhkMin; 
         min2[4] = omegaMin; 
         max2[0] = EpMax; 
         max2[1] = EsMax; 
         max2[2] = tMax; 
         max2[3] = costhkMax; 
         max2[4] = omegaMax; 
         break; 
       case 2: // external only 
         ndim1   = 3; 
         ndim2   = 3; 
         min2[0] = EpMin; 
         min2[1] = EsMin; 
         min2[2] = tMin; 
         max2[0] = EpMax; 
         max2[1] = EsMax; 
         max2[2] = tMax; 
         if(region == 0) {
            min1[0] = EpMin; 
            min1[1] = EsMin; 
            min1[2] = tMin; 
            max1[0] = EpMax; 
            max1[1] = EsMax; 
            max1[2] = tMax; 
         }else if(region == 1) {
            min1[0] = EpMin; 
            min1[1] = EsMax-fDelta; 
            min1[2] = tMin; 
            max1[0] = EpMin+fDelta; 
            max1[1] = EsMax; 
            max1[2] = tMax; 
         }else if(region == 2) {
            min1[0] = EpMin; 
            min1[1] = EsMin; 
            min1[2] = tMin; 
            max1[0] = EpMin+fDelta; 
            max1[1] = EsMax-fDelta; 
            max1[2] = tMax; 
         }else if(region == 3) {
            min1[0] = EpMin+fDelta; 
            min1[1] = EsMax-fDelta; 
            min1[2] = tMin; 
            max1[0] = EpMax; 
            max1[1] = EsMax; 
            max1[2] = tMax; 
         }else if(region == 4) {
            min1[0] = EpMin+fDelta; 
            min1[1] = EsMin; 
            min1[2] = tMin; 
            max1[0] = EpMax; 
            max1[1] = EsMax-fDelta; 
            max1[2] = tMax; 
         }
         break; 
       default: 
         std::cout << "[RADCOR::SetIntegrationParametersElastic]: Invalid choice!  Exiting..." << std::endl;
         exit(1); 
   }

}
//______________________________________________________________________________
Double_t RADCOR::CalculateTExactIntegral(){

   /// Exact integral from Mo & Tsai (Eq A12) 
   Double_t T   = fKinematicsExt.GetT(); 
   Double_t min = 0; 
   Double_t max = T; 
   if(fDebug>0){
      std::cout << "int min = " << min << std::endl;
      std::cout << "int max = " << max << std::endl;
   }

   TExactFuncWrap funcWrap;
   funcWrap.fRADCOR = this;

   double absErr       = 1e100;  /// desired absolute Error 
   double relErr       = 1e-05;  /// desired relative Error 
   unsigned int ncalls = 1.0e6;  /// number of calls (MC only)

   ROOT::Math::IntegrationOneDim::Type Legendre         = ROOT::Math::IntegrationOneDim::kLEGENDRE;         // kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   ROOT::Math::IntegrationOneDim::Type Adaptive         = ROOT::Math::IntegrationOneDim::kADAPTIVE;         // kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   ROOT::Math::IntegrationOneDim::Type AdaptiveSingular = ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR; // kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR

   /// Legendre 
   ROOT::Math::IntegratorOneDim  ig2_leg(Legendre,absErr,relErr,ncalls);
   ig2_leg.SetFunction( funcWrap );
   /// adaptive 
   ROOT::Math::IntegratorOneDim  ig2_adapt(Adaptive,absErr,relErr,ncalls);
   ig2_adapt.SetFunction( funcWrap );
   /// adaptive singular 
   ROOT::Math::IntegratorOneDim  ig2_adapt_singular(AdaptiveSingular,absErr,relErr,ncalls);
   ig2_adapt_singular.SetFunction( funcWrap );
   /// integration 
   auto integral = Double_t( ig2_adapt_singular.Integral(min,max) );
   // Double_t integral = AdaptiveSimpson(&RADCOR::TExactIntegrand,min,max,fEpsilon,fDepth); 

   return integral;

}
//______________________________________________________________________________
Double_t RADCOR::TExactIntegrand(Double_t &t){

   fthickness = t;
   Double_t T = fKinematicsExt.GetT(); 
   Double_t arg=0;
   if(T>0){
      arg = (1./T)*CalculateEsExactIntegral();
   }else{
      std::cout << "[RADCOR]: Thickness is zero!  Exiting..." << std::endl;
      exit(1);
   }
   return arg;

}
//_____________________________________________________________________________________________
Double_t RADCOR::CalculateEsExactIntegral(){

   Double_t Es     = fKinematicsExt.GetEs();
   Double_t Ep     = fKinematicsExt.GetEp();
   Double_t theta  = fKinematicsExt.GetTheta();
   Double_t min    = GetEsMin(Ep,theta); 
   Double_t max    = Es;       
   if(fDebug>0){
      std::cout << "-------- Es Integral -------" << std::endl; 
      std::cout << "int min = " << min << std::endl;
      std::cout << "int max = " << max << std::endl;
   }

   EsExactFuncWrap funcWrap;
   funcWrap.fRADCOR = this;

   EsExactFunc2Wrap funcWrap2;
   funcWrap2.fRADCOR = this;

   double absErr       = 1e100;  /// desired absolute Error 
   double relErr       = 1e-01;  /// desired relative Error 
   unsigned int ncalls = 1.0e6;  /// number of calls (MC only)

   ROOT::Math::IntegrationOneDim::Type Legendre         = ROOT::Math::IntegrationOneDim::kLEGENDRE; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   ROOT::Math::IntegrationOneDim::Type Adaptive         = ROOT::Math::IntegrationOneDim::kADAPTIVE; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   ROOT::Math::IntegrationOneDim::Type AdaptiveSingular = ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR

   /// Legendre 
   ROOT::Math::IntegratorOneDim  ig2_leg(Legendre,absErr,relErr,ncalls);
   ig2_leg.SetFunction( funcWrap2 );
   /// adaptive 
   ROOT::Math::IntegratorOneDim  ig2_adapt(Adaptive,absErr,relErr,ncalls);
   ig2_adapt.SetFunction( funcWrap2 );
   /// adaptive singular 
   ROOT::Math::IntegratorOneDim  ig2_adapt_singular(AdaptiveSingular,absErr,relErr,ncalls);
   ig2_adapt_singular.SetFunction( funcWrap2 );

   /// integration 
   auto integral = Double_t( ig2_adapt_singular.Integral(min,max) );
   // Double_t integral = AdaptiveSimpson(&RADCOR::EsExactIntegrand,min,max,fEpsilon,fDepth);

   return integral;   

}
//______________________________________________________________________________
Double_t RADCOR::EsExactIntegrand(Double_t &EsPrime){

   Double_t arg = CalculateEpExactIntegral(EsPrime);
   return arg;

}
//______________________________________________________________________________
Double_t RADCOR::EsExactIntegrand_2(Double_t &EsPrime){

   Double_t Es       = fKinematicsExt.GetEs(); 
   Double_t t        = fthickness; 
   Double_t ie       = 0;

   if(Es>EsPrime) ie = Ie_2(Es,EsPrime,t);

   Int_t NotANumber  = TMath::IsNaN(ie);

   if(NotANumber) ie = 0; 

   Double_t integral = CalculateEpExactIntegral(EsPrime);
   Double_t arg      = ie*integral;

   return arg;

}
//______________________________________________________________________________
Double_t RADCOR::CalculateEpExactIntegral(Double_t EsPrime){

   fEsPrime       = EsPrime; 
   Double_t Ep    = fKinematicsExt.GetEp();
   Double_t theta = fKinematicsExt.GetTheta();
   Double_t min   = Ep;                /// add a small term to avoid negative ionization terms      
   Double_t max   = GetEpMax(EsPrime,theta);

   EpExactFuncWrap funcWrap;
   funcWrap.fRADCOR = this;

   EpExactFunc2Wrap funcWrap2;
   funcWrap2.fRADCOR = this;

   double absErr       = 1e100;  /// desired absolute Error 
   double relErr       = 1e-05;  /// desired relative Error 
   unsigned int ncalls = 1.0e6;  /// number of calls (MC only)

   ROOT::Math::IntegrationOneDim::Type Legendre         = ROOT::Math::IntegrationOneDim::kLEGENDRE; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   ROOT::Math::IntegrationOneDim::Type Adaptive         = ROOT::Math::IntegrationOneDim::kADAPTIVE; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   ROOT::Math::IntegrationOneDim::Type AdaptiveSingular = ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR

   /// Legendre 
   ROOT::Math::IntegratorOneDim  ig2_leg(Legendre,absErr,relErr,ncalls);
   ig2_leg.SetFunction( funcWrap2 );
   /// adaptive 
   ROOT::Math::IntegratorOneDim  ig2_adapt(Adaptive,absErr,relErr,ncalls);
   ig2_adapt.SetFunction( funcWrap2 );
   /// adaptive singular 
   ROOT::Math::IntegratorOneDim  ig2_adapt_singular(AdaptiveSingular,absErr,relErr,ncalls);
   ig2_adapt_singular.SetFunction( funcWrap2 );
   /// integration 
   Double_t integral = 0.; 

   if(min==max){
      integral = 0.; 
   }else if(min>max){
      std::cout << "[RADCOR::CalculateEpExactIntegral]: FAIL.  min > max!" << std::endl;
      std::cout << "EsPrime  = " << EsPrime      << std::endl;
      std::cout << "Ep  = "      << Ep           << std::endl;
      std::cout << "th  = "      << theta/degree << std::endl;
      std::cout << "min = "      << min          << std::endl;
      std::cout << "max = "      << max          << std::endl;
      exit(1);
   }else if(EsPrime<max){
      std::cout << "[RADCOR::CalculateEpExactIntegral]: FAIL.  max > EsPrime!" << std::endl;
      std::cout << "EsPrime = " << EsPrime      << std::endl;
      std::cout << "Ep  = "     << Ep           << std::endl;
      std::cout << "th  = "     << theta/degree << std::endl;
      std::cout << "max = "     << max          << std::endl;
      exit(1);
   }else{
      integral = Double_t( ig2_adapt_singular.Integral(min,max) );
      // integral = AdaptiveSimpson(&RADCOR::EpExactIntegrand,min,max,fEpsilon,fDepth);
   }

   if(fDebug>0){
      std::cout << "-------- Ep Integral -------" << std::endl; 
      std::cout << "EsPrime = " << EsPrime << std::endl;
      std::cout << "int min = " << min     << std::endl;
      std::cout << "int max = " << max     << std::endl;
   }

   return integral;   

}
//______________________________________________________________________________
Double_t RADCOR::EpExactIntegrand(Double_t &EpPrime){

   // General terms 
   Double_t EsPrime = fEsPrime; 
   Double_t Es      = fKinematicsExt.GetEs();
   Double_t Ep      = fKinematicsExt.GetEp();
   Double_t th      = fKinematicsExt.GetTheta();
   Double_t T       = fKinematicsExt.GetT();
   Double_t t       = fthickness;
   Double_t T1=0,T2=0,T3=0;
   // First term  
   // T1  = Ie(Es,EsPrime,fthickness);
   // T1  = Ie_new(Es,EsPrime,t);
   if(Es>EsPrime){
      T1  = Ie_2(Es,EsPrime,t);
   }
   // Second term
   // [Exact] internally radiated XS tail: Eq. A24 in Stein, B5 in Mo & Tsai  
   // Double_t mp  = F_soft(fEsPrime,EpPrime,th);                  // correct for multiple photon radiation 
   if(EsPrime>EpPrime){
      if(fIsElastic){
         T2  = GetInternalRadiatedXS(EsPrime,EpPrime,th);
      }else{
         T2  = GetInternalRadiatedXS_Inelastic(EsPrime,EpPrime,th);
      }
   }
   // Third term
   // T3  = Ie_new(EpPrime,Ep,T-t);
   if(EpPrime>Ep){
      T3  = Ie_2(EpPrime,Ep,T-t);
   }

   Int_t NotANumber_1 = TMath::IsNaN(T1); 
   Int_t NotANumber_2 = TMath::IsNaN(T2); 
   Int_t NotANumber_3 = TMath::IsNaN(T3); 

   if(NotANumber_1) T1 = 0.;
   if(NotANumber_2) T2 = 0.;
   if(NotANumber_3) T3 = 0.;

   Double_t arg = T1*T2*T3;

   if(fDebug> 3){
      std::cout << "--------- Ep integrand ---------" << std::endl;
      std::cout << "t       = " << t        << std::endl;
      std::cout << "T       = " << T        << std::endl;
      std::cout << "Es      = " << Es       << std::endl;
      std::cout << "EsPrime = " << EsPrime  << std::endl;
      std::cout << "Ep      = " << Ep       << std::endl;
      std::cout << "EpPrime = " << EpPrime  << std::endl;
      // std::cout << "mp      = " << mp       << std::endl;
      std::cout << "T1      = " << T1       << std::endl;
      std::cout << "T2      = " << T2       << std::endl;
      std::cout << "T3      = " << T3       << std::endl;
      std::cout << "arg     = " << arg      << std::endl;
   }

   // if(Es <= EsPrime){
   //   std::cout << "[RADCOR::EpExactIntegrand]: Es <= Es'!  Exiting... " << std::endl;
   //   exit(1); 
   // }
   // if(Ep >= EpPrime){
   //   std::cout << "[RADCOR::EpExactIntegrand]: Ep >= Ep'!  Exiting... " << std::endl;
   //   exit(1); 
   // }      
   // if(EsPrime <= EpPrime){
   //   std::cout << "[RADCOR::EpExactIntegrand]: Es' <= Ep'!  Exiting... " << std::endl;
   //   exit(1); 
   // }     

   return arg;

}
//______________________________________________________________________________
Double_t RADCOR::EpExactIntegrand_2(Double_t &EpPrime){

   // General terms 
   Double_t EsPrime = fEsPrime; 
   Double_t Es      = fKinematicsExt.GetEs();
   Double_t Ep      = fKinematicsExt.GetEp();
   Double_t th      = fKinematicsExt.GetTheta();
   Double_t T       = fKinematicsExt.GetT();
   Double_t t       = fthickness;
   Double_t T1=0,T2=0;
   // First term
   // [Exact] internally radiated XS tail: Eq. A24 in Stein, B5 in Mo & Tsai  
   // Double_t mp  = F_soft(fEsPrime,EpPrime,th);                  // correct for multiple photon radiation 
   if(EsPrime>EpPrime){
      if(fIsElastic){
         T1  = GetInternalRadiatedXS(EsPrime,EpPrime,th);
      }else{
         T1  = GetInternalRadiatedXS_Inelastic(EsPrime,EpPrime,th);
      }
   }
   // Third term
   // T3  = Ie_new(EpPrime,Ep,T-t);
   if(EpPrime>Ep){
      T2  = Ie_2(EpPrime,Ep,T-t);
   }

   Int_t NotANumber_1 = TMath::IsNaN(T1); 
   Int_t NotANumber_2 = TMath::IsNaN(T2); 

   if(NotANumber_1) T1 = 0.;
   if(NotANumber_2) T2 = 0.;

   Double_t arg = T1*T2;

   if(fDebug> 3){
      std::cout << "--------- Ep integrand ---------" << std::endl;
      std::cout << "t       = " << t        << std::endl;
      std::cout << "T       = " << T        << std::endl;
      std::cout << "Es      = " << Es       << std::endl;
      std::cout << "EsPrime = " << EsPrime  << std::endl;
      std::cout << "Ep      = " << Ep       << std::endl;
      std::cout << "EpPrime = " << EpPrime  << std::endl;
      // std::cout << "mp      = " << mp       << std::endl;
      std::cout << "T1      = " << T1       << std::endl;
      std::cout << "T2      = " << T2       << std::endl;
      std::cout << "arg     = " << arg      << std::endl;
   }

   return arg;

}
//______________________________________________________________________________
Double_t RADCOR::EpExactIntegrand_3(Double_t &EsPrime,Double_t &EpPrime,Double_t &t){

   // Elastic 
   // Use for Monte Carlo 3D integration over Es', Ep' and t  

   // General terms 
   Double_t Es      = fKinematicsExt.GetEs();
   Double_t Ep      = fKinematicsExt.GetEp();
   Double_t th      = fKinematicsExt.GetTheta();
   Double_t T       = fKinematicsExt.GetT();
   Double_t T1=0,T2=0,T3=0;
   // First term  
   if(Es>EsPrime){
      T1  = Ib_2(Es,EsPrime,t);
   }
   // Second term
   // Needs to be an elastic cross section! 
   Double_t par[3] = {EpPrime,th,0.}; 
   fUnpolXSEl->SetBeamEnergy(EsPrime);  
   fPolDiffXSEl->SetBeamEnergy(EsPrime);  
   if(EsPrime>EpPrime){
      if(fPol==0){
         T2  = fUnpolXSEl->EvaluateBaseXSec(par);
      }else{
         T2  = fPolDiffXSEl->EvaluateBaseXSec(par);
      }
   }
   // Third term
   if(EpPrime>Ep){
      T3  = Ib_2(EpPrime,Ep,T-t);
   }

   Int_t NotANumber_1 = TMath::IsNaN(T1); 
   Int_t NotANumber_2 = TMath::IsNaN(T2); 
   Int_t NotANumber_3 = TMath::IsNaN(T3); 

   if(NotANumber_1) T1 = 0.;
   if(NotANumber_2) T2 = 0.;
   if(NotANumber_3) T3 = 0.;

   Double_t arg = T1*T2*T3;

   if(fDebug> 3){
      std::cout << "--------- Ep integrand (3D) ---------" << std::endl;
      std::cout << "t       = " << t        << std::endl;
      std::cout << "T       = " << T        << std::endl;
      std::cout << "Es      = " << Es       << std::endl;
      std::cout << "EsPrime = " << EsPrime  << std::endl;
      std::cout << "Ep      = " << Ep       << std::endl;
      std::cout << "EpPrime = " << EpPrime  << std::endl;
      std::cout << "T1      = " << T1       << std::endl;
      std::cout << "T2      = " << T2       << std::endl;
      std::cout << "T3      = " << T3       << std::endl;
      std::cout << "arg     = " << arg      << std::endl;
   }

   // if(Es <= EsPrime){
   //   std::cout << "[RADCOR::EpExactIntegrand]: Es <= Es'!  Exiting... " << std::endl;
   //   exit(1); 
   // }
   // if(Ep >= EpPrime){
   //   std::cout << "[RADCOR::EpExactIntegrand]: Ep >= Ep'!  Exiting... " << std::endl;
   //   exit(1); 
   // }      
   // if(EsPrime <= EpPrime){
   //   std::cout << "[RADCOR::EpExactIntegrand]: Es' <= Ep'!  Exiting... " << std::endl;
   //   exit(1); 
   // }     

   return arg;

}
//______________________________________________________________________________
Double_t RADCOR::EpExactIntegrand_4(Double_t *x){

   /// For Monte Carlo 4D integration... 
   /// Integrate over Es',Ep', t and the real photon angle cos(thk)  
   /// Elastic  

   // check the boundary first 
   if( Boundary(x) == 0) return(0.0);

   // grab the values
   // NOTE THE ORDER!  
   Double_t EpPrime = x[0]; 
   Double_t EsPrime = x[1]; 
   Double_t t       = x[2];
   Double_t COSTHK  = 0; 

   if( fIsInternal ) COSTHK = x[3]; 

   // General terms 
   Double_t Es      = fKinematicsExt.GetEs();
   Double_t Ep      = fKinematicsExt.GetEp();
   Double_t theta   = fKinematicsExt.GetTheta();
   Double_t T       = fKinematicsExt.GetT();

   Double_t T1=0,T2=0,T3=0;
   // First term 
   if(Es>EsPrime){
      T1  = Ib_2(Es,EsPrime,t);   
   }
   // Second term
   // Exact internally radiated XS tail: Eq. A24 in Stein, B5 in Mo & Tsai 
   // NOTE: We have to set the kinematics for the exact internal data members! (Es',Ep',theta)  
   //       We are integrating over cos(thk)... 
   SetKinematicsForExactInternal(EsPrime,EpPrime,theta);
   Double_t M_A   = fKinematicsExt.GetM_A();
   Double_t alpha = fine_structure_const;
   Double_t N = 0.;
   Double_t sig_int=0.;
   Double_t par[3] = {EpPrime,theta,0.}; 
   if(EsPrime>EpPrime){
      if(fIsInternal){
         // N       = (alpha*alpha*alpha/(2.*pi))*(EpPrime/EsPrime)*M_A;         // for Stein's calculation 
         N       = (alpha*alpha*alpha/(4.*pi2))*(EpPrime/EsPrime)*(1./M_A);  // for Mo & Tsai's calculation 
         sig_int = InternalIntegrand_MoTsai69(COSTHK); 
         T2      = N*sig_int;
      }else{
         if(fPol==0){
            T2  = fUnpolXSEl->EvaluateBaseXSec(par);
         }else{
            T2  = fPolDiffXSEl->EvaluateBaseXSec(par);
         }
      }
   }
   // Third term
   if(EpPrime>Ep){
      T3  = Ib_2(EpPrime,Ep,T-t);
   }

   Int_t NotANumber_1 = TMath::IsNaN(T1); 
   Int_t NotANumber_2 = TMath::IsNaN(T2); 
   Int_t NotANumber_3 = TMath::IsNaN(T3); 

   if(NotANumber_1) T1 = 0.;
   if(NotANumber_2) T2 = 0.;
   if(NotANumber_3) T3 = 0.;

   rEs_ext      = Es; 
   rEp_ext      = Ep;
   rEsPrime_ext = EsPrime; 
   rEpPrime_ext = EpPrime; 
   rtPrime_ext  = t; 
   rT1_ext      = T1; 
   rT2_ext      = T2; 
   rT3_ext      = T3; 
   fDebugTree->Fill();

   Double_t arg = T1*T2*T3;

   if(fDebug>3){
      std::cout << "--------- EpExactIntegrand_4 ---------" << std::endl;
      std::cout << "t       = " << t        << std::endl;
      std::cout << "T       = " << T        << std::endl;
      std::cout << "Es      = " << Es       << std::endl;
      std::cout << "EsPrime = " << EsPrime  << std::endl;
      std::cout << "Ep      = " << Ep       << std::endl;
      std::cout << "EpPrime = " << EpPrime  << std::endl;
      std::cout << "cos(thk)= " << COSTHK   << std::endl;
      std::cout << "T1      = " << T1       << std::endl;
      std::cout << "T2      = " << T2       << std::endl;
      std::cout << "T3      = " << T3       << std::endl;
      std::cout << "arg     = " << arg      << std::endl;
   }

   return arg;

}
//______________________________________________________________________________
Double_t RADCOR::ExternalOnly_ExactInelasticIntegrand(Double_t *x){

   /// For Monte Carlo 3D integration... 
   /// Integrate over Es',Ep', and t 
   /// Integrand from Mo Tsai A.22

   // check the boundary first  
   if( Boundary_Inelastic_1(x) == 0 ) return 0.; 

   // grab the values
   // NOTE THE ORDER!  
   Double_t EpPrime = x[0]; 
   Double_t EsPrime = x[1]; 
   Double_t t       = x[2]; 
   // General terms 
   //Double_t Es      = fKinematicsExt.GetEs();
   //Double_t Ep      = fKinematicsExt.GetEp();
   //Double_t theta   = fKinematicsExt.GetTheta();
   //Double_t T       = fKinematicsExt.GetT();
   //Double_t M       = fKinematicsExt.GetM(); 
   Double_t Es      = fKinematics.GetEs();
   Double_t Ep      = fKinematics.GetEp();
   Double_t theta   = fKinematics.GetTheta();
   Double_t T       = fKinematics.GetT();
   Double_t M       = fKinematics.GetM(); 
   // for the Born xs  
   Double_t par[3]  = {EpPrime,theta,0.};
   fUnpolXS->SetBeamEnergy(EsPrime); 
   fPolDiffXS->SetBeamEnergy(EsPrime); 

   Double_t T1=0,T2=0,T3=0;

   if(Es>EsPrime){
      T1  = Ib_2(Es,EsPrime,t);   
   }

   if(EsPrime>EpPrime){
      if(fPol==0){
         // unpolarized 
         T2 = fUnpolXS->EvaluateBaseXSec(par);            // units: nb/GeV/sr  
      }else if(fPol>0){
         // polarized (para or perp) 
         T2 = fPolDiffXS->EvaluateBaseXSec(par);          // units: nb/GeV/sr
      }else{
         std::cout << "[RADCOR::ExternalOnly_ExactInelasticIntegrand]: Error! Invalid cross section!  Exiting..." << std::endl;
         exit(1); 
      }
   }

   if(EpPrime>Ep){
      T3  = Ib_2(EpPrime,Ep,T-t);
   }
   Double_t arg = T1*T2*T3;
   if(fDebug>3){
      std::cout << "--------- Ep integrand (3D) [term 1] ---------" << std::endl;
      std::cout << "t       = " << t        << std::endl;
      std::cout << "T       = " << T        << std::endl;
      std::cout << "Es      = " << Es       << std::endl;
      std::cout << "EsPrime = " << EsPrime  << std::endl;
      std::cout << "Ep      = " << Ep       << std::endl;
      std::cout << "EpPrime = " << EpPrime  << std::endl;
      std::cout << "T1      = " << T1       << std::endl;
      std::cout << "T2      = " << T2       << std::endl;
      std::cout << "T3      = " << T3       << std::endl;
      std::cout << "arg     = " << arg      << std::endl;
   }
   return arg;
}

//______________________________________________________________________________
Double_t RADCOR::ExternalOnly_ElasticPeak(Double_t Es,Double_t Ep,Double_t theta, Double_t phi){
   // Equation A.15 of Mo and Tsai 1969.
   SetKinematics(Es,Ep,theta); 
   Double_t T       = fKinematics.GetT();
   Double_t b       = fKinematics.Getb();
   Double_t M       = fKinematics.GetM(); 
   Double_t eta     = 1.0+Es*(1.0-TMath::Cos(theta))/M;
   Double_t DeltaE  = Es-Ep; // not sure about this one
   Double_t Epmax   = Es/eta;
   Double_t par[3]  = {theta,phi,0.0};
   fUnpolXSEl->SetBeamEnergy(Es); 
   Double_t sig1    = fUnpolXSEl->EvaluateBaseXSec(fUnpolXSEl->GetDependentVariables(par)); 
   Double_t T1      = DeltaE/Epmax;
   Double_t T2      = DeltaE*eta*eta/Es;
   Double_t res     = TMath::Power( T1*T2, b*T/2.0)*sig1;
   return(res);
}

//______________________________________________________________________________
Double_t RADCOR::ExternalOnly_ExactElasticTail(Double_t Es,Double_t Ep,Double_t theta, Double_t phi){

   // Equation A.16 of Mo and Tsai 1969.
   SetKinematics(Es,Ep,theta); 
   Double_t T       = fKinematics.GetT();
   Double_t M       = fKinematics.GetM(); 
   Double_t eta1    = 1.0/(1.0-Ep*(1.0-TMath::Cos(theta))/M);
   Double_t eta2    = 1.0+Es*(1.0-TMath::Cos(theta))/M;

   Double_t par[3]  = {theta,phi,0.0};
   fUnpolXSEl->SetBeamEnergy(Ep*eta1); 
   Double_t sig1    = fUnpolXSEl->EvaluateBaseXSec(fUnpolXSEl->GetDependentVariables(par)); 
   Double_t T1      = Ib_2(Es,Ep*eta1,T/2.0)*eta1*eta1*sig1;

   fUnpolXSEl->SetBeamEnergy(Es); 
   Double_t sig2    = fUnpolXSEl->EvaluateBaseXSec(fUnpolXSEl->GetDependentVariables(par)); 
   Double_t T2      = Ib_2(Es/eta2,Ep,T/2.0)*sig2;

   return(T1+T2);
}
//______________________________________________________________________________
//______________________________________________________________________________
Double_t RADCOR::EpExactIntegrand_Inelastic_Term_1(Double_t *x){

   /// For Monte Carlo 3D integration... 
   /// Integrate over Es',Ep', and t 

   // check the boundary first  
   if( Boundary_Inelastic_1(x) == 0 ) return 0.; 

   // grab the values
   // NOTE THE ORDER!  
   Double_t EpPrime = x[0]; 
   Double_t EsPrime = x[1]; 
   Double_t t       = x[2]; 
   // General terms 
   Double_t Es      = fKinematicsExt.GetEs();
   Double_t Ep      = fKinematicsExt.GetEp();
   Double_t theta   = fKinematicsExt.GetTheta();
   Double_t T       = fKinematicsExt.GetT();
   Double_t M       = fKinematicsExt.GetM(); 
   // for the Born xs  
   Double_t delta_r     = Delta_r(fDelta); 
   Double_t delta_inf_1 = Delta_inf_1(fDelta);
   Double_t delta_inf_2 = Delta_inf_2();
   Double_t delta_inf   = delta_inf_1;
   Double_t BornXS  = 0;
   Double_t par[3]  = {EpPrime,theta,0.};


   fUnpolXS->SetBeamEnergy(EsPrime); 
   fPolDiffXS->SetBeamEnergy(EsPrime); 

   Double_t T1=0,T2=0,T3=0;
   // First term 
   if(Es>EsPrime){
      T1  = Ib_2(Es,EsPrime,t);   
   }
   // Second term
   // Exact internally radiated XS tail: Eq. A24 in Stein, B5 in Mo & Tsai 
   // NOTE: We have to set the kinematics for the exact internal data members! (Es',Ep',theta)  
   //       We are integrating over cos(thk)... 
   SetKinematicsForExactInternal(EsPrime,EpPrime,theta);
   // Double_t M     = fKinematicsExt.GetM();
   // Double_t M_A   = fKinematicsExt.GetM_A();
   if(EsPrime>EpPrime){
      if(fPol==0){
         // unpolarized 
         BornXS = fUnpolXS->EvaluateBaseXSec(par);            // units: nb/GeV/sr  
      }else if(fPol>0){
         // polarized (para or perp) 
         BornXS = fPolDiffXS->EvaluateBaseXSec(par);          // units: nb/GeV/sr
      }else{
         std::cout << "[RADCOR::EpExactIntegrand_Inelastic_Term_1]: Error! Invalid cross section!  Exiting..." << std::endl;
         exit(1); 
      }
      // BornXS *= 1./fhbar_c_sq;          // make this dimensionless so that it's compatible with everything else! [probably not right...]  
      if(fIsMultiPhoton)  T2 = TMath::Exp(delta_inf)*(1.+delta_r-delta_inf)*BornXS;
      if(!fIsMultiPhoton) T2 = (1.+delta_r)*BornXS;
   }
   // Third term
   if(EpPrime>Ep){
      T3  = Ib_2(EpPrime,Ep,T-t);
   }

   Int_t NotANumber_1 = TMath::IsNaN(T1); 
   Int_t NotANumber_2 = TMath::IsNaN(T2); 
   Int_t NotANumber_3 = TMath::IsNaN(T3); 

   if(NotANumber_1) T1 = 0.;
   if(NotANumber_2) T2 = 0.;
   if(NotANumber_3) T3 = 0.;

   rEs_1_ext      = Es; 
   rEp_1_ext      = Ep;
   rEsPrime_1_ext = EsPrime; 
   rEpPrime_1_ext = EpPrime;
   rtPrime_1_ext  = t; 
   rT1_1_ext      = T1; 
   rT2_1_ext      = T2; 
   rT3_1_ext      = T3; 
   //fDebugTree->Fill(); 

   Double_t arg = T1*T2*T3;

   if(fDebug>3){
      std::cout << "--------- Ep integrand (3D) [term 1] ---------" << std::endl;
      std::cout << "t       = " << t        << std::endl;
      std::cout << "T       = " << T        << std::endl;
      std::cout << "Es      = " << Es       << std::endl;
      std::cout << "EsPrime = " << EsPrime  << std::endl;
      std::cout << "Ep      = " << Ep       << std::endl;
      std::cout << "EpPrime = " << EpPrime  << std::endl;
      std::cout << "T1      = " << T1       << std::endl;
      std::cout << "T2      = " << T2       << std::endl;
      std::cout << "T3      = " << T3       << std::endl;
      std::cout << "arg     = " << arg      << std::endl;
   }

   return arg;

}
//______________________________________________________________________________
Double_t RADCOR::EpExactIntegrand_Inelastic_Term_2(Double_t *x){

   /// For Monte Carlo 5D integration... 
   /// Integrate over Es',Ep', t, real photon angle cos(thk) and energy omega  
   /// This term contains the component for omega > fDelta  

   // check the boundary first  
   if( Boundary_Inelastic_2(x) == 0 ) return 0.; 

   // grab the values
   // NOTE THE ORDER!  
   Double_t EpPrime = x[0]; 
   Double_t EsPrime = x[1]; 
   Double_t t       = x[2];
   Double_t COSTHK=0,omega=0; 
   if(fIsInternal){
      COSTHK  = x[3]; 
      omega   = x[4];
   } 
   Double_t y[2]    = {COSTHK,omega}; 

   // General terms 
   Double_t Es      = fKinematicsExt.GetEs();
   Double_t Ep      = fKinematicsExt.GetEp();
   Double_t th      = fKinematicsExt.GetTheta();
   Double_t T       = fKinematicsExt.GetT();

   Double_t T1=0,T2=0,T3=0;
   // First term 
   if(Es>EsPrime){
      T1  = Ib_2(Es,EsPrime,t);   
   }
   // Second term
   // Exact internally radiated XS tail: Eq. A24 in Stein, B5 in Mo & Tsai 
   // NOTE: We have to set the kinematics for the exact internal data members! (Es',Ep',theta)  
   //       We are integrating over cos(thk)... 
   SetKinematicsForExactInternal(EsPrime,EpPrime,th);
   Double_t M       = fKinematicsExt.GetM();
   // Double_t M_A     = fKinematicsExt.GetM_A();
   Double_t alpha   = fine_structure_const;
   Double_t N       = 0.;
   Double_t sig_int = 0.;

   Double_t BornXS=0; 
   Double_t par[3] = {EpPrime,th,0.};
   fUnpolXS->SetBeamEnergy(EsPrime);
   fPolDiffXS->SetBeamEnergy(EsPrime);
   if(EsPrime>EpPrime){
      if(fIsInternal){
         N       = (alpha*alpha*alpha/(4.*pi2))*(EpPrime/EsPrime)*(1./M);    // for Mo & Tsai's calculation 
         sig_int = InternalIntegrand_Inelastic_MoTsai69(y);  
         T2      = N*sig_int*fhbar_c_sq;                                  // give this the right units... 
      }else{
         if(fPol==0){
            // unpolarized 
            BornXS = fUnpolXS->EvaluateBaseXSec(par);            // units: nb/GeV/sr  
         }else if(fPol>0){
            // polarized (para or perp) 
            BornXS = fPolDiffXS->EvaluateBaseXSec(par);          // units: nb/GeV/sr
         }else{
            std::cout << "[RADCOR::EpExactIntegrand_Inelastic_Term_1]: Error! Invalid cross section!  Exiting..." << std::endl;
            exit(1); 
         }
         T2      = BornXS; // FIXME: is this right? or is this term ignored all together?  
      }
   }
   // Third term
   if(EpPrime>Ep){
      T3  = Ib_2(EpPrime,Ep,T-t);
   }

   Int_t NotANumber_1 = TMath::IsNaN(T1); 
   Int_t NotANumber_2 = TMath::IsNaN(T2); 
   Int_t NotANumber_3 = TMath::IsNaN(T3); 

   if(NotANumber_1) T1 = 0.;
   if(NotANumber_2) T2 = 0.;
   if(NotANumber_3) T3 = 0.;

   rEs_2_ext      = 0.5; // Es; 
   rEp_2_ext      = 0.5; // Ep;
   rEsPrime_2_ext = 0.5; // EsPrime; 
   rEpPrime_2_ext = 0.5; // EpPrime;
   rtPrime_2_ext  = 0.5; // t; 
   rT1_2_ext      = 0.5; // T1; 
   rT2_2_ext      = 0.5; // T2; 
   rT3_2_ext      = 0.5; // T3; 
   fDebugTree->Fill(); 

   Double_t arg = T1*T2*T3;

   if(fDebug>3){
      std::cout << "--------- Ep integrand (5D) [term 2] ---------" << std::endl;
      std::cout << "t       = " << t        << std::endl;
      std::cout << "T       = " << T        << std::endl;
      std::cout << "Es      = " << Es       << std::endl;
      std::cout << "EsPrime = " << EsPrime  << std::endl;
      std::cout << "Ep      = " << Ep       << std::endl;
      std::cout << "EpPrime = " << EpPrime  << std::endl;
      std::cout << "cos(thk)= " << COSTHK   << std::endl;
      std::cout << "omega   = " << omega    << std::endl;
      std::cout << "T1      = " << T1       << std::endl;
      std::cout << "T2      = " << T2       << std::endl;
      std::cout << "T3      = " << T3       << std::endl;
      std::cout << "arg     = " << arg      << std::endl;
   }

   return arg;

}
//______________________________________________________________________________
Double_t RADCOR::ElasticTail(Double_t Es,Double_t Ep,Double_t theta){
   /// Approximate calculation from Mo & Tsai

   SetKinematics(Es,Ep,theta); 

   Double_t mp      = F_soft(Es,Ep,theta);
   Double_t sig_int = sigma_p(); 
   Double_t sig_ext = sigma_b();
   Double_t el_tail = (sig_int + sig_ext);  

   // std::cout << "mp        = " << mp      << std::endl;
   // std::cout << "sig (int) = " << sig_int << std::endl;
   // std::cout << "sig (ext) = " << sig_ext << std::endl;

   if(fIsMultiPhoton) el_tail *= mp; 

   return el_tail; 

}
//______________________________________________________________________________
Double_t RADCOR::GetInternalRadiatedXS(Double_t Es,Double_t Ep,Double_t theta){

   /// Exact calculation
   /// Elastic radiative tail 

   /// NOTE: We set ALL kinematics here -- only for this integrand! 
   ///       This defines 4-vectors s, p, t and u and their products. 
   SetKinematicsForExactInternal(Es,Ep,theta); 

   Double_t M_A      = fKinematicsInt.GetM_A();
   Double_t alpha    = fine_structure_const; 
   // Double_t par[3]   = {Ep,theta,0.};
   // fUnpolXS->SetBeamEnergy(Es); 
   // fPolDiffXS->SetBeamEnergy(Es); 
   // Double_t mp       = F_soft(Es,Ep,theta); 
   // Double_t N        = (alpha*alpha*alpha/(2.*pi))*(Ep/Es)*M_A;        // Stein et al 1975
   Double_t N        = (alpha*alpha*alpha/(4.*pi2))*(Ep/Es)*(1./M_A);   // Mo & Tsai 1969  
   // Double_t min      = -1.;
   // Double_t max      =  1.;
   // dividing up the integration into 3 regions 
   // Double_t min_1    = -1.; 
   // Double_t max_1    =  0.5; 
   // Double_t min_2    =  0.5; 
   // Double_t max_2    =  0.9; 
   // Double_t min_3    =  0.9; 
   // Double_t max_3    =  1.0; 

   IntRadFuncWrap funcWrap;
   funcWrap.fRADCOR = this;

   ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVE; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   double absErr                           = 1e100;  /// desired absolute Error 
   double relErr                           = 1e-3;  /// desired relative Error 
   unsigned int ncalls                     = 1.0e6;  /// number of calls (MC only)

   ROOT::Math::IntegrationOneDim::Type Legendre         = ROOT::Math::IntegrationOneDim::kLEGENDRE; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   ROOT::Math::IntegrationOneDim::Type Adaptive         = ROOT::Math::IntegrationOneDim::kADAPTIVE; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   ROOT::Math::IntegrationOneDim::Type AdaptiveSingular = ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR

   ROOT::Math::IntegratorOneDim  ig2(type,absErr,relErr,ncalls);
   ig2.SetFunction( funcWrap );

   ROOT::Math::IntegratorOneDim  ig2_leg(Legendre,absErr,relErr,ncalls);
   ig2_leg.SetFunction( funcWrap );

   ROOT::Math::IntegratorOneDim  ig2_adapt(Adaptive,absErr,relErr,ncalls);
   ig2_adapt.SetFunction( funcWrap );

   ROOT::Math::IntegratorOneDim  ig2_adapt_singular(AdaptiveSingular,absErr,relErr,ncalls);
   ig2_adapt_singular.SetFunction( funcWrap );

   // Double_t integral_1 = Double_t( ig2_leg.Integral(min_1,max_1) );
   // // std::cout << "------------------------------------------------------------" << std::endl; 
   // // std::cout << "Integration from " << min_1 << " to " << max_1 << ": " << Form("%.4E",integral_1) << std::endl;
   // Double_t integral_2 = Double_t( ig2_adapt.Integral(min_2,max_2) ); 
   // // std::cout << "Integration from " << min_2 << " to " << max_2 << ": " << Form("%.4E",integral_2) << std::endl;
   // Double_t integral_3 = Double_t( ig2_adapt_singular.Integral(min_3,max_3) ); 
   // // std::cout << "Integration from " << min_3 << " to " << max_3 << ": " << Form("%.4E",integral_3) << std::endl;
   // Double_t integral = integral_1 + integral_2 + integral_3; 
   // Double_t integral = Double_t( ig2_adapt_singular.Integral(min,max) );
   // Double_t integral = AdaptiveSimpson(&RADCOR::InternalIntegrand,min,max,fEpsilon,fDepth);
   const Int_t NDim = 1; 
   Int_t MCEvents = fNumMCEvents;
   Double_t integral=0,err=0;  
   Double_t Min[NDim] = {-1.}; 
   Double_t Max[NDim] = { 1.}; 
   MCSampleMean(NDim,&RADCOR::InternalIntegrand_MoTsai69,&Min[0],&Max[0],MCEvents,integral,err);

   Double_t sig_r    = N*integral;
   Double_t sig_tot  = sig_r;  

   if(fDebug==3){
      std::cout << "------- GetInternalRadiatedXS --------" << std::endl;
      std::cout << "N   = " << N        << std::endl;
      std::cout << "int = " << integral << std::endl;
      std::cout << "--------------------------------------" << std::endl;
   }else if(fDebug>3){
      std::cout << "------- GetInternalRadiatedXS --------" << std::endl;
      std::cout << "Es       = " << Es           << std::endl;
      std::cout << "Ep       = " << Ep           << std::endl;
      std::cout << "th       = " << theta/degree << std::endl;
      std::cout << "sig_r    = " << sig_r        << std::endl;
      std::cout << "sig_tot  = " << sig_tot      << std::endl;
      std::cout << "--------------------------------------" << std::endl;
   }

   return sig_tot; 

}
//______________________________________________________________________________
Double_t RADCOR::InternalIntegrand(Double_t &COSTHK){

   /// Stein et al, Phys Rev D 12, 1884 (1975) (Eq A24)  
   Double_t Es      = fKinematicsInt.GetEs(); 
   Double_t Ep      = fKinematicsInt.GetEp(); 
   Double_t theta   = fKinematicsInt.GetTheta(); 
   Double_t m       = fKinematicsInt.Getm();
   Double_t M_A     = fKinematicsInt.GetM_A();
   Double_t uVect   = fKinematicsInt.GetuVect();
   Double_t sVect   = fKinematicsInt.GetsVect();
   Double_t pVect   = fKinematicsInt.GetpVect();
   Double_t u_0     = fKinematicsInt.Getu_0(); 
   // Double_t W2      = fKinematicsInt.GetW2();    // M_f = W, see eq A1 in Tsai, SLAC-PUB-848 -- this seems to give omega = 0 though!  
   TLorentzVector s = fKinematicsInt.Gets(); 
   TLorentzVector p = fKinematicsInt.Getp(); 
   TLorentzVector t = fKinematicsInt.Gett(); 
   TLorentzVector u = fKinematicsInt.Getu(); 

   Double_t s_dot_p = s*p;  
   Double_t u_sq    = u*u; 
   Double_t omega   = 0.5*(u_sq - M_A*M_A)/(u_0 - uVect*COSTHK);
   // Double_t omega   = 0.5*(u_sq - W2)/(u_0 - uVect*COSTHK);  // this gives zero!  
   Double_t q2      = 2.*m*m - 2.*(s_dot_p) - 2.*omega*(Es-Ep) + 2.*omega*uVect*COSTHK;  
   Double_t COSTH   = TMath::Cos(theta); 
   // Double_t SINTH   = TMath::Sin(theta); 
   Double_t COSTHP  = (sVect*COSTH - pVect)/uVect; 
   Double_t COSTHS  = ( sVect - pVect*COSTH )/uVect;
   // Double_t SINTHS  = TMath::Sqrt(1.-COSTHS*COSTHS); 
   Double_t SINTHP  = TMath::Sqrt(1.-COSTHP*COSTHP);  
   Double_t SINTHK  = TMath::Sqrt(1.-COSTHK*COSTHK);  
   Double_t a       = omega*(Ep - pVect*COSTHP*COSTHK);  
   Double_t apr     = omega*(Es - sVect*COSTHS*COSTHK);  
   Double_t bpr     = (-1.)*omega*pVect*SINTHP*SINTHK; 
   Double_t v       = TMath::Power(apr-a,-1.);
   Double_t x       = TMath::Sqrt(a*a-bpr*bpr);         /// WARNING: NOT the scaling variable! 
   Double_t y       = TMath::Sqrt(apr*apr-bpr*bpr);     /// WARNING: NOT the scaling variable!

   /// Calculate FTilde, GE and GM  
   Nucleus::NucleusType Target = fTargetNucleus.GetType();
   Double_t FTilde = 0.; 
   switch(Target){
      case Nucleus::kProton:
         FTilde = 1.;
         break;
      case Nucleus::kNeutron:
         FTilde = 1.;
         break;
      default:
         FTilde = GetFTildeExactInternal(-q2);
         break;
   }

   Double_t ff[2]; 
   ProcessFormFactors(-q2,ff);                                      
   Double_t tau     = -q2/(4.*M_A*M_A);                              
   Double_t GE      = ff[0]; 
   Double_t GM      = ff[1]; 
   /// Compute W1Tilde and W2Tilde (see A6 of Stein et al) 
   Double_t W1Tilde = FTilde*(tau*GM*GM);                       // G in Mo & Tsai (see Stein for Tilde)       
   Double_t W2Tilde = FTilde*( (GE*GE + tau*GM*GM)/(1.+tau) );  // F in Mo & Tsai (see Stein for Tilde) 

   if(fDebug > 4){ 
      std::cout << "m       = " << m       << std::endl;
      std::cout << "M_A     = " << M_A     << std::endl;
      std::cout << "s_dot_p = " << s_dot_p << std::endl;
      std::cout << "u_0     = " << u_0     << std::endl; 
      std::cout << "uVect   = " << uVect   << std::endl; 
      std::cout << "sVect   = " << sVect   << std::endl; 
      std::cout << "pVect   = " << pVect   << std::endl; 
      std::cout << "omega   = " << omega   << std::endl; 
      std::cout << "-q2     = " << -q2     << std::endl;
      std::cout << "COSTHK  = " << COSTHK  << std::endl;
      std::cout << "SINTHK  = " << SINTHK  << std::endl;
      std::cout << "COSTHP  = " << COSTHP  << std::endl;
      std::cout << "COSTHS  = " << COSTHS  << std::endl;
      std::cout << "a       = " << a       << std::endl;
      std::cout << "apr     = " << apr     << std::endl;
      std::cout << "bpr     = " << bpr     << std::endl;
      std::cout << "v       = " << v       << std::endl;
      std::cout << "x       = " << x       << std::endl;
      std::cout << "y       = " << y       << std::endl;
      std::cout << "-q2     = " << -q2     << std::endl;
      std::cout << "GE      = " << GE      << std::endl;
      std::cout << "GM      = " << GM      << std::endl;
      std::cout << "FTilde  = " << FTilde  << std::endl;
      std::cout << "W1Tilde = " << W1Tilde << std::endl;
      std::cout << "W2Tilde = " << W2Tilde << std::endl;      
   }

   Double_t T[7]; 

   T[0] = 2.*omega/( q2*q2*(u_0 - uVect*COSTHK) );
   T[1] = -( a*m*m/TMath::Power(x,3.) )*(2.*Es*(Ep + omega) + q2/2. ) 
      - (apr*m*m/TMath::Power(y,3.) )*(2.*Ep*(Es - omega) + q2/2. ); 
   T[2] = -2. + 2.*v*( (1./x) - (1./y) )*(m*m*(s_dot_p - omega*omega) 
         + s_dot_p*( 2.*Es*Ep - s_dot_p + omega*(Es-Ep) ) ); 
   T[3] =  (1./x)*( 2.*(Es*Ep + Es*omega + Ep*Ep) + q2/2. - s_dot_p - m*m);
   T[4] = -(1./y)*( 2.*(Es*Ep - Ep*omega + Es*Es) + q2/2. - s_dot_p - m*m);
   T[5] = ( ( a/TMath::Power(x,3.) ) + (apr/TMath::Power(y,3.) ) )*m*m*(2.*m*m + q2)
      + 4. + 4.*v*( (1./x) - (1./y) )*s_dot_p*(s_dot_p - 2.*m*m); 
   T[6] = ( (1./x) - (1./y) )*(2.*s_dot_p + 2.*m*m - q2);    

   Double_t arg = T[0]*( W2Tilde*(T[1] + T[2] + T[3] + T[4]) + W1Tilde*(T[5] + T[6]) );
   return arg;

}
//______________________________________________________________________________
Double_t RADCOR::InternalIntegrand_MoTsai69(Double_t COSTHK){

   /// Mo & Tsai, Rev Mod Phys 41, 205 (1969)  (Eq B5) 
   Double_t Es      = fKinematicsInt.GetEs(); 
   Double_t Ep      = fKinematicsInt.GetEp(); 
   Double_t theta   = fKinematicsInt.GetTheta(); 
   Double_t m       = fKinematicsInt.Getm();
   Double_t M_A     = fKinematicsInt.GetM_A();
   Double_t uVect   = fKinematicsInt.GetuVect();
   Double_t sVect   = fKinematicsInt.GetsVect();
   Double_t pVect   = fKinematicsInt.GetpVect();
   Double_t u_0     = fKinematicsInt.Getu_0(); 
   // Double_t Wsq     = fKinematicsInt.GetW2();    // M_f = W, see eq A1 in Tsai, SLAC-PUB-848 -- this seems to give omega = 0 though!  
   TLorentzVector s = fKinematicsInt.Gets(); 
   TLorentzVector p = fKinematicsInt.Getp(); 
   TLorentzVector t = fKinematicsInt.Gett(); 
   TLorentzVector u = fKinematicsInt.Getu(); 

   Double_t s_dot_p = s*p;  
   Double_t u_sq    = u*u; 
   Double_t omega   = 0.5*(u_sq - M_A*M_A)/(u_0 - uVect*COSTHK);
   Double_t q2      = 2.*m*m - 2.*(s_dot_p) - 2.*omega*(Es-Ep) + 2.*omega*uVect*COSTHK;  
   Double_t SINTH   = TMath::Sin(theta); 
   Double_t COSTH   = TMath::Cos(theta); 
   Double_t COSTHP  = (sVect*COSTH - pVect)/uVect; 
   Double_t COSTHS  = (sVect - pVect*COSTH)/uVect;
   Double_t SINTHS  = TMath::Sqrt(1.-COSTHS*COSTHS); 
   Double_t SINTHP  = TMath::Sqrt(1.-COSTHP*COSTHP);  
   Double_t SINTHK  = TMath::Sqrt(1.-COSTHK*COSTHK);  
   Double_t a       = omega*(Ep - pVect*COSTHP*COSTHK);  
   Double_t apr     = omega*(Es - sVect*COSTHS*COSTHK);  
   Double_t b       = (-1.)*omega*pVect*SINTHP*SINTHK;  
   Double_t bpr     = (-1.)*omega*sVect*SINTHS*SINTHK; 
   Double_t vden    = omega*(Ep*sVect*SINTHS - Es*pVect*SINTHP + sVect*pVect*SINTH*COSTHK);
   Double_t v       = (-1.)*pVect*SINTHP/vden; 
   Double_t vpr     = (-1.)*sVect*SINTHS/vden; 
   Double_t x       = TMath::Sqrt(a*a-b*b);             /// WARNING: NOT the scaling variable! 
   Double_t y       = TMath::Sqrt(apr*apr-bpr*bpr);     /// WARNING: NOT the scaling variable!

   /// Calculate FTilde, GE and GM  
   Nucleus::NucleusType Target = fTargetNucleus.GetType();
   Double_t FTildeTerm = 0.; 
   switch(Target){
      case Nucleus::kProton:
         FTildeTerm = 1.;
         break;
      case Nucleus::kNeutron:
         FTildeTerm = 1.;
         break;
      default:
         FTildeTerm = GetFTildeExactInternal(-q2);
         break;
   }

   Double_t ff[2]; 
   ProcessFormFactors(-q2,ff);                                      
   Double_t tau     = -q2/(4.*M_A*M_A);                              
   Double_t GE      = ff[0]; 
   Double_t GM      = ff[1]; 
   /// Compute W1Tilde and W2Tilde (see A6 of Stein et al) 
   Double_t W1      = tau*GM*GM; 
   Double_t W2      = (GE*GE + tau*GM*GM)/(1.+tau); 
   // Double_t W1Tilde = FTildeTerm*W1;                       // G in Mo & Tsai (see Stein for Tilde)       
   // Double_t W2Tilde = FTildeTerm*W2;                       // F in Mo & Tsai (see Stein for Tilde) 
   Double_t G       = 4.*M_A*M_A*W1; 
   Double_t F       = 4.*W2; 
   Double_t GTilde  = FTildeTerm*G; 
   Double_t FTilde  = FTildeTerm*F; 

   // rEs_int     = Es; 
   // rEp_int     = Ep;
   // rQ2_int     = -q2; 
   // rCOSTHK_int = COSTHK;
   // rOmega_int  = omega;  
   // fDebugTree->Fill(); 

   if(fDebug > 4){ 
      std::cout << "----------------- InternalIntegrand_MoTsai69 -----------------" << std::endl;
      std::cout << "m          = " << m          << std::endl;
      std::cout << "M_A        = " << M_A        << std::endl;
      std::cout << "Es         = " << Es         << std::endl; 
      std::cout << "Ep         = " << Ep         << std::endl; 
      std::cout << "-q2        = " << -q2        << std::endl;
      std::cout << "tau        = " << tau        << std::endl;
      std::cout << "s_dot_p    = " << s_dot_p    << std::endl;
      std::cout << "u_0        = " << u_0        << std::endl; 
      std::cout << "uVect      = " << uVect      << std::endl; 
      std::cout << "sVect      = " << sVect      << std::endl; 
      std::cout << "pVect      = " << pVect      << std::endl; 
      std::cout << "omega      = " << omega      << std::endl; 
      std::cout << "COSTHK     = " << COSTHK     << std::endl;
      std::cout << "SINTHK     = " << SINTHK     << std::endl;
      std::cout << "COSTHP     = " << COSTHP     << std::endl;
      std::cout << "COSTHS     = " << COSTHS     << std::endl;
      std::cout << "a          = " << a          << std::endl;
      std::cout << "apr        = " << apr        << std::endl;
      std::cout << "bpr        = " << bpr        << std::endl;
      std::cout << "v          = " << v          << std::endl;
      std::cout << "x          = " << x          << std::endl;
      std::cout << "y          = " << y          << std::endl;
      std::cout << "GE         = " << GE         << std::endl;
      std::cout << "GM         = " << GM         << std::endl;
      std::cout << "FTildeTerm = " << FTildeTerm << std::endl;
      std::cout << "G          = " << G          << std::endl;
      std::cout << "F          = " << F          << std::endl;      
      std::cout << "GTilde     = " << GTilde     << std::endl;
      std::cout << "FTilde     = " << FTilde     << std::endl;      
   }

   // F term 
   Double_t FT1   = ( (-2.*pi*a*m*m)/(x*x*x)   )*(2.*Es*(Ep+omega) + 0.5*q2); 
   Double_t FT2   = ( (-2.*pi*apr*m*m)/(y*y*y) )*(2.*Ep*(Es-omega) + 0.5*q2); 
   Double_t FT3   = -4.*pi; 
   Double_t FT4   = 4.*pi*( (v/x) - (vpr/y) )*(m*m*(s_dot_p - omega*omega) + s_dot_p*(2.*Es*Ep - s_dot_p + omega*(Es-Ep) ) );
   Double_t FT5   = (2.*pi/x)*(2.*(Es*Ep + Es*omega + Ep*Ep) + 0.5*q2 - s_dot_p - m*m); 
   Double_t FT6   = (-1.)*(2.*pi/y)*(2.*(Es*Ep - Ep*omega + Es*Es) + 0.5*q2 - s_dot_p - m*m); 
   Double_t FTerm = M_A*M_A*FTilde*(FT1+FT2+FT3+FT4+FT5+FT6);   
   // G term 
   Double_t GT1   = ( (2.*pi*a)/(x*x*x) + (2.*pi*apr)/(y*y*y) )*(m*m)*(2.*m*m + q2); 
   Double_t GT2   = 8.*pi; 
   Double_t GT3   = 8.*pi*( (v/x) - (vpr/y) )*s_dot_p*(s_dot_p - 2.*m*m); 
   Double_t GT4   = 2.*pi*( (1./x) - (1./y) )*(2.*s_dot_p + 2.*m*m - q2); 
   Double_t GTerm = GTilde*(GT1+GT2+GT3+GT4);  
   // Put it together 
   Double_t T     = omega/( 2.*q2*q2*(u_0 - uVect*COSTHK) );
   Double_t arg   = T*(FTerm + GTerm);
   return arg;

}
//______________________________________________________________________________
Double_t RADCOR::GetInternalRadiatedXS_Inelastic(Double_t Es,Double_t Ep,Double_t theta){

   // Exact calculation 

   // NOTE: We set ALL kinematics here -- only for this integrand! 
   //       This defines 4-vectors s, p, t and u and their products. 
   SetKinematicsForExactInternal(Es,Ep,theta); 

   Double_t M           = fKinematicsInt.GetM();
   Double_t delta_r     = Delta_r(fDelta); 
   Double_t delta_inf_1 = Delta_inf_1(fDelta);
   Double_t delta_inf_2 = Delta_inf_2();
   Double_t delta_inf   = delta_inf_1; 
   Double_t alpha       = fine_structure_const; 

   std::cout << std::endl;
   std::cout << "delta_inf_1 = " << delta_inf_1 << "\t" << "delta_inf_2 = " << delta_inf_2 << std::endl;
   std::cout << std::endl;

   Double_t par[3] = {Ep,theta,0.};
   fUnpolXS->SetBeamEnergy(Es); 
   fPolDiffXS->SetBeamEnergy(Es); 
   Double_t BornXS=0; 
   if(fPol==0){
      // unpolarized 
      BornXS = fUnpolXS->EvaluateBaseXSec(par); 
   }else if(fPol>0){
      // polarized (para or perp) 
      BornXS = fPolDiffXS->EvaluateBaseXSec(par); 
   }else{
      std::cout << "[RADCOR::GetInternalRadiatedXS]: Error! Invalid cross section!  Exiting..." << std::endl;
      exit(1); 
   }
   // Double_t N        = (alpha*alpha*alpha/(2.*pi))*(Ep/Es)*(1./M); 
   Double_t N        = (alpha*alpha*alpha/(4.*pi2))*(Ep/Es)*(1./M);    // for Mo & Tsai's calculation 
   // Double_t min      = -1.;
   // Double_t max      =  1.;
   // // dividing up the integration into 3 regions 
   // Double_t min_1    = -1.; 
   // Double_t max_1    = -0.5; 
   // Double_t min_2    = -0.5; 
   // Double_t max_2    =  0.5; 
   // Double_t min_3    =  0.5; 
   // Double_t max_3    =  1.0; 

   // IntRadCosThkFuncWrap funcWrap;
   // funcWrap.fRADCOR = this;

   // ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVESINGULAR; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   // double absErr                           = 1e-02;  /// desired absolute Error 
   // double relErr                           = 1e-02;  /// desired relative Error 
   // unsigned int ncalls                     = 1.0e5;  /// number of calls (MC only)

   // ROOT::Math::IntegratorOneDim  ig2(type,absErr,relErr,ncalls);
   // ig2.SetFunction( funcWrap );

   // Double_t integral   = Double_t( ig2.Integral(min,max) );
   // Double_t integral_1 = Double_t( ig2.Integral(min_1,max_1) ); 
   // std::cout << "Integration from " << min_1 << " to " << max_1 << ": " << integral_1 << std::endl;
   // Double_t integral_2 = Double_t( ig2.Integral(min_2,max_2) ); 
   // std::cout << "Integration from " << min_2 << " to " << max_2 << ": " << integral_2 << std::endl;
   // Double_t integral_3 = Double_t( ig2.Integral(min_3,max_3) ); 
   // std::cout << "Integration from " << min_3 << " to " << max_3 << ": " << integral_3 << std::endl;
   // Double_t integral = integral_1 + integral_2 + integral_3; 
   // Double_t integral = AdaptiveSimpson(&RADCOR::InternalIntegrand,min,max,fEpsilon,fDepth);

   const Int_t NDim   =  2; 
   Int_t MCEvents     =  fNumMCEvents;
   Double_t integral  =  0.;
   Double_t err       =  0.;  
   Double_t costhkMin = -1.;
   Double_t costhkMax =  1.;
   Double_t omegaMin  = fDelta;                        // cutoff to avoid IR divergence! 
   Double_t omegaMax  = GetOmegaMax(costhkMax);        // we use cos(thk) = 1 since that will maximize omegaMax...  
   Double_t Min[NDim] = {costhkMin,omegaMin}; 
   Double_t Max[NDim] = {costhkMax,omegaMax};
   // std::cout << "LIMITS" << std::endl; 
   // std::cout << "cos(thk) min = " << costhkMin << "\t" << "cos(thk) max = " << costhkMax << std::endl;
   // std::cout << "omega min    = " << omegaMin  << "\t" << "omega max    = " << omegaMax  << std::endl;
   MCSampleMean(NDim,&RADCOR::InternalIntegrand_Inelastic_MoTsai69,Min,Max,MCEvents,integral,err);

   Double_t sig_r   = N*integral*fhbar_c_sq;       // we need to scale this piece to the correct units!  
   Double_t sig_tot=0;
   if(fIsMultiPhoton){ 
      // We mimic POLRAD here.  It seems that delta_v = delta_r = delta_inf + delta_vac + delta_vertex. 
      // the exponential term is inferred from looking at POLRAD...
      sig_tot = TMath::Exp(delta_inf)*(1.+delta_r-delta_inf)*BornXS + sig_r;   
   }else{
      sig_tot = BornXS + sig_r; 
   }

   if(fDebug>3){
      std::cout << "------- GetInternalRadiatedXS_Inelastic --------" << std::endl;
      std::cout << "Es       = " << Es           << std::endl;
      std::cout << "Ep       = " << Ep           << std::endl;
      std::cout << "th       = " << theta/degree << std::endl;
      std::cout << "sig_born = " << BornXS       << std::endl;
      std::cout << "delta_r  = " << delta_r      << std::endl;
      std::cout << "N        = " << N            << std::endl;
      std::cout << "integral = " << integral     << std::endl;
      std::cout << "sig_r    = " << sig_r        << std::endl;
      std::cout << "CORRECTION FACTOR = " << 1.+delta_r + sig_r/BornXS << std::endl; 
      std::cout << "sig_tot  = " << sig_tot      << std::endl;
      std::cout << "--------------------------------------" << std::endl;
   }

   return sig_tot; 

}
//______________________________________________________________________________
Double_t RADCOR::InternalIntegrand_costhk_Inelastic(Double_t &COSTHK){

   // Exact calculation 

   fCOSTHK = COSTHK;             // cos(th_k) is needed in the omega integrand 

   // limits for integral over omega (InternalIntegrand_omega_Inelastic)  
   Double_t min      = fDelta;                           /// See Mo & Tsai, Eq B8  
   Double_t max      = GetOmegaMax(COSTHK);        /// See Mo & Tsai, Eq B8 

   if(max<min){
      std::cout << "[RADCOR::InternalIntegrand_costhk_Inelastic]: Error! " << std::endl;
      std::cout << "omega integral limits: " << min << " to " << max << std::endl;
      std::cout << "min > max!" << std::endl;
      exit(1);
   }

   IntRadOmegaFuncWrap funcWrap;
   funcWrap.fRADCOR = this;

   ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kLEGENDRE; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   double absErr                           = 1e100;  /// desired absolute Error 
   double relErr                           = 1e-3;  /// desired relative Error 
   unsigned int ncalls                     = 1.0e5;  /// number of calls (MC only)

   ROOT::Math::IntegratorOneDim  ig2(type,absErr,relErr,ncalls);
   ig2.SetFunction( funcWrap );

   auto integral = Double_t( ig2.Integral(min,max) ); 
   // Double_t integral = AdaptiveSimpson(&RADCOR::InternalIntegrand_omega_Inelastic,min,max,fEpsilon,fDepth);

   return integral;

}
//______________________________________________________________________________
Double_t RADCOR::InternalIntegrand_omega_Inelastic(Double_t &omega){

   /// Stein et al, Phys Rev D 12, 1884 (1975) (Eq A24)  
   /// Mo & Tsai, Rev Mod Phys 41, 205 (1969)  (Eq B5)
   /// Integrating over the photon energy omega!  

   /// Grab all needed variables   
   Double_t m       = fKinematicsInt.Getm();
   Double_t M       = fKinematicsInt.GetM();
   // Double_t M_pion  = fKinematicsInt.GetM_pion();  
   Double_t M_A     = fKinematicsInt.GetM_A();
   Double_t Es      = fKinematicsInt.GetEs();
   Double_t Ep      = fKinematicsInt.GetEp();
   Double_t theta   = fKinematicsInt.GetTheta(); 
   Double_t uVect   = fKinematicsInt.GetuVect();
   Double_t sVect   = fKinematicsInt.GetsVect();
   Double_t pVect   = fKinematicsInt.GetpVect();
   Double_t u_0     = fKinematicsInt.Getu_0(); 
   TLorentzVector s = fKinematicsInt.Gets(); 
   TLorentzVector p = fKinematicsInt.Getp(); 
   TLorentzVector t = fKinematicsInt.Gett(); 
   TLorentzVector u = fKinematicsInt.Getu(); 

   // Double_t u_sq    = u*u; 
   Double_t COSTHK  = fCOSTHK; 
   Double_t s_dot_p = s*p;  
   Double_t u_sq    = 2.*m*m + M_A*M_A - 2.*s_dot_p + 2.*M_A*(Es-Ep); 
   Double_t q2      = 2.*m*m - 2.*(s_dot_p) - 2.*omega*(Es-Ep) + 2.*omega*uVect*COSTHK;  
   Double_t COSTHP  = (sVect*TMath::Cos(theta) - pVect)/uVect; 
   Double_t COSTHS  = ( sVect - pVect*TMath::Cos(theta) )/uVect;
   Double_t SINTHP  = TMath::Sqrt(1.-COSTHP*COSTHP);  
   Double_t SINTHK  = TMath::Sqrt(1.-COSTHK*COSTHK);  
   Double_t a       = omega*(Ep - pVect*COSTHP*COSTHK);  
   Double_t apr     = omega*(Es - sVect*COSTHS*COSTHK);  
   Double_t bpr     = (-1.)*omega*pVect*SINTHP*SINTHK; 
   Double_t v       = TMath::Power(apr-a,-1.);
   Double_t x       = TMath::Sqrt(a*a-bpr*bpr);         /// WARNING: NOT the scaling variable! 
   Double_t y       = TMath::Sqrt(apr*apr-bpr*bpr);     /// WARNING: NOT the scaling variable!

   /// Calculate FTilde, W1Tilde and W2Tilde 
   /// Here is where we differ from the elastic case 
   /// F_j --> int_0^w_max F(q2,Mf2) 
   /// Mo & Tsai, Eq B11 

   // Double_t W_qe   = M; 
   // Double_t W_pion = M + M_pion; 
   Double_t Mf2    = GetMfSq(COSTHK,omega); 
   // Double_t x_bj=0; 
   // if(fThreshold==1){ 
   // 	W2    = TMath::Power(M,2.); 
   // }else if(fThreshold==2){
   // 	W2    = TMath::Power(M+M_pion,2.); 
   // } 
   Double_t x_bj  = 1. - (Mf2 - M*M)/( 2.*M*(Es-Ep-omega) );

   Double_t sf[2] = {0.,0.};   
   Double_t F1=0,F2=0;

   // std::cout << "Evaluating structure functions..." << std::endl;
   // std::cout << "x = " << x_bj << "\t" << "Q2 = " << Q2 << std::endl;

   ProcessStructureFunctions(x_bj,-q2,sf); 

   F1 = sf[0]; 
   F2 = sf[1];

   F1 *= 2.*(u_0 - uVect*COSTHK); 
   F2 *= 2.*(u_0 - uVect*COSTHK); 

   // std::cout << "F1*2*(u0 - uVect*cos(thk)) = " << F1 << std::endl;
   // std::cout << "F2*2*(u0 - uVect*cos(thk)) = " << F2 << std::endl;

   Double_t W1      = F1/M; 
   Double_t W2      = F2/(Es-Ep-omega); 

   Double_t FTilde  = GetFTildeExactInternal(-q2);
   Double_t W1Tilde = FTilde*W1;                           // G in Mo & Tsai (see Stein for Tilde)       
   Double_t W2Tilde = FTilde*W2;                           // F in Mo & Tsai (see Stein for Tilde) 

   if(omega<0 || fDebug>4){
      std::cout << "[RADCOR::InternalIntegrand_omega_Inelastic]: There's a problem.  omega < 0." << std::endl;
      std::cout << "omega   = " << omega        << std::endl; 
      std::cout << "Es      = " << Es           << std::endl;
      std::cout << "Ep      = " << Ep           << std::endl;
      std::cout << "th      = " << theta/degree << std::endl;
      std::cout << "s_dot_p = " << s_dot_p      << std::endl;
      std::cout << "u_0     = " << u_0          << std::endl; 
      std::cout << "u_x     = " << u.X()        << std::endl; 
      std::cout << "u_y     = " << u.Y()        << std::endl; 
      std::cout << "u_z     = " << u.Z()        << std::endl; 
      std::cout << "u_t     = " << u.T()        << std::endl; 
      std::cout << "u_sq    = " << u_sq         << std::endl; 
      std::cout << "-q2     = " << -q2          << std::endl;
      std::cout << "COSTHK  = " << COSTHK       << std::endl;
      std::cout << "SINTHK  = " << SINTHK       << std::endl;
      std::cout << "COSTHP  = " << COSTHP       << std::endl;
      std::cout << "COSTHS  = " << COSTHS       << std::endl;
      std::cout << "a       = " << a            << std::endl;
      std::cout << "apr     = " << apr          << std::endl;
      std::cout << "bpr     = " << bpr          << std::endl;
      std::cout << "v       = " << v            << std::endl;
      std::cout << "x       = " << x            << std::endl;
      std::cout << "y       = " << y            << std::endl;
      std::cout << "Mf2     = " << Mf2          << std::endl;
      std::cout << "x_bj    = " << x_bj         << std::endl;
      std::cout << "W1      = " << W1           << std::endl;
      std::cout << "W2      = " << W2           << std::endl;
      std::cout << "FTilde  = " << FTilde       << std::endl;
      std::cout << "W1Tilde = " << W1Tilde      << std::endl;
      std::cout << "W2Tilde = " << W2Tilde      << std::endl;      
      std::cout << "---------------------"      << std::endl;
      exit(1);
   } 

   Double_t T[7]; 

   T[0] = 2.*omega/( q2*q2*(u_0 - uVect*COSTHK) );
   T[1] = -( a*m*m/TMath::Power(x,3.) )*(2.*Es*(Ep + omega) + q2/2. ) 
      - (apr*m*m/TMath::Power(y,3.) )*(2.*Ep*(Es - omega) + q2/2. ); 
   T[2] = -2. + 2.*v*( (1./x) - (1./y) )*(m*m*(s_dot_p - omega*omega) 
         + s_dot_p*( 2.*Es*Ep - s_dot_p + omega*(Es-Ep) ) ); 
   T[3] =  (1./x)*( 2.*(Es*Ep + Es*omega + Ep*Ep) + q2/2. - s_dot_p - m*m);
   T[4] = -(1./y)*( 2.*(Es*Ep - Ep*omega + Es*Es) + q2/2. - s_dot_p - m*m);
   T[5] = ( ( a/TMath::Power(x,3.) ) + (apr/TMath::Power(y,3.) ) )*m*m*(2.*m*m + q2)
      + 4. + 4.*v*( (1./x) - (1./y) )*s_dot_p*(s_dot_p - 2.*m*m); 
   T[6] = ( (1./x) - (1./y) )*(2.*s_dot_p + 2.*m*m - q2);    

   Double_t sig_prm = T[0]*( W2Tilde*(T[1] + T[2] + T[3] + T[4]) + W1Tilde*(T[5] + T[6]) );
   return sig_prm;

   // // now construct the cross section.  The above is only the actual integral.
   // // we want the integrand to actually give the full cross section, 
   // // so that we can just insert this right into the full calculation 
   // // which takes care of the external radiation    
   // fUnpolXS->SetBeamEnergy(Es); 
   // fPolDiffXS->SetBeamEnergy(Es); 
   // Double_t BornXS=0;
   // Double_t par[3] = {Ep,theta,0.}; 
   // if(fPol==0){
   //         // unpolarized 
   // 	BornXS = fUnpolXS->EvaluateBaseXSec(par); 
   // }else if(fPol>0){
   //         // polarized (para or perp) 
   // 	BornXS = fPolDiffXS->EvaluateBaseXSec(par); 
   // }else{
   // 	std::cout << "[RADCOR::GetInternalRadiatedXS]: Error! Invalid cross section!  Exiting..." << std::endl;
   // 	exit(1); 
   // }
   // BornXS *= 1./fhbar_c_sq;                    // make this dimensionless like the integrand above  
   // Double_t delta_r  = Delta_r(fDelta); 

   // Double_t sig_r=0; 
   // if(fIsMultiPhoton){
   // 	sig_r = BornXS*(1.+delta_r) + sig_prm;
   // }else{
   // 	sig_r = BornXS + sig_prm;
   // }

   // return sig_r;

}
//______________________________________________________________________________
Double_t RADCOR::InternalIntegrand_Inelastic_MoTsai69(Double_t *par){

   /// Inelastic internal integrand 
   /// 2D Integral over cos(thk) and omega 

   // check the boundary first 
   if( Boundary_Internal_Inelastic(par) == 0) return 0.; 

   Double_t COSTHK = par[0]; 
   Double_t omega  = par[1]; 

   /// Mo & Tsai, Rev Mod Phys 41, 205 (1969)  (Eq B5) 
   Double_t Es      = fKinematicsInt.GetEs(); 
   Double_t Ep      = fKinematicsInt.GetEp(); 
   Double_t theta   = fKinematicsInt.GetTheta(); 
   Double_t m       = fKinematicsInt.Getm();
   Double_t M       = fKinematicsInt.GetM();
   // Double_t M_pion  = fKinematicsInt.GetM_pion();
   Double_t uVect   = fKinematicsInt.GetuVect();
   Double_t sVect   = fKinematicsInt.GetsVect();
   Double_t pVect   = fKinematicsInt.GetpVect();
   Double_t u_0     = fKinematicsInt.Getu_0(); 
   TLorentzVector s = fKinematicsInt.Gets(); 
   TLorentzVector p = fKinematicsInt.Getp(); 
   TLorentzVector t = fKinematicsInt.Gett(); 
   TLorentzVector u = fKinematicsInt.Getu(); 


   Double_t s_dot_p = s*p;  
   // Double_t u_sq    = u*u; 
   Double_t q2      = 2.*m*m - 2.*(s_dot_p) - 2.*omega*(Es-Ep) + 2.*omega*uVect*COSTHK;  
   Double_t SINTH   = TMath::Sin(theta); 
   Double_t COSTH   = TMath::Cos(theta); 
   Double_t COSTHP  = (sVect*COSTH - pVect)/uVect; 
   Double_t COSTHS  = (sVect - pVect*COSTH)/uVect;
   Double_t SINTHS  = TMath::Sqrt(1.-COSTHS*COSTHS); 
   Double_t SINTHP  = TMath::Sqrt(1.-COSTHP*COSTHP);  
   Double_t SINTHK  = TMath::Sqrt(1.-COSTHK*COSTHK);  
   Double_t a       = omega*(Ep - pVect*COSTHP*COSTHK);  
   Double_t apr     = omega*(Es - sVect*COSTHS*COSTHK);  
   Double_t b       = (-1.)*omega*pVect*SINTHP*SINTHK;  
   Double_t bpr     = (-1.)*omega*sVect*SINTHS*SINTHK; 
   Double_t vden    = omega*(Ep*sVect*SINTHS - Es*pVect*SINTHP + sVect*pVect*SINTH*COSTHK);
   Double_t v       = (-1.)*pVect*SINTHP/vden; 
   Double_t vpr     = (-1.)*sVect*SINTHS/vden; 
   Double_t x       = TMath::Sqrt(a*a-b*b);             /// WARNING: NOT the scaling variable! 
   Double_t y       = TMath::Sqrt(apr*apr-bpr*bpr);     /// WARNING: NOT the scaling variable!

   /// Calculate FTildeTerm   
   Nucleus::NucleusType Target = fTargetNucleus.GetType();
   Double_t FTildeTerm = 0.; 
   switch(Target){
      case Nucleus::kProton:
         FTildeTerm = 1.;
         break;
      case Nucleus::kNeutron:
         FTildeTerm = 1.;
         break;
      default:
         FTildeTerm = GetFTildeExactInternal(-q2);  
         break;
   }

   /// Calculate FTilde and GTilde  
   /// Here is where we differ from the elastic case 
   /// F_j --> int_Delta0^w_max F(q2,Mf2) 
   /// Mo & Tsai, Eq B11 
   /// First, we convert (-q2,Mf2) to (x_bj,-q2)  
   Double_t nu    = Es-Ep-omega;  
   Double_t Mf2   = GetMfSq(COSTHK,omega);
   // Double_t x_bj  = 1. - (Mf2 - M*M)/( 2.*M*nu ); 
   Double_t x_bj  = -q2/(2.*M*nu); 
   Double_t sf[2] = {0.,0.};   

   ProcessStructureFunctions(x_bj,-q2,sf); 

   Double_t F1      = sf[0]; 
   Double_t F2      = sf[1];
   Double_t W1      = F1/M; 
   Double_t W2      = F2/nu; 
   Double_t G       = 4.*M*M*W1; 
   Double_t F       = 4.*W2;
   G               *= 2.*(u_0 - uVect*COSTHK);                 /// See B11 in Mo & Tsai  
   F               *= 2.*(u_0 - uVect*COSTHK);                 /// See B11 in Mo & Tsai  
   Double_t GTilde  = FTildeTerm*G; 
   Double_t FTilde  = FTildeTerm*F; 

   if(fDebug>4){
      std::cout << "-------------------- InternalIntegrand_Inelastic_MoTsai69 --------------------" << std::endl; 
      std::cout << "Es          = " << Es          << std::endl;
      std::cout << "Ep          = " << Ep          << std::endl;
      std::cout << "omega       = " << omega       << std::endl; 
      std::cout << "x_bj        = " << x_bj        << std::endl;
      std::cout << "-q2         = " << -q2         << std::endl;
      std::cout << "m           = " << m           << std::endl;
      std::cout << "M           = " << M           << std::endl;
      std::cout << "s_dot_p     = " << s_dot_p     << std::endl;
      std::cout << "u_0         = " << u_0         << std::endl; 
      std::cout << "uVect       = " << uVect       << std::endl; 
      std::cout << "sVect       = " << sVect       << std::endl; 
      std::cout << "pVect       = " << pVect       << std::endl; 
      std::cout << "COSTHK      = " << COSTHK      << std::endl;
      std::cout << "SINTHK      = " << SINTHK      << std::endl;
      std::cout << "COSTHP      = " << COSTHP      << std::endl;
      std::cout << "COSTHS      = " << COSTHS      << std::endl;
      std::cout << "a           = " << a           << std::endl;
      std::cout << "apr         = " << apr         << std::endl;
      std::cout << "bpr         = " << bpr         << std::endl;
      std::cout << "v           = " << v           << std::endl;
      std::cout << "x           = " << x           << std::endl;
      std::cout << "y           = " << y           << std::endl;
      std::cout << "FTildeTerm  = " << FTildeTerm  << std::endl;
      std::cout << "W1          = " << W1          << std::endl;
      std::cout << "W2          = " << W2          << std::endl;      
      std::cout << "G           = " << G           << std::endl;
      std::cout << "F           = " << F           << std::endl;      
      std::cout << "GTilde      = " << GTilde      << std::endl;
      std::cout << "FTilde      = " << FTilde      << std::endl;
   }

   // F term 
   Double_t FT1   = ( (-2.*pi*a*m*m)/(x*x*x)   )*(2.*Es*(Ep+omega) + 0.5*q2); 
   Double_t FT2   = ( (-2.*pi*apr*m*m)/(y*y*y) )*(2.*Ep*(Es-omega) + 0.5*q2); 
   Double_t FT3   = -4.*pi; 
   Double_t FT4   = 4.*pi*( (v/x) - (vpr/y) )*(m*m*(s_dot_p - omega*omega) + s_dot_p*(2.*Es*Ep - s_dot_p + omega*(Es-Ep) ) );
   Double_t FT5   = (2.*pi/x)*(2.*(Es*Ep + Es*omega + Ep*Ep) + 0.5*q2 - s_dot_p - m*m); 
   Double_t FT6   = (-1.)*(2.*pi/y)*(2.*(Es*Ep - Ep*omega + Es*Es) + 0.5*q2 - s_dot_p - m*m); 
   Double_t FTerm = M*M*FTilde*(FT1+FT2+FT3+FT4+FT5+FT6);   
   // G term 
   Double_t GT1   = ( (2.*pi*a)/(x*x*x) + (2.*pi*apr)/(y*y*y) )*(m*m)*(2.*m*m + q2); 
   Double_t GT2   = 8.*pi; 
   Double_t GT3   = 8.*pi*( (v/x) - (vpr/y) )*s_dot_p*(s_dot_p - 2.*m*m); 
   Double_t GT4   = 2.*pi*( (1./x) - (1./y) )*(2.*s_dot_p + 2.*m*m - q2); 
   Double_t GTerm = GTilde*(GT1+GT2+GT3+GT4);  
   // Put it together 
   Double_t T       = omega/( 2.*q2*q2*(u_0 - uVect*COSTHK) );
   Double_t sig_prm = T*(FTerm + GTerm);

   rEs_inel_int        = Es; 
   rEp_inel_int        = Ep;
   rQ2_inel_int        = -q2; 
   rCOSTHK_inel_int    = COSTHK;
   rOmega_inel_int     = omega; 
   rIntegrand_inel_int = sig_prm;  
   fDebugTree->Fill(); 

   return sig_prm;
}
//______________________________________________________________________________
Double_t RADCOR::GetOmegaMax(Double_t COSTHK){

   /// Equation for omega_max(th_k) 
   /// Mo & Tsai, Eq B10
   Double_t M       = fKinematicsInt.GetM(); 
   Double_t M_A     = fKinematicsInt.GetM_A(); 
   Double_t M_pion  = fKinematicsInt.GetM_pion();
   TLorentzVector u = fKinematicsInt.Getu(); 
   Double_t u_0     = fKinematicsInt.Getu_0(); 
   Double_t uVect   = fKinematicsInt.GetuVect();
   Double_t u_sq    = u*u; 
   Double_t W_qe    = M;  
   Double_t W_pi    = M + M_pion; 
   Double_t W_el    = M_A; 
   Double_t W_sq    = 0.;
   switch(fThreshold){
      case 0: // elastic
         W_sq = TMath::Power(W_el,2.);
         break;
      case 1: // quasi-elastic 
         W_sq = TMath::Power(W_qe,2.);
         break;
      case 2: // pion production  
         W_sq = TMath::Power(W_pi,2.);
         break;
      default: 
         std::cout << "[RADCOR::GetOmegaMax]: Invalid integration threshold!  Exiting..." << std::endl;
         exit(1);
   }
   Double_t omega_max = 0.5*(u_sq - W_sq)/(u_0 - uVect*COSTHK);

   // Double_t Es = fKinematicsInt.GetEs();
   // Double_t Ep = fKinematicsInt.GetEp();

   // std::cout << "------------ GetOmegaMax ------------" << std::endl;
   // std::cout << "Es        = " << Es         << std::endl;
   // std::cout << "Ep        = " << Ep         << std::endl;
   // std::cout << "u_0       = " << u_0        << std::endl;  
   // std::cout << "uVect     = " << uVect      << std::endl;  
   // std::cout << "u2        = " << u_sq       << std::endl; 
   // std::cout << "threshold = " << fThreshold << std::endl;
   // std::cout << "W_sq      = " << W_sq       << std::endl;
   // std::cout << "cos(thk)  = " << COSTHK     << std::endl; 
   // std::cout << "omega_max = " << omega_max  << std::endl; 

   return omega_max; 

}
//______________________________________________________________________________
Double_t RADCOR::GetMfSq(Double_t COSTHK,Double_t omega){

   /// Equation for Mf2 (W2) for the inelastic continuum 
   /// Mo & Tsai, Eq B9  
   TLorentzVector u = fKinematicsInt.Getu(); 
   Double_t u_0     = fKinematicsInt.Getu_0(); 
   Double_t uVect   = fKinematicsInt.GetuVect();
   Double_t u_sq    = u*u; 
   Double_t Mf2     = u_sq - 2.*omega*(u_0 - uVect*COSTHK); 
   return Mf2; 

}
//______________________________________________________________________________
Double_t RADCOR::sigma_p(){

   /// Elastic radiative tail [internal] 
   /// Angle peaking approximation 
   /// Phys Rev D 12 1884 (1975), Eq A56  
   Double_t Es      = fKinematics.GetEs(); 
   Double_t Ep      = fKinematics.GetEp(); 
   Double_t theta   = fKinematics.GetTheta();
   Double_t b       = fKinematics.Getb(); 
   Double_t M_A     = fKinematics.GetM_A(); 
   Double_t SIN     = TMath::Sin(theta/2.);
   Double_t SIN2    = SIN*SIN;
   Double_t Q2      = insane::Kine::Qsquared(Es,Ep,theta);
   Double_t Tr      = GetTr(Q2); 
   Double_t FTilde  = GetFTilde(Q2); 
   // omega_s 
   Double_t num_s   = Ep;
   Double_t den_s   = 1. - 2.*(Ep/M_A)*SIN2;
   Double_t omega_s = Es - (num_s/den_s);
   // omega_p 
   Double_t num_p   = Es;
   Double_t den_p   = 1. + 2.*(Es/M_A)*SIN2;
   Double_t omega_p = (num_p/den_p) - Ep;
   // v terms 
   Double_t v_s     = omega_s/Es; 
   Double_t v_p     = omega_p/(Ep + omega_p);
   Double_t phi_s   = GetPhi(v_s); 
   Double_t phi_p   = GetPhi(v_p);  
   // put it together 
   Double_t num     = M_A + 2.*(Es - omega_s)*SIN2;
   Double_t den     = M_A - 2.*Ep*SIN2; 
   Double_t T1      = num/den; 
   Double_t T2      = FTilde*sigma_el(Es-omega_s,theta); 
   Double_t T3      = b*Tr*phi_s/omega_s;
   Double_t T4      = FTilde*sigma_el(Es,theta); 
   Double_t T5      = b*Tr*phi_p/omega_p; 
   Double_t sig     = T1*T2*T3 + T4*T5;
   // std::cout << "=========== sigma_p ===========" << std::endl;
   // std::cout << "T1 = " << T1 << std::endl;
   // std::cout << "T2 = " << T2 << std::endl;
   // std::cout << "T3 = " << T3 << std::endl;
   // std::cout << "T5 = " << T5 << std::endl;
   return sig;
}
//______________________________________________________________________________
Double_t RADCOR::sigma_b(){

   /// Elastic radiative tail [external] 
   /// Angle peaking approximation 
   /// Phys Rev D 12 1884 (1975), Eq A49  

   Double_t Es      = fKinematics.GetEs(); 
   Double_t Ep      = fKinematics.GetEp(); 
   Double_t theta   = fKinematics.GetTheta();
   Double_t b       = fKinematics.Getb(); 
   Double_t Tb      = fKinematics.Gett_b(); 
   Double_t Ta      = fKinematics.Gett_a();
   Double_t M_A     = fKinematics.GetM_A(); 
   Double_t Xi      = fKinematics.Getxi();
   Double_t SIN     = TMath::Sin(theta/2.);
   Double_t SIN2    = SIN*SIN;
   Double_t Q2      = insane::Kine::Qsquared(Es,Ep,theta);
   Double_t mp      = F_soft(Es,Ep,theta); 
   if(!fIsExternal){
      Tb = 0.;
      Ta = 0.;
   } 
   // Double_t Tr      = GetTr(Q2); 
   // Double_t FTilde  = GetFTilde(Q2); 
   // omega_s = Es - EsMin (elastic)  
   Double_t num_s   = Ep;
   Double_t den_s   = 1. - 2.*(Ep/M_A)*SIN2;
   Double_t omega_s = Es - (num_s/den_s);
   // omega_p = EpMax (elastic) - Ep  
   Double_t num_p   = Es;
   Double_t den_p   = 1. + 2.*(Es/M_A)*SIN2;
   Double_t omega_p = (num_p/den_p) - Ep;
   // v terms 
   Double_t v_s     = omega_s/Es; 
   Double_t v_p     = omega_p/(Ep + omega_p);
   Double_t phi_s   = GetPhi(v_s); 
   Double_t phi_p   = GetPhi(v_p);  
   // put it together 
   Double_t num     = M_A + 2.*(Es - omega_s)*SIN2;
   Double_t den     = M_A - 2.*Ep*SIN2; 
   Double_t T1      = num/den; 
   Double_t T2      = sigma_el_tilde(Es-omega_s,theta); 
   Double_t T3      = b*Tb*phi_s/omega_s + Xi/(2.*omega_s*omega_s); 
   Double_t T4      = sigma_el_tilde(Es,theta); 
   Double_t T5      = b*Ta*phi_p/omega_p + Xi/(2.*omega_p*omega_p); 
   Double_t sig     = T1*T2*T3 + T4*T5;

   if(fIsMultiPhoton) sig *= mp; 

   Int_t IsNotANumber = TMath::IsNaN(sig);

   if(IsNotANumber) sig = 0.; 

   // std::cout << "=========== sigma_b ===========" << std::endl;
   // std::cout << "Es      = " << Es      << std::endl;
   // std::cout << "Ep      = " << Ep      << std::endl;
   // std::cout << "T1      = " << T1      << std::endl;
   // std::cout << "T2      = " << T2      << std::endl;
   // std::cout << "T3      = " << T3      << std::endl;
   // std::cout << "T4      = " << T4      << std::endl;
   // std::cout << "T5      = " << T5      << std::endl;
   // std::cout << "mp      = " << mp      << std::endl;
   // std::cout << "FTilde  = " << FTilde  << std::endl;
   // std::cout << "xi      = " << Xi      << std::endl;
   // std::cout << "omega_s = " << omega_s << std::endl;
   // std::cout << "omega_p = " << omega_p << std::endl;
   // std::cout << "bT      = " << b*(Tb + Ta) << std::endl;

   return sig;
}
// //____________________________________________________________________________________
// Double_t RADCOR::sigma_b_in(Double_t Es,Double_t Ep,Double_t t){
//  
//         /// Inelastic radiative tail [external] 
//         /// real bremsstrahlung and ionization loss in the target 
//         /// Phys Rev D 12 1884 (1975), Eq A49  
//         /// The difference here is that we use the inelastic 
//         /// cross section instead of elastic; so we 
//         /// pass the appropriate (Ep,th,ph) to the cross section 
//         /// object, based on before or after scattering conditions [is this correct?] 
// 
//         // Double_t Es      = fKinematics.GetEs(); 
//         // Double_t Ep      = fKinematics.GetEp(); 
//         Double_t theta   = fKinematics.GetTheta();
// 	Double_t b       = fKinematics.Getb(); 
// 	Double_t Tb      = fKinematics.Gett_b(); 
// 	Double_t Ta      = fKinematics.Gett_a();
// 	Double_t M       = fKinematics.GetM_A(); 
// 	Double_t Xi      = fKinematics.Getxi();
//         Double_t SIN     = TMath::Sin(theta/2.);
//         Double_t SIN2    = SIN*SIN;
//         Double_t Q2      = insane::Kine::Qsquared(Es,Ep,theta);
//         // Double_t Tr      = GetTr(Q2); 
//         Double_t FTilde  = GetFTilde(Q2); 
//         // omega_s 
//         Double_t num_s   = Ep;
//         Double_t den_s   = 1. - 2.*(Ep/M)*SIN2;
//         Double_t omega_s = Es - (num_s/den_s);
//         // omega_p 
//         Double_t num_p   = Es;
//         Double_t den_p   = 1. + 2.*(Es/M)*SIN2;
//         Double_t omega_p = (num_p/den_p) - Ep;
//         // v terms 
//         Double_t v_s     = omega_s/Es; 
//         Double_t v_p     = omega_p/(Ep + omega_p);
//         Double_t phi_s   = GetPhi(v_s); 
//         Double_t phi_p   = GetPhi(v_p);  
//         // put it together 
// 	Double_t num     = M_A + 2.*(Es - omega_s)*SIN2;
//         Double_t den     = M_A - 2.*Ep*SIN2; 
//         Double_t T1      = num/den; 
//         // Double_t T2      = FTilde*sigma_el(Es-omega_s,theta); 
//         Double_t T2=0;
// 	Double_t par[3]  = {Ep,theta,0.};
//         fUnpolXS->SetBeamEnergy(Es-omega_s); 
//         fPolDiffXS->SetBeamEnergy(Es-omega_s);
//         if(fPol==0){
// 		T2 = FTilde*fUnpolXS->EvaluateBaseXSec(par);
//         }else if(fPol==1){
// 		T2 = FTilde*fPolDiffXS->EvaluateBaseXSec(par);
//         }
//         Double_t T3 = b*Tb*phi_s/omega_s + Xi/(2.*omega_s*omega_s); 
//         // Double_t T4 = FTilde*sigma_el(Es,theta); 
//         Double_t T4=0; 
//         fUnpolXS->SetBeamEnergy(Es);
//         fPolDiffXS->SetBeamEnergy(Es);
// 	par[0] = Ep-omega_p; par[1] = theta; par[2] = 0.;  
//         if(fPol==0){
// 		T2 = FTilde*fUnpolXS->EvaluateBaseXSec(par);
//         }else if(fPol==1){
// 		T2 = FTilde*fPolDiffXS->EvaluateBaseXSec(par);
//         }
//         Double_t T5      = b*Ta*phi_p/omega_p + Xi/(2.*omega_p*omega_p); 
// 	Double_t sig     = T1*T2*T3 + T4*T5;
// 	// std::cout << "=========== sigma_b ===========" << std::endl;
// 	// std::cout << "T1 = " << T1 << std::endl;
// 	// std::cout << "T2 = " << T2 << std::endl;
// 	// std::cout << "T3 = " << T3 << std::endl;
// 	// std::cout << "T4 = " << T4 << std::endl;
// 	// std::cout << "T5 = " << T5 << std::endl;
// 
// 	return sig;
// }
//______________________________________________________________________________
Double_t RADCOR::sigma_el(Double_t Es,Double_t theta){

   Double_t M_A    = fKinematics.GetM_A(); 
   Double_t COS    = TMath::Cos(theta/2.);
   Double_t COS2   = COS*COS; 
   Double_t SIN    = TMath::Sin(theta/2.);
   Double_t SIN2   = SIN*SIN;
   Double_t TAN2   = SIN2/COS2; 
   Double_t Ep     = Es/(1.+2.*Es*SIN2/M_A);
   Double_t Q2     = insane::Kine::Qsquared(Es,Ep,theta);
   // form factors 
   Double_t ff[2]; 
   ProcessFormFactors(Q2,ff);                           
   Double_t tau    = Q2/(4.*M_A*M_A);                    
   Double_t GE     = ff[0];
   Double_t GM     = ff[1];
   // If we use these definitions of W1, W2, need to divide the Mott XS by 4 so that 
   // everything is consistent!  The Mott XS is usually defined as ( alpha*cos/(2*E*sin) )^2.  
   // Double_t W1      = 4.*tau*GM*GM;                     // G in Mo & Tsai       
   // Double_t W2      = 4.*(GE*GE + tau*GM*GM)/(1.+tau);  // F in Mo & Tsai 
   Double_t W1     = tau*GM*GM;                     // following eq A6 in Stein        
   Double_t W2     = (GE*GE + tau*GM*GM)/(1.+tau);  // following eq A6 in Stein 

   // Mott XS
   Double_t alpha  = fine_structure_const; 
   Double_t num    = alpha*alpha*COS*COS;
   Double_t den    = 4.*Es*Es*SIN2*SIN2; 
   Double_t MottXS = num/den;
   // put it together  
   Double_t T1     = MottXS*(Ep/Es); 
   Double_t T2     = W2 + 2.*TAN2*W1; 
   Double_t sig    = T1*T2; 

   // std::cout << "=========== sigma_el ===========" << std::endl;
   // std::cout << "W1 = " << W1 << std::endl;
   // std::cout << "W2 = " << W2 << std::endl;
   // std::cout << "T1 = " << T1 << std::endl;
   // std::cout << "T2 = " << T2 << std::endl;

   return sig;      

}
//______________________________________________________________________________
Double_t RADCOR::sigma_el_tilde(Double_t Es,Double_t theta){

   /// sigma_el_tilde(Es,th) 
   /// Computes the elastic cross section, scaled by FTilde (Eq A55)
   /// Stein Phys Rev D 1884 (1975)  

   Double_t M_A    = fKinematics.GetM_A(); 
   Double_t COS    = TMath::Cos(theta/2.);
   Double_t COS2   = COS*COS; 
   Double_t SIN    = TMath::Sin(theta/2.);
   Double_t SIN2   = SIN*SIN;
   Double_t TAN2   = SIN2/COS2; 
   Double_t Ep     = Es/(1.+2.*Es*SIN2/M_A);
   Double_t Q2     = insane::Kine::Qsquared(Es,Ep,theta);
   Double_t FTilde = GetFTilde(Q2); 
   // form factors 
   Double_t ff[2]; 
   ProcessFormFactors(Q2,ff);                           
   Double_t tau    = Q2/(4.*M_A*M_A);                    
   Double_t GE     = ff[0];
   Double_t GM     = ff[1];
   // If we use these definitions of W1, W2, need to divide the Mott XS by 4 so that 
   // everything is consistent!  The Mott XS is usually defined as ( alpha*cos/(2*E*sin) )^2.  
   // Double_t W1     = 4.*tau*GM*GM;                     // G in Mo & Tsai       
   // Double_t W2     = 4.*(GE*GE + tau*GM*GM)/(1.+tau);  // F in Mo & Tsai 
   Double_t W1     = tau*GM*GM;                     // following eq A6 in Stein        
   Double_t W2     = (GE*GE + tau*GM*GM)/(1.+tau);  // following eq A6 in Stein 

   // Mott XS
   Double_t alpha  = fine_structure_const; 
   Double_t num    = alpha*alpha*COS*COS;
   Double_t den    = 4.*Es*Es*SIN2*SIN2; 
   Double_t MottXS = num/den;
   // put it together  
   Double_t T1     = MottXS*(Ep/Es); 
   Double_t T2     = W2 + 2.*TAN2*W1; 
   Double_t sig    = FTilde*T1*T2; 

   // std::cout << "=========== sigma_el_tilde ===========" << std::endl;
   // std::cout << "W1 = " << W1 << std::endl;
   // std::cout << "W2 = " << W2 << std::endl;
   // std::cout << "T1 = " << T1 << std::endl;
   // std::cout << "T2 = " << T2 << std::endl;

   return sig;      

}
//______________________________________________________________________________
Double_t RADCOR::sigma_qe_b(){

   /// Elastic radiative tail [external] 
   /// Angle peaking approximation
   /// An adapted version of sigma_b,   
   /// The main difference here is that instead of 
   /// calling the elastic cross section, we call the 
   /// quasi-elastic cross section and M_A -> M  
   /// Phys Rev D 12 1884 (1975), Eq A49  

   Double_t Es      = fKinematics.GetEs(); 
   Double_t Ep      = fKinematics.GetEp(); 
   Double_t theta   = fKinematics.GetTheta();
   Double_t b       = fKinematics.Getb(); 
   Double_t Tb      = fKinematics.Gett_b(); 
   Double_t Ta      = fKinematics.Gett_a();
   Double_t M_A     = fKinematics.GetM(); 
   Double_t Xi      = fKinematics.Getxi();
   Double_t SIN     = TMath::Sin(theta/2.);
   Double_t SIN2    = SIN*SIN;
   Double_t Q2      = insane::Kine::Qsquared(Es,Ep,theta);
   Double_t mp      = F_soft(Es,Ep,theta); 
   if(!fIsExternal){
      Tb = 0.;
      Ta = 0.;
   } 
   // Double_t Tr      = GetTr(Q2); 
   // Double_t FTilde  = GetFTilde(Q2); 
   // omega_s = Es - EsMin (quasi-elastic)  
   // Double_t num_s   = Ep;
   // Double_t den_s   = 1. - 2.*(Ep/M_A)*SIN2;
   // Double_t omega_s = Es - (num_s/den_s);
   Double_t EsMin   = GetEsMin(Ep,theta); 
   Double_t omega_s = Es - EsMin;
   // omega_p = EpMax (quasi-elastic) - Ep
   // Double_t num_p   = Es;
   // Double_t den_p   = 1. + 2.*(Es/M_A)*SIN2;
   // Double_t omega_p = (num_p/den_p) - Ep;
   Double_t EpMax   = GetEpMax(Es,theta); 
   Double_t omega_p = EpMax - Ep; 
   // v terms 
   Double_t v_s     = omega_s/Es; 
   Double_t v_p     = omega_p/(Ep + omega_p);
   Double_t phi_s   = GetPhi(v_s); 
   Double_t phi_p   = GetPhi(v_p);  
   // put it together 
   Double_t num     = M_A + 2.*(Es - omega_s)*SIN2;
   Double_t den     = M_A - 2.*Ep*SIN2; 
   Double_t T1      = num/den; 
   Double_t T2      = sigma_qe_tilde(Es-omega_s,theta); 
   Double_t T3      = b*Tb*phi_s/omega_s + Xi/(2.*omega_s*omega_s); 
   Double_t T4      = sigma_qe_tilde(Es,theta); 
   Double_t T5      = b*Ta*phi_p/omega_p + Xi/(2.*omega_p*omega_p); 
   Double_t sig     = T1*T2*T3 + T4*T5;

   if(fIsMultiPhoton) sig *= mp; 

   Int_t IsNotANumber = TMath::IsNaN(sig);

   if(IsNotANumber) sig = 0.; 

   // std::cout << "=========== sigma_qe_b ===========" << std::endl;
   // std::cout << "Es      = " << Es      << std::endl;
   // std::cout << "EsMin   = " << EsMin   << std::endl;
   // std::cout << "omega_s = " << omega_s << std::endl;
   // std::cout << "Ep      = " << Ep      << std::endl;
   // std::cout << "EpMax   = " << EpMax   << std::endl;
   // std::cout << "omega_p = " << omega_p << std::endl;
   // std::cout << "T1      = " << T1      << std::endl;
   // std::cout << "T2      = " << T2      << std::endl;
   // std::cout << "T3      = " << T3      << std::endl;
   // std::cout << "T4      = " << T4      << std::endl;
   // std::cout << "T5      = " << T5      << std::endl;
   // std::cout << "mp      = " << mp      << std::endl;
   // std::cout << "xi      = " << Xi      << std::endl;
   
   return sig;
}
//______________________________________________________________________________
Double_t RADCOR::sigma_qe(Double_t Es,Double_t theta){

   Double_t M_A    = fKinematics.GetM();  // note the change!  
   Double_t COS    = TMath::Cos(theta/2.);
   Double_t COS2   = COS*COS; 
   Double_t SIN    = TMath::Sin(theta/2.);
   Double_t SIN2   = SIN*SIN;
   Double_t TAN2   = SIN2/COS2; 
   // Double_t Ep     = Es/(1.+2.*Es*SIN2/M_A);
   Double_t Ep     = GetEpMax(Es,theta);
   Double_t x      = insane::Kine::xBjorken_EEprimeTheta(Es,Ep,theta); 
   Double_t Q2     = insane::Kine::Qsquared(Es,Ep,theta);
   // form factors 
   Double_t ff[2]; 
   ProcessQuasiElasticFormFactors(x,Q2,ff);                           
   Double_t tau     = Q2/(4.*M_A*M_A);                    
   Double_t GE      = ff[0];
   Double_t GM      = ff[1];
   // If we use these definitions of W1, W2, need to divide the Mott XS by 4 so that 
   // everything is consistent!  The Mott XS is usually defined as ( alpha*cos/(2*E*sin) )^2.  
   // Double_t W1      = 4.*tau*GM*GM;                     // G in Mo & Tsai       
   // Double_t W2      = 4.*(GE*GE + tau*GM*GM)/(1.+tau);  // F in Mo & Tsai 
   Double_t W1      = tau*GM*GM;                     // following eq A6 in Stein        
   Double_t W2      = (GE*GE + tau*GM*GM)/(1.+tau);  // following eq A6 in Stein 

   // Mott XS
   Double_t alpha  = fine_structure_const; 
   Double_t num    = alpha*alpha*COS*COS;
   Double_t den    = 4.*Es*Es*SIN2*SIN2; 
   Double_t MottXS = num/den;
   // put it together  
   Double_t T1     = MottXS*(Ep/Es); 
   Double_t T2     = W2 + 2.*TAN2*W1; 
   Double_t sig    = T1*T2; 

   // std::cout << "=========== sigma_qe ===========" << std::endl;
   // std::cout << "Es  = " << Es  << std::endl; 
   // std::cout << "Ep  = " << Ep  << std::endl; 
   // std::cout << "x   = " << x   << std::endl; 
   // std::cout << "Q2  = " << Q2  << std::endl; 
   // std::cout << "W1  = " << W1  << std::endl;
   // std::cout << "W2  = " << W2  << std::endl;
   // std::cout << "T1  = " << T1  << std::endl;
   // std::cout << "T2  = " << T2  << std::endl;
   // std::cout << "sig = " << sig << std::endl;

   return sig;      

}
//______________________________________________________________________________
Double_t RADCOR::sigma_qe_tilde(Double_t Es,Double_t theta){

   /// sigma_qe_tilde(Es,theta) 
   /// Computes the quasi-elastic cross section scaled by FTilde (Eq A55)  
   /// Adapted from Stein Phys Rev D 12 1884 (1975)

   Double_t M_A    = fKinematics.GetM();  // note the change!  
   Double_t COS    = TMath::Cos(theta/2.);
   Double_t COS2   = COS*COS; 
   Double_t SIN    = TMath::Sin(theta/2.);
   Double_t SIN2   = SIN*SIN;
   Double_t TAN2   = SIN2/COS2; 
   // Double_t Ep     = Es/(1.+2.*Es*SIN2/M_A);
   Double_t Ep     = GetEpMax(Es,theta);
   Double_t x      = insane::Kine::xBjorken_EEprimeTheta(Es,Ep,theta); 
   Double_t Q2     = insane::Kine::Qsquared(Es,Ep,theta);
   Double_t FTilde = GetFTilde(Q2); 
   // form factors 
   Double_t ff[2]; 
   ProcessQuasiElasticFormFactors(x,Q2,ff);                           
   Double_t tau    = Q2/(4.*M_A*M_A);                    
   Double_t GE     = ff[0];
   Double_t GM     = ff[1];
   // If we use these definitions of W1, W2, need to divide the Mott XS by 4 so that 
   // everything is consistent!  The Mott XS is usually defined as ( alpha*cos/(2*E*sin) )^2.  
   // Double_t W1     = 4.*tau*GM*GM;                     // G in Mo & Tsai       
   // Double_t W2     = 4.*(GE*GE + tau*GM*GM)/(1.+tau);  // F in Mo & Tsai 
   Double_t W1     = tau*GM*GM;                     // following eq A6 in Stein        
   Double_t W2     = (GE*GE + tau*GM*GM)/(1.+tau);  // following eq A6 in Stein 

   // Mott XS
   Double_t alpha  = fine_structure_const; 
   Double_t num    = alpha*alpha*COS*COS;
   Double_t den    = 4.*Es*Es*SIN2*SIN2; 
   Double_t MottXS = num/den;
   // put it together  
   Double_t T1     = MottXS*(Ep/Es); 
   Double_t T2     = W2 + 2.*TAN2*W1; 
   Double_t sig    = FTilde*T1*T2; 

   // std::cout << "=========== sigma_qe_tilde ===========" << std::endl;
   // std::cout << "Es  = " << Es  << std::endl; 
   // std::cout << "Ep  = " << Ep  << std::endl; 
   // std::cout << "x   = " << x   << std::endl; 
   // std::cout << "Q2  = " << Q2  << std::endl; 
   // std::cout << "W1  = " << W1  << std::endl;
   // std::cout << "W2  = " << W2  << std::endl;
   // std::cout << "T1  = " << T1  << std::endl;
   // std::cout << "T2  = " << T2  << std::endl;
   // std::cout << "sig = " << sig << std::endl;

   return sig;      

}

//______________________________________________________________________________
Double_t RADCOR::Ie(Double_t E0,Double_t E,Double_t t){
   /// Ionization and bremsstrahlung loss [updated] 
   /// Tsai, SLAC-PUB-0848 (1971) 
   /// Eq B39 
   Double_t delta_0 = 0.0;//Delta_0(E0,t); 
   Double_t b       = fKinematicsExt.Getb(); 
   Double_t X0      = fX0Eff;  
   Double_t gam     = TMath::Gamma(1.+b*t);
   Double_t arg     = (E0-delta_0)/E0;
   Double_t F       = TMath::Power(arg,b*t);
   Double_t Wb      = W_b(E0-delta_0,E0-delta_0-E); 
   Double_t Wi      = W_i(E0-delta_0,E0-delta_0-E); 
   Double_t ie      = (t*X0/gam)*F*(Wb + Wi); 


   if( (E0-delta_0-E)<0 ){
      std::cout << "NO.  Something is wrong." << std::endl;
      std::cout << "E0      = " << E0      << std::endl;
      std::cout << "E       = " << E       << std::endl;
      std::cout << "delta_0 = " << delta_0 << std::endl;
      exit(1);
   }

   if(fDebug>3){
      std::cout << "----------- Ie_new(E0,E,t) -----------" << std::endl;
      std::cout << "E0          = " << E0      << std::endl;
      std::cout << "E           = " << E       << std::endl;
      std::cout << "t           = " << t       << std::endl;
      std::cout << "b           = " << b       << std::endl;
      std::cout << "delta_0     = " << delta_0 << std::endl;
      std::cout << "X0          = " << X0      << std::endl;
      std::cout << "gamma(1+bt) = " << gam     << std::endl;
      std::cout << "(E0-delta)/delta = " << arg << std::endl;
      std::cout << "F           = " << F       << std::endl;
      std::cout << "Wb          = " << Wb      << std::endl;
      std::cout << "Wi          = " << Wi      << std::endl;
      std::cout << "Ie          = " << ie      << std::endl;
   }
   return ie; 

}
//______________________________________________________________________________
Double_t RADCOR::Ie_2(Double_t E0,Double_t E,Double_t t){
   /// Bremsstrahlung loss 
   /// Tsai, SLAC-PUB-0848 (1971), Eq 5.3 
   /// This is utilizing the equivalent radiator method, 
   /// so t should be 1/2 the thickness of the radiating material 
   // general terms 
   Double_t b     = fKinematicsExt.Getb(); 
   Double_t theta = fKinematicsExt.GetTheta();
   Double_t Q2    = insane::Kine::Qsquared(E0,E,theta); 
   Double_t t_r   = GetTr(Q2);
   Double_t v     = (E0-E)/E0;
   Double_t exp1  = b*(t + t_r);
   // Term 1 
   Double_t T1    = TMath::Power(v,exp1);
   // Term 2  
   Double_t T2    = exp1/(E0-E); 
   // Term 3  
   Double_t phi   = GetPhi(v); 
   Double_t T3    = phi;
   // Put it together  
   Double_t ie    = T1*T2*T3;
   return ie; 

}
//______________________________________________________________________________
Double_t RADCOR::Ib(Double_t E0,Double_t E,Double_t t){
   /// Bremsstrahlung loss 
   /// Tsai, SLAC-PUB-0848 (1971), Eq B.43
   /// Most correct equation   
   // general terms 
   Double_t b     = fKinematicsExt.Getb(); 
   Double_t gamma = TMath::Gamma(1. + b*t);
   Double_t Wb    = W_b(E0,E0-E); 
   Double_t v     = (E0-E)/E0;
   Double_t exp1  = b*t;
   Double_t X0    = fX0Eff;
   // Term 1 
   Double_t T1    = t*X0/gamma; 
   // Term 2 
   Double_t T2    = TMath::Power(v,exp1);
   // Term 3  
   Double_t T3    = Wb;
   // Put it together  
   Double_t ib    = T1*T2*T3;
   return ib; 
}
//______________________________________________________________________________
Double_t RADCOR::Ib_2(Double_t E0,Double_t E,Double_t t){
   /// Bremsstrahlung loss 
   /// Tsai, SLAC-PUB-0848 (1971), Eq B.43
   /// Most correct equation  
   /// Same as Ib above, but we explicitly write out W_b(E,eps) 
   /// (gives the same result as Ib)  
   // general terms 
   Double_t b     = fKinematicsExt.Getb(); 
   Double_t gamma = TMath::Gamma(1. + b*t);
   // Double_t Wb    = W_b(E0,E0-E);
   // Wb is (X0 cancels out when combined with everything in this method): 
   // Double_t b     = fKinematicsExt.Getb(); 
   // Double_t X0    = fX0Eff;
   // Double_t Wb    = (1./X0)*(b/eps)*GetPhi(eps/E0);
   Double_t v     = (E0-E)/E0;
   Double_t phi   = GetPhi(v);  
   // Term 1 
   Double_t T1    = (b*t/gamma)*( 1./(E0-E) ); 
   // Term 2 
   Double_t T2    = TMath::Power(v,b*t);
   // Term 3  
   Double_t T3    = phi;
   // Put it together  
   Double_t ib    = T1*T2*T3;
   if(fDebug>4){
      std::cout << "-------------- Ib_2 --------------" << std::endl;
      std::cout << "E0 = " << E0 << std::endl;
      std::cout << "E  = " << E  << std::endl;
      std::cout << "t  = " << t  << std::endl;
      std::cout << "T1 = " << T1  << std::endl;
      std::cout << "T2 = " << T2  << std::endl;
      std::cout << "T3 = " << T3  << std::endl;
      std::cout << "ib = " << ib  << std::endl;
   }
   return ib; 
}
//______________________________________________________________________________
Double_t RADCOR::Delta(Double_t E,Double_t t){

   /// Most probable energy loss due to ionization of an 
   /// electron passing through material of thickness t.
   /// For Es, use t_b; for Ep, use t_a
   /// Tsai, SLAC-PUB-0848 (1971), Eq 1.8 
   Double_t Z     = fZEff;
   Double_t A     = fAEff;
   Double_t m     = fKinematicsExt.Getm();
   Double_t X0    = fX0Eff; 
   Double_t a     = 0.154E-3*(Z/A);               /// B19 [in GeV]
   Double_t arg   = 3E+9*a*t*X0*E*E/(m*m*Z*Z); 
   Double_t delta=0; 
   if(t>0){
      delta = a*t*X0*( TMath::Log(arg) - 0.5772); 
   }else{
      delta = 0.; 
   } 
   return delta;

}
//______________________________________________________________________________
Double_t RADCOR::Delta_0(Double_t E0,Double_t t){
   /// Most probable energy loss due to ionization  
   /// Tsai, SLAC-PUB-0848 (1971) 
   /// Eq B22
   Double_t Z         = fZEff;
   Double_t A         = fAEff;
   Double_t m         = fKinematicsExt.Getm();
   Double_t X0        = fX0Eff; 
   Double_t a         = 0.154E-3*(Z/A);                            /// B19 [in GeV] 
   Double_t I         = 13.2E-6*Z;                                 /// B20 [in GeV] (nota bene!)
   Double_t eps_prime = m*TMath::Exp(1.)*I*I/(2.*E0*E0);           /// B20  
   Double_t delta_0=0;
   if(t>0){
      delta_0   = a*t*X0*(TMath::Log(a*t*X0/eps_prime) + 1. - 0.5772);
   }
   return delta_0; 

}
//______________________________________________________________________________
Double_t RADCOR::W_b(Double_t E0,Double_t eps){
   /// Bremsstrahlung loss [updated] 
   /// Tsai, SLAC-PUB-0848 (1971) 
   /// Eq B3 
   Double_t b         = fKinematicsExt.Getb(); 
   Double_t X0        = fX0Eff;
   Double_t Wb        = (1./X0)*(b/eps)*GetPhi(eps/E0);
   if(fDebug>3){
      std::cout << "------ Wb(E0,eps) -------" << std::endl;
      std::cout << "E0     = " << E0     << std::endl;
      std::cout << "eps    = " << eps    << std::endl;
      std::cout << "b      = " << b      << std::endl;
      std::cout << "eps/E0 = " << eps/E0 << std::endl;
      std::cout << "X0     = " << X0     << std::endl;
      std::cout << "Wb     = " << Wb     << std::endl;
   }
   return Wb; 
}
//______________________________________________________________________________
Double_t RADCOR::W_i(Double_t E0,Double_t eps){
   /// Ionization loss [updated] 
   /// Tsai, SLAC-PUB-0848 (1971) 
   /// Eq B1 
   Double_t Z         = fZEff;
   Double_t A         = fAEff;
   Double_t a         = 0.154E-3*(Z/A);                                /// B19 [in GeV] 
   Double_t Wi        = (a/eps*eps)*TMath::Power(1. + eps*eps/(E0*(E0-eps)),2.);
   return Wi; 
}
//______________________________________________________________________________
Double_t RADCOR::F_soft(Double_t Es,Double_t Ep,Double_t theta){
   /// Stein et al, Phys Rev D 12, 1884 (1975) (Eq A58) 
   Double_t M_A     = fKinematics.GetM_A();
   Double_t Tb      = fKinematics.Gett_b(); 
   Double_t Ta      = fKinematics.Gett_a(); 
   Double_t b       = fKinematics.Getb();
   Double_t SIN     = TMath::Sin(theta/2.); 
   Double_t SIN2    = SIN*SIN;

   if(!fIsExternal){
      Tb = 0.;
      Ta = 0.;
   }
   // FIXME: What about when internal is off? 

   // omega_s 
   Double_t num_s   = Ep; 
   Double_t den_s   = 1. - 2.*(Ep/M_A)*SIN2; 
   Double_t omega_s = Es - (num_s/den_s); 
   // omega_p 
   Double_t num_p   = Es; 
   Double_t den_p   = 1. + 2.*(Es/M_A)*SIN2;
   Double_t omega_p = (num_p/den_p) - Ep; 
   Double_t Q2      = insane::Kine::Qsquared(Es,Ep,theta); 
   Double_t Tr      = GetTr(Q2); 
   // First term    
   Double_t arg1    = omega_s/Es; 
   // if(TMath::Abs(arg1)<1E-15) arg1=0; 
   Double_t T1      = TMath::Power( arg1,b*(Tb+Tr) );
   // Second term     
   Double_t arg2    = omega_p/(Ep + omega_p); 
   // if(TMath::Abs(arg2)<1E-15) arg2=0; 
   Double_t T2      = TMath::Power( arg2,b*(Ta+Tr) ); 
   Double_t FSoft   = T1*T2;
   if(fDebug>4){
      std::cout << "----- FSoft -----" << std::endl;
      std::cout << "M_A      = " << M_A          << std::endl;
      std::cout << "Es       = " << Es           << std::endl;
      std::cout << "Ep       = " << Ep           << std::endl;
      std::cout << "num_s    = " << num_s        << std::endl;
      std::cout << "den_s    = " << den_s        << std::endl;
      std::cout << "omega_s  = " << omega_s      << std::endl;
      std::cout << "num_p    = " << num_p        << std::endl;
      std::cout << "den_p    = " << den_p        << std::endl;
      std::cout << "omega_p  = " << omega_p      << std::endl;
      std::cout << "th       = " << theta/degree << std::endl;
      std::cout << "b        = " << b            << std::endl;
      std::cout << "tr       = " << Tr           << std::endl;
      std::cout << "Tb       = " << Tb           << std::endl;
      std::cout << "Ta       = " << Ta           << std::endl;
      std::cout << "b(Tb+Tr) = " << b*(Tb+Tr)    << std::endl;
      std::cout << "T1       = " << T1           << std::endl;
      std::cout << "b(Ta+Tr) = " << b*(Ta+Tr)    << std::endl;
      std::cout << "T2       = " << T2           << std::endl;
   }
   return FSoft;

}
//________________________________________________________________________
Double_t RADCOR::Delta_r(Double_t Delta){

   /// Appears to be delta_v - delta_inf (?) 
   /// Mo & Tsai, Rev Mod Phys 41, 205 (1969) 
   /// Eq B7 

   TLorentzVector s = fKinematicsInt.Gets(); 
   TLorentzVector p = fKinematicsInt.Getp();
   Double_t Es      = fKinematicsInt.GetEs();  
   Double_t Ep      = fKinematicsInt.GetEp();  
   Double_t m       = fKinematicsInt.Getm();  

   Double_t alpha   = fine_structure_const; 
   Double_t N       = -alpha/pi; 
   Double_t s_dot_p = s*p; 
   Double_t arg1    = 2.*s_dot_p/(m*m); 
   Double_t T1      = (28./9.) - (13./6.)*TMath::Log(arg1); 
   Double_t T2      = ( TMath::Log(Es/Delta) + TMath::Log(Ep/Delta) )*(TMath::Log(arg1) - 1.); 
   Double_t T3      = -GetSpence( (Ep-Es)/Ep ); 
   Double_t T4      = -GetSpence( (Es-Ep)/Es );
   Double_t delta_r = N*(T1+T2+T3+T4);
   return delta_r; 

}
//________________________________________________________________________
Double_t RADCOR::Delta_inf_1(Double_t Delta){

   /// Mo & Tsai, Rev Mod Phys 41, 205 (1969) 
   /// Inferred from Eq B7  

   TLorentzVector s = fKinematicsInt.Gets(); 
   TLorentzVector p = fKinematicsInt.Getp();
   Double_t Es      = fKinematicsInt.GetEs();  
   Double_t Ep      = fKinematicsInt.GetEp();  
   Double_t m       = fKinematicsInt.Getm();  

   Double_t alpha     = fine_structure_const; 
   Double_t N         = -alpha/pi; 
   Double_t s_dot_p   = s*p; 
   Double_t arg1      = 2.*s_dot_p/(m*m); 
   Double_t T1        = ( TMath::Log(Es/Delta) + TMath::Log(Ep/Delta) )*(TMath::Log(arg1) - 1.); 
   Double_t delta_inf = N*T1;
   return delta_inf; 

}
//________________________________________________________________________
Double_t RADCOR::Delta_inf_2(){

   /// POLRAD manual  

   TLorentzVector s = fKinematicsInt.Gets(); 
   TLorentzVector p = fKinematicsInt.Getp();
   Double_t Es      = fKinematicsInt.GetEs();  
   Double_t Ep      = fKinematicsInt.GetEp();  
   Double_t Q2      = fKinematicsInt.GetQ2();
   Double_t Wsq     = fKinematicsInt.GetW2(); 
   Double_t m       = fKinematicsInt.Getm();  
   Double_t M       = fKinematicsInt.GetM(); 
   Double_t M_A     = fKinematicsInt.GetM_A(); 
   Double_t M_pion  = fKinematics.GetM_pion();
   Double_t W_el    = M_A;  
   Double_t W_qe    = M;  
   Double_t W_pi    = M + M_pion; 
   Double_t Wsq_thr = 0.; 
   switch(fThreshold){
      case 0: // elastic
         Wsq_thr = TMath::Power(W_el,2.);
         break;
      case 1: // quasi-elastic 
         Wsq_thr = TMath::Power(W_qe,2.);
         break;
      case 2: // pion production  
         Wsq_thr = TMath::Power(W_pi,2.);
         break;
      default:
         std::cout << "[RADCOR::Delta_inf_2]: Invalid integration threshold!  Exiting..." << std::endl;
         exit(1);
   }
   // Term 1
   Double_t s_dot_p   = s*p; 
   Double_t arg1      = 2.*s_dot_p/(m*m);
   Double_t v1        = TMath::Log(arg1);
   Double_t T1        = 0.5*v1*v1;  
   // Term 2 
   Double_t arg2      = 2.*s_dot_p/(m*m);
   Double_t T2        = (-1.)*(TMath::Log(arg2) - 1.); 
   // Term 3 
   Double_t num       = (2.*M*Ep + Q2)*(2.*M*Es - Q2);  // S'*X' from POLRAD manual  
   // Double_t den       = TMath::Power(Wsq - Wsq_thr,2.); 
   Double_t den       = Wsq*m*m;  
   Double_t arg3      = num/den; 
   Double_t T3        = TMath::Log(arg3); 
   // Put it together 
   Double_t alpha     = fine_structure_const; 
   Double_t N         = alpha/pi; 
   Double_t delta_inf = N*(T1 + T2*T3);
   return delta_inf; 

}
//________________________________________________________________________
Double_t RADCOR::EnergyPeakingApprox(Double_t Es,Double_t Ep,Double_t theta){

   SetKinematics(Es,Ep,theta); 

   Double_t par[3]   = {Ep,theta,0.};
   fUnpolXS->SetBeamEnergy(Es); 
   fPolDiffXS->SetBeamEnergy(Es); 
   Double_t BornXS=0;

   if(fUseGridData){
        BornXS = FTCS(Es,Ep); 
   }else{
      if(fPol==0){ 
         // unpolarized
         BornXS = fUnpolXS->EvaluateBaseXSec(par); 
      }else if(fPol>0){
         // polarized (para or perp -- fPol is applied to fPolDiffXS elsewhere)  
         BornXS = fPolDiffXS->EvaluateBaseXSec(par); 
      }else{
         std::cout << "[RADCOR::EnergyPeakingApprox]: Error!  Invalid cross section!  Exiting..." << std::endl;
         exit(1); 
      }
   }

   Int_t NotANumber = TMath::IsNaN(BornXS);

   if(NotANumber){
      std::cout << "[RADCOR::EnergyPeakingApprox]: Invalid Born cross section!  Exiting..." << std::endl;
      std::cout << "[RADCOR::EnergyPeakingApprox]: Reason: Not a number."                   << std::endl;
      std::cout << "Es = "    << Es           << std::endl;
      std::cout << "Ep = "    << Ep           << std::endl;
      std::cout << "theta = " << theta/degree << std::endl;
      exit(1); 
   }

   Double_t mp     = F_soft(Es,Ep,theta);
   Double_t AnsEs  = CalculateEsIntegral(); 
   Double_t AnsEp  = CalculateEpIntegral(); 
   Double_t RadXS  = fCFACT*BornXS + AnsEs + AnsEp;
  
   // It looks to me that the multiple photon effects are included in CFACT and the integrals... 
   // if(fIsMultiPhoton) RadXS *= mp;                    // I think we need this...  
 
   if(fDebug>5){ 
   std::cout << "Es = "      << Es     << "\t" 
             << "Ep = "      << Ep     << "\t" 
             << "th = "      << theta  << "\t" 
             << "mp = "      << mp     << "\t" 
             << "CFACT = "   << fCFACT << "\t"
             << "born xs = " << BornXS << "\t"
             << "AnsEs = "   << AnsEs  << "\t"
             << "AnsEp = "   << AnsEp  << "\t"
             << "rad xs = "  << RadXS  << std::endl;
   }

   return RadXS; 

}
//______________________________________________________________________________
Double_t RADCOR::EsIntegrand(Double_t &EsPrime){

   // general terms
   Double_t Es         = fKinematics.GetEs();
   Double_t Ep         = fKinematics.GetEp();
   Double_t theta      = fKinematics.GetTheta();
   Double_t M          = fKinematics.GetM();
   Double_t M_A        = fKinematics.GetM_A();
   Double_t Tb         = fKinematics.Gett_b();
   Double_t Ta         = fKinematics.Gett_a();
   Double_t R          = fKinematics.GetR();
   Double_t b          = fKinematics.Getb();
   Double_t Xi         = fKinematics.Getxi();
   Double_t Q2         = insane::Kine::Qsquared(EsPrime,Ep,theta); 
   Double_t FTilde     = GetFTilde(Q2);  
   Double_t Tr         = GetTr(Q2); 
   Double_t dEs        = Es-EsPrime; 
   Double_t v          = dEs/Es; 
   Double_t phi        = GetPhi(v);
   Double_t SIN        = TMath::Sin(theta/2.); 
   Double_t SIN2       = SIN*SIN;  
   Double_t EsQEThresh = Ep/(1. - 2.*(Ep/M)*SIN2); 
   Double_t EsElThresh = Ep/(1. - 2.*(Ep/M_A)*SIN2); 

   // if(!fIsExternal){
   //       Tb = 0.;
   //       Ta = 0.;
   //       Xi = 0.; 
   // }

   // std::cout << "-------------------- Es Integrand (peaking) --------------------" << std::endl;
   // std::cout << "Es     = " << Es      << std::endl;
   // std::cout << "Es'    = " << EsPrime << std::endl;
   // std::cout << "Ep     = " << Ep      << std::endl;
   // std::cout << "Tb     = " << Tb      << std::endl;
   // std::cout << "Ta     = " << Ta      << std::endl;
   // std::cout << "Tr     = " << Tr      << std::endl;
   // std::cout << "Xi     = " << Xi      << std::endl;
   // std::cout << "FTilde = " << FTilde << std::endl;

   Double_t par[3]     = {Ep,theta,0.};    // remember, the third argument is the angle phi = 0! 
   fUnpolXS->SetBeamEnergy(EsPrime); 
   fPolDiffXS->SetBeamEnergy(EsPrime);
   Double_t Sig=0;

   if(fUseGridData){
      Sig = FTCS(EsPrime,Ep); 
   }else{ 
      if(fPol==0){ 
         // unpolarized
         Sig = fUnpolXS->EvaluateBaseXSec(par); 
      }else if(fPol>0){
         // polarized (para or perp) 
         Sig = fPolDiffXS->EvaluateBaseXSec(par); 
      }else{
         std::cout << "[RADCOR::EsIntegrand]: Error!  Invalid cross section!  Exiting..." << std::endl;
         exit(1); 
      }
   }

   if(fDebug>5) std::cout << "Es integrand: Es = " << EsPrime << "\t" << "Ep = " << Ep << "\t" << "th = " << theta/degree << "\t" << "xs = " << Sig << std::endl;

   Double_t arg_qe = 100.*TMath::Abs(EsPrime-EsQEThresh)/EsQEThresh; 
   Double_t arg_el = 100.*TMath::Abs(EsPrime-EsElThresh)/EsElThresh; 

   if( arg_qe < 1.0 ){
      if(fDebug>4) std::cout << "Es = " << EsPrime << "\t" << "Ep = " << Ep << "\t" << "Es (QE thresh) = " << EsQEThresh << "\t" << "xs = " << Sig << std::endl;
   }

   if( arg_el < 1.0 ){
      if(fDebug>4) std::cout << "Es = " << EsPrime << "\t" << "Ep = " << Ep << "\t" << "Es (El thresh) = " << EsQEThresh << "\t" << "xs = " << Sig << std::endl;
   }


   if(Sig!=Sig){
      std::cout << "[RADCOR::EsIntegrand]: Invalid cross section! " << std::endl;
      std::cout << "Es' = " << EsPrime << "\t" << "Ep = " << Ep << "\t" << "th = " << theta/degree << "\t" << "xs = " << Sig << std::endl; 
      exit(1);
   }

   Double_t SigTilde = FTilde*Sig;  
   // first term 
   Double_t Term1    = dEs/(Ep*R);
   Double_t Exp1     = b*(Ta+Tr); 
   Double_t T1       = TMath::Power(Term1,Exp1); 
   // second term
   Double_t Term2    = dEs/Es; 
   Double_t Exp2     = b*(Tb+Tr); 
   Double_t T2       = TMath::Power(Term2,Exp2); 
   // third term 
   Double_t T3       = b*( ((Tb+Tr)/dEs)*phi + Xi/(2.0*TMath::Power(dEs,2.0)) ); 
   // put it all together
   // std::cout << "FES: T1 = " << T1 << "\t" << "T2 = " << T2 << "\t" << "T3 = " << T3 << "\t" << "sig_tilde = " << SigTilde << std::endl;
   Double_t FES      = T1*T2*T3*SigTilde; 
   return FES; 

}
//______________________________________________________________________________
Double_t RADCOR::EpIntegrand(Double_t &EpPrime){

   // general terms 
   Double_t Es         = fKinematics.GetEs();
   Double_t Ep         = fKinematics.GetEp();
   Double_t theta      = fKinematics.GetTheta();
   Double_t M          = fKinematics.GetM();
   Double_t M_A        = fKinematics.GetM_A();
   Double_t Q2         = insane::Kine::Qsquared(Es,EpPrime,theta); 
   Double_t Tb         = fKinematics.Gett_b();
   Double_t Ta         = fKinematics.Gett_a();
   Double_t R          = fKinematics.GetR();
   Double_t b          = fKinematics.Getb();
   Double_t Xi         = fKinematics.Getxi();
   Double_t Tr         = GetTr(Q2);
   Double_t FTilde     = GetFTilde(Q2); 
   Double_t dEp        = EpPrime-Ep; 
   Double_t v          = dEp/EpPrime; 
   Double_t phi        = GetPhi(v);  
   Double_t SIN        = TMath::Sin(theta/2.); 
   Double_t SIN2       = SIN*SIN;  
   Double_t EpQEThresh = Es/(1. + 2.*(Es/M)*SIN2); 
   Double_t EpElThresh = Es/(1. + 2.*(Es/M_A)*SIN2); 

   if(!fIsExternal){
      Tb = 0.;
      Ta = 0.;
      Xi = 0.; 
   }

   // std::cout << "-------------------- Ep Integrand (peaking) --------------------" << std::endl;
   // std::cout << "Es  = "    << Es      << std::endl;
   // std::cout << "Ep  = "    << Ep      << std::endl;
   // std::cout << "Ep' = "    << EpPrime << std::endl;
   // std::cout << "Tb = "     << Tb      << std::endl;
   // std::cout << "Ta = "     << Ta      << std::endl;
   // std::cout << "Tr = "     << Tr      << std::endl;
   // std::cout << "Xi = "     << Xi      << std::endl;
   // std::cout << "FTilde = " << FTilde << std::endl;

   Double_t par[3]     = {EpPrime,theta,0.}; 
   fUnpolXS->SetBeamEnergy(Es); 
   fPolDiffXS->SetBeamEnergy(Es);
   Double_t Sig=0;

   // if(!fUseGridData) std::cout << "FALSE!" << std::endl;

   if(fUseGridData){
      Sig = FTCS(Es,EpPrime); 
   }else{ 
      if(fPol==0){ 
         // unpolarized
         Sig = fUnpolXS->EvaluateBaseXSec(par); 
      }else if(fPol>0){
         // polarized (para or perp) 
         Sig = fPolDiffXS->EvaluateBaseXSec(par); 
      }else{
         std::cout << "[RADCOR::EpIntegrand]: Error!  Invalid cross section!  Exiting..." << std::endl;
         exit(1); 
      }
   }

   if(fDebug>5) std::cout << "Ep integrand: Es = " << Es << "\t" << "Ep = " << EpPrime << "\t" << "th = " << theta/degree << "\t" << "xs = " << Sig << std::endl;

   Double_t arg_qe = 100.*TMath::Abs(EpPrime-EpQEThresh)/EpQEThresh; 
   Double_t arg_el = 100.*TMath::Abs(EpPrime-EpElThresh)/EpElThresh; 

   if( arg_qe < 1.0 ){
      if(fDebug>4) std::cout << "Es = " << Es << "\t" << "Ep = " << EpPrime << "\t" << "Ep (QE thresh) = " << EpQEThresh << "\t" << "xs = " << Sig << std::endl;
   }

   if( arg_el < 1.0 ){
      if(fDebug>4) std::cout << "Es = " << Es << "\t" << "Ep = " << EpPrime << "\t" << "Ep (El thresh) = " << EpElThresh << "\t" << "xs = " << Sig << std::endl;
   }

   if(Sig!=Sig){
      std::cout << "[RADCOR::EpIntegrand]: Invalid cross section! " << std::endl;
      std::cout << "Es = " << Es << "\t" << "Ep' = " << EpPrime << "\t" << "th = " << theta/degree << "\t" << "xs = " << Sig << std::endl; 
      exit(1);
   }
   Double_t SigTilde = FTilde*Sig;  
   // first term 
   Double_t Term1    = dEp/(EpPrime);
   Double_t Exp1     = b*(Ta+Tr); 
   Double_t T1       = pow(Term1,Exp1); 
   // second term
   Double_t Term2    = (dEp*R)/Es; 
   Double_t Exp2     = b*(Tb+Tr); 
   Double_t T2       = pow(Term2,Exp2); 
   // third term 
   Double_t T3       = b*( ((Ta+Tr)/dEp)*phi + Xi/(2.0*pow(dEp,2.0)) ); 
   // put it all together
   // std::cout << "FEP: T1 = " << T1 << "\t" << "T2 = " << T2 << "\t" << "T3 = " << T3 << "\t" << "sig_tilde = " << SigTilde << std::endl;
   Double_t FEP      = T1*T2*T3*SigTilde; 

   return FEP; 

}
//______________________________________________________________________________
Double_t RADCOR::CalculateEsIntegral(){

   Double_t Es = fKinematics.GetEs();
   Double_t Ep = fKinematics.GetEp();
   Double_t R  = fKinematics.GetR();
   Double_t th = fKinematics.GetTheta();

   Double_t min = GetEsMin(Ep,th); 
   Double_t max = Es - R*fDeltaE;

   if(min > max){
      std::cout << "[RADCOR::CalculateEsIntegral]: ERROR!  min greater than max! " << std::endl;
      std::cout << "                Es = " << Es << "\t" << "min = " << min << "\t" << "max = " << max << std::endl;
      exit(1);
   }

   EsFuncWrap funcWrap;
   funcWrap.fRADCOR = this;

   ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVE; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   double absErr                           = 1e100;   /// desired absolute Error 
   double relErr                           = 1e-02;   /// desired relative Error 
   unsigned int ncalls                     = 1.0e5;  /// number of calls (MC only)

   ROOT::Math::IntegratorOneDim  ig2(type,absErr,relErr,ncalls);
   ig2.SetFunction( funcWrap );

   Double_t AnsEs   = ig2.Integral(min,max);
   // Double_t AnsEs_2 = AdaptiveSimpson(&RADCOR::EsIntegrand,min,max,fEpsilon,fDepth);
   // std::cout << "Es: new int = " << AnsEs << "\t" << "old int = " << AnsEs_2 << std::endl; 

   return AnsEs;
}
//______________________________________________________________________________
Double_t RADCOR::CalculateEpIntegral(){

   Double_t Es = fKinematics.GetEs();
   Double_t Ep = fKinematics.GetEp();
   Double_t th = fKinematics.GetTheta();

   Double_t min     = Ep + fDeltaE; 
   Double_t max     = GetEpMax(Es,th);

   if(min > max){
      std::cout << "[RADCOR::CalculateEpIntegral]: ERROR!  min greater than max! " << std::endl;
      std::cout << "                Ep = " << Ep << "\t" << "min = " << min << "\t" << "max = " << max << std::endl;
      exit(1);
   }

   EpFuncWrap funcWrap;
   funcWrap.fRADCOR = this;

   ROOT::Math::IntegrationOneDim::Type type = ROOT::Math::IntegrationOneDim::kADAPTIVE; //kVEGAS,kADAPTIVE,kLEGENDRE,kADAPTIVESINGULAR
   double absErr                           = 1.e100; /// desired absolute Error 
   double relErr                           = 1.e-02; /// desired relative Error 
   unsigned int ncalls                     = 1.0e5;  /// number of calls (MC only)

   ROOT::Math::IntegratorOneDim  ig2(type,absErr,relErr,ncalls);
   ig2.SetFunction( funcWrap );

   Double_t AnsEp   = ig2.Integral(min,max);
   // Double_t AnsEp_2 = AdaptiveSimpson(&RADCOR::EpIntegrand,min,max,fEpsilon,fDepth);
   // std::cout << "Ep: new int = " << AnsEp << "\t" << "old int = " << AnsEp_2 << std::endl; 

   return AnsEp;
}
//______________________________________________________________________________
void RADCOR::SetRadiationLengths(const Double_t *X){

   Double_t Tb = X[0];        /// thickness before scattering in number of X0
   Double_t Ta = X[1];        /// thickness after scattering in number of X0

   /// For the peaking approximation...
   fKinematics.Sett_b(Tb);     
   fKinematics.Sett_a(Ta);
   /// For exact (internal) [always zero]  
   fKinematicsInt.Sett_b(0.);
   fKinematicsInt.Sett_a(0.);
   /// For exact (external)  
   fKinematicsExt.Sett_b(Tb);
   fKinematicsExt.Sett_a(Ta);

}
//______________________________________________________________________________
void RADCOR::SetTargetNucleus(const Nucleus& n){

   fTargetNucleus = n;

   auto A  = Double_t( fTargetNucleus.GetA() );
   auto Z  = Double_t( fTargetNucleus.GetZ() );
   Double_t MT = fTargetNucleus.GetMass(); 

   if(fUnits==1){
      // converts to MeV 
      MT *= 1E+3; 
   } 

   // peaking approx.  
   fKinematics.SetA(A);
   fKinematics.SetZ(Z);
   fKinematics.SetM_A(MT);
   // internal (exact) 
   fKinematicsInt.SetA(A);
   fKinematicsInt.SetZ(Z);
   fKinematicsInt.SetM_A(MT);
   // external (exact) 
   fKinematicsExt.SetA(A);
   fKinematicsExt.SetZ(Z);
   fKinematicsExt.SetM_A(MT);

   fspin2      = 2.*fTargetNucleus.GetSpin();
   fTargetName = fTargetNucleus.GetName(); 

   fUnpolXS->SetTargetNucleus(n); 
   fPolDiffXS->SetTargetNucleus(n); 

}
//______________________________________________________________________________
void RADCOR::SetUnits(Int_t u){

   fUnits = u; 
   fKinematics.SetUnits(u); 
   fKinematicsInt.SetUnits(u); 
   fKinematicsExt.SetUnits(u); 

   TString UnitName = ""; 
   switch(fUnits){
      case 0: 
         UnitName = "GeV";
         fhbar_c_sq = hbarc2_gev_nb;  
         break;
      case 1: 
         UnitName = "MeV"; 
         fhbar_c_sq = hbarc2_mev_pb;  
         break;
   }

   std::cout << "[RADCOR]: Energy units set to " << UnitName << std::endl;

}
//______________________________________________________________________________
void RADCOR::SetKinematics(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi){

   fKinematics.SetEs(Ebeam); 
   fKinematics.SetEp(Eprime); 
   fKinematics.SetTheta(theta); 
   fKinematics.SetPhi(phi); 
   CalculateCFACT();

}
//______________________________________________________________________________
void RADCOR::SetKinematicsForExactInternal(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi){

   /// We want an extra set of kinematics for the exact internally-radiated tail 
   fKinematicsInt.SetEs(Ebeam); 
   fKinematicsInt.SetEp(Eprime); 
   fKinematicsInt.SetTheta(theta); 
   fKinematicsInt.SetPhi(phi); 
}
//______________________________________________________________________________
void RADCOR::SetKinematicsForExactExternal(Double_t Ebeam,Double_t Eprime,Double_t theta,Double_t phi){

   /// Need to offset Ebeam and Eprime appropriately for energy loss 
   /// Tsai, SLAC-PUB-0848 (1971), pg 10 
   Double_t t_b     = fKinematicsExt.Gett_b(); 
   Double_t t_a     = fKinematicsExt.Gett_a(); 
   Double_t delta_s = Delta(Ebeam,t_b); 
   Double_t delta_p = Delta(Eprime,t_a); 
   Double_t Es      = Ebeam  - delta_s; 
   Double_t Ep      = Eprime + delta_p; 

   if(fDebug>0){
      std::cout << "[RADCOR::SetKinematicsForExactExternal]: " << "Energy loss terms: " << std::endl;
      std::cout << "delta_s = " << delta_s << std::endl;
      std::cout << "delta_p = " << delta_p << std::endl;
      std::cout << "Es = Ebeam  - delta_s = " << Ebeam  << " - " << delta_s << " = " << Es << std::endl;
      std::cout << "Ep = Eprime + delta_p = " << Eprime << " + " << delta_p << " = " << Ep << std::endl;
   }

   fKinematicsExt.SetEs(Es); 
   fKinematicsExt.SetEp(Ep); 
   fKinematicsExt.SetTheta(theta); 
   fKinematicsExt.SetPhi(phi); 

}
//______________________________________________________________________________
void RADCOR::ProcessStructureFunctions(Double_t x,Double_t Q2,Double_t *sf){

   Double_t F1=0,F2=0; 
   Nucleus::NucleusType Target = fTargetNucleus.GetType(); 

   if(fUnits==1){
      // convert back to GeV for structure functions 
      Q2 *= 1E-6; 
   }

   switch(Target){
      case Nucleus::kProton: 
         F1 = fUnpolSFs->F1p(x,Q2); 
         F2 = fUnpolSFs->F2p(x,Q2); 
         break;
      case Nucleus::kNeutron:
         F1 = fUnpolSFs->F1n(x,Q2); 
         F2 = fUnpolSFs->F2n(x,Q2); 
         break;
      case Nucleus::k3He:
         F1 = fUnpolSFs->F1He3(x,Q2); 
         F2 = fUnpolSFs->F2He3(x,Q2); 
         break;
      default:
         std::cout << "[RADCOR::ProcessStructureFunctions]: Invalid target!  Exiting..." << std::endl;
         exit(1);	
   }

   sf[0] = F1; 
   sf[1] = F2; 

}
//______________________________________________________________________________
void RADCOR::ProcessFormFactors(Double_t Q2,Double_t *ff){

   Double_t GE=0,GM=0; 

   Nucleus::NucleusType Target = fTargetNucleus.GetType(); 

   auto A = Double_t( fTargetNucleus.GetA() ); 
   auto Z = Double_t( fTargetNucleus.GetZ() ); 

   if(fUnits==1){
      // convert back to GeV for the evaluation of form factors
      Q2 *= 1E-6;    
   }

   switch(Target){
      case Nucleus::kProton:  
         GE = fFF->GEp(Q2); 
         GM = fFF->GMp(Q2); 
         break;
      case Nucleus::kNeutron: 
         GE = fFF->GEn(Q2); 
         GM = fFF->GMn(Q2); 
         break;
      case Nucleus::kDeuteron:  
         GE = fFF->GEd(Q2); 
         GM = fFF->GMd(Q2);
         break;
      case Nucleus::k3He:      
         /// NOTE: we use the fFFT form factor object here!  
         GE = fFFT->GEHe3(Q2); 
         GM = fFFT->GMHe3(Q2); 
         break;
      default: 
         GE = fFF->GE_A(Z,A,Q2); 
         GM = fFF->GM_A(Z,A,Q2);
         break;
   }

   if(fIsSmear){
      GE *= GetSmearFunc(Q2); 
      GM *= GetSmearFunc(Q2); 
   }

   ff[0] = GE; 
   ff[1] = GM; 

}
//______________________________________________________________________________
void RADCOR::ProcessQuasiElasticFormFactors(Double_t x,Double_t Q2,Double_t *ff){

   Double_t GE=0,GM=0; 

   Nucleus::NucleusType Target = fTargetNucleus.GetType(); 

   auto A = Double_t( fTargetNucleus.GetA() ); 
   auto Z = Double_t( fTargetNucleus.GetZ() ); 

   if(fUnits==1){
      // convert back to GeV for the evaluation of form factors
      Q2 *= 1E-6;    
   }

   switch(Target){
      case Nucleus::kProton:  
         GE = 0.; 
         GM = 0.; 
         break;
      case Nucleus::kNeutron: 
         GE = 0.; 
         GM = 0.; 
         break;
      case Nucleus::kDeuteron:  
         GE = fFFQE->GEdQE(x,Q2); 
         GM = fFFQE->GMdQE(x,Q2);
         break;
      case Nucleus::k3He:      
         GE = fFFQE->GEHe3QE(x,Q2); 
         GM = fFFQE->GMHe3QE(x,Q2); 
         break;
      default: 
         GE = 0.; 
         GM = 0.;
         break;
   }

   ff[0] = GE; 
   ff[1] = GM; 

}
//______________________________________________________________________________
Double_t RADCOR::GetX0(){

   Double_t Z = fKinematics.GetZ(); 

   // FIXME: Is this the *effective* radiation length for all materials??
   // T1  = (4.*N/fA)*alpha*r0*r0.  See Particle Data Book, Sec. 27.4.1
   // First term
   Double_t T1  = pow(716.408,-1.);
   // Second term  
   Double_t Z13 = pow(Z,-1./3.); 
   Double_t Z23 = pow(Z,-2./3.); 
   Double_t Lrad,Lradp; 
   if(Z==1){
      Lrad  = 5.31;
      Lradp = 6.144;  
   }else if(Z==2){
      Lrad  = 4.79;
      Lradp = 5.621;
   }else if(Z==3){
      Lrad  = 4.74; 
      Lradp = 5.805; 
   }else if(Z==4){
      Lrad  = 4.71;
      Lradp = 5.924; 
   }else if(Z>4){
      Lrad  = log(184.15*Z13);
      Lradp = log(1194.*Z23);
   }
   Double_t alpha = fine_structure_const; 
   Double_t a     = alpha*Z; 
   Double_t a2    = a*a; 
   Double_t a4    = a2*a2;
   Double_t a6    = a4*a2; 
   Double_t f     = a2*( ( 1./(1 + a2) ) + 0.20206 - 0.0369*a2 + 0.0083*a4 - 0.002*a6); 
   Double_t T2    = Z*Z*(Lrad - f) + Z*Lradp;
   // Put it together 
   Double_t arg = T1*T2;
   Double_t X0  = 1./arg; 
   return X0; 

}
//______________________________________________________________________________
Double_t RADCOR::GetPhi(Double_t v) const {
   // Shape of the Bremsstrahlung spectrum for small v (complete screening)
   return(1.0 - v + (3.0/4.0)*v*v); 
}
//______________________________________________________________________________
Double_t RADCOR::GetTr(Double_t Q2){

   // General terms
   Double_t b     = fKinematics.Getb(); 
   Double_t m     = fKinematics.Getm(); 
   Double_t m2    = m*m; 
   // Individual terms
   Double_t alpha = fine_structure_const; 
   Double_t T1    = (1.0/b)*(alpha/pi); 
   Double_t T2    = TMath::Log(Q2/m2) - 1.0; 
   // Put it all together 
   Double_t Tr    = T1*T2;

   //if(!fIsInternal) Tr = 0.;   // if no internal, set Tr = 0.  

   return Tr; 

}
//______________________________________________________________________________
Double_t RADCOR::GetEsMin(Double_t Ep,Double_t theta){

   Double_t num,den;
   Double_t thr    = theta;  
   Double_t M      = fKinematics.GetM();
   Double_t M_A    = fKinematics.GetM_A();
   Double_t M_pi   = fKinematics.GetM_pion();
   Double_t SIN    = TMath::Sin(thr/2.0); 
   Double_t SIN2   = SIN*SIN;  

   switch(fThreshold){
      case 0: // elastic threshold 
         num = Ep;
         den = 1. - (2.*Ep/M_A)*SIN2; 
         break; 
      case 1: // quasielastic threshold 
         num = Ep; 
         den = 1. - (2.*Ep/M)*SIN2; 
         break;
      case 2: // pion threshold 
         num  = M_pi*M_pi + 2.*M*M_pi + 2.*M*Ep;
         den  = 2.*M - 4.*Ep*SIN2; 
         break; 
      case 3: // custom threshold 
         // std::cout << "[RADCOR::GetEsMin]: Using DeltaM to compute threshold..." << std::endl;
         // num  = fDeltaM*fDeltaM + 2.*M*fDeltaM + 2.*M*Ep;
         // den  = 2.*M - 4.*Ep*SIN2;
         // this way is better...  
         num = Ep; 
         den = 1. - ( 2.*Ep/(M - fDeltaM) )*SIN2; 
         break;
      case 4: // custom threshold (elastic)  
         // num  = fDeltaM*fDeltaM + 2.*M_A*fDeltaM + 2.*M_A*Ep;
         // den  = 2.*M_A - 4.*Ep*SIN2;
         // this way is better...  
         num = Ep;
         den = 1. - ( 2.*Ep/(M_A - fDeltaM) )*SIN2; 
         break;
   }

   Double_t EsMin = num/den; 
   return EsMin; 

}
//______________________________________________________________________________
Double_t RADCOR::GetEpMax(Double_t Es,Double_t theta){

   Double_t num,den; 
   Double_t thr    = theta;  
   Double_t M      = fKinematics.GetM();
   Double_t M_A    = fKinematics.GetM_A();
   Double_t M_pi   = fKinematics.GetM_pion();
   Double_t SIN    = TMath::Sin(thr/2.0); 
   Double_t SIN2   = SIN*SIN; 

   switch(fThreshold){
      case 0: // elastic threshold 
         num = Es;
         den = 1. + (2.*Es/M_A)*SIN2; 
         break; 
      case 1: // quasielastic threshold 
         num = Es; 
         den = 1. + (2.*Es/M)*SIN2; 
         break;
      case 2: // pion threshold 
         num = 2.*M*Es - 2.*M*M_pi - M_pi*M_pi;
         den = 2.*M + 4.*Es*SIN2; 
         break; 
      case 3: // custom threshold  
         // std::cout << "[RADCOR::GetEpMax]: Using DeltaM to compute threhsold..." << std::endl;
         // num = 2.*M*Es - 2.*M*fDeltaM - fDeltaM*fDeltaM;
         // den = 2.*M + 4.*Es*SIN2;
         // this way is better... 
         num = Es; 
         den = 1. + ( 2.*Es/(M - fDeltaM) )*SIN2; 
         break; 
      case 4: // custom threshold (elastic)  
         // num = 2.*M_A*Es - 2.*M_A*fDeltaM - fDeltaM*fDeltaM;
         // den = 2.*M_A + 4.*Es*SIN2;
         // this way is better... 
         num = Es;
         den = 1. + ( 2.*Es/(M_A - fDeltaM) )*SIN2; 
         break; 
   }

   Double_t EpMax = num/den; 
   return EpMax; 

}
//______________________________________________________________________________
void RADCOR::CalculateCFACT(){

   // General terms 
   Double_t Es     = fKinematics.GetEs();
   Double_t Ep     = fKinematics.GetEp();
   Double_t Q2     = fKinematics.GetQ2(); 
   Double_t b      = fKinematics.Getb();
   Double_t Xi     = fKinematics.Getxi();
   Double_t Tb     = fKinematics.Gett_b();
   Double_t Ta     = fKinematics.Gett_a();
   Double_t R      = fKinematics.GetR();
   Double_t Tr     = GetTr(Q2); 
   Double_t FTilde = GetFTilde(Q2);
   if(!fIsExternal){
      Tb = 0.;
      Ta = 0.;
      Xi = 0.; 
   } 
   // First term
   Double_t Term1  = R*fDeltaE/Es;
   Double_t Exp1   = b*(Tb+Tr);  
   Double_t T1     = TMath::Power(Term1,Exp1); 
   // Second term 
   Double_t Term2  = fDeltaE/Ep; 
   Double_t Exp2   = b*(Ta+Tr); 
   Double_t T2     = TMath::Power(Term2,Exp2);    
   // Third term
   Double_t num    = Xi/fDeltaE; 
   Double_t denom  = 1.0 - b*(Ta+Tb+2.0*Tr); 
   Double_t T3     = 1.0 - num/denom; 
   // Put it all together 
   fCFACT          = FTilde*T1*T2*T3; 

}
//______________________________________________________________________________
Double_t RADCOR::GetFTilde(Double_t q2){

   /// Phys.Rev.D 12,1884 (A44)
   /// WARNING: q2, NOT Q2!
   Double_t thr   = fKinematics.GetTheta();
   Double_t Es    = fKinematics.GetEs();
   Double_t Ep    = fKinematics.GetEp();
   Double_t m     = fKinematics.Getm();
   Double_t b     = fKinematics.Getb();
   Double_t Tb    = fKinematics.Gett_b();
   Double_t Ta    = fKinematics.Gett_a();
   Double_t alpha = fine_structure_const; 
   Double_t SIN   = TMath::Sin(thr/2.); 
   Double_t SIN2  = SIN*SIN; 
   Double_t COS2  = 1. - SIN2; 

   if(!fIsExternal){
   	Tb = 0.;
   	Ta = 0.;
   } 

   Double_t T1=0,T2=0,T3=0; 
   Double_t T0 = 1. + 0.5772*b*(Ta + Tb);
   if(fIsInternal){
      T1    = 2*(alpha/pi)*( (-14/9.) + (13./12.)*TMath::Log(q2/(m*m)) );
      T2    = -0.5*(alpha/pi)*TMath::Log(Es/Ep)*TMath::Log(Es/Ep);
      T3    = (alpha/pi)*( pi*pi/6.- GetSpence(COS2) );
   }
   Double_t F      = T0 + T1 + T2 + T3;
   Double_t spence = GetSpence(COS2); 
   Double_t fbar_1 = 2.*(alpha/pi)*(-14./9.) + (alpha/pi)*( pi*pi/6. - GetSpence(COS2) ); 
   Double_t fbar_2 = 2.*(alpha/pi)*(13./12.); 
   Double_t fbar_3 = (-1./2.)*(alpha/pi); 

   if(fDebug>5){ 
      std::cout << "-------- GetFTilde -------" << std::endl;
      std::cout << "T0     = " << T0     << std::endl;
      std::cout << "T1     = " << T1     << std::endl;
      std::cout << "T2     = " << T2     << std::endl;
      std::cout << "T3     = " << T3     << std::endl;
      std::cout << "COS2   = " << COS2   << std::endl;
      std::cout << "SIN2   = " << SIN2   << std::endl;
      std::cout << "spence = " << spence << std::endl;
      std::cout << "fbar1  = " << fbar_1 << std::endl;
      std::cout << "fbar2  = " << fbar_2 << std::endl;
      std::cout << "fbar3  = " << fbar_3 << std::endl;
      std::cout << "F      = " << F      << std::endl;
   }
   return F;
}
//______________________________________________________________________________
Double_t RADCOR::GetFTildeExactInternal(Double_t q2){

   /// Phys.Rev.D 12,1884 (A44)
   /// WARNING: q2, NOT Q2!
   Double_t thr   = fKinematicsInt.GetTheta();
   Double_t Es    = fKinematicsInt.GetEs();
   Double_t Ep    = fKinematicsInt.GetEp();
   Double_t m     = fKinematicsInt.Getm();
   Double_t b     = fKinematicsInt.Getb();
   Double_t Tb    = fKinematicsInt.Gett_b();
   Double_t Ta    = fKinematicsInt.Gett_a();
   Double_t alpha = fine_structure_const; 
   Double_t SIN   = TMath::Sin(thr/2.); 
   Double_t SIN2  = SIN*SIN; 
   Double_t COS2  = 1. - SIN2; 

   if(!fIsExternal){
      Tb = 0.;
      Ta = 0.;
   } 

   Double_t T1=0,T2=0,T3=0; 
   Double_t T0    = 1. + 0.5772*b*(Ta + Tb);
   if(fIsInternal){
      T1     = 2*(alpha/pi)*( (-14/9.) + (13./12.)*TMath::Log(q2/(m*m)) );
      T2     = -0.5*(alpha/pi)*TMath::Log(Es/Ep)*TMath::Log(Es/Ep);
      T3     = (alpha/pi)*( pi*pi/6.- GetSpence(COS2) );
   }
   Double_t F     = T0 + T1 + T2 + T3;

   if(fDebug>5){ 
      std::cout << "-------- GetFTildeExactInternal -------" << std::endl;
      std::cout << "T0 = " << T0 << std::endl;
      std::cout << "T1 = " << T1 << std::endl;
      std::cout << "T2 = " << T2 << std::endl;
      std::cout << "T3 = " << T3 << std::endl;
   }
   return F;
}
//______________________________________________________________________________
Double_t RADCOR::GetSpence(Double_t x){

   // Double_t phi = -TMath::DiLog(-x);
   // Checking against ROSETAIL and RADCOR (fortran), this is the
   // form that gives consistent results: 
   Double_t phi = TMath::DiLog(x);
   return phi; 

}
//________________________________________________________
Double_t RADCOR::GetSmearFunc(Double_t Q2){

   Double_t Q,pf,pf2,f;
   Double_t CONV = 1E+3;

   pf  = 164.0/CONV;     // fermi momentum in GeV 
   pf2 = pf*pf;
   Q   = TMath::Sqrt(Q2)/pf;

   if(Q2 < 4.*pf2){
      f = 0.75*Q - (1./16.)*TMath::Power(Q,3.);
   }else{
      f = 1.;
   }

   return f;

}
//______________________________________________________________________________
Double_t RADCOR::AdaptiveSimpson(Double_t (RADCOR::*f)(Double_t &) ,Double_t A,Double_t B,
      Double_t epsilon,Int_t Depth) {
   // Adaptive Simpson's Rule
   Double_t C   = (A + B)/2.0;
   Double_t H   = B - A;
   Double_t fA  = (this->*f)(A);
   Double_t fB  = (this->*f)(B);
   Double_t fC  = (this->*f)(C);
   Double_t S   = (H/6.0)*(fA + 4.0*fC + fB);
   Double_t arg = AdaptiveSimpsonAux(f,A,B,epsilon,S,fA,fB,fC,Depth);
   return arg;
}
//______________________________________________________________________________
Double_t RADCOR::AdaptiveSimpsonAux(Double_t (RADCOR::*f)(Double_t &) ,Double_t A,Double_t B,
      Double_t epsilon,Double_t S,Double_t fA,Double_t fB,Double_t fC,Int_t bottom) {
   // Recursive auxiliary function for AdaptiveSimpson() function
   Double_t C      = (A + B)/2.0;
   Double_t H      = B - A;
   Double_t D      = (A + C)/2.0;
   Double_t E      = (C + B)/2.0;
   Double_t fD     = (this->*f)(D);
   Double_t fE     = (this->*f)(E);
   Double_t Sleft  = (H/12.0)*(fA + 4.0*fD + fC);
   Double_t Sright = (H/12.0)*(fC + 4.0*fE + fB);
   Double_t S2 = Sleft + Sright;
   if (bottom <= 0 || fabs(S2 - S) <= 15.0*epsilon){
      return S2 + (S2 - S)/15;
   }
   Double_t arg = AdaptiveSimpsonAux(f,A,C,epsilon/2.0,Sleft, fA,fC,fD,bottom-1) +
      AdaptiveSimpsonAux(f,C,B,epsilon/2.0,Sright,fC,fB,fE,bottom-1);
   return arg;
}
//______________________________________________________________________________
// void RADCOR::MCIntegral(Int_t Method,const Int_t &NDim,Double_t (RADCOR::*F)(Double_t *),
//                              Int_t (RADCOR::*H)(Double_t *),
//                              Double_t *min,Double_t *max,Double_t &res,Double_t &err){
// 
//    switch(Method){
//       case 0: // Sample mean
//          break;
//       case 1: // Importance sampling 
//          break;
//       case 2: // Accept/reject 
//          break;
//       default:
//          std::cout << "[RADCOR::MCIntegral]: Invalid integration method!  Exiting..." << std::endl;
//          exit(1);
//    }
// 
// }
//______________________________________________________________________________
void RADCOR::MCAcceptReject(const Int_t &NDim,Double_t (RADCOR::*F)(Double_t *),
      Double_t *min,Double_t *max,
      Int_t MCEvents,Int_t MCBins,Double_t &res,Double_t &err){

   /// Monte Carlo integration: Accept/reject method 
   /// F is the integrand [F(xi,...,xn)]
   /// H is the boundary [H(xi,...,xn)] (to implement variable limits)
   /// (called inside F)  
   /// MCEvents: number of monte carlo events 
   /// MCBins:   number of MC bins (in each dimension)  

   // Int_t Bin     = MCBins;
   // FIXME: How to do we make the proper declaration? 
   // if(NDim==1) TH1F *hist = new TH1F("hist","hist",Bin,&min[0],&max[0]);
   // if(NDim==2) TH2F *hist = new TH1F("hist","hist",Bin,&min[0],&max[0],Bin,&min[1],&max[1]);
   // if(NDim==3) TH3F *hist = new TH3F("hist","hist",Bin,&min[0],&max[0],Bin,&min[1],&max[1],Bin,&min[2],&max[2]);

   // Int_t NumBins = hist->GetSize();

   // Double_t Range[NDim];
   // Double_t boundVol=1.; 
   // for(int i=0;i<NDim;i++){
   // 	Range[i]  = max[i]-min[i];
   // 	boundVol *= Range[i];  
   // }

   // FIXME: how do we handle NDim > 3?? 
   // delete hist;
   res = 0.;
   err = 0.; 

   // fill histogram 
   // Int_t x_bin,y_bin,z_bin;
   // Double_t x=0,y=0,z=0;
   // Double_t f=0;
   // for(int i=1;i<=NumBins;i++){        // bin 0 = underflow; bin NumBins+1 = overflow 
   //         // use the global bin i to find the x, y and z local bins
   //         hist->GetBinXYZ(i,x_bin,y_bin,z_bin);
   //         // get value at the center of local bin (global = local for 1D) 
   //         if(NDim==1){
   // 		x = hist->GetXaxis()->GetBinCenter(x_bin);
   //         }else if(NDim==2){
   // 		x = hist->GetXaxis()->GetBinCenter(x_bin);
   // 		y = hist->GetYaxis()->GetBinCenter(y_bin);
   //         }else if(NDim==3){
   // 		x = hist->GetXaxis()->GetBinCenter(x_bin);
   // 		y = hist->GetYaxis()->GetBinCenter(y_bin);
   // 		z = hist->GetZaxis()->GetBinCenter(z_bin);
   //         }
   // 	// if the bin is valid, compute the function; otherwise, it's zero 
   //         if( (x_bin<=Bin)&&(x_bin>0)&&(y_bin<=Bin)&&(y_bin>0)&&(z_bin<=Bin)&&(z_bin>0) ){
   //                 f = (this->*F)(x,y,z);
   //         }else{
   //                 f = 0.;
   //         }
   //         std::cout << "x = " << Form("%.3E",x) << "\t" 
   //                   << "y = " << Form("%.3E",y) << "\t" 
   //                   << "z = " << Form("%.3E",z) << "\t" 
   //                   << "F = " << Form("%.3E",f) << std::endl;
   //         // fill the histogram 
   //         hist->SetBinContent(i,f);
   // }

   // Double_t MAX    = hist->GetMaximum();
   // Double_t volume = boundVol*MAX;

   // Double_t N      = (double)MCEvents;
   // Double_t n      = 0.; // number of successful samplings below the curve f(x)  

   // f=0;
   // Int_t k=0,h=0;
   // Double_t sum=0,sum2=0;
   // Double_t r=0;
   // Double_t R[3] = {0.,0.,0.}; 
   // fWatch->Start();
   // for(int i=0;i<MCEvents;i++){
   //         R[0]  = min[0] + xRange*gRandom->Rndm();  // number between [xmin,xmax]  
   //         R[1]  = min[1] + yRange*gRandom->Rndm();  // number between [ymin,ymax]  
   //         R[2]  = min[2] + zRange*gRandom->Rndm();  // number between [zmin,zmax]  
   //         r     = MAX*gRandom->Rndm();            // number between [0,MAX], where MAX is the maximum value of f(x,y,z) 
   //         // get the bin for each thrown variable 
   //         x_bin = hist->GetXaxis()->FindBin(R[0]);
   //         y_bin = hist->GetYaxis()->FindBin(R[1]);
   //         z_bin = hist->GetZaxis()->FindBin(R[2]);
   //         // get the global bin 
   //         k     = hist->GetBin(x_bin,y_bin,z_bin);
   // 	// is the bin in the boundary?
   //         h     = (this->*H)(R);          
   //         if(h==0) continue; 
   //         // compute the integrand  
   //         f     = hist->GetBinContent(k)*Double_t(h);
   //         if(r<f){                                // is the value of second random number below the curve at rx? 
   //                 n    += 1.;                     // successful trial, increment n.
   //                 sum  += f;
   //                 sum2 += f*f;
   //         }
   // }
   // // compute the time of the integration  
   // Double_t time = fWatch->RealTime();
   // TString Units = Form("s");
   // if( (time>=minute)&&(time<hour) ){
   // 	time *= 1./minute;
   //         Units = Form("min(s)");
   // }else if(time>=hour){
   // 	time *= 1./hour;
   //         Units = Form("hr(s)");
   // }
   // // FIXME: delete the histogram?? 
   // delete hist; 
   // // compute average of f, f2 for accepted events
   // Double_t avg_f    = sum/N;
   // Double_t avg_f2   = sum2/N;
   // // compute the area of the region 
   // res               = volume*(n/N);  // total area of rectangle * pct successful trials = area under the curve  
   // err               = volume*TMath::Sqrt( (avg_f2-avg_f*avg_f)/N );         

   // std::cout << "INTEGRAL:     " << res << " +/- " << err << std::endl;
   // std::cout << "ELAPSED TIME: " << time << " " << Units << std::endl;

}
//______________________________________________________________________________
void RADCOR::MCImportanceSampling(const Int_t &NDim,Double_t (RADCOR::*F)(Double_t *),
      Double_t (RADCOR::*P)(Double_t *,Double_t *,Double_t *), 
      Double_t (RADCOR::*R)(Int_t,Double_t *,Double_t *), 
      Double_t *min,Double_t *max,
      Int_t MCEvents,Double_t &res,Double_t &err){

   /// Monte Carlo integration: Importance sampling method in N dimensions  
   /// integral: int [ F(xi,...xn)*H(xi,...,xn) prod_sum dxi...dxn ]
   /// Let X = vec{x}  
   /// F(X) = function we want to integrate 
   /// H(X) = imposes boundaries on F according to the thrown variables x, y and z
   ///        (called inside of F(X)) 
   /// P(i,X,min,max) = probability distribution for each xi 
   ///              i = ith indexed variable 
   ///              X = vector of random numbers 
   ///            min = minima for each xi 
   ///            max = maxima for each xi 
   /// R(i,min,max) = probability distribution to *throw* xi 
   ///            i = ith indexed variable 
   ///          min = minima for each xi 
   ///          max = maxima for each xi         

   Double_t Range[NDim],r[NDim];
   Double_t volume=1.;  
   for(int i=0;i<NDim;i++){
      Range[i] = max[i] - min[i];
      volume  *= Range[i]; 
   }

   if(volume==0){
      std::cout << "WHAT THE... volume = 0?" << std::endl;
      exit(1);
   }

   Double_t f=0,FH=0,ptot=1.;
   Double_t sum=0,sum2=0;
   // throw random events in xi,...,xn and find F(xi,...,xn)  
   fWatch->Start();  
   for(int i=0;i<MCEvents;i++){
      // std::cout << "EVENT: " << i+1 << std::endl;
      for(int j=0;j<NDim;j++){
         r[j]   = (this->*R)(j,min,max);
         // std::cout << "j = " << j << "\t" << "r = " << r[j] << std::endl;
      }
      ptot = (this->*P)(r,min,max); 
      // for(int j=0;j<NDim;j++){
      // 	p[j]  = (this->*P)(r,min,max);
      // 	ptot *= p[j]; 
      // }
      f     = (this->*F)(r);          
      FH    = f/ptot;  
      // std::cout << "H  = " << h << std::endl;
      // std::cout << "F  = " << f << std::endl;
      // std::cout << "FH = " << FH << std::endl;
      std::cout << "--------------------------------------" << std::endl;
      sum  += FH;
      sum2 += FH*FH; 
      ptot  = 1.;
   }
   // compute time of calculation 
   Double_t time = fWatch->RealTime();
   TString Units = Form("s");
   if( (time>=minute)&&(time<hour) ){
      time *= 1./minute;
      Units = Form("min(s)");
   }else if(time>=hour){
      time *= 1./hour;
      Units = Form("hr(s)");
   }
   // compute average of F, F^2  
   auto N        = Double_t(MCEvents);
   Double_t avg_f    = sum/N;
   Double_t avg_f2   = sum2/N;
   // compute integral 
   res               = avg_f;
   // compute the error 
   Double_t sarg     = (avg_f2-avg_f*avg_f)/N;
   err               = volume*TMath::Sqrt(sarg);

   std::cout << "INTEGRAL:     " << Form("%.3E",res ) << " +/- " << Form("%.3E",err)   << std::endl; 
   std::cout << "ELAPSED TIME: " << Form("%.3f",time) << " "     << Units << std::endl;

}
//______________________________________________________________________________
void RADCOR::MCSampleMean(const Int_t &NDim,Double_t (RADCOR::*F)(Double_t),
                                Double_t *min,Double_t *max,
                                Int_t MCEvents,Double_t &res,Double_t &err){

   /// Monte Carlo integration: sample mean method in N dimensions  
   /// integral: int [ F(xi,...xn)*H(xi,...,xn) prod_sum dxi...dxn ] 
   /// F(x) = function we want to integrate 
   /// H(x) = imposes boundaries on F according to the thrown variables x, y and z 
   ///        (called inside F(x))  

   Double_t Range[NDim],r[NDim];
   Double_t volume=1.;  
   for(int i=0;i<NDim;i++){
      Range[i] = max[i] - min[i];
      volume *= Range[i]; 
   }

   if(volume==0){
      std::cout << "[RADCOR::MCSampleMean]: volume = 0?" << std::endl;
      exit(1);
   }

   Double_t f=0,FH=0,R=0;
   Double_t sum=0,sum2=0;
   // throw random events in xi,...,xn and find F(xi,...,xn)  
   fWatch->Start();  
   for(int i=0;i<MCEvents;i++){
      // std::cout << "EVENT: " << i+1 << std::endl;
      for(int j=0;j<NDim;j++){
         r[j]   = min[j] + Range[j]*gRandom->Rndm();
         // std::cout << "j = " << j << "\t" << "r = " << r[j] << std::endl;
      }
      R     = r[0]; // FIXME: There's got to be a way around this...   
      f     = (this->*F)(R);          
      FH    = f;  
      // std::cout << "H  = " << h << std::endl;
      // std::cout << "F  = " << f << std::endl;
      // std::cout << "FH = " << FH << std::endl;
      // std::cout << "--------------------------------------" << std::endl;
      sum  += FH;
      sum2 += FH*FH; 
   }
   // compute time of calculation 
   Double_t time = fWatch->RealTime();
   TString Units = Form("s");
   if( (time>=minute)&&(time<hour) ){
      time *= 1./minute;
      Units = Form("min(s)");
   }else if(time>=hour){
      time *= 1./hour;
      Units = Form("hr(s)");
   }
   // compute average of F, F^2  
   auto N        = Double_t(MCEvents);
   Double_t avg_f    = sum/N;
   Double_t avg_f2   = sum2/N;
   // compute integral 
   res               = volume*avg_f;
   err               = volume*TMath::Sqrt( (avg_f2-avg_f*avg_f)/N );

   // std::cout << "INTEGRAL:     " << Form("%.3E",res ) << " +/- " << Form("%.3E",err)   << std::endl; 
   // std::cout << "ELAPSED TIME: " << Form("%.3f",time) << " "     << Units << std::endl;

}
//______________________________________________________________________________
void RADCOR::MCSampleMean(const Int_t &NDim,Double_t (RADCOR::*F)(Double_t *),
                                Double_t *min,Double_t *max,
                                Int_t MCEvents,Double_t &res,Double_t &err){

   /// Monte Carlo integration: sample mean method in N dimensions  
   /// integral: int [ prod_sum F(xi,...xn)*H(xi,...,xn) dxi...dxn ] 
   /// F(x) = function we want to integrate 
   /// H(x) = imposes boundaries on F according to the thrown variables x, y and z
   ///        (called inside F(x))  

   Double_t Range[NDim],r[NDim];
   Double_t volume=1.;  
   for(int i=0;i<NDim;i++){
      Range[i] = max[i] - min[i];
      volume  *= Range[i]; 
   }

   if(volume==0){
      std::cout << "[RADCOR::MCSampleMean]: volume = 0?!  Exiting..." << std::endl;
      exit(1);
   }

   Double_t f=0,FH=0;
   Double_t sum=0,sum2=0;
   // throw random events in xi,...,xn and find F(xi,...,xn)  
   fWatch->Start();  
   for(int i=0;i<MCEvents;i++){
      // std::cout << "EVENT: " << i+1 << std::endl;
      for(int j=0;j<NDim;j++){
         r[j]   = min[j] + Range[j]*gRandom->Rndm();
         // std::cout << "j = " << j << "\t" << "r = " << r[j] << std::endl;
      }
      f     = (this->*F)(r);          
      FH    = f;  
      // std::cout << "H  = " << h << std::endl;
      // std::cout << "F  = " << f << std::endl;
      // std::cout << "FH = " << FH << std::endl;
      // std::cout << "--------------------------------------" << std::endl;
      sum  += FH;
      sum2 += FH*FH; 
   }
   // compute time of calculation 
   Double_t time = fWatch->RealTime();
   TString Units = Form("s");
   if( (time>=minute)&&(time<hour) ){
      time *= 1./minute;
      Units = Form("min(s)");
   }else if(time>=hour){
      time *= 1./hour;
      Units = Form("hr(s)");
   }
   // compute average of F, F^2  
   auto N        = Double_t(MCEvents);
   Double_t avg_f    = sum/N;
   Double_t avg_f2   = sum2/N;
   // compute integral 
   res               = volume*avg_f;
   err               = volume*TMath::Sqrt( (avg_f2-avg_f*avg_f)/N );

   // std::cout << "INTEGRAL:     " << Form("%.3E",res ) << " +/- " << Form("%.3E",err)   << std::endl; 
   // std::cout << "ELAPSED TIME: " << Form("%.3f",time) << " "     << Units << std::endl;

}
//______________________________________________________________________________
Int_t RADCOR::Boundary(Double_t *x){

   /// Boundary function for external tail integrand (elastic) 
   /// Defined by Ep < Ep' < EpMax(Es') 
   /// For completeness, we use the lower limit as well.

   Double_t EpPrime        = x[0]; 
   Double_t EsPrime        = x[1]; 
   // Double_t tPrime         = x[2]; 
   // Double_t costhkPrime    = x[3]; 
   Double_t Ep             = fKinematicsExt.GetEp(); 
   Double_t th             = fKinematicsExt.GetTheta(); 
   Double_t EpMax          = GetEpMax(EsPrime,th); 

   Int_t h = 0; 
   if( (EpPrime>Ep)&&(EpPrime<EpMax) ){
      h = 1;
   }

   return h; 

}
//______________________________________________________________________________
Int_t RADCOR::Boundary_Inelastic_1(Double_t *x){

   /// Boundary function for external tail integrand (inelastic) 
   /// Defined by Ep < Ep' < EpMax(Es')  
   /// For completeness, we use the lower limit as well.

   Double_t EpPrime   = x[0]; 
   Double_t EsPrime   = x[1]; 
   // Double_t tPrime    = x[2]; 
   Double_t Ep        = fKinematics.GetEp(); 
   Double_t th        = fKinematics.GetTheta(); 
   Double_t EpMin     = Ep; 
   Double_t EpMax     = GetEpMax(EsPrime,th); 

   Int_t h            = 0; 
   //std::cout << " EpPrime = " << EpPrime << std::endl;
   //std::cout << " EsPrime = " << EsPrime << std::endl;
   //std::cout << " EpMin = " << EpMin << std::endl;
   //std::cout << " EpMax = " << EpMax << std::endl;
   if( (EpPrime>EpMin)&&(EpPrime<EpMax) ){
      h = 1;
   }
   return h; 
}
//______________________________________________________________________________
Int_t RADCOR::Boundary_Inelastic_2(Double_t *x){

   /// Boundary function for external tail integrand (inelastic) 
   /// Defined by Ep < Ep' < EpMax(Es') and Delta < omega' < omega_max(costhk')  
   /// For completeness, we use the lower limit as well.

   Double_t EpPrime      = x[0]; 
   Double_t EsPrime      = x[1]; 
   // Double_t tPrime       = x[2]; 
   Double_t costhkPrime  = x[3]; 
   Double_t omegaPrime   = x[4]; 
   Double_t Ep           = fKinematicsExt.GetEp(); 
   Double_t th           = fKinematicsExt.GetTheta(); 

   Double_t EpMin    = Ep; 
   Double_t EpMax    = GetEpMax(EsPrime,th); 
   Double_t omegaMin = fDelta; 
   Double_t omegaMax = GetOmegaMax(costhkPrime); 

   Int_t h           = 0; 
   if( (EpPrime>EpMin)&&(EpPrime<EpMax)&&(omegaPrime>omegaMin)&&(omegaPrime<omegaMax) ){
      h = 1;
   }

   return h; 

}
//______________________________________________________________________________
Int_t RADCOR::Boundary_Internal(Double_t *x){
   /// Boundary function for elastic internal tail integrand
   return 1; 
}
//______________________________________________________________________________
Int_t RADCOR::Boundary_Internal_Inelastic(Double_t *x){
   /// Boundary function for inelastic internal tail integrand
   /// Defined for delta < omega < omega_max
   /// See eq B8 of Mo & Tsai, Rev Mod Phys 41, 205 (1969)  
   Int_t h=0; 
   Double_t costhkPrime = x[0];
   Double_t omegaPrime  = x[1];
   Double_t omegaMin    = fDelta; 
   Double_t omegaMax    = GetOmegaMax(costhkPrime); 

   if( (omegaPrime>omegaMin)&&(omegaPrime<omegaMax) ){
      h = 1;
   }

   return h; 
}
//______________________________________________________________________________
Double_t RADCOR::ProbabilityDist(Double_t *x,Double_t *min,Double_t *max){

   /// p(x1,x2,x3,x4) = exp(-x1)exp(-x2)exp(-x3)exp(x4) 
   /// x1 = Ep' 
   /// x2 = Es'
   /// x3 = t 
   /// x4 = cos(thk) 

   // function normalization  
   Double_t num  = 1.; 
   // for x1, x2, x3 
   Double_t T[4]; 
   T[0] = TMath::Exp(-min[0]) - TMath::Exp(-max[0]);  
   T[1] = TMath::Exp(-min[1]) - TMath::Exp(-max[1]);  
   T[2] = TMath::Exp(-min[2]) - TMath::Exp(-max[2]);  
   // for x4 
   T[3] = TMath::Exp(max[3]) - TMath::Exp(min[3]);  

   Double_t den = 1.; 
   for(Int_t j=0;j<4;j++){
      den *= T[j]; 
   } 
   Double_t A   = num/den; 
   Double_t arg = TMath::Exp(-x[0])*TMath::Exp(-x[1])*TMath::Exp(-x[2])*TMath::Exp(x[3]); 
   Double_t P   = A*arg; 

   if(P>1){
      std::cout << "PROBABILITY DISTRIBUTION" << std::endl;
      std::cout << "A  = " << A << std::endl;
      for(Int_t j=0;j<4;j++){
         std::cout << "x" << j+1 << " = " << x[j] << std::endl;  
      }
      std::cout << "arg = " << arg << std::endl; 
      std::cout << "P  = " << P << std::endl;
   }
   return P; 

}
//______________________________________________________________________________
Double_t RADCOR::RandomVariable(Int_t i,Double_t *min,Double_t *max){

   /// Get a random variable corresponding to the probability distribution:  
   /// p(x1,x2,x3,x4) = exp(-x1)exp(-x2)exp(-x3)exp(x4)  
   /// x1 = Ep' 
   /// x2 = Es'
   /// x3 = t 
   /// x4 = cos(thk) 

   Double_t r   = gRandom->Rndm(); // random number between 0 and 1 

   // function normalization  
   Double_t num  = 1.; 
   // for x1, x2, x3: f(x) = exp(-x), we switch the order here to get rid of (-) signs from integration...  
   Double_t T[4]; 
   T[0] = TMath::Exp(-min[0]) - TMath::Exp(-max[0]);  
   T[1] = TMath::Exp(-min[1]) - TMath::Exp(-max[1]);  
   T[2] = TMath::Exp(-min[2]) - TMath::Exp(-max[2]);  
   // for x4: f(x) = exp(x)  
   T[3] = TMath::Exp(max[3]) - TMath::Exp(min[3]);  

   Double_t den = 1.; 
   for(Int_t j=0;j<4;j++){
      den *= T[j]; 
   } 

   Double_t A = num/den; 

   Double_t arg=0,arg1=0,arg2=0; 

   if(i==0){
      num  = (-1.)*r/A; 
      den  = T[1]*T[2]*T[3];
      arg1 = num/den; 
      arg2 = TMath::Exp(-min[0]);
      arg  = (-1.)*TMath::Log( arg1 + arg2 ); 
   }else if(i==1){
      num  = (-1.)*r/A; 
      den  = T[0]*T[2]*T[3];
      arg1 = num/den; 
      arg2 = TMath::Exp(-min[1]);
      arg  = (-1.)*TMath::Log( arg1 + arg2 ); 
   }else if(i==2){
      num  = (-1.)*r/A; 
      den  = T[0]*T[1]*T[3];
      arg1 = num/den; 
      arg2 = TMath::Exp(-min[2]);
      arg  = (-1.)*TMath::Log( arg1 + arg2 ); 
   }else if(i==3){
      num  = (1.)*r/A; 
      den  = T[0]*T[1]*T[2];
      arg1 = num/den; 
      arg2 = TMath::Exp(min[3]);
      arg  =  TMath::Log( arg1 + arg2 ); 
   }

   Int_t IsNotANumber = TMath::IsNaN(arg);

   if(IsNotANumber){
      std::cout << "WHAT THE..." << " not a number?!" << std::endl;
      std::cout << "r = "   << r << std::endl;
      std::cout << "A = "   << A << std::endl;
      std::cout << "arg1 = " << arg1 << std::endl;
      std::cout << "arg2 = " << arg2 << std::endl;
      std::cout << "variable index: " << i << std::endl;
      std::cout << "min = " << min[i] << std::endl;
      std::cout << "max = " << max[i] << std::endl;
      exit(1); 
   }

   return arg; 

}
//______________________________________________________________________________
void RADCOR::BinarySearch(std::vector<Double_t> array,Double_t key,Int_t &lowerbound,Int_t &upperbound){

   Int_t comparisonCount = 1;    //count the number of comparisons (optional)
   Int_t n               = array.size();
   lowerbound            = 0;
   upperbound            = n-1;

   // To start, find the subscript of the middle position.
   Int_t position = ( lowerbound + upperbound) / 2;

   while((array[position] != key) && (lowerbound <= upperbound)){
      comparisonCount++;
      if (array[position] > key){
         // decrease position by one.
         upperbound = position - 1;
      }else{
         // Else, increase position by one.
         lowerbound = position + 1;
      }
      position = (lowerbound + upperbound) / 2;
   }

   Double_t lo=0,hi=0,mid;
   Int_t dump = lowerbound;

   if (lowerbound <= upperbound){
      // std::cout << "[BinarySearch]: The number was found in array subscript " << position << std::endl; 
      // std::cout << "                The binary search found the number after " << comparisonCount << " comparisons." << std::endl;             
      // lo  = array[lowerbound];
      // hi  = array[upperbound];
      // mid = array[position]  
      // if(lo==hi){
         lowerbound = position; 
         upperbound = position;  
      // }
   }else{
      lowerbound = upperbound;
      upperbound = dump;
      // to safeguard against values that are outside the boundaries of the grid 
      if(upperbound>=n){
         upperbound = n-1;
         lowerbound = n-2;
      }
      if(upperbound==0){
         lowerbound = 0;
         upperbound = 1;
      }
      lo   = array[lowerbound];
      hi   = array[upperbound];
      // std::cout << "[BinarySearch]: Sorry, the number is not in this array.  The binary search made " << comparisonCount << " comparisons." << std::endl;
      // std::cout << "                Target = "         << key << std::endl;
      // std::cout << "                Bounding values: " << std::endl;
      // std::cout << "                low  = "           << lo << std::endl;
      // std::cout << "                high = "           << hi << std::endl;
   }



}
//______________________________________________________________________________
Double_t RADCOR::FTCS(Double_t Es,Double_t Ep){

   // mimics the fortran FTCS subroutine
   // linear interpolation in Es; parabolic interpolation in Ep  

   Int_t xLo=0,xHi=0;
   BinarySearch(fEsGridData,Es,xLo,xHi);

   // pulled straight from the fortran
   Double_t f=0,g=0,res=0;
   if(xLo==xHi){
      res = TERP(xLo,Ep);
   }else{
      f   = TERP(xLo,Ep);
      g   = TERP(xHi,Ep);
      res = ( f*(fEsGridData[xHi]-Es) + g*(Es-fEsGridData[xLo]) )/( fEsGridData[xHi]-fEsGridData[xLo] );
   } 

   if(fDebug>7){
      std::cout << "--------------------- FTCS ---------------------" << std::endl;
      std::cout << "Lower point:        Es = " << fEsGridData[xLo] << "\t" << "Ep = " << Ep << "\t" << "xs = " << f   << std::endl;
      std::cout << "Upper point:        Es = " << fEsGridData[xHi] << "\t" << "Ep = " << Ep << "\t" << "xs = " << g   << std::endl;
      std::cout << "Interpolated point: Es = " << Es               << "\t" << "Ep = " << Ep << "\t" << "xs = " << res << std::endl;
      std::cout << "------------------------------------------------" << std::endl;
   }

   return res;

}
//______________________________________________________________________________
Double_t RADCOR::TERP(Int_t i,Double_t Ep){

   // mimics the fortran TERP subroutine
   // parabolic interpolation in Ep
   // (linear extrapolation at the endpoints)  

   Int_t NumBins = fEpGridData[i].size();
   Int_t yLo=0,yHi=0,k=0;
   Double_t X1=0.,X2=0.,X3=0.;
   Double_t Y1=0.,Y2=0.,Y3=0.;
   Double_t XX=0.,XP=0.,XM=0.;
   Double_t AA=0.,BB=0.,CC=0.;
   Double_t num=0.,den=0.,res=0.;
   Double_t FudgeFactor = 1E-5;

   Double_t MAX = fEpGridData[i][NumBins-1];
   Double_t MIN = fEpGridData[i][0];

   if( (Ep>MAX)||(Ep<MIN) ){
      // Use linar extrapolation 
      if(Ep>MAX){
         k   = NumBins-1;
      }
      if(Ep<MIN){
         k   = 1;
      }
      Y1  = fXSGridData[i][k-1];
      Y2  = fXSGridData[i][k];
      X1  = fEpGridData[i][k-1];
      X2  = fEpGridData[i][k];
      res = Y1 - (Y2-Y1)*(Ep-X1)/(X2-X1);
   }else{
      BinarySearch(fEpGridData[i],Ep,yLo,yHi);

      if(yLo==yHi){
         // found the data point.  use it! 
         k  = yLo; 
         res = fXSGridData[i][k]; 
      }else{
         // the code below is pulled directly from the fortran
         X1 = fEpGridData[i][yLo];
         X2 = fEpGridData[i][yHi];
         X3 = fEpGridData[i][yHi+1];
         Y1 = fXSGridData[i][yLo];
         Y2 = fXSGridData[i][yHi];
         Y3 = fXSGridData[i][yHi+1];

         if(yHi>1){
            XX   = Ep-X2;
            XP   = X3-X2;
            XM   = X1-X2;
            AA   = Y2;
            CC   = (Y3-Y1)*(XP+XM)/(XP-XM)-(Y3+Y1-2.*Y2);
            CC   = CC/XP/XM/2.;
            BB   = (Y3-Y1)/(XP-XM)-CC*(XP+XM);
            res  = AA + BB*XX + CC*XX*XX;
         }else{
            num  = Y1*(X2 - Ep) + Y2*(Ep - X1); 
            den  = X2 - X1 + FudgeFactor; 
            res = num/den;
         }
      }

   }

   return res;

}
//______________________________________________________________________________
void RADCOR::SetUpMatrix(std::vector<Double_t> x,std::vector<Double_t> y,std::vector< std::vector<Double_t> > &F){

   int N = x.size();
   Double_t xLast = x[0];

   std::vector<Double_t> v;
   for(int i=0;i<N;i++){
      if(xLast==x[i]){
         v.push_back(y[i]);
      }else{
         F.push_back(v);
         v.clear();
      }
      xLast = x[i];
   }

   F.push_back(v);
   v.clear();

}
//______________________________________________________________________________
void RADCOR::ImportGridData(){

   // x is Es; Y is Ep; F is XS
   // variables with capital letters are matrices 

   Double_t ix,iy,iF;
   Double_t xLast=0;
   std::vector<Double_t> y,f;

   std::ifstream infile;
   infile.open(fGridPath);
   if(infile.fail()){
      std::cout << "[RADCOR::ImportGridData]: Cannot open the file: " 
         << fGridPath << std::endl;
      exit(1);
   }else{
      while(!infile.eof()){
         infile >> ix >> iy >> iF;
         fEsGridData.push_back(ix);
         y.push_back(iy);
         f.push_back(iF);
      }
      fEsGridData.pop_back();
      y.pop_back();
      f.pop_back();
   }

   SetUpMatrix(fEsGridData,y,fEpGridData);
   SetUpMatrix(fEsGridData,f,fXSGridData);

   // beam energy vector should not have repeated entries, so we cut the repeats out   
   xLast = fEsGridData[0];
   std::vector<Double_t> v;
   int N = fEsGridData.size();
   for(int i=0;i<N;i++){
      if(xLast!=fEsGridData[i]) v.push_back(xLast);
      xLast = fEsGridData[i];
   }

   v.push_back(xLast);

   fEsGridData.clear();
   N = v.size();
   for(int i=0;i<N;i++) fEsGridData.push_back(v[i]);

   std::cout << "[RADCOR::ImportGridData]: The data has been imported from file: " 
      << fGridPath << std::endl; 

}
//______________________________________________________________________________
void RADCOR::ImportGridBornData(){

   // x is Ep
   Double_t ix,ixsborn,ixsrad;

   std::ifstream infile;
   infile.open(fGridBornPath);
   if(infile.fail()){
      std::cout << "[RADCOR::ImportGridBornData]: Cannot open the file: " 
                << fGridBornPath << std::endl;
      exit(1);
   }else{
      while(!infile.eof()){
         infile >> ix >> ixsborn >> ixsrad;
         fEpGridBornData.push_back(ix);
         fXSGridBornData.push_back(ixsborn);
      }
      fEpGridBornData.pop_back();
      fXSGridBornData.pop_back();
   }

}
}}
