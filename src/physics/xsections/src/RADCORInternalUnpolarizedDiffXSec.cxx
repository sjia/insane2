#include "RADCORInternalUnpolarizedDiffXSec.h"

#include "InSANEPhysics.h"
namespace insane {
namespace physics {

RADCORInternalUnpolarizedDiffXSec::RADCORInternalUnpolarizedDiffXSec()
{
   fID         = 100010021;

   /// RADCOR object 
   fRADCOR = new RADCOR(); 
   /// Proton and neutron form factors
   auto *amtFF = new AMTFormFactors();
   /// 3He form factors 
   auto *mswFF = new MSWFormFactors();

   /// F1F209 cross section model 
   auto *F1F209 = new F1F209eInclusiveDiffXSec();
   F1F209->UseModifiedModel('y');  // use scaling of F1F209 to E01-012 and E94-010 data (for 3He) 

   /// Set options
   fRADCOR->SetTargetNucleus(Nucleus::He3());         /// by default, use 3He targetl. WHY??!? -whit should always be the proton!
   fRADCOR->SetFormFactors(mswFF);
   fRADCOR->SetUnpolXS(F1F209);

}
//______________________________________________________________________________

RADCORInternalUnpolarizedDiffXSec::~RADCORInternalUnpolarizedDiffXSec()
{ }
//______________________________________________________________________________

Double_t RADCORInternalUnpolarizedDiffXSec::EvaluateXSec(const Double_t *par) const{

   /// par = {Ebeam,Eprime,theta} 
   Double_t Ebeam  = par[0]; 
   Double_t Eprime = par[1]; 
   Double_t theta  = par[2]; 

   Double_t sig   = fRADCOR->GetInternalRadiatedXS(Ebeam,Eprime,theta); 
   return sig; 

}
//______________________________________________________________________________
}}
