!>  ELECTROPRODUCTION YIELDS OF NUCLEONS AND PIONS 
!!  WRITTEN BY J.S. O'CONNELL AND J.W. LIGHTBODY, JR.
!!  NATIONAL BUREAU OF STANDARDS 
!!  SEPTEMBER 1987 
!!
!!  HISTORY:
!!     - transverse scaling region added
!!     - modified by oara to plot like older epc's
!!     - modified slightly by glen warren to compile under linux (g77) sept. 02
!!     - Selected modified subroutines and functions so it can be compiled and linked 
!!         with exisiting codes. Whitney Armstrong 1/10/2014
!!
!! @ingroup EPCV      
!!

      !> QUASI-DEUTERON CROSS SECTION
      !! Modified with fermi and transverse scaling  
      !!
      !!  @ingroup EPCV
      !!
      subroutine dep_3(e1,tp,th,ip,d2qd)
c  quasi-deuteron cross section 
      implicit real*8 (a-h,o-z) 
      character*1 scal,fermi
      common/sg/ia3
      common/qd/qdf 
 	common /fer/ fermi,scal
      data am/939./,amd/1876./
      if(ia3.eq.1)goto 1 
      pn=sqrt(tp**2+2.*am*tp) 
	ep = tp + am
      call kine_3(amd,am,am,pn,th,w0,thc) 
      if(w0.ge.e1)go to 1 
      if(w0.le.0.)go to 1 
      w0g=w0/1000.
      call sigd(w0g,thc,ip,dsqd)
      call part_3(amd,am,am,pn,th,ajt,ajw)
	if(scal.ne.'y') then
      call vtp(amd,am,e1,w0,tp,th,phi)
c  cross section in ub/mev-sr 
c	oara: ajw gives (w+-w-)/(p+-p-) so it is dw/dp, not dw/dt!
c	cross section is then in ub/((mev/c).sr) directly
!	print *,'qdf,phi,dsqd,ajw,ajt',qdf,phi,dsqd,ajw,ajt
      d2qd=qdf*phi*dsqd*ajw*ajt*ep/pn
	else
      brem=1./w0
c  cross section in ub/mev-sr-q
c	cross section was then in ub/(q.(mev/c).sr) directly
	d2qd = qdf*brem*dsqd*ajw*ajt*ep/pn	! from gpc
!	print *,'qdf,brem,dsqd,ajw,ajt',qdf,brem,dsqd,ajw,ajt
!	d2qd = qdf*brem*dsqd*ajw*ajt	one step conversion: next line
!	d2qd = d2qd*ep*1.0d6/pn**2 ! convert to ub.c/(q.(gev/c)^2.sr)
	d2qd = d2qd*1.0d6/pn	! convert to units ub.c/(q.(gev/c)^2.sr)
	end if
      return
    1 d2qd=0. 
      return
      end 

*leg
      real*8 function pl(l,x) 
c  legendre polynomials 
      implicit real*8 (a-h,o-z) 
      if(l.eq.0)then
         pl=1.
      elseif(l.eq.1)then
         pl=x 
      elseif(l.eq.2)then
         pl=.5*(3.*x**2-1.) 
      elseif(l.eq.3)then
         pl=.5*(5.*x**3-3.*x) 
      elseif(l.eq.4)then
         pl=1./8.*(35.*x**4-30.*x**2+3.)
      else
         pl=0.
      endif 
      return
      end 
*delta
      subroutine delta_3(e1,tp,th,d2del)
c  photoproduction of nucleons and pions via delta
      implicit real*8 (a-h,o-z) 
      common/del/ip
      character*1 scal,fermi
 	common /fer/ fermi,scal
      data am/939./,amp/139./ 
      if(abs(ip).eq.1)then
         am1=am 
         am2=amp
      else
         am1=amp
         am2=am 
      endif 
      ep=tp+am1 
      pn=sqrt(ep**2-am1**2) 
!	print *
!	print *,'delta',pn
	if(fermi.ne.'y') then
      call kine_3(am,am1,am2,pn,th,w,tc)
	cpf = 1.
	else
      call kindel(am,am1,am2,pn,th,w,tc,cpf)
	end if
!	print *,'pn,w,cpf',pn,w,cpf
      if(w.le.0.)go to 1
!      if(w.ge.e1)go to 1
      if(w.gt.e1)go to 1
	if(fermi.ne.'y') then
      call part_3(am,am1,am2,pn,th,ajt,ajw) 
	else
      call partdel(am,am1,am2,pn,th,ajt,ajw) 
	end if
      call sigma_3(w,tc,dsigg)
	if(scal.ne.'y') then
c cross section in ub/mev-sr
      call vtp(am,am1,e1,w,tp,th,phi) 
!	write(*,100) am,am1,e1,w,th,phi
100	format(2x,2f8.1,5g12.3)
	d2del = phi*dsigg*ajt*cpf 
!	print *,'phi,ajt,dsigg,d2del,cpf',phi,ajt,dsigg,d2del,cpf
!	print *
	else
c  cross section in ub/mev-sr-q
        brem = 1./w
        dsig = brem*dsigg*ajt*ajw
!       d2del = dsig*cpf
!	d2del = d2del*ep*1.0d6/pn**2 ! convert to ub.c/(q.(gev/c)^2.sr)
        d2del = dsig*ep/pn
	d2del = d2del*1.0d6/pn	! convert to units ub.c/(q.(gev/c)^2.sr)
        d2del = d2del*cpf
	end if
      return
    1 d2del=0.
      return
      end 
*part_3 
      subroutine part_3(amt,am1,am2,pn,tn,ajt,ajw)
c  partial derivatives
      implicit real*8 (a-h,o-z) 
      pi=acos(-1.0d0)
!      dt=pi/180.0d0
!      dp=10.0d0
      dt=pi/720.0d0
      dp=2.0d0
c  angle
      tnp=tn+dt 
      tnm=tn-dt 
      tn0=tn
      call kine_3(amt,am1,am2,pn,tnp,wp,tcp) 
      call kine_3(amt,am1,am2,pn,tnm,wm,tcm) 
      call kine_3(amt,am1,am2,pn,tn0,w,tc0) 
      den=cos(tnp)-cos(tnm) 
      den=abs(den)
!	print *,'den,tcp,tcm,tc0',den,tcp,tcm,tc0
      if(den.gt.1.0d-3.and.(w*wp*wm.gt.0.))then
         ajt=(cos(tcp)-cos(tcm))/den
         ajt=abs(ajt) 
      else
         ajt=(cos(tc0)-cos(tcm))/(cos(tn0)-cos(tnm))
         ajt=abs(ajt) 
      endif 
c  energy 
      pnp=pn+dp 
      pnm=pn-dp 
      call kine_3(amt,am1,am2,pnp,tn,wp,tc) 
      call kine_3(amt,am1,am2,pnm,tn,wm,tc) 
	am12 = am1**2
	tp = sqrt(pnp**2 + am12) - am1
	tm = sqrt(pnm**2 + am12) - am1
      ajw=(wp-wm)/(pnp-pnm) 
!	ajw=(wp-wm)/(tp-tm) 
      ajw=abs(ajw)
      return
      end 
*kine_3 
      subroutine kine_3(amt,am1,am2,pn,th,w,tc) 
c  computes cm variables from lab variables 
      implicit real*8 (a-h,o-z) 
C	common /kf/ e11
	amt2 = amt**2
	am12 = am1**2
      ep=sqrt(pn**2+am12) 
      pnt=pn*sin(th)
      pnl=pn*cos(th)
!      anum=pn**2+am2**2-(amt-ep)**2 
      anum = am2**2 - am12 - amt2 +2*ep*amt
      den=2.*(pnl+amt-ep) 
      w=anum/den
      if(w.le.0.)w=0. 
c  invariant mass 
      ww2 = amt**2+2.*w*amt
      ww = sqrt(ww2)
!	print *,' p,w,e_thr ',pn,ww,w,anum/den
c  cm variables 
      pct=pnt 
      b=w/(amt+w) 
      g=(w+amt)/ww
      pcl=g*(pnl-b*ep)
      pcs=pcl**2+pct**2 
      pc=sqrt(pcs)
      cthc=pcl/pc 
      tc=acos(cthc) 
      return
      end 
*sigma_3
      subroutine sigma_3(e,thrcm,sigcm) 
c  real photon cross section in delta region
c  microbarns per steradian 
      implicit real*8 (a-h,o-z) 
      gam=100.
	pi=acos(-1.0d0)
      if(e.gt.420.)then 
        sigcm=(1.+420./e)*90./4./pi 
      else
        sigcm=360.*(5.-3.*cos(thrcm)**2)
        sigcm=sigcm/16./pi/(1.+(e-320)**2/gam**2) 
      endif 
      return
      end 
*ep 
      subroutine ep_3(e1,tp,thp,dsep) 
c  electro proton production cross sections 
      implicit real*8 (a-h,o-z) 
      common/p/ph(10),wph(10) 
      data aml/.511/
	pi=acos(-1.0d0)
      call gausab_3(10,ph,wph,0.d0,2.*pi,pi)
      ak=sqrt(e1**2-aml**2) 
      call sep_3(ak,tp,thp,dsep)
      dsep=dsep*1.e4
c  cross section in ub/mev-sr 
      end 
*gausab_3 
      subroutine gausab_3(n,e,w,a,b,c)
      implicit real*8 (a-h,o-z) 
      dimension e(24),w(24) 
      data eps/1.d-16/
      if(a.ge.c.or.c.ge.b)stop
c           stops program if a, c, b are out of sequence
	pi=acos(-1.0d0)
      al=(c*(a+b)-2*a*b)/(b-a)
      be=(a+b-2*c)/(b-a)
      m=(n+1)/2 
      dn=n
      do 5 i=1,m
       di=i 
       x=pi*(4.d0*(dn-di)+3.d0)/(4.d0*dn+2.d0)
       xn=(1.d0-(dn-1.d0)/(8.d0*dn*dn*dn))*cos(x) 
       if(i.gt.n/2) xn=0
       do 3 iter=1,10 
        x=xn
        y1=1.d0 
        y=x 
        if(n.lt.2) go to 2
        do 1 j=2,n
         dj=j 
         y2=y1
         y1=y 
    1    y=((2.d0*dj-1.d0)*x*y1-(dj-1.d0)*y2)/dj
    2   continue
        ys=dn*(x*y-y1)/(x*x-1.d0) 
        h=-y/ys 
        xn=x+h
        if(abs(h).lt.eps) go to 4 
    3   continue
    4  e(i)=(c+al*x)/(1.d0-be*x)
       e(n-i+1)=(c-al*x)/(1.d0+be*x)
       gew=2.d0/((1.d0-x*x)*ys*ys)
       w(i)=gew*(al+be*c)/(1.d0-be*x)**2
       w(n-i+1)=gew*(al+be*c)/(1.d0+be*x)**2
    5  continue 
      return
      end 
*vect 
      subroutine vect_3(thp,the,phi,p,ak1,ak2)
c  cartesian components of electron and proton vectors
      implicit real*8 (a-h,o-z) 
      common/v/ak1v(3),ak2v(3),qv(3),pv(3),pp(3)
      pv(1)=p*sin(thp)
      pv(2)=0.
      pv(3)=p*cos(thp)
      ak1v(1)=0.
      ak1v(2)=0.
      ak1v(3)=ak1 
      ak2v(1)=ak2*sin(the)*cos(phi) 
      ak2v(2)=ak2*sin(the)*sin(phi) 
      ak2v(3)=ak2*cos(the)
      qv(1)=ak1v(1)-ak2v(1) 
      qv(2)=ak1v(2)-ak2v(2) 
      qv(3)=ak1v(3)-ak2v(3) 
      pp(1)=pv(1)-qv(1) 
      pp(2)=pv(2)-qv(2) 
      pp(3)=pv(3)-qv(3) 
      return
      end 
*amag_3 
      real*8 function amag_3(v) 
      implicit real*8 (a-h,o-z) 
      dimension v(3)
      amag_3=0. 
      do 1 i=1,3
    1 amag_3=amag_3+v(i)**2 
      amag_3=sqrt(amag_3) 
      return
      end 
*lept_3
      subroutine lept_3(e1,e2,ak1,ak2,aml,qs,qus,the,v) 
c  lepton factors for coincidence cross section 
      implicit real*8 (a-h,o-z) 
      dimension v(5)
      v(1)=(qus/qs)**2*(e1*e2+ak1*ak2*cos(the)+aml**2)
      x=ak1*ak2*sin(the)
      v(2)=x**2/qs+qus/2. 
      v(3)=qus/qs*x/sqrt(qs)*(e1+e2)
      v(4)=x**2/qs
      v(5)=0. 
      return
      end 
*d4s_3
      subroutine d4s_3(ak1,ak2,the,p,pp,thqp,cphip,dsig)
c  fully differential cross section 
      implicit real*8 (a-h,o-z) 
      dimension v(5),w(5) 
      data am/939./,aml/.511/,a/855./ 
      qs=ak1**2+ak2**2-2.*ak1*ak2*cos(the)
      e1=sqrt(ak1**2+aml**2)
      e2=sqrt(ak2**2+aml**2)
      qus=2.*(e1*e2-ak1*ak2*cos(the)-aml**2)
      sm=2.*(1.44)**2/qus**2*ak2/ak1
      ep=sqrt(am**2+p**2) 
      ps=ep*p 
      fns=1./(1.+qus/a**2)**4 
      call lept_3(e1,e2,ak1,ak2,aml,qs,qus,the,v) 
      call form_3(qs,p,thqp,cphip,w)
      sum=0.
      do 1 i=1,5
    1 sum=sum+v(i)*w(i) 
      dsig=sm*ps*fns*sum*sgsl_3(pp) 
      return
      end 
*sthe_3 
      subroutine sthe_3(d2s)
c  integral over electron polar angle 
      implicit real*8 (a-h,o-z) 
      common/s/ ak1,ak2,the,p,thp 
      common/e/th1(12),wt1(12),th2(12),wt2(12)
      common/e1/th3(24),wt3(24) 
      d2s1=0. 
      do 1 i=1,12 
      the=th1(i)
      call sphi_3(d3s)
    1 d2s1=d2s1+d3s*wt1(i)*sin(the) 
      d2s2=0. 
      do 2 i=1,12 
      the=th2(i)
      call sphi_3(d3s)
    2 d2s2=d2s2+d3s*wt2(i)*sin(the) 
      d2s3=0. 
      do 3 i=1,24 
      the=th3(i)
      call sphi_3(d3s)
    3 d2s3=d2s3+d3s*wt3(i)*sin(the) 
      d2s=d2s1+d2s2+d2s3
      return
      end 
*sphi 
      subroutine sphi_3(d3s)
c  integrate over electron azimuthal angle
      implicit real*8 (a-h,o-z) 
      common/s/ ak1,ak2,the,p,thp 
      common/v/ak1v(3),ak2v(3),qv(3),pv(3),pp(3)
      common/p/ph(10),wph(10) 
      dimension qxp(3),ak1x2(3) 
      d3s=0.
      do 1 i=1,10 
      phi=ph(i) 
      call vect_3(thp,the,phi,p,ak1,ak2)
      call cross(qv,pv,qxp) 
      call cross(ak1v,ak2v,ak1x2) 
c  proton theta 
      cthep=dot(pv,qv)/amag_3(pv)/amag_3(qv)
      thqp=acos(cthep)
c  proton phi 
      cphip=dot(qxp,ak1x2)
      if (cphip.eq.0.) then 
         cphip=1. 
      else
         cphip=cphip/amag_3(qxp)/amag_3(ak1x2)
      endif 
      ppm=amag_3(pp)
      call d4s_3(ak1,ak2,the,p,ppm,thqp,cphip,dsig) 
    1 d3s=d3s+dsig*wph(i) 
      return
      end 
*form_3
      subroutine form_3(qs,p,thqp,cphip,w)
c  nuclear form factors 
      implicit real*8 (a-h,o-z) 
      common/m/zz,nn
      common/del/ip 
      dimension w(5)
      data am/939./,up/2.79/,un/-1.91/
      if(ip.eq.1)then 
         z=zz 
         n=0. 
      elseif(ip.eq.-1)then
         z=0. 
         n=nn 
      else
         z=0. 
         n=0. 
      endif 
      y=p/am*sin(thqp)
      w(1)=z
      w(2)=z*y**2 
      w(2)=w(2)+(z*up**2+n*un**2)*qs/2./am**2 
      w(3)=-2.*z*y*cphip
      w(4)=z*y**2*(2.*cphip**2-1.)
      w(5)=0. 
      return
      end 
*sep
      subroutine sep_3(ak,tp,thpp,d2s)
      implicit real*8 (a-h,o-z) 
      common/s/ak1,ak2,the,p,thp
      common/e/th1(12),wt1(12),th2(12),wt2(12)
      common/e1/th3(24),wt3(24) 
      data am/939./,aml/.511/,be/16./ 
	pi=acos(-1.0d0)
      thp=thpp
      ak1=ak
      ak2=ak1-tp-be 
c  gaussian points for the
      themax=aml*(ak1-ak2)/ak1/ak2
      call gausab_3(12,th1,wt1,0.d0,2.*themax,themax) 
      call gausab_3(12,th2,wt2,2.*themax,100.*themax,10.*themax)
      a3=100.*themax
      c3=a3+(pi-a3)/10. 
      call gausab_3(24,th3,wt3,a3,pi,c3)
      p=sqrt(tp**2+2.*tp*am)
      call sthe_3(d2s)
      if(ak2.le.0.)d2s=0. 
      return
      end 

C*fermi3 
C      !> Fermi distributions of nucleons 
C      !!
C      !! @param p     momentum in MeV
C      !! @param ia    A=Z+N
C      !! @param res   ouput
C      !! 
C      !! Revision Histroy:
C      !!   - Added copy from sgsl_3 function. 1/25/2014 Whitney Armstrong
C      !!
C      !!  @ingroup EPCV
C      !!
C      subroutine fermi3(p,ia,res) 
C      implicit DOUBLE PRECISION (a-h,o-z) 
Cc      INTEGER ia
Cc  p integral over sgsl normalized to 1/4pi 
C      if(ia.eq.2)then 
Cc  begin 2-h
C         pp=p/197.3 
C         sgs=3.697-7.428*pp-2.257*pp**2 
C         sgs=sgs+3.618*pp**3-1.377*pp**4+.221*pp**5-.013*pp**6
C         if(sgs.lt.-293.)go to 1
C         sgs=exp(sgs) 
C         sgs=sgs/.18825/4./3.1416/(197.3)**3
C         sgsl_3=sgs/1.
C      elseif(ia.eq.3)then 
Cc  begin 3-he 
C         if(-(p/33)**2.lt.-293.)go to 1 
C         sgs=2.4101e-6*exp(-p/33) 
C         sgs=sgs-1.4461e-6*exp(-(p/33)**2)
C         sgs=sgs+1.6871e-10*exp(-(p/493)**2)
C         sgsl_3=sgs/2.0d0
C      elseif(ia.eq.4)then 
Cc   begin 4-he
C         if(-(p/113.24)**2.lt.-293.)go to 1 
C         sgs=1.39066e-6*exp(-(p/113.24)**2) 
C         sgs=sgs+3.96476e-9*exp(-(p/390.75)**2) 
C         sgsl_3=sgs/2.0d0
C         sgsl_3=sgsl_3/2.0d0/3.1416
C      elseif(ia.gt.4.and.ia.lt.12)then
C         if(-(p/127)**2.lt.-293.)go to 1
C         sgs=1.7052e-7*(1.+(p/127)**2)*exp(-(p/127)**2) 
C         sgs=sgs+1.7052e-9*exp(-(p/493)**2) 
C         sgsl_3=sgs/(float(ia)/2.0d0)
C      elseif(ia.eq.12)then
Cc  begin 12-c 
C         if(-(p/127)**2.lt.-293.)go to 1
C         sgs=1.7052e-7*(1.+(p/127)**2)*exp(-(p/127)**2) 
C         sgs=sgs+1.7052e-9*exp(-(p/493)**2) 
C         sgsl_3=sgs/6.
C      else
Cc  begin 16-o 
C         if(-(p/120)**2.lt.-293.)go to 1
C         sgs=3.0124e-7*(1.+(p/120)**2)*exp(-(p/120)**2) 
C         sgs=sgs+1.1296e-9*exp(-(p/493)**2) 
C         sgsl_3=sgs/(float(ia)/2.0d0)
C      endif 
C      res=sgsl_3
C      return
C    1 sgsl_3=0. 
C      res=sgsl_3
C      return
C      end 
C*sgsl_3 
C      real*8 function sgsl_3(p) 
C      implicit real*8 (a-h,o-z) 
Cc  p integral over sgsl normalized to 1/4pi 
C      common/sp/ia
C      common/qd/qdf 
C      if(ia.eq.2)then 
Cc  begin 2-h
C         pp=p/197.3 
C         sgs=3.697-7.428*pp-2.257*pp**2 
C         sgs=sgs+3.618*pp**3-1.377*pp**4+.221*pp**5-.013*pp**6
C         if(sgs.lt.-293.)go to 1
C         sgs=exp(sgs) 
C         sgs=sgs/.18825/4./3.1416/(197.3)**3
C!         sgsl=sgs/1.
C      elseif(ia.eq.3)then 
Cc  begin 3-he 
C         if(-(p/33)**2.lt.-293.)go to 1 
C         sgs=2.4101e-6*exp(-p/33) 
C         sgs=sgs-1.4461e-6*exp(-(p/33)**2)
C         sgs=sgs+1.6871e-10*exp(-(p/493)**2)
C         sgsl_3=sgs/2.0d0
C      elseif(ia.eq.4)then 
Cc   begin 4-he
C         if(-(p/113.24)**2.lt.-293.)go to 1 
C         sgs=1.39066e-6*exp(-(p/113.24)**2) 
C         sgs=sgs+3.96476e-9*exp(-(p/390.75)**2) 
C         sgsl_3=sgs/2.0d0
C         sgsl_3=sgsl_3/2.0d0/3.1416
C      elseif(ia.gt.4.and.ia.lt.12)then
C         if(-(p/127)**2.lt.-293.)go to 1
C         sgs=1.7052e-7*(1.+(p/127)**2)*exp(-(p/127)**2) 
C         sgs=sgs+1.7052e-9*exp(-(p/493)**2) 
C         sgsl_3=sgs/(float(ia)/2.0d0)
C      elseif(ia.eq.12)then
Cc  begin 12-c 
C         if(-(p/127)**2.lt.-293.)go to 1
C         sgs=1.7052e-7*(1.+(p/127)**2)*exp(-(p/127)**2) 
C         sgs=sgs+1.7052e-9*exp(-(p/493)**2) 
C         sgsl_3=sgs/6.
C      else
Cc  begin 16-o 
C         if(-(p/120)**2.lt.-293.)go to 1
C         sgs=3.0124e-7*(1.+(p/120)**2)*exp(-(p/120)**2) 
C         sgs=sgs+1.1296e-9*exp(-(p/493)**2) 
C         sgsl_3=sgs/(float(ia)/2.0d0)
C      endif 
C      return
C    1 sgsl_3=0. 
C      return
C      end 

*s2pi 
      subroutine s2pi(ip,e1,tp,th,d2sc) 
c  integral over scaling cross section
      implicit real*8 (a-h,o-z) 
      character*1 scal,fermi
 	common /fer/ fermi,scal
      data am/939./,amp/139./ 
      if(abs(ip).eq.1)then
c  one pion thr 
         ap=am
         am2=amp
      elseif(ip.eq.2.or.ip.eq.0)then
c  one pion thr 
         ap=amp 
         am2=am 
      elseif(ip.eq.-2)then
c  two pion thr 
         ap=amp 
         am2=am+amp 
      else
         stop 
      endif 
      p=sqrt(tp**2+2.*ap*tp)
      e=tp+ap 
!	print *,'s2pi',p
	if(fermi.ne.'y') then
      call kine_3(am,ap,am2,p,th,thr,tc)
	cpf = 1.
	else
      call kindel(am,ap,am2,p,th,thr,tc,cpf)
	end if
!	 print *,'p,thr',p,thr
      if(thr.le.0.)goto 2 
!      if(e1.le.thr)goto 2 
      if(e1.lt.thr)goto 2 
      dw=(e1-thr)/20.0d0
      sum=0.0d0
      sumbr=0.0d0
!      do 1 i=1,20 
      do i=1,20 
      w=thr+(float(i)-.5d0)*dw
	if(w.lt.(e1-0.511)) then
      call vtp(am,amp,e1,w,tp,th,gn)
!	write(*,100) i,am,amp,e1,w,dw,thr,gn
100	format(i4,2f8.1,5g12.3)
	call wiser(w/1.e3,p/1.e3,th,f)
!	print *,'p,w,gn,f',p,w,gn,f
      sum=sum+gn*f*dw 
	sumbr = sumbr + f*dw/w
!    1 continue
	end if
      end do
	if(scal.eq.'y') then
	d2sc=sumbr
	else
	d2sc=sum*p**2/e*1.e-6
	end if
	d2sc = d2sc*cpf
!	print *,'d2pi',d2sc,cpf
      return
    2 d2sc=0. 
      return
      end 

*wiser
      subroutine wiser(w,p,th,f)
c  invariant inclusive cross section
c  units in gev 
      implicit real*8 (a-h,o-z) 
      common/del/ip 
      dimension a(7),b(7),c(7)
      data a/5.66e2,8.29e2,1.79,2.10,-5.49,-1.73,0./
      data b/4.86e2,1.15e2,1.77,2.18,-5.23,-1.82,0./
      data c/1.33e5,5.69e4,1.41,0.72,-6.77,1.90,-1.17e-2/ 
      data am/.939/,amp/.139/ 
      if(abs(ip).eq.1)then
         ap=am
      elseif(abs(ip).eq.2.or.ip.eq.0)then 
         ap=amp 
      else
         stop 
      endif 
      e=sqrt(p**2+ap**2)
c  mandelstam variables 
      s=2.*w*am+am**2 
c     t=-2.*w*e+2.*w*p*cos(th) +ap**2 
      u=am**2-2.*am*e+ap**2 
c  fitting variables
      pt=p*sin(th)
      aml=sqrt(pt**2+ap**2) 
      call fxr(w,p,e,th,xr) 
c  fitted 
      if(ip.eq.2.or.ip.eq.0)then
         x1=a(1)+a(2)/sqrt(s) 
         x2=(1.-xr+a(3)**2/s)**a(4) 
         x3=exp(a(5)*aml) 
         x4=exp(a(6)*pt**2/e) 
         f=x1*x2*x3*x4
      elseif(ip.eq.-2)then
         x1=b(1)+b(2)/sqrt(s) 
         x2=(1.-xr+b(3)**2/s)**b(4) 
         x3=exp(b(5)*aml) 
         x4=exp(b(6)*pt**2/e) 
         f=x1*x2*x3*x4
      elseif(abs(ip).eq.1)then
         x1=c(1)+c(2)/sqrt(s) 
         x2=(1.-xr+c(3)**2/s)**c(4) 
         x3=exp(c(5)*aml) 
         x4=1./(1.+abs(u))**(c(6)+c(7)*s) 
         f=x1*x2*x3*x4
      else
         stop 
      endif 
      return
      end 
*fxr
      subroutine fxr(w,p,e,th,xr) 
c  computes ratio of cm particle momentum to photon momentum
c  gev units
      implicit real*8 (a-h,o-z) 
      data am/.939/ 
      pt=p*sin(th)
      pl=p*cos(th)
c  lorentz transformation 
      b=w/(w+am)
      d=sqrt(2.*w*am+am**2) 
      g=(w+am)/d
      bg=b*g
c cm variables
      wc=g*w-bg*w 
      plc=g*pl-bg*e 
      pc=sqrt(pt**2+plc**2) 
      xr=pc/wc
      return
      end 
*kindel
      subroutine kindel(amt,am1,am2,pn,th,w,tc,cpf) 
c  computes cm variables from lab variables 
      implicit real*8 (a-h,o-z) 
	common /kf/ e11
	data cosav/0.63662/
	amt2 = amt**2
	am12 = am1**2
      ep=sqrt(pn**2+am12) 
      pnt=pn*sin(th)
      pnl=pn*cos(th)
!      anum=pn**2+am2**2-(amt-ep)**2 
      anum = am2**2 - am12 - amt2 +2*ep*amt
      den=2.*(pnl+amt-ep) 
      w=anum/den
      if(w.le.0.)w=0. 
c  invariant mass 
      ww2 = amt**2+2.*w*amt
      ww = sqrt(ww2)
!	print *,' p,w,e_thr ',pn,ww,w,anum/den

c	section on fermi motion

        if(cpf.ge.0.) then	! cpf = -1. for jacobian computation
          cpf = 1.0d0
          if(w.ge.e11) then
            dm2 = ww2 - amt2 
            pfmin = e11*((dm2/2./e11)**2 - amt2)/dm2
            cpf0 = sgsl_3(0.d0)
            cpf = sgsl_3(pfmin)
            if(cpf.gt.0.0d0) then
              dpf = 10.0d0
              pf = pfmin
              dspf = cpf*pf
              sumpf = dspf
              sumcf = cpf
              do while((dspf/sumpf).gt.1.0d-4)
                pf = pf + dpf
                cpf = sgsl_3(pf)
                dspf = cpf*pf
                sumpf = dspf + sumpf
                sumcf = cpf + sumcf
              end do
!	      print *,'pf,dspf,sumpf',pf,dspf,sumpf
              pfmean = sumpf/sumcf
              pfmean = pfmean*cosav
              cpf = sgsl_3(pfmean)/cpf0
              cpf = 0.5*cpf
              ef = sqrt(pfmean**2 + amt**2)
              weff = dm2/(2.0d0*(ef + pfmean))
!	      weff = dm2/(2.0d0*ef)
!	      print *,'w,ww,pfmin,cpf0',w,ww,pfmin,cpf0
!	      print *,'pfmean,cpf,weff',pfmean,cpf,weff
              w = weff
            else
              w = 0.
            end if
          end if
        end if

c  cm variables 
      pct=pnt 
      b=w/(amt+w) 
      g=(w+amt)/ww
      pcl=g*(pnl-b*ep)
      pcs=pcl**2+pct**2 
      pc=sqrt(pcs)
      cthc=pcl/pc 
      tc=acos(cthc) 
      return
      end 
*partdel
      subroutine partdel(amt,am1,am2,pn,tn,ajt,ajw)
c  partial derivatives
      implicit real*8 (a-h,o-z) 
      pi=acos(-1.0d0)
!      dt=pi/180.0d0
!      dp=10.0d0
      dt=pi/720.0d0
      dp=2.0d0
c  angle
      tnp=tn+dt 
      tnm=tn-dt 
      tn0=tn
	cpf = -1.
      call kindel(amt,am1,am2,pn,tnp,wp,tcp,cpf)
	cpf = -1.
      call kindel(amt,am1,am2,pn,tnm,wm,tcm,cpf)
	cpf = -1.
      call kindel(amt,am1,am2,pn,tn0,w,tc0,cpf)
      den=cos(tnp)-cos(tnm) 
      den=abs(den)
!	print *,'den,tcp,tcm,tc0',den,tcp,tcm,tc0
      if(den.gt.1.0d-3.and.(w*wp*wm.gt.0.))then
         ajt=(cos(tcp)-cos(tcm))/den
         ajt=abs(ajt) 
      else
         ajt=(cos(tc0)-cos(tcm))/(cos(tn0)-cos(tnm))
         ajt=abs(ajt) 
      endif 
c  energy 
      pnp=pn+dp 
      pnm=pn-dp 
      call kine_3(amt,am1,am2,pnp,tn,wp,tc) 
      call kine_3(amt,am1,am2,pnm,tn,wm,tc) 
	am12 = am1**2
	tp = sqrt(pnp**2 + am12) - am1
	tm = sqrt(pnm**2 + am12) - am1
      ajw=(wp-wm)/(pnp-pnm) 
!	ajw=(wp-wm)/(tp-tm) 
      ajw=abs(ajw)
      return
      end 
*oara
	subroutine oara(w,p,th,dsdwdp)
	implicit real*8 (a-h,m-z)
	integer n
	common/sp/ia
	common/qd/qdf 
      	common/del/ip 
        character*1 scal,fermi
 	common /fer/ fermi,scal
      	common/m/z,n
	common/kf/e11
	
	data mpi/0.140/
	data b/7.0d0/,c/9.0d0/,stot/125.0d0/
	data cp/8.0d3/,t0/124.0d-3/

	if(abs(ip).eq.1) then
	dsdwdp = stot*c*exp(-b*p**2)
	else if (abs(ip).eq.2.or.ip.eq.0) then
	t = sqrt(p**2+mpi**2) - mpi
	dsdwdp = cp*exp(-t/t0)
	end if
	return
	end

