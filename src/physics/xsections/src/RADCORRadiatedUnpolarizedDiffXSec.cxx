#include "RADCORRadiatedUnpolarizedDiffXSec.h"

#include "InSANEPhysics.h"
namespace insane {
namespace physics {

RADCORRadiatedUnpolarizedDiffXSec::RADCORRadiatedUnpolarizedDiffXSec()
{
   fID         = 100010023;

   fApprox      = -1;                              /// default is no setting  

   /// RADCOR object 
   fRADCOR = new RADCOR(); 
   /// Proton and neutron form factors
   auto *amtFF = new AMTFormFactors();
   /// 3He form factors 
   auto *mswFF = new MSWFormFactors();

   /// F1F209 cross section model 
   auto *F1F209 = new F1F209eInclusiveDiffXSec(); 
   F1F209->UseModifiedModel('n');  // use scaling of F1F209 to E01-012 and E94-010 data (for 3He) by D. Flay  

   /// Set models for RADCOR 
   fRADCOR->SetFormFactors(amtFF); 
   fRADCOR->SetTargetFormFactors(mswFF); 
   fRADCOR->SetUnpolXS(F1F209);
   /// Set target type 
   fRADCOR->SetTargetNucleus(Nucleus::He3());   /// 3He is default  

   /// Set integration threshold (peaking approx) 
   fRADCOR->SetThreshold(1);    /// QE threshold 

}
//______________________________________________________________________________

RADCORRadiatedUnpolarizedDiffXSec::~RADCORRadiatedUnpolarizedDiffXSec()
{ }
//______________________________________________________________________________

Double_t RADCORRadiatedUnpolarizedDiffXSec::EvaluateXSec(const Double_t *par) const
{
   Double_t Ebeam  = par[0];
   Double_t Eprime = par[1];
   Double_t theta  = par[2];

   // std::cout << "[RADCORRadiatedUnpolarizedDiffXSec::EvaluateXSec]: Kinematics" << std::endl;
   // std::cout << "Es = " << Ebeam         << std::endl;
   // std::cout << "Ep = " << Eprime        << std::endl; 
   // std::cout << "th = " << theta/degree  << std::endl;  
   // std::cout << "----------------------" << std::endl;

   Double_t sig=0; 

   if(fApprox==0){
      sig = fRADCOR->Exact(Ebeam,Eprime,theta);  
   }else if(fApprox==1){
      sig = fRADCOR->EnergyPeakingApprox(Ebeam,Eprime,theta);
   }else{
      std::cout << "[RADCORRadiatedUnpolarizedDiffXSec]: Invalid choice!  Exiting..." << std::endl;
      exit(1);
   }

   return sig; 

}
//______________________________________________________________________________
}}
