#include "InelasticRadiativeTail2.h"
namespace insane {
namespace physics {

InelasticRadiativeTail2::InelasticRadiativeTail2()
{
   fID = 100010012;
   
   SetTitle("InelasticRadiativeTail2");//,"POLRAD Born cross-section");
   SetPlotTitle("Inelastic Radiative Tail cross-section");
   std::cout << " creating " << GetTitle() << std::endl; 

   fPOLRAD       = nullptr;
   fRADCOR       = nullptr;

   fLabel        = "#frac{d#sigma}{dEd#Omega}";
   fUnits        = "nb/GeV/sr";
   fAddRegion4   = false;
   fInternalOnly = false;

   fRadLen[0]    = 0.05;
   fRadLen[1]    = 0.05;

   //fDiffXSec0 = new POLRADInelasticTailDiffXSec();
   //fDiffXSec0 = new  POLRADBornDiffXSec();
   //fDiffXSec0->SetTargetType(Nucleus::kProton);
   //fDiffXSec0->InitializePhaseSpaceVariables();
   //fDiffXSec0->InitializeFinalStateParticles();

   std::cout << "this is: " << this << std::endl;
   std::cout << "radcor: " <<  GetRADCOR() << std::endl;
   GetRADCOR()->SetUnpolarizedCrossSection(this); 
   GetRADCOR()->SetRadiationLengths(fRadLen);

}
//________________________________________________________________________________

InelasticRadiativeTail2::~InelasticRadiativeTail2()
{ }
//________________________________________________________________________________

Double_t InelasticRadiativeTail2::EvaluateXSec(const Double_t *x) const {
   if (!VariablesInPhaseSpace(fnDim, x)){
      //std::cout << "[POLRADElasticTailDiffXSec::EvaluateXSec]: Something is wrong!" << std::endl;
      return(0.0);
   }

   Double_t Eprime  = x[0];
   Double_t theta   = x[1];
   Double_t phi     = x[2];
   Double_t Ebeam   = GetBeamEnergy();
   //GetBornXSec()->SetBeamEnergy(Ebeam);

   //Double_t nu      = Ebeam-Eprime;
   //Double_t Mtarg   = fPOLRAD->GetTargetMass();  

   Double_t sig_rad = 0.0;

   // Using the POLRAD IRT
   //sig_rad = fDiffXSec0->EvaluateXSec(x);

   // Using the Equiv. Rad. Method
   Double_t sig_rad2 = 0.0;
   if(fInternalOnly){
      sig_rad = GetRADCOR()->ContinuumStragglingStripApprox_InternalEquivRad(Ebeam,Eprime,theta,phi);
      if(fAddRegion4) sig_rad2 = GetRADCOR()->Internal2DEnergyIntegral(Ebeam,Eprime,theta,phi);
      sig_rad += sig_rad2;
   } else {
      sig_rad = GetRADCOR()->ContinuumStragglingStripApprox(Ebeam,Eprime,theta,phi);
      if(fAddRegion4) sig_rad2 = GetRADCOR()->Internal2DEnergyIntegral(Ebeam,Eprime,theta,phi);
      sig_rad += sig_rad2;
   }


   if( IncludeJacobian() ) sig_rad = sig_rad*TMath::Sin(theta);
   if(sig_rad <0.0 || TMath::IsNaN(sig_rad)) sig_rad = 0.0;
   return sig_rad;
} 
//________________________________________________________________________________
void InelasticRadiativeTail2::CreateRADCOR() const {
   fRADCOR = new RADCOR();
   //fRADCOR->Do(false); 
   fRADCOR->SetThreshold(2); 
   fRADCOR->UseMultiplePhoton();
   fRADCOR->UseInternal(false);  
   fRADCOR->UseExternal(true);  
   fRADCOR->SetPolarization(0); 
   fRADCOR->SetVerbosity(0); 
   //fRADCOR->SetCrossSection(fDiffXSec); 
   fRADCOR->SetTargetNucleus(Nucleus::Proton()); 
   fRADCOR->SetRadiationLengths(fRadLen);
}
//________________________________________________________________________________
void InelasticRadiativeTail2::CreatePOLRAD() const {
   fPOLRAD = new POLRAD();
   fPOLRAD->SetVerbosity(1);
   //fPOLRAD->DoQEFullCalc(false); 
   fPOLRAD->SetTargetNucleus(Nucleus::Proton());
   fPOLRAD->fErr   = 1E-1;   // integration error tolerance 
   fPOLRAD->fDepth = 3;     // number of iterations for integration 
   fPOLRAD->SetMultiPhoton(true); 
   //fPOLRAD->SetUltraRel   (false);
}
//________________________________________________________________________________

}}
