#include "RadiativeEffects.h"
#include "TMath.h"
#include "InSANEMathFunc.h"

namespace insane {
  using namespace units;
   namespace physics {


      double F_Tsai(double Es, double Ep, double theta, double T)
      {
         // "Structure function" that multiplies the non-radiative cross section
         // Eqn 2.8, Tsai, SLAC-PUB-848 1971
         using namespace TMath;
         double T1 = insane::Kine::F_tilde_internal(Es,Ep,theta);
         double b  = 4.0/3.0;
         double T2 = 0.5772*b*T;
         return T1+T2;
      }

      double RCToPeak_Tsai(double Es, double EpPeak, double theta, double T, double Z, double A)
      {
         // Multiplicative factor for the radiative correction to the jth peak.
         // Eqn 2.3, Tsai, SLAC-PUB-848 1971
         // This returns the term multiplying sig_j^eff on the RHS. 
         using namespace TMath;
         double Q2     = 4.0*Es*EpPeak*Power(Sin(theta/2.0),2);
         //double EpPeak = Es/(1.0 + 2.0*Es/M*TMath::Power(TMath::Sin(theta/2.0), 2));
         double R      = 1.0;
         double b      = 4.0/3.0;
         double deltaE = 0.01;
         double tr     = tr_Tsai(Q2);
         double X0     = insane::Math::rad_length(Z,A);
         double xi     = 0.000154*(Z/A)*T*X0;
         double Tprime = b*(T/2.0 + tr);
         double res  = Power(R*deltaE*deltaE/(Es*EpPeak),Tprime)*(1.0-xi/deltaE);
         //std::cout  << (1.0-xi/deltaE) << std::endl;
         return res;
      }

      double Ib_integral(double E0, double Eprime, double DeltaE, double T, double Z, double A)
      {
         // Integral of Ib near tip 
         // Eqn B.36, Tsai, SLAC-PUB-848 1971
         using namespace TMath;
         double b      = 4.0/3.0;
         double part1  = (1.0+0.5722*b*T)*Power(DeltaE/E0,b*T);
         double X0     = insane::Math::rad_length(Z,A);
         double xi     = 0.000154*(Z/A)*T*X0;
         double part2  = 1.0-xi/((1.0-b*T)*DeltaE);
         double res    = part1*part2;
         return res;
      }

      double tr_Tsai(double Q2)
      {
         // Eqn 2.7, Tsai, SLAC-PUB-848 1971
         double b      = 4.0/3.0;
         double m2    = (M_e/GeV)*(M_e/GeV); 
         double alpha = fine_structure_const; 
         double T1    = (1.0/b)*(alpha/pi); 
         double T2    = TMath::Log(Q2/m2) - 1.0; 
         return T1*T2;
      }

      double omega_s_Tsai(double Es, double Ep, double theta, double M)
      {
         // Eqn C.11, Tsai, SLAC-PUB-848 1971
         using namespace TMath;
         double Q2 = 4.0*Es*Ep*Power(Sin(theta/2.0),2.0);
         double W2 = M*M - Q2 + 2.0*M*(Es-Ep);
         return( (W2-M*M)/(2.0*(M-Ep*(1.0-Cos(theta))))) ;
      }

      double omega_p_Tsai(double Es, double Ep, double theta, double M)
      {
         // Eqn C.12, Tsai, SLAC-PUB-848 1971
         using namespace TMath;
         double Q2 = 4.0*Es*Ep*Power(Sin(theta/2.0),2.0);
         double W2 = M*M - Q2 + 2.0*M*(Es-Ep);
         return( (W2-M*M)/(2.0*(M+Es*(1.0-Cos(theta))))) ;
      }

   }
}

