#include "PhotonDiffXSec.h"

namespace insane {
namespace physics {
PhotonDiffXSec::PhotonDiffXSec()
{
   fID         = 100001001;
   fBaseID     = 100001001;

   fTitle     = "N(#gamma,#pi)X";
   fPlotTitle = "#frac{d#sigma}{dE_{#pi} d#Omega_{#pi}} nb/GeV-Sr";

   fPIDs.clear();
   fPIDs.push_back(111);//+

   SetProductionParticleType(111);
   fRadiationLength = 0.001;

   /*      fParticleName = "PI0"; // argument for wiser fortran code*/
   /*      fParticle = TDatabasePDG::Instance()->GetParticle(111);//pi0 */
   // pdg code for pi0 see http://www.slac.stanford.edu/BFROOT/www/Computing/Environment/NewUser/htmlbug/node51.html
   std::vector<double> p;
   p.resize(8);

   p[0] = 61.2451;
   p[1] = 1.35525e-13;
   p[2] = 0.638272;
   p[3] = 0.784825;
   p[4] = -7.40602;
   p[5] = -6.37818;
   p[6] = -7.54664;
   p[7] = -22.101;

   fParameters.resize(7);
   fParameters[0] = p[0];// 5.66e2; // +- 0.19e2 
   fParameters[1] = p[1];// 8.29e2; // +- 0.81e2
   fParameters[2] = p[2];// 1.79  ; // +- 0.01     
   fParameters[3] = p[3];// 2.10  ; // +- 0.02     
   fParameters[4] = p[4];//-5.49  ; // +- 0.01     
   fParameters[5] = p[5];//-1.73  ; // +- 0.01     
   fParameters[6] = 0.0   ; // +- 0.01     

   fParameters0.resize(7); //  0
   fParameters1.resize(7); //  + 
   fParameters2.resize(7); //  -

   fParameters0 = fParameters;
   fParameters1 = fParameters;

   fParameters2[0] = p[0];// 4.86e2; // +- 0.19e2 ub/GeV^2  
   fParameters2[1] = p[1];// 1.15e2; // +- 0.81e2 ub/GeV^2  
   fParameters2[2] = p[2];// 1.77  ; // +- 0.01   ub/GeV^2  
   fParameters2[3] = p[3];// 2.18  ; // +- 0.02   ub/GeV^2  
   fParameters2[4] = p[6];//-5.23  ; // +- 0.01   ub/GeV^2  
   fParameters2[5] = p[7];//-1.82  ; // +- 0.01   ub/GeV^2  
   fParameters2[6] = 0.0   ; 

}
//_____________________________________________________________________________

PhotonDiffXSec::~PhotonDiffXSec()
{ }
//______________________________________________________________________________

PhotonDiffXSec::PhotonDiffXSec(const PhotonDiffXSec& rhs) : 
   InclusiveDiffXSec(rhs)
{
   (*this) = rhs; 
}
//______________________________________________________________________________

PhotonDiffXSec& PhotonDiffXSec::operator=(const PhotonDiffXSec& rhs)
{
   if (this != &rhs) {  // make sure not same object
      InclusiveDiffXSec::operator=(rhs);
      //fParticle        = rhs.fParticle        ;
      fRadiationLength = rhs.fRadiationLength ;
      fParameters      = rhs.fParameters      ;
      fParameters0     = rhs.fParameters0     ;
      fParameters1     = rhs.fParameters1     ;
      fParameters2     = rhs.fParameters2     ;
   }
   return *this;    // Return ref for multiple assignment
}
//______________________________________________________________________________

PhotonDiffXSec*  PhotonDiffXSec::Clone(const char * newname) const 
{
   std::cout << "PhotonDiffXSec::Clone()\n";
   auto * copy = new PhotonDiffXSec();
   (*copy) = (*this);
   return copy;
}
//______________________________________________________________________________

PhotonDiffXSec*  PhotonDiffXSec::Clone() const
{ 
   return( Clone("") );
} 
//______________________________________________________________________________

void PhotonDiffXSec::InitializePhaseSpaceVariables() {
   PhaseSpace * ps = GetPhaseSpace();
      auto * varEnergy2 = new PhaseSpaceVariable();
      varEnergy2 = new PhaseSpaceVariable();
      varEnergy2->SetNameTitle("energy_pi", "E_{#pi}");
      varEnergy2->SetMinimum(0.5); //GeV
      varEnergy2->SetMaximum(5.0); //GeV
      ps->AddVariable(varEnergy2);

      auto *   varTheta2 = new PhaseSpaceVariable();
      varTheta2->SetNameTitle("theta_pi", "#theta_{#pi}"); // ROOT string latex
      varTheta2->SetMinimum(25.0 * TMath::Pi() / 180.0); //
      varTheta2->SetMaximum(60.0 * TMath::Pi() / 180.0); //
      ps->AddVariable(varTheta2);

      auto *   varPhi2 = new PhaseSpaceVariable();
      varPhi2->SetNameTitle("phi_pi", "#phi_{#pi}"); // ROOT string latex
      varPhi2->SetMinimum(-75.0 * TMath::Pi() / 180.0); //
      varPhi2->SetMaximum( 75.0 * TMath::Pi() / 180.0); //
      ps->AddVariable(varPhi2);
      SetPhaseSpace(ps);
}
//______________________________________________________________________________

void PhotonDiffXSec::SetProductionParticleType(Int_t PDGcode, Int_t i)
{
   fParticle = TDatabasePDG::Instance()->GetParticle(PDGcode);
   if( fParticle ) {
      if(fParticle->Charge() < 0.0) {
         // negative charge
         fParameters = fParameters2;
      } else if( fParticle->Charge() > 0.0) {
         // positive charge
         fParameters = fParameters1;
      } else {
         // neutral
         fParameters = fParameters0;
      }
      fTitle = Form("Inclusive %s production ", fParticle->GetName());
   }
   SetParticleType(PDGcode,i);
   switch(PDGcode) {
      case  111 : fID = fBaseID + 0;   break;
      case -211 : fID = fBaseID + 100; break;
      case  211 : fID = fBaseID + 200; break;
      default   : fID = fBaseID + 300; break;
   }
   //fPIDs.clear();
   //fPIDs.push_back(PDGcode);

}
//______________________________________________________________________________
double PhotonDiffXSec::FitFunction(double * x, const std::vector<double>& p) const
{
   // Evaluate Cross Section (nbarn/GeV*str).
   double k0   = GetBeamEnergy(); 
   double E    = x[0]; 
   double th   = x[1]; 
   //double phi  = x[2]; 

   double P    = TMath::Sqrt(E*E-(M_pion*M_pion/GeV/GeV));//fParticle->GetMass();
   double s    = M_p*M_p/GeV/GeV+ 2.0*M_p/GeV*k0;
   double P_T  = P*TMath::Sin(th);;

   // Following wiser_fit subroutine:
   double B_CM     =  k0/(k0+M_p/GeV);                              // beta (lorentz trans)
   double GAM_CM   = 1.0/TMath::Sqrt(1.0-B_CM*B_CM);                // gamma (lorentz trans)
   double P_CM_L   = -GAM_CM *B_CM *E + GAM_CM * P *TMath::Cos(th); // x cm longitudinal momentum
   double P_CM     = TMath::Sqrt(P_CM_L*P_CM_L + P_T*P_T);          // X CM momentum
   double E_GAM_CM = GAM_CM*k0 - GAM_CM *B_CM*k0;                   // Photon CM momentum

   // Not sure what the point of the following was
   //      P_CM_MAX =SQRT (S +(M_X**2-MASS2(PARTICLE))**2/S 
   //     >    -2.*(M_X**2 +MASS2(PARTICLE)) )/2.
   // X_R is the ratio of the CM particle momentum to CM photon momentum
   //double X_R =  P_CM/E_GAM_CM 
   // x_R is "x-radial" in Wiser's thesis. 
   // x_R = P_L^{*}/P_max^{*}
   double x_R  = P_CM/E_GAM_CM ;
   double M_L  = TMath::Sqrt(P_T*P_T + M_pion*M_pion/GeV/GeV);
   double t1   = (p[0] + p[1]/TMath::Sqrt(s));
   double t2   = TMath::Power((1.0 - x_R + p[2]*p[2]/s), p[3]);
   double t3   = TMath::Exp(p[4]*M_L);
   double t4   = TMath::Exp(p[5]*P_T*P_T/E);
   //double t4   = TMath::Exp(p[5]*P_T);
   double result = t1*t2*t3*t4;
   //std::cout << " sig = " << result << "\n";
   
   // The result is for the cross section in the form E dsig/dp^3 
   // Here we calculate the Jacobian needed.
   // The first E/p is from dp/dE 
   // the second p^2 is takes the cross section dsig/dp/dOmega = p^2 dsig/dp^3 
   // the last 1/E removes the E in the the fit, ie, E dsig/dp^3
   double jac     = P;//(E/P)*(P*P/E);
   return( jac*result );
}
//______________________________________________________________________________
double PhotonDiffXSec::FitFunction2(double * x, const std::vector<double>& p) const
{
   // Evaluate Cross Section (nbarn/GeV*str).
   double k0   = GetBeamEnergy(); 
   double E    = x[0]; 
   double th   = x[1]; 
   //double phi  = x[2]; 

   double P    = TMath::Sqrt(E*E-(M_pion*M_pion/GeV/GeV));//fParticle->GetMass();
   double s    = M_p*M_p/GeV/GeV+ 2.0*M_p/GeV*k0;
   double P_T  = P*TMath::Sin(th);;

   // Following wiser_fit subroutine:
   double B_CM     =  k0/(k0+M_p/GeV);                              // beta (lorentz trans)
   double GAM_CM   = 1.0/TMath::Sqrt(1.0-B_CM*B_CM);                // gamma (lorentz trans)
   double P_CM_L   = -GAM_CM *B_CM *E + GAM_CM * P *TMath::Cos(th); // x cm longitudinal momentum
   double P_CM     = TMath::Sqrt(P_CM_L*P_CM_L + P_T*P_T);          // X CM momentum
   double E_GAM_CM = GAM_CM*k0 - GAM_CM *B_CM*k0;                   // Photon CM momentum
   double x_R  = P_CM/E_GAM_CM ;
   double M_L  = TMath::Sqrt(P_T*P_T + M_pion*M_pion/GeV/GeV);

   double t1   = (p[0] + p[1]*TMath::Sqrt(s));// + p[2]*s);
   double t2   = (p[2]+P_T*p[3])*(1.0 - x_R);
   double t3   = TMath::Exp(p[4]*P_T);
   //double t4   = TMath::Exp(p[5]*P_T*P_T/E);
   double t4   = TMath::Exp(p[5]*P_T*P_T);
   double result = t1*t2*t3*t4;

   //double t1     = p[0]*TMath::Exp(p[1]*P_T);
   //double t2     = p[2]*TMath::Exp(p[3]*P_T)/s;
   //double t3     = P*p[4]*TMath::Exp(p[5]*P_T);
   //double result = t1 + t2 + t3;
   // The result is for the cross section in the form E dsig/dp^3 
   // Here we calculate the Jacobian needed.
   // The first E/p is from dp/dE 
   // the second p^2 is takes the cross section dsig/dp/dOmega = p^2 dsig/dp^3 
   // the last 1/E removes the E in the the fit, ie, E dsig/dp^3
   double jac     = P;//(E/P)*(P*P/E);
   return( jac*result );
}
//______________________________________________________________________________
double PhotonDiffXSec::FitFunction3(double * x, const std::vector<double>& p) const
{
   // Evaluate Cross Section (nbarn/GeV*str).
   double k0   = GetBeamEnergy(); 
   double E    = x[0]; 
   double th   = x[1]; 
   //double phi  = x[2]; 

   double P    = TMath::Sqrt(E*E-(M_pion*M_pion/GeV/GeV));//fParticle->GetMass();
   double s    = M_p*M_p/GeV/GeV+ 2.0*M_p/GeV*k0;
   double P_T  = P*TMath::Sin(th);;

   // Following wiser_fit subroutine:
   double B_CM     =  k0/(k0+M_p/GeV);                              // beta (lorentz trans)
   double GAM_CM   = 1.0/TMath::Sqrt(1.0-B_CM*B_CM);                // gamma (lorentz trans)
   double P_CM_L   = -GAM_CM *B_CM *E + GAM_CM * P *TMath::Cos(th); // x cm longitudinal momentum
   double P_CM     = TMath::Sqrt(P_CM_L*P_CM_L + P_T*P_T);          // X CM momentum
   double E_GAM_CM = GAM_CM*k0 - GAM_CM *B_CM*k0;                   // Photon CM momentum
   double x_R      = P_CM/E_GAM_CM ;
   double M_L      = TMath::Sqrt(P_T*P_T + M_pion*M_pion/GeV/GeV);

   double t1   = (p[0] + p[1]*TMath::Sqrt(s) + p[2]*s);
   double t2   = TMath::Power(1.0 - x_R,p[3]);
   double t3   = TMath::Exp(p[4]*P_T);
   //double t4   = TMath::Exp(p[5]*P_T*P_T/E);
   double t4   = TMath::Exp(p[5]*P_T*P_T);
   double result = t1*t2*t3*t4;

   //double t1     = p[0]*TMath::Exp(p[1]*P_T);
   //double t2     = p[2]*TMath::Exp(p[3]*P_T)/s;
   //double t3     = P*p[4]*TMath::Exp(p[5]*P_T);
   //double result = t1 + t2 + t3;
   // The result is for the cross section in the form E dsig/dp^3 
   // Here we calculate the Jacobian needed.
   // The first E/p is from dp/dE 
   // the second p^2 is takes the cross section dsig/dp/dOmega = p^2 dsig/dp^3 
   // the last 1/E removes the E in the the fit, ie, E dsig/dp^3
   double jac     = P;//(E/P)*(P*P/E);
   return( jac*result );
}
//______________________________________________________________________________
double PhotonDiffXSec::FitFunction4(double * x, const std::vector<double>& p) const
{
   // Evaluate Cross Section (nbarn/GeV*str).
   double k0   = GetBeamEnergy(); 
   double E    = x[0]; 
   double th   = x[1]; 
   //double phi  = x[2]; 

   double P    = TMath::Sqrt(E*E-(M_pion*M_pion/GeV/GeV));//fParticle->GetMass();
   double s    = M_p*M_p/GeV/GeV+ 2.0*M_p/GeV*k0;
   double P_T  = P*TMath::Sin(th);;

   // Following wiser_fit subroutine:
   double B_CM     =  k0/(k0+M_p/GeV);                              // beta (lorentz trans)
   double GAM_CM   = 1.0/TMath::Sqrt(1.0-B_CM*B_CM);                // gamma (lorentz trans)
   double P_CM_L   = -GAM_CM *B_CM *E + GAM_CM * P *TMath::Cos(th); // x cm longitudinal momentum
   double P_CM     = TMath::Sqrt(P_CM_L*P_CM_L + P_T*P_T);          // X CM momentum
   double E_GAM_CM = GAM_CM*k0 - GAM_CM *B_CM*k0;                   // Photon CM momentum
   double x_R      = P_CM/E_GAM_CM ;
   double M_L      = TMath::Sqrt(P_T*P_T + M_pion*M_pion/GeV/GeV);


   //double arg = 2.0 - x_R + (P - 0.34)*p[2]/(1.0 + P);
   double arg      = 1.0 - x_R ;//+ s*p[2];
   if( arg < 0.0) return 0.0;
   if( x_R > 1.0) return 0.0;

   double x0 = 0.37;
   double b0 = p[4]/4.0;
   double b1 = p[4];

   double t1     = p[0]*(1.0 + p[1]*TMath::Sqrt(s));
   double t2     = TMath::Power( arg , p[3] );
   double t3     = TMath::Exp( b0*P_T );
   if( P_T > x0  ) t3 = TMath::Exp( b1*P_T + (b0 - b1)*x0 );
   double t4     = p[2]*TMath::Exp(p[5]*(P_T)*(P_T));
   double result = t1*t2*(t3+t4);

   //double t1     = p[0]*TMath::Exp(p[1]*P_T);
   //double t2     = p[2]*TMath::Exp(p[3]*P_T)/s;
   //double t3     = P*p[4]*TMath::Exp(p[5]*P_T);
   //double result = t1 + t2 + t3;
   // The result is for the cross section in the form E dsig/dp^3 
   // Here we calculate the Jacobian needed.
   // The first E/p is from dp/dE 
   // the second p^2 is takes the cross section dsig/dp/dOmega = p^2 dsig/dp^3 
   // the last 1/E removes the E in the the fit, ie, E dsig/dp^3
   if(result<0.0) return 0.0;
   double jac     = P;//(E/P)*(P*P/E);
   return( jac*result );
}
//______________________________________________________________________________
double PhotonDiffXSec::FitFunction5(double * x, const std::vector<double>& p) const
{
   // Evaluate Cross Section (nbarn/GeV*str).
   double k0   = GetBeamEnergy(); 
   double E    = x[0]; 
   double th   = x[1]; 
   //double phi  = x[2]; 

   double P    = TMath::Sqrt(E*E-(M_pion*M_pion/GeV/GeV));//fParticle->GetMass();
   double s    = M_p*M_p/GeV/GeV+ 2.0*M_p/GeV*k0;
   double P_T  = P*TMath::Sin(th);;

   // Following wiser_fit subroutine:
   double B_CM     =  k0/(k0+M_p/GeV);                              // beta (lorentz trans)
   double GAM_CM   = 1.0/TMath::Sqrt(1.0-B_CM*B_CM);                // gamma (lorentz trans)
   double P_CM_L   = -GAM_CM *B_CM *E + GAM_CM * P *TMath::Cos(th); // x cm longitudinal momentum
   double P_CM     = TMath::Sqrt(P_CM_L*P_CM_L + P_T*P_T);          // X CM momentum
   double E_GAM_CM = GAM_CM*k0 - GAM_CM *B_CM*k0;                   // Photon CM momentum
   double x_R      = P_CM/E_GAM_CM ;
   double M_L      = TMath::Sqrt(P_T*P_T + M_pion*M_pion/GeV/GeV);

   //double arg = 1.0 - x_R;
   //double arg      = 1.0 - x_R + s*p[5];
   double arg      = 1.0 + s*p[5];
   //if( arg < 0.0) return 0.0;

   double t1     = (p[0] + p[1]*TMath::Sqrt(s)); // + p[2]*th/degree);
   double t2     = TMath::Power( arg , p[3] );
   double t3     = TMath::Exp(p[4]*P_T);
   double t4     = 1.0;//TMath::Exp(p[5]*P_T*P_T);
   double result = t1*t2*t3*t4;

   //double t1     = p[0]*TMath::Exp(p[1]*P_T);
   //double t2     = p[2]*TMath::Exp(p[3]*P_T)/s;
   //double t3     = P*p[4]*TMath::Exp(p[5]*P_T);
   //double result = t1 + t2 + t3;
   // The result is for the cross section in the form E dsig/dp^3 
   // Here we calculate the Jacobian needed.
   // The first E/p is from dp/dE 
   // the second p^2 is takes the cross section dsig/dp/dOmega = p^2 dsig/dp^3 
   // the last 1/E removes the E in the the fit, ie, E dsig/dp^3
   double jac     = P;//(E/P)*(P*P/E);
   if(result<0.0) return 0.0;
   return( jac*result );
}
//______________________________________________________________________________
Double_t PhotonDiffXSec::EvaluateXSec(const Double_t * x) const {

   fArgCopy[0] = x[0];
   fArgCopy[1] = x[1];
   fArgCopy[2] = x[2];

   Double_t res = FitFunction4( fArgCopy, fParameters);

   if ( IncludeJacobian() ) return(res * TMath::Sin(x[1]) ); 
   return(res);
}
//______________________________________________________________________________

}}
