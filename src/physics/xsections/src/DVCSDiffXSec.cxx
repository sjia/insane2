#include "DVCSDiffXSec.h"
#include "PhysicalConstants.h"
#include <cmath>
#include "DVCS.h"

using namespace insane::physics;

DVCSDiffXSec::DVCSDiffXSec()
{
   fID            = 201000000;
   SetTitle("DVCSDiffXSec");
   SetPlotTitle("DVCS cross-section");
   fLabel         = "#frac{d#sigma}{dt dx dQ^{2} d#phi d#phi_{e'} }";
   fUnits         = "nb/sr/GeV^{4}";
   fnDim          = 5;
   fPIDs.clear();
   fnParticles    = 3;
   fPIDs.push_back(11);
   fPIDs.push_back(2212);
   fPIDs.push_back(22);
}
//______________________________________________________________________________

DVCSDiffXSec::~DVCSDiffXSec()
{ }
//______________________________________________________________________________

double DVCSDiffXSec::xsec_e1dvcs(const dvcsvar_t &x,const dvcspar_t &p) const
{
   //std::cout << "x.Q2" << x.Q2 << std::endl;
   //std::cout << "x.x" << x.x << std::endl;
   //std::cout << "x.t" << x.t << std::endl;
   if(x.Q2 < 0.5 ) return 0.0;
   if(x.x < 0.01) return 0.0;
   return pow(p.Q2_0/x.Q2,p.alpha)     *  1./pow(1.+p.b*x.t,p.beta) *
      1./(1.+pow((x.x-0.3)/p.c,2)) * (1.-p.d*(1.-cos(x.phi)));
}
//______________________________________________________________________________

void DVCSDiffXSec::InitializePhaseSpaceVariables()
{
   using namespace insane::units;
   PhaseSpace * ps = GetPhaseSpace();

   // ------------------------------
   // Electron
   auto * varEnergy = new PhaseSpaceVariable("energy_e", "E_{e'}",0.5, 5.0);
   varEnergy->SetParticleIndex(0);
   varEnergy->SetDependent(true);
   ps->AddVariable(varEnergy);

   auto * varTheta = new PhaseSpaceVariable("theta_e", "#theta_{e'}",5.0*degree, 180.0*degree);
   varTheta->SetParticleIndex(0);
   varTheta->SetDependent(true);
   ps->AddVariable(varTheta);

   auto * varPhi = new PhaseSpaceVariable("phi_e", "#phi_{e'}",-180.0*degree, 180.0*degree);
   varPhi->SetParticleIndex(0);
   varPhi->SetUniform(true);
   ps->AddVariable(varPhi);

   // ------------------------------
   // struck nucleon
   auto * varEnergy_p = new PhaseSpaceVariable("P_p", "P_{p}",0.001,4.0);
   varEnergy_p->SetParticleIndex(1);
   varEnergy_p->SetDependent(true);
   ps->AddVariable(varEnergy_p);

   auto * varTheta_p = new PhaseSpaceVariable("theta_p", "#theta_{p}",5.0*degree, 180.0*degree);
   varTheta_p->SetParticleIndex(1);
   varTheta_p->SetDependent(true);
   ps->AddVariable(varTheta_p);

   auto * varPhi_p = new PhaseSpaceVariable("phi_p", "#phi_{p}", -1.0*TMath::Pi(), 1.0*TMath::Pi() );
   varPhi_p->SetParticleIndex(1);
   varPhi_p->SetDependent(true);
   ps->AddVariable(varPhi_p);

   // ------------------------------
   // photon
   auto * varEnergy_g = new PhaseSpaceVariable("energy_gamma", "E_{#gamma}",0.3,11.0);
   varEnergy_g->SetParticleIndex(2);
   varEnergy_g->SetDependent(true);
   ps->AddVariable(varEnergy_g);

   auto * varTheta_g = new PhaseSpaceVariable("theta_gamma", "#theta_{#gamma}",2.0*degree, 170.0*degree);
   varTheta_g->SetParticleIndex(2);
   varTheta_g->SetDependent(true);
   ps->AddVariable(varTheta_g);

   auto * varPhi_g = new PhaseSpaceVariable("phi_gamma", "#phi_{#gamma}",-1.0*TMath::Pi(), 1.0*TMath::Pi() );
   varPhi_g->SetParticleIndex(2);
   varPhi_g->SetDependent(true);
   ps->AddVariable(varPhi_g);


   // -------------------------
   auto * var_x = new PhaseSpaceVariable("x", "x",0.02, 1.0);
   var_x->SetParticleIndex(-1);
   ps->AddVariable(var_x);

   auto * var_t = new PhaseSpaceVariable("t", "t",-2.0, 0.0);
   var_t->SetParticleIndex(-1);
   ps->AddVariable(var_t);

   auto * var_Q2 = new PhaseSpaceVariable("Q2", "Q^{2}",0.5, 6.0);
   var_Q2->SetParticleIndex(-1);
   ps->AddVariable(var_Q2);

   auto * var_phi = new PhaseSpaceVariable("phi", "#varphi",-180.0*degree, 180.0*degree);
   var_phi->SetParticleIndex(-1);
   ps->AddVariable(var_phi);

   //fNuclearTargetMass = GetTargetNucleus().GetMass();

   SetPhaseSpace(ps);
}
//______________________________________________________________________________

void DVCSDiffXSec::DefineEvent(Double_t * vars)
{

   Int_t totvars = 0;
   for (int i = 0; i < fParticles.GetEntries(); i++) {
      /// \todo fix this hard coding of 3 variables per event.
      /// here we are assuming the order E,theta,phi,then others
      /// \todo figure out how to handle vertex.
      insane::Kine::SetMomFromEThetaPhi((TParticle*)(fParticles.At(i)), &vars[totvars]);
      //((TParticle*)fParticles.At(i))->SetProductionVertex(GetRandomVertex());
      totvars += GetNParticleVars(i);
   }

}
//______________________________________________________________________________

Double_t * DVCSDiffXSec::GetDependentVariables(const Double_t * x) const
{
   using namespace TMath;
   using namespace insane::physics;

   double phi_e    =  x[0];
   double xBjorken =  x[1];
   double t        =  x[2];
   double Q2       =  x[3];
   double phi      =  x[4];

   double M       = M_p/GeV;
   double MT      = M_p/GeV;
   double E0      = GetBeamEnergy();
   double nu      = Q2/(2.0*M*xBjorken);
   double y       = nu/E0;
   double eprime  = E0 - nu;
   double theta_e = 2.0*ASin(M*xBjorken*y/Sqrt(Q2*(1-y)));
   double s       = M*M - Q2 + 2*M*nu; // W^2
   double nu_cm   = Sqrt(Power(M*nu-Q2,2.0)/(M*M+2.0*M*nu-Q2));
   double q_cm    = Sqrt(nu_cm*nu_cm+Q2);
   double t_min   = -Q2 - ((s-M*M)/(Sqrt(s)))*(nu_cm-q_cm);
   double t_max   = -Q2 - ((s-M*M)/(Sqrt(s)))*(nu_cm+q_cm);
   double ymax    = y_max(epsilon(Q2,xBjorken,M));

   //std::cout << t_min << " > t=" << t << " > " <<  t_max << std::endl;
   //std::cout << "Delta2_min " << Delta2_min(Q2,xBjorken,M) <<std::endl;
   //std::cout << ymax << " > y=" << y << std::endl;
   if(t<t_max) {
      return nullptr;
   }
   if(t>t_min) {
      return nullptr;
   }
   if( (y>ymax) || (y>1.0)){
      return nullptr;
   }
   if(std::isnan(theta_e)) return nullptr;

   TVector3 k1(0, 0, fBeamEnergy); // incident electron
   TVector3 k2(0, 0, eprime);          // scattered electron
   k2.SetMagThetaPhi(eprime, theta_e, phi_e);
   TVector3 q1 = k1 - k2;
   TVector3 n_gamma = k1.Cross(k2);
   n_gamma.SetMag(1.0);
   TVector3 q1_B = q1;
   q1_B.Rotate(q1.Theta(), n_gamma);
   //q1_B.Print();

   double E2_A  = (2.0*MT*MT-t)/(2.0*MT);
   double P2_A  = Sqrt(E2_A*E2_A - MT*MT);
   double nu2_A = MT + nu - E2_A;
   double cosTheta_qq2_B = (t+Q2+2.0*nu*nu2_A)/(2.0*nu2_A*q1.Mag());
   double theta_qq2_B    = ACos(cosTheta_qq2_B);
   double sintheta_qp2_B = nu2_A*Sin(theta_qq2_B)/Sqrt(E2_A*E2_A - MT*MT);
   double theta_qp2_B    = ASin(sintheta_qp2_B);
   double phi_p2 = q1_B.Phi() + phi;
   //std::cout << "P2_A = " << P2_A << std::endl;
   //std::cout << "E2_A = " << E2_A << std::endl;
   //std::cout << "t     = " << t << std::endl;
   //std::cout << "t_min = " << Delta2_min(Q2,xBjorken) << std::endl;
   TVector3 p2 = {0,0,1} ;
   TVector3 q2 = {0,0,1} ;
   p2.SetMagThetaPhi(P2_A, theta_qp2_B,phi_p2);
   q2.SetMagThetaPhi(nu2_A,theta_qq2_B,phi_p2+180.0*degree);

   q2.Rotate(-q1.Theta(), n_gamma);
   p2.Rotate(-q1.Theta(), n_gamma);
   q1.Rotate(-q1.Theta(), n_gamma);

   fDependentVariables[0] = eprime;
   fDependentVariables[1] = k2.Theta();
   fDependentVariables[2] = k2.Phi();
   fDependentVariables[3] = P2_A;
   fDependentVariables[4] = p2.Theta();
   fDependentVariables[5] = p2.Phi();
   fDependentVariables[6] = nu2_A;
   fDependentVariables[7] = q2.Theta();
   fDependentVariables[8] = k2.Phi();
   fDependentVariables[9]  = xBjorken;
   fDependentVariables[10] = t;
   fDependentVariables[11] = Q2;
   fDependentVariables[12] = phi;
   //std::cout << "vectors" << std::endl;
   //std::cout << "fDependentVariables[0] = " << fDependentVariables[0] << std::endl;
   //std::cout << "fDependentVariables[1] = " << fDependentVariables[1] << std::endl;
   //std::cout << "fDependentVariables[2] = " << fDependentVariables[2] << std::endl;
   //std::cout << "fDependentVariables[3] = " << fDependentVariables[3] << std::endl;
   //std::cout << "fDependentVariables[4] = " << fDependentVariables[4] << std::endl;
   //std::cout << "fDependentVariables[5] = " << fDependentVariables[5] << std::endl;
   //k1.Print();
   //k2.Print();
   //p2.Print();
   //if (p2.Phi() < 0.0) fDependentVariables[5] = p2.Phi() + 2.0 * TMath::Pi() ;
   auto net_p = (k1 - k2 - p2 - q2);
   if( net_p.Mag() > 0.001 ) {
      //net_p.Print();
      return nullptr;
   }
   return(fDependentVariables);
}
//______________________________________________________________________________
Double_t DVCSDiffXSec::GetEPrime(const Double_t theta)const  {
   // Returns the scattered electron energy using the angle. */
   Double_t M  = fTargetNucleus.GetMass();//M_p/GeV;
   return (fBeamEnergy / (1.0 + 2.0 * fBeamEnergy / M * TMath::Power(TMath::Sin(theta / 2.0), 2)));
}
//________________________________________________________________________

Double_t  DVCSDiffXSec::EvaluateXSec(const Double_t * x) const
{
   using namespace TMath;
   using namespace insane::physics;

   if(!x) return(0.0);
   // Get the recoiling proton momentum
   if (!VariablesInPhaseSpace(9, x)) return(0.0);

   double beamSign = 1.0; // negative for a positron beam
   double xB       = x[9];
   double t        = x[10];
   double Q2       = x[11];
   double phi      = x[12];
   double M        = M_p/GeV;
   double E0       = GetBeamEnergy();
   double eps      = epsilon(Q2,xB,M);
   double nu       = Q2/(2.0*M*xB);
   double y        = nu/E0;
   double Delta2   = t;
   double xi       = xi_BKM(xB,Q2,Delta2);

   double t0 = Power(1.0/137.0,3.0)*xB/(16.0*pi*pi*Q2*Q2*Sqrt(1.0+eps*eps));

   double F1  = fFormFactors->F1p(Q2);
   double F2  = fFormFactors->F2p(Q2);

   DVCS_KinematicVariables dvcs_vars { xB, Q2, t, phi, xi, E0, M, y, nu, eps};
   DVCS_FormFactors        dvcs_ffs;
   dvcs_ffs.F1     = F1;
   dvcs_ffs.F2     = F2;
   dvcs_ffs.H      = 0.0;
   dvcs_ffs.Htilde = 0.0;

   //double p1 = P1(E0, Q2, xB, Delta2, phi, M);
   //double p2 = P2(E0, Q2, xB, Delta2, phi, M);
   dvcs_vars.K = K_DVCS(dvcs_vars);
   dvcs_vars.J = J_DVCS(dvcs_vars);
   double p1 = P1(dvcs_vars);
   double p2 = P2(dvcs_vars);

   // -------------------------------
   // BH
   double c0BH = c0_BH_unp(dvcs_vars, dvcs_ffs);
   double c1BH = c1_BH_unp(dvcs_vars, dvcs_ffs);
   double c2BH = c2_BH_unp(dvcs_vars, dvcs_ffs);

   double den_BH = Power(xB*(1.0+eps*eps),2.0)*Delta2*p1*p2;
   double num_BH = c0BH + c1BH*Cos(phi) + c2BH*Cos(2.0*phi);

   // -------------------------------
   // DVCS 
   double c0DVCS = c0_DVCS_unp(dvcs_vars, dvcs_ffs);
   double c1DVCS = c1_DVCS_unp(dvcs_vars, dvcs_ffs);
   double c2DVCS = c2_DVCS_unp(dvcs_vars, dvcs_ffs);
   double s1DVCS = s1_DVCS_unp(dvcs_vars, dvcs_ffs);
   double s2DVCS = s2_DVCS_unp(dvcs_vars, dvcs_ffs);
   double den_DVCS = Q2;
   double num_DVCS = c0DVCS + c1DVCS*Cos(phi) + c2DVCS*Cos(2.0*phi) + s1DVCS*Sin(phi) + s2DVCS*Sin(2.0*phi);

   // -------------------------------
   // Interference
   double c0I = c0_I_unp(dvcs_vars, dvcs_ffs);
   double c1I = c1_I_unp(dvcs_vars, dvcs_ffs);
   double c2I = c2_I_unp(dvcs_vars, dvcs_ffs);
   double s1I = s1_I_unp(dvcs_vars, dvcs_ffs);
   double s2I = s2_I_unp(dvcs_vars, dvcs_ffs);
   double den_I = xB*y*y*y*Delta2*p1*p2;
   double num_I = beamSign*(c0I + c1I*Cos(phi) + c2I*Cos(2.0*phi) + s1I*Sin(phi) + s2I*Sin(2.0*phi));
   
   double sig_BH = t0*(num_BH/den_BH + num_DVCS/den_DVCS + num_I/den_I);

   ////std::cout << " Qsq= " << Qsquared;
   ////std::cout << " tau= " << tau;
   ////std::cout << " GE2= " << GE2;
   ////std::cout << " GM2= " << GM2  << "\n";;

   double jac = 1.0;
   if( IncludeJacobian() ) {
      jac = Jacobian(x);
   }

   double res = sig_BH*jac*TMath::Power(hbarc2_gev_nb,1.5);
   
   return(res);
}
//________________________________________________________________________

Double_t   DVCSDiffXSec::GetBeamSpinAsymmetry(const Double_t * x) const
{
   double xBjorken = x[10];
   double t        = x[11];
   double Q2       = x[12];
   double phi      = x[13];
   double P_p1     = x[14];
   double Asym     = 0.4*TMath::Sin(phi);
   //std::cout << " Asym = " << Asym << std::endl;
   return Asym;
}
//______________________________________________________________________________

double DVCSDiffXSec::Jacobian(const double * vars) const
{
   using namespace TMath;
   auto Csc = [](double x){ return 1.0/TMath::Sin(x) ; };
   auto ArcCos = [](double x){ return TMath::ACos(x) ; };
   double M = M_p/GeV;
   double k1 = fBeamEnergy;

   double k2      = vars[0];
   double thetak2 = vars[1];
   double phik2   = vars[2];

   double q2      = vars[6];
   double thetaq2 = vars[7];
   double phiq2   = vars[8];

   double res = 
(4.*Power(k1,2)*Power(k2,3)*(1. - 1.*Cos(thetak2))*(k1*(k2 - M) + \
k2*M - k1*k2*Cos(thetak2))*Power(Sin(thetak2),2)*Sin(thetaq2)*Power(\
k1 - k2 + M - k1*Cos(thetaq2) + k2*Cos(thetak2)*Cos(thetaq2) + \
k2*Cos(phik2 - \
phiq2)*Sin(thetak2)*Sin(thetaq2),2)*(k2*Power(Sin(phik2 - \
phiq2),2)*Sin(thetak2)*(Power(k1,2) - 2*k1*k2*Cos(thetak2) + \
Power(k2,2)*Power(Cos(thetak2),2) + \
Power(k2,2)*Power(Sin(thetak2),2))*Power(Sin(thetaq2),2) + \
ArcCos((k2*Sin(thetak2)*(k2*Cos(thetaq2)*Sin(thetak2) + Cos(phik2 - \
phiq2)*(k1 - k2*Cos(thetak2))*Sin(thetaq2)))/(Power(k1,2) + \
Power(k2,2) - 2*k1*k2*Cos(thetak2)))*(k2*Power(Cos(phik2 - \
phiq2),2)*Cos(thetaq2)*Sin(thetak2) + k2*Cos(thetaq2)*Power(Sin(phik2 \
- phiq2),2)*Sin(thetak2) + Cos(phik2 - phiq2)*(k1 - \
k2*Cos(thetak2))*Sin(thetaq2))*Sqrt(Power(k1,4) + \
2*Power(k1,2)*Power(k2,2) + Power(k2,4) - \
Power(k2,4)*Power(Cos(thetaq2),2)*Power(Sin(thetak2),4) - \
2*k1*Power(k2,3)*Cos(phik2 - \
phiq2)*Cos(thetaq2)*Power(Sin(thetak2),3)*Sin(thetaq2) - \
Power(k1,2)*Power(k2,2)*Power(Cos(phik2 - \
phiq2),2)*Power(Sin(thetak2),2)*Power(Sin(thetaq2),2) + \
Cos(thetak2)*(-4*k1*k2*(Power(k1,2) + Power(k2,2)) + \
2*Power(k2,4)*Cos(phik2 - \
phiq2)*Cos(thetaq2)*Power(Sin(thetak2),3)*Sin(thetaq2) + \
2*k1*Power(k2,3)*Power(Cos(phik2 - \
phiq2),2)*Power(Sin(thetak2),2)*Power(Sin(thetaq2),2)) + \
Power(Cos(thetak2),2)*(4*Power(k1,2)*Power(k2,2) - \
Power(k2,4)*Power(Cos(phik2 - \
phiq2),2)*Power(Sin(thetak2),2)*Power(Sin(thetaq2),2)))))/(Power(k1 - \
1.*k2,2)*Power(Power(k1,2) + Power(k2,2) - \
2*k1*k2*Cos(thetak2),1.5)*Power(-1.*k1 + k2 - 1.*M + (k1 - \
1.*k2*Cos(thetak2))*Cos(thetaq2) - 1.*k2*Cos(phik2 - \
phiq2)*Sin(thetak2)*Sin(thetaq2),4)*Sqrt(1 - \
(Power(k2,2)*Power(Sin(thetak2),2)*Power(k2*Cos(thetaq2)*Sin(thetak2) \
+ Cos(phik2 - phiq2)*(k1 - \
k2*Cos(thetak2))*Sin(thetaq2),2))/Power(Power(k1,2) + Power(k2,2) - \
2*k1*k2*Cos(thetak2),2)));
   return res;
}


