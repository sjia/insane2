#include "CTEQ6eInclusiveDiffXSec.h"

namespace insane {
namespace physics {

//______________________________________________________________________________
CTEQ6eInclusiveDiffXSec::CTEQ6eInclusiveDiffXSec()
{
   fID = 100000011;
   // Set the unpolarized structure functions 
   auto *CTEQuPDF             = new CTEQ6UnpolarizedPDFs();
   auto * CTEQuSF = new StructureFunctionsFromPDFs();
   CTEQuSF->SetUnpolarizedPDFs(CTEQuPDF);
   fStructureFunctions = CTEQuSF; 

}
//______________________________________________________________________________
CTEQ6eInclusiveDiffXSec::~CTEQ6eInclusiveDiffXSec(){


}
//______________________________________________________________________________
Double_t CTEQ6eInclusiveDiffXSec::EvaluateXSec(const Double_t *x) const{

   // for inelastic scattering 
   Double_t M    = M_p/GeV;            // in GeV 
   Double_t Ep   = x[0];               // scattered electron energy in GeV 
   Double_t th   = x[1];               // electron scattering angle in radians 
   Double_t Nu   = fBeamEnergy - Ep;
   Double_t SIN  = TMath::Sin(th/2.);
   Double_t SIN2 = SIN*SIN;
   Double_t COS2 = 1. - SIN2; 
   Double_t TAN2 = SIN2/COS2; 
   Double_t Q2   = 4.0*fBeamEnergy*Ep*SIN2;
   Double_t xBj  = Q2/(2.0*M*Nu);

   Double_t A    = GetA();
   Double_t Z    = GetZ();

   // Calculate structure functions 
   Double_t F1,F2; 
   if( (A==1)&&(Z==1) ){
      // proton, N = A-Z = 0  
      F1 = fStructureFunctions->F1p(xBj,Q2); 
      F2 = fStructureFunctions->F2p(xBj,Q2); 
   }else if( (A==1)&&(Z==0) ){
      // neutron, N = A-Z = 1
      F1 = fStructureFunctions->F1n(xBj,Q2); 
      F2 = fStructureFunctions->F2n(xBj,Q2); 
   }else if( (A==2)&&(Z==1) ){
      // Remember that the StructureFunctions return results PER NUCLEON 
      // So we multiply by A here
      // deuteron, N = A-Z = 1
      F1 = A*fStructureFunctions->F1d(xBj,Q2); 
      F2 = A*fStructureFunctions->F2d(xBj,Q2); 
   }else if( (A==3)&&(Z==2) ){
      // 3He, N = A-Z = 1
      F1 = A*fStructureFunctions->F1He3(xBj,Q2); 
      F2 = A*fStructureFunctions->F2He3(xBj,Q2); 
   } else {
      Error("EvaluateXSec","Invalid target (or not functional yet). ");
      std::cout << " Use CompositeDiffXSec Instead ... using proton for now " << std::endl;
      F1 = fStructureFunctions->F1p(xBj,Q2);
      F2 = fStructureFunctions->F2p(xBj,Q2);
   }
   Double_t W1 = (1./M)*F1;
   Double_t W2 = (1./Nu)*F2;
   // compute the Mott cross section (units = mb): 
   //Double_t hbarc2 = 0.38939129; // (hbar*c)^2 = 0.38939129 mb*GeV^2  
   Double_t alpha  = 1./137.;
   Double_t num    = alpha*alpha*COS2; 
   Double_t den    = 4.*fBeamEnergy*fBeamEnergy*SIN2*SIN2; 
   Double_t MottXS = num/den;
   // compute the full cross section (units = nb/GeV/sr) 
   Double_t fullXsec = MottXS*(W2 + 2.0*TAN2*W1)*hbarc2_gev_nb;

   if ( IncludeJacobian() ) return fullXsec*TMath::Sin(th);
   return fullXsec;

}
//______________________________________________________________________________
}}
