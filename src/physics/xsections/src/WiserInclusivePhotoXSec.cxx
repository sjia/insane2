#include "WiserInclusivePhotoXSec.h"

#include "BremsstrahlungRadiator.h"

namespace insane {
namespace physics {

WiserInclusivePhotoXSec::WiserInclusivePhotoXSec()
{
   fID         = 100002002;
}
//______________________________________________________________________________

WiserInclusivePhotoXSec::~WiserInclusivePhotoXSec()
{ }
//______________________________________________________________________________

Double_t WiserInclusivePhotoXSec::EvaluateXSec(const Double_t * x) const 
{
   //if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);
   double RES            = 0.0;
   auto    PART           = (int)fWiserParticleCode;
   if( GetTargetNucleus() == Nucleus::Neutron() ) {
      // if target is a neutron use isospin to get the result
      if(fParticle->PdgCode() != 111)  // make sure it is not set to pi0
         PART = GetWiserType(-1*fParticle->PdgCode());
   } else if( !(GetTargetNucleus() == Nucleus::Proton()) ) {
      std::cout << " NOT Proton or neutron" << std::endl;
   }

   double EBEAM          = GetBeamEnergy();
   double Epart          = x[0];
   double Ppart          = TMath::Sqrt(Epart*Epart - M_pion*M_pion/(GeV*GeV));
   double THETA          = x[1];
   double radlen         = fRadiationLength;

   // Minimum photon energy for photoproduction in gamma+p -> x+n
   // where x is a hadron and n is a nucleon
   // Ex and thetax are the produced hadron's energy and angle
   // mx is the hadron mass, mt is the target mass, mn is the recoil nucleon mass.
   double k_min = insane::Kine::k_min_photoproduction(Epart, THETA);//, M_pion/GeV,fTargetNucleus.GetMass() );//, mx, double mt, double mn) 

   if(k_min<0.0) k_min=0.0;

   // simple integration
   int    Nint    = 50;
   double delta_w = (EBEAM-k_min)/double(Nint);
   double w       = 0.0;
   double Igam    = 0.0;
   double tot     = 0.0;
   double U = 0.0;
   if(PART==0){
      // pi0 = (pi+ + pi-)/2.0
      for(int i = 0;i<Nint; i++){

         w      = k_min + (double(i)+0.5)*delta_w;
         Igam   = insane::Kine::I_gamma_1_approx(fRadiationLength,EBEAM,w);

         // pi+
         PART   =1;
         wiser_fit_(&PART, &Ppart, &THETA, &w, &RES);
         if(RES<0.0) RES = 0.0;
         RES  *= (delta_w*Igam);
         tot  += RES;

         // pi-
         PART  =2;
         wiser_fit_(&PART, &Ppart, &THETA, &w, &RES);
         if(RES<0.0) RES = 0.0;
         RES  *= (delta_w*Igam);
         tot  += RES;
         U    += w*Igam*delta_w; 
      }
      tot = tot/2.0;
   }else {
      for(int i = 0;i<Nint; i++){
         w = k_min + (double(i)+0.5)*delta_w;
         wiser_fit_(&PART, &Ppart, &THETA, &w, &RES);
         if(RES<0.0) RES = 0.0;
         Igam   = insane::Kine::I_gamma_1_approx(fRadiationLength,EBEAM,w);
         RES  *= (delta_w*Igam);
         tot  += RES;
         U    += w*Igam*delta_w; 
      }
   }

   //double U  = insane::Kine::I_gamma_1_k_avg(fRadiationLength,0.01,EBEAM);
   U  = insane::Kine::I_gamma_1_k_avg(fRadiationLength,0.01,EBEAM);
   double EQ = U/EBEAM;
   //std::cout << " U  = " << U  << std::endl;
   //std::cout << " EQ = " << EQ << std::endl;

   //tot *= (Ppart); // Takes dsig/dp^3 to dsig/dEdOmega 
   tot *= 1000.0; // converts ub to nb
   tot /= EQ; // cross section per equivalent quant 
   if( TMath::IsNaN(tot) ) tot = 0.0;
   //std::cout << " wiser result is " << tot << " nb/GeV*str " << std::endl;
   //<< " for p,theta " << PscatteredPart << "," << THETA << "\n";
   if ( IncludeJacobian() ) return(tot * TMath::Sin(x[1]) ); 
   return(tot); // converts nb to mb
}
//______________________________________________________________________________


//==============================================================================

WiserInclusivePhotoXSec2::WiserInclusivePhotoXSec2()
{
   fID         = 100002003;
}
//______________________________________________________________________________

WiserInclusivePhotoXSec2::~WiserInclusivePhotoXSec2()
{
}
//______________________________________________________________________________

Double_t WiserInclusivePhotoXSec2::EvaluateXSec(const Double_t * x) const 
{
   //if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);
   double RES            = 0.0;
   auto    PART           = (int)fWiserParticleCode;

   if( GetTargetNucleus() == Nucleus::Neutron() ) {
      // if target is a neutron use isospin to get the result
      if(fParticle->PdgCode() != 111)  // make sure it is not set to pi0
         PART = GetWiserType(-1*fParticle->PdgCode());
      //std::cout << "Neutron" << std::endl;
   }
   else if( !(GetTargetNucleus() == Nucleus::Proton()) ) {
      std::cout << " NOT Proton or neutron" << std::endl;
   }
   //else std::cout << "Proton" << std::endl;
   double EBEAM          = GetBeamEnergy();
   double Epart          = x[0];
   double Ppart          = TMath::Sqrt(Epart*Epart - M_pion*M_pion/(GeV*GeV));
   double THETA          = x[1];
   double radlen         = fRadiationLength;

      // Minimum photon energy for photoproduction in gamma+p -> x+n
      // where x is a hadron and n is a nucleon
      // Ex and thetax are the produced hadron's energy and angle
      // mx is the hadron mass, mt is the target mass, mn is the recoil nucleon mass.
   double k_min = insane::Kine::k_min_photoproduction(Epart, THETA);//, M_pion/GeV,fTargetNucleus.GetMass() );//, mx, double mt, double mn) 
   if(k_min > EBEAM ) return 0.0;

   TargetMaterial           mat   = GetTargetMaterial();
   BremsstrahlungRadiator * brem  = mat.GetBremRadiator();
   Int_t                          matID = mat.GetMatID();

   if(k_min<0.0) k_min=0.0;
   //std::cout << "k_min/k0 = " << k_min/EBEAM << std::endl;
   // simple integration
   int Nint       = 100;
   double delta_w = (EBEAM-k_min)/double(Nint);
   double w       = 0.0;
   double Igam    = 0.0;
   double Igam2    = 0.0;
   double tot     = 0.0;
   double U = 0.0;

   // The first E/p is from dp/dE 
   // the second p^2 is takes the cross section dsig/dp/dOmega = p^2 dsig/dp^3 
   // the last 1/E removes the E in the the fit, ie, E dsig/dp^3
   double jac     = (Epart/Ppart)*(Ppart*Ppart/Epart);

   if(PART==0){
      // pi0 = (pi+ + pi-)/2.0

      for(int i = 0;i<Nint; i++){
         w      = k_min + (double(i)+0.5)*delta_w;

         if(brem) Igam = brem->I_gamma( matID, w, EBEAM );
         else     Igam = insane::Kine::I_gamma_1_approx(fRadiationLength,EBEAM,w);
         // pi+
         PART   =1;
         wiser_fit_(&PART, &Ppart, &THETA, &w, &RES);
         if(RES<0.0) RES = 0.0;
         RES  *= (jac*delta_w*Igam);
         tot  += RES;
         // pi-
         PART  =2;
         wiser_fit_(&PART, &Ppart, &THETA, &w, &RES);
         if(RES<0.0) RES = 0.0;
         RES  *= (jac*delta_w*Igam);
         tot  += RES;
      }
      tot = tot/2.0;

   } else {

      for(int i = 0;i<Nint; i++){

         w = k_min + (double(i)+0.5)*delta_w;
         wiser_fit_(&PART, &Ppart, &THETA, &w, &RES);
         if(RES<0.0) RES = 0.0;

         if(brem) Igam = brem->I_gamma( matID, w, EBEAM );
         else     Igam = insane::Kine::I_gamma_1_approx(fRadiationLength,EBEAM,w);

         RES  *= (jac*delta_w*Igam);
         tot  += RES;
      }
   }
   U  = insane::Kine::I_gamma_1_k_avg(fRadiationLength,0.0001,EBEAM);
   double EQ = U/EBEAM;

   tot *= 1000.0; // converts ub to nb
   tot /= EQ; // cross section per equivalent quant 
   if( TMath::IsNaN(tot) ) tot = 0.0;
   //std::cout << " wiser result is " << tot << " nb/GeV*str " << std::endl;
   //<< " for p,theta " << PscatteredPart << "," << THETA << "\n";
   if ( IncludeJacobian() ) return(tot * TMath::Sin(x[1]) ); 
   return(tot); // converts nb to mb
}
//______________________________________________________________________________


//==============================================================================

PhotoWiserDiffXSec::PhotoWiserDiffXSec()
{
   fID         = 100102002;
   fTitle = "PhotoWiser";
   fPlotTitle = "#frac{d#sigma}{dE_{#pi} d#Omega_{#pi}} nb/GeV-Sr";
   fPIDs.clear();
   fPIDs.push_back(111);//
   fProtonXSec  = new WiserInclusivePhotoXSec();
   fProtonXSec->SetTargetNucleus(Nucleus::Proton());
   fNeutronXSec = new WiserInclusivePhotoXSec();
   fNeutronXSec->SetTargetNucleus(Nucleus::Neutron());
   fProtonXSec->UsePhaseSpace(false);
   fNeutronXSec->UsePhaseSpace(false);
   //std::cout << "done" << std::endl;
}
//_____________________________________________________________________________

PhotoWiserDiffXSec::~PhotoWiserDiffXSec()
{
}
//______________________________________________________________________________

Double_t  PhotoWiserDiffXSec::EvaluateXSec(const Double_t * x) const
{

   //std::cout << " composite eval " << std::endl;
   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);

   if( !fProtonXSec )  return 0.0;
   if( !fNeutronXSec ) return 0.0;

   Double_t z    = GetZ();
   //if( z>0.0 ) z = 1.0;
   Double_t n    = GetN();
   //if( n>0.0 ) n = 1.0;

   Double_t sigP = fProtonXSec->EvaluateXSec(x);
   Double_t sigN = fNeutronXSec->EvaluateXSec(x);
   Double_t xsec = z*sigP + n*sigN;
   //std::cout << " Z  = " << GetZ() << std::endl;
   //std::cout << "sp  = " << sigP << std::endl;
   //std::cout << " N  = " << GetN() << std::endl;
   //std::cout << "sn  = " << sigN << std::endl;

   //std::cout << " composite eval " <<  xsec << std::endl;
   //if ( IncludeJacobian() ) return xsec*TMath::Sin(th);
   return xsec;

}
//______________________________________________________________________________


//==============================================================================

PhotoWiserDiffXSec2::PhotoWiserDiffXSec2(){
   fID         = 100102003;
   fTitle = "PhotoWiser";
   fPlotTitle = "#frac{d#sigma}{dE_{#pi} d#Omega_{#pi}} nb/GeV-Sr";
   fPIDs.clear();
   fPIDs.push_back(111);//
   fProtonXSec  = new WiserInclusivePhotoXSec2();
   fProtonXSec->SetTargetNucleus(Nucleus::Proton());
   fNeutronXSec = new WiserInclusivePhotoXSec2();
   fNeutronXSec->SetTargetNucleus(Nucleus::Neutron());
   fProtonXSec->UsePhaseSpace(false);
   fNeutronXSec->UsePhaseSpace(false);
   //std::cout << "done" << std::endl;
}
//_____________________________________________________________________________

PhotoWiserDiffXSec2::~PhotoWiserDiffXSec2()
{
}
//______________________________________________________________________________

Double_t  PhotoWiserDiffXSec2::EvaluateXSec(const Double_t * x) const
{

   //std::cout << " composite eval " << std::endl;
   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);

   if( !fProtonXSec )  return 0.0;
   if( !fNeutronXSec ) return 0.0;

   Double_t z    = GetZ();
   //if( z>0.0 ) z = 1.0;
   Double_t n    = GetN();
   //if( n>0.0 ) n = 1.0;

   Double_t sigP = fProtonXSec->EvaluateXSec(x);
   Double_t sigN = fNeutronXSec->EvaluateXSec(x);
   Double_t xsec = z*sigP + n*sigN;
   //std::cout << " Z  = " << GetZ() << std::endl;
   //std::cout << "sp  = " << sigP << std::endl;
   //std::cout << " N  = " << GetN() << std::endl;
   //std::cout << "sn  = " << sigN << std::endl;

   //std::cout << " composite eval " <<  xsec << std::endl;
   //if ( IncludeJacobian() ) return xsec*TMath::Sin(th);
   return xsec;

}
//______________________________________________________________________________
}}
