#include "CompositeDiffXSec.h"

//______________________________________________________________________________
namespace  insane {
namespace physics {

CompositeDiffXSec::CompositeDiffXSec()
{
   fID          = 100100000;
   fProtonXSec  = nullptr;
   fNeutronXSec = nullptr;
   fTitle = "CompositeDiffXSec";
   fPlotTitle = "#frac{d#sigma}{dE d#Omega} nb/GeV-Sr";

   fProtonXSec  = new InclusiveDiffXSec();
   fProtonXSec->SetTargetNucleus(Nucleus::Proton());

   fNeutronXSec = new InclusiveDiffXSec();
   fNeutronXSec->SetTargetNucleus(Nucleus::Neutron());

   fNeutronXSec->UsePhaseSpace(false);
   fProtonXSec->UsePhaseSpace(false);
}
//______________________________________________________________________________
CompositeDiffXSec::~CompositeDiffXSec()
{
}
//_____________________________________________________________________________

CompositeDiffXSec::CompositeDiffXSec(const CompositeDiffXSec& rhs) : 
   InclusiveDiffXSec(rhs)
{
   (*this) = rhs;
}
//______________________________________________________________________________

CompositeDiffXSec& CompositeDiffXSec::operator=(const CompositeDiffXSec& rhs) 
{
   if (this != &rhs) {  // make sure not same object
      InclusiveDiffXSec::operator=(rhs);
      fProtonXSec     = rhs.fProtonXSec->Clone();
      fNeutronXSec     = rhs.fNeutronXSec->Clone();
   }
   return *this;    // Return ref for multiple assignment
}
//______________________________________________________________________________

CompositeDiffXSec*  CompositeDiffXSec::Clone(const char * newname) const 
{
   std::cout << "CompositeDiffXSec::Clone()\n";
   auto * copy = new CompositeDiffXSec();
   (*copy) = (*this);
   return copy;
}
//______________________________________________________________________________

CompositeDiffXSec*  CompositeDiffXSec::Clone() const
{ 
   return( Clone("") );
} 
//______________________________________________________________________________
Double_t  CompositeDiffXSec::EvaluateXSec(const Double_t * x) const{

   //std::cout << " composite eval " << std::endl;
   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);

   if( !fProtonXSec )  return 0.0;
   if( !fNeutronXSec ) return 0.0;
   Double_t z    = GetZ();
   //if( z>0.0 ) z = 1.0; // hack for wiser... overridden in wiser codes
   Double_t n    = GetN();
   //if( n>0.0 ) n = 1.0;
   Double_t a =  z+n;

   Double_t xbj = insane::Kine::xBjorken_EEprimeTheta(GetBeamEnergy(),x[0],x[1]);
   Double_t emc = EMC_Effect(xbj,a);

   Double_t sigP = fProtonXSec->EvaluateXSec(x);
   Double_t sigN = fNeutronXSec->EvaluateXSec(x);
   Double_t xsec = (sigP + sigN)*(a/2.0);
   //std::cout << " Z  = " << GetZ() << std::endl;
   //std::cout << "sp  = " << sigP << std::endl;
   //std::cout << " N  = " << GetN() << std::endl;
   //std::cout << "sn  = " << sigN << std::endl;

   //std::cout << " composite eval " <<  xsec << std::endl;
   //if ( IncludeJacobian() ) return xsec*TMath::Sin(th);
   if(TMath::IsNaN(xsec)) xsec = 0.0;
   if(xsec < 0.0) xsec = 0.0; 
   return xsec;
	
}
//______________________________________________________________________________
void       CompositeDiffXSec::SetTargetMaterial(const TargetMaterial& mat){
   if( fProtonXSec ) {
      fProtonXSec->SetTargetMaterial(mat);
      fProtonXSec->SetTargetNucleus(Nucleus::Proton());
   }
   if( fNeutronXSec ) {
      fNeutronXSec->SetTargetMaterial(mat);
      fNeutronXSec->SetTargetNucleus(Nucleus::Neutron());
   }
   fTargetMaterial = mat; fTargetNucleus = fTargetMaterial.GetNucleus();
}
//______________________________________________________________________________
void CompositeDiffXSec::SetTargetNucleus(const Nucleus & targ){
   InclusiveDiffXSec::SetTargetNucleus(targ);
   //if(fProtonXSec)   fProtonXSec->SetTargetNucleus(targ);
   //if(fNeutronXSec) fNeutronXSec->SetTargetNucleus(targ);
}
//______________________________________________________________________________
void       CompositeDiffXSec::SetTargetMaterialIndex(Int_t i){
   if(fProtonXSec)   fProtonXSec->SetTargetMaterialIndex(i);
   if(fNeutronXSec) fNeutronXSec->SetTargetMaterialIndex(i);
   fMaterialIndex = i ;
}
//______________________________________________________________________________
void  CompositeDiffXSec::SetUnits(const char * t){
   fUnits = t;
   if(fProtonXSec)   fProtonXSec->SetUnits(t);
   if(fNeutronXSec) fNeutronXSec->SetUnits(t);
}
//______________________________________________________________________________
void  CompositeDiffXSec::SetBeamEnergy(Double_t en){
   InclusiveDiffXSec::SetBeamEnergy(en);
   if(fProtonXSec) fProtonXSec->SetBeamEnergy(en);
   if(fNeutronXSec) fNeutronXSec->SetBeamEnergy(en);
}
//______________________________________________________________________________
void  CompositeDiffXSec::SetPhaseSpace(PhaseSpace * ps){
   if(fProtonXSec)  fProtonXSec->SetPhaseSpace(ps);
   if(fProtonXSec)  InclusiveDiffXSec::SetPhaseSpace(fProtonXSec->GetPhaseSpace());
   if(fNeutronXSec) fNeutronXSec->SetPhaseSpace(ps);
}
//______________________________________________________________________________
void   CompositeDiffXSec::InitializePhaseSpaceVariables(){
   if(fProtonXSec)  fProtonXSec->InitializePhaseSpaceVariables();
   if(fNeutronXSec) fNeutronXSec->InitializePhaseSpaceVariables();
   if(fProtonXSec)  InclusiveDiffXSec::SetPhaseSpace(fProtonXSec->GetPhaseSpace());
   if(fNeutronXSec)  fNeutronXSec->SetPhaseSpace(fProtonXSec->GetPhaseSpace());
   //InclusiveDiffXSec::InitializePhaseSpaceVariables();
}
//______________________________________________________________________________
void CompositeDiffXSec::InitializeFinalStateParticles(){
   if(fProtonXSec)  fProtonXSec->InitializeFinalStateParticles();
   if(fNeutronXSec) fNeutronXSec->InitializeFinalStateParticles();
   InclusiveDiffXSec::InitializeFinalStateParticles();
}
//_____________________________________________________________________________
void CompositeDiffXSec::SetParticleType(Int_t pdgcode, Int_t part) {
   DiffXSec::SetParticleType(pdgcode, part);
   if(fProtonXSec)  fProtonXSec->SetParticleType(pdgcode,part);
   if(fNeutronXSec) fNeutronXSec->SetParticleType(pdgcode,part);
}
//______________________________________________________________________________

}}
