#include "MAIDExclusivePionDiffXSec2.h"

namespace insane {
namespace physics {
//______________________________________________________________________________
MAIDExclusivePionDiffXSec2::MAIDExclusivePionDiffXSec2(const char * pion,const char * nucleon) : MAIDExclusivePionDiffXSec(pion,nucleon) {
   SetTitle("MAIDExclusiveDiffXSec");
   SetPlotTitle("MAID #vec{p}+e -> #pi + X");
   fLabel      = " #frac{d#sigma}{d#Omega_{e}d#omega_{#pi}#Omega_{#pi}} ";
   fUnits      = "nb/GeV/sr^{2}";
}

//______________________________________________________________________________
MAIDExclusivePionDiffXSec2::~MAIDExclusivePionDiffXSec2(){
   
}

//____________________________________________________________________
void MAIDExclusivePionDiffXSec2::InitializePhaseSpaceVariables() {
   //std::cout << " o InclusiveDiffXSec::InitializePhaseSpaceVariables() \n";
   PhaseSpace * ps = GetPhaseSpace();

      //------------------------
      auto * varEnergy = new PhaseSpaceVariable();
      varEnergy = new PhaseSpaceVariable();
      varEnergy->SetNameTitle("energy_e", "E_{e'}");
      varEnergy->SetMinimum(0.00000003); //GeV
      varEnergy->SetMaximum(5.8); //GeV
      varEnergy->SetParticleIndex(0);
      varEnergy->SetDependent(true);
      ps->AddVariable(varEnergy);

      auto *   varTheta = new PhaseSpaceVariable();
      varTheta->SetNameTitle("theta_e", "#theta_{e'}"); // ROOT string latex
      varTheta->SetMinimum(0.0*degree ); //
      varTheta->SetMaximum(180.0*degree ); //
      varTheta->SetParticleIndex(0);
      ps->AddVariable(varTheta);

      auto *   varPhi = new PhaseSpaceVariable();
      varPhi->SetNameTitle("phi_e", "#phi_{e'}"); // ROOT string latex
      varPhi->SetMinimum(-360.0*degree ); //
      varPhi->SetMaximum( 360.0*degree ); //
      varPhi->SetParticleIndex(0);
      ps->AddVariable(varPhi);

      //------------------------
      auto * varEnergyP = new PhaseSpaceVariable();
      varEnergyP = new PhaseSpaceVariable();
      varEnergyP->SetNameTitle("energy_p", "E_{p}");
      varEnergyP->SetMinimum(0.0); //GeV
      varEnergyP->SetMaximum(5.90); //GeV
      varEnergyP->SetParticleIndex(1);
      varEnergyP->SetDependent(true);
      ps->AddVariable(varEnergyP);

      auto *   varThetaP = new PhaseSpaceVariable();
      varThetaP->SetNameTitle("theta_p", "#theta_{p}"); // ROOT string latex
      varThetaP->SetMinimum(0.0  *degree ); //
      varThetaP->SetMaximum(180.0*degree ); //
      varThetaP->SetDependent(true);
      varThetaP->SetParticleIndex(1);
      ps->AddVariable(varThetaP);

      auto *   varPhiP = new PhaseSpaceVariable();
      varPhiP->SetNameTitle("phi_p", "#phi_{p}"); // ROOT string latex
      varPhiP->SetMinimum(-360.0*degree ); //
      varPhiP->SetMaximum( 360.0*degree ); //
      varPhiP->SetDependent(true);
      varPhiP->SetParticleIndex(1);
      ps->AddVariable(varPhiP);

      //------------------------
      auto * varEnergyPi = new PhaseSpaceVariable();
      varEnergyPi = new PhaseSpaceVariable();
      varEnergyPi->SetNameTitle("energy_pi", "E_{pi}");
      varEnergyPi->SetMinimum(0.0); //GeV
      varEnergyPi->SetMaximum(5.9); //GeV
      varEnergyPi->SetParticleIndex(2);
      //varEnergyPi->SetDependent(true);
      ps->AddVariable(varEnergyPi);

      auto *   varThetaPi = new PhaseSpaceVariable();
      varThetaPi->SetNameTitle("theta_pi", "#theta_{pi}"); // ROOT string latex
      varThetaPi->SetMinimum(0.0  *degree ); //
      varThetaPi->SetMaximum(180.0*degree ); //
      varThetaPi->SetParticleIndex(2);
      ps->AddVariable(varThetaPi);

      auto *   varPhiPi = new PhaseSpaceVariable();
      varPhiPi->SetNameTitle("phi_pi", "#phi_{pi}"); // ROOT string latex
      varPhiPi->SetMinimum( -360.0*degree ); //
      varPhiPi->SetMaximum( 360.0*degree ); //
      varPhiPi->SetParticleIndex(2);
      ps->AddVariable(varPhiPi);

      //------------------------
      SetPhaseSpace(ps);
      //ps->Print();
}

//______________________________________________________________________________
Double_t * MAIDExclusivePionDiffXSec2::GetDependentVariables(const Double_t * x) const {
   //std::cout << "MAIDExclusivePionDiffXSec2::GetDependentVariables" << std::endl;

   // Variables evaluated in the lab frame.
   Double_t E0       = GetBeamEnergy(); 
   Double_t w_pi     = x[0];
   Double_t theta    = x[1];
   Double_t phi      = x[2];
   Double_t theta_pi = x[3];
   Double_t phi_pi   = x[4];
   //std::cout << " w_pi = " <<w_pi << std::endl;
   /// for now we are ONLY using the first Eprime value when the solution is a double valued function
   //Double_t Eprime   = Eprime1(E0,w_pi,theta,theta_pi,fM_1,fM_2,fM_pi);
   
   //Double_t y[5] = {Eprime,x[1],x[2],x[3],x[4]};
   for(int i = 0;i<5;i++) fDependentVariables[i] = x[i];
   return(fDependentVariables);
}

//______________________________________________________________________________
Double_t MAIDExclusivePionDiffXSec2::Eprime1(Double_t E1,Double_t w,Double_t the,Double_t thpi,Double_t M1,Double_t M2,Double_t Mpi ) const {
   using namespace TMath;
   Double_t costhpi = Cos(thpi);
   Double_t sin2the = Power(Sin(the/2.0),2.0);
   Double_t Eprime = (Power(M1,3) - M1*Power(M2,2) + M1*Power(Mpi,2) + 4*Power(E1,2)*sin2the*(M1 - w) - 
         3*Power(M1,2)*w + Power(M2,2)*w - Power(Mpi,2)*w + 2*M1*Power(w,2) + 
         2*E1*(-(Power(M2,2)*sin2the) + Power(Mpi,2)*sin2the + Power(M1,2)*(1 + sin2the) - 
            2*M1*(1 + sin2the)*w + Power(w,2) - 
            Power(costhpi,2)*(-1 + 2*sin2the)*(Power(Mpi,2) - Power(w,2))) - 
         Sqrt(-(Power(costhpi,2)*(Power(Mpi,2) - Power(w,2))*
               (Power(M1,4) + Power(M2,4) + Power(Mpi,4) - 
                8*Power(E1,2)*Power(Mpi,2)*sin2the + 
                16*Power(costhpi,2)*Power(E1,2)*Power(Mpi,2)*sin2the + 
                16*Power(E1,4)*Power(sin2the,2) + 
                16*Power(E1,2)*Power(Mpi,2)*Power(sin2the,2) - 
                16*Power(costhpi,2)*Power(E1,2)*Power(Mpi,2)*Power(sin2the,2) - 
                2*Power(M2,2)*(Power(Mpi,2) + 4*E1*sin2the*(E1*(-1 + 2*sin2the) - w)) + 
                Power(M1,3)*(8*E1*sin2the - 4*w) + 
                4*M1*(-Power(M2,2) + Power(Mpi,2) + 4*E1*sin2the*(E1 - w))*
                (2*E1*sin2the - w) - 8*E1*Power(Mpi,2)*sin2the*w - 
                32*Power(E1,3)*Power(sin2the,2)*w + 16*Power(E1,2)*sin2the*Power(w,2) - 
                16*Power(costhpi,2)*Power(E1,2)*sin2the*Power(w,2) + 
                16*Power(costhpi,2)*Power(E1,2)*Power(sin2the,2)*Power(w,2) + 
                2*Power(M1,2)*(-Power(M2,2) + Power(Mpi,2) + 
                   2*(2*Power(E1,2)*sin2the + 4*Power(E1,2)*Power(sin2the,2) - 
                      6*E1*sin2the*w + Power(w,2)))))))/
                      (2.*(Power(M1,2) + M1*(4*E1*sin2the - 2*w) + Power(-2*E1*sin2the + w,2) + 
                           Power(costhpi,2)*(Power(Mpi,2) - Power(w,2))));
   return Eprime;
}
//______________________________________________________________________________
Double_t MAIDExclusivePionDiffXSec2::Eprime2(Double_t E1,Double_t w,Double_t the,Double_t thpi,Double_t M1,Double_t M2,Double_t Mpi ) const {
   using namespace TMath;
   Double_t costhpi = Cos(thpi);
   Double_t sin2the = Power(Sin(the/2.0),2.0);
   Double_t Eprime  = (Power(M1,3) - M1*Power(M2,2) + M1*Power(Mpi,2) + 4*Power(E1,2)*sin2the*(M1 - w) - 
         3*Power(M1,2)*w + Power(M2,2)*w - Power(Mpi,2)*w + 2*M1*Power(w,2) + 
         2*E1*(-(Power(M2,2)*sin2the) + Power(Mpi,2)*sin2the + Power(M1,2)*(1 + sin2the) - 
            2*M1*(1 + sin2the)*w + Power(w,2) - 
            Power(costhpi,2)*(-1 + 2*sin2the)*(Power(Mpi,2) - Power(w,2))) + 
         Sqrt(-(Power(costhpi,2)*(Power(Mpi,2) - Power(w,2))*
               (Power(M1,4) + Power(M2,4) + Power(Mpi,4) - 
                8*Power(E1,2)*Power(Mpi,2)*sin2the + 
                16*Power(costhpi,2)*Power(E1,2)*Power(Mpi,2)*sin2the + 
                16*Power(E1,4)*Power(sin2the,2) + 
                16*Power(E1,2)*Power(Mpi,2)*Power(sin2the,2) - 
                16*Power(costhpi,2)*Power(E1,2)*Power(Mpi,2)*Power(sin2the,2) - 
                2*Power(M2,2)*(Power(Mpi,2) + 4*E1*sin2the*(E1*(-1 + 2*sin2the) - w)) + 
                Power(M1,3)*(8*E1*sin2the - 4*w) + 
                4*M1*(-Power(M2,2) + Power(Mpi,2) + 4*E1*sin2the*(E1 - w))*
                (2*E1*sin2the - w) - 8*E1*Power(Mpi,2)*sin2the*w - 
                32*Power(E1,3)*Power(sin2the,2)*w + 16*Power(E1,2)*sin2the*Power(w,2) - 
                16*Power(costhpi,2)*Power(E1,2)*sin2the*Power(w,2) + 
                16*Power(costhpi,2)*Power(E1,2)*Power(sin2the,2)*Power(w,2) + 
                2*Power(M1,2)*(-Power(M2,2) + Power(Mpi,2) + 
                   2*(2*Power(E1,2)*sin2the + 4*Power(E1,2)*Power(sin2the,2) - 
                      6*E1*sin2the*w + Power(w,2)))))))/
                      (2.*(Power(M1,2) + M1*(4*E1*sin2the - 2*w) + Power(-2*E1*sin2the + w,2) + 
                           Power(costhpi,2)*(Power(Mpi,2) - Power(w,2))));
   return Eprime;
}
//______________________________________________________________________________
Double_t MAIDExclusivePionDiffXSec2::JacobianEprimeOverEpion1(Double_t E1,Double_t w,Double_t the,Double_t thpi,Double_t M1,Double_t M2,Double_t Mpi ) const {
   using namespace TMath;
   Double_t costhpi = Cos(thpi);
   Double_t sin2the = Power(Sin(the/2.0),2.0);
   Double_t t1 = Abs((32*(M1 + 2*E1*sin2the + (-1 + Power(costhpi,2))*w)*
            (Power(M1,3) - M1*Power(M2,2) + M1*Power(Mpi,2) + 
             4*Power(E1,2)*sin2the*(M1 - w) - 3*Power(M1,2)*w + Power(M2,2)*w - 
             Power(Mpi,2)*w + 2*M1*Power(w,2) + 
             2*E1*(-(Power(M2,2)*sin2the) + Power(Mpi,2)*sin2the + 
                Power(M1,2)*(1 + sin2the) - 2*M1*(1 + sin2the)*w + Power(w,2) - 
                Power(costhpi,2)*(-1 + 2*sin2the)*(Power(Mpi,2) - Power(w,2))) - 
             Sqrt(-(Power(costhpi,2)*(Power(Mpi,2) - Power(w,2))*
                   (Power(M1,4) + Power(M2,4) + Power(Mpi,4) - 
                    8*Power(E1,2)*Power(Mpi,2)*sin2the + 
                    16*Power(costhpi,2)*Power(E1,2)*Power(Mpi,2)*sin2the + 
                    16*Power(E1,4)*Power(sin2the,2) + 
                    16*Power(E1,2)*Power(Mpi,2)*Power(sin2the,2) - 
                    16*Power(costhpi,2)*Power(E1,2)*Power(Mpi,2)*Power(sin2the,2) - 
                    2*Power(M2,2)*(Power(Mpi,2) + 4*E1*sin2the*(E1*(-1 + 2*sin2the) - w)) + 
                    Power(M1,3)*(8*E1*sin2the - 4*w) + 
                    4*M1*(-Power(M2,2) + Power(Mpi,2) + 4*E1*sin2the*(E1 - w))*
                    (2*E1*sin2the - w) - 8*E1*Power(Mpi,2)*sin2the*w - 
                    32*Power(E1,3)*Power(sin2the,2)*w + 16*Power(E1,2)*sin2the*Power(w,2) - 
                    16*Power(costhpi,2)*Power(E1,2)*sin2the*Power(w,2) + 
                    16*Power(costhpi,2)*Power(E1,2)*Power(sin2the,2)*Power(w,2) + 
                    2*Power(M1,2)*(-Power(M2,2) + Power(Mpi,2) + 
                       2*(2*Power(E1,2)*sin2the + 4*Power(E1,2)*Power(sin2the,2) - 
                          6*E1*sin2the*w + Power(w,2))))))) + 
                          4*(Power(M1,2) + M1*(4*E1*sin2the - 2*w) + Power(-2*E1*sin2the + w,2) + 
                                Power(costhpi,2)*(Power(Mpi,2) - Power(w,2)))*
                          (-16*E1*M1 - 12*Power(M1,2) + 4*Power(M2,2) - 4*Power(Mpi,2) - 
                           16*Power(E1,2)*sin2the - 16*E1*M1*sin2the + 16*E1*w - 
                           16*Power(costhpi,2)*E1*w + 16*M1*w + 32*Power(costhpi,2)*E1*sin2the*w + 
                           (4*Power(costhpi,2)*(-(Power(M1,4)*w) - 
                                                Power(Power(M2,2) - Power(Mpi,2),2)*w - 
                                                16*Power(E1,4)*Power(sin2the,2)*w - 
                                                2*Power(M1,3)*(Power(Mpi,2) + (4*E1*sin2the - 3*w)*w) + 
                                                4*E1*(Power(M2,2) - Power(Mpi,2))*sin2the*(Power(Mpi,2) - 3*Power(w,2)) - 
                                                16*Power(E1,3)*Power(sin2the,2)*(Power(Mpi,2) - 3*Power(w,2)) - 
                                                8*Power(E1,2)*sin2the*w*
                                                (Power(M2,2)*(1 - 2*sin2the) + 
                                                 Power(Mpi,2)*(-3 - 4*Power(costhpi,2)*(-1 + sin2the) + 2*sin2the) + 
                                                 4*(1 + Power(costhpi,2)*(-1 + sin2the))*Power(w,2)) - 
                                                2*Power(M1,2)*(4*Power(E1,2)*sin2the*(1 + 2*sin2the)*w - 
                                                   w*(Power(M2,2) + Power(Mpi,2) - 4*Power(w,2)) + 
                                                   6*E1*sin2the*(Power(Mpi,2) - 3*Power(w,2))) + 
                                                2*M1*(-Power(Mpi,4) + 
                                                   Power(M2,2)*(Power(Mpi,2) + (4*E1*sin2the - 3*w)*w) + 
                                                   Power(Mpi,2)*(-4*Power(E1,2)*sin2the*(1 + 2*sin2the) + 
                                                      4*E1*sin2the*w + 3*Power(w,2)) - 
                                                   4*E1*sin2the*w*(4*Power(E1,2)*sin2the + 4*Power(w,2) - 
                                                      3*E1*(w + 2*sin2the*w)))))/
                           Sqrt(Power(costhpi,2)*(Power(Mpi,2) - Power(w,2))*
                                 (-Power(M1,4) - Power(M2,4) - Power(Mpi,4) + 
                                  8*Power(E1,2)*Power(Mpi,2)*sin2the - 
                                  16*Power(costhpi,2)*Power(E1,2)*Power(Mpi,2)*sin2the - 
                                  16*Power(E1,4)*Power(sin2the,2) - 
                                  16*Power(E1,2)*Power(Mpi,2)*Power(sin2the,2) + 
                                  16*Power(costhpi,2)*Power(E1,2)*Power(Mpi,2)*Power(sin2the,2) + 
                                  2*Power(M2,2)*(Power(Mpi,2) + 4*E1*sin2the*(E1*(-1 + 2*sin2the) - w)) - 
                                  4*M1*(-Power(M2,2) + Power(Mpi,2) + 4*E1*sin2the*(E1 - w))*
                                  (2*E1*sin2the - w) + 8*E1*Power(Mpi,2)*sin2the*w + 
                                  32*Power(E1,3)*Power(sin2the,2)*w - 16*Power(E1,2)*sin2the*Power(w,2) + 
                                  16*Power(costhpi,2)*Power(E1,2)*sin2the*Power(w,2) - 
                                  16*Power(costhpi,2)*Power(E1,2)*Power(sin2the,2)*Power(w,2) + 
                                  Power(M1,3)*(-8*E1*sin2the + 4*w) + 
                                  2*Power(M1,2)*(Power(M2,2) - Power(Mpi,2) - 
                                     2*(2*Power(E1,2)*sin2the*(1 + 2*sin2the) - 6*E1*sin2the*w + Power(w,2))
                                     )))))/
                                     Power(Power(M1,2) + M1*(4*E1*sin2the - 2*w) + Power(-2*E1*sin2the + w,2) + 
                                           Power(costhpi,2)*(Power(Mpi,2) - Power(w,2)),2))/32. ;
   return(t1 );
}

//______________________________________________________________________________
Double_t MAIDExclusivePionDiffXSec2::JacobianEprimeOverEpion2(Double_t E1,Double_t w,Double_t the,Double_t thpi,Double_t M1,Double_t M2,Double_t Mpi ) const {
   using namespace TMath;
   Double_t costhpi = Cos(thpi);
   Double_t sin2the = Power(Sin(the/2.0),2.0);
   Double_t t2 = Abs((32*(M1 + 2*E1*sin2the + (-1 + Power(costhpi,2))*w)*
            (Power(M1,3) - M1*Power(M2,2) + M1*Power(Mpi,2) + 
             4*Power(E1,2)*sin2the*(M1 - w) - 3*Power(M1,2)*w + Power(M2,2)*w - 
             Power(Mpi,2)*w + 2*M1*Power(w,2) + 
             2*E1*(-(Power(M2,2)*sin2the) + Power(Mpi,2)*sin2the + 
                Power(M1,2)*(1 + sin2the) - 2*M1*(1 + sin2the)*w + Power(w,2) - 
                Power(costhpi,2)*(-1 + 2*sin2the)*(Power(Mpi,2) - Power(w,2))) + 
             Sqrt(-(Power(costhpi,2)*(Power(Mpi,2) - Power(w,2))*
                   (Power(M1,4) + Power(M2,4) + Power(Mpi,4) - 
                    8*Power(E1,2)*Power(Mpi,2)*sin2the + 
                    16*Power(costhpi,2)*Power(E1,2)*Power(Mpi,2)*sin2the + 
                    16*Power(E1,4)*Power(sin2the,2) + 
                    16*Power(E1,2)*Power(Mpi,2)*Power(sin2the,2) - 
                    16*Power(costhpi,2)*Power(E1,2)*Power(Mpi,2)*Power(sin2the,2) - 
                    2*Power(M2,2)*(Power(Mpi,2) + 4*E1*sin2the*(E1*(-1 + 2*sin2the) - w)) + 
                    Power(M1,3)*(8*E1*sin2the - 4*w) + 
                    4*M1*(-Power(M2,2) + Power(Mpi,2) + 4*E1*sin2the*(E1 - w))*
                    (2*E1*sin2the - w) - 8*E1*Power(Mpi,2)*sin2the*w - 
                    32*Power(E1,3)*Power(sin2the,2)*w + 16*Power(E1,2)*sin2the*Power(w,2) - 
                    16*Power(costhpi,2)*Power(E1,2)*sin2the*Power(w,2) + 
                    16*Power(costhpi,2)*Power(E1,2)*Power(sin2the,2)*Power(w,2) + 
                    2*Power(M1,2)*(-Power(M2,2) + Power(Mpi,2) + 
                       2*(2*Power(E1,2)*sin2the + 4*Power(E1,2)*Power(sin2the,2) - 
                          6*E1*sin2the*w + Power(w,2))))))) + 
                          4*(Power(M1,2) + M1*(4*E1*sin2the - 2*w) + Power(-2*E1*sin2the + w,2) + 
                                Power(costhpi,2)*(Power(Mpi,2) - Power(w,2)))*
                          (-16*E1*M1 - 12*Power(M1,2) + 4*Power(M2,2) - 4*Power(Mpi,2) - 
                           16*Power(E1,2)*sin2the - 16*E1*M1*sin2the + 16*E1*w - 
                           16*Power(costhpi,2)*E1*w + 16*M1*w + 32*Power(costhpi,2)*E1*sin2the*w - 
                           (4*Power(costhpi,2)*(-(Power(M1,4)*w) - 
                                                Power(Power(M2,2) - Power(Mpi,2),2)*w - 
                                                16*Power(E1,4)*Power(sin2the,2)*w - 
                                                2*Power(M1,3)*(Power(Mpi,2) + (4*E1*sin2the - 3*w)*w) + 
                                                4*E1*(Power(M2,2) - Power(Mpi,2))*sin2the*(Power(Mpi,2) - 3*Power(w,2)) - 
                                                16*Power(E1,3)*Power(sin2the,2)*(Power(Mpi,2) - 3*Power(w,2)) - 
                                                8*Power(E1,2)*sin2the*w*
                                                (Power(M2,2)*(1 - 2*sin2the) + 
                                                 Power(Mpi,2)*(-3 - 4*Power(costhpi,2)*(-1 + sin2the) + 2*sin2the) + 
                                                 4*(1 + Power(costhpi,2)*(-1 + sin2the))*Power(w,2)) - 
                                                2*Power(M1,2)*(4*Power(E1,2)*sin2the*(1 + 2*sin2the)*w - 
                                                   w*(Power(M2,2) + Power(Mpi,2) - 4*Power(w,2)) + 
                                                   6*E1*sin2the*(Power(Mpi,2) - 3*Power(w,2))) + 
                                                2*M1*(-Power(Mpi,4) + 
                                                   Power(M2,2)*(Power(Mpi,2) + (4*E1*sin2the - 3*w)*w) + 
                                                   Power(Mpi,2)*(-4*Power(E1,2)*sin2the*(1 + 2*sin2the) + 
                                                      4*E1*sin2the*w + 3*Power(w,2)) - 
                                                   4*E1*sin2the*w*(4*Power(E1,2)*sin2the + 4*Power(w,2) - 
                                                      3*E1*(w + 2*sin2the*w)))))/
                           Sqrt(Power(costhpi,2)*(Power(Mpi,2) - Power(w,2))*
                                 (-Power(M1,4) - Power(M2,4) - Power(Mpi,4) + 
                                  8*Power(E1,2)*Power(Mpi,2)*sin2the - 
                                  16*Power(costhpi,2)*Power(E1,2)*Power(Mpi,2)*sin2the - 
                                  16*Power(E1,4)*Power(sin2the,2) - 
                                  16*Power(E1,2)*Power(Mpi,2)*Power(sin2the,2) + 
                                  16*Power(costhpi,2)*Power(E1,2)*Power(Mpi,2)*Power(sin2the,2) + 
                                  2*Power(M2,2)*(Power(Mpi,2) + 4*E1*sin2the*(E1*(-1 + 2*sin2the) - w)) - 
                                  4*M1*(-Power(M2,2) + Power(Mpi,2) + 4*E1*sin2the*(E1 - w))*
                                  (2*E1*sin2the - w) + 8*E1*Power(Mpi,2)*sin2the*w + 
                                  32*Power(E1,3)*Power(sin2the,2)*w - 16*Power(E1,2)*sin2the*Power(w,2) + 
                                  16*Power(costhpi,2)*Power(E1,2)*sin2the*Power(w,2) - 
                                  16*Power(costhpi,2)*Power(E1,2)*Power(sin2the,2)*Power(w,2) + 
                                  Power(M1,3)*(-8*E1*sin2the + 4*w) + 
                                  2*Power(M1,2)*(Power(M2,2) - Power(Mpi,2) - 
                                     2*(2*Power(E1,2)*sin2the*(1 + 2*sin2the) - 6*E1*sin2the*w + Power(w,2))
                                     )))))/
                                     Power(Power(M1,2) + M1*(4*E1*sin2the - 2*w) + Power(-2*E1*sin2the + w,2) + 
                                           Power(costhpi,2)*(Power(Mpi,2) - Power(w,2)),2))/32.;
   return( t2);
}

//______________________________________________________________________________
Double_t MAIDExclusivePionDiffXSec2::EvaluateXSec(const Double_t *par) const{
   //if (!VariablesInPhaseSpace(GetPhaseSpace()->GetDimension(), par)) return(0.0);
   // compute needed variables in GeV and rad  
   Double_t Es       = GetBeamEnergy();
   Double_t omega_pi = par[0];
   Double_t th       = par[1];
   Double_t ph       = par[2];
   Double_t th_pi    = par[3];
   Double_t ph_pi    = par[4];
   Double_t M_1      = M_p/GeV; // target nucleus
   Double_t M_2      = M_p/GeV; // recoil nucleon
   Double_t M_pi     = M_pion/GeV; // recoil pion
   Double_t res      = 0.0;

   //Double_t Ep1      = Eprime1(Es,omega_pi,th,fTheta_pi_q_lab,M_1,M_2,M_pi);
   //Double_t yy[5]    = { Ep1,par[1],par[2],par[3],par[4]};
   //Double_t Q2       = ::Kine::Qsquared(Es,Ep1,th); 
   //Double_t omega_th = (TMath::Power(M_2+M_pi,2.0) - M_1*M_1 + Q2)/(2.0*M_1);
   //Double_t sig1     =  0.0; 
   //if(omega_th < omega_pi) {
   //   sig1 = MAIDExclusivePionDiffXSec::EvaluateXSec(MAIDExclusivePionDiffXSec::GetDependentVariables(yy));
   //   //std::cout << "sig1 = " << sig1 << std::endl;
   //   Double_t jacobian1   = JacobianEprimeOverEpion1(Es,omega_pi,th,fTheta_pi_q_lab,M_1,M_2,M_pi);
   //   sig1 *= jacobian1;
   //} 
   //Double_t Ep2   = Eprime2(Es,omega_pi,th,fTheta_pi_q_lab,M_1,M_2,M_pi);
   //yy[0]          = Ep2;
   //Q2             = ::Kine::Qsquared(Es,Ep2,th); 
   //omega_th       = (TMath::Power(M_2+M_pi,2.0) - M_1*M_1 + Q2)/(2.0*M_1);
   //Double_t sig2  =  0.0; 
   //if(omega_th < omega_pi) {
   //   sig2 = MAIDExclusivePionDiffXSec::EvaluateXSec(MAIDExclusivePionDiffXSec::GetDependentVariables(yy));
   //   //std::cout << "sig2 = " << sig2 << std::endl;
   //   Double_t jacobian2   = JacobianEprimeOverEpion2(Es,omega_pi,th,fTheta_pi_q_lab,M_1,M_2,M_pi);
   //   sig2 *= jacobian2;
   //} 
   //res = sig1 + sig2;

   Double_t Ep1      = EprimeLab(Es,omega_pi,th,th_pi,ph-ph_pi,M_1,M_2,M_pi);
   Double_t yy[5]    = { Ep1,par[1],par[2],par[3],par[4]};
   Double_t Q2       = insane::Kine::Qsquared(Es,Ep1,th); 
   Double_t omega_th = (TMath::Power(M_2+M_pi,2.0) - M_1*M_1 + Q2)/(2.0*M_1);
   Double_t sig1     =  0.0; 
   if(omega_th < omega_pi) {
      sig1 = MAIDExclusivePionDiffXSec::EvaluateXSec(MAIDExclusivePionDiffXSec::GetDependentVariables(yy));
      //std::cout << "sig1 = " << sig1 << std::endl;
      Double_t jacobian1   = JacobianEprimeOverEpion_2(Es,omega_pi,th,th_pi,ph-ph_pi,M_1,M_2,M_pi);
      sig1 *= jacobian1;
   } 
   res = sig1;
   //res *= TMath::Sin(th)*TMath::Sin(th_pi);
   //if(IncludeJacobian()) return res*TMath::Sin(th);
   //std::cout << " res    = " << res << std::endl; 
   return res;  
}
//______________________________________________________________________________
}}
