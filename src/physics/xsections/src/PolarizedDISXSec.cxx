#include "PolarizedDISXSec.h"
#include "PolarizedStructureFunctions.h"

namespace insane {
namespace physics {
Double_t  PolarizedDiffXSec::EvaluateXSec(const Double_t * x) const {
   if (!VariablesInPhaseSpace(fnDim, x)) return(0.0);
   //Double_t Eprime = x[0];
   //Double_t theta = x[1];
   //Double_t phi = x[2];
   Int_t helicity = fHelicityPSVar->GetDiscreteVariable(x[3]);
   Double_t resultXsec = 0.0;
   if (helicity < fHelicityPSVar->GetNumberOfValues()) {
      if (fCrossSections[helicity]) {
         resultXsec = fCrossSections[helicity]->EvaluateXSec(x) * fXSecCoefficients[helicity] ;
      } else {
         std::cout << " x ERROR NO POLARIZED XSEC PROVIDED!\n";
      }
   } else {
      std::cout << " x ERROR helicity = " << helicity << ">" <<  fHelicityPSVar->GetNumberOfValues() << ", OUT OF RANGE\n";
   }
   return(resultXsec);
}
//______________________________________________________________________________

}}
