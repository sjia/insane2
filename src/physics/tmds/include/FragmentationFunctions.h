#ifndef FragmentationFunctions_HH
#define FragmentationFunctions_HH

#include "TNamed.h"
namespace insane {
namespace physics {

/** Abstract base class for Fragmentation Functions.
 *
 * @ingroup fragfuncs
 */
class FragmentationFunctions : public TNamed {
   public:
      FragmentationFunctions();
      virtual ~FragmentationFunctions();

      // pion FFs
      virtual double D_u_piplus( double z, double Q2)    const = 0;
      virtual double D_u_piminus(double z, double Q2)    const = 0;
      virtual double D_ubar_piplus( double z, double Q2) const = 0;
      virtual double D_ubar_piminus(double z, double Q2) const = 0;
      virtual double D_d_piplus( double z, double Q2)    const = 0;
      virtual double D_d_piminus(double z, double Q2)    const = 0;
      virtual double D_dbar_piplus( double z, double Q2) const = 0;
      virtual double D_dbar_piminus(double z, double Q2) const = 0;
      virtual double D_s_piplus( double z, double Q2)    const = 0;
      virtual double D_s_piminus(double z, double Q2)    const = 0;
      virtual double D_sbar_piplus( double z, double Q2) const = 0;
      virtual double D_sbar_piminus(double z, double Q2) const = 0;


      // kaon FFs
      virtual double D_u_Kplus( double z, double Q2)    const = 0;
      virtual double D_u_Kminus(double z, double Q2)    const = 0;
      virtual double D_ubar_Kplus( double z, double Q2) const = 0;
      virtual double D_ubar_Kminus(double z, double Q2) const = 0;
      virtual double D_d_Kplus( double z, double Q2)    const = 0;
      virtual double D_d_Kminus(double z, double Q2)    const = 0;
      virtual double D_dbar_Kplus( double z, double Q2) const = 0;
      virtual double D_dbar_Kminus(double z, double Q2) const = 0;
      virtual double D_s_Kplus( double z, double Q2)    const = 0;
      virtual double D_s_Kminus(double z, double Q2)    const = 0;
      virtual double D_sbar_Kplus( double z, double Q2) const = 0;
      virtual double D_sbar_Kminus(double z, double Q2) const = 0;

      // pi0 
      // D_q^{pi0} = (D_q^{pi+} + D_q^{pi-})/2
      virtual double D_u_pi0( double z, double Q2)    const ;
      virtual double D_ubar_pi0( double z, double Q2) const ;
      virtual double D_d_pi0( double z, double Q2)    const ;
      virtual double D_dbar_pi0( double z, double Q2) const ;
      virtual double D_s_pi0( double z, double Q2)    const ;
      virtual double D_sbar_pi0( double z, double Q2) const ;

   ClassDef(FragmentationFunctions,1)
};
}
}
#endif

