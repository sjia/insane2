#ifndef TransverseMomentumDistributions_HH
#define TransverseMomentumDistributions_HH

#include "TNamed.h"
namespace insane {
namespace physics {

/** Base class for TMDs 
 *
 * @ingroup tmds
 */
class TransverseMomentumDistributions : public TNamed {

   public:
      TransverseMomentumDistributions();
      virtual ~TransverseMomentumDistributions();

      virtual double f_u(   double x, double k_perp, double Q2) = 0;
      virtual double f_d(   double x, double k_perp, double Q2) = 0;
      virtual double f_ubar(double x, double k_perp, double Q2) = 0;
      virtual double f_dbar(double x, double k_perp, double Q2) = 0;

   ClassDef(TransverseMomentumDistributions,1)
};

typedef TransverseMomentumDistributions TMDs;
}
}

#endif

