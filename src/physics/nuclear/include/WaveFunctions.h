#ifndef WaveFunctions_HH
#define WaveFunctions_HH 1

#include <complex>
#include "TNamed.h"
#include <math.h>

namespace insane {
   namespace physics {
      class WaveFunction : public TNamed {
         protected:
            std::complex<double> fVal;

         public:
            WaveFunction();
            virtual ~WaveFunction();

            virtual std::complex<double> Evaluate(double *,double *) = 0;

            std::complex<double> operator()(double *, double *);
            std::complex<double> Conjugate(double *, double *);
            std::complex<double> GetVal(){ return fVal; }
            double               Abs(){ return( std::abs(fVal) ); }


            ClassDef(WaveFunction,1)
      };
   }
}


#endif

