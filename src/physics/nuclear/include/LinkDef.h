#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedef;


#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ namespace insane;
#pragma link C++ namespace insane::physics;

#pragma link C++ class insane::physics::WaveFunction+;
#pragma link C++ class insane::physics::BonnDeuteronWaveFunction+;

#pragma link C++ class insane::physics::NucelonMomentumDistributions+;
#pragma link C++ class insane::physics::FermiMomentumDist+;

#pragma link C++ function fermi3_(double* , int *, double *)+;

#endif

