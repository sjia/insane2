#include "WaveFunctions.h"

using namespace insane::physics;

//______________________________________________________________________________
WaveFunction::WaveFunction(){
}
//______________________________________________________________________________
WaveFunction::~WaveFunction(){
}
//______________________________________________________________________________
std::complex<double> WaveFunction::operator()(double * x, double * p){
   fVal = this->Evaluate(x,p);
   return fVal;
}
//______________________________________________________________________________
//std::complex<double> WaveFunction::Evaluate(double *,double *){
//}
//______________________________________________________________________________
std::complex<double> WaveFunction::Conjugate(double * x, double * p) {
   fVal = this->Evaluate(x,p);
   return( std::conj(fVal) );
}
//______________________________________________________________________________

