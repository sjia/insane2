#include "BonnWaveFunctions.h"

using namespace insane::physics;

BonnDeuteronWaveFunction::BonnDeuteronWaveFunction(){
   fNu     =  11;
   fNw     =  11;
   fm0     =  0.9;      // 1/fm
   fAlpha  =  0.231609; // = sqrt(m*epsilon) 1/fm

   // From table 11 :
   fCj[0]  =  0.90457337;
   fCj[1]  = -0.35058661;
   fCj[2]  = -0.17635927;
   fCj[3]  = -0.10418261e2;
   fCj[4]  =  0.45089439e2;
   fCj[5]  = -0.14861947e3;
   fCj[6]  =  0.31779642e3;
   fCj[7]  = -0.37496518e3;
   fCj[8]  =  0.22560032e3;
   fCj[9]  = -0.54858290e2;
   fCj[10] = 0.0;  // Eq D.28 for C_nu
   for(int j=0;j<10;j++){ fCj[10] += fCj[j];}
   fCj[10] *= -1.0;

   fDj[0]  =  0.24133026e-1;
   fDj[1]  = -0.64430531;
   fDj[2]  =  0.51093352;
   fDj[3]  = -0.54419065e1;
   fDj[4]  =  0.15872034e2;
   fDj[5]  = -0.14742981e2;
   fDj[6]  =  0.44956539e1;
   fDj[7]  = -0.71152863e-1;
   fDj[8]  = Dj(fNw-2,fNw-1,fNw);   // Eq D.28 for C_nu
   fDj[9]  = Dj(fNw-1,fNw,fNw-2);   // Eq D.28 for C_nu
   fDj[10]  = Dj(fNw,fNw-2,fNw-1);  // Eq D.28 for C_nu

}
//______________________________________________________________________________
BonnDeuteronWaveFunction::~BonnDeuteronWaveFunction(){
}
//______________________________________________________________________________
void BonnDeuteronWaveFunction::Check(int N){
   double rmin = 0.0;
   double rmax = 100.0;
   double delta_r = (rmax-rmin)/double(N);
   double res_u = 0.0;
   double res_w = 0.0;
   double res_tot = 0.0;
   for(int i = 0 ; i<N;i++){
      double r = rmin + (double(i)+0.5)*delta_r;
      double u = u_a(r);
      double w = w_a(r);
      res_u += u*u*delta_r;
      res_w += w*w*delta_r;
      res_tot += (w*w + u*u)*delta_r;
   }
   std::cout << " integral u     : " << res_u << std::endl;
   std::cout << " integral w     : " << res_w << std::endl;
   std::cout << " integral total : " << res_tot << std::endl;


}
//______________________________________________________________________________
double BonnDeuteronWaveFunction::Dj(int nw_2,int nw_1, int nw) const {
   // Eq D.28 for D_nw-2
   double A  = mj2(nw_2)/((mj2(nw)-mj2(nw_2))*(mj2(nw_1)-mj2(nw_2)));
   double t1 = 0.0;
   double t2 = 0.0;
   double t3 = 0.0;
   for(int i=0; i < nw_2-1; i++){
      int j = i+1;
      t1 += ( fDj[i]/mj2(j) );
      t2 += ( fDj[i] );
      t3 += ( fDj[i]*mj2(j) );
   }
   t1 *= ( -1.0*mj2(nw_1)*mj2(nw) );
   t2 *= ( mj2(nw_1) + mj2(nw) );
   t3 *= ( -1.0 );
   return( A*(t1 + t2 + t3) );
}
//______________________________________________________________________________
double BonnDeuteronWaveFunction::u_a(double r) const {
   // Eqn D.25
   double sum = 0.0;
   for(int i=0;i<fNu;i++){
      int j = i+1;
      sum += ( fCj[i] * TMath::Exp( -1.0*mj(j)*r ) );
   }
   return(sum);
}
//______________________________________________________________________________
double BonnDeuteronWaveFunction::w_a(double r) const {
   // Eqn D.25
   double sum = 0.0;
   double mjr = 0.0;
   for(int i=0;i<fNw;i++){
      int j = i+1;
      mjr  = mj(j)*r;
      sum += ( fDj[i] * TMath::Exp( -1.0*mjr )*(1.0 + 3.0/(mjr) + 3.0/(mjr*mjr)) );
   }
   return(sum);
}
//______________________________________________________________________________
double BonnDeuteronWaveFunction::Psi_a_0(double q2) const {
   // Eqn D.26
   double res = TMath::Sqrt(2.0/TMath::Pi());
   double sum = 0.0;
   for(int i=0;i<fNu;i++){
      int j = i+1;
      sum += (fCj[i]/(q2+mj2(j)));
   }
   return(res*sum);
}
//______________________________________________________________________________
double BonnDeuteronWaveFunction::Psi_a_2(double q2) const {
   // Eqn D.26
   double res = TMath::Sqrt(2.0/TMath::Pi());
   double sum = 0.0;
   for(int i=0;i<fNu;i++){
      int j = i+1;
      sum += (fDj[i]/(q2+mj2(j)));
   }
   return(res*sum);
}
//______________________________________________________________________________
std::complex<double> BonnDeuteronWaveFunction::Evaluate(double *x,double *p){
   if(!p){
      Error("Evaluate","No parameters");
      fVal =  std::complex<double>(0.0,0.0) ; 
   }
   auto L    = int(p[0]);
   double q = x[0];

   if(L == 0) {
      // S wave function
      fVal = std::complex<double>(Psi_a_0(q*q), 0.0) ; 
   } else if(L == 2) {
      // D wave function
      fVal = std::complex<double>(Psi_a_2(q*q), 0.0) ; 
   } else {
      Error("Evaluate","Incorrect L value, ie not 0(S) or 2(D).");
      fVal =  std::complex<double>(0.0,0.0) ; 
   }
   
   return fVal;
}
//______________________________________________________________________________

