#ifndef insane_physics_MAID_VCSAs_HH
#define insane_physics_MAID_VCSAs_HH 1

#include "VirtualComptonScatteringAsymmetries.h"

namespace insane {
  namespace physics {

    /** MAID Compton asymmetries.
     * 
     * Paper reference: J. Phys. G: Nucl. Part. Phys 18, 449 (1992)  
     * Uses the total cross sections for each channel and sums. 
     *
     * M A I D  2007    (18.05.2007)
     * D. Drechsel, S.S. Kamalov, L. Tiator
     * Institut fuer Kernphysik, Universitaet Mainz
     * 
     *  sigT=1/2(sig_1/2 + sig_3/2), sigTT'=1/2(sig_3/2 - sig_1/2)
     *  sigL0=sigL*(wcm/Q)**2, sigLT0=sigLT*(wcm/Q), sigLT0'=sigLT'*(wcm/Q)
     *
     * \ingroup physics  
     */ 
    class MAID_VCSAs : public VirtualComptonScatteringAsymmetries {

      private:
        mutable double fSigT;
        mutable double fSigL;
        mutable double fSigLT;
        mutable double fSigLTp;
        mutable double fSigTTp;
        mutable double fSigL0;
        mutable double fSigLT0;
        mutable double fSigLT0p;

      public: 
        MAID_VCSAs();
        virtual ~MAID_VCSAs();

        virtual double A1p(   double x, double Q2) const ;
        virtual double A2p(   double x, double Q2) const ;
        virtual double A1n(   double x, double Q2) const ;
        virtual double A2n(   double x, double Q2) const ;

        virtual double A1p_NoTMC(   double x, double Q2) const { return A1p(x, Q2);}
        virtual double A2p_NoTMC(   double x, double Q2) const { return A2p(x, Q2);}
        virtual double A1n_NoTMC(   double x, double Q2) const { return A1n(x, Q2);}
        virtual double A2n_NoTMC(   double x, double Q2) const { return A2n(x, Q2);}
        //virtual double A1d(   double x, double Q2);
        //virtual double A2d(   double x, double Q2);
        //virtual double A1He3( double x, double Q2);
        //virtual double A2He3( double x, double Q2);

        //virtual double A1p_Error(   double x, double Q2);
        //virtual double A2p_Error(   double x, double Q2);
        //virtual double A1n_Error(   double x, double Q2);
        //virtual double A2n_Error(   double x, double Q2);
        //virtual double A1d_Error(   double x, double Q2);
        //virtual double A2d_Error(   double x, double Q2);
        //virtual double A1He3_Error( double x, double Q2);
        //virtual double A2He3_Error( double x, double Q2);

        ClassDef(MAID_VCSAs,1)
    };
  }
}

#endif 
