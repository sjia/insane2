#ifndef INSANE_PHYSICS_VCSASFROMSFS_HH
#define INSANE_PHYSICS_VCSASFROMSFS_HH 1

#include "VirtualComptonScatteringAsymmetries.h"
#include "StructureFunctions.h"
//#include "SpinStructureFunctions.h"
//#include "SSFsFromPDFs.h"
//#include "SFsFromPDFs.h"
//#include "TMath.h"
#include "InSANEMathFunc.h"

namespace insane {
  namespace physics {

    /** Virtual Compton scattering asymmetries.
     *  \f$ A_1 = \frac{g_1 - (4M^2x^2/Q^2)g_2}{F_1} \f$
     *  \f$ A_2 = \frac{2Mx}{\sqrt{Q^2}} \frac{g_1 + g_2}{F_1} \f$
     *
     */

    template< class T1, class T2 >
    double A1_SFs(const T1& sfs, const T2& ssfs, Nuclei target, double x, double Q2)
    {
      double g_1 = ssfs.g1_TMC(x, Q2, target);
      double g_2 = ssfs.g2_TMC(x, Q2, target);
      double F_1 =  sfs.F1_TMC( x, Q2, target);
      double M   = insane::units::M_p/insane::units::GeV;
      double gam2= 4.0*M*M*x*x/Q2;
      //std::cout << "A1_SFs " << std::endl;
      //std::cout << "g1 : " << g_1 << std::endl;
      //std::cout << "g2 : " << g_2 << std::endl;
      //std::cout << "F1 : " << F_1 << std::endl;
      return( (g_1 - gam2*g_2)/F_1 );
    } 

    template< class T1, class T2 >
    double A2_SFs(const T1& sfs, const T2& ssfs, Nuclei target, double x, double Q2)
    {
      double g_1 = ssfs.g1_TMC(x, Q2, target);
      double g_2 = ssfs.g2_TMC(x, Q2, target);
      double F_1 =  sfs.F1_TMC( x, Q2, target);
      double M   = insane::units::M_p/insane::units::GeV;
      double gam = 2.0*M*x/TMath::Sqrt(Q2);
      //std::cout << "A2_SFs " << g_1 << std::endl;
      return( gam*(g_1 + g_2)/F_1 );
    } 

    template< class T1, class T2 >
    double A1_NoTMC_SFs(const T1& sfs, const T2& ssfs, Nuclei target, double x, double Q2){
      double g_1 = ssfs.g1(x, Q2, target);
      double g_2 = ssfs.g2(x, Q2, target);
      double F_1 = sfs.F1( x, Q2, target);
      double M   = insane::units::M_p/insane::units::GeV;
      double gam2= 4.0*M*M*x*x/Q2;
      //std::cout << "A1_NOTMC_SFs " << g_1 << std::endl;
      return( (g_1 - gam2*g_2)/F_1 );
    } 


    template< class T1, class T2 >
    double A2_NoTMC_SFs(const T1& sfs, const T2& ssfs, Nuclei target, double x, double Q2){
      double g_1 = ssfs.g1(x, Q2, target);
      double g_2 = ssfs.g2(x, Q2, target);
      double F_1 = sfs.F1( x, Q2, target);
      double M   = insane::units::M_p/insane::units::GeV;
      double gam = 2.0*M*x/TMath::Sqrt(Q2);
      //std::cout << "A2_NoTMC_SFs " << g_1 << std::endl;
      return( gam*(g_1 + g_2)/F_1 );
    } 
    //______________________________________________________________________________


    template<class T1, class T2> 
    class VCSAsFromSFs : public VirtualComptonScatteringAsymmetries {
      protected:
        T1 fSFs;
        T2 fSSFs;

      public:
        VCSAsFromSFs(){ }
        virtual ~VCSAsFromSFs(){}

        const T1&    GetSFs()  const {return fSFs;}
        const T2&    GetSSFs() const {return fSSFs;}

        virtual double Calculate    (double x, double Q2,
            std::tuple<ComptonAsymmetry,Nuclei> sf, 
            Twist t                                = Twist::All,
            OPELimit l                             = OPELimit::MassiveTarget) const;
        virtual double Uncertainties(double x, double Q2,
            std::tuple<ComptonAsymmetry,Nuclei> sf,
            Twist t                                = Twist::All, 
            OPELimit l                             = OPELimit::MassiveTarget) const;

        virtual double A1(Nuclei target, double x, double Q2) const {
          return Calculate(x, Q2, std::make_tuple(ComptonAsymmetry::A1, target));
        }
        virtual double A2(Nuclei target, double x, double Q2) const {
          return Calculate(x, Q2, std::make_tuple(ComptonAsymmetry::A2, target));
        }

        virtual double A1_NoTMC(Nuclei target, double x, double Q2) const {
          return Calculate(x, Q2, 
              std::make_tuple(ComptonAsymmetry::A1,target),
              Twist::All,
              OPELimit::MasslessTarget);
        }
        virtual double A2_NoTMC(Nuclei target, double x, double Q2) const {
          return Calculate(x, Q2,
              std::make_tuple(ComptonAsymmetry::A2,target),
              Twist::All,
              OPELimit::MasslessTarget);
        }

        virtual double A1p(double x, double Q2) const{ return Calculate(x, Q2, std::make_tuple(ComptonAsymmetry::A1,Nuclei::p)); }
        virtual double A2p(double x, double Q2) const{ return Calculate(x, Q2, std::make_tuple(ComptonAsymmetry::A2,Nuclei::p)); }
        virtual double A1n(double x, double Q2) const{ return Calculate(x, Q2, std::make_tuple(ComptonAsymmetry::A1,Nuclei::n)); }
        virtual double A2n(double x, double Q2) const{ return Calculate(x, Q2, std::make_tuple(ComptonAsymmetry::A2,Nuclei::n)); }

        virtual double A1p_NoTMC(double x, double Q2) const{ return Calculate(x, Q2, std::make_tuple(ComptonAsymmetry::A1,Nuclei::p)); }
        virtual double A2p_NoTMC(double x, double Q2) const{ return Calculate(x, Q2, std::make_tuple(ComptonAsymmetry::A2,Nuclei::p)); }
        virtual double A1n_NoTMC(double x, double Q2) const{ return Calculate(x, Q2, std::make_tuple(ComptonAsymmetry::A1,Nuclei::n)); }
        virtual double A2n_NoTMC(double x, double Q2) const{ return Calculate(x, Q2, std::make_tuple(ComptonAsymmetry::A2,Nuclei::n)); }

        virtual double R(Nuclei target, double x, double Q2) const { return fSFs.R(x,Q2,target); }

        ClassDef(VCSAsFromSFs,1)
    };


  }
}

#include "VCSAsFromSFs.hxx"


#endif

