#ifndef ASYMMETRYBASE_HH
#define ASYMMETRYBASE_HH 1 

#include <cstdlib> 
#include <iostream> 
#include <iomanip> 
#include "TString.h"
#include "Nucleus.h"
#include "TNamed.h"

namespace insane {
  namespace physics {

    /** ABC for Asymmetries calculated from structure functions.  
     *  Contains mainly functions useful for ROOT TF1 objects. 
     */  
    class AsymmetryBase : public TNamed {

    protected:
      Nucleus fTargetNucleus;  
      TString       fLabel;

    public: 
      AsymmetryBase(){
        fLabel         = "";
        fTargetNucleus = Nucleus::Proton();
      } 
      virtual ~AsymmetryBase(){} 

      void SetLabel(const char * l){ fLabel = l;}
      const char* GetLabel(){ return fLabel; }

      void SetTargetNucleus(const Nucleus& t){ fTargetNucleus = t;}

      virtual Double_t A1(Double_t x,Double_t Q2) = 0;
      virtual Double_t A2(Double_t x,Double_t Q2) = 0;

      //double EvaluateA1n(double *x,double *p){
      //  return A1(x[0],p[0]);
      //}
      //double EvaluateA1p(double *x,double *p){
      //  return A1(x[0],p[0]);
      //}
      //double EvaluateA1d(double *x,double *p){
      //  return A1(x[0],p[0]);
      //}
      //double EvaluateA1He3(double *x,double *p){
      //  return A1(x[0],p[0]);
      //}
      //double EvaluateA2n(double *x,double *p){
      //  return A2(x[0],p[0]);
      //}
      //double EvaluateA2p(double *x,double *p){
      //  return A2(x[0],p[0]);
      //}
      //double EvaluateA2d(double *x,double *p){
      //  return A2(x[0],p[0]);
      //}
      //double EvaluateA2He3(double *x,double *p){
      //  return A2(x[0],p[0]);
      //}

      ClassDef(AsymmetryBase,3)

    };
  }
}

#endif 
