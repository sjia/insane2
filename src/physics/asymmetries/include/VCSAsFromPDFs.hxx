#ifndef insane_physics_VCSAsFromPDFs_HHXX
#define insane_physics_VCSAsFromPDFs_HHXX

// ------------------------------------------
namespace insane {
  namespace physics {

    template< 
      template<class...> class T1, class...T1args,
      template<class...> class T2, class...T2args>
    double   VCSAsFromPDFs<T1<T1args...>,T2<T2args...>>::Calculate(double x, double Q2, std::tuple<ComptonAsymmetry,Nuclei> sf, Twist t, OPELimit l) const
    {
      auto sftype = std::get<ComptonAsymmetry>(sf);
      auto target = std::get<Nuclei>(sf);

      bool use_tmc = true;
      if(l == OPELimit::MasslessTarget) {
        use_tmc = false;
      }
      //constexpr int n_HT   = sizeof...(HT);
      double        result = 0.0;

      switch(sftype) {

        case ComptonAsymmetry::A1 :
          if(use_tmc) {
            result = insane::physics::A1(fPDFs,fPPDFs,target, x, Q2); 
          } else {
            result = insane::physics::A1_NoTMC(fPDFs,fPPDFs,target, x, Q2); 
          }
          break;

        case ComptonAsymmetry::A2 :
          if(use_tmc) {
            result = insane::physics::A2(fPDFs,fPPDFs,target, x, Q2); 
          } else {
            result = insane::physics::A2_NoTMC(fPDFs,fPPDFs,target, x, Q2); 
          }
          break;

        default:
          result =  0.0;
          break;
      }
      return result;
    }
    //______________________________________________________________________________

    template< 
      template<class...> class T1, class...T1args,
      template<class...> class T2, class...T2args>
    double   VCSAsFromPDFs<T1<T1args...>,T2<T2args...>>::Uncertainties(double x, double Q2, std::tuple<ComptonAsymmetry,Nuclei> sf, Twist t, OPELimit l) const
    {
      return 0.0;
    }
    //______________________________________________________________________________


  }
}

#endif

