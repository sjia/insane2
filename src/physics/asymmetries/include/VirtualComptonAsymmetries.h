#ifndef VirtualComptonAsymmetries_HH
#define VirtualComptonAsymmetries_HH 1

#include "TNamed.h"
#include "TF2.h"
#include "TF1.h"
#include "InSANEMathFunc.h"

namespace insane {

  using namespace units;
  namespace physics {

    class StructureFunctions    ;
    class SpinStructureFunctions;

    /** ABC for A1 and A2
    */
    class VCSABase : public TNamed {

    public:
      typedef enum  { 
        kA1p   ,
        kA1n   ,
        kA1d   ,
        kA1He3 ,

        kA2p ,
        kA2n ,
        kA2d ,
        kA2He3 
      } AsymmetryType;


    public:
      VCSABase(){}
      virtual ~VCSABase(){}

      virtual Double_t A1p(   Double_t x, Double_t Q2) = 0;
      virtual Double_t A2p(   Double_t x, Double_t Q2) = 0;
      virtual Double_t A1n(   Double_t x, Double_t Q2) = 0;
      virtual Double_t A2n(   Double_t x, Double_t Q2) = 0;
      virtual Double_t A1d(   Double_t x, Double_t Q2) = 0;
      virtual Double_t A2d(   Double_t x, Double_t Q2) = 0;
      virtual Double_t A1He3( Double_t x, Double_t Q2) = 0;
      virtual Double_t A2He3( Double_t x, Double_t Q2) = 0;

      virtual Double_t A1p_Error(   Double_t x, Double_t Q2) = 0;
      virtual Double_t A2p_Error(   Double_t x, Double_t Q2) = 0;
      virtual Double_t A1n_Error(   Double_t x, Double_t Q2) = 0;
      virtual Double_t A2n_Error(   Double_t x, Double_t Q2) = 0;
      virtual Double_t A1d_Error(   Double_t x, Double_t Q2) = 0;
      virtual Double_t A2d_Error(   Double_t x, Double_t Q2) = 0;
      virtual Double_t A1He3_Error( Double_t x, Double_t Q2) = 0;
      virtual Double_t A2He3_Error( Double_t x, Double_t Q2) = 0;

      // function of x with Q2 = p[0]
      double EvaluateA1p(double *x, double *p) {
        return( A1p(x[0], p[0]) );
      }
      double EvaluateA2p(double *x, double *p) {
        return( A2p(x[0], p[0]) );
      }
      double EvaluateA1n(double *x, double *p) {
        return( A1n(x[0], p[0]) );
      }
      double EvaluateA2n(double *x, double *p) {
        return( A2n(x[0], p[0]) );
      }
      double EvaluateA1d(double *x, double *p) {
        return( A1d(x[0], p[0]) );
      }
      double EvaluateA2d(double *x, double *p) {
        return( A2d(x[0], p[0]) );
      }
      // function of W with Q2 = p[0]
      //double EvaluateA1p_W(double *x, double *p) {
      //  Double_t W = x[0];
      //  Double_t xbj = insane::Kine::xBjorken_WQ2(W,p[0]);
      //  return( A1p(xbj, p[0]) );
      //}
      //double EvaluateA2p_W(double *x, double *p) {
      //  Double_t W = x[0];
      //  Double_t xbj = insane::Kine::xBjorken_WQ2(W,p[0]);
      //  return( A2p(xbj, p[0]) );
      //}
      //double EvaluateA1n_W(double *x, double *p) {
      //  Double_t W = x[0];
      //  Double_t xbj = insane::Kine::xBjorken_WQ2(W,p[0]);
      //  return( A1n(xbj, p[0]) );
      //}
      //double EvaluateA2n_W(double *x, double *p) {
      //  Double_t W = x[0];
      //  Double_t xbj = insane::Kine::xBjorken_WQ2(W,p[0]);
      //  return( A2n(xbj, p[0]) );
      //}
      //double EvaluateA1d_W(double *x, double *p) {
      //  Double_t W = x[0];
      //  Double_t xbj = insane::Kine::xBjorken_WQ2(W,p[0]);
      //  return( A1d(xbj, p[0]) );
      //}
      //double EvaluateA2d_W(double *x, double *p) {
      //  Double_t W = x[0];
      //  Double_t xbj = insane::Kine::xBjorken_WQ2(W,p[0]);
      //  return( A2d(xbj, p[0]) );
      //}

      //// function of Q2 with x = p[0]
      //double EvaluateA1p_Q2(double *x, double *p) {
      //  Double_t Q2  = x[0];
      //  Double_t xbj = p[0] ;
      //  return( A1p(xbj, Q2) );
      //}
      //double EvaluateA2p_Q2(double *x, double *p) {
      //  Double_t Q2  = x[0];
      //  Double_t xbj = p[0] ;
      //  return( A2p(xbj, Q2) );
      //}


      ClassDef(VCSABase,1)
    };

    /** Concrete imp for A1 and A2.
     *  By default it uses the function manager's structure functions (polarized and unpolarized).
     *
     *  \f$ A_1 = \frac{g_1 - (4M^2x^2/Q^2)g_2}{F_1} \f$
     *  \f$ A_2 = \frac{2Mx}{\sqrt{Q^2}} \frac{g_1 + g_2}{F_1} \f$
     */
    class VirtualComptonAsymmetries : public VCSABase {

    protected:

      StructureFunctions         * fSFs;
      SpinStructureFunctions     * fPolSFs;

    public:
      VirtualComptonAsymmetries();
      VirtualComptonAsymmetries(StructureFunctions*, SpinStructureFunctions*);
      virtual ~VirtualComptonAsymmetries();

      virtual Double_t A1p(   Double_t x, Double_t Q2);
      virtual Double_t A2p(   Double_t x, Double_t Q2);
      virtual Double_t A1n(   Double_t x, Double_t Q2);
      virtual Double_t A2n(   Double_t x, Double_t Q2);
      virtual Double_t A1d(   Double_t x, Double_t Q2);
      virtual Double_t A2d(   Double_t x, Double_t Q2);
      virtual Double_t A1He3( Double_t x, Double_t Q2);
      virtual Double_t A2He3( Double_t x, Double_t Q2);

      virtual Double_t A1p_Error(   Double_t x, Double_t Q2);
      virtual Double_t A2p_Error(   Double_t x, Double_t Q2);
      virtual Double_t A1n_Error(   Double_t x, Double_t Q2);
      virtual Double_t A2n_Error(   Double_t x, Double_t Q2);
      virtual Double_t A1d_Error(   Double_t x, Double_t Q2);
      virtual Double_t A2d_Error(   Double_t x, Double_t Q2);
      virtual Double_t A1He3_Error( Double_t x, Double_t Q2);
      virtual Double_t A2He3_Error( Double_t x, Double_t Q2);

      virtual Double_t A1p_TMC(   Double_t x, Double_t Q2);
      virtual Double_t A2p_TMC(   Double_t x, Double_t Q2);
      //virtual Double_t A1n_TMC(   Double_t x, Double_t Q2);
      //virtual Double_t A2n_TMC(   Double_t x, Double_t Q2);
      //virtual Double_t A1d_TMC(   Double_t x, Double_t Q2);
      //virtual Double_t A2d_TMC(   Double_t x, Double_t Q2);
      //virtual Double_t A1He3_TMC( Double_t x, Double_t Q2);
      //virtual Double_t A2He3_TMC( Double_t x, Double_t Q2);

      void SetUnpolarizedSFs(StructureFunctions *usfs){ fSFs = usfs; } 
      void SetPolarizedSFs(SpinStructureFunctions *psfs){ fPolSFs = psfs; } 

      ///** Fills supplied histogram vs x.
      // *
      // */
      //void GetValues(TObject *obj, Double_t Q2, VCSABase::AsymmetryType = VCSABase::kA1p, insane::Kine::Variable var = insane::Kine::kx );

      ///** Get error band.
      // *  Argument obj is assumed to be a TH1. 
      // */ 
      //void GetErrorBand(TObject *obj, Double_t Q2, VCSABase::AsymmetryType = VCSABase::kA1p , insane::Kine::Variable var = insane::Kine::kx);


      ClassDef(VirtualComptonAsymmetries,1)
    };


    /** (p0 + p1 x + p2 x^2 + p3 x^3) x^p4
    */
    class VirtualComptonAsymmetriesModel1 : public VirtualComptonAsymmetries {

    protected:
      Double_t fP_A1p[5];
      Double_t fP_A2p[5];
      TF1 *    fA1pFunc;
      TF1 *    fA2pFunc;

      Double_t ModelFunction(Double_t x, Double_t Q2, const Double_t *p) const {
        double   W  = insane::Kine::W_xQsq(x, Q2);
        Double_t t1 = p[0] + p[1]/W + 0.0*p[2]/(W*W) + p[3]/TMath::Sqrt(Q2) + 0.0*p[4]*TMath::Sqrt(Q2) ;
        return t1;
        //return( t1*TMath::Power(x,p[4]) );
      }

    public: 
      VirtualComptonAsymmetriesModel1(){
        fP_A1p[0] =  1.0;
        fP_A1p[1] =  0.0;
        fP_A1p[2] = -0.2;
        fP_A1p[3] = -0.6;
        fP_A1p[4] =  0.7;
        fA1pFunc = new TF1("A1p",this, &VirtualComptonAsymmetriesModel1::EvaluateA1p,
                           0.0,1.0, 1,"VirtualComptonAsymmetriesModel1","EvaluateA1p");

        fP_A2p[0] =  0.25;
        fP_A2p[1] = -0.1;
        fP_A2p[2] =  0.3;
        fP_A2p[3] = -0.3;
        fP_A2p[4] =  0.9;
        fA2pFunc = new TF1("A2p",this, &VirtualComptonAsymmetriesModel1::EvaluateA2p,
                           0.0, 1.0, 1,"VirtualComptonAsymmetriesModel1","EvaluateA2p");
      }
      virtual ~VirtualComptonAsymmetriesModel1(){ }

      virtual Double_t * GetA1pParameters(){ return fP_A1p;}
      virtual Double_t * GetA2pParameters(){ return fP_A2p;}
      virtual TF1 * GetA1pFunction(){ return fA1pFunc; }
      virtual TF1 * GetA2pFunction(){ return fA2pFunc; }

      virtual Double_t A1p(Double_t x, Double_t Q2){
        return( ModelFunction(x,Q2,fP_A1p) ) ;
      } 
      virtual Double_t A2p(Double_t x, Double_t Q2){ 
        return( ModelFunction(x,Q2,fP_A2p) ) ;
      } 

      ClassDef(VirtualComptonAsymmetriesModel1,1)
    };

  }}
#endif

