#include "JAM_PPDFs.h"
#include "FortranWrappers.h"

namespace insane {
  namespace physics {

    JAM_PPDFs::JAM_PPDFs()
    {
      SetNameTitle("JAM_PPDFs","JAM pol. PDFs");
      SetLabel("JAM");
      char * lib_string  = const_cast<char*>("JAM15        ");
      char * dist_string = const_cast<char*>("PPDF         ");
      char * path_string = const_cast<char*>("             ");
      int    ipos = 0;
      // lib (character*10): library (JAM15,JAM16,etc.)
      // dist (character*10): distribution type (PPDF,FFpion,FFkaon,etc.)
      // ipos (integer): posterior number (0 to 199) from MC analysis
      grid_init_( path_string, lib_string, dist_string, &ipos );
      Reset();
    }
    //______________________________________________________________________________

    JAM_PPDFs::~JAM_PPDFs()
    { }
    //______________________________________________________________________________
    
    const std::array<double,NPartons>& JAM_PPDFs::Calculate(double x, double Q2) const
    {
      fXbjorken      = x;
      fQsquared      = Q2;
      double  f = 0.0;
      char *  flav;

      flav =  const_cast<char*>("up"); double u_plus = 0.0;
      jam_xf_(&u_plus, &fXbjorken, &fQsquared, flav );

      flav =  const_cast<char*>("dp"); double d_plus = 0.0;
      jam_xf_(&d_plus,&fXbjorken, &fQsquared, flav );

      flav =  const_cast<char*>("sp"); double s_plus = 0.0;
      jam_xf_(&s_plus, &fXbjorken, &fQsquared, flav );

      flav =  const_cast<char*>("u"); double u      = 0.0;
      jam_xf_(&u, &fXbjorken, &fQsquared, flav );

      flav =  const_cast<char*>("d"); double d      = 0.0;
      jam_xf_(&d, &fXbjorken, &fQsquared, flav );

      flav =  const_cast<char*>("s"); double s      = 0.0;
      jam_xf_(&s, &fXbjorken, &fQsquared, flav );

      flav =  const_cast<char*>("gl"); double gl     = 0.0;
      jam_xf_(&gl, &fXbjorken, &fQsquared, flav );

      fValues[PartonFlavor::kUP]          = u/x; //Delta up
      fValues[PartonFlavor::kDOWN]        = d/x; //Delta down
      fValues[PartonFlavor::kSTRANGE]     = s/x;      //Delta s
      fValues[PartonFlavor::kGLUON]       = gl/x;       //Delta g
      fValues[PartonFlavor::kANTIUP]      = (u_plus-u)/x;     //Delta ubar
      fValues[PartonFlavor::kANTIDOWN]    = (d_plus-d)/x;     //Delta dbar
      fValues[PartonFlavor::kANTISTRANGE] = (s_plus-s)/x;     //Delta sbar

      return fValues;
    }
    //______________________________________________________________________________

    const std::array<double,NPartons>& JAM_PPDFs::Uncertainties(double x, double Q2) const 
    {
      // do calculations
      return fUncertainties;
    }


  }
}
