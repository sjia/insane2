#include "StatisticalUnpolarizedPDFs.h"

namespace insane {
namespace physics {
//______________________________________________________________________________
StatisticalUnpolarizedPDFs::StatisticalUnpolarizedPDFs(){
   SetLabel("Statistical");
   SetLineColor(7);
   UseQ2Interpolation(true);
}
//______________________________________________________________________________
StatisticalUnpolarizedPDFs::~StatisticalUnpolarizedPDFs(){

}
//______________________________________________________________________________
double *StatisticalUnpolarizedPDFs::GetPDFs(double x,double Q2){

   /** Implementation of pure virtual method.
    *
    *  The function Ctq6Pdf (Iparton, X, Q)
    *  returns the parton distribution inside the proton for parton [Iparton]
    *  at [X] Bjorken_X and scale [Q] (GeV) in PDF set [Iset].
    *  Iparton  is the parton label (5, 4, 3, 2, 1, 0, -1, ......, -5)
    *                           for (b, c, s, d, u, g, u_bar, ..., b_bar),
    */

   //std::cout << " StatisticalUnpolarizedPDFs::GetPDFs " << std::endl;
   double qplus,qminus;
   double qtot;

   fXbjorken = x;
   fQsquared = Q2;

   // If we use Soffer's Q2 interpolation... 
   Bool_t IsInterp = fStatisticalFits.IsInterpolated(); 

   if(IsInterp){
      fPDFValues[0]  = fStatisticalFits.GetUQuarkInterp(x,Q2); 
      fPDFValues[1]  = fStatisticalFits.GetDQuarkInterp(x,Q2); 
      fPDFValues[2]  = fStatisticalFits.GetSQuarkInterp(x,Q2); 
      fPDFValues[3]  = fStatisticalFits.GetCQuarkInterp(x,Q2); 
      fPDFValues[4]  = 0.; 
      fPDFValues[5]  = 0.; 
      fPDFValues[6]  = fStatisticalFits.GetGluonInterp(x,Q2); 
      fPDFValues[7]  = fStatisticalFits.GetAntiUQuarkInterp(x,Q2);  
      fPDFValues[8]  = fStatisticalFits.GetAntiDQuarkInterp(x,Q2);  
      fPDFValues[9]  = fStatisticalFits.GetAntiSQuarkInterp(x,Q2);  
      fPDFValues[10] = 0.; 
      fPDFValues[11] = 0.; 
      fPDFValues[12] = 0.; 
   }else{
      // up quark 
      qplus  = fStatisticalFits.GetUQuark(1,x,Q2);        
      qminus = fStatisticalFits.GetUQuark(-1,x,Q2);        
      qtot   = qplus + qminus;
      fPDFValues[0] = qtot; //up
      // down quark 
      qplus  = fStatisticalFits.GetDQuark(1,x,Q2);        
      qminus = fStatisticalFits.GetDQuark(-1,x,Q2);        
      qtot   = qplus + qminus;
      fPDFValues[1] = qtot ; 
      // strange quark 
      qplus  = fStatisticalFits.GetSQuark(1,x,Q2);        
      qminus = fStatisticalFits.GetSQuark(-1,x,Q2);        
      qtot   = qplus + qminus;
      fPDFValues[2] = qtot ; //s
      // charm, bottom, top quarks
      fPDFValues[3] = 0;//c
      fPDFValues[4] = 0;//b
      fPDFValues[5] = 0;//t
      // gluon 
      qtot = fStatisticalFits.GetGluon(x,Q2);        
      fPDFValues[6] = qtot ; //g
      // anti-up quark 
      qplus  = fStatisticalFits.GetAntiUQuark(1,x,Q2);        
      qminus = fStatisticalFits.GetAntiUQuark(-1,x,Q2);        
      qtot   = qplus + qminus;
      fPDFValues[7] = qtot ; //ubar
      // anti-down quark 
      qplus  = fStatisticalFits.GetAntiDQuark(1,x,Q2);        
      qminus = fStatisticalFits.GetAntiDQuark(-1,x,Q2);        
      qtot   = qplus + qminus;
      fPDFValues[8] = qtot ; //dbar
      // anti-strange quark 
      qplus  = fStatisticalFits.GetAntiSQuark(1,x,Q2);        
      qminus = fStatisticalFits.GetAntiSQuark(-1,x,Q2);        
      qtot   = qplus + qminus;
      fPDFValues[9] = qtot ; //sbar
      // anti-charm, bottom, top quarks 
      fPDFValues[10] = 0;//cbar
      fPDFValues[11] = 0;//bbar
      fPDFValues[12] = 0;//tbar
   }

   return fPDFValues;

}
}}
