#include "CTEQ6UnpolarizedPDFs.h"

namespace insane {
namespace physics {

//______________________________________________________________________________
CTEQ6UnpolarizedPDFs::CTEQ6UnpolarizedPDFs(){
   int i = 1;
   setctq6_(&i);
   SetLabel("CTEQ6");
   SetNameTitle("CTEQ6UnpolarizedPDFs","CTEQ6 PDFs");
   SetLineColor(4);
}
//______________________________________________________________________________
CTEQ6UnpolarizedPDFs::~CTEQ6UnpolarizedPDFs(){

}
//______________________________________________________________________________
double *CTEQ6UnpolarizedPDFs::GetPDFs(double x,double Q2){

   /** Implementation of pure virtual method
    *
    *  The function Ctq6Pdf (Iparton, X, Q)
    *  returns the parton distribution inside the proton for parton [Iparton]
    *  at [X] Bjorken_X and scale [Q] (GeV) in PDF set [Iset].
    *  Iparton  is the parton label (5, 4, 3, 2, 1, 0, -1, ......, -5)
    *                           for (b, c, s, d, u, g, u_bar, ..., b_bar),
    */

   fXbjorken = x;
   fQsquared = Q2;
   double Q = TMath::Sqrt(Q2);
   double res = 0.0;
   int i = 0;
   i = 1;
   getctq6_(&i, &x, &Q, &res);
   fPDFValues[0] = res ; //up
   i = 2;
   getctq6_(&i, &x, &Q, &res);
   fPDFValues[1] = res ; //down
   i = 3;
   getctq6_(&i, &x, &Q, &res);
   fPDFValues[2] = res ; //s
   fPDFValues[3] = 0;//c
   fPDFValues[4] = 0;//b
   fPDFValues[5] = 0;//t
   i = 0;
   getctq6_(&i, &x, &Q, &res);
   fPDFValues[6] = res ; //g
   i = -1;
   getctq6_(&i, &x, &Q, &res);
   fPDFValues[7] = res ; //ubar
   i = -2;
   getctq6_(&i, &x, &Q, &res);
   fPDFValues[8] = res ; //dbar
   i = -3;
   getctq6_(&i, &x, &Q, &res);
   fPDFValues[9] = res ; //sbar
   fPDFValues[10] = 0;//cbar
   fPDFValues[11] = 0;//bbar
   fPDFValues[12] = 0;//tbar

   return fPDFValues;

}
}}

