#include "JAM15PolarizedPDFs.h"

namespace insane {
namespace physics {
JAM15PolarizedPDFs::JAM15PolarizedPDFs(int set)
{
   SetNameTitle("JAM15PolarizedPDFs","BB pol. PDFs");
   SetLabel("JAM15");
   SetLineColor(6);
   SetLineStyle(7);
   // These values are always zero.
   fPDFValues[3] = 0.0;//Delta c
   fPDFValues[4] = 0.0;//Delta b
   fPDFValues[5] = 0.0;//Delta t
   fPDFValues[10] = 0.0;//Delta cbar
   fPDFValues[11] = 0.0;//Delta bbar
   fPDFValues[12] = 0.0;//Delta tbar

   char * lib_string  = const_cast<char*>("JAM15        ");
   char * dist_string = const_cast<char*>("PPDF         ");
   char * path_string = const_cast<char*>("             ");
   int    ipos = set;
   // lib (character*10): library (JAM15,JAM16,etc.)
   // dist (character*10): distribution type (PPDF,FFpion,FFkaon,etc.)
   // ipos (integer): posterior number (0 to 199) from MC analysis
   for(Int_t i=0;i<12;i++) fPDFErrors[i] = 0.; 
   grid_init_( path_string, lib_string, dist_string, &ipos );

   fPDFErrors[0] = 0.0;//TMath::Sqrt(fPDFErrors[0]*fPDFErrors[0]+Dqbar*Dqbar)/x; 
   fPDFErrors[1] = 0.0;//TMath::Sqrt(fPDFErrors[1]*fPDFErrors[1]+Dqbar*Dqbar)/x; 
   fPDFErrors[2] = 0.0;//Dqbar/x; 
   fPDFErrors[6] = 0.0;//fPDFErrors[6]/x; 
   fPDFErrors[7] = 0.0;//Dqbar/x; 
   fPDFErrors[8] = 0.0;//Dqbar/x; 
   fPDFErrors[9] = 0.0;//Dqbar/x; 
}
//______________________________________________________________________________

JAM15PolarizedPDFs::~JAM15PolarizedPDFs()
{ }
//______________________________________________________________________________

double *JAM15PolarizedPDFs::GetPDFs(double x,double Qsq)
{
   fXbjorken = x;
   fQsquared = Qsq;
   char * flav;

   flav =  const_cast<char*>("up"); double u_plus = 0.0;
   jam_xf_(&u_plus, &fXbjorken, &fQsquared, flav );

   flav =  const_cast<char*>("dp"); double d_plus = 0.0;
   jam_xf_(&d_plus,&fXbjorken, &fQsquared, flav );

   flav =  const_cast<char*>("sp"); double s_plus = 0.0;
   jam_xf_(&s_plus, &fXbjorken, &fQsquared, flav );

   flav =  const_cast<char*>("u"); double u      = 0.0;
   jam_xf_(&u, &fXbjorken, &fQsquared, flav );

   flav =  const_cast<char*>("d"); double d      = 0.0;
   jam_xf_(&d, &fXbjorken, &fQsquared, flav );

   flav =  const_cast<char*>("s"); double s      = 0.0;
   jam_xf_(&s, &fXbjorken, &fQsquared, flav );

   flav =  const_cast<char*>("gl"); double gl     = 0.0;
   jam_xf_(&gl, &fXbjorken, &fQsquared, flav );

   fPDFValues[0] = u/x; //Delta up
   fPDFValues[1] = d/x; //Delta down
   fPDFValues[2] = s/x;      //Delta s
   fPDFValues[6] = gl/x;       //Delta g
   fPDFValues[7] = (u_plus-u)/x;     //Delta ubar
   fPDFValues[8] = (d_plus-d)/x;     //Delta dbar
   fPDFValues[9] = (s_plus-s)/x;     //Delta sbar

   double  t3u = 0.0;
   double  t3d = 0.0;
     flav =  const_cast<char*>("u3");
     jam_xf_(&t3u, &fXbjorken, &fQsquared, flav );

     flav =  const_cast<char*>("d3");
     jam_xf_(&t3d, &fXbjorken, &fQsquared, flav );

   fxDu_Twist3 = t3u;
   fxDd_Twist3 = t3d;

   flav =  const_cast<char*>("p4"); double t4p     = 0.0;
   jam_xf_(&t4p, &fXbjorken, &fQsquared, flav );

   flav =  const_cast<char*>("n4"); double t4n     = 0.0;
   jam_xf_(&t4n, &fXbjorken, &fQsquared, flav );

   fxHp_Twist4 = t4p;
   fxHn_Twist4 = t4n;
   //std::cout << "fDu_Twist3 " << fDu_Twist3 << '\n';

   return(fPDFValues);
}
//______________________________________________________________________________

double *JAM15PolarizedPDFs::GetPDFErrors(double x,double Qsq)
{

   fXbjorken    = x;
   fQsquared    = Qsq;
   double qbar  = 0.0;
   double GL    = 0.0;
   double UV    = 0.0;
   double DV    = 0.0;
   double Dqbar = 0.0;
   //ppdf_(&fiSet, &fXbjorken, &fQsquared,
   //      /*UV=*/&UV,/*DUV=*/&fPDFErrors[0],
   //      /*DV=*/&DV,/*DDV=*/&fPDFErrors[1],
   //      /*GL=*/&GL,/*DGL=*/&fPDFErrors[6],
   //      /*QB=*/&qbar,/*DQB=*/&Dqbar,
   //      &fg1p, &fDg1p, &fg1n, &fDg1n);
   // fPDFValues[0] = (UV + qbar)/x; //Delta up
   // fPDFValues[1] = (DV + qbar)/x; //Delta down
   // fPDFValues[2] = 0.0;      //Delta s
   // fPDFValues[6] = GL/x;       //Delta g
   // fPDFValues[7] = qbar/x;     //Delta ubar
   // fPDFValues[8] = qbar/x;     //Delta dbar
   // fPDFValues[9] = 0.0;      //Delta sbar
   fPDFErrors[0] = 0.0;//TMath::Sqrt(fPDFErrors[0]*fPDFErrors[0]+Dqbar*Dqbar)/x; 
   fPDFErrors[1] = 0.0;//TMath::Sqrt(fPDFErrors[1]*fPDFErrors[1]+Dqbar*Dqbar)/x; 
   fPDFErrors[2] = 0.0;//Dqbar/x; 
   fPDFErrors[6] = 0.0;//fPDFErrors[6]/x; 
   fPDFErrors[7] = 0.0;//Dqbar/x; 
   fPDFErrors[8] = 0.0;//Dqbar/x; 
   fPDFErrors[9] = 0.0;//Dqbar/x; 

   return(fPDFErrors);
}
//______________________________________________________________________________
double JAM15PolarizedPDFs::Hp_Twist4(double x, double Q2)
{
  return 0.0;
  GetPDFs(x,Q2);
  return fxHp_Twist4/x;
}
//______________________________________________________________________________

double JAM15PolarizedPDFs::Hn_Twist4(double x, double Q2)
{
  return 0.0;
  GetPDFs(x,Q2);
  return fxHn_Twist4/x;
}
//______________________________________________________________________________

}}

