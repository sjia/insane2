#include "GSPolarizedPDFs.h"

namespace insane {
namespace physics {
GSPolarizedPDFs::GSPolarizedPDFs(){
   SetNameTitle("GSPolarizedPDFs","GS1996 pol. PDFs");
   SetLabel("GS1996");
   SetLineColor(2);
   SetLineStyle(1);
   fiSet = 1;
   nloini_();
   // These values are always zero.
   fPDFValues[3] = 0.0;//Delta c
   fPDFValues[4] = 0.0;//Delta b
   fPDFValues[5] = 0.0;//Delta t
   fPDFValues[10] = 0.0;//Delta cbar
   fPDFValues[11] = 0.0;//Delta bbar
   fPDFValues[12] = 0.0;//Delta tbar
   for(Int_t i=0;i<12;i++) fPDFErrors[i] = 0.;

}
//______________________________________________________________________________
GSPolarizedPDFs::~GSPolarizedPDFs(){

}
//______________________________________________________________________________
double *GSPolarizedPDFs::GetPDFs(double x,double Qsq){

   fXbjorken = x;
   fQsquared = Qsq;
   double uval = 0.0;
   double dval = 0.0;
   double glue = 0.0;
   double ubar = 0.0;
   double dbar = 0.0;
   double str = 0.0;
   polnlo_(&fiSet, &fXbjorken, &fQsquared, &uval, &dval, &glue, &ubar, &dbar, &str);
   fPDFValues[0] = (uval + ubar)/x; //Delta up
   fPDFValues[1] = (dval + dbar)/x; //Delta down
   fPDFValues[2] = str/x;      //Delta s
   fPDFValues[6] = glue/x;       //Delta g
   fPDFValues[7] = ubar/x;     //Delta ubar
   fPDFValues[8] = dbar/x;     //Delta dbar
   fPDFValues[9] = str/x;      //Delta sbar
   return(fPDFValues);

}

}}
