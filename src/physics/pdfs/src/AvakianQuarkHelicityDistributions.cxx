#include "AvakianQuarkHelicityDistributions.h"

namespace insane {
namespace physics {
//______________________________________________________________________________
AvakianQuarkHelicityDistributions::AvakianQuarkHelicityDistributions(){

   /// NOTE: These coefficents are identical to LSS98, with two additional ones
   /// labeled Cup and Cdp, which multiply the logarithmic dependent components 

   fAu      =  3.088;
   fAd      =  0.343;
   fBu      = -3.010;
   fBd      = -0.265;
   fCu      =  2.143;
   fCd      =  1.689;
   fDu      = -2.065;
   fDd      = -1.610;
   fCs      =  0.334; // +/- 0.044 //  1.0;        
   fAs      =  0.001;              // -0.6980 + 0.9877*fCs; 
   fBs      =  0.041;              //  0.8534 - 1.1171*fCs;  
   fDs      = -0.292; // +/- 0.042 //  0.1551 - 1.1294*fCs;
   fAlpha   =  1.313; // +/- 0.056 //  1.12;     /// QCD Pomeron intercept  
   fAlpha_g =  1.233; // +/- 0.073 //  1.0;  
   fAg      =  1.019;              //  0.2381;                   
   fBg      = -0.339; // +/- 0.056 //  1.1739; 
   fCup     =  0.493; // +/- 0.249   
   fCdp     =  1.592; // +/- 0.378 

}
//______________________________________________________________________________
AvakianQuarkHelicityDistributions::~AvakianQuarkHelicityDistributions(){

}
//______________________________________________________________________________
double AvakianQuarkHelicityDistributions::uPlus(double x){
   double arg = fAu*TMath::Power(1.-x,3.) + fBu*TMath::Power(1.-x,4.); 
   double res = arg/( TMath::Power(x,fAlpha) ); 
   return res; 
}
//______________________________________________________________________________
double AvakianQuarkHelicityDistributions::uMinus(double x){
   /// note how this is different from BBS
   double arg = fCu*TMath::Power(1.-x,5.) + fCup*TMath::Power(1.-x,5.)*TMath::Power(TMath::Log(1.-x),2.) 
                + fDu*TMath::Power(1.-x,6.); 
   double res = arg/( TMath::Power(x,fAlpha) ); 
   return res; 
}
//______________________________________________________________________________
double AvakianQuarkHelicityDistributions::dPlus(double x){
   double arg = fAd*TMath::Power(1.-x,3.) + fBd*TMath::Power(1.-x,4.); 
   double res = arg/( TMath::Power(x,fAlpha) ); 
   return res; 
}
//______________________________________________________________________________
double AvakianQuarkHelicityDistributions::dMinus(double x){
   /// note how this is different from BBS
   double arg = fCd*TMath::Power(1.-x,5.) + fCdp*TMath::Power(1.-x,5)*TMath::Power(TMath::Log(1.-x),2.) 
                + fDd*TMath::Power(1.-x,6.); 
   double res = arg/( TMath::Power(x,fAlpha) ); 
   return res; 
}
//______________________________________________________________________________
double AvakianQuarkHelicityDistributions::sPlus(double x){
   double arg = fAs*TMath::Power(1.-x,5.) + fBs*TMath::Power(1.-x,6.); 
   double res = arg/( TMath::Power(x,fAlpha) ); 
   return res; 
}
//______________________________________________________________________________
double AvakianQuarkHelicityDistributions::sMinus(double x){
   double arg = fCs*TMath::Power(1.-x,7.) + fDs*TMath::Power(1.-x,8.); 
   double res = arg/( TMath::Power(x,fAlpha) ); 
   return res; 
}
//______________________________________________________________________________
double AvakianQuarkHelicityDistributions::gPlus(double x){
   double arg = fAg*TMath::Power(1.-x,4.) + fBg*TMath::Power(1.-x,5.); 
   double res = arg/( TMath::Power(x,fAlpha_g) ); 
   return res; 
}
//______________________________________________________________________________
double AvakianQuarkHelicityDistributions::gMinus(double x){
   double arg = fAg*TMath::Power(1.-x,6.) + fBg*TMath::Power(1.-x,7.); 
   double res = arg/( TMath::Power(x,fAlpha_g) ); 
   return res; 
}
//______________________________________________________________________________
Int_t AvakianQuarkHelicityDistributions::CalculateDistributions(double x, double Q2){
   
   fU.fQ_Plus     = uPlus(x);
   fU.fQ_Minus    = uMinus(x);
   fU.fQbar_Plus  = 0.0;
   fU.fQbar_Minus = 0.0;

   fD.fQ_Plus     = dPlus(x);
   fD.fQ_Minus    = dMinus(x);
   fD.fQbar_Plus  = 0.0;
   fD.fQbar_Minus = 0.0;

   fS.fQ_Plus     = sPlus(x);
   fS.fQ_Minus    = sMinus(x);
   fS.fQbar_Plus  = 0.0;
   fS.fQbar_Minus = 0.0;

   fG.fQ_Plus     = gPlus(x);
   fG.fQ_Minus    = gMinus(x);
   fG.fQbar_Plus  = 0.0;
   fG.fQbar_Minus = 0.0;
   
   return 0;
}
//______________________________________________________________________________
}}
