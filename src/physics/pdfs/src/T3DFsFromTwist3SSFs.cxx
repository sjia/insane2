#include "T3DFsFromTwist3SSFs.h"

namespace insane {
  namespace physics {

    T3DFsFromTwist3SSFs::T3DFsFromTwist3SSFs() :
      fInputScale(1.0), 
      //fA(0.0486772), fB(1.57357), fC(5.94918), fD(6.74412), fE(2.19114),
      fParam_p( {0.0486772, 1.57357,  5.94918, 6.74412, 2.19114} ),
      fParam_n( {0.0655158, 0.130996, 1.12101, 2.31342, 1.20598} )
      {
    }
    //______________________________________________________________________________

    T3DFsFromTwist3SSFs::~T3DFsFromTwist3SSFs()
    { }
    //__________________________________________________________________________

    double T3DFsFromTwist3SSFs::g2pTwist3_model(double x) const
    {
      return( g2_model(x, fParam_p));
    }
    //__________________________________________________________________________
    double T3DFsFromTwist3SSFs::g2nTwist3_model(double x) const
    {
      return( g2_model(x, fParam_n));
    }
    //__________________________________________________________________________
    
    double T3DFsFromTwist3SSFs::g2_model(double x, const std::array<double,5>& p) const
    {
      //std::cout << " p0 = " << p[0] << "\n";
      double xbar = 1.0-x; 
      double  t1  = p[0]*(TMath::Log(x) + xbar + xbar*xbar/2.0) ;
      double  t2  = TMath::Power(xbar,3.0)*(p[1] - p[2]*xbar + p[3]*xbar*xbar - p[4]*xbar*xbar*xbar);
      //std::cout << t1+t2 << "\n";
      return( t1 + t2 );
    }
    //______________________________________________________________________________

    const std::array<double,NPartons>& T3DFsFromTwist3SSFs::Calculate(double x, double Q2) const
    {
      fXbjorken      = x;
      fQsquared      = Q2;
      double  t3_val = 0.0;
      //std::cout << "T3DFsFromTwist3SSFs::Calculate \n";
      // Taking the derivative of both sides
      // g2_tw3 = D(x) - \int_x^1 (dy/y) D(y) 
      // and solving the differential equation with the boundary condition
      // D(1) = 1
      // gives
      // D(x) = (-1/x) \int_x^1 (y[dg2/dx](y))
      double Dp = (-1.0/x)*insane::integrate::simple(
          [&](double z){
          double der =   z*insane::math::derivative(
              [&](double y){return g2p_Twist3(y,Q2);},
              z);
          //std::cout << " der (" << z << ") = " << der << "\n";
          return der;
          },
          x, 1.0, 300);
      //std::cout << " Dp = " << Dp << "\n";

      double Dn = 0.0;//(-1.0/x)*insane::integrate::simple(
      //    [&](double z){
      //    return z*insane::math::derivative(
      //        [&](double y){return g2n_Twist3(y,Q2);},
      //        z);},
      //    x, 1.0);
      double Du = (9.0/15.0)*(4.0*Dp - Dn);
      double Dd = (9.0/15.0)*(4.0*Dn - Dp);

      fValues[PartonFlavor::kUP]   = Du;
      fValues[PartonFlavor::kDOWN] = Dd;

      return fValues;
    }
    //______________________________________________________________________________

    const std::array<double,NPartons>& T3DFsFromTwist3SSFs::Uncertainties(double x, double Q2) const 
    {
      // do calculations
      return fUncertainties;
    }
    //__________________________________________________________________________

    double T3DFsFromTwist3SSFs::g2p_Twist3(double x, double Q2) const
    { 
      //double res = g2pTwist3_model(x);
      //std::cout << "  Twist3DistributionFunctions::g2p_Twist3 \n";
      //std::cout << res << " at (" << x << ", " << Q2 << ")\n";
      // override these so that they define the twist-3 distributions 
      return g2pTwist3_model(x);
    }
    //__________________________________________________________________________

    double T3DFsFromTwist3SSFs::g2n_Twist3(double x, double Q2) const
    { 
      // override these so that they define the twist-3 distributions 
      return g2nTwist3_model(x);
    }

  }
}

