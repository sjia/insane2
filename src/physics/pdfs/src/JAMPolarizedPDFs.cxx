#include "JAMPolarizedPDFs.h"
namespace insane {
namespace physics {
//______________________________________________________________________________
JAMPolarizedPDFs::JAMPolarizedPDFs(){
   SetNameTitle("JAMPolarizedPDFs","JAM pol. PDFs");
   SetLabel("JAM");
   SetLineColor(2);
   SetLineStyle(1);

   fPars_uplus[0] =  1.1167; fParsErr_uplus[0] = 0.17;
   fPars_uplus[1] =  0.8244; fParsErr_uplus[1] = 0.04;
   fPars_uplus[2] =  3.3244; fParsErr_uplus[2] = 0.12;
   fPars_uplus[3] = -1.8722; fParsErr_uplus[3] = 0.30;
   fPars_uplus[4] = 11.2858; fParsErr_uplus[4] = 0.8466;

   fPars_dplus[0] = -0.8374; fParsErr_dplus[0] = 0.17;
   fPars_dplus[1] =  0.7193; fParsErr_dplus[1] = 0.05;
   fPars_dplus[2] =  3.9932; fParsErr_dplus[2] = 0.40;
   fPars_dplus[3] = -1.9324; fParsErr_dplus[3] = 0.40;
   fPars_dplus[4] =  7.0703; fParsErr_dplus[4] = 0.7407;

   fPars_g[0] = -0.8120; fParsErr_g[0] = 0.90;
   fPars_g[1] =  1.7718; fParsErr_g[1] = 0.01;
   fPars_g[2] =  5.6588; fParsErr_g[2] = 0.01;
   fPars_g[3] =  0.0;    fParsErr_g[3] = 0.01;
   fPars_g[4] =-33.8287; fParsErr_g[4] = 21.0;

   fPars_ubar[0] =  0.5583; fParsErr_ubar[0] = 0.01;
   fPars_ubar[1] =  0.8224; fParsErr_ubar[1] = 0.01;
   fPars_ubar[2] =  7.6588; fParsErr_ubar[2] = 0.01;
   fPars_ubar[3] =  0.0;    fParsErr_ubar[3] = 0.01;
   fPars_ubar[4] =  0.0;    fParsErr_ubar[4] = 0.01;

   fPars_dbar[0] = -0.4186; fParsErr_dbar[0] = 0.01;
   fPars_dbar[1] =  0.7193; fParsErr_dbar[1] = 0.01;
   fPars_dbar[2] =  7.6588; fParsErr_dbar[2] = 0.01;
   fPars_dbar[3] =  0.0;    fParsErr_dbar[3] = 0.01;
   fPars_dbar[4] =  0.0;    fParsErr_dbar[4] = 0.01;

   fPars_sbar[0] = -0.2073; fParsErr_sbar[0] = 0.01;
   fPars_sbar[1] =  0.7193; fParsErr_sbar[1] = 0.01;
   fPars_sbar[2] =  7.6588; fParsErr_sbar[2] = 0.01;
   fPars_sbar[3] =  0.0;    fParsErr_sbar[3] = 0.01;
   fPars_sbar[4] =  0.0;    fParsErr_sbar[4] = 0.01;

   fPars_Twist3_p[0] = -0.0936;
   fPars_Twist3_p[1] =  0.2837;
   fPars_Twist3_p[2] =  0.7542;
   fPars_Twist3_p[3] = -1.5177;
   fPars_Twist3_p[4] =  0.0;

   fPars_Twist3_n[0] = -0.0193;
   fPars_Twist3_n[1] = -0.0136;
   fPars_Twist3_n[2] =  0.1062;
   fPars_Twist3_n[3] = -0.1456;
   fPars_Twist3_n[4] =  0.0;

   fKnots_x_p[0] = 0.0;
   fKnots_y_p[0] = 0.0;
   fKnots_x_p[1] = 0.1;
   fKnots_y_p[1] = 0.0118;
   fKnots_x_p[2] = 0.3;
   fKnots_y_p[2] =-0.0325;
   fKnots_x_p[3] = 0.5;
   fKnots_y_p[3] =-0.0271;
   fKnots_x_p[4] = 0.7;
   fKnots_y_p[4] =-0.0167;
   fKnots_x_p[5] = 1.0;
   fKnots_y_p[5] = 0.0;

   fKnots_x_n[0] = 0.0;
   fKnots_y_n[0] = 0.0;
   fKnots_x_n[1] = 0.1;
   fKnots_y_n[1] = 0.0079;
   fKnots_x_n[2] = 0.3;
   fKnots_y_n[2] = 0.0290;
   fKnots_x_n[3] = 0.5;
   fKnots_y_n[3] = 0.0362;
   fKnots_x_n[4] = 0.7;
   fKnots_y_n[4] = 0.0171;
   fKnots_x_n[5] = 1.0;
   fKnots_y_n[5] = 0.0;

   fh_Twist4_p_gr     = new TGraph(6,fKnots_x_p,fKnots_y_p);
   fh_Twist4_p_spline = new TSpline3("hTwist4p",fh_Twist4_p_gr,"",0,0);

   fh_Twist4_n_gr     = new TGraph(6,fKnots_x_n,fKnots_y_n);
   fh_Twist4_n_spline = new TSpline3("hTwist4n",fh_Twist4_n_gr,"",0,0);

   for(Int_t i=0;i<12;i++){ 
      fPDFErrors[i] = 0.; 
      fPDFValues[i] = 0.0;
   }
   fQ20 = 1.0;
}
//______________________________________________________________________________
JAMPolarizedPDFs::~JAMPolarizedPDFs(){

}
//______________________________________________________________________________
double *JAMPolarizedPDFs::GetPDFs(double x,double Qsq){

   fXbjorken = x;
   fQsquared = Qsq;
   double xuplus = xDeltaf_model(x,fPars_uplus);
   double xdplus = xDeltaf_model(x,fPars_dplus);
   double xg     = xDeltaf_model(x,fPars_g);
   double xubar  = xDeltaf_model(x,fPars_ubar);
   double xdbar  = xDeltaf_model(x,fPars_dbar);
   double xsbar  = xDeltaf_model(x,fPars_sbar);

   fPDFValues[0] = (xuplus - xubar)/x; //Delta up
   fPDFValues[1] = (xdplus - xdbar)/x; //Delta down
   fPDFValues[2] = xsbar/x;      //Delta s
   fPDFValues[6] = xg/x;       //Delta g
   fPDFValues[7] = xubar/x;     //Delta ubar
   fPDFValues[8] = xdbar/x;     //Delta dbar
   fPDFValues[9] = xsbar/x;      //Delta sbar

   return(fPDFValues);

}
//______________________________________________________________________________
double *JAMPolarizedPDFs::GetPDFErrors(double x,double Qsq){

   fXbjorken    = x;
   fQsquared    = Qsq;

   return(fPDFErrors);

}
//______________________________________________________________________________
}}
