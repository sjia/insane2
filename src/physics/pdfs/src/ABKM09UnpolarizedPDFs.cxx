#include "ABKM09UnpolarizedPDFs.h"
namespace insane {
namespace physics {
//______________________________________________________________________________
ABKM09UnpolarizedPDFs::ABKM09UnpolarizedPDFs(int Nf,int par,int order){

   fN_f   = Nf;    // Number of flavors; 5 => up, down, strange, bottom, charm 
   fPar   = par;   // no errors calculated  
   fOrder = order; // NLO (1) or NNLO (2)  

}
//______________________________________________________________________________
ABKM09UnpolarizedPDFs::~ABKM09UnpolarizedPDFs(){

}
//______________________________________________________________________________
double *ABKM09UnpolarizedPDFs::GetPDFs(double x,double Q2){

   fXbjorken = x;
   fQsquared = Q2;

   double xbj = x; 
   double qsq = Q2; 
   double res=0,res_v=0,res_sea=0; 

   for(int i=0;i<13;i++) fPDFValues[i] = 0; 

   // code for iparton:
   // 0 => alpha_s 
   // 1 => valence up
   // 2 => valence down 
   // 3 => gluons 
   // 4 => sea u-quarks 
   // 5 => s-quarks 
   // 6 => sea d-quarks 
   // 7 => c-quarks
   // 8 => b-quarks

   /// up (valence) 
   int i = 1; 
   getabkm09_(&i,&xbj,&qsq,&fN_f,&fPar,&fOrder,&res_v);
   /// up (sea) 
   i = 4; 
   getabkm09_(&i,&xbj,&qsq,&fN_f,&fPar,&fOrder,&res_sea);
   /// up 
   res = res_v + res_sea; 
   fPDFValues[0] = res;
   /// u-bar
   fPDFValues[7] = res_sea; 
   /// down (valence) 
   i = 2; 
   getabkm09_(&i,&xbj,&qsq,&fN_f,&fPar,&fOrder,&res_v); 
   /// down (sea) 
   i = 6; 
   getabkm09_(&i,&xbj,&qsq,&fN_f,&fPar,&fOrder,&res_sea); 
   /// down 
   res = res_v + res_sea; 
   fPDFValues[1] = res;
   /// d-bar 
   fPDFValues[8] = res_sea; 
   /// gluon 
   i = 3; 
   getabkm09_(&i,&xbj,&qsq,&fN_f,&fPar,&fOrder,&res);
   fPDFValues[6] = res;
   /// strange 
   i = 5; 
   getabkm09_(&i,&xbj,&qsq,&fN_f,&fPar,&fOrder,&res); 
   fPDFValues[2] = res;
   /// charm 
   i = 7; 
   getabkm09_(&i,&xbj,&qsq,&fN_f,&fPar,&fOrder,&res); 
   fPDFValues[3] = res;
   /// bottom 
   i = 8; 
   getabkm09_(&i,&xbj,&qsq,&fN_f,&fPar,&fOrder,&res); 
   fPDFValues[4] = res;

   return fPDFValues; 

}
}}
