#include "AAC08PolarizedPDFs.h"

namespace insane {
  namespace physics {

    //_____________________________________________________________________________
    AAC08PolarizedPDFs::AAC08PolarizedPDFs(){
      SetNameTitle("AAC08PolarizedPDFs","AAC08 pol. PDFs");
      SetLabel("AAC08");
      SetLineColor(4);
      SetLineStyle(8);
      for (int i = 0; i < 11; i++) gradientcol[0] = &gradient[i][0];
      fGRAD = &gradientcol[0];
    }
    //_____________________________________________________________________________
    AAC08PolarizedPDFs::~AAC08PolarizedPDFs() {
    }
    //_____________________________________________________________________________

    double * AAC08PolarizedPDFs::GetPDFs(double x, double Qsq) {
      fXbjorken = x;
      fQsquared = Qsq;
      int iset = 1;
      aac08pdf_(&fQsquared, &fXbjorken, &iset, fXPDF, fGRAD);
      fPDFValues[0] = fXPDF[4]/x;//Delta up
      fPDFValues[1] = fXPDF[5]/x;//Delta down
      fPDFValues[2] = fXPDF[6]/x;//Delta s
      fPDFValues[3] = 0.0;//Delta c
      fPDFValues[4] = 0.0;//Delta b
      fPDFValues[5] = 0.0;//Delta t
      fPDFValues[6] = fXPDF[3]/x;//Delta g
      fPDFValues[7] = fXPDF[2]/x;//Delta ubar
      fPDFValues[8] = fXPDF[1]/x;//Delta dbar
      fPDFValues[9] = fXPDF[0]/x;//Delta sbar
      fPDFValues[10] = 0.0;//Delta cbar
      fPDFValues[11] = 0.0;//Delta bbar
      fPDFValues[12] = 0.0;//Delta tbar
      return(fPDFValues);
    }
    //______________________________________________________________________________
    
    
  }
}

