#include "CJ12UnpolarizedPDFs.h"

// //______________________________________________________________________________
namespace insane {
namespace physics {
// CJ12UnpolarizedPDFs::CJ12UnpolarizedPDFs(){
//    SetNameTitle("CJ12UnpolarizedPDFs","CJ12 PDFs");
//    int iset          = 200; // mid, central PDF values 
//    setcj_(&iset);
//    fLabel            = "CJ12"; 
//    fDefaultLineColor = kMagenta-2;  
// }
//______________________________________________________________________________
CJ12UnpolarizedPDFs::CJ12UnpolarizedPDFs(int iset) : 
   fiSet(iset){
   SetNameTitle("CJ12UnpolarizedPDFs","CJ12 PDFs");
   // iset = 200      => mid, central PDF values 
   //        201--238 => mid, central PDF errors 
   setcj_(&iset);
   SetLabel("CJ12");
   SetLineColor(kMagenta-2);
}
//______________________________________________________________________________
CJ12UnpolarizedPDFs::~CJ12UnpolarizedPDFs(){


}
//______________________________________________________________________________
double *CJ12UnpolarizedPDFs::GetPDFs(double x,double Q2){

   /** Implementation of pure virtual method.
    *
    *  The function cjpdf_(Iparton, X, Q)
    *  returns the parton distribution inside the proton for parton [Iparton]
    *  at [X] Bjorken_X and scale [Q] (GeV) in PDF set [Iset].
    *  Iparton  is the parton label (5, 4, 3, 2, 1, 0, -1, ......, -5)
    *                           for (b, c, s, d, u, g, u_bar, ..., b_bar),
    */

    fXbjorken  = x; 
    fQsquared  = Q2; 
    int ipart; 
    auto ix  = (double)x; 
    double iQ  = TMath::Sqrt(Q2); 
    double res = 0.; 

    // up
    ipart = 1;              
    res   = cjpdf_(&ipart,&ix,&iQ); 
    fPDFValues[0] = res; 

    // down
    ipart = 2;              
    res   = cjpdf_(&ipart,&ix,&iQ); 
    fPDFValues[1] = res;

    // strange
    ipart = 3;              
    res   = cjpdf_(&ipart,&ix,&iQ); 
    fPDFValues[2] = res; 

    // charm 
    ipart = 4;             
    res   = cjpdf_(&ipart,&ix,&iQ); 
    fPDFValues[3] = res;

    // bottom
    ipart = 5;              
    res   = cjpdf_(&ipart,&ix,&iQ); 
    fPDFValues[4] = res; 

    // top
    fPDFValues[5] = 0;   

    // gluon  
    ipart = 0;             
    res   = cjpdf_(&ipart,&ix,&iQ); 
    fPDFValues[6] = res;  

    // up-bar      
    ipart = -1;             
    res   = cjpdf_(&ipart,&ix,&iQ); 
    fPDFValues[7] = res;

    // down-bar 
    ipart = -2;             
    res   = cjpdf_(&ipart,&ix,&iQ); 
    fPDFValues[8] = res;

    // strange-bar
    ipart = -3;             
    res   = cjpdf_(&ipart,&ix,&iQ); 
    fPDFValues[9] = res;

    // charm-bar
    ipart = -4;             
    res   = cjpdf_(&ipart,&ix,&iQ); 
    fPDFValues[10] = res; 
    
    // bottom-bar 
    ipart = -5;            
    res   = cjpdf_(&ipart,&ix,&iQ); 
    fPDFValues[11] = res;
    
    // top-bar
    fPDFValues[12] = 0.;   

    return fPDFValues; 

}
//______________________________________________________________________________
double *CJ12UnpolarizedPDFs::GetPDFErrors(double x,double Q2){

   // Takes a long time!
   int iset           = 0;
   const int set_base = 200;
   const double T     = TMath::Sqrt(100); 
   // quark errors 
   const int FN    = 13;
   double dq_1[FN] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
   double dq_2[FN] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
   double sum[FN]  = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
   double *MyPDFs; 

   MyPDFs = GetPDFs(x,Q2); 
   
   const int N = 19;
   for(int i=1;i<=N;i++){
      iset = set_base + 2*i - 1;
      setcj_(&iset);
      // Fit(x,Q2);
      MyPDFs = GetPDFs(x,Q2); 
      dq_1[0]  = MyPDFs[0];   // up         quark    
      dq_1[1]  = MyPDFs[1];   // down       quark 
      dq_1[2]  = MyPDFs[2];   // str        quark 
      dq_1[3]  = MyPDFs[3];   // charm      quark 
      dq_1[4]  = MyPDFs[4];   // bottom     quark 
      dq_1[5]  = MyPDFs[5];   // top        quark 
      dq_1[6]  = MyPDFs[6];   // gluon 
      dq_1[7]  = MyPDFs[7];   // up-bar     quark  
      dq_1[8]  = MyPDFs[8];   // down-bar   quark  
      dq_1[9]  = MyPDFs[9];   // str-bar    quark  
      dq_1[10] = MyPDFs[10];  // charm-bar  quark  
      dq_1[11] = MyPDFs[11];  // bottom-bar quark  
      dq_1[12] = MyPDFs[12];  // top-bar    quark  
      iset = set_base + 2*i;
      setcj_(&iset);
      MyPDFs = GetPDFs(x,Q2);
      dq_2[0]  = MyPDFs[0];   // up         quark    
      dq_2[1]  = MyPDFs[1];   // down       quark 
      dq_2[2]  = MyPDFs[2];   // str        quark 
      dq_2[3]  = MyPDFs[3];   // charm      quark 
      dq_2[4]  = MyPDFs[4];   // bottom     quark 
      dq_2[5]  = MyPDFs[5];   // top        quark 
      dq_2[6]  = MyPDFs[6];   // gluon 
      dq_2[7]  = MyPDFs[7];   // up-bar     quark  
      dq_2[8]  = MyPDFs[8];   // down-bar   quark  
      dq_2[9]  = MyPDFs[9];   // str-bar    quark  
      dq_2[10] = MyPDFs[10];  // charm-bar  quark  
      dq_2[11] = MyPDFs[11];  // bottom-bar quark  
      dq_2[12] = MyPDFs[12];  // top-bar    quark  
      // build sums of differences 
      sum[0]  += TMath::Power(dq_1[0]-dq_2[0],2.);
      sum[1]  += TMath::Power(dq_1[1]-dq_2[1],2.);
      sum[2]  += TMath::Power(dq_1[2]-dq_2[2],2.);
      sum[3]  += TMath::Power(dq_1[3]-dq_2[3],2.);
      sum[4]  += TMath::Power(dq_1[3]-dq_2[4],2.);
      sum[5]  += TMath::Power(dq_1[3]-dq_2[5],2.);
      sum[6]  += TMath::Power(dq_1[6]-dq_2[6],2.);
      sum[7]  += TMath::Power(dq_1[7]-dq_2[7],2.);
      sum[8]  += TMath::Power(dq_1[8]-dq_2[8],2.);
      sum[9]  += TMath::Power(dq_1[9]-dq_2[9],2.);
      sum[10] += TMath::Power(dq_1[10]-dq_2[10],2.);
      sum[11] += TMath::Power(dq_1[11]-dq_2[11],2.);
      sum[12] += TMath::Power(dq_1[12]-dq_2[12],2.);
   }
   // put it together 
   for(int i=0;i<FN;i++){
      fPDFErrors[i] = (T/2.)*TMath::Sqrt(sum[i]);
   }

   // Return to original state
   setcj_(&fiSet);
   GetPDFs(x,Q2);

   return fPDFErrors;

}

}}
