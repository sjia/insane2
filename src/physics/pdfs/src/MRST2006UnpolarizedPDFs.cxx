#include "MRST2006UnpolarizedPDFs.h"
namespace insane {
namespace physics {
//______________________________________________________________________________
MRST2006UnpolarizedPDFs::MRST2006UnpolarizedPDFs(int iset){
   SetLabel("MRST2006"); 
   SetLineColor(kGreen+2);
   SetLineStyle(7);
   // Initialize data grid 
   // iset = 0 => central values (default) 
   fPDFSet = iset; 
   // quark distributions 
   fuv=0;  fubar=0; 
   fdv=0;  fdbar=0; 
   fstr=0; fglu=0;
   for(Int_t i=0;i<13;i++) fPDFValues[i] = 0.;
   for(Int_t i=0;i<13;i++) fPDFErrors[i] = 0.;
}
//______________________________________________________________________________
MRST2006UnpolarizedPDFs::~MRST2006UnpolarizedPDFs(){
   //	delete fK; 
}
//______________________________________________________________________________
double *MRST2006UnpolarizedPDFs::GetPDFs(double x,double Q2){
   Fit(x,Q2);
   return fPDFValues; 
}
//______________________________________________________________________________
double *MRST2006UnpolarizedPDFs::GetPDFErrors(double x,double Q2){

   int iset = 0; 
   // quark errors 
   const int FN  = 13;  
   double dq_1[FN] = {0.,0.,0.,0.,0.,0.,0.,   // error 1 (lo?) 
                      0.,0.,0.,0.,0.,0.};
   double dq_2[FN] = {0.,0.,0.,0.,0.,0.,0.,   // error 2 (hi?) 
                      0.,0.,0.,0.,0.,0.};
   double sum[FN]  = {0.,0.,0.,0.,0.,0.,0.,   // sum of the difference of dq_1 and dq_2 squared 
                      0.,0.,0.,0.,0.,0.};
   // use the error tables from MRST2006
   const int N = 15;
   for(int i=1;i<=N;i++){
      iset = 2*i-1; 
      fPDFSet = iset;   
      Fit(x,Q2); 
      dq_1[0] = fPDFValues[0];   // up       quark    
      dq_1[1] = fPDFValues[1];   // down     quark 
      dq_1[2] = fPDFValues[2];   // str      quark 
      dq_1[3] = fPDFValues[3];   // charm    quark 
      dq_1[4] = fPDFValues[4];   // bottom   quark 
      dq_1[6] = fPDFValues[6];   // gluon 
      dq_1[7] = fPDFValues[7];   // up-bar   quark  
      dq_1[8] = fPDFValues[8];   // down-bar quark  
      dq_1[9] = fPDFValues[9];   // str-bar  quark  
      iset  = 2*i; 
      fPDFSet = iset;   
      Fit(x,Q2); 
      dq_2[0] = fPDFValues[0];   // up       quark    
      dq_2[1] = fPDFValues[1];   // down     quark 
      dq_2[2] = fPDFValues[2];   // str      quark 
      dq_2[3] = fPDFValues[3];   // charm    quark 
      dq_2[4] = fPDFValues[4];   // bottom   quark 
      dq_2[6] = fPDFValues[6];   // gluon 
      dq_2[7] = fPDFValues[7];   // up-bar   quark  
      dq_2[8] = fPDFValues[8];   // down-bar quark  
      dq_2[9] = fPDFValues[9];   // str-bar  quark  
      // build sums of differences 
      sum[0] += TMath::Power(dq_1[0]-dq_2[0],2.);  
      sum[1] += TMath::Power(dq_1[1]-dq_2[1],2.);  
      sum[2] += TMath::Power(dq_1[2]-dq_2[2],2.);  
      sum[3] += TMath::Power(dq_1[3]-dq_2[3],2.);  
      sum[4] += TMath::Power(dq_1[4]-dq_2[4],2.);  
      sum[6] += TMath::Power(dq_1[6]-dq_2[6],2.);  
      sum[7] += TMath::Power(dq_1[7]-dq_2[7],2.);  
      sum[8] += TMath::Power(dq_1[8]-dq_2[8],2.);  
      sum[9] += TMath::Power(dq_1[9]-dq_2[9],2.);  
   }
   // put it together 
   for(int i=0;i<FN;i++){
      fPDFErrors[i] = 0.5*TMath::Sqrt(sum[i]);  
   }
  
   return fPDFErrors;  

}
//______________________________________________________________________________
void MRST2006UnpolarizedPDFs::Fit(double x,double Q2){
   fXbjorken = x;
   fQsquared = Q2;
   // Calculate all quark distributions here 
   // WARNING: dssvfit returns x*f(x), so we divide by x! 

   // std::cout << "[MRST2006]: x = " << x << "\t" << "Q2 = " << Q2 << std::endl;

   // if Q2 < 1.0, we do a linear interpolation... 
   // k-1 point is Q2 = 1.1 
   // k point is Q2 = 1.0 

   double Q2_km1 = 1.1; 
   double Q2_k   = 1.0;
   auto ix     = (double)x; 
   auto Qsq    = (double)Q2; 
   double Q      = TMath::Sqrt(Qsq); 
   double Q_k    = TMath::Sqrt(Q2_k); 
   double Q_km1  = TMath::Sqrt(Q2_km1); 
 
   double uv_km1,dv_km1,ubar_km1,dbar_km1,str_km1,sbar_km1,chm_km1,bot_km1,glu_km1; 
   double uv_k,dv_k,ubar_k,dbar_k,str_k,sbar_k,chm_k,bot_k,glu_k; 

   fuv   = 0.; 
   fdv   = 0.; 
   fubar = 0.; 
   fdbar = 0.; 
   fstr  = 0.; 
   fsbar = 0.; 
   fchm  = 0.; 
   fbot  = 0.; 
   fglu  = 0.; 

   if(x<1.){
      if(Q2<1.){
         mrstpdfs_(&fPDFSet,&ix,&Q_k,&uv_k,&dv_k,&ubar_k,&dbar_k,&str_k,&sbar_k,&chm_k,&bot_k,&glu_k);
         mrstpdfs_(&fPDFSet,&ix,&Q_km1,&uv_km1,&dv_km1,&ubar_km1,&dbar_km1,&str_km1,&sbar_km1,&chm_km1,&bot_km1,&glu_km1);
         fuv   = Extrapolate(Q2,Q2_km1,uv_km1  ,Q2_k,uv_k  ); 
         fdv   = Extrapolate(Q2,Q2_km1,dv_km1  ,Q2_k,dv_k  ); 
         fubar = Extrapolate(Q2,Q2_km1,ubar_km1,Q2_k,ubar_k); 
         fdbar = Extrapolate(Q2,Q2_km1,dbar_km1,Q2_k,dbar_k); 
         fstr  = Extrapolate(Q2,Q2_km1,str_km1 ,Q2_k,str_k ); 
         fsbar = Extrapolate(Q2,Q2_km1,sbar_km1,Q2_k,sbar_k); 
         fchm  = Extrapolate(Q2,Q2_km1,chm_km1 ,Q2_k,chm_k ); 
         fbot  = Extrapolate(Q2,Q2_km1,bot_km1 ,Q2_k,bot_k ); 
         fglu  = Extrapolate(Q2,Q2_km1,glu_km1 ,Q2_k,glu_k ); 
      }else{
         mrstpdfs_(&fPDFSet,&ix,&Q,&fuv,&fdv,&fubar,&fdbar,&fstr,&fsbar,&fchm,&fbot,&fglu);
      }
   }

   // mrstpdfs_(&fPDFSet,&x,&Q,&fuv,&fdv,&fubar,&fdbar,&fstr,&fsbar,&fchm,&fbot,&fglu);
 
   if(x>0){
      fuv   *= 1./x; 
      fubar *= 1./x; 
      fdv   *= 1./x; 
      fdbar *= 1./x; 
      fstr  *= 1./x; 
      fsbar *= 1./x; 
      fchm  *= 1./x; 
      fbot  *= 1./x; 
      fglu  *= 1./x; 
   }

   for(int i=0;i<13;i++) fPDFValues[i] = 0; 

   /// Convert q_v to q  
   //  q_v = q - qbar => q = q_v + qbar  
   fPDFValues[0]  = (fuv + fubar); 
   fPDFValues[1]  = (fdv + fdbar);
   fPDFValues[2]  = fstr;
   fPDFValues[3]  = fchm; 
   fPDFValues[4]  = fbot; 
   fPDFValues[6]  = fglu; 
   fPDFValues[7]  = fubar; 
   fPDFValues[8]  = fdbar; 
   fPDFValues[9]  = fsbar; 

}
//______________________________________________________________________________
double MRST2006UnpolarizedPDFs::Extrapolate(double x,double x_km1,double f_km1,double x_k,double f_k){
   // extrapolation to point x, using the k-1 and k points preceeding x 
   double f = f_km1 + ( (x-x_km1)/(x_k - x_km1) )*(f_k - f_km1);
   return f;  
}

}}
