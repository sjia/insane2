#include "DSSVPolarizedPDFs.h"

#include "Physics.h"

namespace insane {
namespace physics {
//______________________________________________________________________________
DSSVPolarizedPDFs::DSSVPolarizedPDFs(int iset) : fiSet(iset) {
   
   SetNameTitle("DSSVPolarizedPDFs","DSSV pol. PDFs");
   SetLabel("DSSV"); 
   SetLineColor(kGreen+2);

   // Initialize data grid 
   // iset = 0 => central values (default) 
   //      = +/- (1--19) error tables  
   dssvini_(&fiSet);
   // quark distributions 
   for(Int_t i=0;i<13;i++) fPDFValues[i] = 0.;
   for(Int_t i=0;i<13;i++) fPDFErrors[i] = 0.;
}
//______________________________________________________________________________
DSSVPolarizedPDFs::~DSSVPolarizedPDFs(){
   //	delete fK; 
}
//______________________________________________________________________________
double *DSSVPolarizedPDFs::GetPDFs(double x,double Q2){
   // int iset = 0; 
   // dssvini_(&iset); 
   fXbjorken = x;
   fQsquared = Q2;
   // Calculate all quark distributions here 
   // WARNING: dssvfit returns x*f(x), so we divide by x! 
   // std::cout << "[DSSV]: " << x << std::endl;

   // if Q2 < 0.8, we do a linear interpolation... 
   // k-1 point is Q2 = 1.0 
   // k point is Q2 = 0.80 
   //
   
   double fduv   = 0.; 
   double fddv   = 0.; 
   double fdubar = 0.; 
   double fddbar = 0.; 
   double fdstr  = 0.; 
   double fdglu  = 0.; 

   using quark      = insane::physics::Parton;
   const auto& q_id = to_underlying<quark>;

   if(Q2<1.0){

      double Q2_km1 = 1.2; 
      double Q2_k   = 1.00; 
      double duv_km1,ddv_km1,dubar_km1,ddbar_km1,dstr_km1,dglu_km1; 
      double duv_k,ddv_k,dubar_k,ddbar_k,dstr_k,dglu_k; 


      // Not sure why this extrapolation is needed.
      dssvfit_(&x , &Q2_km1 , &duv_km1 , &ddv_km1 , &dubar_km1 , &ddbar_km1 , &dstr_km1 , &dglu_km1);
      dssvfit_(&x , &Q2_k   , &duv_k   , &ddv_k   , &dubar_k   , &ddbar_k   , &dstr_k   , &dglu_k);
      fduv   = Extrapolate(Q2,Q2_km1,duv_km1  ,Q2_k,duv_k  ); 
      fddv   = Extrapolate(Q2,Q2_km1,ddv_km1  ,Q2_k,ddv_k  ); 
      fdubar = Extrapolate(Q2,Q2_km1,dubar_km1,Q2_k,dubar_k); 
      fddbar = Extrapolate(Q2,Q2_km1,ddbar_km1,Q2_k,ddbar_k); 
      fdstr  = Extrapolate(Q2,Q2_km1,dstr_km1 ,Q2_k,dstr_k ); 
      fdglu  = Extrapolate(Q2,Q2_km1,dglu_km1 ,Q2_k,dglu_k ); 

   } else {

      dssvfit_(&x,&Q2,&fduv,&fddv,&fdubar,&fddbar,&fdstr,&fdglu); 

   }

   // NOTE: In the class PolarizedPDFs, the labeling in the array fPDFValues is: 
   //  \f$ (0,1,2,3,4,5,6,7,8,9,10,11) = (u,d,s,c,b,t,g,\bar{u},\bar{d},\bar{s},\bar{c},\bar{b},\bar{t}) \f$

   if( x>0.0 ) {
      fduv   *= 1.0/x; 
      fdubar *= 1.0/x; 
      fddv   *= 1.0/x; 
      fddbar *= 1.0/x; 
      fdstr  *= 1.0/x; 
      fdglu  *= 1.0/x; 
   }

   for(int i=0;i<13;i++) {
     fPDFValues[i] = 0; 
   }

   // Convert q_v to q  
   //  q_v = q - qbar => q = q_v + qbar  
   fPDFValues[0] = (fduv + fdubar); 
   fPDFValues[1] = (fddv + fddbar);
   fPDFValues[2] = fdstr;
   fPDFValues[6] = fdglu; 
   fPDFValues[7] = fdubar; 
   fPDFValues[8] = fddbar; 
   fPDFValues[9] = fdstr; 
   fPDFValues[3] = 0.0;//Delta c
   fPDFValues[4] = 0.0;//Delta b
   fPDFValues[5] = 0.0;//Delta t
   fPDFValues[10] = 0.0;//Delta cbar
   fPDFValues[11] = 0.0;//Delta bbar
   fPDFValues[12] = 0.0;//Delta tbar

   //Fit(x,Q2);
   return fPDFValues; 
}
//______________________________________________________________________________
double *DSSVPolarizedPDFs::GetPDFErrors(double x,double Q2){

   int iset = 0; 
   // quark errors 
   const int FN  = 13;  
   double dq_1[FN] = {0.,0.,0.,0.,0.,0.,0.,   // error 1 (lo?) 
                      0.,0.,0.,0.,0.,0.};
   double dq_2[FN] = {0.,0.,0.,0.,0.,0.,0.,   // error 2 (hi?) 
                      0.,0.,0.,0.,0.,0.};
   double sum[FN]  = {0.,0.,0.,0.,0.,0.,0.,   // sum of the difference of dq_1 and dq_2 squared 
                      0.,0.,0.,0.,0.,0.};
   // use the error tables from DSSV
   using quark = insane::physics::Parton;
   const auto& q_id = to_underlying<quark>;
   const int N = 19;
   for(int i=1;i<=N;i++){
      iset = i; 
      dssvini_(&iset); 
      GetPDFs(x,Q2); 
      dq_1[0] = fPDFValues[q_id(quark::u)]  - fPDFValues[q_id(quark::ubar)];//fduv;//fPDFValues[0];  // up       quark    
      dq_1[1] = fPDFValues[q_id(quark::d)]  - fPDFValues[q_id(quark::dbar)];//fddv;//fPDFValues[1];  // down     quark 
      dq_1[2] = fPDFValues[2];  // str      quark 
      dq_1[6] = fPDFValues[6];  // gluon 
      dq_1[7] = fPDFValues[7];  // up-bar   quark  
      dq_1[8] = fPDFValues[8];  // down-bar quark  
      dq_1[9] = fPDFValues[9];  // str-bar  quark  
      iset  = (-1)*iset; 
      dssvini_(&iset);
      GetPDFs(x,Q2); 
      dq_2[0] = fPDFValues[q_id(quark::u)]  - fPDFValues[q_id(quark::ubar)];//fduv;//fPDFValues[0];   // up       quark    
      dq_2[1] = fPDFValues[q_id(quark::d)]  - fPDFValues[q_id(quark::dbar)];//fddv;//fPDFValues[1];   // down     quark 
      dq_2[2] = fPDFValues[2];   // str      quark 
      dq_2[6] = fPDFValues[6];   // gluon 
      dq_2[7] = fPDFValues[7];   // up-bar   quark  
      dq_2[8] = fPDFValues[8];   // down-bar quark  
      dq_2[9] = fPDFValues[9];   // str-bar  quark  
      // build sums of differences 
      sum[0] += TMath::Power(dq_1[0]-dq_2[0],2.);  
      sum[1] += TMath::Power(dq_1[1]-dq_2[1],2.);  
      sum[2] += TMath::Power(dq_1[2]-dq_2[2],2.);  
      sum[6] += TMath::Power(dq_1[6]-dq_2[6],2.);  
      sum[7] += TMath::Power(dq_1[7]-dq_2[7],2.);  
      sum[8] += TMath::Power(dq_1[8]-dq_2[8],2.);  
      sum[9] += TMath::Power(dq_1[9]-dq_2[9],2.);  
   }
   // put it together 
   for(int i=0;i<FN;i++){
      fPDFErrors[i] = 0.5*TMath::Sqrt(sum[i]);  
   }

   // Compute the q = qv+qbbar errors
   fPDFErrors[0] = TMath::Sqrt(fPDFErrors[0]*fPDFErrors[0]+fPDFErrors[7]*fPDFErrors[7]);
   fPDFErrors[1] = TMath::Sqrt(fPDFErrors[1]*fPDFErrors[1]+fPDFErrors[8]*fPDFErrors[8]);

   // Compute the original pdfs again.
   dssvini_(&fiSet);
   GetPDFs(x,Q2); 
  
   return fPDFErrors;  

}
//______________________________________________________________________________
//void DSSVPolarizedPDFs::Fit(double x,double Q2){
//}
//______________________________________________________________________________
double DSSVPolarizedPDFs::Extrapolate(double x,double x_km1,double f_km1,double x_k,double f_k){
   // extrapolation to point x, using the k-1 and k points preceeding x 
   double f = f_km1 + ( (x-x_km1)/(x_k - x_km1) )*(f_k - f_km1);
   return f;  
}
//______________________________________________________________________________
}
}

