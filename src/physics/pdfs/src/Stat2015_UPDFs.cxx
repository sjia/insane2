#include "Stat2015_UPDFs.h"
#include <iostream>

namespace insane {
  namespace physics {

    Stat2015_UPDFs::Stat2015_UPDFs(){

      SetNameTitle("Stat2015_UPDFs","Stat2015 pol. PDFs");
      SetLabel("Stat2015");
      SetLineColor(2);
      SetLineStyle(1);

      fiPol    = 1;
      fiSingle = 0;
      fiNum    = 0;

      // These values are always zero.
      fValues[4] = 0.0;//Delta b
      fValues[5] = 0.0;//Delta t
      fValues[11] = 0.0;//Delta bbar
      fValues[12] = 0.0;//Delta tbar

      for(Int_t i=0;i<12;i++) fUncertainties[i] = 0.; 
    }
    //______________________________________________________________________________
    Stat2015_UPDFs::~Stat2015_UPDFs(){

    }
    //______________________________________________________________________________
    const std::array<double,NPartons>& Stat2015_UPDFs::Calculate    (double x, double Q2) const
    {

      fXbjorken = x;
      fQsquared = Q2;

      //* PDF NOTATION FOR UNUnpolarized AND Unpolarized PDF
      //*  -6   -5   -4   -3   -2   -1   0    1  2  3  4  5  6
      //* TBAR BBAR CBAR SBAR DBAR UBAR GLUON U  D  S  C  B  T
      //   0     1    2   3    4    5     6   7  8  9  10 11 12
      partonevol_(&fXbjorken, &fQsquared,&fiPol,pdfs,&fiSingle,&fiNum);

      fValues[0]  = pdfs[7]/x;   // up
      fValues[1]  = pdfs[8]/x;   // down
      fValues[2]  = pdfs[9]/x;   // s
      fValues[3]  = pdfs[10]/x;  // c
      fValues[6]  = pdfs[6]/x;   // g
      fValues[7]  = pdfs[5]/x;   // ubar
      fValues[8]  = pdfs[4]/x;   // dbar
      fValues[9]  = pdfs[3]/x;   // sbar
      fValues[10] = pdfs[2]/x;   // cbar

      fUncertainties[0] = 0.0;
      fUncertainties[1] = 0.0;
      fUncertainties[2] = 0.0;
      fUncertainties[3] = 0.0;
      fUncertainties[6] = 0.0;
      fUncertainties[7] = 0.0;
      fUncertainties[8] = 0.0;
      fUncertainties[9] = 0.0;
      fUncertainties[10] = 0.0;

      //std::cout << " u check: " << Get(kUP) << " vs " << pdfs[7]/x << std::endl;;
      return(fValues);

    }
    //______________________________________________________________________________

    const std::array<double,NPartons>& Stat2015_UPDFs::Uncertainties(double x, double Q2) const
    {

      fUncertainties[0] = 0.0;
      fUncertainties[1] = 0.0;
      fUncertainties[2] = 0.0;
      fUncertainties[3] = 0.0;
      fUncertainties[6] = 0.0;
      fUncertainties[7] = 0.0;
      fUncertainties[8] = 0.0;
      fUncertainties[9] = 0.0;
      fUncertainties[10] = 0.0;

      return(fUncertainties);

    }
    //______________________________________________________________________________

  }
}
