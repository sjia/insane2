#include "BBSQuarkHelicityDistributions.h"

namespace insane {
namespace physics {
//______________________________________________________________________________
BBSQuarkHelicityDistributions::BBSQuarkHelicityDistributions(){

   // FIXME: 1. Is there any Q2 dependence? (There doesn't seem to be)
   //        2. What about anti-quarks?

   /// coefficients from Eq 3.11--3.14, 3.16 
   fAu      =  3.784;
   fAd      =  0.757;
   fBu      = -3.672; 
   fBd      = -0.645; 
   fCu      =  2.004; 
   fCd      =  3.230; 
   fDu      = -1.892;
   fDd      = -3.118; 
   fCs      =  1.0;       /// bounds: 0.7067 < Cs < 1.2013 (they choose Cs = 1) 
   fAs      = -0.6980 + 0.9877*fCs; 
   fBs      =  0.8534 - 1.1171*fCs;  
   fDs      =  0.1551 - 1.1294*fCs;
   fAlpha   =  1.12;     /// QCD Pomeron intercept  
   fAlpha_g = 1.0;  
   fAg      = 0.2381;                   
   fBg      = 1.1739; 
   // fAlpha_g =  1.12;  
   // fAg      =  2.000;                   
   // fBg      = -1.250; 

}
//______________________________________________________________________________
BBSQuarkHelicityDistributions::~BBSQuarkHelicityDistributions(){

}
//______________________________________________________________________________
double BBSQuarkHelicityDistributions::uPlus(double x){
   double arg = fAu*TMath::Power(1.-x,3.) + fBu*TMath::Power(1.-x,4.); 
   double res = arg/( TMath::Power(x,fAlpha) ); 
   return res; 
}
//______________________________________________________________________________
double BBSQuarkHelicityDistributions::uMinus(double x){
   double arg = fCu*TMath::Power(1.-x,5.) + fDu*TMath::Power(1.-x,6.); 
   double res = arg/( TMath::Power(x,fAlpha) ); 
   return res; 
}
//______________________________________________________________________________
double BBSQuarkHelicityDistributions::dPlus(double x){
   double arg = fAd*TMath::Power(1.-x,3.) + fBd*TMath::Power(1.-x,4.); 
   double res = arg/( TMath::Power(x,fAlpha) ); 
   return res; 
}
//______________________________________________________________________________
double BBSQuarkHelicityDistributions::dMinus(double x){
   double arg = fCd*TMath::Power(1.-x,5.) + fDd*TMath::Power(1.-x,6.); 
   double res = arg/( TMath::Power(x,fAlpha) ); 
   return res; 
}
//______________________________________________________________________________
double BBSQuarkHelicityDistributions::sPlus(double x){
   double arg = fAs*TMath::Power(1.-x,5.) + fBs*TMath::Power(1.-x,6.); 
   double res = arg/( TMath::Power(x,fAlpha) ); 
   return res; 
}
//______________________________________________________________________________
double BBSQuarkHelicityDistributions::sMinus(double x){
   double arg = fCs*TMath::Power(1.-x,7.) + fDs*TMath::Power(1.-x,8.); 
   double res = arg/( TMath::Power(x,fAlpha) ); 
   return res; 
}
//______________________________________________________________________________
double BBSQuarkHelicityDistributions::gPlus(double x){
   double arg = fAg*TMath::Power(1.-x,4.) + fBg*TMath::Power(1.-x,5.); 
   double res = arg/( TMath::Power(x,fAlpha_g) ); 
   return res; 
}
//______________________________________________________________________________
double BBSQuarkHelicityDistributions::gMinus(double x){
   double arg = fAg*TMath::Power(1.-x,6.) + fBg*TMath::Power(1.-x,7.); 
   double res = arg/( TMath::Power(x,fAlpha_g) ); 
   return res; 
}
//______________________________________________________________________________
Int_t BBSQuarkHelicityDistributions::CalculateDistributions(double x, double Q2){
   
   fU.fQ_Plus     = uPlus(x);
   fU.fQ_Minus    = uMinus(x);
   fU.fQbar_Plus  = 0.0;
   fU.fQbar_Minus = 0.0;

   fD.fQ_Plus     = dPlus(x);
   fD.fQ_Minus    = dMinus(x);
   fD.fQbar_Plus  = 0.0;
   fD.fQbar_Minus = 0.0;

   fS.fQ_Plus     = sPlus(x);
   fS.fQ_Minus    = sMinus(x);
   fS.fQbar_Plus  = 0.0;
   fS.fQbar_Minus = 0.0;

   fG.fQ_Plus     = gPlus(x);
   fG.fQ_Minus    = gMinus(x);
   fG.fQbar_Plus  = 0.0;
   fG.fQbar_Minus = 0.0;
   
   return 0;
}
//______________________________________________________________________________
}}
