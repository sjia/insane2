#include "BBPolarizedPDFs.h"

namespace insane {
namespace physics {
BBPolarizedPDFs::BBPolarizedPDFs()
{
   SetNameTitle("BBPolarizedPDFs","BB pol. PDFs");
   SetLabel("BB");
   SetLineColor(6);
   SetLineStyle(7);
   fiSet = 1;
   // These values are always zero.
   fPDFValues[3] = 0.0;//Delta c
   fPDFValues[4] = 0.0;//Delta b
   fPDFValues[5] = 0.0;//Delta t
   fPDFValues[10] = 0.0;//Delta cbar
   fPDFValues[11] = 0.0;//Delta bbar
   fPDFValues[12] = 0.0;//Delta tbar

   for(Int_t i=0;i<12;i++) fPDFErrors[i] = 0.; 
}
//______________________________________________________________________________

BBPolarizedPDFs::~BBPolarizedPDFs()
{ }
//______________________________________________________________________________

double *BBPolarizedPDFs::GetPDFs(double x,double Qsq)
{

   fXbjorken = x;
   fQsquared = Qsq;
   double qbar = 0.0;
   double GL = 0.0;
   double UV = 0.0;
   double DV = 0.0;
   double Dqbar = 0.0;
   ppdf_(&fiSet, &fXbjorken, &fQsquared,
         /*UV=*/&UV,/*DUV=*/&fPDFErrors[0],
         /*DV=*/&DV,/*DDV=*/&fPDFErrors[1],
         /*GL=*/&GL,/*DGL=*/&fPDFErrors[6],
         /*QB=*/&qbar,/*DQB=*/&Dqbar,
         &fg1p, &fDg1p, &fg1n, &fDg1n);

   fPDFValues[0] = (UV + qbar)/x; //Delta up
   fPDFValues[1] = (DV + qbar)/x; //Delta down
   fPDFValues[2] = qbar/x;      //Delta s
   fPDFValues[6] = GL/x;       //Delta g
   fPDFValues[7] = qbar/x;     //Delta ubar
   fPDFValues[8] = qbar/x;     //Delta dbar
   fPDFValues[9] = qbar/x;     //Delta sbar

   fPDFErrors[0] = TMath::Sqrt(fPDFErrors[0]*fPDFErrors[0]+Dqbar*Dqbar)/x; 
   fPDFErrors[1] = TMath::Sqrt(fPDFErrors[1]*fPDFErrors[1]+Dqbar*Dqbar)/x; 
   fPDFErrors[2] = Dqbar/x; 
   fPDFErrors[6] = fPDFErrors[6]/x; 
   fPDFErrors[7] = Dqbar/x; 
   fPDFErrors[8] = Dqbar/x; 
   fPDFErrors[9] = Dqbar/x; 

   return(fPDFValues);

}
//______________________________________________________________________________
double *BBPolarizedPDFs::GetPDFErrors(double x,double Qsq){

   fXbjorken    = x;
   fQsquared    = Qsq;
   double qbar  = 0.0;
   double GL    = 0.0;
   double UV    = 0.0;
   double DV    = 0.0;
   double Dqbar = 0.0;
   ppdf_(&fiSet, &fXbjorken, &fQsquared,
         /*UV=*/&UV,/*DUV=*/&fPDFErrors[0],
         /*DV=*/&DV,/*DDV=*/&fPDFErrors[1],
         /*GL=*/&GL,/*DGL=*/&fPDFErrors[6],
         /*QB=*/&qbar,/*DQB=*/&Dqbar,
         &fg1p, &fDg1p, &fg1n, &fDg1n);
   // fPDFValues[0] = (UV + qbar)/x; //Delta up
   // fPDFValues[1] = (DV + qbar)/x; //Delta down
   // fPDFValues[2] = 0.0;      //Delta s
   // fPDFValues[6] = GL/x;       //Delta g
   // fPDFValues[7] = qbar/x;     //Delta ubar
   // fPDFValues[8] = qbar/x;     //Delta dbar
   // fPDFValues[9] = 0.0;      //Delta sbar
   fPDFErrors[0] = TMath::Sqrt(fPDFErrors[0]*fPDFErrors[0]+Dqbar*Dqbar)/x; 
   fPDFErrors[1] = TMath::Sqrt(fPDFErrors[1]*fPDFErrors[1]+Dqbar*Dqbar)/x; 
   fPDFErrors[2] = Dqbar/x; 
   fPDFErrors[6] = fPDFErrors[6]/x; 
   fPDFErrors[7] = Dqbar/x; 
   fPDFErrors[8] = Dqbar/x; 
   fPDFErrors[9] = Dqbar/x; 


   return(fPDFErrors);

}
}}
