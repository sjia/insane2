#include "Twist4DistributionFunctions.h" 

namespace insane {
  namespace physics {

    Twist4DistributionFunctions::Twist4DistributionFunctions()
    { }
    //______________________________________________________________________________

    Twist4DistributionFunctions::~Twist4DistributionFunctions()
    { }
    //______________________________________________________________________________

    double Twist4DistributionFunctions::g1(double x, double Q2, Nuclei n) const
    {
      switch(n) {
        case Nuclei::p : 
          return g1p_Twist4(x, Q2);
          break;
        case Nuclei::n : 
          return g1n_Twist4(x, Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double Twist4DistributionFunctions::g2(double x, double Q2, Nuclei n) const
    {
      switch(n) {
        case Nuclei::p : 
          return g2p_Twist4(x, Q2);
          break;
        case Nuclei::n : 
          return g2n_Twist4(x, Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double Twist4DistributionFunctions::g1_TMC(double x, double Q2, Nuclei n) const
    {
      switch(n) {
        case Nuclei::p : 
          return g1p_Twist4_TMC(x, Q2);
          break;
        case Nuclei::n : 
          return g1n_Twist4_TMC(x, Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________

    double Twist4DistributionFunctions::g2_TMC(double x, double Q2, Nuclei n) const
    {
      switch(n) {
        case Nuclei::p : 
          return g2p_Twist4_TMC(x, Q2);
          break;
        case Nuclei::n : 
          return g2n_Twist4_TMC(x, Q2);
          break;
      }
      return 0.0;
    }
    //______________________________________________________________________________


    double Twist4DistributionFunctions::g2p_Twist4(double x, double Q2) const
    { 
      double result          = Dp_Twist4(x, Q2);
      double integral_result = insane::integrate::gaus(
          [&](double z){
          return( Dp_Twist4(z,Q2)/z );
          }, x,1.0);
      return( result - integral_result );
    }
    //______________________________________________________________________________
    
    double Twist4DistributionFunctions::g1p_Twist4_TMC(double x, double Q2) const
    {
      // Twist three part of g1p with full TMC treatment
      double xi        = insane::Kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::Kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M      = (insane::units::M_p/insane::units::GeV);
      double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho    = TMath::Sqrt(1.0+gamma2);
      double res    = (rho*rho-1.0)/(rho*rho*rho)*Dp_Twist4(xi,Q2);
      double t1     = (1.0-rho*rho)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::gaus(
          [&](double z){
          return( ((3.0-(3.0-rho*rho)*TMath::Log(z/xi)/rho)/z)*Dp_Twist4(z,Q2) );
          }, xi,xi_thresh);
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________
    
    double Twist4DistributionFunctions::g2p_Twist4_TMC(double x, double Q2) const
    {
      // Twist three part of g1p with full TMC treatment
      double xi        = insane::Kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::Kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M      = (insane::units::M_p/insane::units::GeV);
      double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho    = TMath::Sqrt(1.0+gamma2);
      double res    = 1.0/(rho*rho*rho)*Dp_Twist4(xi,Q2);
      double t1     = (-1.0)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::gaus(
          [&](double z){
          return(((3.0-2.0*rho*rho+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/rho)/z)*Dp_Twist4(z,Q2) );
          }, xi,xi_thresh);
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________

    double Twist4DistributionFunctions::g2n_Twist4(double x, double Q2) const
    { 
      double result          = Dn_Twist4(x, Q2);
      double integral_result = insane::integrate::gaus(
          [&](double z){
          return( Dn_Twist4(z,Q2)/z );
          }, x,1.0);
      return( result - integral_result );
    }
    //______________________________________________________________________________
    
    double Twist4DistributionFunctions::g1n_Twist4_TMC(double x, double Q2) const
    {
      // Twist three part of g1n with full TMC treatment
      double xi        = insane::Kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::Kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M      = (insane::units::M_p/insane::units::GeV);
      double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho    = TMath::Sqrt(1.0+gamma2);
      double res    = (rho*rho-1.0)/(rho*rho*rho)*Dn_Twist4(xi,Q2);
      double t1     = (1.0-rho*rho)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::gaus(
          [&](double z){
          return( ((3.0-(3.0-rho*rho)*TMath::Log(z/xi)/rho)/z)*Dn_Twist4(z,Q2) );
          }, xi,xi_thresh);
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________

    double Twist4DistributionFunctions::g2n_Twist4_TMC(double x, double Q2) const
    {
      // Twist three part of g1n with full TMC treatment
      double xi        = insane::Kine::xi_Nachtmann(x  , Q2);
      double xi_thresh = insane::Kine::xi_Nachtmann(1.0, Q2);
      if(xi>xi_thresh) {return 0.0;}
      double M      = (insane::units::M_p/insane::units::GeV);
      double gamma2 = TMath::Power(2.0*M*x,2.0)/Q2;
      double rho    = TMath::Sqrt(1.0+gamma2);
      double res    = 1.0/(rho*rho*rho)*Dn_Twist4(xi,Q2);
      double t1     = (-1.0)/(rho*rho*rho*rho);
      double integral_result =  insane::integrate::gaus(
          [&](double z){
          return(((3.0-2.0*rho*rho+3.0*(rho*rho-1.0)*TMath::Log(z/xi)/rho)/z)*Dn_Twist4(z,Q2) );
          }, xi,xi_thresh);
      return( res + t1*integral_result );
    }
    //______________________________________________________________________________

    double Twist4DistributionFunctions::Dp_Twist4(double x, double Q2) const
    { 
      double result = 0.0;
      Calculate(x, Q2);
      result += (4.0 / 9.0) * Get(PartonFlavor::kUP)/x;
      result += (1.0 / 9.0) * Get(PartonFlavor::kDOWN)/x;
      //result += 0.5 * (1.0 / 9.0) * Deltas();
      //result += 0.5 * (4.0 / 9.0) * Deltaubar();
      //result += 0.5 * (1.0 / 9.0) * Deltadbar();
      //result += 0.5 * (1.0 / 9.0) * Deltasbar();
      //std::cout << "Dp_Twist4 " <<  result << '\n';
      return(result);
    }
    //______________________________________________________________________________

    double Twist4DistributionFunctions::Dn_Twist4(double x, double Q2) const
    { 
      double result = 0.0;
      Calculate(x, Q2);
      result += (1.0 / 9.0) * Get(PartonFlavor::kUP)/x;
      result += (4.0 / 9.0) * Get(PartonFlavor::kDOWN)/x;
      //result += 0.5 * (1.0 / 9.0) * Deltas();
      //result += 0.5 * (4.0 / 9.0) * Deltaubar();
      //result += 0.5 * (1.0 / 9.0) * Deltadbar();
      //result += 0.5 * (1.0 / 9.0) * Deltasbar();
      //std::cout << "Dp_Twist4 " <<  result << '\n';
      return(result);
    }
    //______________________________________________________________________________

    // Twist 3 quark distribution functions
    double Twist4DistributionFunctions::D_u(   double x, double Q2) const
    {return Get(PartonFlavor::kUP,x,Q2);}
    //______________________________________________________________________________

    double Twist4DistributionFunctions::D_d(   double x, double Q2) const
    {return Get(PartonFlavor::kDOWN,x,Q2);}
    //______________________________________________________________________________

    double Twist4DistributionFunctions::D_s(   double x, double Q2) const
    {return Get(PartonFlavor::kSTRANGE,x,Q2);}
    //______________________________________________________________________________

    double Twist4DistributionFunctions::D_ubar(double x, double Q2) const
    {return Get(PartonFlavor::kANTIUP,x,Q2);}
    //______________________________________________________________________________

    double Twist4DistributionFunctions::D_dbar(double x, double Q2) const
    {return Get(PartonFlavor::kANTIDOWN,x,Q2);}
    //______________________________________________________________________________

    double Twist4DistributionFunctions::D_sbar(double x, double Q2) const
    {return Get(PartonFlavor::kANTISTRANGE,x,Q2);}
    //______________________________________________________________________________


  }
}

