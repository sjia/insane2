#include "MSTW08UnpolarizedPDFs.h"
namespace insane {
namespace physics {
//______________________________________________________________________________
MSTW08UnpolarizedPDFs::MSTW08UnpolarizedPDFs(int iset){
   fPDFSet = iset; 
}
//______________________________________________________________________________
MSTW08UnpolarizedPDFs::~MSTW08UnpolarizedPDFs(){

}
//______________________________________________________________________________
double *MSTW08UnpolarizedPDFs::GetPDFs(double x,double Q2){
   Fit(x,Q2); 
   return fPDFValues; 
}
//______________________________________________________________________________
double *MSTW08UnpolarizedPDFs::GetPDFErrors(double x,double Q2){

   int iset = 0;
   // quark errors 
   const int FN  = 13;
   double dq_1[FN] = {0.,0.,0.,0.,0.,0.,0.,   // error 1 (lo?) 
                      0.,0.,0.,0.,0.,0.};
   double dq_2[FN] = {0.,0.,0.,0.,0.,0.,0.,   // error 2 (hi?) 
                      0.,0.,0.,0.,0.,0.};
   double sum[FN]  = {0.,0.,0.,0.,0.,0.,0.,   // sum of the difference of dq_1 and dq_2 squared 
                      0.,0.,0.,0.,0.,0.};
   // use the error tables from MSTW08
   const int N = 20;
   for(int i=1;i<=N;i++){
      iset = 2*i-1;
      fPDFSet = iset;
      Fit(x,Q2);
      dq_1[0] = fPDFValues[0];   // up       quark    
      dq_1[1] = fPDFValues[1];   // down     quark 
      dq_1[2] = fPDFValues[2];   // str      quark 
      dq_1[3] = fPDFValues[3];   // charm    quark 
      dq_1[4] = fPDFValues[4];   // bottom   quark 
      dq_1[6] = fPDFValues[6];   // gluon 
      dq_1[7] = fPDFValues[7];   // up-bar   quark  
      dq_1[8] = fPDFValues[8];   // down-bar quark  
      dq_1[9] = fPDFValues[9];   // str-bar  quark  
      iset  = 2*i;
      fPDFSet = iset;
      Fit(x,Q2);
      dq_2[0] = fPDFValues[0];   // up       quark    
      dq_2[1] = fPDFValues[1];   // down     quark 
      dq_2[2] = fPDFValues[2];   // str      quark 
      dq_2[3] = fPDFValues[3];   // charm    quark 
      dq_2[4] = fPDFValues[4];   // bottom   quark 
      dq_2[6] = fPDFValues[6];   // gluon 
      dq_2[7] = fPDFValues[7];   // up-bar   quark  
      dq_2[8] = fPDFValues[8];   // down-bar quark  
      dq_2[9] = fPDFValues[9];   // str-bar  quark  
      // build sums of differences 
      sum[0] += TMath::Power(dq_1[0]-dq_2[0],2.);
      sum[1] += TMath::Power(dq_1[1]-dq_2[1],2.);
      sum[2] += TMath::Power(dq_1[2]-dq_2[2],2.);
      sum[3] += TMath::Power(dq_1[3]-dq_2[3],2.);
      sum[4] += TMath::Power(dq_1[4]-dq_2[4],2.);
      sum[6] += TMath::Power(dq_1[6]-dq_2[6],2.);
      sum[7] += TMath::Power(dq_1[7]-dq_2[7],2.);
      sum[8] += TMath::Power(dq_1[8]-dq_2[8],2.);
      sum[9] += TMath::Power(dq_1[9]-dq_2[9],2.);
   }
   // put it together 
   for(int i=0;i<FN;i++){
      fPDFErrors[i] = 0.5*TMath::Sqrt(sum[i]);
   }

   return fPDFErrors;
}
//______________________________________________________________________________
void MSTW08UnpolarizedPDFs::Fit(double x,double Q2){

   fXbjorken = x; 
   fQsquared = Q2; 

   double res,res_v; 
   double xbj = x;
   double q   = TMath::Sqrt(Q2); 

   for(int i=0;i<13;i++) fPDFValues[i] = 0.;

   // parton code 
   // 1 => down 
   // 2 => up 
   // 3 => strange 
   // 4 => charm 
   // 5 => bottom 
   // 0 => gluon 
   // 7 => down (valence) 
   // 8 => up (valence) 
   // 9 => strange (valence) 
   // 10 => charm (valence) 
   // 11 => bottom (valence) 

   /// up 
   int i = 2; 
   getmstw08_(&fPDFSet,&i,&xbj,&q,&res); 
   fPDFValues[0] = res;  
   /// down 
   i = 1; 
   getmstw08_(&fPDFSet,&i,&xbj,&q,&res); 
   fPDFValues[1] = res; 
   /// strange 
   i = 3; 
   getmstw08_(&fPDFSet,&i,&xbj,&q,&res); 
   fPDFValues[2] = res; 
   /// charm 
   i = 4; 
   getmstw08_(&fPDFSet,&i,&xbj,&q,&res); 
   fPDFValues[3] = res; 
   /// bottom 
   i = 5; 
   getmstw08_(&fPDFSet,&i,&xbj,&q,&res); 
   fPDFValues[4] = res;
   /// top 
   fPDFValues[5] = 0.;  
   /// gluon 
   i = 0; 
   getmstw08_(&fPDFSet,&i,&xbj,&q,&res); 
   fPDFValues[6] = res;
   /// up-bar 
   i = 2; 
   getmstw08_(&fPDFSet,&i,&xbj,&q,&res); 
   i = 8; 
   getmstw08_(&fPDFSet,&i,&xbj,&q,&res_v); 
   fPDFValues[7] = res - res_v;
   /// down-bar 
   i = 1; 
   getmstw08_(&fPDFSet,&i,&xbj,&q,&res); 
   i = 7; 
   getmstw08_(&fPDFSet,&i,&xbj,&q,&res_v); 
   fPDFValues[8] = res - res_v; 
   /// strange-bar 
   i = 3; 
   getmstw08_(&fPDFSet,&i,&xbj,&q,&res); 
   i = 9; 
   getmstw08_(&fPDFSet,&i,&xbj,&q,&res_v); 
   fPDFValues[9] = res - res_v; 
   /// charm-bar
   i = 4; 
   getmstw08_(&fPDFSet,&i,&xbj,&q,&res); 
   i = 10; 
   getmstw08_(&fPDFSet,&i,&xbj,&q,&res_v); 
   fPDFValues[10] = res - res_v; 
   /// bottom-bar
   i = 5; 
   getmstw08_(&fPDFSet,&i,&xbj,&q,&res);
   i = 11; 
   getmstw08_(&fPDFSet,&i,&xbj,&q,&res_v); 
   fPDFValues[11] = res - res_v;
   /// top-bar 
   fPDFValues[5] = 0.;  

}
}}
