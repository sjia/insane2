#include "StatisticalPolarizedPDFs.h"

namespace insane {
namespace physics {

//______________________________________________________________________________
StatisticalPolarizedPDFs::StatisticalPolarizedPDFs(){
   SetNameTitle("StatisticalPolarizedPDFs","Statistical Polarized PDFs");
   SetLabel("Statistical"); 
   SetLineColor(7);
   // quark distributions 
   fdu=0;  fdubar=0; 
   fdd=0;  fddbar=0; 
   fds=0;  fdsbar=0;
   fdglu=0;
   for(Int_t i=0;i<12;i++) fPDFErrors[i] = 0.;
   UseQ2Interpolation();
}

//______________________________________________________________________________
StatisticalPolarizedPDFs::~StatisticalPolarizedPDFs(){

}

//______________________________________________________________________________
void StatisticalPolarizedPDFs::FormPDFs(double x,double Q2){
   fXbjorken = x;
   fQsquared = Q2;
   double qplus,qminus;

   for(int i=0;i<13;i++) fPDFValues[i] = 0; 

   // If we use Soffer's Q2 interpolation... 
   Bool_t IsInterp = fStatisticalFits.IsInterpolated();

   if(IsInterp){
      fPDFValues[0]  = fStatisticalFits.GetDeltaUQuarkInterp(x,Q2);
      fPDFValues[1]  = fStatisticalFits.GetDeltaDQuarkInterp(x,Q2);
      fPDFValues[2]  = fStatisticalFits.GetDeltaSQuarkInterp(x,Q2);
      fPDFValues[3]  = fStatisticalFits.GetDeltaCQuarkInterp(x,Q2);
      fPDFValues[4]  = 0.;
      fPDFValues[5]  = 0.;
      fPDFValues[6]  = fStatisticalFits.GetDeltaGluonInterp(x,Q2);
      fPDFValues[7]  = fStatisticalFits.GetDeltaAntiUQuarkInterp(x,Q2);
      fPDFValues[8]  = fStatisticalFits.GetDeltaAntiDQuarkInterp(x,Q2);
      fPDFValues[9]  = fStatisticalFits.GetDeltaAntiSQuarkInterp(x,Q2);
      fPDFValues[10] = 0.;
      fPDFValues[11] = 0.;
      fPDFValues[12] = 0.;
   }else{
      // Calculate all quark distributions here 
      // delta up 
      qplus  = fStatisticalFits.GetUQuark(1,x,Q2);
      qminus = fStatisticalFits.GetUQuark(-1,x,Q2);
      fdu    = qplus-qminus;        
      // delta anti-up 
      qplus  = fStatisticalFits.GetAntiUQuark(1,x,Q2);
      qminus = fStatisticalFits.GetAntiUQuark(-1,x,Q2);
      fdubar = qplus-qminus;        
      // delta down 
      qplus  = fStatisticalFits.GetDQuark(1,x,Q2);
      qminus = fStatisticalFits.GetDQuark(-1,x,Q2);
      fdd    = qplus-qminus;        
      // delta anti-down
      qplus  = fStatisticalFits.GetAntiDQuark(1,x,Q2);
      qminus = fStatisticalFits.GetAntiDQuark(-1,x,Q2);
      fddbar = qplus-qminus;        
      // delta strange 
      qplus  = fStatisticalFits.GetSQuark(1,x,Q2);
      qminus = fStatisticalFits.GetSQuark(-1,x,Q2);
      fds    = qplus-qminus;        
      // delta anti-strange
      qplus  = fStatisticalFits.GetAntiSQuark(1,x,Q2);
      qminus = fStatisticalFits.GetAntiSQuark(-1,x,Q2);
      fdsbar = qplus-qminus;        
      // delta gluon 
      qplus  = fStatisticalFits.GetPolarizedGluon(1,x,Q2);
      qminus = fStatisticalFits.GetPolarizedGluon(-1,x,Q2);
      fdglu  = qplus-qminus;        
      // store results 
      fPDFValues[0] = fdu; 
      fPDFValues[1] = fdd;
      fPDFValues[2] = fds;
      fPDFValues[6] = fdglu; 
      fPDFValues[7] = fdubar; 
      fPDFValues[8] = fddbar; 
      fPDFValues[9] = fdsbar; 
   }


}
//______________________________________________________________________________
}}
