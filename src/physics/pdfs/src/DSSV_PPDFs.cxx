#include "DSSV_PPDFs.h"

namespace insane {
  namespace physics {
    
    DSSV_PPDFs::DSSV_PPDFs(){
      SetNameTitle("DSSV_PPDFs","DSSV pol. PDFs");
      SetLabel("DSSV");


      for(Int_t i=0;i<12;i++) {
        fUncertainties[i] = 0.; 
        fValues[i] = 0.0;
      }
    }
    
    DSSV_PPDFs::~DSSV_PPDFs()
    { }
    
    const std::array<double,NPartons>& DSSV_PPDFs::Calculate    (double x, double Q2) const
    {
      // These values are always zero.
      fValues[3] = 0.0;//Delta c
      fValues[4] = 0.0;//Delta b
      fValues[5] = 0.0;//Delta t
      fValues[10] = 0.0;//Delta cbar
      fValues[11] = 0.0;//Delta bbar
      fValues[12] = 0.0;//Delta tbar

      fXbjorken = x;
      fQsquared = Q2;

      auto vals = old_pdfs.GetPDFs(x,Q2);

      for(Int_t i=0;i<12;i++) {
        fValues[i] = vals[i]; 
      }
      // NOTE: In the class PolarizedPDFs, the labeling in the array fPDFValues is: 
      //  \f$ (0,1,2,3,4,5,6,7,8,9,10,11) = (u,d,s,c,b,t,g,\bar{u},\bar{d},\bar{s},\bar{c},\bar{b},\bar{t}) \f$

      return(fValues);

    }
    //______________________________________________________________________________

    const std::array<double,NPartons>& DSSV_PPDFs::Uncertainties(double x, double Q2) const
    {

      fUncertainties[0] = 0.0;
      fUncertainties[1] = 0.0;
      fUncertainties[2] = 0.0;
      fUncertainties[3] = 0.0;
      fUncertainties[6] = 0.0;
      fUncertainties[7] = 0.0;
      fUncertainties[8] = 0.0;
      fUncertainties[9] = 0.0;
      fUncertainties[10] = 0.0;

      return(fUncertainties);

    }
    //______________________________________________________________________________
  }
}

