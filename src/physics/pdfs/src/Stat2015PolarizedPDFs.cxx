#include "Stat2015PolarizedPDFs.h"

namespace insane {
namespace physics {
//______________________________________________________________________________
Stat2015PolarizedPDFs::Stat2015PolarizedPDFs(){

   SetNameTitle("Stat2015PolarizedPDFs","Stat2015 pol. PDFs");
   SetLabel("Stat2015");
   SetLineColor(4);
   SetLineStyle(1);

   fiPol    = 2;
   fiSingle = 0;
   fiNum    = 0;

   // These values are always zero.
   fPDFValues[4] = 0.0;//Delta b
   fPDFValues[5] = 0.0;//Delta t
   fPDFValues[11] = 0.0;//Delta bbar
   fPDFValues[12] = 0.0;//Delta tbar

   for(Int_t i=0;i<12;i++) fPDFErrors[i] = 0.; 
}
//______________________________________________________________________________
Stat2015PolarizedPDFs::~Stat2015PolarizedPDFs(){

}
//______________________________________________________________________________
double *Stat2015PolarizedPDFs::GetPDFs(double x,double Q2){

   fXbjorken = x;
   fQsquared = Q2;

//* PDF NOTATION FOR UNPOLARIZED AND POLARIZED PDF
//*  -6   -5   -4   -3   -2   -1   0    1  2  3  4  5  6
//* TBAR BBAR CBAR SBAR DBAR UBAR GLUON U  D  S  C  B  T
//   0     1    2   3    4    5     6   7  8  9  10 11 12
   partonevol_(&fXbjorken, &fQsquared,&fiPol,pdfs,&fiSingle,&fiNum);

   fPDFValues[0]  = pdfs[7]/x;   //Delta up
   fPDFValues[1]  = pdfs[8]/x;   //Delta down
   fPDFValues[2]  = pdfs[9]/x;   //Delta s
   fPDFValues[3]  = pdfs[10]/x;  //Delta c
   fPDFValues[6]  = pdfs[6]/x;   //Delta g
   fPDFValues[7]  = pdfs[5]/x;   //Delta ubar
   fPDFValues[8]  = pdfs[4]/x;   //Delta dbar
   fPDFValues[9]  = pdfs[3]/x;   //Delta sbar
   fPDFValues[10] = pdfs[2]/x;   //Delta cbar

   fPDFErrors[0] = 0.0;//TMath::Sqrt(fPDFErrors[0]*fPDFErrors[0]+Dqbar*Dqbar)/x; 
   fPDFErrors[1] = 0.0;//TMath::Sqrt(fPDFErrors[1]*fPDFErrors[1]+Dqbar*Dqbar)/x; 
   fPDFErrors[2] = 0.0;//Dqbar/x; 
   fPDFErrors[3] = 0.0;
   fPDFErrors[6] = 0.0;//fPDFErrors[6]/x; 
   fPDFErrors[7] = 0.0;//Dqbar/x; 
   fPDFErrors[8] = 0.0;//Dqbar/x; 
   fPDFErrors[9] = 0.0;//Dqbar/x; 
   fPDFErrors[10] = 0.0;

   return(fPDFValues);

}
//______________________________________________________________________________
double *Stat2015PolarizedPDFs::GetPDFErrors(double x,double Qsq){

   fPDFErrors[0] = 0.0;//TMath::Sqrt(fPDFErrors[0]*fPDFErrors[0]+Dqbar*Dqbar)/x; 
   fPDFErrors[1] = 0.0;//TMath::Sqrt(fPDFErrors[1]*fPDFErrors[1]+Dqbar*Dqbar)/x; 
   fPDFErrors[2] = 0.0;//Dqbar/x; 
   fPDFErrors[3] = 0.0;
   fPDFErrors[6] = 0.0;//fPDFErrors[6]/x; 
   fPDFErrors[7] = 0.0;//Dqbar/x; 
   fPDFErrors[8] = 0.0;//Dqbar/x; 
   fPDFErrors[9] = 0.0;//Dqbar/x; 
   fPDFErrors[10] = 0.0;

   return(fPDFErrors);

}
//______________________________________________________________________________
}}
