#include "MHKPolarizedPDFs.h"
//______________________________________________________________________________
namespace insane {
namespace physics {
MHKPolarizedPDFs::MHKPolarizedPDFs(){
   SetNameTitle("MHKPolarizedPDFs","MHK pol. PDFs");
   SetLabel("MHK");
   SetLineColor(2);
   SetLineStyle(1);

   fPars_uplus[0] =  0.928; fParsErr_uplus[0] = 0.01;
   fPars_uplus[1] =  0.558; fParsErr_uplus[1] = 0.012;
   fPars_uplus[2] =  3.46 ; fParsErr_uplus[2] = 0.006;
   fPars_uplus[3] =  8.848; fParsErr_uplus[3] = 0.01;
   fPars_uplus[4] =  0.0  ; fParsErr_uplus[4] = 0.0;

   fPars_dplus[0] = -0.342; fParsErr_dplus[0] = 0.010;
   fPars_dplus[1] =  0.250; fParsErr_dplus[1] = 0.033;
   fPars_dplus[2] =  3.912; fParsErr_dplus[2] = 0.116;
   fPars_dplus[3] = 14.162; fParsErr_dplus[3] = 0.01;
   fPars_dplus[4] =  0.0  ; fParsErr_dplus[4] = 0.0;

   // qbar =  ubar = dbar = sbar = s
   fPars_ubar[0] = -0.0605; fParsErr_ubar[0] = 0.006;
   fPars_ubar[1] =  0.567 ; fParsErr_ubar[1] = 0.009;
   fPars_ubar[2] =  4.993 ; fParsErr_ubar[2] = 0.01;
   fPars_ubar[3] =  0.0   ; fParsErr_ubar[3] = 0.01;
   fPars_ubar[4] =  0.0   ; fParsErr_ubar[4] = 0.01;

   fPars_g[0] =  0.201; fParsErr_g[0] = 0.044;
   fPars_g[1] =  2.253; fParsErr_g[1] = 0.01;
   fPars_g[2] =  3.082; fParsErr_g[2] = 0.01;
   fPars_g[3] =  0.0  ; fParsErr_g[3] = 0.01;
   fPars_g[4] =  0.0  ; fParsErr_g[4] = 0.0;

   // qbar =  ubar = dbar = sbar = s
   fPars_dbar[0] = -0.0605; fParsErr_dbar[0] = 0.006;
   fPars_dbar[1] =  0.567 ; fParsErr_dbar[1] = 0.009;
   fPars_dbar[2] =  4.993 ; fParsErr_dbar[2] = 0.01;
   fPars_dbar[3] =  0.0   ; fParsErr_dbar[3] = 0.01;
   fPars_dbar[4] =  0.0   ; fParsErr_dbar[4] = 0.01;

   // qbar =  ubar = dbar = sbar = s
   fPars_sbar[0] = -0.0605; fParsErr_sbar[0] = 0.006;
   fPars_sbar[1] =  0.567 ; fParsErr_sbar[1] = 0.009;
   fPars_sbar[2] =  4.993 ; fParsErr_sbar[2] = 0.01;
   fPars_sbar[3] =  0.0   ; fParsErr_sbar[3] = 0.01;
   fPars_sbar[4] =  0.0   ; fParsErr_sbar[4] = 0.01;

   fPars_Twist3_p[0] =  0.034;
   fPars_Twist3_p[1] =  0.554;
   fPars_Twist3_p[2] = -0.387;
   fPars_Twist3_p[3] = -1.17;
   fPars_Twist3_p[4] =  0.969;

   fPars_Twist3_n[0] =  0.067;
   fPars_Twist3_n[1] =  0.106;
   fPars_Twist3_n[2] = -0.448;
   fPars_Twist3_n[3] =  0.569;
   fPars_Twist3_n[4] = -0.098;

   ///\todo include deuteron

   // no twist 4 knots
   fKnots_x_p[0] = 0.0;
   fKnots_y_p[0] = 0.0;
   fKnots_x_p[1] = 0.1;
   fKnots_y_p[1] = 0.0118;
   fKnots_x_p[2] = 0.3;
   fKnots_y_p[2] =-0.0325;
   fKnots_x_p[3] = 0.5;
   fKnots_y_p[3] =-0.0271;
   fKnots_x_p[4] = 0.7;
   fKnots_y_p[4] =-0.0167;
   fKnots_x_p[5] = 1.0;
   fKnots_y_p[5] = 0.0;

   fKnots_x_n[0] = 0.0;
   fKnots_y_n[0] = 0.0;
   fKnots_x_n[1] = 0.1;
   fKnots_y_n[1] = 0.0079;
   fKnots_x_n[2] = 0.3;
   fKnots_y_n[2] = 0.0290;
   fKnots_x_n[3] = 0.5;
   fKnots_y_n[3] = 0.0362;
   fKnots_x_n[4] = 0.7;
   fKnots_y_n[4] = 0.0171;
   fKnots_x_n[5] = 1.0;
   fKnots_y_n[5] = 0.0;

   fh_Twist4_p_gr     = nullptr;//new TGraph(6,fKnots_x_p,fKnots_y_p);
   fh_Twist4_p_spline = nullptr;//new TSpline3("hTwist4p",fh_Twist4_p_gr,"",0,0);

   fh_Twist4_n_gr     = nullptr;//new TGraph(6,fKnots_x_n,fKnots_y_n);
   fh_Twist4_n_spline = nullptr;//new TSpline3("hTwist4n",fh_Twist4_n_gr,"",0,0);

   for(Int_t i=0;i<12;i++){ 
      fPDFErrors[i] = 0.; 
      fPDFValues[i] = 0.0;
   }
   fQ20 = 4.0;
}
//______________________________________________________________________________
MHKPolarizedPDFs::~MHKPolarizedPDFs(){

}
//______________________________________________________________________________
double *MHKPolarizedPDFs::GetPDFs(double x,double Qsq){

   fXbjorken = x;
   fQsquared = Qsq;
   double xuplus = xDeltaf_model(x,fPars_uplus);
   double xdplus = xDeltaf_model(x,fPars_dplus);
   double xg     = xDeltaf_model(x,fPars_g);
   double xubar  = xDeltaf_model(x,fPars_ubar);
   double xdbar  = xDeltaf_model(x,fPars_dbar);
   double xsbar  = xDeltaf_model(x,fPars_sbar);

   fPDFValues[0] = (xuplus - xubar)/x; //Delta up
   fPDFValues[1] = (xdplus - xdbar)/x; //Delta down
   fPDFValues[2] = xsbar/x;      //Delta s
   fPDFValues[6] = xg/x;       //Delta g
   fPDFValues[7] = xubar/x;     //Delta ubar
   fPDFValues[8] = xdbar/x;     //Delta dbar
   fPDFValues[9] = xsbar/x;      //Delta sbar

   return(fPDFValues);

}
//______________________________________________________________________________
double *MHKPolarizedPDFs::GetPDFErrors(double x,double Qsq){

   fXbjorken    = x;
   fQsquared    = Qsq;

   return(fPDFErrors);

}
//______________________________________________________________________________
}}
