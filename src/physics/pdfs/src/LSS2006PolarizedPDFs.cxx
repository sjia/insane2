#include "LSS2006PolarizedPDFs.h"

namespace insane {
namespace physics {

//______________________________________________________________________________
LSS2006PolarizedPDFs::LSS2006PolarizedPDFs(){
   SetNameTitle("LSS2006PolarizedPDFs","LSS2006 pol. PDFs");
   SetLabel("LSS2006");
   SetLineColor(5);
   SetLineStyle(3);
   lss2006init_();
   fiSet = 3;
   for(Int_t i=0;i<12;i++) fPDFErrors[i] = 0.;
}
//______________________________________________________________________________
LSS2006PolarizedPDFs::~LSS2006PolarizedPDFs(){

}
//______________________________________________________________________________
double *LSS2006PolarizedPDFs::GetPDFs(double x,double Qsq){

   fXbjorken = x;
   fQsquared = Qsq;
   auto FISET       = (int)fiSet; 
   auto xBjorken = (double)x; 
   auto Q2       = (double)Qsq;
   double UUB, DDB, SSB, GL, UV, DV, UB, DB, ST, g1pLT, g1p, g1nLT, g1n;

   double Q2_km1 = 1.0;
   double Q2_k   = 1.1;
   double UUB_km1,DDB_km1,SSB_km1,GL_km1,UV_km1,DV_km1,UB_km1,DB_km1,ST_km1,g1pLT_km1,g1p_km1,g1nLT_km1,g1n_km1;
   double UUB_k,DDB_k,SSB_k,GL_k,UV_k,DV_k,UB_k,DB_k,ST_k,g1pLT_k,g1p_k,g1nLT_k,g1n_k;

   if(Q2<1.0){
      lss2006_(&FISET,&xBjorken,&Q2_km1,&UUB_km1,&DDB_km1,&SSB_km1,&GL_km1,&UV_km1,&DV_km1,&UB_km1,&DB_km1,&ST_km1,&g1pLT_km1,&g1p_km1,&g1nLT_km1,&g1n_km1);
      lss2006_(&FISET,&xBjorken,&Q2_k  ,&UUB_k  ,&DDB_k  ,&SSB_k  ,&GL_k  ,&UV_k  ,&DV_k  ,&UB_k  ,&DB_k  ,&ST_k  ,&g1pLT_k  ,&g1p_k  ,&g1nLT_k  ,&g1n_k);
      UUB    = Extrapolate(Q2,Q2_km1,UUB_km1,Q2_k,UUB_k); 
      UB     = Extrapolate(Q2,Q2_km1,UB_km1,Q2_k,UB_k); 
      DDB    = Extrapolate(Q2,Q2_km1,DDB_km1,Q2_k,DDB_k); 
      DB     = Extrapolate(Q2,Q2_km1,DB_km1,Q2_k,DB_k); 
      ST     = Extrapolate(Q2,Q2_km1,ST_km1,Q2_k,ST_k); 
      GL     = Extrapolate(Q2,Q2_km1,GL_km1,Q2_k,GL_k); 
   }else{
      lss2006_(&FISET, &xBjorken, &Q2,
            &UUB, &DDB, &SSB, &GL, &UV, &DV, &UB, &DB, &ST, &g1pLT, &g1p, &g1nLT, &g1n);
   }

   fPDFValues[0] = (UUB - UB)/x; //Delta up
   fPDFValues[1] = (DDB - DB)/x; //Delta down
   fPDFValues[2] = ST/x;//Delta s
   fPDFValues[3] = 0.0;//Delta c
   fPDFValues[4] = 0.0;//Delta b
   fPDFValues[5] = 0.0;//Delta t
   fPDFValues[6] = GL/x;//Delta g
   fPDFValues[7] = UB/x;//Delta ubar
   fPDFValues[8] = DB/x;//Delta dbar
   fPDFValues[9] = 0.0;//Delta sbar
   fPDFValues[10] = 0.0;//Delta cbar
   fPDFValues[11] = 0.0;//Delta bbar
   fPDFValues[12] = 0.0;//Delta tbar
   return(fPDFValues);

}
//______________________________________________________________________________
double LSS2006PolarizedPDFs::Extrapolate(double x,double x_km1,double f_km1,double x_k,double f_k){
   // extrapolation to point x, using the k-1 and k points preceeding x 
   double f = f_km1 + ( (x-x_km1)/(x_k - x_km1) )*(f_k - f_km1);
   return f;
}

}}
