#include "DNS2005PolarizedPDFs.h"


namespace insane {
namespace physics {
//______________________________________________________________________________
DNS2005PolarizedPDFs::DNS2005PolarizedPDFs(){
   SetNameTitle("DNS2005PolarizedPDFs","DNS2005 polarized PDFs");
   SetLabel("DNS2005");
   SetLineColor(1);
   SetLineStyle(2);
   fiSet = 3;
   ini_();
   for(Int_t i=0;i<12;i++) fPDFErrors[i] = 0.;

}
//______________________________________________________________________________
DNS2005PolarizedPDFs::~DNS2005PolarizedPDFs(){

}
//______________________________________________________________________________
double *DNS2005PolarizedPDFs::GetPDFs(double x,double Qsq){

   fXbjorken = x;
   fQsquared = Qsq;
   //      double qbar=0.0;
   //      double Dqbar=0.0;
   polfit_(&fiSet, &fXbjorken, &fQsquared,
         /*DUV=*/&fPDFValues[0],/*DDV=*/&fPDFValues[1],
         /*DUBAR=*/&fPDFValues[7],/*DDBAR=*/&fPDFValues[8],
         /*DSTR=*/&fPDFValues[2],/*DGLU=*/&fPDFValues[6],
         &fg1p, &fg1n);

   fPDFValues[0] = fPDFValues[0]/x;//Delta up
   fPDFValues[1] = fPDFValues[1]/x;//Delta down
   fPDFValues[2] = fPDFValues[2]/x;//Delta s
   fPDFValues[3] = 0.0;//Delta c
   fPDFValues[4] = 0.0;//Delta b
   fPDFValues[5] = 0.0;//Delta t
   fPDFValues[6] = fPDFValues[6]/x;//Delta g
   fPDFValues[7] = fPDFValues[7]/x;//Delta ubar
   fPDFValues[8] = fPDFValues[8]/x;//Delta dbar
   fPDFValues[9] = 0.0;//Delta sbar
   fPDFValues[10] = 0.0;//Delta cbar
   fPDFValues[11] = 0.0;//Delta bbar
   fPDFValues[12] = 0.0;//Delta tbar
   return(fPDFValues);
}
}}
