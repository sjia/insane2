#include "LSS98UnpolarizedPDFs.h"


namespace insane {
namespace physics {
//______________________________________________________________________________
LSS98UnpolarizedPDFs::LSS98UnpolarizedPDFs(){
   SetNameTitle("LSS98UnpolarizedPDFs","LSS98 PDFs");
   SetLabel("LSS98");
   SetLineColor(kGreen+1);
}
//______________________________________________________________________________
LSS98UnpolarizedPDFs::~LSS98UnpolarizedPDFs(){

}
//______________________________________________________________________________
double *LSS98UnpolarizedPDFs::GetPDFs(double x,double Q2){

   double up = fqhd.uPlus(x); 
   double um = fqhd.uMinus(x); 
   double dp = fqhd.dPlus(x); 
   double dm = fqhd.dMinus(x); 
   double sp = fqhd.sPlus(x); 
   double sm = fqhd.sMinus(x); 
   double gp = fqhd.gPlus(x); 
   double gm = fqhd.gMinus(x); 

   double u = up + um; 
   double d = dp + dm; 
   double s = sp + sm; 
   double g = gp + gm; 

   for(int i=0;i<13;i++) fPDFValues[i] = 0;

   fPDFValues[0] = u;
   fPDFValues[1] = d;
   fPDFValues[2] = s;
   fPDFValues[6] = g;

   return fPDFValues;  

}
}}
