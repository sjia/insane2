#include "JAM_T3DFs.h"
#include "FortranWrappers.h"

namespace insane {
  namespace physics {

    JAM_T3DFs::JAM_T3DFs()
    {
      char * lib_string  = const_cast<char*>("JAM15        ");
      char * dist_string = const_cast<char*>("PPDF         ");
      char * path_string = const_cast<char*>("             ");
      int    ipos = 0;
      // lib (character*10): library (JAM15,JAM16,etc.)
      // dist (character*10): distribution type (PPDF,FFpion,FFkaon,etc.)
      // ipos (integer): posterior number (0 to 199) from MC analysis
      grid_init_( path_string, lib_string, dist_string, &ipos );
      Reset();
    }
    //______________________________________________________________________________

    JAM_T3DFs::~JAM_T3DFs()
    { }
    //______________________________________________________________________________

    const std::array<double,NPartons>& JAM_T3DFs::Calculate(double x, double Q2) const
    {
      fXbjorken      = x;
      fQsquared      = Q2;
      double  t3_val = 0.0;
      char *  flav;

      flav =  const_cast<char*>("u3");
      jam_xf_(&t3_val, &fXbjorken, &fQsquared, flav );
      fValues[PartonFlavor::kUP] = t3_val;

      flav =  const_cast<char*>("d3");
      jam_xf_(&t3_val, &fXbjorken, &fQsquared, flav );
      fValues[PartonFlavor::kDOWN] = t3_val;

      //fxDd_Twist3 = t3d;
      // do calculations
      return fValues;
    }
    //______________________________________________________________________________

    const std::array<double,NPartons>& JAM_T3DFs::Uncertainties(double x, double Q2) const 
    {
      // do calculations
      return fUncertainties;
    }


  }
}
