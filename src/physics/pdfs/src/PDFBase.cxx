#include "PDFBase.h"

namespace insane {
  namespace physics {

    PDFBase2::PDFBase2()
    { }
    //__________________________________________________________________________

    PDFBase2::~PDFBase2()
    { }
    //__________________________________________________________________________

    void PDFBase2::Reset()
    {
      for(int i = 0; i<NPartons ; i++) {
        fValues[i]        = 0.0;
        fUncertainties[i] = 0.0;
      }
      fXbjorken = -1.0;
      fQsquared = 0.0;
    }
    //______________________________________________________________________________

    bool PDFBase2::IsComputed(double x, double Q2) const
    {
      //if( x  != fXbjorken ) return false;
      //if( Q2 != fQsquared ) return false;
      //return true;
      return false;
    }
    //______________________________________________________________________________

    double  PDFBase2::Get(const PartonFlavor& f, double x, double Q2) const 
    {
      if( !IsComputed(x, Q2) ) {
        Calculate(x,Q2);
        Uncertainties(x,Q2);
      }
      return( fValues[int(f)] );
    }
    //______________________________________________________________________________
    
    double  PDFBase2::Get(Parton f, double x, double Q2) const 
    {
      if( !IsComputed(x, Q2) ) {
        Calculate(x,Q2);
        Uncertainties(x,Q2);
      }
      return fValues[static_cast<unsigned int>(f)];
    }
    //______________________________________________________________________________
    
    double  PDFBase2::Get(const PartonFlavor& f) const 
    {
      return( fValues[int(f)] );
    }
    //______________________________________________________________________________
    
    double  PDFBase2::Get(Parton f) const 
    {
      return fValues[static_cast<unsigned int>(f)];
    }
    //______________________________________________________________________________
    
    const std::array<double,NPartons>& PDFBase2::Calculate(double x, double Q2) const
    {
      // do calculations
      return fValues;
    }
    //______________________________________________________________________________

    const std::array<double,NPartons>& PDFBase2::Uncertainties(double x, double Q2) const 
    {
      // do calculations
      return fUncertainties;
    }

  }
}


