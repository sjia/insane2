#include "LHAPDFPolarizedPDFs.h"


namespace insane {
namespace physics {
//______________________________________________________________________________
LHAPDFPolarizedPDFs::LHAPDFPolarizedPDFs() {
   fSubset = 0;
   SetLabel("NNPDFpol10_100");
   SetLineColor(1);
   LHAPDF::initPDFSet(GetLabel(), LHAPDF::LHGRID, fSubset);
   //       const double Q = 10.0, mz = 91.2;
   //       const int NUMBER = LHAPDF::numberPDF();
   LHAPDF::initPDF(0);

   for(Int_t i=0;i<12;i++) fPDFErrors[i] = 0.; 

}
//______________________________________________________________________________
LHAPDFPolarizedPDFs::~LHAPDFPolarizedPDFs() {
}
void LHAPDFPolarizedPDFs::SetPDFType(const char * pdfset, Int_t type) {
   SetLabel(pdfset);
   if (!type) SetPDFDataSet(GetLabel(), LHAPDF::LHGRID, fSubset);
   else SetPDFDataSet(GetLabel(), LHAPDF::EVOLVE, fSubset);
}
//______________________________________________________________________________
void LHAPDFPolarizedPDFs::SetPDFDataSet(const char * pdfset, LHAPDF::SetType type , Int_t subset) {
   fSubset = subset;
   //     int SUBSET = subset;
   SetLabel(pdfset);
   //     const double  mz = 91.2;
   LHAPDF::initPDFSet(GetLabel(), type, subset);

   //   std::cout << "alphas(mz) = " << LHAPDF::alphasPDF(mz) << std::endl;
   //   std::cout << "qcdlam4    = " << LHAPDF::getLam4(SUBSET) << std::endl;
   //   std::cout << "qcdlam5    = " << LHAPDF::getLam5(SUBSET) << std::endl;
   //   std::cout << "orderPDF   = " << LHAPDF::getOrderPDF() << std::endl;
   //   std::cout << "xmin       = " << LHAPDF::getXmin(SUBSET) << std::endl;
   //   std::cout << "xmax       = " << LHAPDF::getXmax(SUBSET) << std::endl;
   //   std::cout << "q2min      = " << LHAPDF::getQ2min(SUBSET) << std::endl;
   //   std::cout << "q2max      = " << LHAPDF::getQ2max(SUBSET) << std::endl;
   //   std::cout << "orderalfas = " << LHAPDF::getOrderAlphaS() << std::endl;
   //   std::cout << "num flav   = " << LHAPDF::getNf(SUBSET) << std::endl;
   //   std::cout << "name       = " << GetLabel() << std::endl;
   //   std::cout << "number     = " << LHAPDF::numberPDF() << std::endl;
   //   std::cout << std::endl;

}
double *LHAPDFPolarizedPDFs::GetPDFs(double x, double Qsq) {
   /** Implementation of pure virtual method
    *
    *  For std::vector< double > LHAPDF::xfx(const double &x;, const double &Q;)
    *  the returned vector has index labels as such...
    * \code
    *   0..5 = tbar, ..., ubar, dbar;
    *   6 = g;
    *   7..12 = d, u, ..., t
    * \endcode
    */
   fXbjorken = x;
   fQsquared = Qsq;
   std::vector<double> pdfvals = LHAPDF::xfx(fXbjorken,  TMath::Sqrt(fQsquared));
   fPDFValues[0] = pdfvals[8]/x;//up
   fPDFValues[1] = pdfvals[7]/x;//down
   fPDFValues[2] = pdfvals[9]/x;//s
   fPDFValues[3] = pdfvals[10]/x;//c
   fPDFValues[4] = pdfvals[11]/x;//b
   fPDFValues[5] = pdfvals[12]/x;//t
   fPDFValues[6] = pdfvals[6]/x;//g
   fPDFValues[7] = pdfvals[4]/x;//ubar
   fPDFValues[8] = pdfvals[5]/x;//dbar
   fPDFValues[9] = pdfvals[3]/x;//sbar
   fPDFValues[10] = pdfvals[2]/x;//cbar
   fPDFValues[11] = pdfvals[1]/x;//bbar
   fPDFValues[12] = pdfvals[0]/x;//tbar
   /*      std::cout << "u = " << fPDFValues[0] << "\n";*/
   return(fPDFValues);
}
}}
