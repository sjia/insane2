#include "LSS2010_PPDFs.h"

namespace insane {
  namespace physics {
    
    LSS2010_PPDFs::LSS2010_PPDFs(){

      SetNameTitle("LSS2010_PPDFs","LSS2010 pol. PDFs");
      SetLabel("LSS2010");
      // These values are always zero.
      fValues[4] = 0.0;//Delta b
      fValues[5] = 0.0;//Delta t
      fValues[11] = 0.0;//Delta bbar
      fValues[12] = 0.0;//Delta tbar

      for(Int_t i=0;i<12;i++) fUncertainties[i] = 0.; 
    }
    
    LSS2010_PPDFs::~LSS2010_PPDFs()
    { }
    
    const std::array<double,NPartons>& LSS2010_PPDFs::Calculate    (double x, double Q2) const
    {

      fXbjorken = x;
      fQsquared = Q2;

      auto vals = old_pdfs.GetPDFs(x,Q2);

      for(Int_t i=0;i<12;i++) fValues[i] = vals[i]; 

      fUncertainties[0] = 0.0;
      fUncertainties[1] = 0.0;
      fUncertainties[2] = 0.0;
      fUncertainties[3] = 0.0;
      fUncertainties[6] = 0.0;
      fUncertainties[7] = 0.0;
      fUncertainties[8] = 0.0;
      fUncertainties[9] = 0.0;
      fUncertainties[10] = 0.0;

      return(fValues);

    }
    //______________________________________________________________________________

    const std::array<double,NPartons>& LSS2010_PPDFs::Uncertainties(double x, double Q2) const
    {

      fUncertainties[0] = 0.0;
      fUncertainties[1] = 0.0;
      fUncertainties[2] = 0.0;
      fUncertainties[3] = 0.0;
      fUncertainties[6] = 0.0;
      fUncertainties[7] = 0.0;
      fUncertainties[8] = 0.0;
      fUncertainties[9] = 0.0;
      fUncertainties[10] = 0.0;

      return(fUncertainties);

    }
    //______________________________________________________________________________
  }
}

