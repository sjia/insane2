#include "CTEQ10UnpolarizedPDFs.h"


//______________________________________________________________________________
namespace insane {
namespace physics {
CTEQ10UnpolarizedPDFs::CTEQ10UnpolarizedPDFs()
{
   int iSet = 100;     // default
   setct10_(&iSet);  
   SetLabel("CTEQ10");
   SetNameTitle("CTEQ10UnpolarizedPDFs","CTEQ10 PDFs");
   SetLineColor(kCyan+2);
}
//______________________________________________________________________________
CTEQ10UnpolarizedPDFs::~CTEQ10UnpolarizedPDFs(){


}
//______________________________________________________________________________
double *CTEQ10UnpolarizedPDFs::GetPDFs(double x,double Q2){

   /** Implementation of pure virtual method.
    *
    *  The function getct10 (Iparton, X, Q)
    *  returns the parton distribution inside the proton for parton [Iparton]
    *  at [X] Bjorken_X and scale [Q] (GeV) in PDF set [Iset].
    *  Iparton  is the parton label (5, 4, 3, 2, 1, 0, -1, ......, -5)
    *                           for (b, c, s, d, u, g, u_bar, ..., b_bar),
    */

   for(int i=0;i<13;i++) fPDFValues[i] = 0.; 

   fXbjorken = x;
   fQsquared = Q2;
   double Q = TMath::Sqrt(Q2);
   double res = 0.0;
   int i = 0;
   /// up quark 
   i = 1;
   getct10_(&i,&x,&Q,&res);
   fPDFValues[0] = res; 
   /// down quark 
   i = 2;
   getct10_(&i,&x,&Q,&res);
   fPDFValues[1] = res;
   /// strange 
   i = 3;
   getct10_(&i,&x,&Q,&res);
   fPDFValues[2] = res;
   /// charm 
   i = 4; 
   getct10_(&i,&x,&Q,&res);
   fPDFValues[3] = res;
   /// bottom
   i = 5;  
   getct10_(&i,&x,&Q,&res);
   fPDFValues[4] = res;
   /// top 
   fPDFValues[5] = 0;
   /// gluon 
   i = 0;
   getct10_(&i,&x,&Q,&res);
   fPDFValues[6] = res; 
   /// u-bar 
   i = -1;
   getct10_(&i,&x,&Q,&res);
   fPDFValues[7] = res; 
   /// d-bar 
   i = -2;
   getct10_(&i,&x,&Q,&res);
   fPDFValues[8] = res; 
   /// s-bar 
   i = -3;
   getct10_(&i,&x,&Q,&res);
   fPDFValues[9] = res;
   /// c-bar 
   i = -4; 
   getct10_(&i,&x,&Q,&res);
   fPDFValues[10] = res;
   /// b-bar  
   i = -5; 
   getct10_(&i,&x,&Q,&res);
   fPDFValues[11] = res;
   /// t-bar 
   fPDFValues[12] = 0;

   return fPDFValues; 

}
}}
