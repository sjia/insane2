#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedef;

#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;


#pragma link C++ namespace insane;
#pragma link C++ namespace insane::physics;
#pragma link C++ namespace insane::physics::TMCs;
#pragma link C++ namespace insane::kinematics;

#pragma link C++ enum  insane::physics::SF;
#pragma link C++ enum  insane::physics::FormFactor;
#pragma link C++ enum  insane::physics::Nuclei;
#pragma link C++ enum  insane::physics::Parton;
#pragma link C++ enum  insane::physics::ComptonAsymmetry;

#pragma link C++ class insane::physics::BBSQuarkHelicityDistributions+; 
#pragma link C++ class insane::physics::BBSUnpolarizedPDFs+; 
#pragma link C++ class insane::physics::BBSPolarizedPDFs+;
#pragma link C++ class insane::physics::AvakianQuarkHelicityDistributions+; 
#pragma link C++ class insane::physics::AvakianUnpolarizedPDFs+; 
#pragma link C++ class insane::physics::AvakianPolarizedPDFs+; 
#pragma link C++ class insane::physics::LSS98QuarkHelicityDistributions+; 
#pragma link C++ class insane::physics::LSS98UnpolarizedPDFs+; 
#pragma link C++ class insane::physics::LSS98PolarizedPDFs+; 
#pragma link C++ class insane::physics::StatisticalQuarkFits+;
#pragma link C++ class insane::physics::StatisticalUnpolarizedPDFs+;
#pragma link C++ class insane::physics::StatisticalPolarizedPDFs+;
#pragma link C++ class insane::physics::Stat2015UnpolarizedPDFs+;
#pragma link C++ class insane::physics::Stat2015PolarizedPDFs+;
//#pragma link C++ clasinsane::physics::s LHAPDFUnpolarizedPDFs+;
//#pragma link C++ clasinsane::physics::s LHAPDFPolarizedPDFs+;
#pragma link C++ class insane::physics::AAC08PolarizedPDFs+;
#pragma link C++ class insane::physics::BBPolarizedPDFs+;
#pragma link C++ class insane::physics::JAMPolarizedPDFs+;
#pragma link C++ class insane::physics::JAM15PolarizedPDFs+;
#pragma link C++ class insane::physics::MHKPolarizedPDFs+;
#pragma link C++ class insane::physics::DNS2005PolarizedPDFs+;
#pragma link C++ class insane::physics::LSS2006PolarizedPDFs+;
#pragma link C++ class insane::physics::LSS2010PolarizedPDFs+;
#pragma link C++ class insane::physics::DSSVPolarizedPDFs+;

#pragma link C++ global insane::physics::fgPartonDistributionFunctions+;
#pragma link C++ global insane::physics::fgPolarizedPartonDistributionFunctions+;

#pragma link C++ class std::tuple<insane::physics::FormFactor,insane::physics::Nuclei>+;
#pragma link C++ class std::tuple<insane::physics::SF,insane::physics::Nuclei>+;
#pragma link C++ class std::tuple<insane::physics::ComptonAsymmetry,insane::physics::Nuclei>+;

#pragma link C++ class insane::physics::PDFBase2+;

#pragma link C++ class insane::physics::UnpolarizedPDFs+;
#pragma link C++ class insane::physics::PolarizedPDFs+;
#pragma link C++ class insane::physics::Twist3DistributionFunctions+;
#pragma link C++ class insane::physics::Twist4DistributionFunctions+;
#pragma link C++ class insane::physics::T3DFsFromTwist3SSFs+;

#pragma link C++ class insane::physics::CTEQ10_UPDFs+;
#pragma link C++ class std::tuple<insane::physics::CTEQ10_UPDFs>+;

#pragma link C++ class insane::physics::JAM_PPDFs+;
#pragma link C++ class insane::physics::JAM_T3DFs+;
#pragma link C++ class insane::physics::JAM_T4DFs+;
#pragma link C++ class std::tuple<insane::physics::JAM_PPDFs>+;
#pragma link C++ class std::tuple<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs>+;
#pragma link C++ class std::tuple<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs,insane::physics::JAM_T4DFs>+;

#pragma link C++ class insane::physics::Stat2015_UPDFs+;
#pragma link C++ class insane::physics::Stat2015_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::Stat2015_UPDFs>+;
#pragma link C++ class std::tuple<insane::physics::Stat2015_PPDFs>+;

#pragma link C++ class insane::physics::LCWF_T3DFs+;

#pragma link C++ class insane::physics::AAC08_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::AAC08_PPDFs>+;

#pragma link C++ class insane::physics::BB_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::BB_PPDFs>+;

#pragma link C++ class insane::physics::BBS_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::BBS_PPDFs>+;

#pragma link C++ class insane::physics::DSSV_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::DSSV_PPDFs>+;

#pragma link C++ class insane::physics::LSS2006_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::LSS2006_PPDFs>+;

#pragma link C++ class insane::physics::LSS2010_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::LSS2010_PPDFs>+;

#pragma link C++ class insane::physics::DNS2005_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::DNS2005_PPDFs>+;

#pragma link C++ class insane::physics::GS_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::GS_PPDFs>+;

#pragma link C++ class insane::physics::ABDY_PPDFs+;
#pragma link C++ class std::tuple<insane::physics::ABDY_PPDFs>+;





#pragma link C++ class insane::physics::F1F209_SFs+;
#pragma link C++ class insane::physics::NMC95_SFs+;

//#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::AAC08_PPDFs>+;
//#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs>+;
//#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs>+;
//#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::JAM_PPDFs,insane::physics::JAM_T3DFs,insane::physics::JAM_T4DFs>+;
//
//#pragma link C++ class insane::physics::SFsFromPDFs< insane::physics::CTEQ10_PDFs>+;
//
//#pragma link C++ class insane::physics::SFsFromPDFs< insane::physics::Stat2015_UPDFs>+;
//#pragma link C++ class insane::physics::SSFsFromPDFs<insane::physics::Stat2015_PPDFs>+;

//#pragma link C++ class insane::physics::SFsFromPDFs< std::tuple<insane::physics::Stat2015_UPDFs>>+;
//#pragma link C++ class insane::physics::SSFsFromPDFs<std::tuple<insane::physics::Stat2015_PPDFs>>+;

//#pragma link C++ class insane::physics::WaveFunction+;
//#pragma link C++ class BonnDeuteronWaveFunction+;


#pragma link C++ class insane::physics::StrongCouplingConstant+;

#pragma link C++ class insane::physics::PDFBase+;
#pragma link C++ enum  insane::physics::PDFBase::PartonFlavor;
#pragma link C++ class insane::physics::PartonDistributionFunctions+;
#pragma link C++ class insane::physics::PolarizedPartonDistributionFunctions+;

#pragma link C++ class insane::physics::LCWFPartonDistributionFunctions+;
#pragma link C++ class insane::physics::LCWFPolarizedPartonDistributionFunctions+;

#pragma link C++ class insane::physics::PartonHelicityDistributions+;
#pragma link C++ class insane::physics::UnpolarizedPDFsFromPHDs+;
#pragma link C++ class insane::physics::PolarizedPDFsFromPHDs+;

//#pragma link C++ class LHAPDFStructureFunctions+;


#pragma link C++ function formc_(double *)+; 
#pragma link C++ function formm_(double *)+; 

#pragma link C++ function inif1f209_()+;
#pragma link C++ function emc_09_(float * ,float * ,int *,float *)+;
#pragma link C++ function cross_tot_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function cross_qe_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function f1f2in09_(double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function cross_tot_mod_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function cross_qe_mod_(double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function f1f2in09_mod_(double * ,double * ,double *,double * ,double * ,double * ,double * ,double * )+;

#pragma link C++ function EMC_Effect(double * ,double *)+;

#pragma link C++ function qfs_(double*,double*,double*,double*,double*, double*,double*, double*)+;
#pragma link C++ function qfs_born_(double*,double*,double*,double*,double*,double*,double*,double*, double*,double*, double*,int*)+;
#pragma link C++ function qfs_radiated_(double*,double*,double*,double*,double*,double*,double*,double*)+;
#pragma link C++ function wiser_sub_(double * ,int * ,double * ,double * , double * )+;
#pragma link C++ function wiser_all_sig_(double * ,double * ,double * ,double * , int *  ,double * )+;
#pragma link C++ function wiser_all_sig0_(double * ,double * ,double * ,double * , int *  ,double * )+;
#pragma link C++ function wiser_fit_(int * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function vtp_(double * ,double * ,double * ,double * ,double * ,double * ,double * )+;
#pragma link C++ function aac08pdf_(double*,double*,int*,double*,double**)+;
#pragma link C++ function dssvini_(int *)+;
#pragma link C++ function dssvfit_(double *,double *,double *,double *,double *,double *,double *,double *)+;
#pragma link C++ function partondf_(double *,double *,int *)+;
#pragma link C++ function partonevol_(double *x, double *Q2, int *ipol, double * pdf, int * isingle, int *num)+;
#pragma link C++ function mrst2002_(int *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *);
#pragma link C++ function mrst2001e_(int *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *);
#pragma link C++ function mrstpdfs_(int *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *,double *);
#pragma link C++ function setctq6_(int*)+;
#pragma link C++ function getctq6_(int* ,double*, double*, double*)+;
#pragma link C++ function setct10_(int*)+; 
#pragma link C++ function getct10_(int*,double*,double*,double*)+; 
#pragma link C++ function getabkm09_(int*,double*,double*,int*,int*,int*,double*)+; 
#pragma link C++ function getmstw08_(int*,int*,double*,double*,double*)+; 
#pragma link C++ function ppdf_( int*, double*,double*,double*, double*,double*,double*, double*,double*,double*,double*,double*,double*,double*,double*)+;
#pragma link C++ function ini_()+;
#pragma link C++ function jam_xF_(double* res,double* x,double* Q2, char* flav)+;
#pragma link C++ function grid_init_(char*, char*, char*, int*)+;
#pragma link C++ function polfit_(int*,double*,double*,double*, double*, double*,double*,double*, double*,double*,double*)+;
#pragma link C++ function lss2006init_()+;
#pragma link C++ function lss2006_(int*,double*,double*, double*,double*,double*, double*,double*,double*,double*, double*, double*,double*,double*, double*,double*)+;
#pragma link C++ function nloini_()+;
#pragma link C++ function polnlo_(int* ,double*, double*, double*, double*, double*, double*, double*, double*)+;
#pragma link C++ function epcv_single_(char* , int *, double *, double *,double*, double*, double*, double*)+;
#pragma link C++ function epcv_single_v3_(char* , int *, double *, double *,double*, double*, double*, double*)+;
#pragma link C++ function gpc_single_v3_(char* , int *, double *, double *,double*, double*, double*, double*)+;

//#pragma link C++ function sane_pol_(double * ,double * ,double * ,int * ,int * ,double * )+;

#pragma link C++ class insane::physics::GSPolarizedPDFs+;
#pragma link C++ class insane::physics::CTEQ6UnpolarizedPDFs+;
#pragma link C++ class insane::physics::CTEQ10UnpolarizedPDFs+;
#pragma link C++ class insane::physics::CJ12UnpolarizedPDFs+; 
#pragma link C++ class insane::physics::ABKM09UnpolarizedPDFs+; 
#pragma link C++ class insane::physics::MSTW08UnpolarizedPDFs+; 
#pragma link C++ class insane::physics::MRST2001UnpolarizedPDFs+; 
#pragma link C++ class insane::physics::MRST2002UnpolarizedPDFs+; 
#pragma link C++ class insane::physics::MRST2006UnpolarizedPDFs+; 

//// quasi elastic 
//#pragma link C++ class QuasiElasticInclusiveDiffXSec+; 
//#pragma link C++ class QEIntegral+; 
//#pragma link C++ class QEFuncWrap+; 

#endif

