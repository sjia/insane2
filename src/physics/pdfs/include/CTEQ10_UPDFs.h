#ifndef INSANE_PHYSICS_CTEQ10_UPDFs_HH
#define INSANE_PHYSICS_CTEQ10_UPDFs_HH

#include "UnpolarizedPDFs.h"

namespace insane {
  namespace physics {

    class CTEQ10_UPDFs : public UnpolarizedPDFs {
      public:
        CTEQ10_UPDFs();
        virtual ~CTEQ10_UPDFs();

        virtual const std::array<double,NPartons>& Calculate    (double x, double Q2) const;
        virtual const std::array<double,NPartons>& Uncertainties(double x, double Q2) const;

        ClassDef(CTEQ10_UPDFs,1)
    };
  }
}

#endif
