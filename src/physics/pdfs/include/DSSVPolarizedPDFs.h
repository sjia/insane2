#ifndef DSSVPOLARIZEDPDFS_H 
#define DSSVPOLARIZEDPDFS_H 

#include <cstdlib> 
#include <iostream> 
#include <iomanip> 
#include <cmath> 
#include "PolarizedPartonDistributionFunctions.h"
#include "FortranWrappers.h"


namespace insane {
namespace physics {

/** DSSV Polarized parton distruction functions.
 *
 * \ingroup ppdfs
 */
class DSSVPolarizedPDFs: public PolarizedPartonDistributionFunctions{

   private:
      // quark distributions  
      // NOTE: In the class PolarizedPDFs, the labeling in the array fPDFValues is: 
      //  \f$ (0,1,2,3,4,5,6,7,8,9,10,11) = (u,d,s,c,b,t,g,\bar{u},\bar{d},\bar{s},\bar{c},\bar{b},\bar{t}) \f$

      //void Fit(double x,double Q2);
      double Extrapolate(double,double,double,double,double); 

   protected:

      int fiSet;

   public: 
      DSSVPolarizedPDFs(int iset=0);
      virtual ~DSSVPolarizedPDFs();

      virtual double * GetPDFs(     double x, double Q2);  
      virtual double * GetPDFErrors(double x, double Q2);

      /// for switching between PDF sets (to calculate errors on observables that are combos of the PDFs). 
      void SetGrid(int i){fiSet=i;dssvini_(&fiSet);}

      ClassDef(DSSVPolarizedPDFs,1)
};
}
}

#endif 
