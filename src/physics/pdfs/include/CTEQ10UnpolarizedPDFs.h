#ifndef CTEQ10UnpolarizedPDFs_H  
#define CTEQ10UnpolarizedPDFs_H 1 

#include "PartonDistributionFunctions.h"
#include "FortranWrappers.h"
#include "TMath.h"

namespace insane {
namespace physics {
/** CTEQ10 parton distribution functions.
 *
 * H.-L. Lai, M. Guzzi, J. Huston, Z. Li, P.M. Nadolsky, J. Pumplin and C.-P. Yuan
 * Reference: Phys. Rev. D 82, 074024 (2010) 
 *
 * \ingroup updfs
 */
class CTEQ10UnpolarizedPDFs : public PartonDistributionFunctions {

   public:

      CTEQ10UnpolarizedPDFs();
      virtual ~CTEQ10UnpolarizedPDFs();

      double *GetPDFs(double,double);
      double *GetPDFErrors(double x,double Q2){return fPDFErrors;}

      ClassDef(CTEQ10UnpolarizedPDFs,1)

};
}}
#endif
