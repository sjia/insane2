#ifndef Twist4DistributionFunctions_HH
#define Twist4DistributionFunctions_HH 1

#include "InSANEMathFunc.h"
#include "Math.h"
#include "PDFBase.h"
#include <array>

namespace insane {
  namespace physics {

    /** Distributions for constructing the twist 4
     *  spin structure functions.
     *
     */
    class Twist4DistributionFunctions  : public PDFBase2 {
      public:
        Twist4DistributionFunctions();
        virtual ~Twist4DistributionFunctions();

        double g1(double x, double Q2, Nuclei n) const;
        double g2(double x, double Q2, Nuclei n) const;

        double g1_TMC(double x, double Q2, Nuclei n) const;
        double g2_TMC(double x, double Q2, Nuclei n) const;

        virtual double g2p_Twist4(    double x, double Q2) const;
        virtual double g2p_Twist4_TMC(double x, double Q2) const;
        virtual double g1p_Twist4(    double x, double Q2) const{ return 0.0; }
        virtual double g1p_Twist4_TMC(double x, double Q2) const;

        virtual double g2n_Twist4(    double x, double Q2) const;
        virtual double g2n_Twist4_TMC(double x, double Q2) const;
        virtual double g1n_Twist4(    double x, double Q2) const{ return 0.0; }
        virtual double g1n_Twist4_TMC(double x, double Q2) const;

        // Twist 3 quark distribution functions
        virtual double D_u(   double, double) const;
        virtual double D_d(   double, double) const;
        virtual double D_s(   double, double) const;
        virtual double D_ubar(double, double) const;
        virtual double D_dbar(double, double) const;
        virtual double D_sbar(double, double) const;

        // Twist 3 quark-gluon distribution functions
        virtual double Dp_Twist4(  double, double) const ;
        virtual double Dn_Twist4(  double, double) const ;
        virtual double Dd_Twist4(  double, double) const  { return 0.0; }
        virtual double DHe3_Twist4(double, double) const  { return 0.0; }

        ClassDef(Twist4DistributionFunctions,1)
    };

  }
}

#endif

