#ifndef BBSPolarizedPDFs_H
#define BBSPolarizedPDFs_H

#include "BBSQuarkHelicityDistributions.h"
#include "PolarizedPartonDistributionFunctions.h"

namespace insane {
namespace physics {
/** BBS polarized parton distribution functions.  
  * A fit from S.J. Brodsky, M. Burkardt and Ivan Schmidt (BBS)
  *
  * From abstract: For polarized quark and gluon distributions in the nucleon 
  * at low Q2.  Utilizes constraints obtained from requiring 
  * color coherence of gluon couplings at x ~ 0 and helicity 
  * retention properties of pQCD couplings at x ~ 1.  
  * 
  * Paper reference: Nucl. Phys. B 441 (1995) 197--214 
  * DOI: 10.1016/0550-3213(95)00009-H
  * e-Print: hep-ph/9401328 
  * 
  * \ingroup ppdfs
  */
class BBSPolarizedPDFs: public PolarizedPartonDistributionFunctions{

   private: 
      BBSQuarkHelicityDistributions fqhd; 

   public: 
      BBSPolarizedPDFs();
      virtual ~BBSPolarizedPDFs();

      double *GetPDFs(double,double); 
      double *GetPDFErrors(double /*x*/,double /*Q2*/){ for(Int_t i=0;i<12;i++) fPDFErrors[i] = 0.; return fPDFErrors;}

      ClassDef(BBSPolarizedPDFs,1) 

};
}
}
#endif 
