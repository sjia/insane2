#ifndef BBSUnpolarizedPDFs_H
#define BBSUnpolarizedPDFs_H

#include "BBSQuarkHelicityDistributions.h"
#include "PartonDistributionFunctions.h"

namespace insane {
namespace physics {
/** BBS unpolarized parton distribution functions.  
  * A fit from S.J. Brodsky, M. Burkardt and Ivan Schmidt (BBS)
  *
  * From abstract: For polarized quark and gluon distributions in the nucleon 
  * at low Q2.  Utilizes constraints obtained from requiring 
  * color coherence of gluon couplings at x ~ 0 and helicity 
  * retention properties of pQCD couplings at x ~ 1.  
  * 
  * Paper reference: Nucl. Phys. B 441 (1995) 197--214 
  * DOI: 10.1016/0550-3213(95)00009-H
  * e-Print: hep-ph/9401328 
  * 
  * \ingroup updfs
  */
class BBSUnpolarizedPDFs: public PartonDistributionFunctions{

   private: 
      BBSQuarkHelicityDistributions fqhd; 

   public: 
      BBSUnpolarizedPDFs();
      virtual ~BBSUnpolarizedPDFs();

      double *GetPDFs(double,double); 
      double *GetPDFErrors(double /*x*/,double /*Q2*/){ for(Int_t i=0;i<13;i++) fPDFErrors[i] = 0.; return fPDFErrors;}

      ClassDef(BBSUnpolarizedPDFs,1) 

};

}
}

#endif 
