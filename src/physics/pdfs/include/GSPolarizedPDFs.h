#ifndef GSPolarizedPDFs_HH
#define GSPolarizedPDFs_HH 1

#include "PolarizedPartonDistributionFunctions.h"
#include "FortranWrappers.h"

namespace insane {
namespace physics {
/** T. Gehrmann and W.J. Stirling: "Polarized Parton Distributions
 *   of the Nucleon", Phys.Rev. D53 (1996) 6100.
 *
 *  Makes use of the subroutine polnlo which returns the
 *  valence quark polarized pdfs along with the anti-quark distributions.
 *  The full quark pdfs are calculated using the valence and q-bar distributions as
 *  \f$ \Delta u = \Delta u_v + \Delta \bar{q} \f$
 *  since
 *  \f$ \Delta u_v = \Delta u - \Delta \bar{q} \f$
 *
 *
 * \ingroup ppdfs 
 */
class GSPolarizedPDFs : public PolarizedPartonDistributionFunctions {
	private:
		int fiSet;

	public:

		/** C'tor initializes the "pointer to a pointer" gradient matrix, fGrad,
		 *  which returned by the subroutine
		 */
		GSPolarizedPDFs();
		~GSPolarizedPDFs(); 

		/** Virtual method should get all values of pdfs and set
		 *  values of fX and fQsquared
		 *
		 * @param iflag = selects gluon set    0:A, 1:B, 2:C
		 * @param x     = xbjorken
		 * @param q2    = Q^2
		 * OUTPUT parameters (note that always x*distribution is returned)
		 * @param uval  = u-ubar
		 * @param dval  = d-dbar
		 * @param glue
		 * @param ubar  = 1/2 usea
		 * @param dbar = 1/2 dsea
		 * @param str = sbar = 1/2 strsea
		 *
		 *  Calls
		 *  polnlo(iflag,x,q2,uval,dval,glue,ubar,dbar,str)
		 *
		 *  \todo check that the strange is calculated correctly... look at paper..
		 */
		double *GetPDFs(double,double); 
		double *GetPDFErrors(double x,double Q2){ for(Int_t i=0;i<12;i++) fPDFErrors[i] = 0.; return fPDFErrors;}  

		ClassDef(GSPolarizedPDFs, 1)
};

}
}
#endif
