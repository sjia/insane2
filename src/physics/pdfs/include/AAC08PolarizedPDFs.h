#ifndef AAC08PolarizedPDFs_HH
#define AAC08PolarizedPDFs_HH 1

#include "PartonDistributionFunctions.h"
#include "PolarizedPartonDistributionFunctions.h"
#include "FortranWrappers.h"
#include "PPDFs.h"

namespace insane {
namespace physics {
/** Implementation of the "Asymmetry Analysis Collaboration" PDFS
 *
 *  Uses subroutine AAC08PDFs
 *
 * \ingroup ppdfs 
 */
class AAC08PolarizedPDFs : public PolarizedPartonDistributionFunctions {

   protected:

      double fXPDF[7];
      double * gradientcol[11];
      double gradient[11][11];
      double ** fGRAD; // note reverse array order for C->Fortran

   public:

      /** C'tor initializes the "pointer to a pointer" gradient matrix, fGrad,
       *  which returned by the subroutine AAC08PDF
       */
      AAC08PolarizedPDFs();
      virtual ~AAC08PolarizedPDFs();

      /** Virtual method should get all values of pdfs and set
       *  values of fX and fQsquared
       *
       *  \code
       *  !!    XPPDF(I) --> AAC08 polarized PDFs.
       *  !!     I = -3 ... s-bar quark   --> Carray[0]
       *  !!         -2 ... d-bar quark   --> Carray[1]
       *  !!         -1 ... u-bar quark   --> Carray[2]
       *  !!          0 ... gluon D_g(x)  --> Carray[3]
       *  !!          1 ... u quark       --> Carray[4]
       *  !!          2 ... d quark       --> Carray[5]
       *  !!          3 ... s quark       --> Carray[6]
       *  \endcode
       */
      double * GetPDFs(double,double);
      double * GetPDFErrors(double /*x*/,double /*Q2*/){return fPDFErrors;} 

      ClassDef(AAC08PolarizedPDFs, 1)
};



}
}
#endif

