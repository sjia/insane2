#ifndef JAMPolarizedPDFs_HH
#define JAMPolarizedPDFs_HH 2 

#include "PolarizedPartonDistributionFunctions.h"
#include "FortranWrappers.h"
#include "TSpline.h"
#include "TGraph.h"

namespace insane {
namespace physics {
/** JAM Polarized PDFs.
 *
 *  https://www.jlab.org/theory/jam/
 *  http://inspirehep.net/search?p=find+eprint+1310.3734
 *
 * Note that q^+ = q + qbar
 *
 * \ingroup ppdfs
 */
class JAMPolarizedPDFs : public PolarizedPartonDistributionFunctions {

   private:
      double fPars_uplus[5];
      double fPars_dplus[5];
      double fPars_g[5];
      double fPars_ubar[5];
      double fPars_dbar[5];
      double fPars_sbar[5];

      double fParsErr_uplus[5];
      double fParsErr_dplus[5];
      double fParsErr_g[5];
      double fParsErr_ubar[5];
      double fParsErr_dbar[5];
      double fParsErr_sbar[5];

      double fPars_Twist3_p[5];
      double fPars_Twist3_n[5];

      double fKnots_x_p[6];
      double fKnots_y_p[6];

      double fKnots_x_n[6];
      double fKnots_y_n[6];


      double fQ20;

   public:
      TGraph   * fh_Twist4_p_gr;
      TSpline3 * fh_Twist4_p_spline;
      TGraph   * fh_Twist4_n_gr;
      TSpline3 * fh_Twist4_n_spline;


   protected:
      double xDeltaf_model(double x,double *p){
         // Model has 5 parameters
         double Nf = p[0];
         double af = p[1];
         double bf = p[2];
         double cf = p[3];
         double df = p[4];
         double res = Nf*TMath::Power(x,af)*TMath::Power(1.0-x,bf)*(1.0+cf*TMath::Power(x,0.5)+df*x);
         return res;
      }
      
      double xDeltaf_model_error(double x, double *p, double *perr) {
         double res = 0.0;
         double Nf  = p[0];
         double af  = p[1];
         double bf  = p[2];
         double cf  = p[3];
         double df  = p[4];
         // dxf/dNf
         double d0 = TMath::Power(x,af)*TMath::Power(1.0-x,bf)*(1.0+cf*TMath::Power(x,0.5)+df*x);
         // dxf/daf
         double d1 = Nf*d0*TMath::Log(x);
         // dxf/dbf
         double d2 = Nf*d0*TMath::Log(1.0-x);
         // dxf/dcf
         double d3 = Nf*TMath::Power(1.0-x,bf)*TMath::Power(x,0.5+af);
         // dxf/ddf
         double d4 = Nf*TMath::Power(1.0-x,bf)*TMath::Power(x,1.0+af);
         res  = d0*d0*perr[0]*perr[0];
         res += d1*d1*perr[1]*perr[1];
         res += d2*d2*perr[2]*perr[2];
         res += d3*d3*perr[3]*perr[3];
         res += d4*d4*perr[4]*perr[4];
         return TMath::Sqrt(res);
      }
      

   public:

      /** C'tor initializes the "pointer to a pointer" gradient matrix, fGrad,
       *  which returned by the subroutine
       */
      JAMPolarizedPDFs(); 
      virtual ~JAMPolarizedPDFs(); 

      virtual double g1p_Twist4_Q20(double x) {
         double res = fh_Twist4_p_spline->Eval(x)/fQ20;
         //std::cout << "h(x) = " << res << std::endl;
         return res;
      }
      virtual double g1p_Twist4(double x, double Q2) {
         double res = fh_Twist4_p_spline->Eval(x)/Q2;
         //std::cout << "h(x) = " << res << std::endl;
         return res;
      }
      virtual double g1n_Twist4_Q20(double x) {
         double res = fh_Twist4_n_spline->Eval(x)/fQ20;
         return res;
      }
      virtual double g1n_Twist4(double x, double Q2) {
         double res = fh_Twist4_n_spline->Eval(x)/Q2;
         return res;
      }

      virtual double g2p_Twist3_Q20(double x) {
         double xbar = 1.0-x; 
         double fA = fPars_Twist3_p[0];
         double fB = fPars_Twist3_p[1];
         double fC = -1.0*fPars_Twist3_p[2]; // minus from difference between JAM and Braun et al
         double fD = fPars_Twist3_p[3];
         double fE = -1.0*fPars_Twist3_p[4];

         double  t1  = fA*(TMath::Log(x) + xbar + xbar*xbar/2.0) ;
         double  t2  = TMath::Power(xbar,3.0)*(fB - fC*xbar + fD*xbar*xbar - fE*xbar*xbar*xbar);
         return t1 + t2;
      }
      virtual double g2p_Twist3(double x,double Q2) {
         double xbar = 1.0-x; 
         double fA = fPars_Twist3_p[0];
         double fB = fPars_Twist3_p[1];
         double fC = -1.0*fPars_Twist3_p[2]; // minus from difference between JAM and Braun et al
         double fD = fPars_Twist3_p[3];
         double fE = -1.0*fPars_Twist3_p[4];

         double  t1  = fA*(TMath::Log(x) + xbar + xbar*xbar/2.0) ;
         double  t2  = TMath::Power(xbar,3.0)*(fB - fC*xbar + fD*xbar*xbar - fE*xbar*xbar*xbar);
         return t1 + t2;
      }
      virtual double g2n_Twist3_Q20(double x) {
         double xbar = 1.0-x; 
         double fA = fPars_Twist3_n[0];
         double fB = fPars_Twist3_n[1];
         double fC = -1.0*fPars_Twist3_n[2]; // minus from difference between JAM and Braun et al
         double fD = fPars_Twist3_n[3];
         double fE = -1.0*fPars_Twist3_n[4];

         double  t1  = fA*(TMath::Log(x) + xbar + xbar*xbar/2.0) ;
         double  t2  = TMath::Power(xbar,3.0)*(fB - fC*xbar + fD*xbar*xbar - fE*xbar*xbar*xbar);
         return t1 + t2;
      }
      virtual double g2n_Twist3(double x,double Q2) {
         double xbar = 1.0-x; 
         double fA = fPars_Twist3_n[0];
         double fB = fPars_Twist3_n[1];
         double fC = -1.0*fPars_Twist3_n[2]; // minus from difference between JAM and Braun et al
         double fD = fPars_Twist3_n[3];
         double fE = -1.0*fPars_Twist3_n[4];

         double  t1  = fA*(TMath::Log(x) + xbar + xbar*xbar/2.0) ;
         double  t2  = TMath::Power(xbar,3.0)*(fB - fC*xbar + fD*xbar*xbar - fE*xbar*xbar*xbar);
         return t1 + t2;
      }

      /** Virtual method should get all values of pdfs and set
       *  values of fX and fQsquared
       */
      double *GetPDFs(double,double); 
      double *GetPDFErrors(double,double); 

      ClassDef(JAMPolarizedPDFs,2)
};
}}

#endif

