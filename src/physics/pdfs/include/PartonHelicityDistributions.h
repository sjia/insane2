#ifndef PartonHelicityDistribtuions_HH
#define PartonHelicityDistribtuions_HH 1

#include "TObject.h"

namespace insane {
namespace physics {

/** Parton Helicity Distribution Pair.*/
struct PHDSet {
   double fQ_Plus;
   double fQ_Minus;
   double fQbar_Plus;
   double fQbar_Minus;
};

/** Parton (or quark) Helicity Distributions of the nucleon. 
 *
 */
class PartonHelicityDistributions {

   private:

   protected:
      PHDSet fU;
      PHDSet fD;
      PHDSet fS;
      //PHDSet fC;
      //PHDSet fB;
      //PHDSet fT;
      PHDSet fG;


   public:
      PartonHelicityDistributions();
      virtual ~PartonHelicityDistributions();

      const PHDSet& GetU(double x, double Q2) { CalculateDistributions(x,Q2); return fU;}
      const PHDSet& GetD(double x, double Q2) { CalculateDistributions(x,Q2); return fD;}
      const PHDSet& GetS(double x, double Q2) { CalculateDistributions(x,Q2); return fS;} 
      const PHDSet& GetG(double x, double Q2) { CalculateDistributions(x,Q2); return fG;} 

      /** Pure virtual method that must be implemented. 
       *  It should calculate all of the distributions.
       */
      virtual Int_t CalculateDistributions(double x, double Q2) = 0;


   ClassDef(PartonHelicityDistributions,1)
}; 

}}
#endif

