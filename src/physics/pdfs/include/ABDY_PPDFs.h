#ifndef insane_physics_ABDY_PPDFs_HH
#define insane_physics_ABDY_PPDFs_HH 1 

#include "AvakianPolarizedPDFs.h"
#include "PPDFs.h"

namespace  insane {
  namespace physics {

    class ABDY_PPDFs : public PPDFs {
    protected:
      mutable AvakianPolarizedPDFs   old_pdfs;
    public:
      ABDY_PPDFs(); 
      virtual ~ABDY_PPDFs(); 

      virtual const std::array<double,NPartons>& Calculate    (double x, double Q2) const;
      virtual const std::array<double,NPartons>& Uncertainties(double x, double Q2) const;

      ClassDef(ABDY_PPDFs,2)
    };
  }
}

#endif

