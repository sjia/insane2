#ifndef BBPolarizedPDFs_HH
#define BBPolarizedPDFs_HH 2 

#include "PolarizedPartonDistributionFunctions.h"
#include "FortranWrappers.h"
namespace  insane {
  namespace physics {

    /** Bluemlein and Boettcher's LO and NLO Polarized PDFs
     *
     *  Makes use of the subroutine PPDF which returns (with errors) the
     *  valence quark polarized pdfs along with the anti-quark distributions.
     *  The full quark pdfs are calculated using the valence and q-bar distributions as
     *  \f$ \Delta u = \Delta u_v + \Delta \bar{q} \f$
     *  since
     *  \f$ \Delta u_v = \Delta u - \Delta \bar{q} \f$
     *
     *
     * \ingroup ppdfs
     */
    class BBPolarizedPDFs : public PolarizedPartonDistributionFunctions {
    private:
      int fiSet;
      double fg1p, fDg1p, fg1n, fDg1n;

    public:

      /** C'tor initializes the "pointer to a pointer" gradient matrix, fGrad,
       *  which returned by the subroutine
       */
      BBPolarizedPDFs(); 
      virtual ~BBPolarizedPDFs(); 

      /** Virtual method should get all values of pdfs and set
       *  values of fX and fQsquared
       *
       *  Calls
       *  PPDF(ISET, X, Q2,
       *  UV, DUV,
       *  DV, DDV,
       *  GL, DGL,
       *  QB, DQB,
       *  G1P, DG1P, G1N, DG1N)
       *  where quantities with a leading "D" are the corresponding errors.
       *  Note that the quark distributions are valence: q = (qvalence + qsea)
       */
      double *GetPDFs(double,double); 
      double *GetPDFErrors(double,double); 

      ClassDef(BBPolarizedPDFs,2)
    };
  }
}

#endif

