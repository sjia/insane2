/*! \mainpage InSANE2

  \tableofcontents

  \section introduction Introduction

  `InSANE` is a collection C++ libraries for nuclear physics. Starting as
  a tool for analysis of the SANE experiment, `InSANE` has evolved since its
  first version and is currently being refactored for modern C++, hence \b InSANE2.
  (\e 2SANE or \e 2InSANE were considered for new names but starting with 
  a number is probably not a good idea.)

  \section quickstart Quick Start

  - `InSANE2` requires a \b C++17 compiler! But you might be OK with C++14 ;) 
  - [ROOT](https://root.cern.ch/) (compiled with `-Dcxx17:BOOL=ON -Droot7:BOOL=ON` among others)
  - [EIGEN](http://eigen.tuxfamily.org)
  - [CLHEP](https://gitlab.cern.ch/CLHEP/CLHEP)

```
git clone https://eicweb.phy.anl.gov/whit/insane2.git
mkdir build && cd build
cmake ../insane2/. -DCMAKE_INSTALL_PREFIX=$HOME
make -j4 install

# then make sure that paths are configured
export PATH=$HOME/bin:$PATH
export LD_LIBRARY_PATH=$HOME/lib:$LD_LIBRARY_PATH
```

  \section about About

  \subsection contributors Contributors

  InSANE was created and is maintained by Whitney Armstrong (whit@temple.edu).

  InSANE has recieved contributions from the following people:
  - Whitney Armstrong
  - David Flay 
  - Matthew Posik

  \subsection license License
  LGPL 

  <b>In it's current state </b> InSANE does not have complete references. 
  Hopefully that will be my in thesis. <b>Also</b> many exitisting codes, 
  mostly older standlone fortran codes, are used. <b> If you find your code </b> (which you hold copy(ing) rights), 
  and <b>you do not want it used here</b>
  please kindly let me know and it we be removed from the project. 
  But its mostly old fotran codes that have no copyright information. 
  </p>

*/

/*! \page linkingwithpython Using Python and PyROOT with InSANE

    \section pyROOTlink Linking InSANE Libraries with pyROOTlink

    First you need <a href="http://www.python.org/">python</a> 
    and <a href="http://root.cern.ch/drupal/content/pyroot">PyROOT</a>. 
    Also recommended is the python module 
    <a href="http://packages.python.org/rootplot/index.html"> rootplot </a>.
    
    The following example shows one way to use ROOT in python:
    \code
    from ROOT import gROOT,TCanvas
    c=TCanvas() 
    \endcode
    See <a href="http://wlav.web.cern.ch/wlav/pyroot/using.html"> PyROOT Manual</a> for 
    other ways and more details on using PyROOT. 

    \section pyinsanelink Using InSANE in python
    To use InSANE in python you import classes from the module ROOT (like the example above)
    but first you need to load the InSANE libraries and their dependencies into ROOT. 
    This similarly done in your \ref rootlogonC . 
    
    Create the file .rootlogon.py which is executed when you import ROOT. 
    The following example shows how to load and use InSANE from your .rootlogon.py script.
    
    \subsection rootlogonpy Example .rootlogon.py loading InSANE:
    \include simple_rootlogon.py
    
    \subsection example2py Example python script using InSANE:
    \include example2.py
    \image html bigcal_calibration.png "Plot of bigcal calibration coefficients produced by example2.py"
 */

/*! \page treesandbranches Tree and Branch Naming
  \section Trees TTree Names
   <a href="http://root.cern.ch/root/html/TTree.html">TTrees </a>(everyone is 
   <a href="http://root.cern.ch/root/html/TTree.html#TTree:AddFriend">Friends</a> 
   with everyone when it doesn't slow down too much)
   The <a href="http://root.cern.ch/root/html/TTree.html#TTree:BuildIndex">indices are built</a> with (run number, event number) 
   
   The following is a list of trees for SANE analysis (but not complete )
   <ul> 
   <li><em>betaDetectors</em> -> Detector data in a very raw form (uncorrected)
   <li><em>betaDetectors0</em> -> Detector data after a <em>zeroth pass</em> (corrected)
   <li><em>betaDetectors1</em> -> Detector data after a <em>first pass</em> (selection)
   <li><em>Clusters</em> -> Clustering results
   <li><em>pi0results</em> -> Pi0 reconstruction

   </ul>
 
   \section Branches TBranch Names
   <b>betaDetectors</b> Branches:
    - <em>betaDetectorEvent</em> -> Branch for BETAEvent. Do not set manually use helper class SANEEvents
    - <em>fBigcalEvent</em> -> Branch for BigcalEvent. Do not set manually use helper class SANEEvents
    - <em>fGasCherenkovEvent</em> -> Branch for GasCherenkovEvent. Do not set manually use helper class SANEEvents
    - <em>fLuciteHodoscopeEvent</em> -> Branch for LuciteHodoscopeEvent. Do not set manually use helper class SANEEvents
    - <em>fForwardTracker</em> -> Branch for ForwardTracker. Do not set manually use helper class SANEEvents

 */

/*!
   \page newClass Adding a New Class to Shared libraries
   Adding a new class is straightforward following the procedures highlighted 
   in the ROOT documentation. I list the steps for sanity's sake:
   <ol> 
   <li> Make source and header 
   <li> Add ClassDef(Class,#) to header just before the end of the class declaration's "};" 
        It does not need a semicolon after it and should always be the last thing.
   <li> Add ClassImp(Class), with no trailing semicolon at the top of the source. Between the
   includes and member function definitions.
   <li> Add appropriate line to (module)_LinkDef.h file. (eg. #pragma link C++ class BigcalEvent+;)
   <li> Create Dictionary header and source definitions with rootcint ( rootcint -f $(srcdir)/BETAEventDict.cxx -c $(CXXFLAGS) -p $^ ) This makes BETAEventDict.cxx and BETAEventDict.h. Or just find the makefiles in build and add a lib/NEWCLASSTHATNEEDSDICT.o to list of object files
   <li> Compile this to get the object file 
   <li> Assuming you have all objects make the shared libraries : g++ $(CXXFLAGS) -shared  -Wl,-soname,libWHATEVER.so.1.1 -o allTheObjectFiles.o) 
   <li> All goes well you have a shared library libWHATEVER.so.1.1
   </ol>
 */

//______________________________________________________________________________
/*!
   \page UnitsAndConstants Units and Constants

   \subpage SystemOfUnits     "System of Units"
   \subpage PhysicalConstants "Physical Constants"
 */

//______________________________________________________________________________
/*!
   \page Analysis General Analysis Flow
   
   The data splits up into three distinct groups. 
   <ol>
   <li> Raw hardware data
   <li> Measured (or reconstructed) quantities 
   <li> Calculated physics quantities
   </ol>
   Going from Raw to Measured quantities is the most difficult task and can be done in many ways. 
   The physics quantities (e.g. Qsq,x,W etc..) are calculated as a function of the measured 
   quantities and should be independent of how they were reconstructed.
   
   Therefore, for measured quantities, we have the InSANEDetectorEvent class to store their data
   and a separate class, InSANEPhysics, for calculating the physics quantities. 
   
   \section Events Event structures
    (see Events modules)
   
   \section TClonesArray Creating and looping through the TClonesArray
   The various detector hit classes (see Hits module) are stored in a TClonesArray which is then written to the TTree/File. 
   <a href="http://root.cern.ch/root/htmldoc/TClonesArray.html">TClonesArray</a> acts like an array but is dynamic in size and quite fast. Here is some example code to create and fill a TClonesArray:
   \code 
   fgForwardTrackerHits = new TClonesArray("ForwardTrackerHit", 12);
   for(i=0;i<y2t_hit;i++) 
   {
     ftEvent->fNumberOfHits++;
     aFThit = new((*trackerHits)[i]) fgForwardTrackerHits();
     aFThit->fScintLayer = 2; 
   }
   \endcode
   Here is some example code to loop over a TClonesArray:
   \code 
     TClonesArray   clusters = 0; 
     for(Int_t i = 0;i<clusters->GetEntries();i++) 
     {
       aCluster = (BIGCALCluster *)(*clusters)[i];
       aCluster->DoStuff();
     }
   \endcode

*/

/*!
    \page scalers Scalers 
    \section scalerdata Scaler Data Source
    
    \section scaleranalysis Analysis and Syncronization of Scalers
    \section scalerFilters Temporal Scaler Filters
 */


/*!
  @defgroup asymmetries Asymmetries
  Asymmetries
  */

/*! @defgroup physics Physics
  Physics based classes
  */

/*! @defgroup xsections Cross sections
  Cross section specific code

  \image  html polXsecs.png "Example from scripts/polXsecs.cxx showing random sampling of 200k events by helicity" 

  \image  html unpolXsecs.png "Example from scripts/unpolXsecs.cxx" 

  @ingroup physics
  */

/*!
  @defgroup inclusiveXSec Inclusive cross sections
  Inclusive cross sections
  @ingroup xsections
  */
/*! @defgroup semiinclusiveXSec Semi-inclusive cross sections
  Semi-inclusive cross sections
  @ingroup xsections
  */
/*! @defgroup exclusiveXSec Exclusive cross sections
  Exclusive cross sections
  @ingroup xsections
  */

/*! \defgroup QFS QFS
  Quasi-free Scattering code 

  \ingroup xsections
  */
/*! \defgroup wiser WISER
  Inclusive particle production code from 1977
  \ingroup inclusiveXSec
  */
/*! \defgroup EPCV EPCV
  Inclusive particle production code from 1987
  \ingroup inclusiveXSec
  */


//______________________________________________________________________________
/*! @defgroup structurefunctions Structure Functions
  Unpolarized and Polarized structure functions for nucleons and nuclei.

  \image html F2p_compare.png "Comparing F1F2 and LHAPDF code to data " width=10cm

  \image html g1p_compare.png "Comparing g1p code to data " width=10cm

  \image html polStructureFuncs.png "Polarized Structure Functions using ..." width=10cm

  \image html unpolStructureFuncs_LHAPDF.png "Structure Functions from LHAPDF" width=10cm

  \image html unpolStructureFuncs_F1F209.png "Structure Functions from F1F209" width=10cm

  @ingroup physics
  */

//______________________________________________________________________________
/*! @defgroup formfactors Form Factors
  Form factors for nucleons and nuclei

  @ingroup physics
  */

//______________________________________________________________________________
/*! @defgroup partondistributions Parton Distribution Functions
  Unpolarized and Polarized parton distribution functions

  \image html pol_pdfs.png "Polarized PDFs" width=10cm

  @ingroup physics
  */
/*! @defgroup updfs Unpolarized PDFs
  Unpolarized  parton distribution functions

  \image html pol_pdfs.png "Polarized PDFs" width=10cm

  @ingroup partondistributions
  */
/*! @defgroup ppdfs Polarized PDFs
  Unpolarized  parton distribution functions

  \image html pol_pdfs.png "Polarized PDFs" width=10cm


  @ingroup partondistributions
  */

/*! defgroup lhapdf LHAPDF - Unpolarized PDFs
  A simple interface to LHAPDF 
  ingroup partondistributions
  */

/** @name Polarized 
 *  @{ */
/*! defgroup AAC AAC Polarized PDFs
  Uses the Fortran subroutine AAC08PDF 
  ingroup partondistributions
  */
/*! defgroup BB BB Polarized PDFs
  Polarized parton distributions by Bluemlein and Boettcher
  ingroup partondistributions
  */
/*! defgroup DNS2005 DNS2005 Polarized PDFs
  Polarized parton distributions by D. de Florian G.A. Navarro and R. Sassot.
  ingroup partondistributions
  */
/*! defgroup LSS2006 LSS2006 Polarized PDFs
  Polarized parton distributions by E. LEADER, A.V. SIDOROV AND D.B. STAMENOV  
  ingroup partondistributions
  */
/*! defgroup GS1996 GS1996 Polarized PDFs
  Polarized parton distributions by T. Gehrmann and W.J. Stirling 
  ingroup partondistributions
  */
//@}

//______________________________________________________________________________
/*! @defgroup fragfuncs Fragmentation Functions
  Quark fragmentation functions

  @ingroup physics
  */

//______________________________________________________________________________
/*! @defgroup tmds TMDs
  Transverse momentum distributions

  @ingroup physics
  */

//______________________________________________________________________________
/*! @defgroup Analysis Analysis
  This represents a generic analysis procedure
  */

/*! @defgroup emfields Electromagnetic Fields
  Electric and Magnetic fields
  */
/*! @defgroup Calculations Corrections and Calculations
  @ingroup Analysis
  Calculations are attached to a concrete implementation of InSANEAnalyzer and are used for clustering and
  creating new trees and branches in subsequent analysis levels. 
  */
/*! @defgroup Analyzers Analyzers
  @ingroup Analysis
  "Analyzers" are used to collect the detectors, corrections, calculations, clusterings, etc...
  and process an them all in an orderly way. 
  */

/*! @defgroup Analyses Analyses
  @ingroup Analysis
  Analyses ...
  */

/*! @defgroup EventGen Event Generation  

*/

/*! @defgroup Management Management 

*/

/*! @defgroup AnalysisManager  Analysis Manager
  @ingroup Management
  */

/*! @defgroup RunManager  Run Manager
  @ingroup Analysis
  @ingroup Management
  */


/*! @defgroup RunReport  Run Report
  @ingroup RunManager
  @ingroup Management
  */

/*! @defgroup RunTiming  Run Timing
  @ingroup RunManager
  */

/*! @defgroup Runs Runs
  @ingroup RunManager
  Run information
  */

/*! @defgroup Events Events
  Events are interesting indeed!
  */
/*! @defgroup Hits Hits
  @ingroup Events
  "Hit" classes are made to fill arrays with a generic data structure and provided some member functions
  */

/*! @defgroup Apparatus Apparatus
  Particle detectors have data associated with them for each event: ADCs TDCs etc...

  The important detector lingo to focus on here would be "Events" and "Hits". 
  That is an event contains all the data for said event. This may be a single value or
  a large array of numbers. To generalize a large array we use ROOT's TClonesArray. 
  We fill this array not with numbers but "objects" and in particluar a "Hit" object. 
  Thus a need for the classes Events and Hits 

*/
/*! @defgroup Detectors Detectors
  Detectors
  @ingroup Apparatus 
  */
/*! @defgroup detComponents Detector components
  Detector and Apparatus components 
  @ingroup Detectors 
  */
/*! @defgroup beam Hall C Beam
  BETA related code
  @ingroup Detectors 
  */
/*! @defgroup BETA BETA
  BETA related code
  @ingroup Detectors 
  */
/*! @defgroup BIGCAL BigCal
  BigCal related code
  @ingroup Detectors 
  */
/*! @defgroup Cherenkov Gas Cherenkov
  Gas Cherenkov related code
  @ingroup Detectors 
  */
/*! @defgroup tracker Forward Tracker
  Forward Tracker related code
  @ingroup Detectors 
  */
/*! @defgroup hodoscope Lucite Hodoscope
  Lucite Hodoscope related code
  @ingroup Detectors 
  */

/*! @defgroup Geometry Geometry
  Geometry related classes
  */

/*! @defgroup Coordinates Coordinate systems
  @ingroup Geometry
  */

/*! @defgroup Clustering Clustering
  General clustering. Mostly specific to calorimeter clustering... but not limited to
  */

/*! @defgroup Clusters Cluster Objects
  @ingroup Clustering
  Cluster classes used in various algorithms
  */

/*! @defgroup ClusteringAlgorithms Clustering Algorithms
  @ingroup Clustering
  These algorithms for clustering fill the InSANEClusterEvent's TClonesArray with the corresponding clusters.
  These clusters are the 
  concrete implementation of InSANECluster which can be specific to the algorithm.
  */



/*! @defgroup Scalers Scalers for all
  The HallC scalers are filled every 30 seconds and every 2 mins
  */

/*! @defgroup Simulation Simulation specific 
  more to come.
  */


/*! @defgroup Cuts Event Selection 
  @ingroup Analysis
  "Hit" classes are made to fill arrays with a generic data structure and provided some member functions


*/

/*! @defgroup Reconstruction Event Reconstruction
  Event reconstruction uses neural networks, assumed event types and decays, 
  and determines the physics quantities from the detected. 
  */
/*! @defgroup pi0 Pi0 Reconstruction
  @ingroup Reconstruction
  \f$ \pi^{0} \f$ Reconstruction 
  */

/*! @defgroup NeuralNetworks Neural Networks
  @ingroup Reconstruction
  Artificial Neural Networks used in event reconstruction
  */



/*! @defgroup SANE SANE
  This module contains all the SANE specific concrete classes and information
  */
// A simple program that computes the square root of a number
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "InSANEConfig.h"

int main (int argc, char *argv[])
{
  if (argc < 2)
  {
    fprintf(stdout,"%s Version %d.%d\n",
            argv[0],
            InSANE_VERSION_MAJOR,
            InSANE_VERSION_MINOR);
    fprintf(stdout,"Usage: %s number\n",argv[0]);
    return 1;
  }
  double inputValue = atof(argv[1]);
  double outputValue = sqrt(inputValue);
  fprintf(stdout,"The square root of %g is %g\n",
          inputValue, outputValue);
  return 0;
}

