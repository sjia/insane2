#ifndef EventGenerator_H
#define EventGenerator_H 1

#include "TNamed.h"
#include "PhaseSpaceSampler.h"
#include "TList.h"
//#include "BETAG4MonteCarloEvent.h"
#include "TRandom3.h"
#include "InSANEMathFunc.h"
//#include "RunManager.h"
#include "TLorentzVector.h"
#include <iostream>
#include "Particle.h"
#include "TParticlePDG.h"
#include "TClonesArray.h"
#include <vector>
#include "_EG_Event.h"

/*! \page simulation Simulation Tools

 \section montecarlo Monte Carlo Event Generation
     An example that creates a few different cross sections and puts them together in an event generator.
     @include event_generator.cxx

 */


namespace insane {
namespace physics {

  class DiffXSec;

/** Event generator for Montecarlo.
 *  Add phase space samplers which have mutually exclusive phase spaces.
 *  For example, you can add inclusive pi0 and inclusive e-, but NOT,
 *  inclusive e- and a helicity dependent cross section.
 *
 *  The basic operation of an event generator goes like this...
 *  First the phase space samplers are added (see PhaseSpaceSampler for more). 
 *  Then the total cross-section, \f$ \sigma_{T} \f$, is calculated in order to normalize the total probability.
 *  Also, the total cross-section for each sampler, \f$ \sigma_{T}^{i} \f$ added.
 *  When an event is generated using GenerateEvent() a random sampler is selected 
 *  with probability \f$ P(i) = \sigma_{T}^{i}/\sigma_{T} \f$. If there is one sampler the probabilty is 1.
 *  The event is then generated using PhaseSpaceSampler::GenerateEvent() which returns a TList of Particle.
 *  For details of how the kinematic variables are sampled, see PhaseSpaceSampler. 
 *
 *  The volume for uniformly sampling the vertex can be either a box or
 *  a cylinder (rastered beam).
 *  
 *  When creating an event generator, the Initialize() method should be implemented. 
 *  In Initialize() you should attach the appropriate phase space samplers. Here you
 *  can also create cross sections and phase spaces for said phase space samplers. 
 *  There is no need to call Refresh() here. This is done just before using the event generator.
 *
 *
 * \ingroup EventGen
 */
class EventGenerator : public TNamed {

   private:
      bool        fIsModified;

   protected:
      Double_t        fTotalXSection;
      TList           fSamplers;
      Double_t        fBeamEnergy;

      TRandom                 * fRandom;     //!
      Double_t                * fEventArray; //!
      TList                   * fParticles;  //!
      PhaseSpaceSampler * fSampler;    //! currently randomly selected sampler

   public :
      bool        fIsBeamRastered;
      Double_t    fSlowRasterSize;
      Double_t    fdxVertex;
      Double_t    fdyVertex;
      Double_t    fdzVertex;

      _EG_Event fEG_Event;

   public:
      EventGenerator(const char * name = "eventGen", const char * title = "Event Gen") ;
      EventGenerator( const EventGenerator & eg );
      EventGenerator& operator=(const EventGenerator& v);

      virtual ~EventGenerator() ;

      /** Set the beam energy for EVERY cross section.  */ 
      void     SetBeamEnergy(Double_t E0);
      Double_t GetBeamEnergy() const { return fBeamEnergy; }  

      /** Virtual method which should create the specific samplers, cross-sections, etc...
       *  for the specific event generator implementation.
       *  Otherwise it is the same as refresh.
       */
      virtual void Initialize() {
         //std::cout << "EventGenerator::Initialize" << std::endl; 
         /// Add here.
         Refresh();
      }
      void InitFromDisk();

      void Refresh(bool print = false); ///< Called after phase spaces have been modified. 

      void ListSamplers(); ///< Lists each phase space sampler

      void ListPhaseSpaceVariables(); ///< Prints the phase space variables associated with each phase space sampler.

      void Print(const Option_t * opt = "") const ;
      void Print(std::ostream& stream) const ;

      Double_t CalculateTotalCrossSection(); ///< Adds all the sampler's total Xsections together 
      Double_t GetTotalCrossSection() const { return fTotalXSection; } 
      Double_t GetTotalCrossSectionForParticle(Int_t pid =0) ;

      /** Add a configured phase space sampler to the event generator.
       */
      void AddSampler(PhaseSpaceSampler * ps) ;

      /** Clear the Event generator. Removes all samplers and resets the total Xsection to zero.
       */
      void Clear(Option_t * opt = "") ;

      /** Get a random sampler with a probablity proportional to it's total cross section.
       *  Used to select the phase space sampler from which the event will generated.
       */
      PhaseSpaceSampler * GetRandomSampler() ;

      TList * GetSamplers(); ///< Returns pointer to the list of PS samplers

      /** Returns a list with the variable names that match.
       *  Looks through all sameplers/phase spaces. 
       */
      TList * GetPSVariables(const char * var );

      /** Generates a random vertex. 
       *  The argument is
       */ 
      TLorentzVector GetRandomVertex(Int_t i=-1) ;

      /** Checks each cross-section their phase-space and its associated variables by using
       *  their methods IsModified().
       *  Use as a check, but not at the event level!
       */
      bool NeedsRefreshed() ;

      bool IsModified() const { return fIsModified; }
      void SetModified(bool val = true) { fIsModified = val; }

      void PrintXSecSummary(std::ostream& s = std::cout);

      /** Generate an event. 
       *  This method retuns a list of Particles to be thrown.
       *  Get a random sampler and use it to generate a random event
       *
       *  \todo Need to translate P.S. variable (which are arbitrary quantities)
       *        into well defined Particle
       */
      TList * GenerateEvent() ;

      Double_t * GetEventArray() { return(fEventArray); }

      void LundFormat( std::ostream& s = std::cout );

      void LundHeaderFormat(
            _EG_Event& eg_event,
            std::ostream& s = std::cout) ;

      void LundEventFormat(
            int           i, 
            _EG_Event& eg_event,
            std::ostream& s = std::cout) ;


      ClassDef(EventGenerator, 4)
};

}
}


#endif

