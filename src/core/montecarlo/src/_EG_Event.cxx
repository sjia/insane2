#include "_EG_Event.h"
#include "TList.h"

namespace insane {
namespace physics {
_EG_Event::_EG_Event() : 
   //fEventNumber(0),
   //fRunNumber(0),
   fXS_id(0), 
   fBeamPol(0.0), 
   fTargetPol(0.0),
   fXSec(nullptr)
{
   fParticles = new TClonesArray("Particle",3);
   fParticles->SetOwner(true);
}
//______________________________________________________________________________

_EG_Event::~_EG_Event()
{
   delete fParticles;
}
//______________________________________________________________________________

void _EG_Event::Clear()
{
   fArray.clear();
   fParticles->Clear();
   fPSVariables.clear();
}
//______________________________________________________________________________

Particle * _EG_Event::GetParticle(int i)
{
   if( i<fParticles->GetEntries() ) { 
      return( (Particle*)((*fParticles)[i]) );
   }
   return nullptr;
}
//______________________________________________________________________________

void _EG_Event::SetParticles(TList * l)
{
   Clear();
   if(l){
      int n = l->GetEntries();
      for(int i = 0; i<n ; i++){
         fArray.push_back((Particle*)l->At(i));
         new ((*fParticles)[i]) Particle(*((Particle*)l->At(i)));
      }
   }
}
//______________________________________________________________________________
void _EG_Event::SetPSVariables(double * vars, int n)
{
   fPSVariables.resize(n);
   for(int i = 0; i<n; i++){
      fPSVariables[i] = vars[i];
   }
}
//______________________________________________________________________________

Particle* _EG_Event::GetParticle(int i) const
{
   int n = fParticles->GetEntries();
   if( (i>=0) && (i<n) ){
      return (Particle*)fParticles->At(i);
   }
   return nullptr;
}
//______________________________________________________________________________

int _EG_Event::GetNParticles() const
{
   return fParticles->GetEntries();
}
//______________________________________________________________________________
}}
