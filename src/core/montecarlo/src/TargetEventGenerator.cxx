#include "TargetEventGenerator.h"
//#include "Radiator.h"
//#include "QuasiElasticInclusiveDiffXSec.h"
//#include "ElasticRadiativeTail.h"
#include "TFile.h"

namespace insane {
namespace physics {
//______________________________________________________________________________
TargetEventGenerator::TargetEventGenerator(const char *n, const char * t) : EventGenerator(n,t) {
   fNCrossSections = 0;
   fTarget = nullptr;//new SimpleTargetWithWindows();
   fLumin  = nullptr;//new Luminosity(fTarget,100.0e-9);
}
//______________________________________________________________________________
TargetEventGenerator::TargetEventGenerator(const TargetEventGenerator & eg) : EventGenerator(eg){
   fTarget = eg.fTarget;
   fXSecs.AddAll(&(eg.fXSecs));
   fLumin  = eg.fLumin;
}
//______________________________________________________________________________
TargetEventGenerator::~TargetEventGenerator(){
}
//________________________________________________________________________________
TargetEventGenerator& TargetEventGenerator::operator=(const TargetEventGenerator& eg){
   EventGenerator::operator=(eg);
   if (this != &eg) {
      fTarget = eg.fTarget;
      fXSecs.AddAll(&(eg.fXSecs));
      fLumin  = eg.fLumin;
      fLumin->SetTarget(fTarget);
   }
   return *this;
}
//______________________________________________________________________________
void TargetEventGenerator::SetTarget(Target * t){
   fTarget = t;
   //if(fLumin) delete fLumin;
   // here we set the beam current to 100nA but it doesn't really matter
   // unless we are somehow interested in absolute rates.
   fLumin = new Luminosity(fTarget,100.0e-9);
}
//______________________________________________________________________________

Target *  TargetEventGenerator::GetTarget(){ return fTarget; }
//______________________________________________________________________________

void TargetEventGenerator::InitializeMaterialXSec(
      const Int_t i, 
      const Double_t weight, 
      const TargetMaterial * mat, 
      const Nucleus * targ)
{
   std::cout << "i = " << i << std::endl;

   if( i==0 ) fNCrossSections = fSamplers.GetEntries();

   for(int isamp = 0; isamp < fNCrossSections; isamp++) {

      auto * a_samp = (PhaseSpaceSampler*)fSamplers.At(isamp);
      PhaseSpaceSampler * samp   = nullptr;
      if( i == 0) {
         samp = a_samp;
      } else {
         samp = new PhaseSpaceSampler(*a_samp);
      }

      DiffXSec * xsec = samp->GetXSec();
      xsec->SetTargetMaterial(*mat);
      xsec->SetTargetMaterialIndex(i);
      xsec->SetBeamEnergy(GetBeamEnergy());
      xsec->SetTargetNucleus(*targ);
      // note that copying the sampler (cloning the cross section) re-uses the phase space
      // pointer so we do not need to re-init
      //xsec->InitializePhaseSpaceVariables();
      //xsec->InitializeFinalStateParticles();
      samp->SetWeight(weight);
      if(i != 0 ) AddSampler(samp);

   }
   //F1F209eInclusiveDiffXSec * xsec = new F1F209eInclusiveDiffXSec();
   //InclusiveBornDISXSec * xsec = new InclusiveBornDISXSec();
   //Radiator<QuasiElasticInclusiveDiffXSec> * xsec = new Radiator<QuasiElasticInclusiveDiffXSec>();
   //Radiator<InclusiveBornDISXSec> * xsec = new Radiator<InclusiveBornDISXSec>();
   //xsec->Dump();
   //xsec->SetTargetMaterial(*mat);
   //xsec->SetTargetMaterialIndex(i);
   //xsec->SetBeamEnergy(GetBeamEnergy());
   //xsec->SetTargetNucleus(*targ);
   //xsec->InitializePhaseSpaceVariables();
   //xsec->InitializeFinalStateParticles();
   //samp = new PhaseSpaceSampler(xsec);
   ////samp->SetFoamCells(500);
   //samp->SetFoamChat(0);
   //samp->SetFoamCells(200);
   //samp->SetFoamSample(50);
   ////   samp->SetFoamCells(10);
   ////   samp->SetFoamSample(10);
   //samp->SetWeight(weight);
   //AddSampler(samp);

   //if( i==0 ) {
   //   // Add elastic radiative tail for proton 
   //   ElasticRadiativeTail * xsec_tail = new  ElasticRadiativeTail();
   //   xsec_tail->SetPolarizations(0.0,0.0);
   //   xsec_tail->SetTargetMaterial(*mat);
   //   xsec_tail->SetTargetMaterialIndex(i);
   //   xsec_tail->SetBeamEnergy(GetBeamEnergy());
   //   //xsec_tail->SetTargetNucleus(Nucleus::Proton());
   //   xsec_tail->SetTargetNucleus(*targ);
   //   xsec_tail->InitializePhaseSpaceVariables();
   //   xsec_tail->InitializeFinalStateParticles();
   //   xsec_tail->GetPhaseSpace()->GetVariableWithName("energy")->SetMinimum(0.5);
   //   xsec_tail->GetPhaseSpace()->GetVariableWithName("energy")->SetMaximum(4.5);
   //   xsec_tail->GetPhaseSpace()->GetVariableWithName("theta")->SetMinimum(25.0*degree);
   //   samp = new PhaseSpaceSampler(xsec_tail);
   //   //samp->SetFoamCells(100);
   //   samp->SetFoamCells(10);
   //   samp->SetFoamSample(10);
   //   samp->SetWeight(weight);
   //   AddSampler(samp);
   //}

   //ElectroWiserDiffXSec * xsec2 = new ElectroWiserDiffXSec();
   //xsec2->SetTargetMaterial(*mat);
   //xsec2->SetTargetMaterialIndex(i);
   //xsec2->SetBeamEnergy(GetBeamEnergy());
   //xsec2->SetTargetNucleus(*targ);
   //xsec2->SetProductionParticleType(211);
   //xsec2->InitializePhaseSpaceVariables();
   //xsec2->InitializeFinalStateParticles();
   //samp = new PhaseSpaceSampler(xsec2);
   //samp->SetFoamCells(100);
   //samp->SetWeight(weight);
   //AddSampler(samp);

   //ElectroWiserDiffXSec * xsec3 = new ElectroWiserDiffXSec();
   //xsec3->SetTargetMaterial(*mat);
   //xsec3->SetTargetMaterialIndex(i);
   //xsec3->SetBeamEnergy(GetBeamEnergy());
   //xsec3->SetTargetNucleus(*targ);
   //xsec3->SetProductionParticleType(-211);
   //xsec3->InitializePhaseSpaceVariables();
   //xsec3->InitializeFinalStateParticles();
   //samp = new PhaseSpaceSampler(xsec3);
   //samp->SetFoamCells(100);
   //samp->SetWeight(weight);
   //AddSampler(samp);

   //ElectroWiserDiffXSec * xsec4 = new ElectroWiserDiffXSec();
   //xsec4->SetTargetMaterial(*mat);
   //xsec4->SetTargetMaterialIndex(i);
   //xsec4->SetBeamEnergy(GetBeamEnergy());
   //xsec4->SetTargetNucleus(*targ);
   //xsec4->SetProductionParticleType(111);
   //xsec4->InitializePhaseSpaceVariables();
   //xsec4->InitializeFinalStateParticles();
   //samp = new PhaseSpaceSampler(xsec4);
   ////samp->SetFoamCells(200);
   //samp->SetWeight(weight);
   //AddSampler(samp);

   //PhotoWiserDiffXSec * xsec5 = new PhotoWiserDiffXSec();
   //xsec5->SetTargetMaterial(*mat);
   //xsec5->SetTargetMaterialIndex(i);
   //xsec5->SetBeamEnergy(GetBeamEnergy());
   //xsec5->SetTargetNucleus(*targ);
   //xsec5->SetRadiationLength(mat->GetNumberOfRadiationLengths()/2.0);
   //xsec5->SetProductionParticleType(111);
   //xsec5->InitializePhaseSpaceVariables();
   //xsec5->InitializeFinalStateParticles();
   //samp = new PhaseSpaceSampler(xsec5);
   //samp->SetFoamCells(500);
   //samp->SetFoamSample(100);
   //samp->SetWeight(weight);
   //AddSampler(samp);


}
//______________________________________________________________________________
void TargetEventGenerator::Initialize() {
   if(!fTarget) {
      Error("Initialize","No target set!");
   } else {

      //std::cout << "Target\n";
      std::cout << "-------------------------------------------------------------\n";
      fTarget->Print();

      Double_t rl_tot = fTarget->GetNumberOfRadiationLengths();

      Double_t L_tot  = fLumin->CalculateLuminosity();

      std::cout << "-------------------------------------------------------------\n";
      std::cout << "Target Materials: \n";
      for(int i=0;i<fTarget->GetNMaterials();i++){

         TargetMaterial * mat = fTarget->GetMaterial(i);
         mat->Print();

         // Old method (by radiation legnth) :
         //    The material cross section is independent of how much material
         //    so the phase space samplers are weighted by the fraction of the total
         //    radiation length for a given material.
         //Double_t weight = mat->GetNumberOfRadiationLengths()/rl_tot;
         Double_t rl = mat->GetNumberOfRadiationLengths();

         // New method :
         // Weight is now the luminosity fraction for that target material. 
         // We use the luminosity fraction so that weight is less than 1.
         Double_t L  = fLumin->GetMaterialLuminosity(i);

         Double_t weight = L/L_tot;

         //std::cout << " old weight (RL) to new weight (Lumin) " << rl/rl_tot << " : " << weight << std::endl;

         Nucleus targ(int(mat->fZ),int(mat->fA));
         InitializeMaterialXSec(i,weight,mat,&targ);

      }
      std::cout << "-------------------------------------------------------------\n";
   }
   Refresh(true);
}
//______________________________________________________________________________
void TargetEventGenerator::Print(std::ostream& stream) const {
   EventGenerator::Print(stream);
}
//______________________________________________________________________________
void TargetEventGenerator::Print(const Option_t * opt) const {
   EventGenerator::Print(opt);
}
//______________________________________________________________________________



//______________________________________________________________________________
BETAG4SavedEventGenerator::BETAG4SavedEventGenerator(const char *n, const char * t) : TargetEventGenerator(n,t){
   fThetaTarget                = 0.0   ;
   fNumberOfGeneratedParticles = 1     ;
   fIsInitialized              = false ;

   fEnergyMax                  = 0.0   ;
   fEnergyMin                  = 0.0   ;
   fMomentumMax                = 0.0   ;
   fMomentumMin                = 0.0   ;
   fDeltaEnergy                = 0.0   ;
   fCentralEnergy              = 0.0   ;

   fThetaMax                   = 0.0   ;
   fThetaMin                   = 0.0   ;
   fDeltaTheta                 = 0.0   ;
   fCentralTheta               = 0.0   ;

   fPhiMax                     = 0.0   ;
   fPhiMin                     = 0.0   ;
   fDeltaPhi                   = 0.0   ;
   fCentralPhi                 = 0.0   ;
}
//______________________________________________________________________________
BETAG4SavedEventGenerator::BETAG4SavedEventGenerator(const BETAG4SavedEventGenerator & eg) : TargetEventGenerator(eg){
   fThetaTarget                = eg.fThetaTarget                ;
   fNumberOfGeneratedParticles = eg.fNumberOfGeneratedParticles ;
   fIsInitialized              = eg.fIsInitialized              ;

   fEnergyMax                  = eg.fEnergyMax                  ;
   fEnergyMin                  = eg.fEnergyMin                  ;
   fMomentumMax                = eg.fMomentumMax                ;
   fMomentumMin                = eg.fMomentumMin                ;
   fDeltaEnergy                = eg.fDeltaEnergy                ;
   fCentralEnergy              = eg.fCentralEnergy              ;

   fThetaMax                   = eg.fThetaMax                   ;
   fThetaMin                   = eg.fThetaMin                   ;
   fDeltaTheta                 = eg.fDeltaTheta                 ;
   fCentralTheta               = eg.fCentralTheta               ;

   fPhiMax                     = eg.fPhiMax                     ;
   fPhiMin                     = eg.fPhiMin                     ;
   fDeltaPhi                   = eg.fDeltaPhi                   ;
   fCentralPhi                 = eg.fCentralPhi                 ;
}
//______________________________________________________________________________
BETAG4SavedEventGenerator::~BETAG4SavedEventGenerator() {
}
//______________________________________________________________________________
BETAG4SavedEventGenerator& BETAG4SavedEventGenerator::operator=(const BETAG4SavedEventGenerator& eg) {
   TargetEventGenerator::operator=(eg);
   if (this != &eg) {
      fThetaTarget                = eg.fThetaTarget                ;
      fNumberOfGeneratedParticles = eg.fNumberOfGeneratedParticles ;
      fIsInitialized              = eg.fIsInitialized              ;

      fEnergyMax                  = eg.fEnergyMax                  ;
      fEnergyMin                  = eg.fEnergyMin                  ;
      fMomentumMax                = eg.fMomentumMax                ;
      fMomentumMin                = eg.fMomentumMin                ;
      fDeltaEnergy                = eg.fDeltaEnergy                ;
      fCentralEnergy              = eg.fCentralEnergy              ;

      fThetaMax                   = eg.fThetaMax                   ;
      fThetaMin                   = eg.fThetaMin                   ;
      fDeltaTheta                 = eg.fDeltaTheta                 ;
      fCentralTheta               = eg.fCentralTheta               ;

      fPhiMax                     = eg.fPhiMax                     ;
      fPhiMin                     = eg.fPhiMin                     ;
      fDeltaPhi                   = eg.fDeltaPhi                   ;
      fCentralPhi                 = eg.fCentralPhi                 ;
   }
   return *this;
}
//______________________________________________________________________________
BETAG4SavedEventGenerator::BETAG4SavedEventGenerator(const char * name, TFile * f){
   if(!f) {
      Error("BETAG4SavedEventGenerator","File is null");
   } else {

      f->cd();
      auto * evgen = (BETAG4SavedEventGenerator*)gROOT->FindObject(name);
      if(!evgen) {
         Error("BETAG4SavedEventGenerator","Cannot find cross section : %s",name);
      } else {
         (*this) = (*evgen);
         this->InitFromDisk();
      }
   }
}
//______________________________________________________________________________
Int_t BETAG4SavedEventGenerator::SaveToFile(const char * fName, const char * n){

   auto * fout = new TFile(fName,"UPDATE");
   fout->cd();
   Int_t nBytes = 0;
   if(n) {
      nBytes = this->Write(n);
   } else {
      nBytes = this->Write(GetName());
   }
   std::cout << "written\n";
   fout->Write();
   fout->Close();
   if( !nBytes ) return 0;
}
//______________________________________________________________________________
Int_t BETAG4SavedEventGenerator::LoadFromFile(const char * fName, const char * n){
   auto * fin = new TFile(fName,"READ");
   if(!fin) {
      Error("LoadFromFile", "could not load file");
      return -1;
   }
   fin->cd();
   auto * evgen = (BETAG4SavedEventGenerator*)gROOT->FindObject(n);
   std::cout << "loaded object from file\n";
   //evgen->fTarget->InitFromFile();
   if(!evgen) {
      Error("LoadFromFile","Cannot find cross section : %s",n);
   } else {
      (*this) = (*evgen);
      this->InitFromDisk();
   }
   //fin->Close();
   return 0;
}
//______________________________________________________________________________


}}
