#include "EventGenerator.h"
#include <sstream>

namespace insane {
namespace physics {

EventGenerator::EventGenerator(const char * name, const char * title) : TNamed(name, title) {

   fIsModified     = true;
   fTotalXSection  = 0.0;
   fSamplers.Clear();
   fSamplers.SetOwner(true);
   fBeamEnergy     = 5.9;//GeV
   fRandom         = new TRandom3(0);
   fEventArray     = nullptr;
   fParticles      = nullptr;
   fSampler        = nullptr;
   fIsBeamRastered = true;
   fSlowRasterSize = 3.0; //cm
   fdzVertex       = 2.0; //cm
   fdxVertex       = fSlowRasterSize/2.0; //cm
   fdyVertex       = fSlowRasterSize/2.0; //cm
}
//________________________________________________________________________________

EventGenerator::EventGenerator( const EventGenerator & eg ) : TNamed(eg) {
   fIsModified     = eg.fIsModified;
   fTotalXSection  = eg.fTotalXSection;
   fSamplers.AddAll( &(eg.fSamplers) );
   fSamplers.SetOwner(true); // not sure about this
   fBeamEnergy     = eg.fBeamEnergy;//GeV
   fRandom         = new TRandom3(0);
   fIsBeamRastered = eg.fIsBeamRastered;
   fSlowRasterSize = eg.fSlowRasterSize; //cm
   fdxVertex       = eg.fdxVertex; //cm
   fdyVertex       = eg.fdyVertex; //cm
   fdzVertex       = eg.fdzVertex; //cm
}
//________________________________________________________________________________

EventGenerator& EventGenerator::operator=(const EventGenerator& eg){
   TNamed::operator=(eg);
   if (this != &eg) {
      fIsModified     = eg.fIsModified;
      fTotalXSection  = eg.fTotalXSection;
      fSamplers.AddAll( &(eg.fSamplers) );
      fSamplers.SetOwner(true); // not sure about this
      fBeamEnergy     = eg.fBeamEnergy;//GeV
      fRandom         = new TRandom3(0);
      fIsBeamRastered = eg.fIsBeamRastered;
      fSlowRasterSize = eg.fSlowRasterSize; //cm
      fdxVertex       = eg.fdxVertex; //cm
      fdyVertex       = eg.fdyVertex; //cm
      fdzVertex       = eg.fdzVertex; //cm
   }
   return *this;
}
//______________________________________________________________________________
EventGenerator::~EventGenerator() {

}
//________________________________________________________________________________
void EventGenerator::SetBeamEnergy(Double_t E0) {
   fBeamEnergy = E0;
   for(int i = 0; i< fSamplers.GetEntries(); i++) {
      auto * samp = (PhaseSpaceSampler*)fSamplers.At(i);
      DiffXSec * xsec = samp->GetXSec();
      if(xsec) xsec->SetBeamEnergy(E0);
   }
   SetModified(true);
}

//________________________________________________________________________________
void EventGenerator::Refresh(bool print) {
   //std::cout << " Refreshing Event Generator\n";
   fTotalXSection = 0.0;
   PhaseSpaceSampler * aSampler = nullptr;
   TListIter samplerIter(&fSamplers);
   while ((aSampler = (PhaseSpaceSampler*)samplerIter.Next())) {
      /*         if( !(aSampler->GetXSec() ) ) aSampler->Load();*/
      //std::cout << "refreshing sampler of " << aSampler->GetXSec()->GetName() << " ... " << std::endl;;
      //std::cout << "solid angle: " << aSampler->GetXSec()->GetPhaseSpace()->GetSolidAngle() << " sr " << std::endl;
      //std::cout << "delta E    : " << aSampler->GetXSec()->GetPhaseSpace()->GetEnergyBite() << " GeV " << std::endl;

      aSampler->Refresh();
      if( print ) {
         const TargetMaterial& mat = aSampler->GetXSec()->GetTargetMaterial();
         //if(mat) 
            std::cout << "material (" << std::setw(10) << mat.GetName() << ") ";
         //std::cout << " Done refreshing PS Sampler " << aSampler->GetName() << std::endl;;
         std::cout << "XS_id: " << aSampler->GetXSec()->GetID() << " (" << std::setw(30) << aSampler->GetXSec()->GetName() << ") = " << aSampler->GetTotalXSection() << " nb" << std::endl;;
      }
   }
   //std::cout << " Calculating the total cross-section ... " << std::endl;
   CalculateTotalCrossSection();
   SetModified(false);
}
//________________________________________________________________________________

void EventGenerator::ListSamplers()
{
   PhaseSpaceSampler * aSampler = nullptr;
   TListIter samplerIter(&fSamplers);
   while ((aSampler = (PhaseSpaceSampler*)samplerIter.Next())) {
      aSampler->Print();
   }
}
//________________________________________________________________________________

void EventGenerator::PrintXSecSummary(std::ostream& s)
{
   if(IsModified()) Refresh();
   PhaseSpaceSampler * aSampler = nullptr;
   Double_t total = 0;
   TListIter samplerIter(&fSamplers);
   s 
      << std::setw(11) << "XS_ID"
      << std::setw(10)  << "PDG_codes"
      << std::setw(20)  << "XS (nb)"
      << std::endl;;
   while ((aSampler = (PhaseSpaceSampler*)samplerIter.Next())) {

      Double_t tot = aSampler->GetTotalXSection();
      total += tot;
      //Int_t   part = aSampler->GetXSec()->GetParticleType();
      std::stringstream ss_pdg;
      int npart = aSampler->GetXSec()->GetParticles()->GetEntries();
      for(int ipdg = 0; ipdg < npart; ipdg++) {
         if(ipdg != 0 ) ss_pdg << ", ";
         ss_pdg << aSampler->GetXSec()->GetParticleType(ipdg) ;
      }
      std::string part = ss_pdg.str();

   s  
      << "{" 
      << std::setw(11) << aSampler->GetXSec()->GetID()
      << ", {"
      << std::setw(10)  << part
      << "}, "
      << std::setw(20)  << std::right << std::setprecision(6) << std::scientific << tot << std::defaultfloat << std::left
      << std::endl;
      //std::cout << part << " " << tot << " nb" << std::endl;
      //aSampler->Refresh();
   }
   s << "-------------------------------" << std::endl;
   s  
      << std::setw(21) << " "
      << std::setw(20)  << total
      << std::endl;
   //std::cout << " total : " << total << " nb" << std::endl;

}
//________________________________________________________________________________

Double_t EventGenerator::GetTotalCrossSectionForParticle(Int_t pid )
{
   if(IsModified()) Refresh();
   if( pid==0 ) return fTotalXSection;
   PhaseSpaceSampler * aSampler = nullptr;
   Double_t total = 0;
   TListIter samplerIter(&fSamplers);
   while ((aSampler = (PhaseSpaceSampler*)samplerIter.Next())) {

      Double_t tot = aSampler->GetTotalXSection();
      Int_t   part = aSampler->GetXSec()->GetParticleType();
      if(pid == part) total += tot;
      //aSampler->Refresh();
   }
   return total;
}
//________________________________________________________________________________

void EventGenerator::ListPhaseSpaceVariables()
{
   PhaseSpaceSampler * aSampler = nullptr;
   TListIter samplerIter(&fSamplers);
   while ((aSampler = (PhaseSpaceSampler*)samplerIter.Next())) {
      aSampler->GetXSec()->GetPhaseSpace()->Print();
   }
}
//______________________________________________________________________________

void EventGenerator::InitFromDisk()
{
   TListIter samplerIter(&fSamplers);
   std::cout << "init from disk\n";
   PhaseSpaceSampler * aSampler = nullptr;
   while (( aSampler = (PhaseSpaceSampler*)samplerIter.Next())) {
      aSampler->InitFromDisk();
      std::cout << "done init sampler\n";
   }
}
//________________________________________________________________________________

void EventGenerator::Print(std::ostream& stream) const
{
   stream << "================================================================================" << std::endl;
   stream << "EG: " <<  GetName() << std::endl;
   stream << "================================================================================" << std::endl;
   stream <<  " Total cross section = " << 
     std::scientific << fTotalXSection         << " [nb] = " << 
     std::scientific << fTotalXSection*1.0e-9  << " [b] = " << 
     std::scientific << fTotalXSection*1.0e-33 << " [cm^2] " << std::endl; 
   PhaseSpaceSampler * aSampler = nullptr;
   TListIter samplerIter(&fSamplers);
   int i = 0;
   while ((aSampler = (PhaseSpaceSampler*)samplerIter.Next())) {
      stream << "  ----------------------------------------------------------------------------" << std::endl;
      stream << "  PS Sampler #" << i << std::endl;
      aSampler->Print(stream);
      i++;
   }
}
//________________________________________________________________________________

void EventGenerator::Print(const Option_t * opt) const
{
   EventGenerator::Print(std::cout);
   //std::cout << "--------------------------------------------------------------------------------" << std::endl;
   //std::cout << "Event Generator     : " <<  GetName() << std::endl;
   //std::cout << "Total Cross Section : " << fTotalXSection << " nb" << std::endl;
   //PhaseSpaceSampler * aSampler = 0;
   //TListIter samplerIter(&fSamplers);
   //int i = 1;
   //while ((aSampler = (PhaseSpaceSampler*)samplerIter.Next())) {
   //   std::cout << "   o----------------------------------------------------------------------------" << std::endl;
   //   std::cout << "   o phase space sampler " << i << std::endl;
   //   aSampler->Print();
   //   i++;
   //}
}
//________________________________________________________________________________

Double_t EventGenerator::CalculateTotalCrossSection()
{
   PhaseSpaceSampler * aSampler = nullptr;
   fTotalXSection = 0.0;
   TListIter samplerIter(&fSamplers);
   while ((aSampler = (PhaseSpaceSampler*)samplerIter.Next())) {
      fTotalXSection += aSampler->CalculateTotalXSection();
   }
   //printf(" EventGenerator total Xsection is %f nb.\n", fTotalXSection);
   std::cout << " material (" << std::setw(20) << "All" << "), ";
   std::cout << " cross section (" << std::setw(30) << "Total" << ") = " << fTotalXSection << " nb" << std::endl;
   return(fTotalXSection);
}

//________________________________________________________________________________

void EventGenerator::AddSampler(PhaseSpaceSampler * ps)
{
   if(ps) fSamplers.Add(ps);
   else Error("AddSampler","Sampler is null!");
}

//________________________________________________________________________________

void EventGenerator::Clear(Option_t * opt)
{
   fSamplers.Clear(opt);
   fTotalXSection = 0.0;
}
//________________________________________________________________________________

PhaseSpaceSampler * EventGenerator::GetRandomSampler()
{
   if (fSamplers.GetEntries() == 0) {
      Error("GetRandomSampler","Event generator has no samplers.");
   }
   PhaseSpaceSampler * aSampler = nullptr;
   PhaseSpaceSampler * selectedSampler = nullptr;
   Double_t rand = fRandom->Uniform(fTotalXSection);
   TListIter samplerIter(&fSamplers);
   while ((aSampler = (PhaseSpaceSampler*)samplerIter.Next())) {
      rand = rand - aSampler->GetTotalXSection();
      if (rand  < 0.0) {
         selectedSampler = aSampler;
         break;
      }
   }
   return(selectedSampler);
}
//________________________________________________________________________________

TList * EventGenerator::GetSamplers()
{
   return (&fSamplers);
}
//________________________________________________________________________________

TList * EventGenerator::GetPSVariables(const char * var )
{
   auto * vars = new TList();
   TList * samps = GetSamplers();
   for(int i = 0;i<samps->GetEntries(); i++) {
      auto * aSamp = (PhaseSpaceSampler*)samps->At(i);
      DiffXSec * xsec = aSamp->GetXSec();
      if(xsec){
         PhaseSpace * ps = xsec->GetPhaseSpace();
         if( ps ) {
            PhaseSpaceVariable * aVar = ps->GetVariableWithName( var );
            if(aVar)
               vars->Add(aVar);
         }
      }
   }
   return(vars);
}
//________________________________________________________________________________

TLorentzVector EventGenerator::GetRandomVertex(Int_t i) {

   Double_t x = 0;
   Double_t y = 0;
   Double_t z = 0;
   if( i >= 0 ){
      // Use cross section's target material z position
      //fSampler->GetXSec()->GetTargetMaterial()->Dump();
      Double_t zpos = fSampler->GetXSec()->GetTargetMaterial().fZposition;
      Double_t l    = fSampler->GetXSec()->GetTargetMaterial().fLength;
      z = fRandom->Uniform(zpos-l/2.0, zpos+l/2.0);
   } else {
      // 
      z = fRandom->Uniform(-fdzVertex, fdzVertex);
   }
   if( fIsBeamRastered ) {
      fdxVertex       = fSlowRasterSize/2.0; //cm
      fdyVertex       = fSlowRasterSize/2.0; //cm
      do {
         //fRandom->Circle(x,y,fSlowRasterSize/2.0);
         x = fRandom->Uniform(-fdxVertex, fdxVertex);
         y = fRandom->Uniform(-fdyVertex, fdyVertex);

      } while ( x*x + y*y > (fSlowRasterSize/2.0)*(fSlowRasterSize/2.0)) ;
   } else {
      x = fRandom->Uniform(-fdxVertex, fdxVertex);
      y = fRandom->Uniform(-fdyVertex, fdyVertex);
   }
   TLorentzVector vertex(x,y,z,0.0);
   return(vertex);
}
//________________________________________________________________________________

bool EventGenerator::NeedsRefreshed()
{
   int res = 0;
   PhaseSpaceSampler * aSampler = nullptr;
   for (int i = 0; i < fSamplers.GetEntries(); i++) {
      aSampler = (PhaseSpaceSampler*)fSamplers.At(i);
      res += aSampler->IsModified();
      DiffXSec * aXSec = aSampler->GetXSec();
      if (aXSec) res += aXSec->IsModified();
      PhaseSpace * aPS = aXSec->GetPhaseSpace();
      if (aPS) res += aPS->IsModified();
      PhaseSpaceVariable * aPSVar = nullptr;
      for (int j = 0; j < aPS->GetDimension(); j++) {
         aPSVar = aPS->GetVariable(j);
         if (aPSVar) res += aPSVar->IsModified();
      }
   }
   fIsModified = res;
   return(res);
}
//________________________________________________________________________________

TList * EventGenerator::GenerateEvent()
{
   if( IsModified() ) Refresh();

   fSampler = GetRandomSampler();

   if(!fSampler) {
      Error("GenerateEvent","Null Sampler");
      return(nullptr);
   }

   fEventArray      = fSampler->GenerateEvent();
   fParticles       = fSampler->GetXSec()->GetParticles();
   Int_t matIndex   = fSampler->GetXSec()->GetTargetMaterialIndex(); // index of associated target material, -1 if none
   Int_t xsec_index = fSamplers.IndexOf(fSampler);

   auto  rand_vertex = GetRandomVertex(matIndex);

   // this is now done by a mandatory class DiffXSec::DefineEvent() which
   // is called from PhaseSpaceSampler::GenerateEvent();
   for (int i = 0; i < fParticles->GetEntries(); i++) {
      TParticle * apart = ((TParticle*)fParticles->At(i));
      apart->SetProductionVertex( rand_vertex );
      //anpart->SetStatusCode(matIndex);
      apart->SetFirstMother(matIndex);
      apart->SetStatusCode(xsec_index);
   }

   fEG_Event.SetPSVariables(fEventArray, fSampler->GetXSec()->GetPhaseSpace()->GetDimension());
   fEG_Event.SetParticles(fParticles);
   fEG_Event.fXSec      = fSampler->GetXSec();
   fEG_Event.fXS_id     = fSampler->GetXSec()->GetID();
   fEG_Event.fBeamPol   = fSampler->GetXSec()->GetHelicity();

   return fParticles;
}
//________________________________________________________________________________

void EventGenerator::LundFormat( std::ostream& s )
{
   LundHeaderFormat(fEG_Event,s);
   for(int i = 0; i<fEG_Event.fParticles->GetEntries(); i++ ){
      LundEventFormat(i,fEG_Event,s);
   }
}
//________________________________________________________________________________

void EventGenerator::LundHeaderFormat(
      _EG_Event& eg_event,
      std::ostream& s)
{
   // Lund format reference: https://gemc.jlab.org/gemc/Documentation/Entries/2011/3/18_The_LUND_Format.html
   // Header:
   // Column    Quantity
   //   1      Number of particles
   //   2*     Number of target nucleons
   //   3*     Number of target protons
   //   4*     Target Polarization
   //   5      Beam Polarization
   //   6*     x
   //   7*     y
   //   8*     W
   //   9*     Q2
   //   10*    nu
   s  << eg_event.fParticles->GetEntries()    << " "
      << eg_event.fXSec->GetA()              << " "
      << eg_event.fXSec->GetZ()              << " "
      << eg_event.fTargetPol                 << " "
      << eg_event.fBeamPol                   << " "
      << 0.0 /* x  */                         << " "
      << 0.0 /* y  */                         << " "
      << 0.0 /* W  */                         << " "
      << 0.0 /* Q2 */                         << " "
      << 0.0 /* nu */                         << "\n";
}
//________________________________________________________________________________
void EventGenerator::LundEventFormat(
      int           i, 
      _EG_Event& eg_event,
      std::ostream& s)
{
   // Lund format reference: https://gemc.jlab.org/gemc/Documentation/Entries/2011/3/18_The_LUND_Format.html
   // Particles:
   // Column    Quantity
   //   1      index
   //   2      charge
   //   3      type(=1 is active)
   //   4      particle id
   //   5      parent id (decay bookkeeping)
   //   6      daughter (decay bookkeeping)
   //   7      px [GeV]
   //   8      py [GeV]
   //   9      pz [GeV]
   //   10     E [GeV]
   //   11     mass (not used)
   //   12     x vertex [m]
   //   13     y vertex [m]
   //   14     z vertex [m]
   // NOTE the vertex is in METERS unlike the website above
   int iLund = i+1;
   auto * part     = (Particle*)eg_event.fParticles->At(i);
   TParticlePDG   * part_pdg = part->GetPDG();
   s << "  "
      << iLund                  << " "
      << part_pdg->Charge()/3.0 << " "
      << 1                      << " "
      << part_pdg->PdgCode()    << " "
      << 0                      << " "
      << 0                      << " "
      << part->Px()             << " "
      << part->Py()             << " "
      << part->Pz()             << " "
      << part->Energy()         << " "
      << part_pdg->Mass()       << " "
      << part->Vx()/100.0       << " "
      << part->Vy()/100.0       << " "
      << part->Vz()/100.0       << "\n";
}
//______________________________________________________________________________

}
}
