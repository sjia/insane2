#ifndef InSANE_MATH_HH
#define InSANE_MATH_HH

#include "Math/GaussIntegrator.h"
#include "Math/Integrator.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/AllIntegrationTypes.h"
#include "Math/Functor.h"
#include "Math/GaussIntegrator.h"
#include "Math/RichardsonDerivator.h"
#include "Math/Derivator.h"
#include <functional>

namespace insane {
  namespace integrate   {

    //template <class T>
    double gaus(std::function<double(double)> func, double min, double max);
    double legendre(std::function<double(double)> func, double min, double max);
    double adaptive(std::function<double(double)> func, double min, double max);
    double simple(std::function<double(double)> func, double min, double max, int N = 100);
    double simple_threads(std::function<double(double)> func, double min, double max, int N = 100, int nthreads = 4);

  }

  namespace math   {

    double simple_quad_sum(std::function<double(double)> func, double min, double max, int N = 100);

    double derivative(std::function<double(double)> func, double x);

    double StandardPhi(double phi);

  }
}
#endif

