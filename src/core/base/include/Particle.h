#ifndef Particle_HH
#define Particle_HH

#include "TParticle.h"
#include "TLorentzVector.h"

namespace insane {

/** Container class for Monte Carlo.
 *  Typically there is only one of these which are stored, but sometimes,
 *  e.g. pi0 decay, you have more than one.
 *
 * \ingroup Events
 */
class Particle : public TParticle {

   public: 
      TLorentzVector   fMomentum4Vector;
      Int_t            fHelicity;
      Int_t            fXSId; 

   public: 
      Particle();
      virtual ~Particle();

      Particle(const Particle&) = default;               // Copy constructor
      Particle(Particle&&) = default;                    // Move constructor
      Particle& operator=(const Particle&) & = default;  // Copy assignment operator
      Particle& operator=(Particle&&) & = default;       // Move assignment operato
      //virtual ~Particle() { }

      //Particle(const Particle& rhs);
      //Particle& operator=(const Particle &rhs);

      void CalculateVectors();
      void Clear(Option_t * opt = "");

      ClassDef(Particle, 6)
};

}
#endif

