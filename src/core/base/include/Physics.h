#ifndef insane_physics_HH
#define insane_physics_HH

#include "Kinematics.h"

#include <functional>

namespace insane {

  /*! @brief Physics related functions.
   *
   */
  namespace physics {

    /** \name elasticScattering 
     *  Elastic scattering
     *  @{
     */

    /** Dirac Form factor from Sachs form factors.
     * 
     * \f[  F_1 = \frac{G_E + \tau G_M}{1+\tau} \f]
     * where
     * \f$ \tau = \frac{Q^2}{4M^2} \f$.
     */
    double F1(double Q2, double GE, double GM);

    /** Pauli Form factor from Sachs form factors.
     * 
     * \f[  F_2 = \frac{G_M - G_E}{1+\tau} \f]
     * where
     * \f$ \tau = \frac{Q^2}{4M^2} \f$.
     */
    double F2(double Q2, double GE, double GM);

    /** The total squared form factor.
     * 
     * \f[ \Epsilon^{\prime}(Q^2, \theta_e) = \Big(1+2(1+\tau)\tan^2(\theta_e/2)\Big)^{-1} \f]
     * \f[ \frac{1}{(1+\tau)\Epsilon^{\prime}}\Big(\Epsilon^{\prime}(G_{E})^{2} + \tau (G_M)^2 \Big) \f]
     * where
     * \f$ \tau = \frac{Q^2}{4M^2} \f$.
     * \param sachs_FFs Sachs form factors \f$G_E\f$ and \f$G_M\f$.
     * \param Q2 \f$-q^2\f$
     * \param tan2th  \f$\tan^2(\theta_e/2)\f$ where \f$\theta_e\f$ is the electron scattering angle in collinear lab frame.
     */
    double F_total_squared(std::array<double,2> sachs_FFs,  const kinematics::ElasticKinematics& kine);

    /** Elastic cross section for a generic collinear frame.
     *
     * From http://inspirehep.net/record/895827 .
     *
     * \param FFs form factors.
     * \param Q2 \f$Q^2 \geq 0\f$
     * \param E_e electron beam energy.
     * \param E_p proton beam energy.
     * \param theta_e electron scattering angle in lab (not necessarily nucleon rest frame)
     *
     */
    double Elastic_XS(std::array<double, 2> FFs, const kinematics::ElasticKinematics& kine);

    /** @}*/

    /** \addtogroup Deep Inelastic Scattering 
     *  @{
     */

    /** R \f$ = \sigma_L/\sigma_T\f$ (ratio of L/T cross sections).
     *
     * Double_t res = F2p(x, Q2) / (2.0 * x * F1p(x, Q2)) ;
     * res = res * (1 + 4.0 * M_p/GeV * M_p/GeV * x * x / Q2) - 1.0;
     *
     */
    double R(double F1, double F2, double x, double Q2);

    /** Soffer-Teryaev  Positivity bound.
     *
     *  \f$ |A_2| < \sqrt{R(1+A_1)/2} \f$
     */
    double A2_PositivityBound(double F_1, double F_2, double A_1, double x, double Q2);
    double A2_PositivityBound(double R, double A_1, double x, double Q2);

    /** Wandzura-Wilczek function form.
     *
     *  \f$ \displaystyle g_2 = -g_1(x) + \int g_1(y)\frac{dy}{y} \f$
     *
     */
    double WW(std::function<double(double)> func, double x, double max=1.0);

    /** @}*/


    namespace TMCs {
      // Target Mass correction function
      // J. Phys. G 35, 053101 (2008)
      double h2(std::function<double(double)> F1, double xi, double Q2);
      double g2(std::function<double(double)> F1, double xi, double Q2);
    }

    /** Cornwall-Norton Moments.
     *
     * \f$ M_n = \int x^n f(x) dx \f$
     *
     * Note this is the same as a n+1 Mellin moment.
     *
     */
    double CN_moment(std::function<double(double)> f, int n, double x_low=0.0, double x_high=1.0);

    const size_t NPartons     = 13;
    const size_t NLightQuarks = 6;

    // deprecated
    enum PartonFlavor {
      kUP          = 0,
      kDOWN        = 1,
      kSTRANGE     = 2,
      kCHARM       = 3,
      kBOTTOM      = 4,
      kTOP         = 5,
      kGLUON       = 6,
      kANTIUP      = 7,
      kANTIDOWN    = 8,
      kANTISTRANGE = 9,
      kANTICHARM   = 10,
      kANTIBOTTOM  = 11,
      kANTITOP     = 12,
      kUPBAR       = kANTIUP,
      kDOWNBAR     = kANTIDOWN,
      kSTRANGEBAR  = kANTISTRANGE,
      kCHARMBAR    = kANTICHARM,
      kBOTTOMBAR   = kANTIBOTTOM,
      kTOPBAR      = kANTITOP
    };

    template <typename E>
    constexpr typename std::underlying_type<E>::type to_underlying(E e) noexcept {
      return static_cast<typename std::underlying_type<E>::type>(e);
    }

    /** Parton flavor.
    */
    enum class Parton : unsigned int {
      u    = 0,
      d    = 1,
      s    = 2,
      c    = 3,
      b    = 4,
      t    = 5,
      g    = 6,
      ubar = 7,
      dbar = 8,
      sbar = 9,
      cbar = 10,
      bbar = 11,
      tbar = 12
    };

    template <typename E = Parton>
    constexpr typename std::underlying_type<E>::type q_id(E e) noexcept {
      return static_cast<typename std::underlying_type<E>::type>(e);
    }

    constexpr std::array<PartonFlavor,NLightQuarks> LightQuarks = {
      kUP    , kDOWN    , kSTRANGE     ,
      kANTIUP, kANTIDOWN, kANTISTRANGE
    };
    constexpr std::array<double,NPartons> PartonCharge2 = { 
      4.0/9.0, 1.0/9.0, 1.0/9.0, 4.0/9.0, 1.0/9.0, 4.0/9.0,
      0.0,
      4.0/9.0, 1.0/9.0, 1.0/9.0, 4.0/9.0, 1.0/9.0, 4.0/9.0
    };

    constexpr std::array<double,NPartons> PartonCharge = { 
      2.0/3.0, -1.0/3.0, -1.0/3.0, 2.0/3.0, -1.0/3.0, 2.0/3.0,
      0.0,
      -2.0/3.0, 1.0/3.0, 1.0/3.0, -2.0/3.0, 1.0/3.0, -2.0/3.0
    };

    constexpr std::array<double,NPartons> IsoSpinConjugatePartonCharge2 = { 
      1.0/9.0, 4.0/9.0, 1.0/9.0, 4.0/9.0, 1.0/9.0, 4.0/9.0,
      0.0,
      1.0/9.0, 4.0/9.0, 1.0/9.0, 4.0/9.0, 1.0/9.0, 4.0/9.0
    };
    constexpr std::array<double,NPartons> IsoSpinConjugatePartonCharge = { 
      -1.0/3.0, 2.0/3.0, -1.0/3.0, 2.0/3.0, -1.0/3.0, 2.0/3.0,
      0.0,
      1.0/3.0, -2.0/3.0, 1.0/3.0, -2.0/3.0, 1.0/3.0, -2.0/3.0
    };

    enum class SFType {
      Unpolarized, Polarzed, Spin, FormFactor, Compton,
    };

    const size_t NStructureFunctions = 15;
    enum class SF {
      F1, F2, FL, R, W1, W2, g1, g2, gT, g2_WW, g1_BT,
    };

    const size_t NSpinStructureFunctions = 15;
    enum class SSF {
      g1,
      g2,
      gT,
      g2_WW,
      g1_BT,
    };

    const size_t NComptonAsymmetries = 16;
    enum class ComptonAsymmetry {
      A1, A2, AT, AL,
    };

    const size_t NFormFactors = 10;
    enum class FormFactor {
      GE, GM, F1, F2, GC, GQ, A, B,
    };

    enum class FragmentationType {
      D1, H1,
    };

    enum class Chirality {
      Even, Odd,
    };

    const size_t NNuclei = 7;
    enum class Nuclei {
      p,
      n,
      d,
      t,
      _3He,
      _4He,
      H     = p,
      D     = d,
      _2H   = d,
      _3H   = t,
      alpha = _4He,
      Other,
    };

    enum class TargetSpin {
      Zero,
      OneHalf,
      One,
      ThreeHalves,
      Two,
      FiveHalves,
      ThreeHalf = ThreeHalves,
      FiveHalf  = ThreeHalf,
    };

    enum class OPELimit {
      Bjorken,
      MasslessTarget,
      MasslessQuark,
      MassiveTarget,
      MassiveQuark,
      MassiveTargetAndQuark,
      TMC                    = MassiveTarget,
      Massless               = Bjorken,
      Massive                = MassiveTargetAndQuark,
    };
    enum class Twist {
      Two,
      Three,
      Four,
      TwoAndThree,
      TwoThreeAndFour,
      Leading             = Two,
      NextToLeading       = TwoAndThree,
      NextToNextToLeading = TwoThreeAndFour,
      All                 = TwoThreeAndFour,
    };

    enum class GPDType {
      H,
      Htilde,
      E,
      Etilde,
      H_T,
      Htilde_T,
      E_T,
      Etilde_T,
    };

  }

}

#endif

