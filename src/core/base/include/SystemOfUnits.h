#ifndef InSANESystemOfUnits_HH
#define InSANESystemOfUnits_HH 1

#include "TObject.h"
#include "CLHEP/Units/SystemOfUnits.h"

/*! \page SystemOfUnits   System of Units

  \section constantSources Data sources 

  System of units emulating CLHEP/Geant4 style units.

  See \ref SystemOfUnits "System of Units" for more details.

  In order to use these directly in the interpreter you must do a "using namespace CLHEP".
  The best spot for this probably your .rootlogon.C script.

  \subpage PhysicalConstants 

*/


namespace insane {

  /**  Units and constants.
   *
   */
  namespace units {

    using CLHEP::degree;
    using CLHEP::millimeter;
    using CLHEP::millimeter2;
    using CLHEP::millimeter3;
    using CLHEP::centimeter;
    using CLHEP::centimeter2;
    using CLHEP::centimeter3;
    using CLHEP::meter;
    using CLHEP::meter2;
    using CLHEP::meter3;
    using CLHEP::kilometer;
    using CLHEP::kilometer2;
    using CLHEP::kilometer3;
    using CLHEP::parsec;
    using CLHEP::micrometer;
    using CLHEP::nanometer;
    using CLHEP::angstrom;
    using CLHEP::fermi;
    using CLHEP::barn;
    using CLHEP::millibarn;
    using CLHEP::microbarn;
    using CLHEP::nanobarn;
    using CLHEP::picobarn;
    using CLHEP::mm;
    using CLHEP::um;
    using CLHEP::nm;
    using CLHEP::mm2;
    using CLHEP::mm3;
    using CLHEP::cm;
    using CLHEP::cm2;
    using CLHEP::cm3;
    using CLHEP::m;
    using CLHEP::m2;
    using CLHEP::m3;
    using CLHEP::km;
    using CLHEP::km2;
    using CLHEP::km3;
    using CLHEP::pc;
    using CLHEP::radian;
    using CLHEP::milliradian;
    using CLHEP::degree;
    using CLHEP::steradian;
    using CLHEP::rad;
    using CLHEP::mrad;
    using CLHEP::sr;
    using CLHEP::deg;
    using CLHEP::nanosecond;
    using CLHEP::second;
    using CLHEP::millisecond;
    using CLHEP::microsecond;
    using CLHEP::picosecond;
    using CLHEP::hertz;
    using CLHEP::kilohertz;
    using CLHEP::megahertz;
    using CLHEP::ns;
    using CLHEP::s;
    using CLHEP::ms;
    using CLHEP::eplus;
    using CLHEP::e_SI;
    using CLHEP::coulomb;
    using CLHEP::megaelectronvolt;
    using CLHEP::electronvolt;
    using CLHEP::kiloelectronvolt;
    using CLHEP::gigaelectronvolt;
    using CLHEP::teraelectronvolt;
    using CLHEP::petaelectronvolt;
    using CLHEP::joule;
    using CLHEP::MeV;
    using CLHEP::eV;
    using CLHEP::keV;
    using CLHEP::GeV;
    using CLHEP::TeV;
    using CLHEP::PeV;
    using CLHEP::kilogram;
    using CLHEP::gram;
    using CLHEP::milligram;
    using CLHEP::kg;
    using CLHEP::g;
    using CLHEP::mg;
    using CLHEP::watt;
    using CLHEP::newton;
    using CLHEP::hep_pascal;
    using CLHEP::bar;
    using CLHEP::atmosphere;
    using CLHEP::ampere;
    using CLHEP::milliampere;
    using CLHEP::microampere;
    using CLHEP::nanoampere;
    using CLHEP::megavolt;
    using CLHEP::kilovolt;
    using CLHEP::volt;
    using CLHEP::ohm;
    using CLHEP::farad;
    using CLHEP::millifarad;
    using CLHEP::microfarad;
    using CLHEP::nanofarad;
    using CLHEP::picofarad;
    using CLHEP::weber;
    using CLHEP::tesla;
    using CLHEP::gauss;
    using CLHEP::kilogauss;
    using CLHEP::henry;
    using CLHEP::kelvin;
    using CLHEP::mole;
    using CLHEP::becquerel;
    using CLHEP::curie;
    using CLHEP::gray;
    using CLHEP::candela;
    using CLHEP::lumen;
    using CLHEP::lux;
    using CLHEP::perCent;
    using CLHEP::perThousand;
    using CLHEP::perMillion;

    static const double minute = 60.;        // 1 min. = 60 s 
    static const double hour   = 60.*minute; // 1 hr = 60 mins 

    // Takes GeV/c to 
    static const double P_GeVOverc_SI = 5.34e-19; // 1GeV/c = 5.34×10^-19 J s/m  (joule seconds per meter)

  }
}


#endif 

