#ifndef PhaseSpaceVariable_HH
#define PhaseSpaceVariable_HH 1

#include <iostream>
#include <iomanip>
#include <vector>
#include <string>

#include "TMath.h"
#include "TNamed.h"
#include "TString.h"
#include "TList.h"

namespace insane {
  namespace physics {

    /** Base class for a phase space variable
     *
     *  The variable normalization is for FOAM. Each variable is scaled using the
     *  minima and maxima such that its scaled value falls between 0 and 1 (as required
     *  for FOAM random variables)
     *
     *  @param name The name given to refer to the variable.
     *  @param varexp The displayed expression for the varaible.
     *  This can include the ROOT style latex, eg #theta
     *
     *  \ingroup EventGen
     *  \ingroup xsections
     */
    class PhaseSpaceVariable : public TNamed {
    protected:
      bool            fIsModified     = true;
      bool            fIsDependent    = false;
      bool            fInverted       = false;
      bool            fUniform        = false;
      int             fiParticle      = -1;      // associated particle number (for events with multiple particles).
      double          fVariableMinima = 0.0;
      double          fVariableMaxima = 1.0;
      TString         fVariableUnits  = "";
      mutable double  fCurrentValue   = 0.1;

    public:
      PhaseSpaceVariable(const char * name = "" , const char * varexp = "" , Double_t min = 0.0, Double_t max = 1.0);
      PhaseSpaceVariable(const PhaseSpaceVariable& rhs);

      //PhaseSpaceVariable(const PhaseSpaceVariable&) = default;               // Copy constructor
      PhaseSpaceVariable(PhaseSpaceVariable&&) = default;                    // Move constructor
      PhaseSpaceVariable& operator=(const PhaseSpaceVariable&) & = default;  // Copy assignment operator
      PhaseSpaceVariable& operator=(PhaseSpaceVariable&&) & = default;       // Move assignment operato
      virtual ~PhaseSpaceVariable();

      Double_t GetCurrentValue() const { return fCurrentValue; }
      void     SetCurrentValue(Double_t v) const { fCurrentValue = v;}

      Double_t * GetCurrentValueAddress() { return &fCurrentValue ; }

      void  SetParticleIndex(Int_t i) { fiParticle = i; }
      Int_t GetParticleIndex() const { return fiParticle; }

      Double_t GetCentralValue() const { return((fVariableMinima + fVariableMaxima) / 2.0); }

      /** Use for azimuthal angles near the branch cut at -pi/pi */
      void SetInverted(Bool_t inv = true) { fInverted = inv; fIsModified = true; }

      void   SetUniform(Bool_t u) { fUniform = u; fIsModified = true; }
      Bool_t IsUniform() const { return fUniform; }

      void   SetDependent(Bool_t val = true) { fIsDependent = val; fIsModified = true; }
      Bool_t IsDependent() const { return fIsDependent; }

      Bool_t IsModified() const { return fIsModified; }
      void   SetModified(bool val = false) { fIsModified = val; }

      /** Note that we invert the cut for azimuthal angles centered around the
       * branch cut near phi ~ -pi or pi .
       *
       * Here we use 0 < phi < 2pi
       */
      Bool_t IsInVariableRange() const {
        //Double_t min = GetMinimum();  
        //Double_t max = GetMaximum();  
        //if( !( (fCurrentValue >= min)&&(fCurrentValue <= max) ) ){
        //   std::cout << "[PhaseSpace::IsInVariableRange]: Current value = " << fCurrentValue << std::endl;
        //   std::cout << "min: " << min << std::endl;
        //   std::cout << "max: " << max << std::endl;
        //}
        if (!fInverted) {
          return((fCurrentValue <= GetMaximum()) &&
                 (fCurrentValue >= GetMinimum()));
        } /* else it is inverted (e.g. azimuthal angle near -pi/pi cut!) */
        if (fCurrentValue > 0.0) {
          return((fCurrentValue <= GetMaximum()) &&
                 (fCurrentValue >= GetMinimum()));
        }
        if (fCurrentValue < 0.0) {
          return((fCurrentValue + 2.0 * TMath::Pi() <= GetMaximum()) &&
                 (fCurrentValue + 2.0 * TMath::Pi()  >= GetMinimum())) ;
        }
        return false;
      }

      /** Returns offset needed such that 0 < (variable-offset)/scale < 1
      */
      Double_t GetNormScale() const {
        return(fVariableMaxima - GetNormOffset());
      }

      /** Returns offset needed such that 0 < (variable-offset)/scale < 1
      */
      Double_t GetNormOffset() const {
        return(fVariableMinima);
      }

      void   SetRange(double min, double max){
        if(min>max) Warning("SetRange","min is greater than max");
        SetMinimum(min);
        SetMaximum(max);
      }

      void   SetMaximum(double val) {
        fVariableMaxima = val;
        fIsModified = true;
      }
      void   SetMinimum(double val) {
        fVariableMinima = val;
        fIsModified = true;
      }
      double GetMaximum() const {
        return(fVariableMaxima);
      }
      double GetMinimum() const {
        return(fVariableMinima);
      }
      void   SetVariableUnits(const char * unit) {
        fVariableUnits = unit;
        fIsModified = true;
      }

      const char * GetVariableUnits() const {
        return(fVariableUnits.Data());
      }

      void Print(const Option_t * opt = "") const ;

      void Print(std::ostream& stream) const ;

      void PrintTable() const {
        std::cout << " " <<  GetName() << " = " << fCurrentValue << "\n";
      }


      ClassDef(PhaseSpaceVariable, 3)
    };


    /** A discrete phase space variable such as helicity.
     *
     *  \ingroup EventGen
     *  \ingroup xsections
     */
    class DiscretePhaseSpaceVariable : public PhaseSpaceVariable {
    private:
      Int_t fNDivisions;

    public:
      DiscretePhaseSpaceVariable(const char * n = "", const char * t = "") : PhaseSpaceVariable(n,t) {
        fNDivisions = 2;
      }
      DiscretePhaseSpaceVariable(const DiscretePhaseSpaceVariable& rhs) : PhaseSpaceVariable(rhs),
      fNDivisions(rhs.fNDivisions) {}

      //PhaseSpaceVariable(const PhaseSpaceVariable&) = default;               // Copy constructor
      DiscretePhaseSpaceVariable(DiscretePhaseSpaceVariable&&) = default;                    // Move constructor
      DiscretePhaseSpaceVariable& operator=(const DiscretePhaseSpaceVariable&) & = default;  // Copy assignment operator
      DiscretePhaseSpaceVariable& operator=(DiscretePhaseSpaceVariable&&) & = default;       // Move assignment operato
      virtual ~DiscretePhaseSpaceVariable() { }

      void  SetNumberOfValues(int i) {
        fNDivisions = i;
        fIsModified = true;
      }
      Int_t GetNumberOfValues() const {
        return(fNDivisions);
      }

      /** Returns an integer value less than the set number of possible values
       *  where the range 0 to 1 is divided equally.
       *
       *  @param randomValue should be a value between 0 and 1
       */
      Int_t GetDiscreteVariable(Double_t randomValue) const {
        Double_t val = randomValue * ((Double_t)fNDivisions);
        return((Int_t)val);
      }

      ClassDef(DiscretePhaseSpaceVariable, 1)
    };

  }
}

#endif

