#ifndef PhaseSpace_H
#define PhaseSpace_H 1

#include "PhaseSpaceVariable.h"

#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <array>
#include <tuple>

#include "TMath.h"
#include "TObject.h"
#include "TNamed.h"
#include "TString.h"
#include "TFoamIntegrand.h"
#include "TList.h"

#include "Math/Vector4D.h"
#include "Math/Vector4Dfwd.h"
#include "Math/VectorUtil.h"

namespace insane {
  namespace physics {


    /** Base for phase space of a differential cross section.
     * 
     *  \f$ \frac{d^N\sigma}{dx_1 dx_2 ... dx_N} \f$
     *
     *  \ingroup xsections
     *  \ingroup EventGen
     */
    class PhaseSpace : public TObject {

    protected:
      std::vector<PhaseSpaceVariable>  fVariables;
      int                              fnDim;
      int                              fnIndependentVars;
      bool                             fIsModified;
      double                           fScaledIntegralJacobian; // For multiplying the foam integral result to get the actual integral
                                                                // this is because foam uses variables between 0 and 1 

    public:
      PhaseSpace();
      PhaseSpace(const PhaseSpace&) = default;               // Copy constructor
      PhaseSpace(PhaseSpace&&) = default;                    // Move constructor
      PhaseSpace& operator=(const PhaseSpace&) = default;  // Copy assignment operator
      PhaseSpace& operator=(PhaseSpace&&)  = default;       // Move assignment operato

      virtual ~PhaseSpace();

      void ClearVariables() {
        fnDim = 0;
        fnIndependentVars = 0;
        fVariables.clear();
        SetModified(true);
      }

      Double_t GetScaledIntegralJacobian() const { return fScaledIntegralJacobian; }

      Int_t GetNIndependentVariables() const { return fnIndependentVars; }

      Int_t GetDimension() const {
        return(fnDim) ;
      }
      void  SetDimension(Int_t dim) {
        fnDim = dim;
        SetModified(true);
      }

      /** Create a PhaseSpaceVariable and return a handle to it.
       *
       */
      PhaseSpaceVariable* CreateVariable(std::string name = "", std::string title = "", bool independent = true) {
        fVariables.push_back( PhaseSpaceVariable(name.c_str(),title.c_str()) );
        fnDim++;
        PhaseSpaceVariable* var = &(fVariables[fnDim]);
        if(independent) {
          fnIndependentVars++;
        } else {
          var->SetDependent(true);
        }
        SetModified(true);
        return var;
      }

      // makes a copy
      void AddVariable(const PhaseSpaceVariable& var) {
        fVariables.push_back(var);
        fnDim++;
        if(!var.IsDependent()) fnIndependentVars++;
        SetModified(true);
      }
      
      // Makes a copy once added
      void AddVariable(PhaseSpaceVariable* var) {
        fVariables.push_back(*var);
        fnDim++;
        if(!var->IsDependent()) fnIndependentVars++;
        SetModified(true);
      }


      void Print(Option_t * opt = "") const ;
      void Print(std::ostream& stream) const ;

      /** Returns a list of PhaseSpaceVariable pointers.
       *  Use pIndex to select a particle's variables.
       *  By default it returns a list with all phase space variables
       */
      std::vector<PhaseSpaceVariable*> GetVariables(Int_t pIndex = -1) {
        std::vector<PhaseSpaceVariable*> thelist;
        const int N  = fVariables.size();
        if (pIndex == -1) {
          for (int i = 0; i < N; i++) {
            thelist.push_back(GetVariable(i));
          }
        } else {
          for (int i = 0; i < N; i++) {
            if (GetVariable(i)->GetParticleIndex() == pIndex) thelist.push_back(GetVariable(i));
          }
        }
        return(thelist);
      }

      /** Get the ith variable added.
       *
       */
      PhaseSpaceVariable* GetVariable(Int_t i)
      {
        if (i < fnDim) return( &(fVariables.at(i)) );
        else {
          std::cout << " Index " << i << " is larger than phase space dimension, " << fnDim << ". \n";
          return(nullptr);
        }
      }

      const PhaseSpaceVariable& GetVariable(Int_t i) const {
        if (i < fnDim) return( fVariables.at(i) );
        else {
          std::cout << " Index " << i << " is larger than phase space dimension, " << fnDim << ". \n";
          return( fVariables.at(fnDim-1) );
        }
      }

      /** Get the PhaseSpaceVariable by name.
       *
       *  \note You should use theta,phi,energy,momentum,... etc when naming Phase space variables.
       *  Usually there is one particle so if it has a PS variable named "theta_e", you can
       *  just ask for "theta" and it will return the first variable to have "theta" in it.
       *  If there are two thetas then it returns the first one,
       *  and therefore you should be more explicit in your search.
       *
       */
      PhaseSpaceVariable* GetVariableWithName(const char* name) {
        const int N  = fVariables.size();
        for (int i = 0; i < N; i++) {
          TString aVarName = GetVariable(i)->GetName();
          if (aVarName.Contains(name)) return(GetVariable(i));
          /*         if( !strcmp(name,GetVariable(i)->GetName())  ) return(GetVariable(i));*/
        }
        std::cout << "  PhaseSpaceVariable named " << name << " was not found!\n";
        return(nullptr);
      }

      PhaseSpaceVariable* GetIndependentVariable(int i_ind) {
        const int N  = fVariables.size();
        int n_ind = 0;
        for (int i = 0; i < N; i++) {
          auto  var = GetVariable(i);
          if( !(var->IsDependent()) ) {
            if( n_ind == i_ind ){
              return var;
            } 
            n_ind++;
          }
        }
        return(nullptr);
      }

      /** Called by DiffXSec::Refresh() */
      void Refresh() {
        fScaledIntegralJacobian = 1.0;
        const int N  = fVariables.size();
        for (int i = 0; i < N; i++) {
          auto * aVar  = (PhaseSpaceVariable*) GetVariable(i);
          fScaledIntegralJacobian *= aVar->GetNormScale();
          aVar->SetCurrentValue( aVar->GetCentralValue() );
          aVar->SetModified(false);
        }
      }

      Double_t GetSolidAngle(Int_t ipart = 0){
        PhaseSpaceVariable * phi   = GetVariableWithName("phi");
        PhaseSpaceVariable * theta = GetVariableWithName("theta");
        if(!phi)   return 0.0;
        if(!theta) return 0.0;
        Double_t deltaPhi   = phi->GetMaximum() - phi->GetMinimum();
        Double_t deltaTheta = -1.0*(TMath::Cos(theta->GetMaximum()) - TMath::Cos(theta->GetMinimum()));
        return deltaPhi*deltaTheta;
      }
      Double_t GetEnergyBite(Int_t ipart = 0){
        PhaseSpaceVariable * energy = GetVariableWithName("energy");
        Double_t deltaE   = energy->GetMaximum() - energy->GetMinimum();
        return deltaE;
      }

      void ListVariables()  {
        std::cout << " Variables in phase space are:\n";
        const int N = fVariables.size(); 
        for (int i = 0; i < N; i++) {
          std::cout << GetVariable(i)->GetName() << "\n";
        }
        std::cout << "\n";
      }

      /** Sets the values of each PS variable. Is called from DiffXSec::VariablesInPhaseSpace().
       *  The dimension vals should be of dimension fNParticles*3
       */
      void SetEventValues(const Double_t * vals, int nVars=0) const {
        int N = fVariables.size(); 
        if(nVars > 0) {
          N= nVars;
        }
        for (int i = 0; i < N; i++) {
          GetVariable(i).SetCurrentValue(vals[i]);
        }
      }

      /** Returns the array filled with the current values of each phase space
       *  variable.
       */
      void GetEventValues(double * vals) const {
        const int N = fVariables.size();
        if(N>0){ 
          for (int i=0;i<N;i++) {
            vals[i] = GetVariable(i).GetCurrentValue();
          }
        }else{
          std::cout << "[PhaseSpace::GetEventValues]: WARNING!  ";
          std::cout << "There seems to be no phase space variables..." << std::endl;
        }

      }

      void PrintTableVariables() {
        const int N = fVariables.size();
        for (int i = 0; i < N; i++) {
          GetVariable(i)->PrintTable();
        }
      }

      bool IsModified() const {
        return fIsModified;
      }
      void SetModified(bool val = true) {
        fIsModified = val;
      }



      ClassDef(PhaseSpace,3)
    };

  }
}

#endif


