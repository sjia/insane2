#ifndef insane_physics_InSANEDiffXSec_H
#define insane_physics_InSANEDiffXSec_H 1

#include <ostream>
#include <vector>
#include <array>
#include <tuple>

#include "TFoam.h"
#include "TFoamIntegrand.h"
#include "TMath.h"
#include "TVector3.h"
#include "Math/IFunction.h"
#include "Math/Functor.h"
#include "TString.h"
#include "Math/Integrator.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/AllIntegrationTypes.h"
#include "Math/GaussIntegrator.h"

#include "Nucleus.h"
#include "TargetMaterial.h"
#include "Particle.h"
#include "PhaseSpace.h"
//#include "InitialState.h"

//#include "BETAG4MonteCarloEvent.h"
#include "FormFactors.h"
#include "StructureFunctions.h"
#include "PolarizedStructureFunctions.h"

#include "Math/Vector4D.h"
#include "Math/Vector4Dfwd.h"
#include "Math/VectorUtil.h"
//#include "Math/LorentzVector.h"
//#include "Jacobians.h"

namespace insane {
  namespace physics {

    //class FormFactors;
    //class StructureFunctions;
    //class PolarizedStructureFunctions;

    /**  Abstract base class for a Differential Cross Section.
     *  \f$ \frac{d^N\sigma}{dx_1 dx_2 ... dx_N}  \f$
     *
     *  Note, that the solid angle \f$ d\Omega = sin(\theta)d\theta d\phi \f$
     *  requires that the flagg fIncludeJacobian be set to true to include the
     *  \f$sin(\theta)\f$ factor in calculated result.
     *
     *  Units used:
     *    - GeV
     *    - radian
     *    - steradian
     *    - nanobarn
     *
     *  Methods to override:
     *    - EvaluateXSec
     *    - InitPhaseSpace
     *    - GetDependentVariables
     *
     *  See xsec_ids.md for XS ID numbering convetion.
     *
     *  Since we are often using natural units to do a calculation, i.e. h=1 and c=1,
     *  we need to multiply cross sections by 
     *  (hc)^2=(197.33 MeV (-fm))^2 = 0.038939129 GeV^2 fm^2 =
     *   0.00038939129 GeV^2 barn =  0.38939129 GeV^2 mbarn
     *
     * \ingroup EventGen
     * \ingroup xsections
     *
     */
    class DiffXSec : public TFoamIntegrand, public ROOT::Math::IBaseFunctionMultiDim {

      private:
        Bool_t                fIncludeJacobian;
        Bool_t                fIsModified;
        Bool_t                fUsePhaseSpace;  // When false, xsec evaluation should ignore values of phase space limits
        PhaseSpace            fPhaseSpace;     //
        mutable Double_t      fVars[30];       //->

      protected:
        Int_t                 fID;              ///< ID unique to the cross section. Used in e.g. for identifying source of thrown event.
        Int_t                 fBaseID;          ///< ID which is used to set fID to differentiate cross sections for final state particles
        std::vector<Int_t>    fPIDs;            ///< PIDs of final state particles, should have fnParticles entries
        Int_t                 fnDim;            ///< Dimension of differential.
        Int_t                 fnParticles;      ///< Number of particles in the final state.
        Int_t                 fnPrimaryParticles; ///< Number of particles printed to lund file.
        std::vector<Int_t>    fnParticleVars;   ///< Number of PS variables associated with particle
        TList                 fAddedXSections;  ///< Currently not used!!
        TList                 fParticles;       ///< List of particles that are thrown for reaction.  
        TList                 fVariableNames;   ///< List of variable names 
        Double_t              fBeamEnergy;      ///< Incoming beam energy 
        Double_t              fTotalXSec;       ///< Total cross-section over phase space.
        Double_t              fHelicity;        ///< Beam helicity, 0 if unpolarized
        TVector3              fTargetPol;       ///< Target Polariztion vector (in the lab)

        Nucleus               fTargetNucleus;   ///< Isotope used to calculate cross section 
        TargetMaterial        fTargetMaterial;  ///< Target material associated with cross section
        Int_t                 fMaterialIndex;   ///< Target material index associated with cross section 

        std::vector<Double_t> fScaleFactors;    ///< Scale factors are set so that the random variables fall between 0 and 1
        ///< Scale factor for multilying random variable such that it is less than 1.
        std::vector<Double_t> fOffsets;         ///< Off sets for negative numbers

        mutable Double_t      fNormalizedVariables[30];   //-> should be more clever here
        mutable Double_t      fUnnormalizedVariables[30]; //-> should be more clever here
        mutable Double_t      fDependentVariables[30];    //-> should be more clever here

        TString           fTitle;
        TString           fPlotTitle;
        TString           fLabel;
        TString           fUnits;

        //InitialState      fInitialState;
        //FrameOfReference  fFrame;

        StructureFunctions           * fStructureFunctions;           //!
        PolarizedStructureFunctions  * fPolarizedStructureFunctions;  //!
        FormFactors                  * fFormFactors;                  //!

      public: 
        DiffXSec();
        DiffXSec(const DiffXSec& old);
        DiffXSec& operator=(const DiffXSec& old);

        //DiffXSec(const DiffXSec&) = default;               // Copy constructor
        DiffXSec(DiffXSec&&) = default;                    // Move constructor
        //DiffXSec& operator=(const DiffXSec&) & = default;  // Copy assignment operator
        DiffXSec& operator=(DiffXSec&&) & = default;       // Move assignment operato

        virtual ~DiffXSec();

        virtual void SetTitle(const char * t){ fTitle = t;}
        const char * GetTitle() const {return( fTitle.Data());}
        virtual void SetPlotTitle(const char * t){ fPlotTitle = t;}
        const char * GetPlotTitle() const {return( fPlotTitle.Data());}
        virtual void SetLabel(const char * t){ fLabel = t;}
        const char * GetLabel(){ return(Form("%s (%s)",fLabel.Data(),fUnits.Data() )); }
        virtual void SetUnits(const char * t){ fUnits = t;}
        const char * GetUnits() const {return( fUnits.Data());}

        Int_t        GetID() const { return fID; }

        virtual void UsePhaseSpace(Bool_t val = true) {fUsePhaseSpace = val; }

        virtual void           SetTargetMaterial(const TargetMaterial& mat){ fTargetMaterial = mat; fTargetNucleus = fTargetMaterial.GetNucleus(); }
        const TargetMaterial&  GetTargetMaterial() const {return fTargetMaterial;}

        virtual void          SetTargetMaterialIndex(Int_t i){ fMaterialIndex = i ; }
        Int_t                 GetTargetMaterialIndex() const {return fMaterialIndex;}

        /** A method that should be overriden which creates a phase space and phase
         *  space variables.
         *  It should end with SetPhaseSpace() which sets the member
         *  fPhaseSpace and does some checks.
         */
        virtual void InitializePhaseSpaceVariables() {}

        /** Virtual method to define the random event from the variables provided.
         *  This is the transition point between the phase space variables and
         *  the particles. The argument should be the full list of variables returned from
         *  GetDependentVariables. 
         */
        virtual void DefineEvent(Double_t * vars ){}

        /** Clone the cross section. Note: you should provide both versions of clone
         *  with the 0 argument version that calls the 1 arg version.
         */ 
        virtual DiffXSec*  Clone(const char * newname) const ;
        virtual DiffXSec*  Clone() const { return( Clone("") ); } 

        /** Creates an instance of Particle for each final state particle.
         *  Using the array fPIDs it sets the appropriate PDG encoding for
         *  each final state particle.
         */
        virtual void InitializeFinalStateParticles();

        /** The list of final state particles.   */
        TList * GetParticles();
        TParticlePDG * GetParticlePDG(int i = 0) const;

        /** Returns the number of PS vars for the ith particle. */
        Int_t   GetNParticleVars(unsigned int i=0){ if( i>=fnParticleVars.size() ) return(0); return(fnParticleVars[i]);}

        const Nucleus& GetTargetNucleus() const { return fTargetNucleus; }
        virtual void SetTargetNucleus(const Nucleus & targ){ fTargetNucleus = targ; }

        Double_t GetZ() const { return( Double_t( fTargetNucleus.GetZ() )) ;}
        Double_t GetA() const { return( Double_t( fTargetNucleus.GetA() )) ;}
        Double_t GetN() const { return( Double_t( fTargetNucleus.GetN() )) ;}


        Double_t           GetHelicity() const { return fHelicity; }
        virtual void       SetHelicity(Double_t h) { 
          // make sure to add call to this in override
          if(TMath::Abs(h) > 1.0){
            Error("SetHelicity","Argument too big: |h|<=1.");
          }else{ 
            fHelicity = h;
          }
        }
        const TVector3& GetTargetPolarization(){ return fTargetPol; }
        virtual void SetTargetPolarization(const TVector3 &P){
          // make sure to add call to this in override
          fTargetPol = P; 
          if(P.Mag() > 1.0) {
            Error("SetTargetPolarization","Magnitude too big. Setting to 1.");
            fTargetPol.SetMag(1.0);
          }
        }

        virtual void SetTargetPolDirection(Double_t theta, Double_t phi = 0.0){
          TVector3 pol = fTargetPol;
          if(pol.Mag() == 0.0)  {
            Warning("SetTargetPolDirection","Target Polarization was 0. Setting to 1.");
            pol.SetMag(1.0);
          }
          pol.SetTheta(theta);
          pol.SetTheta(phi);
          SetTargetPolarization(pol);
        }
        virtual void SetPolarizations(Double_t pe, Double_t pt){
          // make sure to add call to this in override or repeat this
          TVector3 pol = fTargetPol;
          pol.SetMag(pt);
          //fHelicity = pe;
          SetHelicity(pe);
          SetTargetPolarization(pol);
        }

        /** Includes sin(theta) term for solid angle integration. 
         *  This is set automatically when the cross section is added to a phase space sampler,
         *  otherwise it should be ignored.
         */
        Bool_t IncludeJacobian() const { return fIncludeJacobian ; }
        void   SetIncludeJacobian(Bool_t v = true){ fIncludeJacobian = v; }

        /** Virtual method that returns the calculated values of dependent variables. This method
         *  should be overridden for exclusive cross sections in order to conserve momentum and energy!
         *  This is called from Density(const Double_t *y), where argument is an array  
         *  independent random variables only. 
         *
         *  For example, in mott scattering (elastic) there is really only two random variables,
         *  the the scattered angles theta and phi. The rest can be calculated from these two angles
         *  (assuming the beam energy is known).
         *
         *  This implementation assumes that all variables are independent.
         */
        virtual Double_t * GetDependentVariables(const Double_t * x) const;

        const Double_t* DependentVariables() const { return fDependentVariables; }

        /** @name Evaluating the cross section
         *  Various methods used to evaluate the cross sections.
         *  Some are required by base classes some are just convenient
         *  @{
         */

        /** The main virtual method which implements the cross section. 
         *  
         *  The argument array should be passed as the results of GetDependentVariables() (especially for
         *  exclusive processes. 
         */
        virtual Double_t EvaluateXSec(const Double_t * x) const ;

        /** Calculate the Jacobian determinant.
         *  Returns the jacobian needed to convert the cross section 
         *  into the right form for integrating over the independent
         *  variables for the particles defined in the phase space.
         *  Note: make sure the jacobian is used if IncludeJacobian
         *  is set, otherwise, using the cross section with an event
         *  generator will produce the wrong total cross section.
         *  Takes the same arguments as EvaluateXSec which is the
         *  result of GetDependentVariables.
         */
        virtual Double_t Jacobian(const Double_t * x) const;

        virtual Double_t GetBeamSpinAsymmetry(const Double_t * x) const { return 0.0; }

        /** Used to evaluate the base cross section. 
         *  Useful for radiative corrections where EvaluteXSec implements 
         *  methods that want to call the base. Override this so RADCOR and POLRAD
         *  Use the base class.
         */
        virtual Double_t EvaluateBaseXSec(const Double_t * x) const { return EvaluateXSec(x); }

        /**  Pure virtual method from TFoamIntegrand method used by TFoam.
         *   \f$  \frac{d^3\sigma}{d\theta d\phi d\omega}  \f$
         *   DiffXSec::Density() is sampled by TFoam with  0 < x < 1
         *   and dimension equal to the differntial dimension (not the number of phase space variables).
         */
        virtual Double_t Density(Int_t ndim, Double_t * x) ;

        /** Evaluates the cross section with the current values of
         *  the phase space variables.
         */
        virtual Double_t EvaluateCurrent() const ;

        double       DoEval(const double* x) const { return (double)EvaluateXSec((Double_t*) x) ; }
        double       operator()(double *x, double *p)  { return (double)EvaluateXSec((Double_t*) x); }
        unsigned int NDim() const { return fnDim; }

        //@}

        /** Refresh the cross section.
         *  Recalculates the scale factors for each variable, which puts the
         *  values between 0 and 1 (as required for TFoam).
         *  Called from PhaseSpaceSampler::Refresh().
         */
        void Refresh();

        /** Used when object is read from disk.
         *  If there are special initializations that need to be done for persistency, this method should
         *  be implemented.
         */ 
        virtual Int_t InitFromDisk(){ return 0; }

        /** Sets the phase space and scale factors.
         *  The argument PS object is owned by this class.
         *  Make sure phase space has had all variables added prior to adding.
         *  Initializes the final state particles ( see InitializeFinalStateParticles() )
         */
        virtual void   SetPhaseSpace(PhaseSpace * ps);

        PhaseSpace*    GetPhaseSpace() { return(&fPhaseSpace); }
        const PhaseSpace* const   GetPhaseSpace() const { return(&fPhaseSpace); }

        /** Sets the integral of the XSection over the phase space*/
        void     SetTotalXSec(Double_t norm) {  fTotalXSec = norm; }
        Double_t GetTotalXSec(){ return fTotalXSec;}

        Double_t GetPDFNormalization() const { return(fTotalXSec); }        ///< Deprecated
        Double_t GetIntegratedCrossSection() const { return(fTotalXSec); }
        Double_t CalculateTotalCrossSection();

        /** Set Beam Energy in units of GeV */
        virtual void      SetBeamEnergy(Double_t  ebeam)  { fBeamEnergy = ebeam; fIsModified = true; }
        virtual Double_t  GetBeamEnergy() const { return(fBeamEnergy); }

        /** Returns the variables with ther physical values. */
        Double_t * GetUnnormalizedVariables(const Double_t * x);

        /** Returns the variables scaled between 0 and 1 (for TFoam). */
        Double_t * GetNormalizedVariables(const Double_t * x);

        /** Returns true if the set of variables fall within the phase space.
         *  See DiffXSec::SetPhaseSpace().
         *  Calls SetEventValues() which takes the raw array of values and sets the
         *  variable's datamember, fCurrentValue. 
         */
        bool VariablesInPhaseSpace(const Int_t ndim, const Double_t * x) const;

        virtual void Print(std::ostream &stream) const ;
        virtual void Print(const Option_t * opt = "") const ; // *MENU*
        virtual void PrintTable();                  // *MENU*
        virtual void PrintTable(std::ostream &out);
        virtual void PrintGrid(std::ostream &out,Int_t N = 20);

        /** Returns the PDG particle encoding.
         *  Note: For multi particle production cross sections you should use
         *        the argument to get that particles PDG encoding.
         */
        Int_t GetParticleType(Int_t part = 0) const ;

        virtual void SetParticleType( Int_t pdgcode, Int_t part = 0) ;
        virtual void SetProductionParticleType( Int_t PDGcode, Int_t part = 0);

        void PrintFinalStatePIDs() const ;

        bool IsModified() const { return fIsModified; }
        void SetModified(bool val = false) { fIsModified = val; }


        /** Set the unpolarized structure functions to be used to calculate W1,W2,F1,F2, etc...  */
        virtual void SetUnpolarizedStructureFunctions(StructureFunctions * sf)  { fStructureFunctions = sf; }
        StructureFunctions *  GetUnpolarizedStructureFunctions() const { return(fStructureFunctions); }

        /** Set the polarized structure functions to be used to calculate G1,G2,g1,g2, etc...  */
        virtual void SetPolarizedStructureFunctions(PolarizedStructureFunctions * sf) { fPolarizedStructureFunctions = sf; }
        PolarizedStructureFunctions *  GetPolarizedStructureFunctions() const { return(fPolarizedStructureFunctions); }

        /** Set the form factors */
        virtual void SetFormFactors(FormFactors * ff) { fFormFactors = ff; }
        FormFactors *  GetFormFactors() const { return(fFormFactors); }

        ClassDef(DiffXSec,6)
    };

  }
}

#endif

