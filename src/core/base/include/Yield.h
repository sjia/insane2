#ifndef Yield_HH
#define Yield_HH

#include "Luminosity.h"
#include "Target.h"
#include "TargetMaterial.h"
#include "DiffXSec.h"

namespace insane {
namespace physics {

/** Calculte the Yield from a luminosity and cross section.
 *
 *  Note: units are A, seconds, g, cm, and g/cm^3
 *  The cross section units are nb
 */
class Yield : public Luminosity {

   protected:
      TList              fXSections;
      DiffXSec   * fXsec;
      //Luminosity * fLumi;

   public : 
      Yield(Target * targ = nullptr, Double_t current = 0.0);
      virtual ~Yield();

      void AddCrossSection(DiffXSec * xs);
      void SetCrossSection(DiffXSec * xs) { fXsec = xs;};
      void SetLuminosity(DiffXSec * xs) { fXsec = xs;};

      // Calculate the rate for the ith material 
      Double_t CalculateRate(Int_t i, Double_t * x , int pdgcode = 0);

      // Calculate the total rate for particle type 
      Double_t CalculateRate(Double_t * x, int pdgcode = 0);

      Double_t CalculateTotalRate(Int_t i, Double_t * x , int pdgcode = 0);
      Double_t CalculateTotalRate(Double_t * x, int pdgcode = 0);

      ClassDef(Yield,2)
};

}
}

#endif

