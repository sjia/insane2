#ifndef ExclusiveDiffXSec_H
#define ExclusiveDiffXSec_H 1

#include "DiffXSec.h"
#include "TFoamIntegrand.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TParticlePDG.h"
#include "TDatabasePDG.h"
#include "TVector3.h"


namespace insane {
namespace physics {
/**  Base class for an Exclusive Differential Cross Section.
 *   Examples include the Mott cross section, (e,e'p) elastic scattering, exclusive meson productions etc...
 *   Uses units of GeV, radians, and nanobarns.
 *
 * \ingroup exclusiveXSec
 */
class ExclusiveDiffXSec : public DiffXSec {
   protected:

      //Double_t * fTheta_p; //!
      //Double_t * fPhi_p; //!
      //Double_t * fEnergy_p; //!

   public:
      ExclusiveDiffXSec() ;
      virtual ~ExclusiveDiffXSec();

      ExclusiveDiffXSec(const ExclusiveDiffXSec& old) : DiffXSec(old) { (*this) = old; }

      ExclusiveDiffXSec& operator=(const ExclusiveDiffXSec& old) {
         if (this != &old) {  // make sure not same object
            this->DiffXSec::operator=(old);
         }
         return *this;    // Return ref for multiple assignment
      }
      //ExclusiveDiffXSec(const ExclusiveDiffXSec&) = default;               // Copy constructor
      ExclusiveDiffXSec(ExclusiveDiffXSec&&) = default;                    // Move constructor
      //ExclusiveDiffXSec& operator=(const ExclusiveDiffXSec&) & = default;  // Copy assignment operator
      ExclusiveDiffXSec& operator=(ExclusiveDiffXSec&&) & = default;       // Move assignment operato
      //virtual ~ExclusiveDiffXSec() { }

      /** \todo how to add cross sections ? */
      ExclusiveDiffXSec operator+ (ExclusiveDiffXSec& right) {
         ExclusiveDiffXSec temp;
         return (temp);
      }

      /** Virtual method that returns the calculated values of dependent variables. This method
       *  should be overridden for exclusive cross sections in order to conserve momentum and energy!
       *  This is called from Density(const Double_t *y), where argument is an array of only the 
       *  independent random variables. 
       *
       *  For example, in mott scattering (elastic) there is really only two random variables,
       *  the the scattered angles theta and phi. The rest can be calculated from these two angles
       *  (assuming the beam energy is known).
       *
       */
      virtual Double_t * GetDependentVariables(const Double_t * x) const {
         std::cout << "ExclusiveDiffXSec::GetDependentVariables" << std::endl;
         TVector3 k1(0, 0, fBeamEnergy); // incident electron
         TVector3 k2(0, 0, 0);          // scattered electron
         double eprime = fBeamEnergy / (1.0 + 2.0 * fBeamEnergy / M_p/GeV * TMath::Power(TMath::Sin(x[0] / 2.0), 2)) ;
         k2.SetMagThetaPhi(eprime, x[0], x[1]);
         TVector3 p2 = k1 - k2; // recoil proton
         fDependentVariables[0] = eprime;
         fDependentVariables[1] = x[0];
         fDependentVariables[2] = x[1];
         fDependentVariables[3] = p2.Mag();
         fDependentVariables[4] = p2.Theta();
         fDependentVariables[5] = p2.Phi();
         return(fDependentVariables);
      }

      virtual void DefineEvent(Double_t * vars);

      /**  TFoamIntegrand method used by TFoam.
       *   Density() is sampled by TFoam with  0 < x < 1
       *     \frac{d^3\sigma}{d\theta d\phi d\omega}
       */
      virtual Double_t Density(Int_t ndim, Double_t * x) {
         return(EvaluateXSec(GetDependentVariables(GetUnnormalizedVariables(x))));
      }

      /** Initialize a phase space for (e,e'p) */
      virtual void InitializePhaseSpaceVariables();

      /** Virtual Method used to evaluate cross section */
      virtual Double_t EvaluateXSec(const Double_t * x) const;

      /**  Needed by ROOT::Math::IBaseFunctionMultiDim. */
      unsigned int NDim() const { return fnDim; }

      /** Needed by ROOT::Math::IBaseFunctionMultiDim. */
      double DoEval(const double * x) const { return (double)EvaluateXSec((Double_t*) x); }

      virtual ExclusiveDiffXSec*  Clone(const char * newname) const {
         std::cout << "ExclusiveDiffXSec::Clone()\n";
         auto * copy = new ExclusiveDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual ExclusiveDiffXSec*  Clone() const { return( Clone("") ); } 

      /** Set the unpolarized structure functions to be used to calculate W1,W2,F1,F2, etc...
      */
      void SetUnpolarizedStructureFunctions(StructureFunctions * sf)  { fStructureFunctions = sf; }
      StructureFunctions *  GetUnpolarizedStructureFunctions()  { return(fStructureFunctions); }

      /** Set the polarized structure functions to be used to calculate G1,G2,g1,g2, etc...
      */
      void SetPolarizedStructureFunctions(PolarizedStructureFunctions * sf) { fPolarizedStructureFunctions = sf; } 
      PolarizedStructureFunctions *  GetPolarizedStructureFunctions()  { return(fPolarizedStructureFunctions); }


      ///** @name Useful functions for plotting...
      // *  @{
      // */
      ///** For using with TF1 functions */
      //virtual double EnergyDepXSec(double *x, double *p) {
      //   Double_t y[3];
      //   y[0] = x[0];
      //   y[1] = p[0];
      //   y[2] = p[1];
      //   return(EvaluateXSec(GetDependentVariables(y)));
      //}

      ///** For using with TF1 functions. */
      //virtual double EnergyFractionDepXSec(double *x, double *p) {
      //   Double_t y[3];
      //   y[0] = x[0] * (fBeamEnergy);
      //   y[1] = p[0];
      //   y[2] = p[1];
      //   return(EvaluateXSec(GetDependentVariables(y)));
      //}

      ///** Cross section as a function of energy.
      // *  x[0] = energy
      // *  p[0] = theta
      // *  p[1] = phi
      // */
      //virtual double EnergyDependentXSec(double *x, double *p) {
      //   Double_t y[3];
      //   y[0] = x[0];
      //   y[1] = p[0];
      //   y[2] = p[1];
      //   return(EvaluateXSec(GetDependentVariables(y)));
      //}

      ///** Cross section as a function of momentum.
      // *  x[0] = momentum
      // *  p[0] = theta
      // *  p[1] = phi
      // */
      //virtual double MomentumDependentXSec(double *x, double *p) {
      //   Double_t y[3];
      //   y[0] = x[0];
      //   y[1] = p[0];
      //   y[2] = p[1];
      //   return(EvaluateXSec(GetDependentVariables(y)));
      //}

      ///** Cross section as a function of polar angle .
      // *  x[0] = theta
      // *  p[0] = energy
      // *  p[1] = phi
      // */
      //virtual double PolarAngleDependentXSec(double *x, double *p) {
      //   Double_t y[3];
      //   y[0] = x[0];
      //   y[1] = p[0];
      //   y[2] = p[0];
      //   return(EvaluateXSec(GetDependentVariables(y)));
      //}

      ///** Cross section as a function of polar angle .
      // *  x[0] = phi
      // *  p[0] = energy
      // *  p[1] = theta
      // */
      //virtual double AzimuthalAngleDependentXSec(double *x, double *p) {
      //   Double_t y[3];
      //   y[0] = p[0];
      //   y[1] = p[1];
      //   y[2] = x[0];
      //   return(EvaluateXSec(GetDependentVariables(y)));
      //}

      ////@}

      ClassDef(ExclusiveDiffXSec, 1)
};



/** A flat exclusive cross section for (e,e'p)
 *
 * \ingroup exclusiveXSec
 */
class FlatExclusiveDiffXSec : public ExclusiveDiffXSec {
   public:
      FlatExclusiveDiffXSec() { }
      virtual ~FlatExclusiveDiffXSec() { }
      virtual FlatExclusiveDiffXSec*  Clone(const char * newname) const {
         std::cout << "FlatExclusiveDiffXSec::Clone()\n";
         auto * copy = new FlatExclusiveDiffXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual FlatExclusiveDiffXSec*  Clone() const { return( Clone("") ); } 


      /** Virtual method that returns the calculated values of dependent variables. This method
       *  should be overridden for exclusive cross sections in order to conserve momentum and energy!
       *
       *  For example, in mott scattering (elastic) there is really only two random variables,
       *  the the scattered angles theta and phi. The rest can be calculated from these two angles
       *  (assuming the beam energy is known).
       *
       */
      virtual Double_t * GetDependentVariables(const Double_t * x) const  {
         // A faster way would be to solve the kinematics and plug the solutions in explicitly.
         TVector3 k1(0, 0, fBeamEnergy); // incident electron
         TVector3 k2(0, 0, 0);           // scattered electron
         double eprime = fBeamEnergy / (1.0 + 2.0 * fBeamEnergy / M_p/GeV * TMath::Power(TMath::Sin(x[0] / 2.0), 2)) ;
         k2.SetMagThetaPhi(eprime, x[0], x[1]);
         TVector3 p2 = k1 - k2; // recoil proton
         fDependentVariables[0] = eprime;
         fDependentVariables[1] = x[0];
         fDependentVariables[2] = x[1];
         fDependentVariables[3] = p2.Mag();
         fDependentVariables[4] = p2.Theta();
         fDependentVariables[5] = p2.Phi();
         p2.Print();
         k2.Print();
         return(fDependentVariables);
      }


      /** Evaluate Cross Section. Flat cross section returns 1 */
      virtual Double_t EvaluateXSec(const Double_t * x) const;

      ClassDef(FlatExclusiveDiffXSec, 1)
};



/** Exclusive Mott Cross Section for scattering off a particle with proton mass.
 *  The mott cross section describes scattering from a point-like spinless particle.
 *
 * \ingroup exclusiveXSec
 */
class ExclusiveMottXSec : public ExclusiveDiffXSec {
   public:
      ExclusiveMottXSec() { }

      virtual ~ExclusiveMottXSec() { }
      virtual ExclusiveMottXSec*  Clone(const char * newname) const {
         std::cout << "ExclusiveMottXSec::Clone()\n";
         auto * copy = new ExclusiveMottXSec();
         (*copy) = (*this);
         return copy;
      }
      virtual ExclusiveMottXSec*  Clone() const { return( Clone("") ); } 

      /**  Needed by ROOT::Math::IBaseFunctionMultiDim */
      unsigned int NDim() const {
         return fnDim;
      }

      /**  Returns the scattered electron energy using the angle.
      */
      Double_t GetEPrime(const Double_t theta)const  {
         return (fBeamEnergy / (1.0 + 2.0 * fBeamEnergy / M_p/GeV * TMath::Power(TMath::Sin(theta / 2.0), 2)));
      }

      /** Evaluate Cross Section (mbarn/sr) */
      virtual Double_t EvaluateXSec(const Double_t * x) const;


      ClassDef(ExclusiveMottXSec, 1)
};

}
}

#endif

