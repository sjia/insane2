#ifndef RadiativeCorrections_HH
#define RadiativeCorrections_HH 1

#include <iostream>
#include "TNamed.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TVector3.h"
#include "TBrowser.h"
#include "AveragedKinematics.h"
#include "PhaseSpace.h"
#include "InclusiveDiffXSec.h"
#include "POLRADInternalPolarizedDiffXSec.h"
#include "POLRADBornDiffXSec.h"
#include "POLRADInelasticTailDiffXSec.h"
#include "POLRADQuasiElasticTailDiffXSec.h"
#include "POLRADElasticTailDiffXSec.h"
#include "InelasticRadiativeTail.h"
#include "ElasticRadiativeTail.h"

namespace insane {
namespace physics {

/** Class to handle radiative corrections to binned data.  
 */ 
class RadiativeCorrections1D : public TNamed {

   protected:
      PhaseSpace * fPolarizedPhaseSpace; //!
      PhaseSpace * fPhaseSpace         ; //!

   public:
      InclusiveDiffXSec * fBorn0 ; //!
      InclusiveDiffXSec * fBorn1 ; //!
      InclusiveDiffXSec * fBorn2 ; //!
      InclusiveDiffXSec * fERT0_rt ; //!
      InclusiveDiffXSec * fERT1_rt ; //!
      InclusiveDiffXSec * fERT2_rt ; //!
      InclusiveDiffXSec * fInelasticXSec0_rt ; //!
      InclusiveDiffXSec * fInelasticXSec1_rt ; //!
      InclusiveDiffXSec * fInelasticXSec2_rt ; //!
      InclusiveDiffXSec * fDiffXSec03 ; //!
      InclusiveDiffXSec * fDiffXSec13 ; //!
      InclusiveDiffXSec * fDiffXSec23 ; //!

   public:

      TH1F * fSigmaBorn;       //->
      TH1F * fSigmaBorn_Plus;  //->
      TH1F * fSigmaBorn_Minus; //->
      TH1F * fDeltaBorn;       //->
      TH1F * fAsymBorn;        //->

      TH1F * fSigmaERT;        //->
      TH1F * fSigmaERT_Plus;   //->
      TH1F * fSigmaERT_Minus;  //->
      TH1F * fDeltaERT;        //->
      TH1F * fAsymERT;         //->

      TH1F * fSigmaIRT;        //->
      TH1F * fSigmaIRT_Plus;   //->
      TH1F * fSigmaIRT_Minus;  //->
      TH1F * fDeltaIRT;        //->
      TH1F * fAsymIRT;         //->

      Double_t fThetaTarget;
      Double_t fBeamEnergy;

      AveragedKinematics1D * fAvgKine; //->

      Int_t InitHist(const TH1F * h0, TH1F ** h1, const char *n = "");

   public:

      RadiativeCorrections1D(const char * n="", const char * t="");
      virtual ~RadiativeCorrections1D();

      Bool_t         IsFolder() const { return kTRUE; }
      void           Browse(TBrowser* b);

      void SetAvgKine(AveragedKinematics1D * kine){ fAvgKine = kine; }
      AveragedKinematics1D* GetAvgKine(){ return fAvgKine; }

      void      SetBeamEnergy(Double_t en){ fBeamEnergy = en; }
      Double_t  GetBeamEnergy(){ return fBeamEnergy; }

      void      SetThetaTarget(Double_t en){ fThetaTarget = en; }
      Double_t  GetThetaTarget(){ return fThetaTarget; }

      Int_t CreateHistograms(TH1F * h0);         ///< Uses the supplied histogram to create the binning. 
      virtual Int_t InitCrossSections();
      virtual Int_t Calculate();

      void PrintConfiguration(std::ostream& s = std::cout );
      ClassDef(RadiativeCorrections1D,1)
};
}}

#endif

