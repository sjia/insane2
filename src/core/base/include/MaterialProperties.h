#ifndef insane_materialproperties_HH
#define insane_materialproperties_HH

namespace insane {

  namespace materials {

    // Common target densities
    static const double LH2_density  = 0.07085; // g/cm3
    static const double LD2_density  = 0.1711;  // g/cm3
    static const double L4He_density = 0.1450;  // g/cm3
    static const double LN2_density  = 0.8116;
    static const double Al_density   = 2.7;
    static const double Cu_density   = 8.96;
    static const double Ni_density   = 8.908;
    static const double SNH3_density = 0.817; //(wikipedia)
    //static const double  SNH3_density = 0.696; //(Wolfram Alpha)

  }

}

#endif
