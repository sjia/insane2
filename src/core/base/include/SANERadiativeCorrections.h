#ifndef SANERadiativeCorrections_HH
#define SANERadiativeCorrections_HH 1

#include "RadiativeCorrections.h"
namespace insane {
namespace physics {


/** */ 
class SANE_RCs_Model0 : public RadiativeCorrections1D {
   public:
      SANE_RCs_Model0(const char * n="", const char * t="");
      virtual ~SANE_RCs_Model0();
      virtual Int_t InitCrossSections();

      ClassDef(SANE_RCs_Model0,1)
};


/** */ 
class SANE_RCs_Model1 : public RadiativeCorrections1D {
   public:
      SANE_RCs_Model1(const char * n="", const char * t="");
      virtual ~SANE_RCs_Model1();
      virtual Int_t InitCrossSections();

      ClassDef(SANE_RCs_Model1,1)
};
}}

#endif

