#ifndef insane_physics_InclusiveDiffXSec_H
#define insane_physics_InclusiveDiffXSec_H 1

#include "DiffXSec.h"

#include "TROOT.h"
#include "TObject.h"
#include "TFoamIntegrand.h"
#include "TMath.h"
#include "StructureFunctions.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TParticlePDG.h"
#include "TDatabasePDG.h"

namespace insane {

  using namespace units;

  namespace physics {


/**  Base class for an Inclusive Differential Cross Section.
 *
 *   Uses units of GeV and radians. The Default unit for cross sections is
 *
 *
 * \ingroup xsections
 * \ingroup inclusiveXSec
 */
class InclusiveDiffXSec : public DiffXSec {

   protected:

      Int_t    fPolType;        ///< what does this do?
      Double_t fFuncArgs[9];    // used in plotting functions

   public:
      InclusiveDiffXSec();
      InclusiveDiffXSec(const InclusiveDiffXSec& old);
      virtual ~InclusiveDiffXSec();
      InclusiveDiffXSec& operator=(const InclusiveDiffXSec& old);
      virtual InclusiveDiffXSec*  Clone(const char * newname) const ;
      virtual InclusiveDiffXSec*  Clone() const ;

      /** \todo how to add cross sections ? */
      InclusiveDiffXSec operator+ (InclusiveDiffXSec& right) {
         InclusiveDiffXSec temp;
         /*      temp.fL*/
         return (temp);
      }

      virtual void DefineEvent(Double_t*);

      /// \todo A better way to implement polarization of cross section difference 
      /// NEEDS DOCUMENTED. When and how is this used?
      virtual void SetPolarizationType(Int_t t){fPolType = t;}  

      virtual void InitializePhaseSpaceVariables();

      /**  TFoamIntegrand method used by TFoam.
       *   Density() is sampled by TFoam with  0 < x < 1
       *     \frac{d^3\sigma}{d\theta d\phi d\omega}
       */
      virtual Double_t Density(Int_t ndim, Double_t * x);

      /** Virtual Method used to evaluate cross section. */
      virtual Double_t EvaluateXSec(const Double_t * x) const;



      /**  Needed by ROOT::Math::IBaseFunctionMultiDim */
      unsigned int NDim() const { return fnDim; }

      /** Needed by ROOT::Math::IBaseFunctionMultiDim */
      double DoEval(const double * x) const ;

      /** @name Useful functions for plotting.
       *  For using with TF1 functions.
       *  ------------------------------------------------------------
       *  @{
       */
      /** Cross section as a function of W.
       *  Calulates E'. 
       *  \param x[0] = W
       *  \param p[0] = theta
       *  \param p[1] = phi
       */
      virtual double Vs_W(double *x, double *p) ;

      /** sig(E).  E=x[0], theta=p[0], phi=p[1] */
      virtual double EnergyDepXSec(double *x, double *p); 

      /** sig(E). y=nu/Ebeam=x[0] */
      virtual double EnergyFractionDepXSec(double *x, double *p);

      /** Cross section as a function of energy.
       *  \param x[0] = energy
       *  \param p[0] = theta
       *  \param p[1] = phi
       */
      virtual double EnergyDependentXSec(double *x, double *p) ;

      /** Cross section as a function of energy.
       *  \param x[0] = W2
       *  \param p[0] = theta
       *  \param p[1] = phi
       *  \todo This function is incomplte. 
       */
      virtual double W2DependentXSec(double *x, double *p) ;

      /** Cross section as a function of energy.
       *  \param x[0] = W
       *  \param p[0] = Q2 
       *  \param p[1] = phi
       *  \todo This function is incomplte. 
       */
      virtual double WDependentXSec(double *x, double *p) ;

      /** Cross section as a function of Bjorken x.
       *  \param x[0] = x_bjorken 
       *  \param p[0] = Q2 
       *  \param p[1] = phi
       */
      virtual double xDependentXSec(double *x, double *p) ;

      /** Cross section as a function of Photon energy.
       *  \param x[0] = photon energy
       *  \param p[0] = theta
       *  \param p[1] = phi
       */
      virtual double PhotonEnergyDependentXSec(double *x, double *p) ;

      /** Cross section as a function of momentum.
       *  \param x[0] = momentum
       *  \param p[0] = theta
       *  \param p[1] = phi
       */
      virtual double MomentumDependentXSec(double *x, double *p) ;

      /** Cross section as a function of polar angle .
       *  \param x[0] = theta
       *  \param p[0] = energy
       *  \param p[1] = phi
       */
      virtual double PolarAngleDependentXSec(double *x, double *p) ;

      virtual double PolarAngleDependentXSec_deg(double *x, double *p) ;

      /** Cross section as a function of polar angle .
       *  \param x[0] = phi
       *  \param p[0] = energy
       *  \param p[1] = theta
       */
      virtual double AzimuthalAngleDependentXSec(double *x, double *p) ;

      virtual double Evaluate2D_p_theta(double *x, double *p){
         // using pion mass... add as parameter?...
         using namespace insane::units;
         double pp = x[0];
         double Ep = TMath::Sqrt(x[0]*x[0]+(M_pi0/GeV)*(M_pi0/GeV));
         fFuncArgs[0] = Ep;
         fFuncArgs[1] = x[1];
         fFuncArgs[2] = p[0];
         return( (pp/Ep)*EvaluateXSec(fFuncArgs));
      }

      //@}


      ClassDef(InclusiveDiffXSec,2)
};



/** A flat inclusive cross section
 *
 *
 * \ingroup inclusiveXSec
 */
class FlatInclusiveDiffXSec : public InclusiveDiffXSec {
   public:
      FlatInclusiveDiffXSec();
      virtual ~FlatInclusiveDiffXSec();

      /** Evaluate Cross Section. Flat cross section returns 1 */
      virtual Double_t EvaluateXSec(const Double_t * x) const;

      ClassDef(FlatInclusiveDiffXSec, 1)
};

}
}

#endif


