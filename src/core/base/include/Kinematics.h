#ifndef insane_kinematics_HH
#define insane_kinematics_HH 1

#include "SystemOfUnits.h"
#include "PhysicalConstants.h"
// See the excellent Fluentcpp blog series on strong types.
// https://www.fluentcpp.com/2017/05/23/strong-types-inheriting-functionalities-from-underlying/
#include "named_type.hpp"
#include <array>

namespace insane {

  namespace kinematics {

     using units::M_p;
     using units::M_pion;
     using units::GeV;

      /** @name Elastic Electron Scattering Kinematic Factors.
       *
       *  From page 90, Donnely and Raskin, Annals of Physics 191, 780142 (1989)
       *  " Polarization in Coincidence Electron Scattering from Nuclei".
       *
       *  Note: \f$ Q^2 >0 \f$ which is different from Donnely and Raskin's definition which is negative.
       *  @{
       */

      double v0(double en, double enprime, double q);
      /** eq 2.27a */
      double vL(double Q2, double q);
      /** eq 2.27b */
      double vT(double Q2, double q, double theta);
      /** eq 2.27c */
      double vTT(double Q2, double q);
      /** eq 2.27d */
      double vTL(double Q2, double q, double theta);
      /** eq 2.27e */
      double vTprime(double Q2, double q, double theta);
      /** eq 2.27f */
      double vTLprime(double Q2, double q, double theta);

      //@}
      
      double tau(  double Q2, double Mtarg = M_p/GeV);
      double q_abs(double Q2, double Mtarg = M_p/GeV);

      double Sig_Mott(double en, double theta);
      double fRecoil(double en, double theta, double Mtarg = M_p/GeV);

      namespace variables {

        /**  Stronly typed variables.
         */
        template<typename Tag>
        using KinematicInputVariable = fluent::NamedType<long double, Tag,
              fluent::Multiplicative,
              fluent::Addable,
              fluent::Comparable,
              fluent::Printable,
              fluent::ImplicitlyConvertibleTo >;

        using MomentumTransfer = KinematicInputVariable< struct MomentumTransfer_Tag >;
        using Theta            = KinematicInputVariable< struct Theta_Tag >;

        /** \name SIDIS variable tags.
         * @{
         */

        template<typename Tag> struct VariableSet{};

        using SIDIS_x_y_z_phih_phie  = VariableSet<struct SIDIS_x_y_z_phih_phie_Tag>;
        using SIDIS_x_Q2_z_phih_phie = VariableSet<struct SIDIS_x_Q2_z_phih_phie_Tag>;
        using SIDIS_eprime_ph        = VariableSet<struct SIDIS_eprime_ph_Tag>;
        //@}

      }

      /** Computes elastic kinematics for a collider frame.
       *
       * Follows Donnelly and Soffiatti (2011) http://inspirehep.net/record/895827 .
       *
       * Note, only collinear form has been implemented here.
       *
       * \todo implement crossed beam kinematics.
       *
       * Notation                      | 4 vectors = \f$(E,\vec{P})\f$
       * ----------------------------- | -------------------------
       * Electron beam 4-vector        | K  = (E_e,k_e)
       * Proton   beam 4-vector        | P  = (E_p,P_p)
       * Scattered electron 4-vector   | K' = (kp, kp)
       * Electron angle relative to e-beam  | theta_e
       * Electron angle relative to p-beam  | theta_e_p
       * Proton angle relative to p-beam    | theta_p
       *
       */
      struct ElasticKinematics {
        double E_e;        /// electron beam energy
        double E_p;        /// proton beam energy
        double P_e;        /// electron beam momentum
        double P_p;        /// proton beam momentum
        double kp;         /// scattered electron energy
        double kp_check;   /// scattered electron energy
        double P_p_prime;  /// scattered proton momentum
        double P_tot; 
        double E_tot; 
        double theta_e;    /// scattered electron angle relative to electron beam
        double theta_e_p;  /// scattered electron angle relative to proton beam
        double theta_p;    /// scatter proton angle relative to proton beam 
        double theta_q_p;    /// momentum transfer angle relative to proton beam 
        double theta_q;    /// momentum transfer angle relative to electron beam 
        double omega;      /// virtual photon energy
        double P_q;          /// virtual photon mementum
        double P_q2;         /// virtual photon mementum
        double xi;         /// \f$ \xi = k.p = E_e E_p + k_e P_p \f$
        double s ;         /// \f$ s = (K+P)^2 \f$
        double Q2; 
        double tau;
        double epsilon; 
        double m2; 
        double me2;
        double beta_e;
        double beta_p;
        double cos_th; /// cos(theta_e)
        double sin_th; /// sin(theta_e)
        double tilde_Epsilon_prime; /// equation 80
        double tilde_epsilon;       /// \f$\tilde{\epsilon}\f$, equation 74 
        double V0; 
        double tan2theta2_prime;  /// equation 78

        explicit ElasticKinematics(double Ee, double Ep, variables::MomentumTransfer Q2);
        explicit ElasticKinematics(double Ee, double Ep, variables::Theta th);

        void Print() const ;
      };

      /** Generalized Mott cross section.
       *
       * From http://inspirehep.net/record/895827
       * Equation 121
       *
       */
      double Mott_XS(const ElasticKinematics& kine);

      /** Recoil Factor.
       *
       * From http://inspirehep.net/record/895827
       *
       * Expresses the conservation of energy and momentum in elastic scattering.
       */
      double F_recoil(const ElasticKinematics& kine);

      /** Kinematic variables and other useful functions.
       *See
       * \f$ F_L = \rho^2 F_2 - 2x F_1) = 2x F_1 R \f$
       * \f$ \rho = \sqrt{1+ \frac{4M^2x^2}{Q^2}} \f$
       *
       */

      double xi_Nachtmann(double x, double Q2);

      double rho2(double x, double Q2);
      double rho(double x, double Q2);


      /** Computes semi-inclusive scattering kinematics.
       *
       * Follows the Trento convention. See http://inspirehep.net/record/660999
       * and http://inspirehep.net/record/677636 for details.
       *
       * \todo implement crossed beam kinematics.
       *
       * Notation                      | 4 vectors = \f$(E,\vec{P})\f$
       * ----------------------------- | -------------------------
       * Electron beam 4-vector        | l  = (E_l, P_l )
       * Proton   beam 4-vector        | p  = (E_p, P_p )
       * Scattered electron 4-vector   | l' = (E_l2,P_lp)
       * Produced hadron 4-vector      | Ph = (E_h, P_h)
       * Electron angle relative to e-beam  | theta_e
       * Electron angle relative to p-beam  | theta_e_p
       * Proton angle relative to p-beam    | theta_p
       *
       */
      struct SIDISKinematics {

        /** \name SIDIS Variables in lab frame.
         * @{
         */
        double E_l1     = 0.0;        /// electron beam energy
        double P_l1     = 0.0;        /// electron beam momentum
        double E_p      = 0.0;        /// proton beam energy
        double P_p      = 0.0;        /// proton beam momentum
        double E_l2     = 0.0;       /// scattered electron energy
        double P_l2     = 0.0;       /// scattered electron momentum
        double E_p2      = 0.0;        /// hadron energy
        double P_p2      = 0.0;        /// hadron momentum
        double nu       = 0.0;         /// virtual photon energy
        double P_q      = 0.0;        /// virtual photon momentum
        double E_tot    = 0.0;      /// Total energy
        double P_tot    = 0.0;      /// Total momentum
        double theta_l2 = 0.0;   /// scattered electron angle relative to electron beam
        double phi_l2   = 0.0;     /// scattered electron angle relative to electron beam
        double theta_p2 = 0.0;   /// scattered electron angle relative to electron beam
        double phi_p2   = 0.0;     /// scattered electron angle relative to electron beam
        double theta_q  = 0.0;    /// momentum transfer angle relative to electron beam 
        double phi_q    = 0.0;    /// momentum transfer angle relative to electron beam 
        double psi      = 0.0;        /// angle  in lab frame corresponding roughly to phi_S
        //@}

        /** \name Variables in target rest frame with \f$ \vec{q} \f$ along \f$\hat{z}\f$.
         */
        double PT_h      = 0.0; /// Transversze momentum of hadron \f$P_{h\perp}\f$
        double P_hPerp   = 0.0; /// Transversze momentum of hadron \f$P_{h\perp}\f$
        double P_hPara   = 0.0; /// Parallel momentum of hadron \f$P_{h\perp}\f$
        double phi_S     = 0.0; /// azimuthal angle of \f$ S_{\perp} \f$ relative to leptonic plane
        double phi_h     = 0.0; /// azimuthal angle of \f$ P_{h\perp} \f$ relative to leptonic plane
        double theta_h   = 0.0; /// polar angle of \f$ P_{h\perp} \f$ relative to q
        double sin_phi_h = 0.0; /// azimuthal angle of \f$ P_{h\perp} \f$ relative to leptonic plane
        double cos_phi_h = 0.0; /// azimuthal angle of \f$ P_{h\perp} \f$ relative to leptonic plane
        double S_para    = 0.0; /// \f$ S_{\parallel} = \frac{S\cdot q}{p\cdot q}} \frace{M}{\sqrt{1+\gamma^2}} \f$
        //@}

        /** \name SIDIS invariant variables.
         * @{
         */
        double s        = 0.0;         /// \f$ s = (l+p)^2 \f$
        double Q2       = 0.0;         /// \f$ Q^2 = -(q\cdot q) \f$
        double x        = 0.0;         /// \f$ x = Q^2/2(p\cdot q) \f$
        double y        = 0.0;         /// \f$ y = p\cdot q/p\cdot l \f$
        double z        = 0.0;         /// \f$ z = p\cdot P_h/p\cdot q \f$
        double gamma    = 0.0;      /// \f$ \gamma = 2 M x/Q \f$
        double epsilon  = 0.0;    /// \f$ \epsilon = (1-y-y^2\gamma^2/4)/(1-y+y^2/2+y^2\gamma^2/4)\f$
        double P_dot_Ph = 0.0;   /// \f$ p\cdot P_h = E_h M \f$ in target rest frame
        double P_dot_q  = 0.0;    /// \f$ p\cdot q = \nu M \f$ in target rest frame
        double Mp       = 0.0;        /// proton mass
        double Mp2      = 0.0;        /// proton mass squared
        double Mh       = 0.135; 
        double W2       = 0.0;  /// Invariant mass of p+gamma system
        double Wprime2  = 0.0;  /// Invariant mass of p+gamma-hadron system

        bool   isValid    = true;
        //@}

        explicit SIDISKinematics(double E0, double Mh, const std::array<double,6>& vars, variables::SIDIS_x_y_z_phih_phie );
        explicit SIDISKinematics(double E0, double Mh, const std::array<double,6>& vars, variables::SIDIS_x_Q2_z_phih_phie);
        explicit SIDISKinematics(double E0, double Mh, const std::array<double,6>& vars, variables::SIDIS_eprime_ph       );

        void Print() const ;

        /** Useful cross section functions.
         * @{
         */

        /** Term multiplying form factors in SIDIS cross section.
         *  Returns term multiplying braces in equation 2.14 from 
         *  Bacchetta, et.al., 2007 ( http://inspirehep.net/record/732275 ).
         */
        double SIDIS_xs_term() const ;

        /** Jacobian transformation \f$ d^3 l^{\prime} =  |J| dx dQ^2 d\psi \f$.
         *  Function returns \f$ |J| = (2 E_{l^{\prime}}) \frac{y}{4x} \f$.
         */
        double JacobianTransform_1() const ;
        //@}



      };

  }

}

#endif

