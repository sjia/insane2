#ifndef InSANEMathFunc_HH
#define InSANEMathFunc_HH 1
#include <cmath>
#include <iostream>
#include <complex>

#include "TParticle.h"
#include "SystemOfUnits.h"
#include "PhysicalConstants.h"
//#include <cstdarg>

// using namespace CLHEP; 

/** Functions which come in handy. */
namespace insane {


   /*! @brief Useful Kinematic functions for scattering experiments.

     All functions have arguments for each variable first then parameters
     with default values. No arguments should be pointers so that we use them
     inside of ROOT functions.

     To include....
      - Donnely and Raskin, Annals of Physics 191, 780142 (1989)
        " Polarization in Coincidence Electron Scattering from Nuclei"
          v functions,
      - Altareli,et al., Physics Report ...
        Polarized DIS kinematic functions
      - Simple Cross section formula
      - Simple form factors and structure function formula
      - Scaling variables
      - Conversions between variables
      - Multipole decomposition, CGLN amplitudes
      -

     Need to figure out how (or find library) to calculate....
      - Wigner D and d functions
      - 3j, 6j and 9j symbols. SOLUTION: http://www.gnu.org/software/gsl/manual/html_node/Coupling-Coefficients.html
      - CG coefficients

    */
   namespace Kine {

     using units::M_p;
     using units::M_pion;
     using units::GeV;

      typedef enum  { 
         kx       ,
         ky       ,
         kQ2      ,
         kW       ,
         kW2      ,
         knu      ,
         kepsilon ,
         kE       ,
         kEprime  ,
         ktheta   ,
         kphi     
      } Variable;


      /** Assuming the x has the structure:
       *  x[0]=energy
       *  x[1]=theta
       *  x[2]=phi
       */
      void SetMomFromEThetaPhi(TParticle*p, double *x);

      void SetEFromMomThetaPhi(TParticle*p, double *x);


      double v0(double en, double enprime, double q);

      /** @name Electron Kinematic Factors
       *  From page 90, Donnely and Raskin, Annals of Physics 191, 780142 (1989)
       *  " Polarization in Coincidence Electron Scattering from Nuclei"
       *  Note: \f$ Q^2 >0 \f$ which is different from Donnely and Raskin's definition which is negative.
       *  @{
       */
      /** eq 2.27a */
      double vL(double Qsq, double q);
      /** eq 2.27b */
      double vT(double Qsq, double q, double theta);
      /** eq 2.27c */
      double vTT(double Qsq, double q);
      /** eq 2.27d */
      double vTL(double Qsq, double q, double theta);
      /** eq 2.27e */
      double vTprime(double Qsq, double q, double theta);
      /** eq 2.27f */
      double vTLprime(double Qsq, double q, double theta);
      //@}

      double tau(double Qsq, double Mtarg = M_p/GeV);
      double q_abs(double Qsq, double Mtarg = M_p/GeV);

      double Sig_Mott(double en, double theta);
      double fRecoil(double en, double theta, double Mtarg = M_p/GeV);

      /** @name DIS Kinematics
       *  Typical kinematic functions for DIS experiments
       *  @{
       */
      double Qsquared(double en, double enprime, double theta);
//   double Qsquared_max(double en, double x, ouble Mtarg = M_p/GeV);

      double xBjorken(double Qsq, double nu, double Mtarg = M_p/GeV);
      double xBjorken_EEprimeTheta(double e, double eprime, double theta);
      double xi_Nachtmann(double x, double Q2);
      double nu(double en, double enprime);
      double nu_WsqQsq(double Wsq, double Qsq,double Mtarg = M_p/GeV);
      double W2(double Qsq, double nu, double Mtarg = M_p/GeV);
      double epsilon(double en, double enprime, double theta);
//   double W2_xQsq(double x, double Qsq, double Mtarg = M_p/GeV);
      double W_xQsq(double x, double Qsq, double Mtarg = M_p/GeV);
      double W_xQ2( double x, double Q2,  double Mtarg = M_p/GeV);
      double W_EEprimeTheta(double e, double eprime, double theta);
      double Q2_xW(double x, double W, double Mtarg = M_p/GeV);
      double xBjorken_WQsq(double W, double Qsq, double Mtarg = M_p/GeV);
      double xBjorken_WQ2( double W, double Q2,  double Mtarg = M_p/GeV);
      
      double BeamEnergy_xQ2y(double x, double Q2, double y, double Mtarg = M_p/GeV);
      double Eprime_xQ2y(double x, double Q2, double y, double Mtarg = M_p/GeV);
      double Eprime_W2theta(double W2,double theta,double Ebeam, double Mtarg = M_p/GeV); 
      double Theta_xQ2y(double x, double Q2, double y, double Mtarg = M_p/GeV);

      double Theta_epsilonQ2W2(double eps, double Q2, double W2, double Mtarg = M_p/GeV);
      double Eprime_epsilonQ2W2(double eps, double Q2, double W2, double Mtarg = M_p/GeV);
      double BeamEnergy_epsilonQ2W2(double eps, double Q2, double W2, double Mtarg = M_p/GeV);

      double K_Hand(double x, double Q2);

      
      //@}

   /** @name Bremsstrahlung Beams. 
    *  Photon spectrum from bremsstralung beams and virtual photon spectra 
    *  for calculating electroproduction cross sections
    *  @{
    */

      /** Bremsstrahlung bremsstrahlung spectrum. 
       *  Tsai and Whitis, SLAC-PUB-184 1966 eqn.24
       */ 
      double I_gamma_1(double t, double E0, double k);
      double I_gamma_1_test(double t, double E0, double k);
      double I_gamma_1_approx(double t, double E0, double k);

      /** Number of photons from bremsstrahlung.  */ 
      double N_gamma(double dOverX0, double kmin, double kmax, double E0 = -1.0);

      /** The average photon energy from a bremsstrahlung beam. */
      double Ebrem_avg(double t, double kmin, double E0);
      double I_gamma_1_k_avg(double t, double kmin, double E0);

      /** Number of equivalent quanta for bremsstrahlung photoproduction. */
      double Q_equiv_quanta(double t, double kmin, double E0);

      /** Minimum photon energy needed for the reaction gamma+Mt -> x+n. */
      double k_min_photoproduction(double Ex, double thetax, double mx=M_pion/GeV, double mt=M_p/GeV, double mn=M_p/GeV);



   //@}

      /** @name Polarized DIS Kinematic Functinos
       *  Typical kinematic functions for DIS experiments
       *  @{
       */

      /** \f$ D = \frac{E-\epsilon E^{\prime} }{E(1+\epsilon R)} \f$.  */
      double D(double e, double eprime,double theta, double R);
      double Dprime(double e, double eprime,double theta, double R);

      /** \f$ d = D \sqrt{ \frac{2 \epsilon}{1+\epsilon} } \f$ */
      double d(double e, double eprime,double theta, double R);

      /** \f$ \eta =  \epsilon \frac{\sqrt{Q^2}}{E - \epsilon E^{\prime}} \f$ */
      double Eta(double e, double eprime, double theta);

      /** \f$ \xi = \eta \frac{ 1+\epsilon}{2\epsilon} \f$ */
      double Xi(double e, double eprime, double theta);

      /** New variable for calculating Virtual Compton Scattering Asymmetries using A180 and Aperp,
       *  where Aperp is not 90 degrees but some other arbitray angle, alpha.
       *  \f$ \chi = \frac{E^{'} \sin(\theta) \sec(\phi)}{E-E^{'} \cos(\theta)} \f$
       */
      double Chi(double e, double eprime,  double theta,double phi);

      //@}

      // Parameterization of the world data of R = sig_L/sig_T 
      // by the SLAC E143 Collaboration  
      double R1998(double x,double Q2); 
      double RErr1998(double x,double Q2);
      // Auxillary functions needed by the SLAC fit 
      double Ra1998(double x,double Q2);  
      double Rb1998(double x,double Q2);  
      double Rc1998(double x,double Q2);  
      double Theta1998(double x,double Q2);  

      // Beam depolarization calculated from Maximon 1954
      double BeamDepol(double , double , double );

      // Function representing the vertex correction, vacuum polarization, and others from Mo and Tsai
      // Also found in Stein et.al. We do not include the (1+0.5bT) term because it is only for external radiation
      double F_tilde_internal(double Es, double Ep, double theta);

   } // namespace Kine

   namespace Math {

      std::complex<double> CGLN_F1(double Q2, double s, double t);
      std::complex<double> CGLN_F2(double Q2, double s, double t);
      std::complex<double> CGLN_F3(double Q2, double s, double t);
      std::complex<double> CGLN_F4(double Q2, double s, double t);
      std::complex<double> CGLN_F5(double Q2, double s, double t);
      std::complex<double> CGLN_F6(double Q2, double s, double t);
      std::complex<double> CGLN_F7(double Q2, double s, double t);
      std::complex<double> CGLN_F8(double Q2, double s, double t);

      std::complex<double> multipole_E(double Q2, double W);
      std::complex<double> multipole_M(double Q2, double W);

      double  f_rad_length(double a);

      /** Returns the radition length in g/cm^2. */
      double rad_length(int Z,int A);

      double TestFunction22(double x);


      //typedef int (*NDimInterpolation_t) (int n, ...);
      //double NDimInterpolation(int n, NDimInterpolation_t f ){
      //       va_list vl;
      //       va_start(vl,n);


   } // namespace Math

   // color transparency parametrized from simple take off from http://inspirehep.net/record/1202504
   // alpha = 0.00857*Q2 + 0.771
   double alpha_CT(double Q2);

   namespace Phys {

      double A_pair_corr( 
            double A, 
            double Apair, 
            double Rpair );

      double delta_A_pair_corr(
            double A, double delta_A,
            double Apair, double delta_Apair,
            double Rpair, double delta_Rpair );

      double A_elastic_sub_corr(
            double A,
            double SigmaEl, 
            double SigmaIn,
            double DelEl   );

      double delta_A_elastic_sub_corr(
            double A, double deltaA,
            double SigmaEl, double deltaSigmaEl,
            double SigmaIn, double deltaSigmaIn,
            double DelEl,   double deltaDelEl);

      double delta_g1(double x, double Q2, double E0, double R, double deltaR, double F1, double deltaF1, double Apara, double deltaApara, double Aperp, double deltaAperp);
      double delta_g2(double x, double Q2, double E0, double R, double deltaR, double F1, double deltaF1, double Apara, double deltaApara, double Aperp, double deltaAperp);

   }
} // namespace InSANE


#endif
