#ifndef InSANEPhysicalConstants_HH
#define InSANEPhysicalConstants_HH 1

#include "TObject.h"

#include "CLHEP/Units/SystemOfUnits.h"
#include "CLHEP/Units/PhysicalConstants.h"
#include "SystemOfUnits.h"

/*! \page PhysicalConstants  Physical Constants

  \section constantSources Data sources 

  - CLHEP
  - http://physics.nist.gov/cuu/index.html
  - Magnetic moments and other constants taken from
  <a href="http://physics.nist.gov/cgi-bin/cuu/Value?gp|search_for=atomnuc!"> CODATA </a>.
  - PDG 

*/

/*! Physical Constants emulating CLHEP/Geant4 style units.

     In order to use these directly in the interpreter you must do a "using namespace CLHEP".
     The best spot for this probably your .rootlogon.C script.

   The basic units are :
     - millimeter  (mm)
     - nanosecond  (ns)
     - Mega electron Volt  (MeV)
     - positon charge (eplus)
     - degree Kelvin
     - amount of substance (mole)

 */
//using CLHEP::pi;
//using CLHEP::twopi;
//using CLHEP::halfpi;
//using CLHEP::pi2;
//using CLHEP::Avogadro;
//using CLHEP::c_light;
//using CLHEP::c_squared;
//using CLHEP::h_Planck;
//using CLHEP::hbar_Planck;
//using CLHEP::hbarc;
//using CLHEP::hbarc_squared;
//using CLHEP::electron_charge;
//using CLHEP::e_squared;
//using CLHEP::electron_mass_c2;
//using CLHEP::proton_mass_c2;
//using CLHEP::neutron_mass_c2;
//using CLHEP::amu_c2;
//using CLHEP::amu;

//
// permeability of free space mu0    = 2.01334e-16 Mev*(ns*eplus)^2/mm
// permittivity of free space epsil0 = 5.52636e+10 eplus^2/(MeV*mm)
//
//static const double mu0      = 4*pi*1.e-7 * henry/m;
//static const double epsilon0 = 1./(c_squared*mu0);
//using CLHEP::mu0;
//using CLHEP::epsilon0;
//
////
//// electromagnetic coupling = 1.43996e-12 MeV*mm/(eplus^2)
////
////
//
//
//
//using CLHEP::elm_coupling;
//using CLHEP::fine_structure_const;
//using CLHEP::classic_electr_radius;
//using CLHEP::electron_Compton_length;
//using CLHEP::Bohr_radius;
//using CLHEP::alpha_rcl2;
//using CLHEP::twopi_mc2_rcl2;
//using CLHEP::k_Boltzmann;
//using CLHEP::STP_Temperature;
//using CLHEP::STP_Pressure;
//using CLHEP::kGasThreshold;
//using CLHEP::universe_mean_density;

namespace insane {

  /**  Units and constants.
   *
   */
  namespace units {

    using CLHEP::GeV;

    using CLHEP::pi;
    using CLHEP::twopi;
    using CLHEP::halfpi;
    using CLHEP::pi2;
    using CLHEP::Avogadro;
    using CLHEP::c_light;
    using CLHEP::c_squared;
    using CLHEP::h_Planck;
    using CLHEP::hbar_Planck;
    using CLHEP::hbarc;
    using CLHEP::hbarc_squared;
    using CLHEP::electron_charge;
    using CLHEP::e_squared;
    using CLHEP::electron_mass_c2;
    using CLHEP::proton_mass_c2;
    using CLHEP::neutron_mass_c2;
    using CLHEP::amu_c2;
    using CLHEP::amu;

    //static const double mu0      = 4*pi*1.e-7 * henry/m;
    //static const double epsilon0 = 1./(c_squared*mu0);
    using CLHEP::mu0;
    using CLHEP::epsilon0;

    // electromagnetic coupling = 1.43996e-12 MeV*mm/(eplus^2)
    using CLHEP::elm_coupling;
    using CLHEP::fine_structure_const;
    using CLHEP::classic_electr_radius;
    using CLHEP::electron_Compton_length;
    using CLHEP::Bohr_radius;
    using CLHEP::alpha_rcl2;
    using CLHEP::twopi_mc2_rcl2;
    using CLHEP::k_Boltzmann;
    using CLHEP::STP_Temperature;
    using CLHEP::STP_Pressure;
    using CLHEP::kGasThreshold;
    using CLHEP::universe_mean_density;

    static const double alpha  = CLHEP::fine_structure_const;
    static const double alpha2 = CLHEP::fine_structure_const*CLHEP::fine_structure_const;

    static const double M_test      =  0.9382720013 *GeV; 

    /// lepton masses 
    static const double M_e       =  0.000511*GeV; 
    static const double M_muon    =  0.1134289256*GeV;                /// PDG: 0.1134289256 ± 0.0000000029 
    static const double M_tau     =  1.77682*GeV;                     /// PDG: 1776.82 ± 0.16 
    /// quark masses: 
    /// \todo Add the quark masses  
    static const double M_up      =  0*GeV; 
    static const double M_down    =  0*GeV; 
    static const double M_strange =  0*GeV; 
    static const double M_top     =  0*GeV; 
    static const double M_bottom  =  0*GeV; 
    static const double M_beauty  =  0*GeV; 
    static const double M_charm   =  0*GeV;
    /// quark charges 
    static const double Q_up      = 2./3.; 
    static const double Q_down    = 1./3.; 
    static const double Q_strange = 1./3.; 
    /// meson masses 
    static const double M_pion    =  0.13957018   *GeV;               /// PDG: 139.57018 ± 3.5E-4 MeV 
    static const double M_pi0     =  0.1349766    *GeV;
    /// nucleon masses 
    static const double M_p       =  0.9382720013 *GeV; 
    static const double M_n       =  0.939565378  *GeV; 
    static const double M_p_GeV   =  0.9382720013     ; 
    static const double M_n_GeV   =  0.939565378      ; 

    /// nuclei masses
    static const double M_d       =  1.875612859  *GeV; 
    static const double M_3He     =  3.0160293    *amu_c2;            /// wikipedia



    /// nucleon resonances
    static const double M_Delta   =  1.232*GeV; 
    static const double M_P33     =  1.232*GeV; 
    static const double M_S11     =  1.535*GeV; 
    static const double M_D13     =  1.520*GeV; 
    static const double M_F15     =  1.680*GeV; 
    static const double M_S15     =  1.650*GeV; 
    static const double M_P11     =  1.440*GeV; 

    //static const double bohr_magneton = eplus*hbar
    static const double bohr_magneton    =  5.7883818066e-5*eV/tesla; ///  5.7883818066(38)×10−5   eV·T−1
    static const double nuclear_magneton =  3.1524512605e-8*eV/tesla; ///  3.152 451 2605(22) x 10-8 eV T-1 

    static const double Mu_B = bohr_magneton ;
    static const double Mu_N = nuclear_magneton;

    /// magnetic moments 
    static const double Mu_p   =  2.792847356;                        /// PDG: 2.792847356 ± 0.000000023 Mu_N
    static const double Mu_n   = -1.91304272;                         /// PDG: -1.91304272 ± 0.00000045 Mu_N
    static const double Mu_d   =  0.8574382308;                       /// CODATA: 0.857 438 2308(72)   Mu_N
    static const double Mu_He3 = -2.12762523466;                      /// source??  
    static const double Mu_t   =  2.978962448 ;                       /// triton magnetic moment CODATA: 2.978 962 448(38)    Mu_N

    static const double Kappa_p   = Mu_p - 1.0;                       /// Anomalous magnetic moment of the proton 
    static const double Kappa_n   = Mu_n;                             /// Anomalous magnetic moment of the neutron
    static const double Kappa_He3 = -4.200;                           /// Anomalous magnetic moment of 3He  

    static const double hbarc2_gev_b  = 3.89379*1e-04;                /// (h*c)^2 in GeV^2*b
    static const double hbarc2_gev_mb = 3.89379*1e-01;                /// (h*c)^2 in GeV^2*mb
    static const double hbarc2_gev_ub = 3.89379*1e+02;                /// (h*c)^2 in GeV^2*ub
    static const double hbarc2_gev_nb = 3.89379*1e+05;                /// (h*c)^2 in GeV^2*nb
    static const double hbarc2_mev_pb = 3.89379*1e+05;                /// (h*c)^2 in MeV^2*pb
    static const double hbarc_gev_fm  = 0.197326968;                  /// h*c in GeV fm

    static const double g_A = 1.2673;    // nucleon axial charge / Axial-vector coupling constant
    static const double M_A = 1.015;     // GeV/c^2 Axial nucleon mass (a dipole FF fit mass)
    static const double M_V = 0.8426;     // =sqrt(0.71)  GeV/c^2 Vector nucleon mass (a dipole FF fit mass)

  }

  namespace masses
  {
    using namespace insane::units;
    static const double M_pion    =  0.13957018   *GeV;  /// PDG: 139.57018 ± 3.5E-4 MeV 
    static const double M_pi0     =  0.1349766    *GeV;  /// nucleon masses 
    static const double M_p       =  0.9382720013 *GeV; 
    static const double M_n       =  0.939565378  *GeV; 
    static const double M_p_GeV   =  0.9382720013     ; 
    static const double M_n_GeV   =  0.939565378      ; 
    static const double M_d       =  1.875612859  *GeV; 
    static const double M_2H      =  M_d; 
    static const double M_3H      =  2.80943      *GeV;
    static const double M_3He     =  2.80941      *GeV;
    static const double M_4He     =  3.7284       *GeV;
  }

}


/** Extention of CLHEP constants.
 * \deprecated use insane::units.
 */
//namespace CLHEP {
//
//  static const double M_test      =  0.9382720013 *GeV; 
//
//  /// lepton masses 
//  static const double M_e       =  0.000511*GeV; 
//  static const double M_muon    =  0.1134289256*GeV;                /// PDG: 0.1134289256 ± 0.0000000029 
//  static const double M_tau     =  1.77682*GeV;                     /// PDG: 1776.82 ± 0.16 
//  /// quark masses: 
//  /// \todo Add the quark masses  
//  static const double M_up      =  0*GeV; 
//  static const double M_down    =  0*GeV; 
//   static const double M_strange =  0*GeV; 
//   static const double M_top     =  0*GeV; 
//   static const double M_bottom  =  0*GeV; 
//   static const double M_beauty  =  0*GeV; 
//   static const double M_charm   =  0*GeV;
//   /// quark charges 
//   static const double Q_up      = 2./3.; 
//   static const double Q_down    = 1./3.; 
//   static const double Q_strange = 1./3.; 
//   /// meson masses 
//   static const double M_pion    =  0.13957018   *GeV;               /// PDG: 139.57018 ± 3.5E-4 MeV 
//   static const double M_pi0     =  0.1349766    *GeV;
//   /// nucleon masses 
//   static const double M_p       =  0.9382720013 *GeV; 
//   static const double M_n       =  0.939565378  *GeV; 
//   static const double M_p_GeV   =  0.9382720013     ; 
//   static const double M_n_GeV   =  0.939565378      ; 
//   /// nucleus masses
//   static const double M_d       =  1.875612859  *GeV; 
//   static const double M_3He     =  3.0160293    *amu_c2;            /// wikipedia
//   /// nucleon resonances
//   static const double M_Delta   =  1.232*GeV; 
//   static const double M_P33     =  1.232*GeV; 
//   static const double M_S11     =  1.535*GeV; 
//   static const double M_D13     =  1.520*GeV; 
//   static const double M_F15     =  1.680*GeV; 
//   static const double M_S15     =  1.650*GeV; 
//   static const double M_P11     =  1.440*GeV; 
//
//   //static const double bohr_magneton = eplus*hbar
//   static const double bohr_magneton    =  5.7883818066e-5*eV/tesla; ///  5.7883818066(38)×10−5   eV·T−1
//   static const double nuclear_magneton =  3.1524512605e-8*eV/tesla; ///  3.152 451 2605(22) x 10-8 eV T-1 
//
//   static const double Mu_B = bohr_magneton ;
//   static const double Mu_N = nuclear_magneton;
//
//   /// magnetic moments 
//   static const double Mu_p   =  2.792847356;                        /// PDG: 2.792847356 ± 0.000000023 Mu_N
//   static const double Mu_n   = -1.91304272;                         /// PDG: -1.91304272 ± 0.00000045 Mu_N
//   static const double Mu_d   =  0.8574382308;                       /// CODATA: 0.857 438 2308(72)   Mu_N
//   static const double Mu_He3 = -2.12762523466;                      /// source??  
//   static const double Mu_t   =  2.978962448 ;                       /// triton magnetic moment CODATA: 2.978 962 448(38)    Mu_N
//
//   static const double Kappa_p   = Mu_p - 1.0;                       /// Anomalous magnetic moment of the proton 
//   static const double Kappa_n   = Mu_n;                             /// Anomalous magnetic moment of the neutron
//   static const double Kappa_He3 = -4.200;                           /// Anomalous magnetic moment of 3He  
//     
//   static const double hbarc2_gev_b  = 3.89379*1e-04;                /// (h*c)^2 in GeV^2*b
//   static const double hbarc2_gev_mb = 3.89379*1e-01;                /// (h*c)^2 in GeV^2*mb
//   static const double hbarc2_gev_ub = 3.89379*1e+02;                /// (h*c)^2 in GeV^2*ub
//   static const double hbarc2_gev_nb = 3.89379*1e+05;                /// (h*c)^2 in GeV^2*nb
//   static const double hbarc2_mev_pb = 3.89379*1e+05;                /// (h*c)^2 in MeV^2*pb
//   static const double hbarc_gev_fm  = 0.197326968;                  /// h*c in GeV fm
//
//   static const double g_A = 1.2673;    // nucleon axial charge / Axial-vector coupling constant
//   static const double M_A = 1.015;     // GeV/c^2 Axial nucleon mass (a dipole FF fit mass)
//   static const double M_V = 0.8426;     // =sqrt(0.71)  GeV/c^2 Vector nucleon mass (a dipole FF fit mass)
//
//}

//using CLHEP::hbarc_gev_fm; 
//using CLHEP::hbarc2_gev_b; 
//using CLHEP::hbarc2_gev_mb; 
//using CLHEP::hbarc2_gev_ub; 
//using CLHEP::hbarc2_gev_nb; 
//using CLHEP::hbarc2_mev_pb; 

//using CLHEP::Q_up; 
//using CLHEP::Q_down; 
//using CLHEP::Q_strange; 
//
//using CLHEP::M_test;
//using CLHEP::M_p;
//using CLHEP::M_e;
//using CLHEP::M_pion;
//using CLHEP::M_muon; 
//using CLHEP::M_tau; 
//using CLHEP::M_Delta;
//using CLHEP::M_P33;
//using CLHEP::M_S11;
//using CLHEP::M_D13;
//using CLHEP::M_F15;
//using CLHEP::M_S15;
//using CLHEP::M_P11;
//using CLHEP::M_n;
//using CLHEP::M_d;
//using CLHEP::M_3He;
//using CLHEP::bohr_magneton;
//using CLHEP::nuclear_magneton;
//using CLHEP::Mu_B;
//using CLHEP::Mu_N;
//using CLHEP::Mu_p;
//using CLHEP::Mu_n;
//using CLHEP::Mu_d;
//using CLHEP::Mu_t;
//using CLHEP::Mu_He3;
//using CLHEP::Kappa_p;
//using CLHEP::Kappa_n;
//using CLHEP::Kappa_He3; 

#endif

