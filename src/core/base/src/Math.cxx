#include "Math.h"
#include <future>

#include "PhysicalConstants.h"
#include "SystemOfUnits.h"
#include "Math/GaussIntegrator.h"
#include "Math/Integrator.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/AllIntegrationTypes.h"
#include "Math/Functor.h"
#include "Math/GaussIntegrator.h"
#include "Math/RichardsonDerivator.h"
#include "Math/Derivator.h"
#include <functional>

namespace insane {
  namespace integrate   {

    double gaus(std::function<double(double)> func, double min, double max)
    {
      //ROOT::Math::GaussIntegrator ig4;
      ROOT::Math::Functor1D wf(func);
      ROOT::Math::Integrator integrator(ROOT::Math::IntegrationOneDim::kGAUSS); 
      integrator.SetFunction(wf);
      auto val = integrator.Integral(min,max);
      return val;
      //TF1 f("Sin Function", "sin(x)", 0, TMath::Pi());
      //ROOT::Math::Functor wf(&amp;f2,2);
      //ROOT::Math::WrappedTF1 wf1(f);
      //ROOT::Math::GaussIntegrator ig;
      //ig.SetFunction(wf1, false);
      //ig.SetRelTolerance(0.001);
      //cout << ig.Integral(0, TMath::PiOver2()) << endl;
      //return 0;
    }

    double legendre(std::function<double(double)> func, double min, double max)
    {
      //ROOT::Math::GaussIntegrator ig4;
      ROOT::Math::Functor1D wf(func);
      ROOT::Math::Integrator integrator(ROOT::Math::IntegrationOneDim::kLEGENDRE); 
      auto options = integrator.Options();
      options.SetNPoints(36);
      integrator.SetOptions(options);
      integrator.SetFunction(wf);
      auto val = integrator.Integral(min,max);
      return val;
    }

    double simple(std::function<double(double)> func, double min, double max, int N)
    {
      double dx  = (max-min)/double(N);
      double res = 0.0;
      for(int i = 0; i<N; i++ ) {
        double x = min + (double(i)+0.5)*dx;
        res += func(x)*dx;
      }
      return res;
    }

    double simple_threads(std::function<double(double)> func, double min, double max, int N, int nthreads)
    {
      double delta_x    = (max-min)/double(nthreads);
      double thread_min = min;

      std::vector<std::future<double>> results;

      for(int ith = 0; ith<nthreads; ith++) {
        double thread_max = min + delta_x;
        results.push_back(
          std::async(std::launch::async, std::function<decltype(simple)>(simple), func, thread_min, thread_max, N)
                     //std::function<double(std::function<double(double)>, double, double, int>(simple), func, thread_min, thread_max)
          );
      }
      double total = 0.0;
      for(auto& f: results) {
        total += f.get();
      }
      return total;
    }


    double adaptive(std::function<double(double)> func, double min, double max)
    {
      ROOT::Math::Functor1D wf(func);
      ROOT::Math::Integrator integrator(ROOT::Math::IntegrationOneDim::kADAPTIVE); 
      auto options = integrator.Options();
      options.SetNPoints(1);
      integrator.SetOptions(options);
      integrator.SetFunction(wf);
      auto val = integrator.Integral(min,max);
      return val;
    }

  }

  namespace math {

    double simple_quad_sum(std::function<double(double)> func, double min, double max, int N)
    {
      double dx  = (max-min)/double(N);
      double res = 0.0;
      for(int i = 0; i<N; i++ ) {
        double x = min + (double(i)+0.5)*dx;
        res += func(x)*dx*dx;
      }
      return res;
    }
    double derivative(std::function<double(double)> func, double x0)
    {
      ROOT::Math::Functor1D wf(func);
      //ROOT::Math::RichardsonDerivator rd;
      //rd.SetFunction(wf);
      //return rd.Derivative1(x0);
      ROOT::Math::Derivator der;
      der.SetFunction(wf);
      return der.Eval(x0);
      //std::cout << "First Derivative:   " << rd.Derivative1(x0) << std::endl;
      //std::cout << "Second Derivative:  " << rd.Derivative2(x0) << std::endl;
      //std::cout << "Third Derivative:   " << rd.Derivative3(x0) << std::endl;

    }

    double StandardPhi(double phi)
    {
    using namespace insane::units;
      if( phi > pi ) {
        phi -= twopi;
      } else if ( phi <= -pi ) {
        phi += twopi;
      }
      return phi;
    }

  }

}

