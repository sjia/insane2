#include "PMTResponse.h"

namespace insane {
//___________________________________________________________________________

PMTResponse::PMTResponse() {
   //fN0;fP0,fP1,fpe,fxp,fsigp,fx0,fsig0,fx1,fsig1,fmu;
   fA             = 10.0;
   fA2            = 0.0;
   fFit_low_light = nullptr;
   fFit           = nullptr;
   fNmax          = 80;
   fNIntPoints    = 200;
   fXIntMin       = -200.0;
   fXIntMax       = 200.0;
}
//___________________________________________________________________________

PMTResponse::~PMTResponse()
{
}

//___________________________________________________________________________
void PMTResponse::PrintParameters()
{
   std::cout << "fN0   = " << fN0   <<  std::endl;
   std::cout << "fP0   = " << fP0   <<  std::endl;
   std::cout << "fP1   = " << fP1   <<  std::endl;
   std::cout << "fA    = " << fA    <<  std::endl; //slope of exponential part
   std::cout << "fpe   = " << fpe   <<  std::endl; // fraction of ents under the exponential
   std::cout << "fxp   = " << fxp   <<  std::endl; // ped
   std::cout << "fsigp = " << fsigp <<  std::endl; // ped
   std::cout << "fx0   = " << fx0   <<  std::endl; // spe
   std::cout << "fsig0 = " << fsig0 <<  std::endl; // spe
   std::cout << "fmu   = " << fmu   <<  std::endl;
   std::cout << "fx1   = " << fx1   <<  std::endl; 
   std::cout << "fsig1 = " << fsig1 <<  std::endl;
   std::cout << "fgN   = " << fgN   <<  std::endl;
}
//___________________________________________________________________________

void PMTResponse::SetParameters(double *p)
{
   fN0   = p[0];
   fP0   = p[1];
   fP1   = p[2];
   fA    = p[3]; //slope of exponential part
   fpe   = p[4]; // fraction of ents under the exponential
   fxp   = p[5]; // ped
   fsigp = p[6]; // ped
   fx0   = p[7]; // spe
   fsig0 = p[8]; // spe
   fmu   = p[9];
   fx1   = p[10];
   fsig1 = p[11];
   fgN  = (1.0 + TMath::Erf(fx0 / (TMath::Sqrt(2.0) * fsig0))) / 2.0;
}
//___________________________________________________________________________

double PMTResponse::PMT_fit(double *x, double *p) const
{
   Double_t res  = 0;
   Double_t xx   = x[0];
   Double_t N0   = p[0];
   Double_t P0   = p[1];
   Double_t P1   = p[2];
   //Double_t A    = p[3]; //slope of exponential part
   //Double_t pe   = p[4]; // fraction of ents under the exponential
   Double_t xp   = p[5]; // ped
   Double_t sigp = p[6]; // ped
   //Double_t x0   = p[7]; // spe
   //Double_t sig0 = p[8]; // spe
   Double_t mu   = p[9];
   Double_t x1   = p[10];
   Double_t sig1 = p[11];

   Double_t Mpars[] = {mu, xp, x1, sig1};
   Double_t M       = M_pmt(x, Mpars);

   // Quick and dirty integration
   //double deltax = (fXIntMax - fXIntMin) / ((double)fNIntPoints);
   //double covres = 0.0;
   //double tau = 0.0;
   double ser = SER(x, &p[3]);
   //double Noise = 0.0;
   //for (int i = 0; i < fNIntPoints; i++) {
   //   tau     = fXIntMin + ((double)i + 0.5) * deltax;
   //   Noise   = TMath::Gaus(xx - tau, xp, sigp, kTRUE);
   //   SER     = SER0(&tau, &p[3]);
   //   covres += Noise * SER * deltax;
   //}

   double Noise   = TMath::Gaus(xx, xp, sigp, kTRUE);
   res = N0 * (P0 * Noise + P1 * ser + M);
   return(res);
}
//___________________________________________________________________________

double PMTResponse::SER0_exp(double *x, double *p) const
{
   Double_t xx   = x[0];
   Double_t A    = p[0]; //slope of exponential part
   Double_t pe   = p[1]; // fraction of ents under the exponential
   Double_t xp   = p[2]; // ped
   //Double_t sigp = p[3]; // ped
   //Double_t x0   = p[4]; // spe
   //Double_t sig0 = p[5]; // spe
   //Double_t gN  = (1.0 + TMath::Erf(x0 / (TMath::Sqrt(2.0) * sig0))) / 2.0;
   Double_t expo = (pe / A) * TMath::Exp(-1.0 * ((xx - xp) / A));
   if (xx < xp/*+fsigp*6.0*/) return(0.0);
   return(expo);
}
//___________________________________________________________________________
double PMTResponse::SER_exp(double *x, double *p) const {
   Double_t xx   = x[0];
   //Double_t A    = p[0]; //slope of exponential part
   //Double_t pe   = p[1]; // fraction of ents under the exponential
   Double_t xp   = p[2]; // ped
   Double_t sigp = p[3]; // ped
   //Double_t x0   = p[4]; // spe
   //Double_t sig0 = p[5]; // spe
   Double_t N    = p[6]; // spe

   // Quick and dirty integration
   double deltax = (fXIntMax - fXIntMin) / ((double)fNIntPoints);
   double covres = 0.0;
   double tau = 0.0;
   double tau2 = 0.0;
   double Noise;
   double ser;
   //return( SER0_exp(x, p)* N);
   for (int i = 0; i < fNIntPoints; i++) {
      tau     = fXIntMin + (double(i) + 0.5) * deltax;
      tau2    = xx - tau;
      Noise   = TMath::Gaus(tau, xp,sigp, kTRUE);
      ser     = SER0_exp(&tau2, p);
      covres += Noise * ser * deltax;
   }
   return(covres * N);
}
//___________________________________________________________________________
double PMTResponse::SER0_peak(double *x, double *p) const {
   Double_t xx   = x[0];
   //Double_t A    = p[0]; //slope of exponential part
   Double_t pe   = p[1]; // fraction of ents under the exponential
   Double_t xp   = p[2]; // ped
   //Double_t sigp = p[3]; // ped
   Double_t x0   = p[4]; // spe
   Double_t sig0 = p[5]; // spe
   Double_t gN  = (1.0 + TMath::Erf(x0 / (TMath::Sqrt(2.0) * sig0))) / 2.0;
   //Double_t expo = (pe / A) * TMath::Exp(-1.0 * ((xx - xp) / A));
   Double_t gaus = ((1.0 - pe) / gN) * TMath::Gaus(xx, x0 + xp, sig0, kTRUE);
   //if (xx < xp/*+fsigp*6.0*/) return(0.0);
   return(gaus);
}
//___________________________________________________________________________
double PMTResponse::SER_peak(double *x, double *p) const {
   Double_t xx   = x[0];
   //Double_t A    = p[0]; //slope of exponential part
   //Double_t pe   = p[1]; // fraction of ents under the exponential
   Double_t xp   = p[2]; // ped
   Double_t sigp = p[3]; // ped
   //Double_t x0   = p[4]; // spe
   //Double_t sig0 = p[5]; // spe
   Double_t N    = p[6]; // norm 

   // Quick and dirty integration
   double deltax = (fXIntMax - fXIntMin) / ((double)fNIntPoints);
   double covres = 0.0;
   double tau = 0.0;
   double tau2 = 0.0;
   double Noise;
   double ser = SER0_peak(x, p);
   for (int i = 0; i < fNIntPoints; i++) {
      tau     = fXIntMin + ((double)i + 0.5) * deltax;
      tau2    = xx - tau;
      Noise   = TMath::Gaus(tau, xp,sigp, kTRUE);
      ser     = SER0_peak(&tau2, p);
      covres += Noise * ser * deltax;
   }
   return(covres * N);
}
//___________________________________________________________________________
double PMTResponse::SER0(double *x, double *p) const {
   Double_t xx   = x[0];
   Double_t A    = p[0]; //slope of exponential part
   Double_t pe   = p[1]; // fraction of ents under the exponential
   Double_t xp   = p[2]; // ped
   //Double_t sigp = p[3]; // ped
   Double_t x0   = p[4]; // spe
   Double_t sig0 = p[5]; // spe
   Double_t gN   = (1.0 + TMath::Erf(x0 / (TMath::Sqrt(2.0) * sig0))) / 2.0;
   Double_t expo = (pe / A) * TMath::Exp(-1.0 * ((xx - xp) / A));
   Double_t gaus = ((1.0 - pe) / gN) * TMath::Gaus(xx, x0 + xp, sig0, kTRUE);
   if (xx < xp/*+fsigp*6.0*/) return(gaus);
   return(expo + gaus);
}
//___________________________________________________________________________
double PMTResponse::SER(double *x, double *p) const {
   Double_t xx   = x[0];
   //Double_t A    = p[0]; //slope of exponential part
   //Double_t pe   = p[1]; // fraction of ents under the exponential
   Double_t xp   = p[2]; // ped
   Double_t sigp = p[3]; // ped
   //Double_t x0   = p[4]; // spe
   //Double_t sig0 = p[5]; // spe
   double Noise, ser;
   // Quick and dirty integration
   double deltax = (fXIntMax - fXIntMin) / ((double)fNIntPoints);
   double covres = 0.0;
   double tau = 0.0;
   double tau2 = 0.0;
   for (int i = 0; i < fNIntPoints; i++) {
      tau     = fXIntMin + ((double)i + 0.5) * deltax;
      tau2    = xx - tau;
      Noise   = TMath::Gaus(tau, xp, sigp, kTRUE);
      ser     = SER0(&tau2, &p[0]);
      covres += Noise * ser * deltax;
   }
   return(covres);
}
//___________________________________________________________________________
double PMTResponse::f_N(double *x, double *p, int n) const {
   Double_t xx   = x[0];
   //Double_t A    = p[0]; //slope of exponential part
   //Double_t pe   = p[1]; // fraction of ents under the exponential
   Double_t xp   = p[2]; // ped
   Double_t sigp = p[3]; // ped

   double deltax = (fXIntMax - fXIntMin) / ((double)fNIntPoints);
   double covres = 0.0;
   double tau = 0.0;
   double tau2 = 0.0;
   //double ser;
   //return(f_N_0(x,p,n));// * Noise * deltax;
   double Noise   = 0;//TMath::Gaus(tau, xp, sigp, kTRUE);
   for (int i = 0; i < fNIntPoints; i++) {
      tau     = fXIntMin + ((double)i + 0.5) * deltax;
      tau2    = xx - tau;
      //ser     = SER0(&tau2, &p[0]);
      Noise   = TMath::Gaus(tau, xp, sigp, kTRUE);
      covres += f_N_0(&tau2,p,n) * Noise * deltax;
   }
   return covres; 
}

//___________________________________________________________________________
double PMTResponse::f_N_0(double *x, double *p, int n) const {
   if(n < 1) return(0.0);

   double xx = x[0];

   if(n == 1) {
      return SER0(x,p);
   } else {
      double deltax = (xx - fXIntMin) / ((double)int(xx));
      double covres = 0.0;
      double tau = 0.0;
      double tau2 = 0.0;
      double ser;
      for (int i = 0; i < int(xx); i++) {
         tau     = fXIntMin + ((double)i + 0.5) * deltax;
         tau2    = xx - tau;
         ser     = SER0(&tau2, &p[0]);
         covres += f_N_0(&tau,p,n-1) * ser * deltax;
      }
      return covres; 
   }
   return(0);   
}

//___________________________________________________________________________
double PMTResponse::M_pmt(double *x, double *p) const {
   double   res   = 0;
   Double_t xx    = x[0];
   Double_t mu    = p[0];
   Double_t xp    = p[1];
   Double_t x1    = p[2];
   Double_t sig1  = p[3];

   for (int i = 2; i <= fNmax; i++) {
      auto n = (double)i;
      res += TMath::Poisson(n, mu) * TMath::Gaus(xx, n * x1 + xp, TMath::Sqrt(n) * sig1, kTRUE);
   }
   return(res);
}
//___________________________________________________________________________
double PMTResponse::M_pmt1(double *x, double *p) const {
   double   res   = 0;
   Double_t xx    = x[0];
   Double_t mu    = p[0];
   Double_t xp    = p[1];
   Double_t x1    = p[2];
   Double_t sig1  = p[3];
   Double_t sigp  = p[4];

   // Quick and dirty integration
   double deltax = (fXIntMax - fXIntMin) / ((double)fNIntPoints);
   double tau = 0.0;
   double covres = 0.0;
   double tau2 = 0.0;
   double Noise;
   for (int i = 2; i <= fNmax; i++) {
      auto n = (double)i;
      covres = 0;// TMath::Poisson(n, mu) * TMath::Gaus(xx, n * x1 + xp, TMath::Sqrt(n) * sig1, kTRUE);
      for (int i = 0; i < fNIntPoints; i++) {
         tau     = fXIntMin + ((double)i + 0.5) * deltax;
         tau2    = xx - tau;
         Noise   = TMath::Gaus(tau, xp, sigp, kTRUE);
         //ser     = SER0(&tau2, &p[0]);
         covres += TMath::Poisson(n, mu) * TMath::Gaus(tau2, n * x1 + xp, TMath::Sqrt(n) * sig1, kTRUE) * Noise * deltax;
         //covres += Noise * ser * deltax;
      }
      res += covres; //TMath::Poisson(n, mu) * TMath::Gaus(xx, n * x1 + xp, TMath::Sqrt(n) * sig1, kTRUE);
   }
   return(res);
}
//___________________________________________________________________________

double PMTResponse::PMT_fit_low_light(double *x, double *p) const
{
   Double_t res  = 0;
   //Double_t xx   = x[0];
//    fN0   = p[0];
//    fP0   = p[1];
//    fP1   = p[2];
//    fA    = p[3]; //slope of exponential part
//    fpe   = p[4]; // fraction of ents under the exponential
//    fxp   = p[5]; // ped
//    fsigp = p[6]; // ped
//    fx0   = p[7]; // spe
//    fsig0 = p[8]; // spe
//    fmu   = p[9];
//    fx1   = (1.0 - fpe) * fx0 + fpe * fA;
//    fsig1 = (1.0 - fpe) * (fsig0 * fsig0 + fx0 * fx0) + 2.0 * fpe * fA * fA - fx1 * fx1;
// 
//    Double_t Mpars[] = {fmu, fxp, fx1, fsig1};
// 
//    Double_t Noise   = TMath::Gaus(xx, fxp, fsigp, kTRUE);
//    Double_t SER     = Ser_pmt(x, &p[3]);
//    Double_t M       = M_pmt(x, Mpars);
// 
//    res = fN0 * (fP0 * Noise + fP1 * SER + M);
   return(res);
}
//___________________________________________________________________________

TF1 * PMTResponse::GetPeakFunction(Int_t n) const {
   TF1 *f1 = nullptr;
   if (n == 0) {
      f1 = new TF1(Form("pmt-peak-%d", n), "[0]*TMath::Gaus(x,[1],[2],1)", -25, 500);
      f1->SetParameter(0, fN0 * fP0);
      f1->SetParameter(1, fxp);
      f1->SetParameter(2, fsigp);
   } else if (n == 1) {
      f1 = new TF1("pmt-peak-1", this, &PMTResponse::SER_peak, -20, 500, 7, "PMTResponse", "SER_peak");
      f1->SetParameter(0, fA);
      f1->SetParameter(1, fpe);
      f1->SetParameter(2, fxp);
      f1->SetParameter(3, fsigp);
      f1->SetParameter(4, fx0);
      f1->SetParameter(5, fsig0);
      f1->SetParameter(6, fP1*fN0);
      //       f1 = new TF1(Form("pmt-peak-%d", n), "[0]*TMath::Gaus(x,[1],[2],1)", -25, 8000);
      //       f1->SetParameter(0, fN0 * fP1*((1.0 - fpe) / fgN));
      //       f1->SetParameter(1, fx0 + fxp);
      //       f1->SetParameter(2, fsig0);
   } else {
      f1 = new TF1(Form("pmt-peak-%d", n), "[0]*TMath::Gaus(x,[1],[2],1)", -25, 8000);
      f1->SetParameter(0, fN0 * TMath::Poisson(n, fmu));
      f1->SetParameter(1, n * fx1 + fxp);
      f1->SetParameter(2, TMath::Sqrt(n)*fsig1);
   }
   return(f1);
}
//___________________________________________________________________________

TF1 * PMTResponse::GetExpFunction() const
{
   TF1 *f1 = new TF1("pmt-exp", this, &PMTResponse::SER_exp, -20, 500, 7, "PMTResponse", "SER_exp");
   f1->SetParameter(0, fA);
   f1->SetParameter(1, fpe);
   f1->SetParameter(2, fxp);
   f1->SetParameter(3, fsigp);
   f1->SetParameter(4, fx0);
   f1->SetParameter(5, fsig0);
   f1->SetParameter(6, fP1*fN0);
   return(f1);
}
//___________________________________________________________________________

TF1 * PMTResponse::GetDoublePeakFunction(Int_t n) const
{
   TF1 *f1 = nullptr;
   if (n == 0) {
      f1 = new TF1(Form("pmt-peak-%d", n), "[0]*TMath::Gaus(x,[1],[2],1)", -25, 2000);
      f1->SetParameter(0, fN0 * fP0);
      f1->SetParameter(1, fxp);
      f1->SetParameter(2, fsigp);
   } else if (n == 1) {
      f1 = new TF1(Form("pmt-peak-%d", n), "[0]*TMath::Gaus(x,[1],[2],1)", -25, 2000);
      f1->SetParameter(0, fN0 * fP1*((1.0 - fpe) / fgN));
      f1->SetParameter(1, fx0 + fxp);
      f1->SetParameter(2, fsig0);
   } else {
      f1 = new TF1(Form("pmt-peak-%d", n), "[0]*TMath::Gaus(x,[1],[2],1)", -25, 2000);
      f1->SetParameter(0, fN0*fA2 * TMath::Poisson(n, 2.0*fmu));
      f1->SetParameter(1, n * fx1 + fxp);
      f1->SetParameter(2, TMath::Sqrt(n)*fsig1);
   }
   return(f1);
}
//___________________________________________________________________________

//___________________________________________________________________________

void PMTResponse2::SetParameters(double *p) {
   fN0   = p[0];
   fP0   = p[1];
   fA    = p[2]; //slope of exponential part
   fpe   = p[3]; // fraction of ents under the exponential
   fxp   = p[4]; // ped
   fsigp = p[5]; // ped
   fx0   = p[6]; // spe
   fsig0 = p[7]; // spe
   fmu   = p[8];
   fx1   = (1.0 - fpe)*fx0 + fpe*fA; //fx0;//p[10];
   fsig1 = (1.0 - fpe)*(fsig0*fsig0 + fx0*fx0) + 2.0*fpe*fA*fA - fx1*fx1; //fsig0;//p[11];
   fP1   = TMath::Poisson(1.0, fmu); //p[2];
   fgN  = (1.0 + TMath::Erf(fx0 / (TMath::Sqrt(2.0) * fsig0))) / 2.0;
}
//___________________________________________________________________________

double PMTResponse2::PMT_fit(double *x, double *p) const
{
   Double_t res  = 0;
   Double_t xx   = x[0];
   Double_t N0   = p[0];
   Double_t P0   = p[1];
   Double_t A    = p[2]; //slope of exponential part
   Double_t pe   = p[3]; // fraction of ents under the exponential
   Double_t xp   = p[4]; // ped
   Double_t sigp = p[5]; // ped
   Double_t x0   = p[6]; // spe
   Double_t sig0 = p[7]; // spe
   Double_t mu   = p[8];
   //Double_t x1   = x0;//p[10];
   //Double_t sig1 = sig0;//p[11];
   Double_t x1   = (1.0 - pe)*x0 + pe*A; //fx0;//p[10];
   Double_t sig1 = (1.0 - pe)*(sig0*sig0 + x0*x0) + 2.0*pe*A*A - x1*x1; //fsig0;//p[11];
   Double_t P1   = TMath::Poisson(1.0, mu); //p[2];

   Double_t Mpars[] = {mu, xp, x1, sig1};

   Double_t Noise   = TMath::Gaus(xx, xp, sigp, kTRUE);
   Double_t ser     = SER(x, &p[2]);
   Double_t M       = M_pmt(x, Mpars);

   res = N0 * (P0 * Noise + P1 * ser + M);
   return(res);
}
//___________________________________________________________________________

//___________________________________________________________________________

void PMTResponse3::SetParameters(double *p)
{
   fN0   = p[0];
   fP0   = p[1];
   fA    = p[2]; //slope of exponential part
   fpe   = p[3]; // fraction of ents under the exponential
   fxp   = p[4]; // ped
   fsigp = p[5]; // ped
   fx0   = p[6]; // spe
   fsig0 = p[7]; // spe
   fmu   = p[8];
   fx1   = fx0;//p[10];
   fsig1 = fsig0;//p[11];
   fP1   = TMath::Poisson(1.0, fmu); //p[2];
   fgN  = (1.0 + TMath::Erf(fx0 / (TMath::Sqrt(2.0) * fsig0))) / 2.0;
}
//___________________________________________________________________________

double PMTResponse3::PMT_fit(double *x, double *p) const
{
   Double_t res  = 0;
   Double_t xx   = x[0];
   Double_t N0   = p[0];
   Double_t P0   = p[1];
   //Double_t A    = p[2]; //slope of exponential part
   //Double_t pe   = p[3]; // fraction of ents under the exponential
   Double_t xp   = p[4]; // ped
   Double_t sigp = p[5]; // ped
   Double_t x0   = p[6]; // spe
   Double_t sig0 = p[7]; // spe
   Double_t mu   = p[8];
   Double_t x1   = x0*0.90;//p[10];
   Double_t sig1 = sig0;//p[11];
   Double_t P1   = TMath::Poisson(1.0, mu); //p[2];

   Double_t Mpars[] = {mu, xp, x1, sig1,sigp};

   Double_t Noise   = TMath::Gaus(xx, xp, sigp, kTRUE);
   Double_t ser     = SER(x, &p[2]);
   Double_t M       = M_pmt(x, Mpars);

   res = N0 * (P0 * Noise + P1 * ser + M);
   return(res);
}
//___________________________________________________________________________

//___________________________________________________________________________
void PMTResponse32::SetParameters(double *p) {
   fN0   = p[0];
   fP0   = p[1];
   fA    = p[2]; //slope of exponential part
   fpe   = p[3]; // fraction of ents under the exponential
   fxp   = p[4]; // ped
   fsigp = p[5]; // ped
   fx0   = p[6]; // spe
   fsig0 = p[7]; // spe
   fmu   = p[8];
   fx1   = p[9];
   fsig1 = p[10];
   fP1   = TMath::Poisson(1.0, fmu); //p[2];
   fgN  = (1.0 + TMath::Erf(fx0 / (TMath::Sqrt(2.0) * fsig0))) / 2.0;
}
//___________________________________________________________________________
double PMTResponse32::PMT_fit(double *x, double *p) const {
   Double_t res  = 0;
   Double_t xx   = x[0];
   Double_t N0   = p[0];
   Double_t P0   = p[1];
   //Double_t A    = p[2]; //slope of exponential part
   //Double_t pe   = p[3]; // fraction of ents under the exponential
   Double_t xp   = p[4]; // ped
   Double_t sigp = p[5]; // ped
   //Double_t x0   = p[6]; // spe
   //Double_t sig0 = p[7]; // spe
   Double_t mu   = p[8];
   Double_t x1   = p[9];
   Double_t sig1 = p[10];
   Double_t P1   = TMath::Poisson(1.0, mu); //p[2];

   Double_t Mpars[] = {mu, xp, x1, sig1,sigp};

   Double_t Noise   = TMath::Gaus(xx, xp, sigp, kTRUE);
   Double_t ser     = SER(x, &p[2]);
   Double_t M       = M_pmt(x, Mpars);

   res = N0 * (P0 * Noise + P1 * ser + M);
   return(res);
}
//___________________________________________________________________________

//___________________________________________________________________________
void PMTResponse33::SetParameters(double *p) {
   fN0   = p[0];
   //fP0   = p[1]; // Determined from mu below
   fA    = p[1]; //slope of exponential part
   fpe   = p[2]; // fraction of ents under the exponential
   fxp   = p[3]; // ped
   fsigp = p[4]; // ped
   fx0   = p[5]; // spe
   fsig0 = p[6]; // spe
   fmu   = p[7];
   fx1   = p[8];
   fsig1 = p[9];
   fP0   = TMath::Poisson(0.0, fmu); //p[2];
   fP1   = TMath::Poisson(1.0, fmu); //p[2];
   fgN  = (1.0 + TMath::Erf(fx0 / (TMath::Sqrt(2.0) * fsig0))) / 2.0;
}
//___________________________________________________________________________
double PMTResponse33::PMT_fit(double *x, double *p) const {
   Double_t res  = 0;
   Double_t xx   = x[0];
   Double_t N0   = p[0];
   //Double_t P0   = p[1];
   Double_t A    = p[1]; //slope of exponential part
   Double_t pe   = p[2]; // fraction of ents under the exponential
   Double_t xp   = p[3]; // ped
   Double_t sigp = p[4]; // ped
   Double_t x0   = p[5]; // spe
   Double_t sig0 = p[6]; // spe
   Double_t mu   = p[7];
   Double_t x1   = p[8];
   Double_t sig1 = p[9];
   Double_t P0   = TMath::Poisson(0.0, mu); 
   Double_t P1   = TMath::Poisson(1.0, mu); 

   Double_t Mpars[] = {mu, xp, x1, sig1,sigp};

   Double_t Noise   = TMath::Gaus(xx, xp, sigp, kTRUE);
   Double_t ser     = SER(x, &p[1]);
   Double_t M       = M_pmt(x, Mpars);

   res = N0 * (P0 * Noise + P1 * ser + M);
   return(res);
}
//___________________________________________________________________________

//___________________________________________________________________________

void PMTResponse4::SetParameters(double *p)
{
   fN0   = p[0];
   fP0   = p[1];
   fP1   = p[2];
   fA    = p[3]; //slope of exponential part
   fpe   = p[4]; // fraction of ents under the exponential
   fxp   = p[5]; // ped
   fsigp = p[6]; // ped
   fx0   = p[7]; // spe
   fsig0 = p[8]; // spe
   fmu   = p[9];
   fx1   = p[10];
   fsig1 = p[11];
   fA2   = p[12];
   fgN  = (1.0 + TMath::Erf(fx0 / (TMath::Sqrt(2.0) * fsig0))) / 2.0;
}
//___________________________________________________________________________

double PMTResponse4::PMT_fit(double *x, double *p) const
{
   Double_t res  = 0;
   Double_t xx   = x[0];

   Double_t N0   = p[0];
   Double_t P0   = p[1];
   Double_t P1   = p[2];
   //Double_t A    = p[3]; //slope of exponential part
   //Double_t pe   = p[4]; // fraction of ents under the exponential
   Double_t xp   = p[5]; // ped
   Double_t sigp = p[6]; // ped
   //Double_t x0   = p[7]; // spe
   //Double_t sig0 = p[8]; // spe
   Double_t mu   = p[9];
   Double_t x1   = p[10];
   Double_t sig1 = p[11];
   Double_t A2   = p[12];

   Double_t Mpars[] = {mu, xp, x1, sig1};
   Double_t M       = M_pmt(x, Mpars);

   Double_t M2pars[] = {2.0*mu, xp, x1, sig1};
   Double_t M2       = M_pmt(x, M2pars);

// Quick and dirty integration
   double deltax = (fXIntMax - fXIntMin) / ((double)fNIntPoints);
   double covres = 0.0;
   double tau = 0.0;
   double Noise   = 0.0;
   double ser     = 0.0;
   for (int i = 0; i < fNIntPoints; i++) {
      tau     = fXIntMin + ((double)i + 0.5) * deltax;
      Noise   = TMath::Gaus(xx - tau, xp, sigp, kTRUE);
      ser     = SER0(&tau, &p[3]);
      covres += Noise * ser * deltax;
   }

   Noise   = TMath::Gaus(xx, xp, sigp, kTRUE);
   res = N0 * (P0 * Noise + P1 * covres + M + A2*M2);
   return(res);
}
//___________________________________________________________________________


//___________________________________________________________________________
void PMTResponse42::SetParameters(double *p) {
   fN0   = p[0];
   fA    = p[1]; //slope of exponential part
   fpe   = p[2]; // fraction of ents under the exponential
   fxp   = p[3]; // ped
   fsigp = p[4]; // ped
   fx0   = p[5]; // spe
   fsig0 = p[6]; // spe
   fmu   = p[7];
   fx1   = p[8];
   fsig1 = p[9];
   fA2   = p[10];
   //fSmear= p[11];
   fP0   = TMath::Poisson(0.0, fmu); 
   fP1   = TMath::Poisson(1.0, fmu); 
   fgN  = (1.0 + TMath::Erf(fx0 / (TMath::Sqrt(2.0) * fsig0))) / 2.0;
}
//___________________________________________________________________________
double PMTResponse42::PMT_fit(double *x, double *p) const {
   Double_t res  = 0;
   Double_t xx   = x[0];

   Double_t N0   = p[0];
   Double_t A    = p[1]; //slope of exponential part
   Double_t pe   = p[2]; // fraction of ents under the exponential
   Double_t xp   = p[3]; // ped
   Double_t sigp = p[4]; // ped
   Double_t x0   = p[5]; // spe
   Double_t sig0 = p[6]; // spe
   Double_t mu   = p[7];
   Double_t x1   = p[8];
   Double_t sig1 = p[9];
   Double_t A2   = p[10];
   //Double_t smear= 80.0;
   Double_t P0   = TMath::Poisson(0.0, mu); 
   Double_t P1   = TMath::Poisson(1.0, mu); 

   // Quick and dirty integration
   double deltax = (fXIntMax - fXIntMin) / ((double)fNIntPoints);
   double covres = 0.0;
   double tau    = 0.0;
   double Noise  = 0.0;
   double ser    = 0.0;
   for(int i = 0; i < fNIntPoints; i++) {
      tau     = fXIntMin + ((double)i + 0.5) * deltax;
      Noise   = TMath::Gaus(xx - tau, xp, sigp, kTRUE);
      ser     = SER0(&tau, &p[1]);
      covres += Noise * ser * deltax;
   }

   Double_t Mpars[] = {mu, xp, x1, sig1};
   Double_t M       = M_pmt(x, Mpars);

   Double_t M2pars[] = {2.0*mu, xp, x1, sig1};
   Double_t M2       = M_pmt(x, M2pars);

   //int Nint2 = 30;
   //double deltax2 = (fXIntMax - fXIntMin) / (double(Nint2));
   //double M_conv  = 0.0;
   //double M2_conv = 0.0;
   //for(int i = 0; i < Nint2; i++) {
   //   tau     = fXIntMin + ((double)i + 0.5) * deltax2;
   //   Noise   = TMath::Gaus(xx - tau, 0.0 , smear, kTRUE);
   //   M       = M_pmt(&tau, Mpars);
   //   M2      = M_pmt(&tau, M2pars);
   //   M_conv  += Noise * M * deltax;
   //   M2_conv += Noise * M2 * deltax;
   //}

   Noise   = TMath::Gaus(xx, xp, sigp, kTRUE);
   //res = N0 * (P0 * Noise + P1 * covres + M_conv + A2*M2_conv);
   res = N0 * (P0 * Noise + P1 * covres + M + A2*M2);
   return(res);
}
//___________________________________________________________________________


//___________________________________________________________________________
void PMTResponse5::SetParameters(double *p) {
   fN0   = p[0];
   fP0   = p[1];
   fP1   = p[2];
   fA    = p[3]; //slope of exponential part
   fpe   = p[4]; // fraction of ents under the exponential
   fxp   = p[5]; // ped
   fsigp = p[6]; // ped
   fx0   = p[7]; // spe
   fsig0 = p[8]; // spe
   fmu   = p[9];
   fx1   = p[10];
   fsig1 = p[11];
   fA2   = p[12];
   fA0   = p[13];
   fgN  = (1.0 + TMath::Erf(fx0 / (TMath::Sqrt(2.0) * fsig0))) / 2.0;
}
//___________________________________________________________________________

double PMTResponse5::PMT_fit(double *x, double *p) const
{
   Double_t res  = 0;
   Double_t xx   = x[0];

   Double_t N0   = p[0];
   Double_t P0   = p[1];
   Double_t P1   = p[2];
   Double_t A    = p[3]; //slope of exponential part
   //Double_t pe   = p[4]; // fraction of ents under the exponential
   Double_t xp   = p[5]; // ped
   Double_t sigp = p[6]; // ped
   //Double_t x0   = p[7]; // spe
   //Double_t sig0 = p[8]; // spe
   Double_t mu   = p[9];
   Double_t x1   = p[10];
   Double_t sig1 = p[11];
   Double_t A2   = p[12];
   Double_t A0   = p[13];

   Double_t Mpars[] = {mu, xp, x1, sig1};
   Double_t M       = M_pmt(x, Mpars);

   Double_t M2pars[] = {2.0*mu, xp, x1, sig1};
   Double_t M2       = M_pmt(x, M2pars);

// Quick and dirty integration
   double deltax = (fXIntMax - fXIntMin) / ((double)fNIntPoints);
   double covres = 0.0;
   double tau = 0.0;
   double Noise   = 0.0;
   double ser     = 0.0;
   for (int i = 0; i < fNIntPoints; i++) {
      tau     = fXIntMin + ((double)i + 0.5) * deltax;
      Noise   = TMath::Gaus(xx - tau, xp, sigp, kTRUE);
      ser     = SER0(&tau, &p[3]);
      covres += Noise * ser * deltax;
   }

   Noise   = TMath::Gaus(xx, xp, sigp, kTRUE);
   res = N0 * (P0 * Noise + P1 * covres + M + A2*M2 + A0*TMath::Exp(-(xx-xp)/A) );
   return(res);
}
//___________________________________________________________________________


TF1 * PMTResponse5::GetBackgroundExpFunction() const
{
   auto *f1 = new TF1("pmt-bg-exp","[0]*TMath::Exp(-(x-[1])/[2])",-20,6000);
   f1->SetParameter(0, fN0*fA0);
   f1->SetParameter(1, fxp);
   f1->SetParameter(2, fA);
   return(f1);
}
}

