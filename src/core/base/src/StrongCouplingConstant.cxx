#include <iostream>
#include "StrongCouplingConstant.h"
namespace insane {
namespace physics {

StrongCouplingConstant::StrongCouplingConstant() 
   : n_f(3.0), fLambda(0.34)
{ }
StrongCouplingConstant::~StrongCouplingConstant() {}
double StrongCouplingConstant::operator()(double mu2){
   double t  = TMath::Log(mu2/(fLambda*fLambda));
   double b_0 = b0(mu2);
   //std::cout << "mu2 = " << mu2 << std::endl;
   //std::cout << "lam = " << fLambda << std::endl;
   //std::cout << " t  = " << t << std::endl;
   //std::cout << " b0 = " << b_0 << std::endl;
   return( 1.0/(b_0*t) );
}
double StrongCouplingConstant::b0(double mu2) const { 
   return( (33.0-2.0*n_f)/(12.0*TMath::Pi()))  ;
   //return( (11.0*C_A - 4.0*n_f*T_R)/(12.0*TMath::Pi()))  ;
}
double StrongCouplingConstant::b1(double mu2) const {
   return( (153.0-19.0*n_f)/(24.0*TMath::Pi()*TMath::Pi()) );
} 
double StrongCouplingConstant::b2(double mu2) const {
   return( (2857.0 - (5033.0/9.0)*n_f + (325.0/27.0)*n_f*n_f)/(128.0*TMath::Pi()*TMath::Pi()*TMath::Pi()) );
}
double StrongCouplingConstant::b3(double mu2) const {
   return 0.0;
}
}}

