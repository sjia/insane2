#include "Physics.h"
#include "Kinematics.h"

#include "Math.h"
#include "TMath.h"
#include "InSANEMathFunc.h"

using namespace TMath;
using namespace insane::units;

namespace insane {
  
  namespace physics {

    double F1(double Q2, double GE, double GM)
    {
      double t  = Q2/(4.0*0.938*0.938);
      return (GE + GM*t)/(1.0+t);
    }

    double F2(double Q2, double GE, double GM)
    {
      double t  = Q2/(4.0*0.938*0.938);
      return (GM - GE)/(1.0+t);
    }

    double F_total_squared(std::array<double,2> sachs_FFs, const kinematics::ElasticKinematics& kin)
    {
      double Epsilon_prime_inverse = 1.0/kin.tilde_Epsilon_prime;
      double t0                    = Epsilon_prime_inverse / (1.0 + kin.tau);
      return t0*(sachs_FFs.at(0)/Epsilon_prime_inverse + kin.tau*sachs_FFs.at(1));
    }

    double Elastic_XS(std::array<double, 2> FFs, const kinematics::ElasticKinematics& kin)
    {
      using namespace insane::units;
      using namespace kinematics;
      double mott      = Mott_XS( kin);
      double F_rec     = F_recoil(kin);
      double F_squared = F_total_squared(FFs, kin);
      return mott*F_squared/F_rec;
    }

    double R(double F1, double F2, double x, double Q2)
    {
      double res = F2 / (2.0 * x * F1) ;
      res = res * (1 + 4.0 * (M_p/GeV)*(M_p/GeV)* x * x / Q2) - 1.0;
      return res;
    }

    // Soffer-Teryaev  Positivity bound.
    // |A_2| < \sqrt{R(1+A_1)/2}
    double A2_PositivityBound(double F_1, double F_2, double A_1, double x, double Q2){
      //double F_1 = F1(updfs, target, x, Q2);
      //double F_2 = F2(updfs, target, x, Q2);
      double RLT = insane::physics::R(F_1,F_2,x,Q2);
      //double A_2 = A2_NoTMC(updfs,ppdfs,target,x,Q2);
      return TMath::Sqrt(RLT*(1.0+A_1)/2.0 );
    }
    double A2_PositivityBound(double R, double A_1, double x, double Q2){
      return TMath::Sqrt( R*(1.0+A_1)/2.0 );
    }

    double WW(std::function<double(double)> func, double x, double max)
    {
      return( -1.0*func(x) + insane::integrate::simple(func,x,max) );
    }

    namespace TMCs {

      double h2(std::function<double(double)> F1,double xi, double Q2)
      {
        // Target Mass correction function
        // J. Phys. G 35, 053101 (2008)
        return(insane::integrate::gaus(
              [&](double z){ return F1(z)/(z*z);}, xi, 1.0) );
      }

      double g2(std::function<double(double)> F1,double xi, double Q2)
      {
        // J. Phys. G 35, 053101 (2008)
        return(insane::integrate::gaus(
              [&](double z){ return F1(z)*(z-xi)/(z*z);}, xi, 1.0) );
      }

    }

    double CN_moment(std::function<double(double)> f, int n, double x_low, double x_high)
    {
      return(
        insane::integrate::simple([&](double x){ return f(x)*TMath::Power(x,double(n)); },
                                x_low,
                                x_high,
                                50)
        );
    }

  } // insane::physics


  namespace kinematics {

  }

}
