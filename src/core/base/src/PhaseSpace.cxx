#include "PhaseSpace.h"

#include "TString.h"
#include "TClonesArray.h"
#include <iostream>
#include <vector>


namespace insane {
  namespace physics {

    PhaseSpace::PhaseSpace()
    {
      //fVariables.SetOwner(true);
      fVariables.clear();
      fnDim             = 0;
      fnIndependentVars = 0;
      fIsModified       = false;
      fScaledIntegralJacobian = 1.0;
    }
    //______________________________________________________________________________

    //PhaseSpace::PhaseSpace(const PhaseSpace& rhs) : 
    //  TObject(rhs), fnDim(rhs.fnDim), fnIndependentVars(rhs.fnIndependentVars), fIsModified(rhs.fIsModified)
    //{
    //  fVariables.AddAll(&(rhs.fVariables));
    //}
    //______________________________________________________________________________

    PhaseSpace::~PhaseSpace()
    { }

    //PhaseSpace& PhaseSpace::operator=(const PhaseSpace& rhs)
    //{
    //  fVariables              = rhs.fVariables ;
    //  fnDim                   = rhs.fnDim                   ;
    //  fnIndependentVars       = rhs.fnIndependentVars       ;
    //  fIsModified             = rhs.fIsModified             ;
    //  fScaledIntegralJacobian = rhs.fScaledIntegralJacobian ;
    //}

    void PhaseSpace::Print(Option_t * opt ) const {
      //std::cout << "    Phase Space Address   : " << this << "\n";
      std::cout << std::setw(25) << std::right <<  "Phase Space Dimension : " << fnDim << "\n";
      const int N  = fVariables.size();
      if (N != fnDim) std::cout << " ARRAY SIZE AND DIMENSION DO NOT MATCH!!!!\n";
      for (int i = 0; i < N; i++) fVariables.at(i).Print();
    }
    //______________________________________________________________________________
    void PhaseSpace::Print(std::ostream& stream) const {
      //std::cout << "    Phase Space Address   : " << this << "\n";
      stream << std::setw(25) << std::right <<  "Phase Space Dimension : " << fnDim << "\n";
      const int N  = fVariables.size();
      if (N != fnDim) stream << " ARRAY SIZE AND DIMENSION DO NOT MATCH!!!!\n";
      for (int i = 0; i < N; i++) fVariables.at(i).Print(stream);
    }
    //______________________________________________________________________________
  }
}

