!File E.12. R1990.FORTRN.                                               R1900010
!Reference:  L.W.Whitlow, SLAC-Report-357,                              R1900020
!            Ph.D. Thesis, Stanford University,                         R1900030
!            March 1990.                                                R1900040
!For details see file HELP.DOCUMENT.                                    R1900050
                                                                        !R1900060
!Program contains 135 lines of Fortran code, of 72 characters each, withR1900070
!one subroutine.                                                        R1900080
                                                                        !R1900090
                                                                        !R1900100
      SUBROUTINE R1990(X,Q2,R,DR,GOODFIT)                               !R1900110
                                                                        !R1900120
!----------------------------------------------------------------       R1900130
! X      : Bjorken x                                                    R1900140
! Q2     : Q squared in (GeV/c)**2                                      R1900150
! R      :                                                              R1900160
! DR     : Absolute error on R                                          R1900170
! GOODFIT:  = .TRUE. if the X,Q2 are within the range of the fit.       R1900180
!-------------------------------------------------------------------    R1900190
! Model for R, based on a fit to world R measurements. Fit performed by R1900200
! program RFIT8 in pseudo-gaussian variable: log(1+.5R).  For details   R1900210
! see Reference.                                                        R1900220
!                                                                       R1900230
! Three models are used, each model has three free parameters.  The     R1900240
! functional forms of the models are phenomenological and somewhat      R1900250
! contrived.  Each model fits the data very well, and the average of    R1900260
! the fits is returned.  The standard deviation of the fit values is    R1900270
! used to estimate the systematic uncertainty due to model dependence.  R1900280
!                                                                       R1900290
! Statistical uncertainties due to fluctuations in measured values have R1900300
! have been studied extensively.  A parametrization of the statistical  R1900310
! uncertainty of R1990 is presented in FUNCTION DR1990.                 R1900320
!                                                                       R1900330
! The three model forms are given by:                                   R1900340
!                                                                       R1900350
!     R_A = A(1)/LOG(Q2/.04)*FAC + A(2)/[Q2[4+A(3)[4][.25 ;             R1900360
!     R_B = B(1)/LOG(Q2/.04)*FAC + B(2)/Q2 + B(3)/(Q2**2+.3**2) ;       R1900370
!     R_C = C(1)/LOG(Q2/.04)*FAC + C(2)/[(Q2-Q2thr)[2+C(3)[2][.5 ,      R1900380
!                               ...where Q2thr = 5(1-X)[5 ;             R1900390
!           where FAC = 1+12[Q2/(1+Q2)][.125[2/(.125[2+x[2)] gives the  R1900400
!           x-dependence of the logarithmic part in order to match Rqcd R1900410
!           at high Q2.                                                 R1900420
!                                                                       R1900430
! Each model fits very well.  As each model has its own strong points   R1900440
! and drawbacks, R1990 returns the average of the models.  The          R1900450
! chisquare for each fit (124 degrees of freedom) are:                  R1900460
!     R_A: 110,    R_B: 110,    R_C: 114,    R1990(=avg): 108           R1900470
!                                                                       R1900480
! This subroutine returns reasonable values for R for all x and for all R1900490
! Q2 greater than or equal to .3 GeV.                                   R1900500
!                                                                       R1900510
! The uncertainty in R originates in three sources:                     R1900520
!                                                                       R1900530
!     D1 = uncertainty in R due to statistical fluctuations of the data R1900540
!          and is parameterized in FUNCTION DR1990, for details see     R1900550
!          Reference.                                                   R1900560
!                                                                       R1900570
!     D2 = uncertainty in R due to possible model dependence, approxi-  R1900580
!          mated by the variance between the models.                    R1900590
!                                                                       R1900600
!     D3 = uncertainty in R due to possible epsilon dependent errors    R1900610
!          in the radiative corrections, taken to be +/- .025.  See     R1900620
!          theses (mine or Dasu's) for details.                         R1900630
!                                                                       R1900640
! and the total error is returned by the program:                       R1900650
!                                                                       R1900660
!     DR = is the total uncertainty in R, DR = sqrt(D1[2+D2[2+D3[2).    R1900670
!          DR is my best estimate of how well we have measured R.  At   R1900680
!          high Q2, where R is small, DR is typically larger than R.  IfR1900690
!          you have faith in QCD, then, since R1990 = Rqcd at high Q2,  R1900700
!          you might wish to assume DR = 0 at very high Q2.             R1900710
!                                                                       R1900720
! NOTE:    In many applications, for example the extraction of F2 from  R1900730
!          measured cross section, you do not want the full error in R  R1900740
!          given by DR.  Rather, you will want to use only the D1 and D2R1900750
!          contributions, and the D3 contribution from radiative        R1900760
!          corrections propogates complexely into F2.  For more informa-R1900770
!          tion, see the documentation to dFRC in HELP.DOCUMENT, or     R1900780
!          for explicite detail, see Reference.                         R1900790
!                                                                       R1900800
!    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^R1900810
      IMPLICIT NONE                                                     !R1900820
      REAL FAC,RLOG,Q2THR,R_A,R_B,R_C,R, D1,D2,D3,DR,DR1990,X,Q2        !R1900830
      REAL A(3)  / .06723, .46714, 1.89794 /,                           !R1900840
     >     B(3)  / .06347, .57468, -.35342 /,                           !R1900850
     >     C(3)  / .05992, .50885, 2.10807 /                            !R1900860
      LOGICAL GOODFIT                                                   !R1900870
!    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^R1900880
                                                                        !R1900890
      FAC   = 1+12.*(Q2/(1.+Q2))*(.125**2/(X**2+.125**2))               !R1900900
      RLOG  = FAC/LOG(Q2/.04)!   <--- we use natural logarithms only!   R1900910
      Q2thr = 5.*(1.-X)**5                                              !R1900920
                                                                        !R1900930
      R_A   = A(1)*RLOG + A(2)/SQRT(SQRT(Q2**4+A(3)**4))                !R1900940
      R_B   = B(1)*RLOG + B(2)/Q2 + B(3)/(Q2**2+.3**2)                  !R1900950
      R_C   = C(1)*RLOG + C(2)/SQRT((Q2-Q2thr)**2+C(3)**2)              !R1900960
      R     = (R_A+R_B+R_C)/3.                                          !R1900970
                                                                        !R1900980
      D1    = DR1990(X,Q2)                                              !R1900990
      D2    = SQRT(((R_A-R)**2+(R_B-R)**2+(R_C-R)**2)/2.)               !R1901000
      D3    = .023*(1.+.5*R)                                            !R1901010
              IF (Q2.LT.1.OR.X.LT..1) D3 = 1.5*D3                       !R1901020
      DR    = SQRT(D1**2+D2**2+D3**2)                                   !R1901030
                                                                        !R1901040
      IF (Q2.LT..3) GOODFIT = .FALSE.                                   !R1901050
      RETURN                                                            !R1901060
      END                                                               !R1901070
                                                                        !R1901080
!^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^R1901090
                                                                        !R1901100
      FUNCTION DR1990(X,Q2)                                             !R1901110
                                                                        !R1901120
! Parameterizes the uncertainty in R1990 due to the statistical         !R1901130
! fluctuations in the data.  Values reflect an average of the R-values  R1901140
! about a neighborhood of the specific (x,Q2) value.  That neighborhood R1901150
! is of size [+/-.05] in x, and [+/-33%] in Q2.  For details, see       R1901160
! Reference.                                                            R1901170
!                                                                       R1901180
! This subroutine is accurate over all (x,Q2), not only the SLAC deep   R1901190
! inelastic range.  Where there is no data, for example in the resonanceR1901200
! region, it returns a realistic uncertainty, extrapolated from the deepR1901210
! inelastic region (suitably enlarged).  We similarly estimate the      R1901220
! uncertainty at very large Q2 by extrapolating from the highest Q2     R1901230
! measurments.  For extremely large Q2, R is expected to fall to zero,  R1901240
! so the uncertainty in R should not continue to grow.  For this reason R1901250
! DR1990 uses the value at 64 GeV for all larger Q2.                    R1901260
!                                                                       R1901270
! XHIGH accounts for the rapidly diminishing statistical accuracy for   R1901280
! x>.8, and does not contribute for smaller x.                          R1901290
                                                                        !R1901300
                                                                        !R1901310
      IMPLICIT NONE                                                     !R1901320
!      REAL U(10,10)
      REAL DR1990,QMAX,Q,S,A,XLOW,XHIGH,Q2,X                            !R1901330
                                                                        !R1901340
                                                                        !R1901350
      QMAX = 64.                                                        !R1901360
                                                                        !R1901370
      Q = MIN(Q2,QMAX)                                                  !R1901380
      S = .006+.03*X**2                                                 !R1901390
      A = MAX(.05,8.33*X-.66)                                           !R1901400
                                                                        !R1901410
      XLOW  = .020+ABS(S*LOG(Q/A))                                      !R1901420
      XHIGH = .1*MAX(.1,X)**20/(.86**20+MAX(.1,X)**20)                  !R1901430
                                                                        !R1901440
      DR1990 = SQRT(XLOW**2+XHIGH**2)                                   !R1901450
      RETURN                                                            !R1901460
      END                                                               !R1901470
!                                                                       R1901480
!End of file R1990.FORTRN.  135 Fortran lines.                          R1901490
