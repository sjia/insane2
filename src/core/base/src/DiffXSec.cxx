#include "DiffXSec.h"
//#include "F1F209StructureFunctions.h"
//#include "PolarizedStructureFunctionsFromPDFs.h"
//#include "DipoleFormFactors.h"

#include "FormFactors.h"
#include "StructureFunctions.h"
#include "PolarizedStructureFunctions.h"

namespace insane {
namespace physics {

DiffXSec::DiffXSec() :
   fIncludeJacobian(false),fIsModified(true),fUsePhaseSpace(true),
   //fPhaseSpace(nullptr),
   fID(-1), fBaseID(0),fnDim(3)
{
   // By default a single particle.
   fMaterialIndex                 = -1;
   fnDim                          = 3;
   //fPhaseSpace                    = nullptr;
   fnParticles                    = 1;
   fnPrimaryParticles             = fnParticles;
   fPIDs.push_back(11); //electron
   fTitle                         = "";
   fPlotTitle                     = "";
   fLabel                         = "#frac{d#sigma}{dE'd#Omega}";
   fUnits                         = "nb/GeV/sr";
   fBeamEnergy                    = 5.9;

   fVariableNames.SetOwner(true);
   fVariableNames.Clear();

   fAddedXSections.SetOwner(true);
   fAddedXSections.Clear();
   
   fTargetNucleus = Nucleus::Proton();

   //fParticles.SetOwner(true);
   fParticles.Clear();

   fHelicity = 0.0;
   fTargetPol.SetMagThetaPhi(0.0,180*degree,0.0);
   fTotalXSec = 0.0;

   fStructureFunctions          = nullptr;//new DefaultStructureFunctions();
   fPolarizedStructureFunctions = nullptr;//new DefaultPolarizedStructureFunctions();
   fFormFactors                 = nullptr;//new DefaultFormFactors();
}
//_______________________________________________________________________

DiffXSec::~DiffXSec()
{
   fPIDs.clear();
   fScaleFactors.clear();
   fOffsets.clear();
   fVariableNames.Clear();
   fAddedXSections.Clear();
   fParticles.Clear();
   //if (fPhaseSpace) delete fPhaseSpace;
   //fPhaseSpace = nullptr;
}
//_______________________________________________________________________

DiffXSec::DiffXSec(const DiffXSec& old)
{
   (*this) = old;
}
//_______________________________________________________________________

DiffXSec& DiffXSec::operator=(const DiffXSec& rhs)
{
   if (this != &rhs) {  // make sure not same object
      fIncludeJacobian      = rhs.fIncludeJacobian                     ;
      fIsModified           = rhs.fIsModified                          ;
      fUsePhaseSpace        = rhs.fUsePhaseSpace                       ;
      fPhaseSpace           = rhs.fPhaseSpace                     ;
      //mutable Double_t      fVars[15]                                  ;
      fPIDs                 = rhs.fPIDs                                  ;
      fnDim                 = rhs.fnDim                                  ;
      fnParticles           = rhs.fnParticles                            ;
      fnParticleVars        = rhs.fnParticleVars                         ;
      //fAddedXSections       = rhs.fAddedXSections                        ;
      fParticles.Clear();
      fParticles.AddAll(&rhs.fParticles)                             ;
      fVariableNames.Clear();
      fVariableNames.AddAll(&rhs.fVariableNames )                        ;
      fBeamEnergy           = rhs.fBeamEnergy                            ;
      fTotalXSec            = rhs.fTotalXSec                             ;
      fHelicity             = rhs.fHelicity                              ;
      fTargetPol            = rhs.fTargetPol                             ;
      fTargetNucleus        = rhs.fTargetNucleus                     ;
      fTargetMaterial       = rhs.fTargetMaterial                    ;
      fMaterialIndex        = rhs.fMaterialIndex                     ;

      fScaleFactors         = rhs.fScaleFactors                            ;
      fOffsets              = rhs.fOffsets                                 ;

      //mutable Double_t      fNormalizedVariables[15]                   ;
      //mutable Double_t      fUnnormalizedVariables[15]                 ;
      //mutable Double_t      fDependentVariables[15]                    ;

      fTitle                        = rhs.fTitle                                       ;
      fPlotTitle                    = rhs.fPlotTitle                                   ;
      fLabel                        = rhs.fLabel                                       ;
      fUnits                        = rhs.fUnits                                       ;
      fStructureFunctions           = rhs.fStructureFunctions          ;
      fPolarizedStructureFunctions  = rhs.fPolarizedStructureFunctions ;
      fFormFactors                  = rhs.fFormFactors                 ;

      //fnDim = old.fnDim;
      //fnParticles = old.fnParticles;
      //fPIDs = old.fPIDs;
      //fTitle = old.fTitle;
      //fPlotTitle = old.fPlotTitle;
      //fBeamEnergy = old.fBeamEnergy;
   }
   return *this;    // Return ref for multiple assignment
}
//_______________________________________________________________________

DiffXSec*  DiffXSec::Clone(const char * ) const
{
   std::cout << "DiffXSec::Clone()\n";
   auto * copy = new DiffXSec();
   (*copy) = (*this);
   return copy;
}
//______________________________________________________________________________

Double_t DiffXSec::Density(Int_t ndim, Double_t * x) {
      //std::cout << " called Density in dummy base class!!\n;";
      return(EvaluateXSec(GetDependentVariables(GetUnnormalizedVariables(x))));
}

//_______________________________________________________________________
Double_t DiffXSec::EvaluateCurrent() const
{
      for(int i=0;i<fPhaseSpace.GetDimension();i++) fVars[i] = 0; 
      /*      std::cout << vars[0] << " , " << vars[1] << "\n";*/
      fPhaseSpace.GetEventValues(fVars);
      return(EvaluateXSec(GetDependentVariables(fVars)));
}

//_______________________________________________________________________

Double_t DiffXSec::EvaluateXSec(const Double_t * x) const
{
      std::cout << "You need to define EvaluateXSec in the derived class!!" << std::endl;
      return(0);
}
//______________________________________________________________________________

Double_t DiffXSec::Jacobian(const Double_t * x) const
{
   return( TMath::Sin(x[1]));
}
//_______________________________________________________________________

bool DiffXSec::VariablesInPhaseSpace(const Int_t ndim, const Double_t * x) const {
   // also checks that the variables are not NaN
   for(int i = 0; i < ndim; i++) {
      if( std::isnan(x[i]) ) {
         Warning("VariablesInPhaseSpace","Variable %d is NaN",i);
         return false;
      }
   }
   //if (!fPhaseSpace) {
   //   std::cout << "[DiffXSec::VariablesInPhaseSpace]: No phase space defined \n";
   //   return(false);
   //}
   fPhaseSpace.SetEventValues(x,fPhaseSpace.GetDimension());//ndim); // allowed because PhaseSpaceVariable::fCurrentValue is mutable
   // ndim is the total number of phase space variables.
   if( !fUsePhaseSpace ) return true;
   bool result = true;
   if (ndim < fnDim) {
      // phase space must have the same or more variables than the differential
      std::cout << " Dimensions Do Not Match! \n";
      std::cout << "    VariablesInPhaseSpace argument ndim = " << ndim << " \n";
      std::cout << "                  DiffXSec::fnDim = " << fnDim << " \n";
      return(false);
   }
   for (int i = 0; i < ndim /*fPhaseSpace->GetDimension()*/; i++) {
      if (!(fPhaseSpace.GetVariable(i).IsInVariableRange())) {
         result = false;
         //std::cout << "DiffXSec::VariablesInPhaseSpace  " << i << " out of range: " << x[i] << std::endl;
         //if(!(fPhaseSpace->GetVariable(i)->IsDependent())) {
         //   std::cout << "DiffXSec::VariablesInPhaseSpace - independent variable " << i << " out of range: " << x[i] << std::endl;
         //}
         break;
      }
   }
   return(result);
}
//_______________________________________________________________________

Double_t DiffXSec::CalculateTotalCrossSection()
{
   Double_t result = 0.0;
   Double_t a[30], b[30];
   for (int i = 0; i < GetPhaseSpace()->GetDimension() && i < 30; i++) {
      a[i] = GetPhaseSpace()->GetVariable(i)->GetMinimum();
      b[i] = GetPhaseSpace()->GetVariable(i)->GetMaximum();
   }
   // ROOT::Math::IntegratorMultiDim  ig2(ROOT::Math::IntegrationMultiDim::kPLAIN);
   // ROOT::Math::GSLMCIntegrator    ig2(ROOT::Math::IntegrationMultiDim::kVEGAS);
   ROOT::Math::IntegratorMultiDim     ig2(ROOT::Math::IntegrationMultiDim::kVEGAS);
   ig2.SetFunction((*this));
   /* ig2.SetRelTolerance(0.05);*/
   result = ig2.Integral(a, b);
   std::cout << " DiffXSec::CalculateTotalCrossSection() integral result is " << result << std::endl;
   fTotalXSec = result;
   return(result);
}
//_______________________________________________________________________
void DiffXSec::Print(std::ostream& stream) const {
   //std::cout << " --------------------------------------------------------- " << std::endl;
   stream << "    - cross section : " << fTitle.Data() << "\n";
   stream << "      title         : " << fPlotTitle.Data() << "\n";
   /*      std::cout << "  Cross Section address : " << this << "\n";*/
   stream << "      N Particles   : " << fnParticles << "\n";
   stream << "      Beam Energy   : " << fBeamEnergy << std::endl; 
   stream << "      Jacobian      : " << fIncludeJacobian << std::endl;
   fTargetNucleus.Print(stream);
   stream << "      Phase Space   :  " << "\n";
   fPhaseSpace.Print(stream);
   stream << "                 PIDs : " ;
   for(unsigned int i=0; i< fPIDs.size(); i++) { std::cout << fPIDs[i] << " " ; }
   stream << std::endl;
   stream << "               sigma  : " << EvaluateCurrent() << " nb/GeV/sr"  << std::endl;
   //PrintTable();
   stream << " --------------------------------------------------------- " << std::endl;
}
//_______________________________________________________________________
void DiffXSec::Print(const Option_t * opt) const {
  Print(std::cout);
  ////std::cout << " --------------------------------------------------------- " << std::endl;
  //std::cout << "      XSection title  : " << fTitle.Data() << "\n";
  //std::cout << "          plot title  : " << fPlotTitle.Data() << "\n";
  ///*      std::cout << "  Cross Section address : " << this << "\n";*/
  //std::cout << "          fnParticles : " << fnParticles << "\n";
  //std::cout << "          Beam Energy : " << fBeamEnergy << std::endl; 
  //std::cout << "             Jacobian : " << fIncludeJacobian << std::endl;
  //fTargetNucleus.Print();
  //std::cout << "          Phase Space :  " << "\n";
  //if (fPhaseSpace){
  //   fPhaseSpace->Print();
  //}
  //std::cout << "                 PIDs : " ;
  //for(unsigned int i=0; i< fPIDs.size(); i++) { std::cout << fPIDs[i] << " " ; }
  //std::cout << std::endl;
  //std::cout << "               sigma  : " << EvaluateCurrent() << " nb/GeV/sr"  << std::endl;
  ////PrintTable();
  //std::cout << " --------------------------------------------------------- " << std::endl;
}
//_______________________________________________________________________
void DiffXSec::PrintTable(std::ostream &out){
   //if(fPhaseSpace){
      fPhaseSpace.GetEventValues(fVars);
      for(int i = 0; i<fPhaseSpace.GetDimension(); i++) out << fVars[i] << " "; 
      out << EvaluateXSec(fVars) << std::endl;
   //}
}
//_______________________________________________________________________
void DiffXSec::PrintTable() {
   PrintTable(std::cout);
}
//_______________________________________________________________________
void DiffXSec::PrintGrid(std::ostream &out, Int_t N){
   //if(fPhaseSpace){
      std::vector<Double_t> minima;   
      std::vector<Double_t> maxima;   
      std::vector<Double_t> step;   
      std::vector<Int_t> counter;
      for(int i = 0;i<fPhaseSpace.GetDimension(); i++){
         PhaseSpaceVariable * var = fPhaseSpace.GetVariable(i);
         minima.push_back(var->GetMinimum());
         maxima.push_back(var->GetMaximum());
         step.push_back( (maxima[i] - minima[i])/(double(N-1)) );
         counter.push_back(0);
      }
      auto Neval = (unsigned int) TMath::Power(N,fPhaseSpace.GetDimension());

      for(unsigned int n = 0; n<Neval ; n++){ 
         for(unsigned int i = 0; i<counter.size(); i++){
            fVars[i] = minima[i] + double(counter[i])*step[i];
         }
         fPhaseSpace.SetEventValues(fVars);
         PrintTable(out); 
         counter[0]++;
         for(unsigned int i = 0; i<counter.size(); i++){
            if(counter[i] == N) {
               counter[i] = 0;
               if(i+1 < N)counter[i+1]++;
            } 
         }
      }
   //} 
}
//______________________________________________________________________________

Int_t DiffXSec::GetParticleType(Int_t part) const {
   Int_t N = fPIDs.size(); 
   if (N > part) return fPIDs.at(part);
   /* else */
   return(0);
}
//______________________________________________________________________________

void DiffXSec::SetParticleType(Int_t pdgcode, Int_t part) {
   Int_t N = fPIDs.size(); 
   if (N > part) fPIDs[part] = pdgcode;
   else fPIDs.push_back(pdgcode);
   fIsModified = true;
}
//______________________________________________________________________________

void DiffXSec::SetProductionParticleType( Int_t PDGcode, Int_t part )
{
   SetParticleType(PDGcode,part);
}
//______________________________________________________________________________

void DiffXSec::PrintFinalStatePIDs() const {
   Int_t N = fPIDs.size(); 
   for (int i = 0; i < N ; i++)
      std::cout << " particle: " << i << " , PID = " << fPIDs.at(i) << "\n";
}
//_______________________________________________________________________

Double_t * DiffXSec::GetUnnormalizedVariables(const Double_t * x)
{
   for (unsigned int i = 0; i < NDim() && i < 30 ; i++) {
      fUnnormalizedVariables[i] = x[i] * fScaleFactors[i] + fOffsets[i];
   }
   return(fUnnormalizedVariables);
}
//_______________________________________________________________________

Double_t * DiffXSec::GetNormalizedVariables(const Double_t * x)
{
   for (unsigned int i = 0; i < NDim() && i < 30 ; i++) {
      fNormalizedVariables[i] = (x[i] - fOffsets[i]) / fScaleFactors[i];
   }
   return(fNormalizedVariables);
}
//_______________________________________________________________________

Double_t * DiffXSec::GetDependentVariables(const Double_t * x) const {
   for (unsigned int i = 0; i < NDim() && i < 30 ; i++) {
      fDependentVariables[i] = x[i];
   }
   return(fDependentVariables);
}
//_______________________________________________________________________
void DiffXSec::Refresh() {

   if (GetPhaseSpace()) {
      fScaleFactors.clear();
      fOffsets.clear();
      for (int i = 0 ; i < GetPhaseSpace()->GetDimension() ; i++) {
         if(GetPhaseSpace()->GetVariable(i)->IsDependent()) continue;
         fScaleFactors.push_back(GetPhaseSpace()->GetVariable(i)->GetNormScale());
         fOffsets.push_back(GetPhaseSpace()->GetVariable(i)->GetNormOffset());
      }
      GetPhaseSpace()->Refresh();
      SetModified(false);
   } else {
      Error("Refresh","No phase space set!");
   }

   InitializeFinalStateParticles();
}
//_______________________________________________________________________

void DiffXSec::SetPhaseSpace(PhaseSpace * ps)
{
   //if( ps != fPhaseSpace) if( ps!=0 ) if( fPhaseSpace != 0) delete fPhaseSpace; 
   fPhaseSpace = *ps;
   if (fPhaseSpace.GetDimension() < (Int_t)NDim()) Error("SetPhaseSpace", "Phase space dimension less than XSec dimension");
   if (fPhaseSpace.GetNIndependentVariables() > (Int_t)NDim()) Error("SetPhaseSpace", "Phase space dimension greater than XSec dimension");
   fIsModified = true;
}
//______________________________________________________________________________

void DiffXSec::InitializeFinalStateParticles()
{
   // Sets the list of final state particles
   fParticles.Clear();
   if( fnParticles != (Int_t)fPIDs.size() ) {
      Warning("InitializeFinalStateParticles", "warning fnParticles do not have their PIDs defined");
   }
   fnParticleVars.clear();
   for (int i = 0; i < fnParticles; i++) {

      Int_t nVars = 0; 
      for (int j = 0; j < GetPhaseSpace()->GetDimension(); j++) {
         if( GetPhaseSpace()->GetVariable(j)->GetParticleIndex() == i ) nVars++;
      }
      fnParticleVars.push_back(nVars);
      auto aParticle = new Particle();
      aParticle->SetPdgCode( fPIDs[i] );
      fParticles.Add( aParticle );
   }
}
//______________________________________________________________________________

TList * DiffXSec::GetParticles()
{
   return(&fParticles);
}
//______________________________________________________________________________

TParticlePDG * DiffXSec::GetParticlePDG(int i) const
{
   if( fParticles.GetEntries() > 0 ){
      auto * part = (TParticle*)fParticles.At(i);
      return(part->GetPDG(1)); // mode=1 does not return a new TParticlePDG, it reuses it (don't use with TclonesArray)
   } 
   return nullptr;
}
//______________________________________________________________________________

//Int_t  DiffXSec::GetParticleType(int i = 0) const
//{
//   if( fParticles.GetEntries() > 0 ){
//      TParticle * part = (TParticle*)fParticles.At(i);
//      return(part->GetPdgCode());
//   } 
//   return 0;
//}
//______________________________________________________________________________

}
}

