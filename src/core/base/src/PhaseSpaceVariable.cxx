#include "PhaseSpaceVariable.h"
#include <iostream>


namespace insane {
namespace physics {

PhaseSpaceVariable::PhaseSpaceVariable(const char * name, const char * varexp, Double_t min, Double_t max)
   : TNamed(name, varexp)
{
   fVariableMinima = min;
   fVariableMaxima = max;
   fVariableUnits  = "";
   fCurrentValue   = min + (max-min)/2.0;
   fiParticle      = 0;
   fInverted       = false;
   fIsDependent    = false;
   fIsModified     = true;
}
//______________________________________________________________________________

PhaseSpaceVariable::PhaseSpaceVariable(const PhaseSpaceVariable& rhs) : TNamed(rhs),
   fIsModified(rhs.fIsModified),
   fIsDependent(rhs.fIsDependent),
   fInverted(rhs.fInverted),
   fiParticle(rhs.fiParticle),
   fVariableMinima(rhs.fVariableMinima),
   fVariableMaxima(rhs.fVariableMaxima),
   fVariableUnits(rhs.fVariableUnits),
   fCurrentValue(rhs.fCurrentValue)
{ }
//______________________________________________________________________________

PhaseSpaceVariable::~PhaseSpaceVariable()
{ }
//______________________________________________________________________________

void PhaseSpaceVariable::Print(std::ostream& stream) const
{
   stream << "   " 
      << std::setw(10) << std::left <<  GetName() <<  " "
      << "part. " << std::left << std::setw(2) << fiParticle << " : "
      << std::setw(10) << std::right << GetMinimum() << " "
      << " < " << std::setw(10) << std::left <<  GetTitle() << " < "
      << std::setw(10) << std::left << GetMaximum() << " [" << std::setw(5) << GetVariableUnits() << "] with ";
   stream << "value = " << std::setw(4) <<  fCurrentValue << "\n";
}
//______________________________________________________________________________

void PhaseSpaceVariable::Print(const Option_t * opt ) const
{
   Print(std::cout);
   this->Dump();
   //std::cout << "Variable, " 
   //   << std::setw(10) << std::left <<  GetName() <<  ", "
   //   << "particle " << std::left << std::setw(2) << fiParticle << ", limits: "
   //   << std::setw(10) << std::right << GetMinimum() << " "
   //   << " < " << std::setw(10) << std::left <<  GetTitle() << " < "
   //   << std::setw(10) << std::left << GetMaximum() << " [" << std::setw(10) << GetVariableUnits() << "] with ";
   //std::cout << "current value = " << std::setw(4) <<  fCurrentValue << "\n";
}
//______________________________________________________________________________
}}
