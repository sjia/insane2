#include <iostream>
#include <fstream>
#include <string>
#include <cmath>

#include "Math/Vector3D.h"
#include "PhaseSpaceVariables.h"
#include "NFoldDifferential.h"
#include "Jacobians.h"
#include "DiffCrossSection.h"
#include "FinalState.h"
#include "Helpers.h"
#include <typeinfo>
#define CATCH_CONFIG_MAIN
#include "catch/catch.hpp"

SCENARIO( "Function argument constraints", "[FinalState_args]" ) {

  using namespace insane::helpers;
  using namespace insane::physics;
  double eps = 1.0e-3;

  GIVEN( "a lambda returning four vector" ) {

    auto f = [](const InitialState& is, const std::array<double,1>& x){ return ROOT::Math::XYZTVector{ 0,0,1,1}; };
    auto f2 = [](const InitialState& is, const std::array<double,1>& x){ return ROOT::Math::XYZTVector{ 0,0,0,1}; };

    //debug_type< is_array< function_traits<decltype(f)>::arg<1>::type > >();
    //std::cout  << is_array<second_arg_type<decltype(f)>>::value << std::endl;
    //debug_type< second_arg_array_size_type<decltype(f)>>();
    //debug_type< is_array< function_traits<decltype(f)>::arg<0>::type > >();

    WHEN( "the signature is (const InitialState& is, const std::array<double,1>& x)" ) {
      THEN( "the first arg type should not be an array" ) {
        REQUIRE( is_array< function_traits<decltype(f)>::arg<0>::type >::value == false );
      }
      THEN( "the second arg type should be an array" ) {
        REQUIRE( is_array< function_traits<decltype(f)>::arg<1>::type >::value == true );
      }
      THEN( "the second arg array size is 1" ) {
        REQUIRE( arg_array_size<decltype(f),1> == 1 );
      }
      THEN( "the first arg is InititalState" ) {
        REQUIRE( is_first_arg_inital_state<decltype(f)>::value == true );
      }
      //THEN( "the first arg array size is 0" ) {
      //  REQUIRE( arg_array_size<decltype(f),0> == 0 );
      //}

    }

    WHEN( "constructing a FourMomentumFunction" ) {
      FourMomentumFunction<decltype(f)>  p1(f);
      THEN( "the lambda function  returns a four vector") {
        REQUIRE(  returns_a_four_vector<decltype(f)>::value == true );
      }
      THEN( "the operator() still returns a four vector") {
        REQUIRE(  returns_a_four_vector<decltype(p1)>::value == true );
      }
      THEN( "the operator() still takes InitialState in first arg") {
        REQUIRE( is_first_arg_inital_state<decltype(p1)>::value == true );
      }
      THEN( "the operator() still takes std::array in second arg") {
        REQUIRE( is_second_arg_array<decltype(p1)>::value == true );
      }
    }
    WHEN( "constructing FinalStateParticle ") {
      THEN( " a") {
        //PSVBase x1("x","x");
        //FinalStateParticle<decltype(f)>  part(f,{&x1});
      }
    }
  }
}

SCENARIO( "FinalStateParticle", "[FinalState_PThetaPhi]" ) {
  using namespace insane::physics;
  using namespace insane::helpers;
  using namespace insane::physics::variables;
  double test_prec = 1.0e-5;

  double p1   = 2.0;
  double th1  = 0.2;
  double ph1  = 3.0;

  GIVEN( "P, Theta, Phi" ) {

  auto f  = [](const InitialState& is, const std::array<double,1>& x){ ROOT::Math::XYZTVector res = { 0,0,1,1}; };

  }

}


SCENARIO( "Building lorentz invariant ", "[FinalState_args]" ) {
  using namespace insane::physics;
  using namespace insane::helpers;
  using namespace insane::physics::variables;
  double eps = 1.0e-5;

  GIVEN( "Fixed DIS kinematics" ) {
    Energy en(3.0);
    Theta  th(0.1);
    Phi    ph(0.5);
    InitialState init_state(12.0,0.938);
    WHEN( " Constructing the Finalstate") {
      KinematicVars< std::tuple<Energy,Theta,Phi>, InitialState> kv(init_state,std::make_tuple(en,th,ph));
      std::cout << std::get<0>(std::get<1>(kv.Vars)) << std::endl;
      std::cout << std::get<1>(std::get<1>(kv.Vars)) << std::endl;
      std::cout << std::get<2>(std::get<1>(kv.Vars)) << std::endl;
      Energy v1 = std::get<0>(std::get<1>(kv.Vars));
      Theta v2 = std::get<1>(std::get<1>(kv.Vars));
      Phi v3 = std::get<2>(std::get<1>(kv.Vars));
      REQUIRE( std::abs( (en - v1).get() ) - eps );
    }
    WHEN( " Constructing the Finalstate") {
      KinematicVars< std::tuple<Energy,Theta,Phi>, InitialState> kv(init_state,std::make_tuple(en,th,ph));
      //auto ar = kv.GetArray();
    }
  }
}

TEST_CASE( "FinalState tests", "[FinalState]" )
{
  //INFO( "Tester derp " );
  using namespace insane::physics;
  using namespace insane::helpers;
  double eps    = 1.0e-3;
  double P1     = 5.0;
  double m1     = 0.000511;
  double P2     = 0.0;
  double m2     = 0.938;
  double E1     = std::sqrt(P1*P1+m1*m1);
  double E2     = std::sqrt(P2*P2+m2*m2);
  double s_calc = m2*m2 + m1*m1 + 2.0*E1*E2;

  auto f  = [](const InitialState& is, const std::array<double,1>& x){ return ROOT::Math::XYZTVector{ 0,0,1,1}; };
  auto f2 = [](const InitialState& is, const std::array<double,1>& x){ return 2.0; };
  auto f3 = [](const InitialState& is, const double& x){ return 2.0; };
  auto f4 = [](const InitialState& is, const std::array<double,4>& x){ return 2.0; };

  //std::cout << typeid(second_arg_type<decltype(f)>).name() << '\n';
  //std::cout << typeid(derp).name() << '\n';

  REQUIRE( returns_a_four_vector<decltype(f)>::value == true );
  REQUIRE( returns_a_double<decltype(f2)>::value     == true );
  REQUIRE( is_first_arg_inital_state<decltype(f)>::value  == true );
  REQUIRE( is_first_arg_inital_state<decltype(f2)>::value == true );

  //debug_type<second_arg_type<decltype(f2)>>();
  //debug_type<has_array_second_arg<decltype(f2)>>();
  //debug_type<is_second_arg_array<decltype(f2)>>();

  REQUIRE( is_second_arg_array<decltype(f2)>::value == true );
  //REQUIRE( is_second_arg_array<decltype(f3)>::value == false );

}

SCENARIO( "DIS Final State Kinematics", "[FinalState]" ) {

  INFO("The DIS cross section for various fixed target kinematics \n");

  using namespace insane::physics;
  using namespace insane::helpers;
  double test_prec = 1.0e-7;

  // Q2 as a function of x and y
  auto Q2_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double s = is.s();
    double m2= is.p2().M2();
    double xv = vars[0];
    double yv = vars[1];
    return (s-m2)*xv*yv; };

  // x as a function of x (trivial)
  auto x_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double xv = vars[0];
    return xv; };

  // x as a function of x (trivial)
  auto simple_func = [=](const InitialState& is, const std::array<double,1>& vars){
    double xv = vars[0];
    return xv; };

  // y as a function  of x and Q2
  auto y_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double s = is.s();
    double m2= is.p2().M2();
    double xv = vars[0];
    double Q2v = vars[1];
    return Q2v/((s-m2)*xv); };

  //  as a function of x and nu
  auto nu_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double s = is.s();
    double m2= is.p2().M2();
    double xv = vars[0];
    double yv = vars[1];
    double Q2 = (s-m2)*xv*yv;
    return Q2/(2.0*TMath::Sqrt(m2)*xv);
  };

  // -------------------------------------------------------
  // Phase space variables

  PSV<Invariant> v1({0.1, 0.9}, Invariant::x,  "x" );
  PSV<Invariant> v2({1.0, 8.0}, Invariant::Q2, "Q2");
  IPSV phi_psv({0.0, 3.14*2.0},  "phi");
  IPSV nu("nu");
  PSV<KinematicRelationFunction<decltype(y_func)>> v3(y_func ,"y");

  IPSV y2("y");
  PSV<KinematicRelationFunction<decltype(Q2_func)>> Q2_2(Q2_func, "Q2");
  PSV<KinematicRelationFunction<decltype(nu_func)>> nu_2(nu_func, "nu");

  GIVEN( "Valid fixed DIS kinematics" ) {
    // Prepare the initial state
    double P1 = 12.0;
    double m1 = 0.000511;
    double E1 = std::sqrt(P1*P1+m1*m1);

    double P2 = 0.0;
    double m2 = 0.938;
    double E2 = std::sqrt(P2*P2+m2*m2);

    InitialState init_state(P1, P2, m2);

    // Final state kinematics
    double Q2_0 = 5.0;
    double x_0  = 0.4;
    double y_0  = Q2_0/(init_state.s()-(m2*m2))/x_0;

    //std::cout << "     Q2_0 : " << Q2_0 << "\n";
    //std::cout << "      x_0 : " << x_0  << "\n";
    //std::cout << "      y_0 : " << y_0  << "\n";
    //std::cout << " |J| = d(x,Q2)/d(x,y) = Q2_0/y_0 = " << Q2_0/y_0 << "\n";

    auto diff0 = make_diff(v1,v2,phi_psv);
    auto ps0 = make_phase_space( diff0 , v3);

    auto E_prime = [](const InitialState& is, const std::array<double,3>& x){
      double x_bj = x.at(0);
      double Q2   = x.at(1);
      double phi  = x.at(2);
      double s    = is.s();
      double M    = is.p2().M();
      double E0   = (s-M*M)/(2.0*M);
      double y    = Q2/(x_bj*(s-M*M));
      double Ep   = (Q2/(2.0*M*x_bj))*(1.0-y)/y;
      double th   = 2.0*std::asin(M*x_bj*y/std::sqrt(Q2*(1.0-y)));
      ROOT::Math::Polar3D<double> pvec(Ep,th,phi);
      return ROOT::Math::XYZTVector(pvec.x(), pvec.y(), pvec.z(), Ep);
      };

    auto fsp0 = make_final_state_particle(E_prime,11 );
    std::cout << fsp0(init_state, {x_0,Q2_0,0.1}) << std::endl;

    REQUIRE( insane::physics::is_MultiFinalStateParticle<decltype(fsp0)>::value == false );
    REQUIRE( insane::physics::is_FinalStateParticle<decltype(fsp0)>::value == true );
    //std::cout << impl::FSPart_t_impl<decltype(fsp0)>::N << std::endl;
    //std::cout << impl::FSPart<decltype(fsp0)>().N << std::endl;
    //std::cout << fsp0.N << std::endl;
    REQUIRE( insane::physics::impl::is_dimension_same<decltype(ps0),decltype(std::make_tuple(fsp0))>::value == true );

    auto fs0 = make_final_state(ps0, std::make_tuple(fsp0));

    auto parts = fs0.CalculateFinalState(init_state, {x_0,Q2_0,0.1});
    for(auto p : parts) {
      std::cout << p << std::endl;
    }

  }
}

SCENARIO( "Elastic scattering Kinematics", "[FinalState]" ) {

  using namespace insane::physics;
  using namespace insane::helpers;
  double test_prec = 1.0e-7;

  // Q2 as a function of x and y
  auto Q2_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double s = is.s();
    double m2= is.p2().M2();
    double xv = vars[0];
    double yv = vars[1];
    return (s-m2)*xv*yv; };

  // x as a function of x (trivial)
  auto x_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double xv = vars[0];
    return xv; };

  // x as a function of x (trivial)
  auto simple_func = [=](const InitialState& is, const std::array<double,1>& vars){
    double xv = vars[0];
    return xv; };

  // y as a function  of x and Q2
  auto y_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double s = is.s();
    double m2= is.p2().M2();
    double xv = vars[0];
    double Q2v = vars[1];
    return Q2v/((s-m2)*xv); };

  //  as a function of x and nu
  auto nu_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double s = is.s();
    double m2= is.p2().M2();
    double xv = vars[0];
    double yv = vars[1];
    double Q2 = (s-m2)*xv*yv;
    return Q2/(2.0*TMath::Sqrt(m2)*xv);
  };

  // -------------------------------------------------------
  // Phase space variables

  PSV<Invariant> Q2_psv({1.0, 8.0}, Invariant::Q2, "Q2");
  IPSV phi_psv({0.0, 3.14*2.0},  "phi");


  GIVEN( "Valid fixed elastic  kinematics" ) {
    // Prepare the initial state
    double P1 = 2.0;
    double m1 = 0.000511;
    double E1 = std::sqrt(P1*P1+m1*m1);

    double P2 = 0.0;
    double m2 = 0.938;
    double E2 = std::sqrt(P2*P2+m2*m2);

    InitialState init_state(P1, P2, m2);

    // Final state kinematics
    double Q2_0 = 0.2;

    auto diff0 = make_diff(Q2_psv, phi_psv);
    auto ps0 = make_phase_space( diff0 );

    auto diff3 = make_diff(Q2_psv, phi_psv, phi_psv);
    auto ps3 = make_phase_space( diff3 );

    auto E_prime = [](const InitialState& is, const std::array<double,2>& x){
      double Q2   = x.at(0);
      double phi  = x.at(1);
      double s    = is.s();
      double M    = is.p2().M();
      double E0   = (s-M*M)/(2.0*M);
      double y    = Q2/((s-M*M));
      double Ep   = (Q2/(2.0*M))*(1.0-y)/y;
      double th   = 2.0*std::asin(M*y/std::sqrt(Q2*(1.0-y)));
      ROOT::Math::Polar3D<double> pvec(Ep,th,phi);
      return ROOT::Math::XYZTVector(pvec.x(), pvec.y(), pvec.z(), Ep);
      };
    auto P_prime = [](const InitialState& is, const std::array<double,2>& x){
      double Q2   = x.at(0);
      double phi  = x.at(1);
      double s    = is.s();
      double M    = is.p2().M();
      double E0   = (s-M*M)/(2.0*M);
      double y    = Q2/((s-M*M));
      double Ep   = (Q2/(2.0*M))*(1.0-y)/y;
      double th   = 2.0*std::asin(M*y/std::sqrt(Q2*(1.0-y)));
      ROOT::Math::Polar3D<double> pvec(Ep,th,phi);
      auto E_prime = ROOT::Math::XYZTVector(pvec.x(), pvec.y(), pvec.z(), Ep);
      return is.p1()+is.p2()-E_prime;
      };


    auto fsp0 = make_final_state_particle(E_prime,11 );
    auto fsp1 = make_final_state_particle(P_prime,2212,0.938 );

    std::cout << fsp0(init_state, {Q2_0,0.1}) << std::endl;

    auto fs0   = make_final_state(ps0, std::make_tuple(fsp0,fsp1));
    auto parts = fs0.CalculateFinalState(init_state, {Q2_0,0.1});

    for(auto p : parts) {
      std::cout << p << std::endl;
      std::cout << p.M() << std::endl;
    }

    auto E_and_P_prime = [](const InitialState& is, const std::array<double,2>& x){
      double Q2   = x.at(0);
      double phi  = x.at(1);
      double s    = is.s();
      double M    = is.p2().M();
      double E0   = (s-M*M)/(2.0*M);
      double y    = Q2/((s-M*M));
      double Ep   = (Q2/(2.0*M))*(1.0-y)/y;
      double th   = 2.0*std::asin(M*y/std::sqrt(Q2*(1.0-y)));
      ROOT::Math::Polar3D<double> pvec(Ep,th,phi);
      auto E_prime = ROOT::Math::XYZTVector(pvec.x(), pvec.y(), pvec.z(), Ep);
      auto P_prime = is.p1()+is.p2()-E_prime;
      return std::array<ROOT::Math::XYZTVector,2>({E_prime,P_prime});
      };

    //auto fsps01 = make_final_state_particles(E_and_P_prime , 11, 0.000511);
    auto fsps01 = make_final_state_particles({11,2212},{0.000511,0.938}, E_and_P_prime );

    REQUIRE( insane::physics::is_MultiFinalStateParticle<decltype(fsps01)>::value == true );
    REQUIRE( insane::physics::is_FinalStateParticle<decltype(fsps01)>::value == false );
    //std::cout << impl::FSPart_t_impl<decltype(fsp0)>::N << std::endl;
    //std::cout << impl::FSPart<decltype(fsp0)>().N << std::endl;
    //std::cout << fsp0.N << std::endl;
    REQUIRE( insane::physics::impl::is_dimension_same<decltype(ps3),decltype(std::make_tuple(fsp0))>::value == false );
    REQUIRE( insane::physics::impl::is_dimension_same<decltype(ps0),decltype(std::make_tuple(fsp0))>::value == true );

    auto fs2    = make_final_state(ps0, std::make_tuple(fsps01));
    auto parts2 = fs0.CalculateFinalState(init_state, {Q2_0,0.1});

    for(auto p : parts2) {
      std::cout << p << std::endl;
      std::cout << p.M() << std::endl;
    }
  }
}

