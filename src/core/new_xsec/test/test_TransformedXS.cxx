#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <typeinfo>

#include "PhaseSpaceVariables.h"
#include "NFoldDifferential.h"
#include "Jacobians.h"
#include "DiffCrossSection.h"
#include "PhysicalConstants.h"
#include "TransformedDiffXSec.h"

#include "Math/Integrator.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/AllIntegrationTypes.h"
#include "Math/Functor.h"
#include "Math/GaussIntegrator.h"

#include "Math/Vector3Dfwd.h"
#include "Math/Polar3D.h"


#define CATCH_CONFIG_MAIN
#include "catch/catch.hpp"


SCENARIO( "Transformed Cross Section", "[TransformedXS]" ) {

  using namespace insane::physics;
  using namespace insane::helpers;
  double test_prec = 1.0e-7;

  // -------------------------------------------------------

  // Prepare the initial state
  double P1 = 12.0;
  double m1 = 0.000511;
  double E1 = std::sqrt(P1*P1+m1*m1);

  double P2 = 0.0;
  double m2 = 0.938;
  double E2 = std::sqrt(P2*P2+m2*m2);

  InitialState init_state(P1, P2, m2);

  // Phase space variables
  PSV<Invariant> v1(  {0.3, 0.5}, Invariant::x,  "x" );
  PSV<Invariant> v2( {4.0, 6.0}, Invariant::Q2, "Q2");
  IPSV           phi_psv({0.0, insane::units::twopi}, "phi");

  // Final state kinematics
  double Q2_0 = 5.0;
  double x_0  = 0.4;
  double phi_0 = 0.1;
  double y_0  = Q2_0/(init_state.s()-(m2*m2))/x_0;

  //std::cout << "     Q2_0 : " << Q2_0 << "\n";
  //std::cout << "      x_0 : " << x_0  << "\n";
  //std::cout << "      y_0 : " << y_0  << "\n";
  //std::cout << " |J| = d(x,Q2)/d(x,y) = Q2_0/y_0 = " << Q2_0/y_0 << "\n";

  GIVEN( "DIS cross section as a function of (x,Q2,phi)" ) {
    auto diff0 = make_diff(v1,v2,phi_psv);
    auto ps0 = make_phase_space( diff0 );
    auto xs0 = make_diff_cross_section(ps0, [=](const InitialState& is, const std::array<double,3>& vars)
                                       {
                                         double x_bj = vars.at(0);
                                         double Q2   = vars.at(1);
                                         double phi  = vars.at(2);
                                         double M    = is.p2().M();
                                         double alpha  = 1./137.;
                                         double s    = is.s();
                                         double E0   = (s-M*M)/(2.0*M);
                                         double y    = Q2/(x_bj*(s-M*M));
                                         double nu   = y*E0;//nu_func(is,vars);
                                         //double M  = 0.938;
                                         double F1 = 0.0;
                                         double F2 = 1.0;
                                         double W1 = (1./M)*F1;
                                         double W2 = (1./nu)*F2;
                                         //// compute the Mott cross section (units = mb): 
                                         ////Double_t hbarc2 = 0.38939129; // (hbar*c)^2 = 0.38939129 mb*GeV^2  
                                         double Ep   = (Q2/(2.0*M*x_bj))*(1.0-y)/y;
                                         double th   = 2.0*std::asin(M*x_bj*y/std::sqrt(Q2*(1.0-y)));
                                         double COS2 = std::cos(th)*std::cos(th);
                                         double SIN2 = std::sin(th)*std::sin(th);
                                         double TAN2 = SIN2/COS2; 
                                         double num    = alpha*alpha*COS2; 
                                         double den    = 4.*E0*E0*SIN2; 
                                         //return 1.0/vars.at(1);
                                         double MottXS = num/den;
                                         //// compute the full cross section (units = nb/GeV/sr) 
                                         double fullXsec = MottXS*(W2 + 2.0*TAN2*W1)*insane::units::hbarc2_gev_nb;
                                         return fullXsec;
                                       });

    // -------------------------

    WHEN( "changing variables to the same variables " ) {

      auto const_var1 = [=](const InitialState& is, const std::array<double,3>& vars){
        double Q2v = vars[0];
        return Q2v; };
      auto const_var2 = [=](const InitialState& is, const std::array<double,3>& vars){
        double Q2v = vars[1];
        return Q2v; };
      auto const_var3 = [=](const InitialState& is, const std::array<double,3>& vars){
        double Q2v = vars[2];
        return Q2v; };
      auto jm0 = make_jacobian(diff0,
                               diff0,
                               std::make_tuple(const_var1,const_var2,const_var3));

      auto xs1 = make_jacobian_transformed_xs(xs0, init_state,
                                              jm0, ps0);

      auto xs2 = make_jacobian_transformed_xs(xs0, init_state,
                                              jm0, diff0);
      //xs0_prime.Print();

      THEN( "then the cross sections should be the same " ) {

        double xs0_val = xs0(init_state, {x_0,Q2_0,phi_0});
        double xs1_val = xs1(init_state, {x_0,Q2_0,phi_0});
        double xs2_val = xs2(init_state, {x_0,Q2_0,phi_0});
        REQUIRE(  std::abs(xs0_val - xs1_val) < 0.001 );
        REQUIRE(  std::abs(xs0_val - xs2_val) < 0.001 );

        auto ixs0 = make_integrated_cross_section(init_state, xs0);
        auto ixs1 = make_integrated_cross_section(init_state, xs1);
        auto ixs2 = make_integrated_cross_section(init_state, xs2);

        double ixs0_val = 0.0;
        double ixs1_val = 0.0;
        double ixs2_val = 0.0;
        {
          ROOT::Math::IntegratorMultiDim ig2(ROOT::Math::IntegrationMultiDim::kVEGAS); 
          ig2.SetFunction(ixs0);

          const auto& x_v0   = std::get<0>(ixs0.fXS.fPS.fDiff.fIndVars);
          const auto& Q2_v1  = std::get<1>(ixs0.fXS.fPS.fDiff.fIndVars);
          const auto& phi_v2 = std::get<2>(ixs0.fXS.fPS.fDiff.fIndVars);

          std::vector<double> a_min = {x_v0.Min(), Q2_v1.Min(), phi_v2.Min()};
          std::vector<double> b_max = {x_v0.Max(), Q2_v1.Max(), phi_v2.Max()};

          double val = ig2.Integral(a_min.data(), b_max.data() );
          //std::cout << "integral result is " << val << std::endl;
          ixs0_val = val;
        }
        {
          ROOT::Math::IntegratorMultiDim ig2(ROOT::Math::IntegrationMultiDim::kVEGAS); 
          ig2.SetFunction(ixs1);

          const auto& x_v0   = std::get<0>(ixs1.fXS.fPS.fDiff.fIndVars);
          const auto& Q2_v1  = std::get<1>(ixs1.fXS.fPS.fDiff.fIndVars);
          const auto& phi_v2 = std::get<2>(ixs1.fXS.fPS.fDiff.fIndVars);

          std::vector<double> a_min = {x_v0.Min(), Q2_v1.Min(), phi_v2.Min()};
          std::vector<double> b_max = {x_v0.Max(), Q2_v1.Max(), phi_v2.Max()};

          double val = ig2.Integral(a_min.data(), b_max.data() );
          //std::cout << "integral result is " << val << std::endl;
          ixs1_val = val;
        }
        {
          ROOT::Math::IntegratorMultiDim ig2(ROOT::Math::IntegrationMultiDim::kVEGAS); 
          ig2.SetFunction(ixs2);

          const auto& x_v0   = std::get<0>(ixs2.fXS.fPS.fDiff.fIndVars);
          const auto& Q2_v1  = std::get<1>(ixs2.fXS.fPS.fDiff.fIndVars);
          const auto& phi_v2 = std::get<2>(ixs2.fXS.fPS.fDiff.fIndVars);

          std::vector<double> a_min = {x_v0.Min(), Q2_v1.Min(), phi_v2.Min()};
          std::vector<double> b_max = {x_v0.Max(), Q2_v1.Max(), phi_v2.Max()};

          double val = ig2.Integral(a_min.data(), b_max.data() );
          //std::cout << "integral result is " << val << std::endl;
          ixs2_val = val;
        }
        REQUIRE(  std::abs(ixs0_val - ixs1_val) < 0.001 );
        REQUIRE(  std::abs(ixs0_val - ixs2_val) < 0.001 );
      }
    }
  }
}

SCENARIO( "Transformed DIS from (x,Q2) to (E',theta) ", "[TransformedXS]" ) {

  using namespace insane::physics;
  using namespace insane::helpers;
  double test_prec = 1.0e-7;

  // -------------------------------------------------------
  // Phase space variables
  PSV<Invariant> x_psv(  {0.1, 0.5}, Invariant::x,  "x" );
  PSV<Invariant> Q2_psv( {3.0, 6.0}, Invariant::Q2, "Q2");
  IPSV           phi_psv({0.0, insane::units::twopi}, "phi");

  // Eprime (x,Q2,phi)
  auto Eprime_func = [=](const InitialState& is, const std::array<double,3>& vars){
    if( (is.Frame() != FrameOfReference::Lab) || (is.Accelerator() != AcceleratorType::FixedTarget) ) { 
      std::cerr << "InitialState for DIS cross section not in FixedTarget Lab Frame !\n"
      << __FILE__ ":" << __LINE__  << "\n";
      return 0.0;
    }
    double s = is.s();
    double m2= is.p2().M2();
    double x_bj = vars.at(0);
    double Q2   = vars.at(1);
    double phi  = vars.at(2);
    double M    = is.p2().M();
    double alpha  = 1./137.;
    double E0   = (s-M*M)/(2.0*M);
    double y    = Q2/(x_bj*(s-M*M));
    double nu   = y*E0;//nu_func(is,vars);
    double Ep   = (Q2/(2.0*M*x_bj))*(1.0-y)/y;
    return Ep;
  };

  // theta (x,Q2,phi)
  auto theta_func = [=](const InitialState& is, const std::array<double,3>& vars){
    if( (is.Frame() != FrameOfReference::Lab) || (is.Accelerator() != AcceleratorType::FixedTarget) ) { 
      std::cerr << "InitialState for DIS cross section not in FixedTarget Lab Frame !\n"
      << __FILE__ ":" << __LINE__  << "\n";
      return 0.0;
    }
    double s = is.s();
    double m2= is.p2().M2();
    double x_bj = vars.at(0);
    double Q2   = vars.at(1);
    double phi  = vars.at(2);
    double M    = is.p2().M();
    double alpha  = 1./137.;
    double E0   = (s-M*M)/(2.0*M);
    double y    = Q2/(x_bj*(s-M*M));
    double nu   = y*E0;//nu_func(is,vars);
    double Ep   = (Q2/(2.0*M*x_bj))*(1.0-y)/y;
    double th   = 2.0*std::asin(M*x_bj*y/std::sqrt(Q2*(1.0-y)));
    return th;
  };

  auto x_bj_func = [=](const InitialState& is, const std::array<double,3>& vars){
    if( (is.Frame() != FrameOfReference::Lab) || (is.Accelerator() != AcceleratorType::FixedTarget) ) { 
      std::cerr << "InitialState for DIS cross section not in FixedTarget Lab Frame !\n"
      << __FILE__ ":" << __LINE__  << "\n";
      return 0.0;
    }
    double s    = is.s();
    double M    = is.p2().M();
    double E0   = (s-M*M)/(2.0*M);
    double Ep   = vars.at(0);
    double nu   = E0 - Ep;
    double th   = vars.at(1);
    double phi  = vars.at(2);
    double Q2   = 4.0*Ep*E0*std::sin(th/2.0)*std::sin(th/2.0);
    double x_bj = Q2/(2.0*M*nu);
    return x_bj;
  };
  auto Q2_func = [=](const InitialState& is, const std::array<double,3>& vars){
    if( (is.Frame() != FrameOfReference::Lab) || (is.Accelerator() != AcceleratorType::FixedTarget) ) { 
      std::cerr << "InitialState for DIS cross section not in FixedTarget Lab Frame !\n"
      << __FILE__ ":" << __LINE__  << "\n";
      return 0.0;
    }
    double s    = is.s();
    double M    = is.p2().M();
    double E0   = (s-M*M)/(2.0*M);
    double Ep   = vars.at(0);
    double nu   = E0 - Ep;
    double th   = vars.at(1);
    double phi  = vars.at(2);
    double Q2   = 4.0*Ep*E0*std::sin(th/2.0)*std::sin(th/2.0);
    return Q2;
  };

  // Prepare the initial state
  double P1 = 12.0;
  double m1 = 0.000511;
  double E1 = std::sqrt(P1*P1+m1*m1);

  double P2 = 0.0;
  double m2 = 0.938;
  double E2 = std::sqrt(P2*P2+m2*m2);

  InitialState init_state(RefFrame<FrameOfReference::Lab>{},P1, P2, m2);

  // Final state kinematics
  double Q2_0  = 5.0;
  double x_0   = 0.4;
  double phi_0 = 0.1;
  double y_0   = Q2_0/(init_state.s()-(m2*m2))/x_0;

  GIVEN( "DIS cross section as a function of (x,Q2,phi)" ) {

    auto diff0 = make_diff(x_psv,Q2_psv,phi_psv);
    auto ps0   = make_phase_space( diff0 );
    auto xs0   = make_diff_cross_section(ps0, [=](const InitialState& is, const std::array<double,3>& vars)
                                         {
                                           double x_bj = vars.at(0);
                                           double Q2   = vars.at(1);
                                           double phi  = vars.at(2);
                                           double M    = is.p2().M();
                                           double alpha  = 1./137.;
                                           double s    = is.s();
                                           double E0   = (s-M*M)/(2.0*M);
                                           double y    = Q2/(x_bj*(s-M*M));
                                           double nu   = y*E0;//nu_func(is,vars);
                                           //double M  = 0.938;
                                           double F1 = 0.0;
                                           double F2 = 1.0;
                                           double W1 = (1./M)*F1;
                                           double W2 = (1./nu)*F2;
                                           //// compute the Mott cross section (units = mb): 
                                           ////Double_t hbarc2 = 0.38939129; // (hbar*c)^2 = 0.38939129 mb*GeV^2  
                                           double Ep   = (Q2/(2.0*M*x_bj))*(1.0-y)/y;
                                           double th   = 2.0*std::asin(M*x_bj*y/std::sqrt(Q2*(1.0-y)));
                                           double COS2 = std::cos(th)*std::cos(th);
                                           double SIN2 = std::sin(th)*std::sin(th);
                                           double TAN2 = SIN2/COS2; 
                                           double num    = alpha*alpha*COS2; 
                                           double den    = 4.*E0*E0*SIN2; 
                                           //return 1.0/vars.at(1);
                                           double MottXS = num/den;
                                           //// compute the full cross section (units = nb/GeV/sr) 
                                           double fullXsec = MottXS*(W2 + 2.0*TAN2*W1)*insane::units::hbarc2_gev_nb;
                                           return fullXsec;
                                         });
    auto const_var3 = [=](const InitialState& is, const std::array<double,3>& vars){
      double Q2v = vars[2];
      return Q2v; };
    WHEN( "converting to a function of (E',theta,phi)" ) {

      IPSV Ep_psv( {0.000, 100.0},     "Ep" );
      IPSV th_psv( {0.001, insane::units::pi }, "theta");
      IPSV phi_psv2({0.0, insane::units::twopi},"phi");

      // New differential
      auto diff1 = make_diff(Ep_psv, th_psv, phi_psv2);
      auto ps1   = make_phase_space( diff1 );

      auto jm0 = make_jacobian(diff0, diff1,
                               std::make_tuple(x_bj_func, Q2_func, const_var3));

      auto xs1 = make_jacobian_transformed_xs(xs0, init_state,
                                         jm0, ps1);

      double Eprime_0 = Eprime_func(init_state, {x_0,Q2_0,phi_0});
      double Theta_0  = theta_func( init_state, {x_0,Q2_0,phi_0});

      THEN( "the computed variables should be consistent ") {
        std::cout << " Ep   = " << Eprime_0 << "\n";
        std::cout << " th   = " << Theta_0 << "\n";
        double Q2_check   =  Q2_func(init_state,{Eprime_0, Theta_0,phi_0})  ;
        double x_bj_check =  x_bj_func(init_state,{Eprime_0, Theta_0,phi_0});
        std::cout << " Q2   = " << Q2_func(init_state,{Eprime_0, Theta_0,phi_0})  << "\n";
        std::cout << " x_bj = " << x_bj_func(init_state,{Eprime_0, Theta_0,phi_0}) << "\n";
        REQUIRE(  std::abs(Q2_check - Q2_0) < test_prec );
        REQUIRE(  std::abs(x_bj_check - x_0) < test_prec );
      }

      THEN( "the jacobian should make the cross sections the same ") {

        double xs0_val =  xs0(init_state, {x_0,      Q2_0,   phi_0}) ;
        double xs1_val =  xs1(init_state, {Eprime_0, Theta_0,phi_0}) ;
        std::cout << " xs0_val : " << xs0_val << "\n";
        std::cout << " xs1_val : " << xs1_val << "\n";

        auto jm0_val = jm0.Det(init_state, {Eprime_0, Theta_0, phi_0});

        REQUIRE( std::abs(xs0_val*jm0_val - xs1_val ) < 0.0001 );

        jm0.PrintDebug(init_state, {Eprime_0, Theta_0, phi_0});

        auto ixs0 = make_integrated_cross_section(init_state, xs0);
        auto ixs1 = make_integrated_cross_section(init_state, xs1);

        double ixs0_val = 0.0;
        double ixs1_val = 0.0;
        {
          ROOT::Math::IntegratorMultiDim ig2(ROOT::Math::IntegrationMultiDim::kVEGAS); 
          ig2.SetFunction(ixs0);

          const auto& x_v0   = std::get<0>(ixs0.fXS.fPS.fDiff.fIndVars);
          const auto& Q2_v1  = std::get<1>(ixs0.fXS.fPS.fDiff.fIndVars);
          const auto& phi_v2 = std::get<2>(ixs0.fXS.fPS.fDiff.fIndVars);

          std::vector<double> a_min = {x_v0.Min(), Q2_v1.Min(), phi_v2.Min()};
          std::vector<double> b_max = {x_v0.Max(), Q2_v1.Max(), phi_v2.Max()};

          double val = ig2.Integral(a_min.data(), b_max.data() );
          std::cout << "integral result is " << val << std::endl;
          ixs0_val = val;
        }

        {
          ROOT::Math::IntegratorMultiDim ig2(ROOT::Math::IntegrationMultiDim::kVEGAS); 
          ig2.SetFunction(ixs1);

          const auto& x_v0   = std::get<0>(ixs1.fXS.fPS.fDiff.fIndVars);
          const auto& Q2_v1  = std::get<1>(ixs1.fXS.fPS.fDiff.fIndVars);
          const auto& phi_v2 = std::get<2>(ixs1.fXS.fPS.fDiff.fIndVars);

          std::vector<double> a_min = {x_v0.Min(), Q2_v1.Min(), phi_v2.Min()};
          std::vector<double> b_max = {x_v0.Max(), Q2_v1.Max(), phi_v2.Max()};

          double val = ig2.Integral(a_min.data(), b_max.data() );
          std::cout << "integral result is " << val << std::endl;
          ixs1_val = val;
        }
        REQUIRE( std::abs(ixs0_val - ixs1_val ) < 0.01 );
        std::cout << " ------------------------------------\n";
      }
    }
  }
}


SCENARIO( "Boosted frame muon-DIS from lab-rest (E',th) to collider with variables (P,th)  ", "[TransformedXS]" ) {

  using namespace insane::physics;
  using namespace insane::helpers;
  double test_prec = 1.0e-7;

  // Prepare the initial state
  double P1 = 4.0;
  double m1 = 0.105;
  double E1 = std::sqrt(P1*P1+m1*m1);

  double P2 = 3.0;
  double m2 = 0.938;
  double E2 = std::sqrt(P2*P2+m2*m2);

  //InitialState init_state(RefFrame<FrameOfReference::Lab>{},P1, P2, m2);
  InitialState init_state_LAB({P1, m1}, {P2, m2});

  InitialState init_state_REST = init_state_LAB.GetSecondaryRestFrame();

  init_state_LAB.Print();
  init_state_REST.Print();

  // -------------------------------------------------------
  // Phase space variables
  IPSV Ep_psv(  {1.0, 3.0},                         "Ep" );
  IPSV th_psv(  { insane::units::pi-0.5, insane::units::pi-0.001 }, "theta");
  IPSV phi_psv( {-insane::units::pi, insane::units::pi},            "phi");

  // Variables in the (collider) LAB frame
  IPSV P_LAB_psv(  {0.0, 50.0},              "P_LAB" );
  IPSV th_LAB_psv( { 0, insane::units::pi },         "theta_LAB");
  IPSV phi_LAB_psv({-insane::units::pi, insane::units::pi},  "phi_LAB");

  // Differential cross section evaluated in the nucleon rest frame
  auto diff0 = make_diff(Ep_psv, th_psv, phi_psv);
  auto ps0   = make_phase_space( diff0 );
  auto xs0   = make_diff_cross_section(ps0, [=](const InitialState& is, const std::array<double,3>& vars)
                                       {
                                         if( (is.Frame() != FrameOfReference::TargetAtRest) 
                                            || (is.Accelerator() != AcceleratorType::FixedTarget) ) { 
                                           std::cerr << "InitialState for DIS cross section not in FixedTarget Lab Frame !\n"
                                           << __FILE__ ":" << __LINE__  << "\n";
                                           return 0.0;
                                         }
                                         double Ep   = vars.at(0);
                                         if(Ep > is.p1().E() ) {return 0.0;}
                                         return 1.0;
                                         double th   = vars.at(1);
                                         double phi  = vars.at(2);
                                         double M    = is.p2().M();
                                         double alpha  = 1./137.;
                                         ROOT::Math::Polar3D<double>  pvec = {Ep,th,phi};
                                         ROOT::Math::XYZTVector       PprimeVec{pvec.x(), pvec.y(), pvec.z(), std::sqrt(Ep*Ep+is.p1().M2())};
                                         auto q = is.p1() - PprimeVec;
                                         double s    = is.s();
                                         double E0   = (s-M*M)/(2.0*M);
                                         double nu   = E0-Ep;
                                         double Q2   = -1.0*q.Dot(q);
                                         double x_bj = Q2/(2.0*M*nu);
                                         double y    = Q2/(x_bj*(s-M*M));
                                         //double M  = 0.938;
                                         double F1 = 0.01;
                                         double F2 = (1.0-x_bj);
                                         double W1 = (1./M)*F1;
                                         double W2 = (1./nu)*F2;
                                         //// compute the Mott cross section (units = mb): 
                                         ////Double_t hbarc2 = 0.38939129; // (hbar*c)^2 = 0.38939129 mb*GeV^2  
                                         double COS2 = std::cos(th)*std::cos(th);
                                         double SIN2 = std::sin(th)*std::sin(th);
                                         double TAN2 = SIN2/COS2; 
                                         double num    = alpha*alpha*COS2; 
                                         double den    = 4.*E0*E0*SIN2; 
                                         //return 1.0/vars.at(1);
                                         double MottXS = num/den;
                                         //// compute the full cross section (units = nb/GeV/sr) 
                                         double fullXsec = MottXS*(W2 + 2.0*TAN2*W1)*insane::units::hbarc2_gev_nb;
                                         return fullXsec;
                                       });
  // Eprime(P,theta,phi)
  auto Eprime_REST_func = [=](const InitialState& is, const std::array<double,3>& vars){
    auto boost_to_Lab = is.GetBoostToRestFrame();
    double P    = vars.at(0);
    double th   = vars.at(1);
    double phi  = vars.at(2);
    double Ep   = std::sqrt(P*P+is.p1().M2());
    ROOT::Math::Polar3D<double>  pvec{P, th, phi};
    ROOT::Math::XYZTVector  p4vec{pvec.x(), pvec.y(), pvec.z(), Ep};
    //kddis.Print();
    auto p4vec_boosted = ROOT::Math::VectorUtil::boost(p4vec, boost_to_Lab);
    //std::cout << p4vec_boosted << std::endl;
    return p4vec_boosted.E();
  };
  // theta(P,theta,phi)
  auto theta_REST_func = [=](const InitialState& is, const std::array<double,3>& vars){
    auto boost_to_Lab = is.GetBoostToRestFrame();
    double P    = vars.at(0);
    double th   = vars.at(1);
    double phi  = vars.at(2);
    double Ep   = std::sqrt(P*P+is.p1().M2());
    ROOT::Math::Polar3D<double>  pvec{P, th, phi};
    ROOT::Math::XYZTVector  p4vec{pvec.x(), pvec.y(), pvec.z(), Ep};
    auto p4vec_boosted = ROOT::Math::VectorUtil::boost(p4vec,boost_to_Lab);
    //std::cout << p4vec_boosted << std::endl;
    return p4vec_boosted.Theta();
  };
  // phi(P,theta,phi)
  auto phi_REST_func = [=](const InitialState& is, const std::array<double,3>& vars){
    auto boost_to_Lab = is.GetBoostToRestFrame();
    double P    = vars.at(0);
    double th   = vars.at(1);
    double phi  = vars.at(2);
    double Ep   = std::sqrt(P*P+is.p1().M2());
    ROOT::Math::Polar3D<double>  pvec{P,th,phi};
    ROOT::Math::XYZTVector  p4vec{pvec.x(), pvec.y(), pvec.z(), Ep};
    auto p4vec_boosted = ROOT::Math::VectorUtil::boost(p4vec,boost_to_Lab);
    //std::cout << p4vec_boosted << std::endl;
    return p4vec_boosted.Phi();
  };

  auto diff1 = make_diff( P_LAB_psv, th_LAB_psv, phi_LAB_psv );
  auto ps1   = make_phase_space( diff1 );
  auto jm0   = make_jacobian(diff0, diff1,
                            std::make_tuple( Eprime_REST_func, theta_REST_func, phi_REST_func));

  GIVEN( "a cross section" ) {

    auto                         boost_to_REST = init_state_LAB.GetBoostToRestFrame();
    double                       Pprime_LAB_0 =  0.9;
    double                       theta_LAB_0 =  insane::units::pi-0.4;
    double                       phi_LAB_0 =  0.1;
    ROOT::Math::Polar3D<double>  pvec{ Pprime_LAB_0, theta_LAB_0, phi_LAB_0 };
    ROOT::Math::XYZTVector       pp_vec_LAB{ pvec.x(), pvec.y(), pvec.z(), std::sqrt(Pprime_LAB_0*Pprime_LAB_0+init_state_LAB.p1().M2()) };
    auto                         pp_vec_REST = ROOT::Math::VectorUtil::boost( pp_vec_LAB, boost_to_REST );

    WHEN( "the kinematics are fixed" ){

      auto qvec       = init_state_LAB.p1() - pp_vec_LAB;
      auto p2_lab     = init_state_LAB.p2() ;
      std::cout << "    Q2  = " << -1.0*qvec.Dot(qvec) << " \n";
      std::cout << "    x   = " << -1.0*qvec.Dot(qvec)/(2.0*p2_lab.Dot(qvec)) << " \n";

      THEN( "they should be self consistent with the jacobian input functions" ) {

        //std::cout << "  p1 LAB : " << pp_vec_LAB << "\n";
        //std::cout << "  p1 REST: " << pp_vec_REST << "\n";
        //std::cout << "p2 boost : " << ROOT::Math::VectorUtil::boost(init_state_LAB.p2(),boost_to_REST) << " \n";

        REQUIRE( ROOT::Math::VectorUtil::boost(init_state_LAB.p2(),boost_to_REST).Vect().R() < 0.00001 );

        //double Eprime_check = Eprime_REST_func( init_state_LAB , {pp_vec_LAB.P(),  pp_vec_LAB.Theta(),  pp_vec_LAB.Phi()} );
        //std::cout << " Eprime_check    " << Eprime_check    << std::endl;
        //std::cout << " Pprime_LAB_0 " << Pprime_LAB_0 << std::endl;

        //double th_check = theta_REST_func( init_state_LAB , {pp_vec_LAB.P(),  pp_vec_LAB.Theta(),  pp_vec_LAB.Phi()} );
        //std::cout << " th_check    " << th_check    << std::endl;
        //std::cout << " theta_LAB_0 " << theta_LAB_0 << std::endl;

        //double phi_check = phi_REST_func( init_state_LAB , {pp_vec_LAB.P(),  pp_vec_LAB.Theta(),  pp_vec_LAB.Phi()} );
        //std::cout << " phi_check    " << phi_check    << std::endl;
        //std::cout << " phi_LAB_0 " << phi_LAB_0 << std::endl;
      }
    }

    auto xs1 = make_jacobian_transformed_xs(xs0, init_state_REST,
                                       jm0, ps1);
    std::cout << " ------------------------------------\n";

    auto   ixs0    = make_integrated_cross_section(init_state_REST, xs0);
    auto   ixs1    = make_integrated_cross_section(init_state_LAB, xs1);

    double xs0_val =  xs0( init_state_REST, {pp_vec_REST.E(), pp_vec_REST.Theta(), pp_vec_REST.Phi()}) ;
    double xs1_val =  xs1( init_state_LAB , {pp_vec_LAB.P(),  pp_vec_LAB.Theta(),  pp_vec_LAB.Phi()})  ;
    double jm0_val =  jm0.Det(init_state_LAB  ,{pp_vec_LAB.P(),  pp_vec_LAB.Theta(),  pp_vec_LAB.Phi()})  ;

    //std::cout << "xs0_val : " << xs0_val << std::endl;
    //std::cout << "xs1_val : " << xs1_val << std::endl;
    //std::cout << "xs0*jm0 : " << xs0_val*jm0_val << std::endl;
    //std::cout << "    jm0 : " << jm0_val << std::endl;

    REQUIRE( std::abs(xs1_val - xs0_val*jm0_val) < 0.0001 );

    //jm0.PrintDebug( init_state_LAB, {p1_vec_LAB.P(),p1_vec_LAB.Theta(),p1_vec_LAB.Phi()});

    double ixs0_val = 0.0;
    double ixs1_val = 0.0;
    {
      ROOT::Math::IntegratorMultiDim ig2(ROOT::Math::IntegrationMultiDim::kVEGAS); 
      ig2.SetFunction(ixs0);

      const auto& v0 = std::get<0>(ixs0.fXS.fPS.fDiff.fIndVars);
      const auto& v1 = std::get<1>(ixs0.fXS.fPS.fDiff.fIndVars);
      const auto& v2 = std::get<2>(ixs0.fXS.fPS.fDiff.fIndVars);

      std::vector<double> a_min = {v0.Min(), v1.Min(), v2.Min()};
      std::vector<double> b_max = {v0.Max(), v1.Max(), v2.Max()};

      double val = ig2.Integral(a_min.data(), b_max.data() );
      std::cout << "integral result is " << val << std::endl;
      ixs0_val = val;
    }

    {
      ROOT::Math::IntegratorMultiDim ig2(ROOT::Math::IntegrationMultiDim::kVEGAS); 
      ig2.SetFunction(ixs1);
      //ixs1.Print();

      const auto& v0 = std::get<0>( ixs1.fXS.ConstDifferential().fIndVars);
      const auto& v1 = std::get<1>( ixs1.fXS.ConstDifferential().fIndVars);
      const auto& v2 = std::get<2>( ixs1.fXS.ConstDifferential().fIndVars);

      std::vector<double> a_min = {v0.Min(), v1.Min(), v2.Min()};
      std::vector<double> b_max = {v0.Max(), v1.Max(), v2.Max()};

      double val = ig2.Integral(a_min.data(), b_max.data() );
      std::cout << "integral result is " << val << std::endl;
      ixs1_val = val;
    }
    REQUIRE( std::abs(ixs0_val - ixs1_val ) < 0.01 );
    std::cout << " ------------------------------------\n";
  }
}

SCENARIO( "Transform an already Transformed X section", "[TransformedXS]" )  {


  using namespace insane::physics;
  using namespace insane::helpers;
  double test_prec = 1.0e-7;

  // -------------------------------------------------------
  // Phase space variables
  PSV<Invariant> x_psv(  {0.1, 0.5}, Invariant::x,  "x" );
  PSV<Invariant> Q2_psv( {3.0, 6.0}, Invariant::Q2, "Q2");
  IPSV           phi_psv({0.0, insane::units::twopi}, "phi");

  // Eprime (x,Q2,phi)
  auto Eprime_func = [=](const InitialState& is, const std::array<double,3>& vars){
    if( (is.Frame() != FrameOfReference::Lab) || (is.Accelerator() != AcceleratorType::FixedTarget) ) { 
      std::cerr << "InitialState for DIS cross section not in FixedTarget Lab Frame !\n"
      << __FILE__ ":" << __LINE__  << "\n";
      return 0.0;
    }
    double s = is.s();
    double m2= is.p2().M2();
    double x_bj = vars.at(0);
    double Q2   = vars.at(1);
    double phi  = vars.at(2);
    double M    = is.p2().M();
    double alpha  = 1./137.;
    double E0   = (s-M*M)/(2.0*M);
    double y    = Q2/(x_bj*(s-M*M));
    double nu   = y*E0;//nu_func(is,vars);
    double Ep   = (Q2/(2.0*M*x_bj))*(1.0-y)/y;
    return Ep;
  };

  // theta (x,Q2,phi)
  auto theta_func = [=](const InitialState& is, const std::array<double,3>& vars){
    if( (is.Frame() != FrameOfReference::Lab) || (is.Accelerator() != AcceleratorType::FixedTarget) ) { 
      std::cerr << "InitialState for DIS cross section not in FixedTarget Lab Frame !\n"
      << __FILE__ ":" << __LINE__  << "\n";
      return 0.0;
    }
    double s = is.s();
    double m2= is.p2().M2();
    double x_bj = vars.at(0);
    double Q2   = vars.at(1);
    double phi  = vars.at(2);
    double M    = is.p2().M();
    double alpha  = 1./137.;
    double E0   = (s-M*M)/(2.0*M);
    double y    = Q2/(x_bj*(s-M*M));
    double nu   = y*E0;//nu_func(is,vars);
    double Ep   = (Q2/(2.0*M*x_bj))*(1.0-y)/y;
    double th   = 2.0*std::asin(M*x_bj*y/std::sqrt(Q2*(1.0-y)));
    return th;
  };

  auto x_bj_func = [=](const InitialState& is, const std::array<double,3>& vars){
    if( (is.Frame() != FrameOfReference::Lab) || (is.Accelerator() != AcceleratorType::FixedTarget) ) { 
      std::cerr << "InitialState for DIS cross section not in FixedTarget Lab Frame !\n"
      << __FILE__ ":" << __LINE__  << "\n";
      return 0.0;
    }
    double s    = is.s();
    double M    = is.p2().M();
    double E0   = (s-M*M)/(2.0*M);
    double Ep   = vars.at(0);
    double nu   = E0 - Ep;
    double th   = vars.at(1);
    double phi  = vars.at(2);
    double Q2   = 4.0*Ep*E0*std::sin(th/2.0)*std::sin(th/2.0);
    double x_bj = Q2/(2.0*M*nu);
    return x_bj;
  };
  auto Q2_func = [=](const InitialState& is, const std::array<double,3>& vars){
    if( (is.Frame() != FrameOfReference::Lab) || (is.Accelerator() != AcceleratorType::FixedTarget) ) { 
      std::cerr << "InitialState for DIS cross section not in FixedTarget Lab Frame !\n"
      << __FILE__ ":" << __LINE__  << "\n";
      return 0.0;
    }
    double s    = is.s();
    double M    = is.p2().M();
    double E0   = (s-M*M)/(2.0*M);
    double Ep   = vars.at(0);
    double nu   = E0 - Ep;
    double th   = vars.at(1);
    double phi  = vars.at(2);
    double Q2   = 4.0*Ep*E0*std::sin(th/2.0)*std::sin(th/2.0);
    return Q2;
  };

  // Prepare the initial state
  double P1 = 12.0;
  double m1 = 0.000511;
  double E1 = std::sqrt(P1*P1+m1*m1);

  double P2 = 0.0;
  double m2 = 0.938;
  double E2 = std::sqrt(P2*P2+m2*m2);

  InitialState init_state(RefFrame<FrameOfReference::Lab>{},P1, P2, m2);

  // Final state kinematics
  double Q2_0  = 5.0;
  double x_0   = 0.4;
  double phi_0 = 0.1;
  double y_0   = Q2_0/(init_state.s()-(m2*m2))/x_0;

  GIVEN( "DIS cross section as a function of (x,Q2,phi)" ) {

    auto diff0 = make_diff(x_psv,Q2_psv,phi_psv);
    auto ps0   = make_phase_space( diff0 );
    auto xs0   = make_diff_cross_section(ps0, [=](const InitialState& is, const std::array<double,3>& vars)
                                         {
                                           double x_bj = vars.at(0);
                                           double Q2   = vars.at(1);
                                           double phi  = vars.at(2);
                                           double M    = is.p2().M();
                                           double alpha  = 1./137.;
                                           double s    = is.s();
                                           double E0   = (s-M*M)/(2.0*M);
                                           double y    = Q2/(x_bj*(s-M*M));
                                           double nu   = y*E0;//nu_func(is,vars);
                                           //double M  = 0.938;
                                           double F1 = 0.0;
                                           double F2 = 1.0;
                                           double W1 = (1./M)*F1;
                                           double W2 = (1./nu)*F2;
                                           //// compute the Mott cross section (units = mb): 
                                           ////Double_t hbarc2 = 0.38939129; // (hbar*c)^2 = 0.38939129 mb*GeV^2  
                                           double Ep   = (Q2/(2.0*M*x_bj))*(1.0-y)/y;
                                           double th   = 2.0*std::asin(M*x_bj*y/std::sqrt(Q2*(1.0-y)));
                                           double COS2 = std::cos(th)*std::cos(th);
                                           double SIN2 = std::sin(th)*std::sin(th);
                                           double TAN2 = SIN2/COS2; 
                                           double num    = alpha*alpha*COS2; 
                                           double den    = 4.*E0*E0*SIN2; 
                                           //return 1.0/vars.at(1);
                                           double MottXS = num/den;
                                           //// compute the full cross section (units = nb/GeV/sr) 
                                           double fullXsec = MottXS*(W2 + 2.0*TAN2*W1)*insane::units::hbarc2_gev_nb;
                                           return fullXsec;
                                         });
    auto const_var3 = [=](const InitialState& is, const std::array<double,3>& vars){
      double Q2v = vars[2];
      return Q2v; };
    WHEN( "converting to a function of (E',theta,phi)" ) {

      IPSV Ep_psv( {0.000, 100.0},     "Ep" );
      IPSV th_psv( {0.001, insane::units::pi }, "theta");
      IPSV phi_psv2({0.0, insane::units::twopi},"phi");

      // New differential
      auto diff1 = make_diff(Ep_psv, th_psv, phi_psv2);
      auto ps1   = make_phase_space( diff1 );

      auto jm0 = make_jacobian(diff0, diff1,
                               std::make_tuple(x_bj_func, Q2_func, const_var3));

      auto jm2 = make_identity_jacobian(diff0);

      auto xs2 = make_jacobian_transformed_xs(xs0, init_state,
                                              jm2, ps0);

      //auto xs2 = make_jacobian_transformed_xs(xs1, init_state,
      //                                        jm2, ps1);
      auto xs1 = make_jacobian_transformed_xs(xs2, init_state,
                                              jm0, ps1);

      double Eprime_0 = Eprime_func(init_state, {x_0,Q2_0,phi_0});
      double Theta_0  = theta_func( init_state, {x_0,Q2_0,phi_0});

      THEN( "the computed variables should be consistent ") {
        std::cout << " Ep   = " << Eprime_0 << "\n";
        std::cout << " th   = " << Theta_0 << "\n";
        double Q2_check   =  Q2_func(init_state,{Eprime_0, Theta_0,phi_0})  ;
        double x_bj_check =  x_bj_func(init_state,{Eprime_0, Theta_0,phi_0});
        std::cout << " Q2   = " << Q2_func(init_state,{Eprime_0, Theta_0,phi_0})  << "\n";
        std::cout << " x_bj = " << x_bj_func(init_state,{Eprime_0, Theta_0,phi_0}) << "\n";
        REQUIRE(  std::abs(Q2_check - Q2_0) < test_prec );
        REQUIRE(  std::abs(x_bj_check - x_0) < test_prec );
      }

      THEN( "the jacobian should make the cross sections the same ") {

        double xs0_val =  xs0(init_state, {x_0,      Q2_0,   phi_0}) ;
        double xs2_val =  xs2(init_state, {x_0,      Q2_0,   phi_0}) ;
        double xs1_val =  xs1(init_state, {Eprime_0, Theta_0,phi_0}) ;
        std::cout << " xs0_val : " << xs0_val << "\n";
        std::cout << " xs1_val : " << xs1_val << "\n";

        auto jm0_val = jm0.Det(init_state, {Eprime_0, Theta_0, phi_0});

        REQUIRE( std::abs(xs0_val*jm0_val - xs1_val ) < 0.0001 );

        jm0.PrintDebug(init_state, {Eprime_0, Theta_0, phi_0});

        auto ixs0 = make_integrated_cross_section(init_state, xs0);
        auto ixs1 = make_integrated_cross_section(init_state, xs1);
        auto ixs2 = make_integrated_cross_section(init_state, xs2);

        double ixs0_val = 0.0;
        double ixs1_val = 0.0;
        double ixs2_val = 0.0;
        {
          ROOT::Math::IntegratorMultiDim ig2(ROOT::Math::IntegrationMultiDim::kVEGAS); 
          ig2.SetFunction(ixs0);

          const auto& x_v0   = std::get<0>(ixs0.fXS.fPS.fDiff.fIndVars);
          const auto& Q2_v1  = std::get<1>(ixs0.fXS.fPS.fDiff.fIndVars);
          const auto& phi_v2 = std::get<2>(ixs0.fXS.fPS.fDiff.fIndVars);

          std::vector<double> a_min = {x_v0.Min(), Q2_v1.Min(), phi_v2.Min()};
          std::vector<double> b_max = {x_v0.Max(), Q2_v1.Max(), phi_v2.Max()};

          double val = ig2.Integral(a_min.data(), b_max.data() );
          std::cout << "integral result is " << val << std::endl;
          ixs0_val = val;
        }
        {
          ROOT::Math::IntegratorMultiDim ig2(ROOT::Math::IntegrationMultiDim::kVEGAS); 
          ig2.SetFunction(ixs2);

          const auto& x_v0   = std::get<0>(ixs2.fXS.fPS.fDiff.fIndVars);
          const auto& Q2_v1  = std::get<1>(ixs2.fXS.fPS.fDiff.fIndVars);
          const auto& phi_v2 = std::get<2>(ixs2.fXS.fPS.fDiff.fIndVars);

          std::vector<double> a_min = {x_v0.Min(), Q2_v1.Min(), phi_v2.Min()};
          std::vector<double> b_max = {x_v0.Max(), Q2_v1.Max(), phi_v2.Max()};

          double val = ig2.Integral(a_min.data(), b_max.data() );
          std::cout << "integral result is " << val << std::endl;
          ixs2_val = val;
        }

        {
          ROOT::Math::IntegratorMultiDim ig2(ROOT::Math::IntegrationMultiDim::kVEGAS); 
          ig2.SetFunction(ixs1);

          const auto& x_v0   = std::get<0>(ixs1.fXS.fPS.fDiff.fIndVars);
          const auto& Q2_v1  = std::get<1>(ixs1.fXS.fPS.fDiff.fIndVars);
          const auto& phi_v2 = std::get<2>(ixs1.fXS.fPS.fDiff.fIndVars);

          std::vector<double> a_min = {x_v0.Min(), Q2_v1.Min(), phi_v2.Min()};
          std::vector<double> b_max = {x_v0.Max(), Q2_v1.Max(), phi_v2.Max()};

          double val = ig2.Integral(a_min.data(), b_max.data() );
          std::cout << "integral result is " << val << std::endl;
          ixs1_val = val;
        }
        REQUIRE( std::abs(ixs0_val - ixs1_val ) < 0.01 );
        REQUIRE( std::abs(ixs0_val - ixs2_val ) < 0.01 );
        std::cout << " ------------------------------------\n";
      }
    }
  }
}
