#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <typeinfo>

#include "PhaseSpaceVariables.h"
#include "NFoldDifferential.h"
#include "Jacobians.h"
#include "DiffCrossSection.h"
#include "PhysicalConstants.h"

#include "Math/Integrator.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/AllIntegrationTypes.h"
#include "Math/Functor.h"
#include "Math/GaussIntegrator.h"

#define CATCH_CONFIG_MAIN
#include "catch/catch.hpp"

SCENARIO( "DIS Diff Cross Section", "[DiffCrossSection]" ) {

  INFO("The DIS cross section for various fixed target kinematics \n"
      );

  using namespace insane::physics;
  using namespace insane::helpers;
  double test_prec = 1.0e-7;

  // Q2 as a function of x and y
  auto Q2_func = [=](const InitialState& is, const std::array<double,2>& vars){
    double s = is.s();
    double m2= is.p2().M2();
    double xv = vars[0];
    double yv = vars[1];
    return (s-m2)*xv*yv; };

  // x as a function of x (trivial)
  auto x_func = [=](const InitialState& is, const std::array<double,2>& vars){
    double xv = vars[0];
    return xv; };

  // x as a function of x (trivial)
  auto simple_func = [=](const InitialState& is, const std::array<double,1>& vars){
    double xv = vars[0];
    return xv; };

  // y as a function  of x and Q2
  auto y_func = [=](const InitialState& is, const std::array<double,2>& vars){
    double s = is.s();
    double m2= is.p2().M2();
    double xv = vars[0];
    double Q2v = vars[1];
    return Q2v/((s-m2)*xv); };

  //  nu as a function of x and y
  auto nu_func = [=](const InitialState& is, const std::array<double,2>& vars){
    double s = is.s();
    double m2= is.p2().M2();
    double xv = vars[0];
    double yv = vars[1];
    double Q2 = (s-m2)*xv*yv;
    return Q2/(2.0*TMath::Sqrt(m2)*xv);
  };

  // -------------------------------------------------------
  // Phase space variables

  PSV<Invariant> v1({0.001, 0.99}, Invariant::x,  "x" );
  PSV<Invariant> v2({0.5,   10.0}, Invariant::Q2, "Q2");
  IPSV nu("nu");
  PSV<KinematicRelationFunction<decltype(y_func)>> v3(y_func ,"y");

  IPSV y2("y");
  PSV<KinematicRelationFunction<decltype(Q2_func)>> Q2_2(Q2_func, "Q2");
  PSV<KinematicRelationFunction<decltype(nu_func)>> nu_2(nu_func, "nu");

  GIVEN( "Valid fixed DIS kinematics" ) {
    // Prepare the initial state
    double P1 = 12.0;
    double m1 = 0.000511;
    double E1 = std::sqrt(P1*P1+m1*m1);

    double P2 = 0.0;
    double m2 = 0.938;
    double E2 = std::sqrt(P2*P2+m2*m2);

    InitialState init_state(P1, P2, m2);

    // Final state kinematics
    double Q2_0 = 5.0;
    double x_0  = 0.4;
    double y_0  = Q2_0/(init_state.s()-(m2*m2))/x_0;

    //std::cout << "     Q2_0 : " << Q2_0 << "\n";
    //std::cout << "      x_0 : " << x_0  << "\n";
    //std::cout << "      y_0 : " << y_0  << "\n";
    //std::cout << " |J| = d(x,Q2)/d(x,y) = Q2_0/y_0 = " << Q2_0/y_0 << "\n";

    auto diff0 = make_diff(v1,v2);
    auto ps0 = make_phase_space( diff0 , v3);
    auto xs0 = make_diff_cross_section(ps0,
                                  [=](const InitialState& is, const std::array<double,2>& vars){
                                    return 1.0;
                                  });

    WHEN( "the independent and dependent variables are in the PS " ) {
      THEN( " PS::IndVarsInPhaseSpace should return true") {
        REQUIRE( ps0.IndVarsInPhaseSpace({x_0, Q2_0}) == true  );
      }
      THEN( " PS::DepVarsInPhaseSpace should return true") {
        REQUIRE( ps0.DepVarsInPhaseSpace(init_state, {x_0, Q2_0}) == true  );
      }
      THEN( " the cross section should return 1.0") {
        REQUIRE( std::abs(xs0(init_state,{x_0, Q2_0}) - 1.0) < test_prec );
      }
    }

    WHEN( "one independent variable is not in the PS but a computed dependent variable is  " ) {
      double Q2_0 = 11.0;
      double x_0  = 0.8;
      double y_0  = Q2_0/(init_state.s()-(m2*m2))/x_0; // y_0 = 5.5
      //std::cout << "     Q2_0 : " << Q2_0 << "\n";
      //std::cout << "      x_0 : " << x_0  << "\n";
      //std::cout << "      y_0 : " << y_0  << "\n";
      THEN( " PS::IndVarsInPhaseSpace should return false") {
        REQUIRE( ps0.IndVarsInPhaseSpace({x_0, Q2_0}) == false );
      }
      THEN( " PS::DepVarsInPhaseSpace should return true") {
        REQUIRE( ps0.DepVarsInPhaseSpace(init_state, {x_0, Q2_0}) == true  );
      }
      THEN( " the cross section should return 0.0") {
        REQUIRE( std::abs(xs0(init_state,{x_0, Q2_0}) ) < test_prec );
      }
    }

  }

  GIVEN( "Invalid DIS kinematics (y>1)" ) {

    // Prepare the initial state
    double P1 = 5.0;
    double m1 = 0.000511;
    double E1 = std::sqrt(P1*P1+m1*m1);

    double P2 = 0.0;
    double m2 = 0.938;
    double E2 = std::sqrt(P2*P2+m2*m2);

    InitialState init_state(P1, P2, m2);

    // Final state kinematics
    double Q2_0 = 5.0;
    double x_0  = 0.4;
    double y_0  = Q2_0/(init_state.s()-(m2*m2))/x_0;

    //std::cout << "     Q2_0 : " << Q2_0 << "\n";
    //std::cout << "      x_0 : " << x_0  << "\n";
    //std::cout << "      y_0 : " << y_0  << "\n";
    //std::cout << " |J| = d(x,Q2)/d(x,y) = Q2_0/y_0 = " << Q2_0/y_0 << "\n";

    auto diff0 = make_diff(v1,v2);
    auto ps0   = make_phase_space( diff0 , v3);
    //ps0.Print();
    //ps0.PrintDependentVars(init_state, {x_0,Q2_0});
    //std::cout << " DONE \n";
    auto xs0 = make_diff_cross_section(ps0,
                                  [=](const InitialState& is, const std::array<double,2>& vars){
                                    return 1.0;
                                  });

    auto xs2 = make_diff_cross_section(diff0,
                                  [=](const InitialState& is, const std::array<double,2>& vars){
                                    return 1.0;
                                  });

    // this should fail because lambda arguments are incorrect
    //auto xs2 = make_diff_cross_section(ps0,
    //                              [=](const std::array<double,2>& vars){
    //                                return 1.0;
    //                              });

    WHEN( "the independent variables are in the PS but a dependent variable isn't " ) {
      THEN( " PS::IndVarsInPhaseSpace should return true") {
        REQUIRE( ps0.IndVarsInPhaseSpace({x_0, Q2_0}) == true  );
      }
      THEN( " PS::DepVarsInPhaseSpace should return false") {
        REQUIRE( ps0.DepVarsInPhaseSpace(init_state, {x_0, Q2_0}) == false  );
      }
      THEN( " the cross section should return 0.0") {
        REQUIRE( std::abs(xs0(init_state,{x_0, Q2_0}) ) < test_prec );
      }
    }

    WHEN( "one independent and one dependent variable is not in the PS" ) {
      double Q2_0 = 20.0;
      double y_0  = Q2_0/(init_state.s()-(m2*m2))/x_0; // y_0 = 5.5
      //std::cout << "     Q2_0 : " << Q2_0 << "\n";
      //std::cout << "      x_0 : " << x_0  << "\n";
      //std::cout << "      y_0 : " << y_0  << "\n";
      THEN( " PS::IndVarsInPhaseSpace should return false") {
        REQUIRE( ps0.IndVarsInPhaseSpace({x_0, Q2_0}) == false );
      }
      THEN( " PS::DepVarsInPhaseSpace should return false") {
        REQUIRE( ps0.DepVarsInPhaseSpace(init_state, {x_0, Q2_0}) == false  );
      }
      THEN( " the cross section should return 0.0") {
        REQUIRE( std::abs(xs0(init_state,{x_0, Q2_0}) ) < test_prec );
      }
    }
  }

}


SCENARIO( "Integrated DIS Diff Cross Section", "[DiffCrossSection]" ) {

  INFO("The DIS cross section for various fixed target kinematics \n");

  using namespace insane::physics;
  using namespace insane::helpers;
  double test_prec = 1.0e-7;

  // Q2 as a function of x and y
  auto Q2_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double s = is.s();
    double m2= is.p2().M2();
    double xv = vars[0];
    double yv = vars[1];
    return (s-m2)*xv*yv; };

  // x as a function of x (trivial)
  auto x_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double xv = vars[0];
    return xv; };

  // x as a function of x (trivial)
  auto simple_func = [=](const InitialState& is, const std::array<double,1>& vars){
    double xv = vars[0];
    return xv; };

  // y as a function  of x and Q2
  auto y_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double s = is.s();
    double m2= is.p2().M2();
    double xv = vars[0];
    double Q2v = vars[1];
    return Q2v/((s-m2)*xv); };

  //  nu as a function of x andy 
  auto nu_func = [=](const InitialState& is, const std::array<double,3>& vars){
    double s = is.s();
    double m2= is.p2().M2();
    double xv = vars[0];
    double yv = vars[1];
    double Q2 = (s-m2)*xv*yv;
    return Q2/(2.0*TMath::Sqrt(m2)*xv);
  };

  // -------------------------------------------------------
  // Phase space variables

  PSV<Invariant> v1({0.1, 0.9}, Invariant::x,  "x" );
  PSV<Invariant> v2({1.0, 8.0}, Invariant::Q2, "Q2");
  IPSV           phi_psv({0.0, 3.0}, "phi");
  IPSV nu("nu");
  PSV<KinematicRelationFunction<decltype(y_func)>> v3(y_func ,"y");

  IPSV y2("y");
  PSV<KinematicRelationFunction<decltype(Q2_func)>> Q2_2(Q2_func, "Q2");
  PSV<KinematicRelationFunction<decltype(nu_func)>> nu_2(nu_func, "nu");

  GIVEN( "Valid fixed DIS kinematics" ) {
    // Prepare the initial state
    double P1 = 12.0;
    double m1 = 0.000511;
    double E1 = std::sqrt(P1*P1+m1*m1);

    double P2 = 0.0;
    double m2 = 0.938;
    double E2 = std::sqrt(P2*P2+m2*m2);

    InitialState init_state(P1, P2, m2);

    // Final state kinematics
    double Q2_0 = 5.0;
    double x_0  = 0.4;
    double y_0  = Q2_0/(init_state.s()-(m2*m2))/x_0;

    //std::cout << "     Q2_0 : " << Q2_0 << "\n";
    //std::cout << "      x_0 : " << x_0  << "\n";
    //std::cout << "      y_0 : " << y_0  << "\n";
    //std::cout << " |J| = d(x,Q2)/d(x,y) = Q2_0/y_0 = " << Q2_0/y_0 << "\n";

    auto diff0 = make_diff(v1,v2,phi_psv);
    auto ps0 = make_phase_space( diff0 , v3);
    auto xs0 = make_diff_cross_section(ps0, [=](const InitialState& is, const std::array<double,3>& vars)
                                       {
                                         double x_bj = vars.at(0);
                                         double Q2   = vars.at(1);
                                         double phi  = vars.at(2);
                                         double M    = is.p2().M();
                                         double alpha  = 1./137.;
                                         double s    = is.s();
                                         double E0   = (s-M*M)/(2.0*M);
                                         double y    = Q2/(x_bj*(s-M*M));
                                         double nu   = y*E0;//nu_func(is,vars);
                                         //double M  = 0.938;
                                         double F1 = 0.0;
                                         double F2 = 1.0;
                                         double W1 = (1./M)*F1;
                                         double W2 = (1./nu)*F2;
                                         //// compute the Mott cross section (units = mb): 
                                         ////Double_t hbarc2 = 0.38939129; // (hbar*c)^2 = 0.38939129 mb*GeV^2  
                                         double Ep   = (Q2/(2.0*M*x_bj))*(1.0-y)/y;
                                         double th   = 2.0*std::asin(M*x_bj*y/std::sqrt(Q2*(1.0-y)));
                                         double COS2 = std::cos(th)*std::cos(th);
                                         double SIN2 = std::sin(th)*std::sin(th);
                                         double TAN2 = SIN2/COS2; 
                                         double num    = alpha*alpha*COS2; 
                                         double den    = 4.*E0*E0*SIN2; 
                                         //return 1.0/vars.at(1);
                                         double MottXS = num/den;
                                         //// compute the full cross section (units = nb/GeV/sr) 
                                         double fullXsec = MottXS*(W2 + 2.0*TAN2*W1)*insane::units::hbarc2_gev_nb;
                                         return fullXsec;
                                       });

    auto ixs0 = make_integrated_cross_section(init_state, xs0);
    ixs0.Print();

    ROOT::Math::IntegratorMultiDim ig2(ROOT::Math::IntegrationMultiDim::kVEGAS); 
    ig2.SetFunction(ixs0);

    const auto& x_v0   = std::get<0>(ixs0.fXS.fPS.fDiff.fIndVars);
    const auto& Q2_v1  = std::get<1>(ixs0.fXS.fPS.fDiff.fIndVars);
    const auto& phi_v2 = std::get<2>(ixs0.fXS.fPS.fDiff.fIndVars);

    std::vector<double> a_min = {x_v0.Min(), Q2_v1.Min(), phi_v2.Min()};
    std::vector<double> b_max = {x_v0.Max(), Q2_v1.Max(), phi_v2.Max()};

    double val = ig2.Integral(a_min.data(), b_max.data() );
    std::cout << "integral result is " << val << std::endl;
    //status += std::fabs(val-RESULT) > ERRORLIMIT;

    //WHEN( "the independent and dependent variables are in the PS " ) {
    //  THEN( " the cross section should return 1.0") {
    //    REQUIRE( std::abs(xs0(init_state,{x_0, Q2_0}) - 1.0) < test_prec );
    //  }
    //}

  }
}
