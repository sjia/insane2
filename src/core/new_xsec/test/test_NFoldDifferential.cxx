#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <typeinfo>

#include "PhaseSpaceVariables.h"
#include "NFoldDifferential.h"

#define CATCH_CONFIG_MAIN
#include "catch/catch.hpp"

SCENARIO( "Differential", "[NFoldDifferential]" ) {

  using namespace insane::physics;
  using namespace insane::helpers;
  double eps = 1.0e-7;
  double P1 = 5.0;
  double m1 = 0.000511;
  double P2 = 0.0;
  double m2 = 0.938;
  double E1 = std::sqrt(P1*P1+m1*m1);
  double E2 = std::sqrt(P2*P2+m2*m2);
  //double s_calc = m2*m2 + m1*m1 + 2.0*E1*E2;

  InitialState init_state(P1, P2, m2);
  double Q2_0 = 5.0;
  double x_0  = 0.4;
  double y_0  = Q2_0/(init_state.s()-(0.938*0.938))/x_0;
  std::cout << "     Q2_0 " << Q2_0 << "\n";
  std::cout << "      x_0 " << x_0  << "\n";
  std::cout << "      y_0 " << y_0  << "\n";
  std::cout << " Q2_0/y_0 " << Q2_0/y_0 << "\n";
  std::array<double,2> vars = {x_0,y_0};

  GIVEN( "a lambdas returning variable" ) {

    auto Q2_func = [=](const InitialState& is, const std::array<double,2>& vars){
      double s = is.s();
      double m2= is.p2().M2();
      double xv = vars[0];
      double yv = vars[1];
      return (s-m2)*xv*yv; };

    // x as a function of x (trivial)
    auto x_func = [=](const InitialState& is, const std::array<double,2>& vars){
      double xv = vars[0];
      return xv; };

    // y as a function  of x and Q2
    auto y_func = [=](const InitialState& is, const std::array<double,2>& vars){
      double s = is.s();
      double m2= is.p2().M2();
      double xv = vars[0];
      double Q2v = vars[1];
      return Q2v/((s-m2)*xv); };

    //  as a function of x and nu
    auto nu_func = [=](const InitialState& is, const std::array<double,2>& vars){
      double s = is.s();
      double m2= is.p2().M2();
      double xv = vars[0];
      double yv = vars[1];
      double Q2 = (s-m2)*xv*yv;
      return Q2/(2.0*TMath::Sqrt(m2)*xv);
    };

    PSV<Invariant> v1({0.0001,0.999}, Invariant::x , "x" );
    PSV<Invariant> v2({1.0,10.0}    , Invariant::Q2, "Q2");
    IPSV nu("nu");
    PSV<KinematicRelationFunction<decltype(y_func)>> v3(y_func ,"y");
    v3.Print();

    IPSV y2({0.0001,0.999},"y");
    PSV<KinematicRelationFunction<decltype(Q2_func)>> Q2_2(Q2_func, "Q2");
    PSV<KinematicRelationFunction<decltype(nu_func)>> nu_2(nu_func, "nu");

    auto diff =  make_diff(v1,v2);

    int n_i  = diff.N_I;
    REQUIRE( n_i == 2 ); 

    auto diff2 =  make_diff(v1,v2);
    n_i  = diff2.N_I;
    REQUIRE( n_i == 2 ); 

    //REQUIRE( diff2.N_I == 2 ); 
    //REQUIRE( diff2.N_D == 1 ); 

    //diff.Print();
    //std::cout << "N_I " << diff.N_I << std::endl;
    //std::cout << "N_D " << diff.N_D << std::endl;

    //WHEN( "" ) {
    //  THEN( "KinematicRelationFunction can be constructed" ) {
    //    KinematicRelationFunction<decltype(Q2_func)> kine_rel(Q2_func);
    //    REQUIRE( std::abs(kine_rel(init_state,vars)-5.0) < eps  );
    //  }
    //  THEN( "the KinematicRelationFunction helper works" ) {
    //    auto kine_rel = make_kinematic_relation(Q2_func);
    //    REQUIRE( std::abs(kine_rel(init_state,vars)-5.0) < eps  );
    //  }
    //}

      //InitialState init_state2(12.0,0.938);

      //auto ps0 = make_phase_space( make_diff(v1,v2), v3);
      //ps0.Print();

      //auto xs0 = make_cross_section( make_phase_space( make_diff(v1,v2), v3) );

      //auto jm0 = make_jacobian(make_diff(v1,v2),
      //                         make_diff(v1,y2),
      //                         x_func,
      //                         Q2_func );
      ////auto jm0 = make_jacobian(make_diff(v1,v2),
      ////                         make_diff(v1,v2),
      ////                         x_func,
      ////                         [=](const InitialState& is, const std::array<double,2>& vars){
      ////                           double Q2v = vars[1];
      ////                           return Q2v; }
      ////                        );
      //jm0.PrintInfo();
      //jm0.PrintDebug(boosted_state, {x_0,y_0});

      //auto xs0_prime = make_jacobian_transform(xs0,
      //                                         init_state,
      //                                         jm0,
      //                                         make_phase_space( make_diff(v1,y2), Q2_2) );

      //auto xs1_prime = make_jacobian_transform(make_cross_section( make_phase_space( make_diff(v1,v2), v3) ), 
      //                                         init_state, 
      //                                         std::make_tuple(x_func, Q2_func), 
      //                                         make_phase_space( make_diff(v1,y2), Q2_2) );

      //std::cout << " XS0= "  << xs0_prime(init_state2, {x_0,y_0}) << std::endl;
      //std::cout << " XS1= "  << xs1_prime(init_state2, {x_0,y_0}) << std::endl;


  }
}


