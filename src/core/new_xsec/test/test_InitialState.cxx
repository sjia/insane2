#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include "InitialState.h"

#define CATCH_CONFIG_MAIN
#include "catch/catch.hpp"


TEST_CASE( "InitialState tests", "[InitialState]" )
{
  //INFO( "Tester derp " );

  using namespace insane::physics;
  double eps = 1.0e-3;

  double P1 = 5.0;
  double m1 = 0.000511;
  double P2 = 0.0;
  double m2 = 0.938;
  double E1 = std::sqrt(P1*P1+m1*m1);
  double E2 = std::sqrt(P2*P2+m2*m2);
  double s_calc = m2*m2 + m1*m1 + 2.0*E1*E2;

  InitialState init_state(P1, P2, m2);
  init_state.Print();
  REQUIRE( std::abs(init_state.s() - s_calc) < eps );

  InitialState boosted_state   = init_state.GetNewFrame(ROOT::Math::LorentzRotation(ROOT::Math::BoostZ(0.999)));
  boosted_state.Print();
  REQUIRE( std::abs(boosted_state.s() - s_calc) < eps );

  InitialState unboosted_state = boosted_state.GetCMFrame();
  unboosted_state.Print();
  REQUIRE( std::abs(unboosted_state.s() - s_calc) < eps );

  InitialState rest_state      = boosted_state.GetSecondaryRestFrame();
  rest_state.Print();
  REQUIRE( std::abs(rest_state.s() - s_calc) < eps );


}

