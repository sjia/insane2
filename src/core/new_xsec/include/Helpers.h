#ifndef insane_physics_Helpers_H
#define insane_physics_Helpers_H

#include "Math/Vector4D.h"
#include "Math/Vector4Dfwd.h"
#include "Math/VectorUtil.h"
#include "Math/Polar3D.h"

#include <type_traits>
#include <experimental/type_traits>
#include <utility>
#include <tuple>
#include <cstddef>
#include <functional>
#include <stdexcept>
#include <array>
#include <iostream>


namespace insane {

  namespace physics {
    class InitialState;
  }

  /** Template helpers.
   *
   * \ingroup templateHelpers
   */
  namespace helpers {

    using InitialState = insane::physics::InitialState;



    // ---------------------------------------------------------
    // template debugging
    // to print a warning do something like:
    // debug_type<second_arg_type<decltype(f2)>>()
    //
    template <typename T>                
    inline void debug_type(const T&) __attribute__((deprecated));        

    template <typename T>                                          
    inline void debug_type(const T&) { }                           

    template <typename T>                                        
    inline void debug_type() __attribute__((deprecated));        

    template <typename T>                                            
    inline void debug_type() { } 
    //__________________________________________________________

    /** \addtogroup templateHelpers Template helpers
     * @{
     */

    template <bool... Bs>
    constexpr bool require = std::conjunction<std::bool_constant<Bs>...>::value;

    template <bool... Bs>
    constexpr bool either = std::disjunction<std::bool_constant<Bs>...>::value;

    template <bool... Bs>
    constexpr bool disallow = not require<Bs...>;

    using std::experimental::is_detected;
    using std::experimental::is_detected_convertible;
    using std::experimental::is_detected_exact;

    template <template <class...> class Op, class... Args>
    constexpr bool exists = is_detected<Op, Args...>::value;

    template <class To, template <class...> class Op, class... Args>
    constexpr bool converts_to = is_detected_convertible<To, Op, Args...>::value;

    template <class Exact, template <class...> class Op, class... Args>
    constexpr bool identical_to = is_detected_exact<Exact, Op, Args...>::value;


    //@{
    /** Filter variadic template pack by predicate.
     *
     * https://stackoverflow.com/questions/33543287/how-to-filter-a-variadic-template-pack-by-type-derivation
     *
     */
    template <template <typename> class Pred, typename TUPLE, typename Res = std::tuple<> >
    struct Filter;

    template <template <typename> class Pred, typename Res> 
    struct Filter<Pred, std::tuple<>, Res>
    {
      using type = Res;
    };

    template <template <typename> class Pred, typename T, typename ... Ts, typename ... TRes> 
    struct Filter<Pred, std::tuple<T, Ts...>, std::tuple<TRes...>> : 
    Filter<Pred, 
    std::tuple<Ts...>, 
    std::conditional_t<Pred<T>::value, std::tuple<TRes..., T>, std::tuple<TRes...>>>
    { };


    //@}


    /** @name is_tuple
     *  Determine if type is a std::tuple.
     */
    //@{
    template<typename T>
    struct is_tuple_impl : std::false_type {};

    template<typename... Ts>
    struct is_tuple_impl<std::tuple<Ts...>> : std::true_type {};

    template<typename T>
    struct is_tuple : is_tuple_impl<std::decay_t<T>> {};
    //@}

    // std::is_array< std::array > == false !
    // WTF

    /** @name is_std_array
     *  Determine if type is a std::tuple.
     */
    //@{
    template<class T>
    struct is_array : std::is_array<T> {};

    template<class T, std::size_t N>
    struct is_array< std::array<T, N>> : std::true_type {
      using array_t = T; 
      static constexpr std::size_t size = N;
    };

    template<class T>
    struct is_std_array_impl : std::false_type {};

    template<class T, std::size_t N>
    struct is_std_array_impl<std::array<T, N>> : std::true_type {
      using array_t = T;
      static constexpr std::size_t size = N;
    };

    template<class T>
    struct is_std_array : is_std_array_impl<T> {
      using  type                  = typename is_std_array_impl<T>::array_t;
      static constexpr size_t size = is_std_array_impl<T>::N;
    };

    template <class T>
    using is_std_array_t = typename is_std_array<std::decay_t<T>>::type;
    //@}

    // The following code provides std::apply until we have c++17 support
    namespace notstd {

      template<std::size_t...Is>
      auto index_over(std::index_sequence<Is...>){
        return [](auto&&f)->decltype(auto){
          return decltype(f)(f)( std::integral_constant<std::size_t, Is>{}... );
        };
      }
      template<std::size_t N>
      auto index_upto(std::integral_constant<std::size_t, N> ={}){
        return index_over( std::make_index_sequence<N>{} );
      }
      template<class F, class Tuple>
      decltype(auto) apply( F&& f, Tuple&& tup ) {
        auto indexer = index_upto< std::tuple_size<std::remove_reference_t<Tuple>>::value >();
        return indexer(
          [&](auto...Is)->decltype(auto) {
            return std::forward<F>(f)( std::get<Is>(std::forward<Tuple>(tup))... );
          });
      }
    }

    namespace detail {

      template <class Tuple, size_t... Is>
      constexpr auto take_front_impl(Tuple t,
                                     std::index_sequence<Is...>) {
        return std::make_tuple(std::get<Is>(t)...);
      }
      template <size_t N, class Tuple>
      constexpr auto take_front(Tuple t) {
        return take_front_impl(t, std::make_index_sequence<N>{});
      }
      template <class F, std::size_t... Is>
      constexpr auto index_apply_impl(F f,
                                      std::index_sequence<Is...>) {
        return f(std::integral_constant<std::size_t, Is> {}...);
      }

      template <size_t N, class F>
      constexpr auto index_apply(F f) {
        return index_apply_impl(f, std::make_index_sequence<N>{});
      }
      template <class Tuple>
      constexpr auto reverse(Tuple t) {
        return index_apply<std::tuple_size<Tuple>{}>(
          [&](auto... Is) {
            return std::make_tuple(
              std::get<std::tuple_size<Tuple>{} - 1 - Is>(t)...);
          });
      }

      template<typename F, typename Tuple, size_t ...S >
      decltype(auto) apply_tuple_impl(F&& fn, Tuple&& t, std::index_sequence<S...>) {
        return std::forward<F>(fn)(std::get<S>(std::forward<Tuple>(t))...);
      }

      template<typename F, typename Tuple>
      decltype(auto) apply_from_tuple(F&& fn, Tuple&& t) {
        std::size_t constexpr tSize = std::tuple_size<typename std::remove_reference<Tuple>::type>::value;
        return apply_tuple_impl(std::forward<F>(fn),
                                std::forward<Tuple>(t),
                                std::make_index_sequence<tSize>());
      }

      template<typename E>
      using is_class_enum = std::integral_constant<bool, std::is_enum<E>::value && !std::is_convertible<E, int>::value>;

      template<bool...> struct bool_pack;
      template<bool... bs> 
      using all_true = std::is_same<bool_pack<bs..., true>, bool_pack<true, bs...>>;

      template<class R, class... Ts>
      using are_all_convertible = all_true<std::is_convertible<Ts, R>::value...>;

      template<class R, class... Ts>
      using are_all_enum = all_true<std::is_enum<R>::value && !std::is_convertible<Ts, R>::value...>;

    }

    template<typename T> struct is_std_function : public std::false_type {
      using Func_t = T;
    };

    template<typename T>
    struct is_std_function<std::function<T>> : public std::true_type {
      using Func_t = T;
    };

    template <typename T>
    using is_std_function_t = typename is_std_function<T>::Func_t;


    /** @name FunctionTraits
     *  Helpers for function_traits.
     */
    //@{

    /** Function Traits.
     * 
     * https://stackoverflow.com/questions/7943525/is-it-possible-to-figure-out-the-parameter-type-and-return-type-of-a-lambda
     */
    template <typename T>
    struct function_traits : public function_traits<decltype(&T::operator())>
    {};
    // For generic types, directly use the result of the signature of its 'operator()'
    template <typename ClassType, typename ReturnType, typename... Args>
    struct function_traits<ReturnType(ClassType::*)(Args...) const>
    // we specialize for pointers to member function
    {
      enum { arity = sizeof...(Args) }; // arity is the number of arguments.
      typedef ReturnType result_type;
      template <size_t i>
      struct arg {
        using type = typename std::decay<typename std::tuple_element<i, std::tuple<Args...>>::type>::type;
        // the i-th argument is equivalent to the i-th tuple element of a tuple
        // composed of those arguments.
      };
    };

    template<class Func, size_t i>
    using arg_type = typename function_traits<Func>::template arg<i>::type;

    template<class Func, size_t i,
            typename = typename std::enable_if<is_array<arg_type<Func,i>>::value>::type>
    struct arg_array_size_type : std::tuple_size<arg_type<Func,i>>::type {};

    template<class Func, size_t i>
    constexpr size_t arg_array_size = arg_array_size_type<Func,i>::value;

    //@}

    // -------------------------------------------------

    //@{
    /** Return type helpers.
     * Determine the return types.
     */
    template<class Func> using 
    returns_a_four_vector = std::is_same< ROOT::Math::XYZTVector, typename function_traits<Func>::result_type >;

    template<class Func> using 
    returns_a_double = std::is_same< double, typename function_traits<Func>::result_type >;

    template<class Func> using 
    returns_a_std_array = is_std_array< typename function_traits<Func>::result_type >;

    template<class Func> 
    using result_t = typename function_traits<Func>::result_type;

    template<class Func, typename = typename std::enable_if<returns_a_std_array<Func>::value>::type>
    struct result_array_size_impl  {
      static constexpr size_t N = std::tuple_size<result_t<Func>>::value;
    };
    template<class Func>
    constexpr size_t result_array_size = result_array_size_impl<Func>::N;
    template<class Func>
    constexpr size_t result_array_size_v = result_array_size_impl<Func>::N;


    //@}

    template<class Func> using 
    is_first_arg_inital_state = std::is_same<InitialState, typename function_traits<Func>::template arg<0>::type >;

    template<class Func> using 
    second_arg_type = typename std::decay_t< typename function_traits<Func>::template arg<1>::type>;

    template<class Func> using
    second_arg_array_size_type = typename std::conditional< is_array<second_arg_type<Func>>::value,
              typename std::tuple_size< second_arg_type<Func> >::type,
              typename std::integral_constant<size_t, 0>::type >::type;

    template<class Func>
    constexpr size_t second_arg_array_size = second_arg_array_size_type<Func>::value;
    
    //template<class Func>
    //constexpr size_t second_arg_array_size = std::tuple_size< second_arg_type<Func>>::value;


    template<class Func > using 
    array_from_lambda = typename std::array<double, arg_array_size<Func,1> >;

    template<class Func > using 
    is_second_arg_array = std::is_same< second_arg_type<Func>, array_from_lambda<Func> >;

    //template<class Func> using
    //has_array_second_arg  =  std::is_same< second_arg_type<Func>, array_from_lambda<Func> >;
    //std::cout << __PRETTY_FUNCTION__ << "\n";
    
    //template<class Func>
    //constexpr bool has_array_second_arg2(){ return std::is_same< second_arg_type<Func>, array_from_lambda<Func> >::value;}

    template<class Func>
    constexpr bool will_return_a_four_vector(){ return returns_a_four_vector<Func>::value;}


    /** Index dispatcher for looping over tuple.
     *  See https://blog.tartanllama.xyz/exploding-tuples-fold-expressions/
     */
    template <std::size_t... Idx>
    constexpr auto make_index_dispatcher(std::index_sequence<Idx...>) {
      return [] (auto&& f) {
        return std::array<double,sizeof...(Idx)>{f(std::integral_constant<std::size_t,Idx>{})...}; };
    }
    template <std::size_t N>
    constexpr auto make_index_dispatcher() {
      return make_index_dispatcher(std::make_index_sequence<N>{}); 
    }

    template <typename T, std::size_t... Idx>
    constexpr auto make_index_dispatcher(std::index_sequence<Idx...>) {
      return [] (auto&& f) {
        return std::array<T,sizeof...(Idx)>{f(std::integral_constant<std::size_t,Idx>{})...}; };
    }
    template <typename T, std::size_t N>
    constexpr auto make_index_dispatcher() {
      return make_index_dispatcher<T>(std::make_index_sequence<N>{}); 
    }

    //template <typename... Args, typename Func>
    //void for_each(const std::tuple<Args...>& t, const Func& f) {
    //  auto dispatcher = make_index_dispatcher<sizeof...(Args)>();
    //  dispatcher([&](auto idx) { f(std::get<idx>(t)); });
    //}


    /** @} */

    /** 
     * \addtogroup enumHelpers Enum helpers
     *
     * https://stackoverflow.com/questions/207976/how-to-easily-map-c-enums-to-strings
     *
     * \code
     * std::map<MyEnum, const char*> MyMap;
     * map_init(MyMap)
     *    (eValue1, "A")
     *    (eValue2, "B")
     *    (eValue3, "C");
     * \endcode
     *
     * @{
     */

    template<typename T> struct map_init_helper
    {
      T& data;
      map_init_helper(T& d) : data(d) {}
      map_init_helper& operator() (typename T::key_type const& key, typename T::mapped_type const& value)
      {
        data[key] = value;
        return *this;
      }
    };

    template<typename T> map_init_helper<T> map_init(T& item)
    {
      return map_init_helper<T>(item);
    }
    /** @} */


    template<typename T,typename = typename std::enable_if<is_array<std::decay_t<T>>::value>::type>
    auto to_vector_impl(T&& arr, std::true_type){
      //debug_type<T>();
      std::cout << " rvalue \n";
      return std::vector<is_std_array_t<T>>(std::make_move_iterator(arr.begin()),
                                 std::make_move_iterator(arr.end())); // move
    }
    template<typename T,typename = typename std::enable_if<is_array<std::decay_t<T>>::value>::type>
    auto to_vector_impl(T&& arr, std::false_type){
      return std::vector<is_std_array_t<T>>(arr.begin(), arr.end());
    }

    template<typename T>
    auto to_vector(T&& arr){
      return to_vector_impl(arr, std::is_rvalue_reference<T>());
    }


  }
}

#endif
