#ifndef insane_physics_NFoldDifferential_HH
#define insane_physics_NFoldDifferential_HH 1

#include "Helpers.h"
#include "InitialState.h"
#include "PhaseSpaceVariable.h"

#include <functional>
#include <numeric>
#include <utility>

#include <Eigen/Dense>

namespace insane {
  namespace physics {

    namespace helper = helpers;
    namespace detail = helpers::detail;
    namespace notstd = helpers::notstd;

    /** N-Fold Differential.
     *
     *  (dx_1 dx_2 ... dx_N)
     *  Constructor takes \b only \b independent \b variables.
     *  These variables fix the size, N (or N_I), of the std::array<double,N>
     * passed to subsequent functions, such as, dependent variables, cross
     * sections, acceptances, and Jacobians. using the alias VarArray.
     *
     */
    template <class... Vs>
    class NDiff {
    public:
      static constexpr std::size_t N_I = sizeof...(Vs);
      static constexpr std::size_t N   = N_I;
      using VarArray                   = std::array<double, N_I>;

      std::tuple<Vs...> fIndVars;

    public:
      NDiff(const Vs&... vs) : fIndVars(std::make_tuple(vs...)) {}

      /** Test whether the independent variables are within their limits.
       *
       *  Returns array of type std::array<bool,N_I>
       */
      constexpr auto IndVarsInPhaseSpace(const VarArray& v) const {
        auto dispatcher = helper::make_index_dispatcher<bool, N_I>( );
        return dispatcher([&](auto idx) {
          return std::get<idx>(fIndVars).IsContained(v.at(idx));
        });
      }

      constexpr auto GetIndVarsMin() const {
        auto dispatcher = helper::make_index_dispatcher<double, N_I>( );
        return dispatcher([&](auto idx) {
          return std::get<idx>(fIndVars).Min();
        });
      }
      constexpr auto GetIndVarsMax() const {
        auto dispatcher = helper::make_index_dispatcher<double, N_I>( );
        return dispatcher([&](auto idx) {
          return std::get<idx>(fIndVars).Max();
        });
      }


      /** Get array indicating which variables are unformily distributied.
       * Returns std::array<bool,N_I> where the corresponding
       * uniform variables are set to true. See PSVBase::IsUniform().
       */
      constexpr auto IndVarsUniform() const {
        auto dispatcher = helper::make_index_dispatcher<bool, N_I>( );
        return dispatcher([&](auto idx) {
          return std::get<idx>(fIndVars).IsUniform();
        });
      }

      /** Compute rescaled independent variables.
       *
       * The argument are values of the \b normalized \b variables which span
       * [0,1]. This function returns the rescaled (physical) values which are
       * defined by the PSV's min and max.
       *
       * Returns array of type std::array<double,N_I>.
       */
      constexpr auto RescaledVars(const VarArray& v) const {
        auto dispatcher = helper::make_index_dispatcher<double, N_I>( );
        return dispatcher(
            [&](auto idx) { return std::get<idx>(fIndVars).Var(v.at(idx)); });
      }

      /** Compute scaled-variable jacobian.
       *
       *   \f[ \frac{\partial(y_1,...,y_I)}{\partial(x_1,...,x_{N_I})} =
       *       \sum_{i=0}^{N_I} (x_{i}^{\mathrm{max}}-x_{i}^{\mathrm{min}}) \f]
       *
       *   where y are normalized to the range [0,1] and x are the regular
       *   independent variables.
       *
       */
      constexpr auto ScaledVarJacobian( ) const {
        auto dispatcher = helper::make_index_dispatcher<double, N_I>( );
        auto res        = dispatcher([&](auto idx) {
          return std::get<idx>(fIndVars).NormalizedScale( );
        });
        return std::accumulate(res.begin( ), res.end( ), 1.0,
                               std::multiplies<double>( ));
      }

      /** Print.
       */
      void Print(Option_t* opt = "") const {
        std::cout << "NDiff :  (N_I = " << N_I << ")\n";
        auto dispatcher = helper::make_index_dispatcher<int, N_I>( );
        dispatcher([&](auto idx) {
          std::get<idx>(fIndVars).Print( );
          return 0;
        });
      }
    };
    template <class... Vs>
    constexpr auto make_diff(const Vs&... vs) {
      return NDiff<Vs...>(vs...);
    }



    /** Phase Space.
     *
     *  Constructed with N-fold differentail (which provides independent PS
     * variables). The \b dependent \b variables are defined after the
     * differential. \param Diff The differential defining the independent
     * variables \param Vs   The dependent variables
     */
    template <class Diff, class... Vs>
    class PS {
    public:
      static constexpr std::size_t N_I = Diff::N_I;
      static constexpr std::size_t N_D = sizeof...(Vs);
      using VarArray                   = std::array<double, N_I>;
      using DepVarArray                = std::array<double, N_D>;
      using CompleteVars               = std::tuple<InitialState, VarArray>;
      Diff              fDiff;    // Independent variables
      std::tuple<Vs...> fDepVars; // Dependent variables

    public:
      PS(const Diff& d, const Vs&... vs)
          : fDiff(d), fDepVars(std::make_tuple(vs...)) {}
      PS(Diff&& d, Vs&&... vs)
          : fDiff(std::forward<Diff>(d)), fDepVars(std::make_tuple(vs...)) {}

      const auto& ConstDifferential( ) const { return fDiff; }
      auto&       Differential( ) { return fDiff; }

      /** Compute Dependent variables.
       *
       *  Returns array of type std::array<double,N_D>.
       */
      constexpr auto DependentVars(const InitialState& is, const VarArray& v) {
        auto dispatcher = helper::make_index_dispatcher<
            std::tuple_size<decltype(fDepVars)>::value>( );
        return dispatcher(
            [&](auto idx) { return std::get<idx>(fDepVars).func(is, v); });
      }

      /** Compute re-scaled independent variables.
       *
       * The argument is an array of \b normalized \b variables which each span
       * [0,1]. This function returns the rescaled (physical) values which are
       * defined by the PSV's min and max.
       *
       * Returns array of type std::array<double,N_I>.
       */
      constexpr auto RescaledVars(const VarArray& v) const {
        return fDiff.RescaledVars(v);
      }

      /** Test whether all \e independent variables are with their limits.
       *
       * Returns true if all independent variable values are within their
       * defined limits.
       *
       * Note the \c InitialState is not needed since the independent variables
       * are just checked against the PSV's limits. Generally this will not be
       * the case for dependent variables which are computed from the
       * independent variables and the initial state.
       */
      bool IndVarsInPhaseSpace(const VarArray& v) const {
        auto res      = fDiff.IndVarsInPhaseSpace(v);
        //int iv = 0;
        //for(const auto&  var : v){
        //  std::cout << var << " (" << res.at(iv) << "), ";
        //  iv++; 
        //} std::cout << "\n";
        bool all_true = std::accumulate(res.begin( ), res.end( ), true,
                                        std::logical_and<bool>( ));
        //bool all_true = std::accumulate(res.begin( ), res.end( ), true,
        //                                std::logical_and<bool>( ));
        return all_true;
      }

      /** Tests whether all \e dependent variables are within their limits.
       *  Returns true if they are all within their limits.
       */
      bool DepVarsInPhaseSpace(const InitialState& is,
                               const VarArray&     v) const {
        auto dispatcher = helper::make_index_dispatcher<bool, N_D>( );
        auto res        = dispatcher([&](auto idx) {
          auto dv = std::get<idx>(fDepVars);
          return dv.IsContained(dv(is, v));
        });
        bool all_true   = std::accumulate(res.begin( ), res.end( ), true,
                                        std::logical_and<bool>( ));
        return all_true;
      }

      /** Helper function to print the differential and dependent variables. */
      void Print(Option_t* opt = "") const {
        std::cout << "PS :  (N_I = " << N_I << ", N_D = " << N_D << ")\n";
        fDiff.Print( );
        std::cout << "Dependent varaibles:\n";
        auto dispatcher = helper::make_index_dispatcher<
            int, std::tuple_size<decltype(fDepVars)>::value>( );
        dispatcher([&](auto idx) {
          std::get<idx>(fDepVars).Print( );
          return 0;
        });
      }

      /** Helper function to evalute and print the dependent variables. */
      void PrintDependentVars(const InitialState& is, const VarArray& v) const {
        auto vars = DependentVars(is, v);
        for (auto v : vars) {
          std::cout << v << "\n";
        }
      }
    };
    template <class Diff, class... Vs>
    constexpr auto make_phase_space(const Diff& d, const Vs&... vs) {
      return PS<Diff, Vs...>(d, vs...);
    }

    /** Type helpers.
     *
     */
    //@{
    template <typename... X>
    struct is_Differential : std::false_type {};

    template <typename P>
    struct is_Differential<NDiff<P>> : std::true_type {
      using PS_type = P;
    };

    template <typename... X>
    struct is_PhaseSpace : std::false_type {};

    template <typename P>
    struct is_PhaseSpace<PS<P>> : std::true_type {
      using PS_type = PS<P>;
    };

    template <typename P, typename... Vs>
    struct is_PhaseSpace<PS<P, Vs...>> : std::true_type {
      using PS_type = PS<P, Vs...>;
      using DepVars = std::tuple<Vs...>;
    };

    /** Not a phase space, but we can build a PS from a NDiff.
     * Use this template to get the PS type constructed from the NDiff.
     */
    template <typename... P>
    struct is_PhaseSpace<NDiff<P...>> : std::true_type {
      using PS_type = PS<NDiff<P...>>;
    };
    //@}
  
  } // namespace physics
} // namespace insane

#endif

