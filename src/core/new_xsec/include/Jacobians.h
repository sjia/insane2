#ifndef insane_physics_Jacobians_H
#define insane_physics_Jacobians_H 1

#include "Math/IFunction.h"
#include "Math/Functor.h"
#include "Math/Derivator.h"

#include "FinalState.h"
#include "NFoldDifferential.h"

namespace insane {
  namespace physics {

    namespace helper = helpers;
    namespace detail = helpers::detail;
    namespace notstd = helpers::notstd;

    /** @addtogroup derivativematrix Derivative matrix.
     * Implementation of the derivative matrix.
     * @{
     */

    /** Function Derivative.
     * Takes the derivative in the Nth variable of a function F which 
     * has the arguments Initial and std::array<double,N_max>
     *
     * Uses ROOT::Math::Derivator.
     */
    template<class F, std::size_t N_max, std::size_t N>
    struct FunctionDerivative {
      using VarArray  = std::array<double,N_max>;
      F func;
      FunctionDerivative(const F& f) : func(f) {}
      FunctionDerivative(F&& f) : func(std::forward<F>(f)) {}

      double operator()(const InitialState& is, const VarArray& args ) {
        // 1D function
        double x0 = std::get<N>(args);
        auto fx = [=](double xx) { 
          VarArray dv = args; //copy args
          std::get<N>(dv) = xx; // set the arg to xx 
          return func(is,dv);
        };
        ROOT::Math::Functor1D wf(fx);
        ROOT::Math::Derivator der;
        der.SetFunction(wf);
        return der.Eval(x0);
      }
    };
    template<class F, size_t N_max, size_t N>
    constexpr auto make_function_derivative(const F& f) {
      return FunctionDerivative<F,N_max,N>(f);
    }


    /** Calculate each derivative of F.
     *  Again using pack expansion to populate the make_tuple.
     */
    template< class F, size_t N_max, size_t... Nx>
    constexpr auto derivative_vector_impl(const F& f, std::index_sequence<Nx...>){
      return std::make_tuple( FunctionDerivative<F,N_max, Nx>(f)...);
    }

    /** Returns std::tuple<dF2/dx_1,  dF2/dx_2,dF2/dx_3...>.
     * This returns a sequence of derivates. 
     * Each derivative is done with respecto to the Nth  argument of the array vector
     * (which are doubles)
     * Note here we have hard coded the argumetns of all the functions, but 
     * this could be passed from the template instead of just the size.  
     * (Initialsate, array<double,N>)
     */
    template<class F, size_t N>
    constexpr auto  derivative_vector(const F& f){
      return derivative_vector_impl<F,N>(f, std::make_index_sequence<N>{});
    }

    /** Imp helper for derivatives.
     *  Uses pack expansion to get the paramerter pack for make_tuple.
     *  This is just a tuple of tuples.
     */
    template<class Tuple1, size_t...I_var>
    constexpr auto derivatves_impl(const Tuple1& t, std::index_sequence<I_var...>) {
      constexpr int N = std::tuple_size<Tuple1>::value;
      return std::make_tuple( derivative_vector<typename std::tuple_element<I_var,Tuple1>::type,N>(std::get<I_var>(t))... );
    }

    /** Derivative matrix = std::tuple<std::tuple<...> >.
     */
    template<class Tuple1 >
    constexpr auto derivatives(const Tuple1& t) {
      return derivatves_impl(t, std::make_index_sequence<std::tuple_size<Tuple1>{}>{} );
    }



    /** Det helpers.
     * Getting the functions from nested std::tuples 
     */
    template< size_t I, size_t J, class Mat, class VarArray, class F>
    static constexpr auto Det_impl3(Mat& mat, const InitialState& is, const VarArray& vars, F& f){
      return mat(I,J) = f(is,vars);
    }

    template<size_t I, class Mat, class VarArray, class Row, size_t...J_var>
    static constexpr auto Det_impl2(Mat& mat, const InitialState& is, const VarArray& vars,
                                    Row&  r, std::index_sequence<J_var...>) {
      constexpr int N = std::tuple_size<VarArray>::value;
      return std::make_tuple( Det_impl3<I,J_var>(mat, is, vars, std::get<J_var>(r))...);
    }

    template<class Mat, class VarArray, class Tuple, size_t...I_var>
    constexpr auto Det_impl1(Mat& mat, const InitialState& is, const VarArray& vars, Tuple& fs, std::index_sequence<I_var...>) {
      constexpr int N = std::tuple_size<typename std::remove_reference<Tuple>::type>::value;
      return std::make_tuple( Det_impl2<I_var>(mat,is,vars, std::get<I_var>(fs), std::make_index_sequence<N>{})...);
    }


    /** Derivative Matrix implementation class.
     *
     */
    template<class D, std::size_t N>
    class DerivativeMatrix_impl {
    public:
      using VarArray           = std::array<double,N>;
      using EigenMatrix        = Eigen::Matrix<double,N,N>;
      using RawTuplesType      = D;

      D    fFuncs;

    public:
      DerivativeMatrix_impl(const D& t) : fFuncs(t) {
        std::cout << __PRETTY_FUNCTION__ << "\n";
      }
      DerivativeMatrix_impl(D&& t) : fFuncs(std::forward<D>(t)) { }

      double Det(const InitialState& is, const VarArray& vars){
        EigenMatrix mat ;
        Det_impl1(mat, is, vars, fFuncs, std::make_index_sequence<N>{});
        return mat.determinant();
      }
      double Det(const InitialState& is, const VarArray& vars) const {
        EigenMatrix mat ;
        Det_impl1(mat, is, vars, fFuncs, std::make_index_sequence<N>{});
        return mat.determinant();
      }
    };


    /** Derivative Matrix.
     *
     */
    template<class...Vs >
    struct DM {
      static constexpr 
      size_t Ndim     = sizeof...(Vs);
      using  Funcs    = std::tuple<Vs...>;
      using  DMatrix  = decltype(derivatives<Funcs>(std::declval<const Funcs&>()));
      using  DM_Impl  = DerivativeMatrix_impl<DMatrix,Ndim>;

      Funcs   fs;
      DM_Impl fDM;

      DM(std::tuple<Vs...>&& t) : fs(t), fDM( derivatives<Funcs>(t) ) { }
      DM(const std::tuple<Vs...>& t) : fs(t), fDM( derivatives<Funcs>(t) ) { }

      const auto& GetDM_Impl() const { return fDM; }
    };


    /** Construct DerivativeMatrix_impl. 
     *
     *  Building using tuple of lambdas.
     */ 
    template<class... Vs>
    constexpr auto   get_DM(const std::tuple<Vs...>& t) {
      return DM<Vs...>(t);
    }

    //@}


    /** Jacobian Matrix Determminant. 
     *
     * Constructed with two differentials and lambdas which fix the order of
     * the new variables. These functions compute the old variables and are 
     * functions of the new variables.
     * 
     * \f$ J_{ij} = \partial f_{i}/\partial x_{j} \f$
     * \f$ |J| = (\frac{\partial(f_1, ..., f_n)}{\partial(x_1, ..., x_m)}) \f$
     *
     * \code
     *      | (0  ,0), (0,1),   ... (0  ,N-1) |
     *   J= | ...               ... ...       |
     *      | (N-1,0), (N-1,1), ... (N-1,N-1) |
     * \endcode
     */
    template<class D1, class D2, class... Vs>
    class Jacobian {
    public:
      static constexpr std::size_t N = sizeof...(Vs);
      using VarArray         = std::array<double,N>;
      using KineFuncs        = std::tuple<Vs...>;
      using DMatrix          = DM<Vs...>;
      using CompleteVars     = std::tuple<InitialState, VarArray>;

    public:
      D1                 fDiff1; // Numerator
      D2                 fDiff2; // Denominator
      KineFuncs          fFuncs; // var1
      mutable DMatrix    fJMatrix;

    public:
      Jacobian(const Jacobian&)  = default;
      Jacobian(const D1& d1, const D2& d2, Vs... vs) : 
         fDiff1(d1), fDiff2(d2), fFuncs(std::make_tuple(vs...)), fJMatrix(std::make_tuple(vs...))
      { }

      /** Compute the determinant.
       *
       * \code
       *      | (0  ,0), (0,1),   ... (0  ,N-1) |
       *   J= | ...               ... ...       |
       *      | (N-1,0), (N-1,1), ... (N-1,N-1) |
       * \endcode
       */
      double Det(const InitialState& is, const VarArray& vars){
        return fJMatrix.fDM.Det(is,vars);
      }
      double Det(const InitialState& is, const VarArray& vars) const {
        return fJMatrix.fDM.Det(is,vars);
      }

      /** Returns Diff1 variables calculated from Diff2 variables.
       *
       *  Useful for getting the variable values used to evaluate the 
       *  cross section this jacobian is multiplying.
       *
       */
      constexpr auto Vars(const InitialState& is, const VarArray& vars2) {
        auto dispatcher = helper::make_index_dispatcher<N>();
        return dispatcher([&](auto idx) { return std::get<idx>(fFuncs)(is,vars2); } );
      }

      /** Returns Diff1 variables calculated from Diff2 variables.
       *
       *  Useful for getting the variable values used to evaluate the 
       *  cross section this jacobian is multiplying.
       *
       */
      constexpr auto Vars(const InitialState& is, const VarArray& vars2) const {
        auto dispatcher = helper::make_index_dispatcher<N>();
        return dispatcher([&](auto idx) { return std::get<idx>(fFuncs)(is,vars2); } );
      }

      constexpr auto Vars(const CompleteVars& v){
        //return std::apply( Vars, v); // c++17
        return Vars(std::get<0>(v), std::get<1>(v));
      }

      void PrintInfo()
      {
        std::cout << "       d(";
        auto dispatcher = helper::make_index_dispatcher<int,N>();
        dispatcher([&](auto idx) { std::cout << std::get<idx>(fDiff1.fIndVars).GetName() << ", "; return 0; } );
        std::cout << "\b\b)\n";
        std::cout << " J =   -------------\n";
        std::cout << "       d(";
        dispatcher([&](auto idx) { std::cout << std::get<idx>(fDiff2.fIndVars).GetName() << ", "; return 0; } );
        std::cout << "\b\b)\n";
      }

      void PrintDebug(const InitialState& is, const VarArray& vars)
      {
        std::cout << "JacobianDeterminant Print Debug\n";
        auto dep = Vars(is,vars);
        std::cout << "Vars = ( ";
        for(auto& dv : dep) {
          std::cout << dv << ", ";
        }
        std::cout << "\b\b )\n";
        std::cout << "       d(";
        auto dispatcher = helper::make_index_dispatcher<int,N>();
        dispatcher([&](auto idx) { std::cout << std::get<idx>(fDiff1.fIndVars).GetName() << ", "; return 0; } );
        std::cout << "\b\b)\n";
        std::cout << " J =   -------------   =  " << Det(is,vars) << "\n";
        std::cout << "       d(";
        dispatcher([&](auto idx) { std::cout << std::get<idx>(fDiff2.fIndVars).GetName() << ", "; return 0; } );
        std::cout << "\b\b)\n";
      }
    };

    template <
        class D1, class D2, class... Vs>
        //typename = typename std::enable_if<(helpers::returns_a_double<Vs>::value && ...)>::type>
    constexpr auto make_jacobian(const D1& d1, const D2& d2, const Vs&... vs) {
      return Jacobian<D1,D2,Vs...>(d1,d2, vs...);
    }

    template<class D1, class D2, class... Vs>
    constexpr auto make_jacobian(const D1& d1, const D2& d2, const std::tuple<Vs...>& vs) {
      //return notstd::apply([&](const Vs&...args){return make_jacobian(d1,d2, args...);}, vs);
      return notstd::apply([&](const Vs&...args){return Jacobian<D1,D2,Vs...>(d1,d2, args...);}, vs);
    }



    ///** helper for make_functions_tuple
    // */
    //template< class F, size_t N_max, size_t... Nx>
    //constexpr auto make_functions_tuple_impl(const F& f, std::index_sequence<Nx...>){
    //  return std::make_tuple( [&f](const InitialState& is, const std::array<double,N_max>& x){
    //    return std::get<Nx>(f(is,x));
    //  }... );
    //}

    ///** Makes tuple of functions for each value in the returned array. 
    // */
    //template<class F, size_t N = helpers::result_array_size_v<F>>
    //constexpr auto make_functions_tuple(const F& f){
    //  return make_functions_tuple_impl<F,N>(f, std::make_index_sequence<N>{});
    //}
    ///** Used to turn a returned array of variables into a tuple of functions returning 
    // * doubles.
    // */
    //template <
    //    class D1, class D2, class VsFunc,
    //    typename std::enable_if<helpers::returns_a_std_array<VsFunc>::value &&
    //                                       (helpers::result_array_size_v<VsFunc> == D1::N), D1* >::type = nullptr>
    //constexpr auto make_jacobian(const D1& d1, const D2& d2, const VsFunc& vsf) {
    //  return make_jacobian(d1,d2, make_functions_tuple(vsf));
    //}

    //template<class D1, class D2, class VsFunc>
    //class Jacobian<D1,D2, : {
    //};


    template<class D1, class...Vs>
    class IdentityJacobian {
    };


    /** Identity Jacobian.
     *  Differentials on the top and bottom are the same.
     */
    template<class...Vs>
    class IdentityJacobian<NDiff<Vs...>> {
    public:
      using D1 = NDiff<Vs...>;
      static constexpr std::size_t N = D1::N_I;
      using VarArray         = std::array<double,N>;
      using CompleteVars     = std::tuple<InitialState, VarArray>;

    public:
      D1                 fDiff1; // Numerator
      D1                 fDiff2; // Denominator

    public:
      IdentityJacobian(const IdentityJacobian&)  = default;
      IdentityJacobian(const D1& d1) : 
         fDiff1(d1), fDiff2(d1)//, fFuncs(std::make_tuple(vs...)), fJMatrix(std::make_tuple(vs...))
      { }

      /** Compute the determinant of identity matrix.
       */
      double Det(const InitialState& is, const VarArray& vars){ return 1.0; }
      double Det(const InitialState& is, const VarArray& vars) const { return 1.0; }

      /** Returns Diff1 variables calculated from Diff2 variables.
       *  Useful for getting the variable values used to evaluate the 
       *  cross section this jacobian is multiplying.
       */
      constexpr auto Vars(const InitialState& is, const VarArray& vars2) { return vars2; }

      /** Returns Diff1 variables calculated from Diff2 variables.
       *  Useful for getting the variable values used to evaluate the 
       *  cross section this jacobian is multiplying.
       */
      constexpr auto Vars(const InitialState& is, const VarArray& vars2) const { return vars2; }

      constexpr auto Vars(const CompleteVars& v){
        //return std::apply( Vars, v); // c++17
        return Vars(std::get<0>(v), std::get<1>(v));
      }

      void PrintInfo() { }
      void PrintDebug(const InitialState& is, const VarArray& vars) { }
    };

    template<class D1>
    constexpr auto make_identity_jacobian(const D1& d1) {
      return IdentityJacobian<D1>(d1);
    }


  }
}

#endif

