#ifndef insane_physics_PSSampler_HH
#define insane_physics_PSSampler_HH

#include <memory>
#include "TObject.h"
#include "TFoam.h"
#include "TFoamIntegrand.h"
#include "TRandom3.h"

#include "DiffCrossSection.h"


namespace insane {
  namespace physics {
    namespace helper = insane::helpers;


    //@{
    /** Phase Space Sampler.
     *
     * This uses FOAM to efficiently sample an n-dimensional distribution.
     *
     * The template parameter \c IntXS is an \c IntegratedCrossSection.
     *
     * \code
     * class IntegratedCrossSection : public TFoamIntegrand, public ROOT::Math::IBaseFunctionMultiDim ;
     * \endcode
     *
     */
    template<class IntXS>
    class PSSampler : public TObject {
    public:
      static constexpr std::size_t N   = IntXS::N; // N indepedent variables

      struct FoamOptions {
        int            fFoamCells     = 2000; // default(1000) No of allocated number of cells,
        int            fFoamSample    = 1000;  // default(200)  No. of MC events in the cell MC exploration      
        int            fFoamBins      = 40;    // default(8)    No. of bins in edge-histogram in cell exploration
        int            fFoamOptRej    = 1;    // Switch =0 for weighted events; =1 for unweighted events in MC
        int            fFoamOptDrive  = 1;    // (D=2) Option, type of Drive =0,1,2 for TrueVol,Sigma,WtMax
        int            fFoamEvPerBin  = 50;  //maximum no. of EFFECTIVE event per bin, =0 option is inactive
        double         fFoamMaxWtRej  = 1.05;  // Maximum weight in rejection for getting wt=1 events 
        int            fFoamChat      = 1;    // 0,1,2 
      };
      FoamOptions fOptions;

    protected:

      std::shared_ptr<TFoam>          fFoam;
      IntXS                           fIntegratedXS;
      double                          fWeight{1.0}; // To accomodate different relative luminosities 
                                               // (from different material thicknesses), per nucleon cross sections, etc
      
    public:
      mutable std::array<double,N>    fMCvect; // Array for getting normalized variables from FOAM 
      mutable std::array<double,N>    fVars;  // Array of re-scaled (regular) variables

    public:
      PSSampler(const IntXS& xs) : fIntegratedXS(xs), fFoam(std::make_shared<TFoam>("FoamX")), fWeight(1.0) { }
      PSSampler(IntXS&& xs) : fIntegratedXS(std::forward<IntXS>(xs)), fFoam(std::make_shared<TFoam>("FoamX")), fWeight(1.0)  { }

      const auto& ConstIntegratedXS() const {return fIntegratedXS;}
      auto&       IntegratedXS() {return fIntegratedXS;}

      const InitialState& GetInitialState() const { return ConstIntegratedXS().GetInitialState(); }

      double Init() {
        //fFoam = std::make_unique("FoamX");   // Create Simulators
        fFoam->SetkDim(      N );  // No. of dimensions, obligatory!
        fFoam->SetnCells(    fOptions.fFoamCells);
        fFoam->SetnSampl(    fOptions.fFoamSample);   // optional
        fFoam->SetChat(      fOptions.fFoamChat);     // optional
        fFoam->SetnBin(      fOptions.fFoamBins);     // optional
        fFoam->SetOptRej(    fOptions.fFoamOptRej);   // optional
        fFoam->SetOptDrive(  fOptions.fFoamOptDrive); // optional
        fFoam->SetMaxWtRej(  fOptions.fFoamMaxWtRej); // optional
        fFoam->SetEvPerBin(  fOptions.fFoamEvPerBin); // optional
        fFoam->SetRho(       &fIntegratedXS);     // Set n-dim distribution,
        TRandom* PseRan   = new TRandom3(time(NULL));  // Create random number generator
        fFoam->SetPseRan(PseRan);   // Set random number generator
        fFoam->Initialize();        // Initialize simulator, takes a few seconds...

        // Set the variables that are uniform
        int ivar = 0; 
        for(const auto& is_uni : fIntegratedXS.ConstDifferential().IndVarsUniform()) {
          if(is_uni) {
            fFoam->SetInhiDiv(ivar,1);
          }
          ivar++;
        }
        //// Note here we are using the weight in calculating the total cross section
        //// This could be passed in as luminosity which means that totalXSection is 
        //// actually a rate.
        //fTotalXSection = fWeight*fFoam->GetPrimary()*fDiffXSec->GetPhaseSpace()->GetScaledIntegralJacobian();
        double foam_integral = fFoam->GetPrimary();
        double jac           = fIntegratedXS.ScaledVarJacobian();
        double tot           = fWeight * jac * foam_integral;
        fIntegratedXS.SetTotalXS( tot );
        return tot;
      }

      /** Generate event (independent vars).
       *
       */
      auto Generate() {
        fFoam->MakeEvent();          // generate MC event
        fFoam->GetMCvect(fMCvect.data());    // get generated vector (x,y)
        fVars = fIntegratedXS.RescaledVars(fMCvect);
        return fVars;
      }

      /** Foam Option helpers
       *
       */
      FoamOptions GetFoamOptions() const { return fOptions; }
      void        SetFoamOptions(const FoamOptions& opt) {
        fOptions = opt;
        fFoam->SetnCells(  fOptions.fFoamCells   );
        fFoam->SetnSampl(  fOptions.fFoamSample  );
        fFoam->SetnBin(    fOptions.fFoamBins    );
        fFoam->SetOptRej(  fOptions.fFoamOptRej  );
        fFoam->SetOptDrive(fOptions.fFoamOptDrive);
        fFoam->SetEvPerBin(fOptions.fFoamEvPerBin);
        fFoam->SetChat(    fOptions.fFoamChat    );
        fFoam->SetMaxWtRej(fOptions.fFoamMaxWtRej); 
      }
      void SetFoamCells(Int_t v)       { fOptions.fFoamCells = v;   if(fFoam) fFoam->SetnCells(  fOptions.fFoamCells   ); }
      void SetFoamSample(Int_t v)      { fOptions.fFoamSample = v;  if(fFoam) fFoam->SetnSampl(  fOptions.fFoamSample  ); }
      void SetFoamBins(Int_t v)        { fOptions.fFoamBins = v;    if(fFoam) fFoam->SetnBin(    fOptions.fFoamBins    ); }
      void SetFoamOptRej(Int_t v)      { fOptions.fFoamOptRej = v;  if(fFoam) fFoam->SetOptRej(  fOptions.fFoamOptRej  ); }
      void SetFoamOptDrive(Int_t v)    { fOptions.fFoamOptDrive = v;if(fFoam) fFoam->SetOptDrive(fOptions.fFoamOptDrive); }
      void SetFoamEvPerBin(Int_t v)    { fOptions.fFoamEvPerBin = v;if(fFoam) fFoam->SetEvPerBin(fOptions.fFoamEvPerBin); }
      void SetFoamChat(Int_t v)        { fOptions.fFoamChat = v;    if(fFoam) fFoam->SetChat(    fOptions.fFoamChat    ); }
      void SetFoamMaxWtRej(Double_t v) { fOptions.fFoamMaxWtRej = v;if(fFoam) fFoam->SetMaxWtRej(fOptions.fFoamMaxWtRej); }

    };
    template<class IntXS>
    constexpr auto make_ps_sampler(const IntXS& xs) {
      return PSSampler<IntXS>(xs);
    }

    template<typename T>
    struct is_PSSampler : std::false_type { using type = T; };

    template<typename T>
    struct is_PSSampler<PSSampler<T>> : std::true_type { using type = T; };
    //@}



    /** Event Generator.
     *
     * Constructed from PS sampler and Final state. Both objects know about the 
     * "phase sapce" (or NDiff) 
     *
     */
    template <class PSS, class FS,
             typename = typename std::enable_if<is_PSSampler<PSS>::value>::type>
    class EvGen : public PSS {
    public:

      using XYZTVector       = ROOT::Math::XYZTVector;
      static constexpr std::size_t N_parts = FS::N_parts; // N FS particles
      static constexpr std::size_t N_I     = PSS::N; // N independent variables

      using VarArray         = std::array<double,N_I>;
      using CompleteVars     = std::tuple<InitialState, VarArray>;

      FS  fFinalState;

    public:
      EvGen(const PSS& pss, const FS& fs) :  PSS(pss), fFinalState(fs) { }
      EvGen(PSS&& pss, FS&& fs) :  PSS(pss), fFinalState(std::forward<FS>(fs)) { }

      auto GenerateEvent(){
        auto vars = PSS::Generate();
        auto fs_particles = fFinalState.CalculateFinalState(PSS::GetInitialState(), vars);
        return fs_particles;
      }
      /** Generates an event.
       * Returns a tuple containing:
       * 1. Independent variables (std::array<double,N_I>)
       * 2. Final state particles (std::array<XYZTVector,N_parts>)
       * 3. Array of pdg codes    (std::array<int,N_parts>)
       * 4. \todo other dependent variables
       */
      auto operator()(){
        auto vars         = PSS::Generate();
        auto fs_particles = fFinalState.CalculateFinalState(PSS::GetInitialState(), vars);
        auto fs_pdgs      = fFinalState.GetPDGCodes();
        return std::make_tuple(vars, fs_particles, fs_pdgs);
      }

    };
    template <class PSS, class FS> 
    constexpr auto make_event_generator(const PSS& pss, const FS& fs) {
      return EvGen<PSS,FS>(pss,fs);
    }

  }
}

#endif

