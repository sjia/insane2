#ifndef insane_physics_InitialState_H
#define insane_physics_InitialState_H

#include "Math/Vector4D.h"
#include "Math/Vector4Dfwd.h"
#include "Math/VectorUtil.h"
#include "Math/LorentzRotation.h"
#include "Math/Boost.h"

#include "PhysicalConstants.h"
#include "SystemOfUnits.h"

namespace insane {
  namespace physics {

    /** Accelerator Facility Type.
     *  Useful for defining what Frame::Lab means in InitialState.
     */
    enum class AcceleratorType : unsigned int {
      FixedTarget  = 1<<0,
      Collider     = 1<<1,
      IonCollider  = Collider
    };

    /** Frame types.
     *
     *  - Lab : the laboratory frame
     *  - TargetAtRest : A frame where the target is at rest (what about rotation?) 
     *  - RestFrame : Same as TargetAtRest
     *  - NucleonRestFrame : A nucleon at rest frame where the bound nucleon 
     *    is moving with some Fermi motion relative to the nuclear rest frame.
     *  - Photon : For rocesses with inital and final state photons; a frame where 
     *    both photons are colinear. 
     *  - Arbitrary : Any old frame; Unknown;
     */
    enum class FrameOfReference : unsigned int {
      Lab              = 1<<4,
      TargetAtRest     = 1<<5,
      RestFrame        = TargetAtRest,
      NucleonRestFrame = 1<<6,
      Photon           = 1<<7,
      Arbitrary        = 1<<8,
      CM               = 1<<9
    };


    template <FrameOfReference F>
    using RefFrame = std::integral_constant<FrameOfReference, F>;

    /** The inital state for any scattering process.
     *
     *  The "primary" is:
     *  1. The (only) beam in fixed taget experiments. Secondary is fixed target particle.
     *  2. The e beam in electron Ion colliders. Secondary is the ion.
     *  3. The polarized beam in single polarized ion-ion colliders. Secondary is the unpolarized ion.
     *  4. The e- beam in e+ e- colliders.
     *
     * Note that the particles can be offshell w.r.t. their PDG codes.
     */
    class InitialState {
    protected:
      int                    fPrimaryPdgCode{11};
      ROOT::Math::XYZTVector fPrimaryMomentum{0,0,10.0,10.0};
      int                    fSecondaryPdgCode{2212};
      ROOT::Math::XYZTVector fSecondaryMomentum{0.0,0.0,0.0,units::M_p/units::GeV};
      //ROOT::Math::XYZTVector fSecondaryMomentum{0.0,0,-49.99120,50.0}; // collider
      AcceleratorType   fAcceleratorType{AcceleratorType::FixedTarget};
      FrameOfReference  fFrame{FrameOfReference::Lab};

    public:


      InitialState();
      InitialState(int pdg1, const ROOT::Math::XYZTVector& p1, 
                   int pdg2, const ROOT::Math::XYZTVector& p2,
                   AcceleratorType   a = AcceleratorType::FixedTarget,
                   FrameOfReference  f = FrameOfReference::Lab);

      /** constructor taking array of {momentum,mass}.
       */
      InitialState(const std::array<double,2>& , const std::array<double,2>& );
      InitialState(double Pe, double Pp, double Mp = units::M_p/units::GeV);

      explicit InitialState(RefFrame<FrameOfReference::Lab>, double P1, double P2, double M2);

      InitialState(const InitialState&) = default;
      InitialState(InitialState&&)      = default;
      InitialState& operator=(const InitialState&) = default;
      InitialState& operator=(InitialState&&)      = default;
      virtual ~InitialState() = default;

      auto GetNewFrame(const ROOT::Math::LorentzRotation& t) const {
        return InitialState( fPrimaryPdgCode,
                            t(fPrimaryMomentum),
                             fSecondaryPdgCode,
                            t(fSecondaryMomentum) );
      }

      ROOT::Math::XYZTVector p1() const { return fPrimaryMomentum;  }
      ROOT::Math::XYZTVector p2() const { return fSecondaryMomentum;}

      ROOT::Math::XYZTVector GetPrimaryMomentum()   const { return fPrimaryMomentum;  }
      ROOT::Math::XYZTVector GetSecondaryMomentum() const { return fSecondaryMomentum;}

      int GetPrimaryPdgCode()   const { return fPrimaryPdgCode;  }
      int GetSecondaryPdgCode() const { return fSecondaryPdgCode;}

      auto GetCrossingAngle() const { return ROOT::Math::VectorUtil::Angle(fPrimaryMomentum, fSecondaryMomentum); }

      ROOT::Math::XYZTVector::BetaVector GetBoostVectorToCM()            const{return (fPrimaryMomentum + fSecondaryMomentum).BoostToCM(); }
      ROOT::Math::XYZTVector::BetaVector GetBoostVectorToSecondaryRest() const{return (-fSecondaryMomentum.Vect())/(fSecondaryMomentum.E()); }
      ROOT::Math::XYZTVector::BetaVector GetBoostToRestFrame() const { return (-fSecondaryMomentum.Vect())/(fSecondaryMomentum.E()); }

      auto s() const { return (fPrimaryMomentum + fSecondaryMomentum).M2(); } 

      const auto& Frame() const { return fFrame; }
      const auto& Accelerator() const { return fAcceleratorType; }

      void SetFrame(FrameOfReference f) { fFrame = f; }
      void SetAccelerator(AcceleratorType a) { fAcceleratorType = a; }

      void Print() const ;

      /** Get initial state boosted to target (p2) rest frame.
       *  This assumes the secondary particle is the target.
       */
      InitialState GetSecondaryRestFrame() const ;

      /** Get initial state in CM frame.
       */
      InitialState GetCMFrame() const ;

      /** Lorentz boost \e to the frame where p2 is at rest.
       */
      auto BoostToRestFrame() const {
        return ROOT::Math::LorentzRotation( ROOT::Math::Boost( GetBoostVectorToSecondaryRest() ));
      }
      /** Lorentz boost \e from the frame where p2 is at rest.
       */
      auto BoostFromRestFrame() const {
        return ROOT::Math::LorentzRotation( ROOT::Math::Boost( -1.0*GetBoostVectorToSecondaryRest() ));
      }

    };

  }
}

#endif

