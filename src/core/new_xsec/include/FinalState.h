#ifndef insane_physics_FinalState_H
#define insane_physics_FinalState_H

#include "Helpers.h"
#include "PhaseSpaceVariables.h"
#include "InitialState.h"
#include "NFoldDifferential.h"
#include "TransformedDiffXSec.h"
#include "Math.h"
#include <Eigen/Dense>

namespace insane {
  namespace physics {
    
    namespace helper = insane::helpers;


    /** Kinematic variables implementation.
     *
     *  This is the argument that is passed to most functions. 
     *  This class simply holds them in a tuple.
     *  <InitialState, std::array<double,N>>
     */
    template < size_t N, class  I = InitialState >
    struct KinematicVars_Impl {
      using VarArray = std::array<double, N>;
      std::tuple< I, VarArray > Vars;
    };

    /** Kinematic variables.
     *
     *  This is the argument that is passed to most functions. 
     *  This class simply holds them in a tuple.
     *  <InitialState, std::array<double,N>>
     */
    template < typename T,
             class  I = InitialState,
             typename = typename std::enable_if< helper::is_tuple< T >::value >::type >
    struct KinematicVars {
      using VarTuple         = T;
      static constexpr int N = std::tuple_size<typename std::decay<VarTuple>::type>::value;
      using KineVars         = KinematicVars_Impl<N,I>;

      std::tuple<I, VarTuple> Vars;

      KinematicVars(const I& init, const VarTuple& t) : Vars(std::make_tuple(init,t)) { } 
      KinematicVars(VarTuple&& t) : Vars(std::forward(t)) { } 
    };



    /** Four momentum function.
     * 
     * The constructing function/lambda should have the following properties:
     *
     * 1. Returns  a four vector (ROOT::Math::XYZTVector)
     * 2. First argument is insane::physics::InitialState
     * 3. Second argumnet is std::array<double,N>
     *
     */
    template<class Func, typename = typename std::enable_if<
             (helper::returns_a_four_vector    <Func>::value) && 
             (helper::is_first_arg_inital_state<Func>::value) && 
             (helper::is_second_arg_array      <Func>::value) >::type >
    class FourMomentumFunction : public Func {
    public: 
      using XYZTVector   = ROOT::Math::XYZTVector;
      FourMomentumFunction(const Func& v) : Func(v) {}
      FourMomentumFunction(Func&& v) : Func(std::forward<Func>(v)) {}
    };


    /** Multi-four momentum function.
     * 
     * Same as FourMomentumFunction except the return type is an std::array
     * of four momentum. This is useful for efficiently calculating the final
     * state momenta in one function (where the full kinematics would be calculated
     * repeatedly for final states with more than one particle).
     *
     */
    template<class Func, typename = typename std::enable_if<
             (helper::returns_a_std_array    <Func>::value) && 
             (helper::is_first_arg_inital_state<Func>::value) && 
             (helper::is_second_arg_array      <Func>::value) >::type >
    class MultiFourMomentumFunction : public Func {
    public: 
      static constexpr int N_part = std::tuple_size<typename helper::result_t<Func>>::value;
      using  XYZTVector           = ROOT::Math::XYZTVector;
      MultiFourMomentumFunction(const Func& v) : Func(v) {}
      MultiFourMomentumFunction(Func&& v) : Func(std::forward<Func>(v)) {}
    };


    //@{
    /** Final State Particle. 
     *
     *  When constructed the following should be provided:
     *   - A lambda function to calculate the momentum four-vector (see FourMomentumFunction above)
     *   - The lambda arguments like those for a \c KinematicRelationFunction, i.e., InitialState + 
     *     independent variables (std::array<double,N>).
     *
     * Design note:
     *  Since a differential cross section is defined by the final state (and the differential 
     *  variables), the arguments for a lambda defining a \c FinalStateParticle are identical
     *  to those needed to evaluate the coresponding cross section.
     */
    template<class PFunc>
    class FinalStateParticle : public FourMomentumFunction<PFunc> {
    public:
      using XYZTVector       = ROOT::Math::XYZTVector;
      using VarArray         = typename helper::function_traits<PFunc>::template arg<1>::type;
      static constexpr int N = std::tuple_size<typename std::decay<VarArray>::type>::value;
      using CompleteVars     = std::tuple<InitialState, VarArray>;

      int                                 fPdgCode{11};
      double                              fMass{0.0};

    public:

      /** Define final state particle with computing 
       *  function and required phasespace variables.
       */
      FinalStateParticle(const PFunc& f, int pdg, double mass = 0.0) :
        FourMomentumFunction<PFunc>(f), fPdgCode(pdg), fMass(mass) { }

      FinalStateParticle(PFunc&& f, int pdg, double mass = 0.0) :
        FourMomentumFunction<PFunc>(std::forward<PFunc>(f)), fPdgCode(pdg), fMass(mass) { }

      constexpr auto operator()(const CompleteVars& v){
        //return std::apply( operator(), v); // c++17
        return operator()(std::get<0>(v), std::get<1>(v));
      }

      void Print(Option_t* opt = "") const { }
    };
    template<class Func>
    constexpr auto make_final_state_particle(const Func& f, int pdg = 11, double mass = 0.0){
      return FinalStateParticle<Func>(f,pdg,mass);
    }
    //@}




    //@{
    /** Multiple final state particles defined with single function.
     *  Note the "s" on the end of the helper name!
     *
     *  See \c MultiFourMomentumFunction for return type details.
     */
    template<class PFunc>
    class MultiFinalStateParticle : public MultiFourMomentumFunction<PFunc> {
    public:
      using XYZTVector            = ROOT::Math::XYZTVector;
      using VarArray              = typename helper::function_traits<PFunc>::template arg<1>::type;
      static constexpr int N      = std::tuple_size<typename std::decay<VarArray>::type>::value;
      using CompleteVars          = std::tuple<InitialState, VarArray>;
      static constexpr int N_part = std::tuple_size<typename helper::result_t<PFunc>>::value;

      std::array<int,N_part>        fPdgCodes;
      std::array<double,N_part>     fMasses;

    public:

      /** Define final state particle with computing 
       *  function and required phasespace variables.
       */
      MultiFinalStateParticle(const std::array<int,N_part>& pdgs, const std::array<double,N_part>& masses, const PFunc& f) :
        MultiFourMomentumFunction<PFunc>(f), fPdgCodes(pdgs), fMasses(masses) { }

      MultiFinalStateParticle(std::array<int,N_part>&& pdgs, std::array<double,N_part>& masses, PFunc&& f) :
        MultiFourMomentumFunction<PFunc>(std::forward<PFunc>(f)), fPdgCodes(pdgs), fMasses(masses) { }

      constexpr auto operator()(const CompleteVars& v){
        //return std::apply( operator(), v); // c++17
        return operator()(std::get<0>(v), std::get<1>(v));
      }
      void Print(Option_t* opt = "") const { }
    };
    template<class F>
    constexpr auto make_final_state_particles(const std::array<int,   helper::result_array_size<F>>&    pdg,
                                              const std::array<double,helper::result_array_size<F>>& mass, 
                                              const F&  f){
      return MultiFinalStateParticle<F>(pdg,mass,f);
    }
    //@}
    

    //@{
    /**  is_FinalStateParticle type.
     */
    template<typename...X>
    struct is_FinalStateParticle : std::false_type { };
    template<typename T>
    struct is_FinalStateParticle<FinalStateParticle<T>> : std::true_type { using type = T;};
    //@}
    
    //@{
    /**  is_MultiFinalStateParticle type.
     */
    template<typename...X>
    struct is_MultiFinalStateParticle : std::false_type { };
    template<typename T>
    struct is_MultiFinalStateParticle<MultiFinalStateParticle<T>> : std::true_type { using type = T;};
    //@}



    namespace impl {

      template<typename...T2>
      struct FSPart_t_impl : std::false_type {
      };

      //@{
      /** Partial specialization for FinalStateParticle.
       *  Deal with the std::tuple<FinalStateParticle,...>
       */
      template<typename T1, typename T2, typename...T3>
      struct FSPart_t_impl <std::tuple<T1,T2,T3...>,
                           typename std::enable_if<is_FinalStateParticle<T1>::value 
                           && (T1::N  == T2::N) >::type> : std::true_type {
        using  fs_type            = std::tuple<T1,T2,T3...>;
        static constexpr size_t N = T1::N;
      };

      template<typename T1>
      struct FSPart_t_impl <std::tuple<T1>, typename std::enable_if<is_FinalStateParticle<T1>::value>::type> : std::true_type {
        using  fs_type            = std::tuple<T1>;
        static constexpr size_t N = T1::N;
      };
      //@}

      //@{
      /** partial specialization for MultiFinalStateParticle. 
       *  Deal with the std::tuple<MultiFinalStateParticle,...>
       */
      template<typename T1, typename T2, typename...T3>
      struct FSPart_t_impl <std::tuple<T1,T2,T3...>, typename std::enable_if<is_MultiFinalStateParticle<T1>::value
                           && (T1::N  == T2::N) >::type> : std::true_type {
        using  fs_type            = std::tuple<T1,T2,T3...>;
        static constexpr size_t N = T1::N;
      };

      template<typename T1>
      struct FSPart_t_impl <std::tuple<T1>, typename std::enable_if<is_MultiFinalStateParticle<T1>::value>::type> : std::true_type {
        using  fs_type            = std::tuple<T1>;
        static constexpr size_t N = T1::N;
      };
      //@}

      /** Final state particle type info.
       *
       */
      template<typename P, typename...Args> struct FSPart { };

      template<typename P, typename...Args>
      struct FSPart<std::tuple<P,Args...>> : FSPart_t_impl<std::tuple<P,Args...>>  {
        using  FSP_t                    = typename std::tuple<P,Args...>;
        using  part_t                   = P; // Just the first type in the tuple
        static constexpr size_t N       = P::N; // number of independent variagbles
        static constexpr size_t N_parts = std::tuple_size<std::decay_t<FSP_t>>::value; // number of particles

        static std::array<int,N_parts> GetPDGs(const FSP_t& parts) {
          auto dispatcher = helper::make_index_dispatcher<int,N_parts>();
          return dispatcher([&](auto idx){
            return std::get<idx>(parts).fPdgCode; 
          });
        }

        static auto Eval(const FSP_t& parts, const InitialState& is, const std::array<double,N>& v){
          using XYZTVector = ROOT::Math::XYZTVector;
          auto dispatcher = helper::make_index_dispatcher<XYZTVector,N_parts>();
          auto fFS_momentum = dispatcher([&](auto idx) {
            return std::get<idx>(parts)(is,v); 
          });
          return fFS_momentum;
        }
      };

      template<typename P>
      struct FSPart<MultiFinalStateParticle<P>>  {
        using  FSP_t                    = std::tuple<MultiFinalStateParticle<P>>;
        using  part_t                   = MultiFinalStateParticle<P>;
        static constexpr size_t N       = MultiFinalStateParticle<P>::N;//FSPart_t_impl<std::tuple<MultiFinalStateParticle<P>>>::N;
        static constexpr size_t N_part  = MultiFinalStateParticle<P>::N_part;

        static std::array<int,N_part> GetPDGs(const FSP_t& parts) {
          return std::get<0>(parts).fPdgCodes; 
        }
        static auto Eval(const FSP_t& parts, const InitialState& is, const std::array<double,N>& v){
          return std::get<0>(parts)(is,v); 
        }
      };
      
      template<typename P, typename F>
      using is_dimension_same = typename std::is_same< std::integral_constant<size_t, P::N_I>,std::integral_constant<size_t,FSPart<F>::N>>::type;

    } // namespace impl



    /** Final state of cross section. 
     *
     *  Holds the description of the final state and how to calculate 
     *  all the final state particles and their four momenta.
     *
     */
    template <class PhaseSpace, class FSP, 
             typename = typename std::enable_if<impl::is_dimension_same<PhaseSpace,FSP>::value>::type>
    class FinalState {
    public:
      //using XYZTVector                 = ROOT::Math::XYZTVector;
      static constexpr size_t N_I      = PhaseSpace::N_I;
      using VarArray                   = std::array<double,N_I>;
      using CompleteVars               = std::tuple<InitialState, VarArray>;
      using FSTuple_t                  = typename impl::FSPart<FSP>::FSP_t;

      FSTuple_t           fParts;
      PhaseSpace          fPS;
      static constexpr size_t N_parts  = std::tuple_size<FSTuple_t>::value;

    public:
      FinalState(const PhaseSpace& ps, const FSP& parts) : fPS(ps), fParts(parts) { }
      FinalState(PhaseSpace&& ps, FSP&& parts) : fPS(std::forward<PhaseSpace>(ps)), fParts(std::forward<FSP>(parts)) { }

      /** Compute the final state from \c InitialState and \b independent variables.
       *
       *  Returns an array of four-momentum for the final state particles.
       */
      auto CalculateFinalState(const InitialState& is, const VarArray& v) const {
        return impl::FSPart<FSP>::Eval(fParts, is, v);
      }

      auto GetPDGCodes() const {
        return impl::FSPart<FSP>::GetPDGs(fParts);
      }

      void Print(Option_t* opt = "") const {
        //std::cout << __PRETTY_FUNCTION__ << "\n";
      }
    };

    /** Make a sinal state.
     *  Builds final state from \c PhaseSpace and final state particles FSParts.
     */
    template<class FSParts, class Df, class...Vs>
    constexpr auto make_final_state(const PS<Df,Vs...>& ps, const FSParts& parts){
      return FinalState<PS<Df,Vs...>,FSParts>(ps,parts);
    }

    /** Make a sinal state.
     *  Builds final state from \c Differential and final state particles FSParts.
     */
    template<class FSParts, class...Vs>
    constexpr auto make_final_state(const NDiff<Vs...>& d, const FSParts& parts){
      auto ps = make_phase_space(d);
      return FinalState<PS<NDiff<Vs...>>,FSParts>(ps, parts);
    }

    /** Make a sinal state.
     *  Builds final state from \c DiffCrossSection and final state particles FSParts.
     */
    template<class Df, class FSParts, class...F>
    constexpr auto make_final_state(const DiffCrossSection<Df,F...>& xs, const FSParts& parts){
      return FinalState<decltype(xs.fPS),FSParts>(xs.ConstPhaseSpace(), parts);
    }


  }
}

#endif

