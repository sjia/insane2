#ifndef insane_physics_PhaseSpaceVariables_HH
#define insane_physics_PhaseSpaceVariables_HH 1

#include "TNamed.h"
#include <iostream>

#include "Helpers.h"
#include "PhysicalConstants.h"
 
 
// See the excellent Fluentcpp blog series on strong types.
// https://www.fluentcpp.com/2017/05/23/strong-types-inheriting-functionalities-from-underlying/
#include "named_type.hpp"

namespace insane {
  namespace physics {

    namespace helper = insane::helpers;
    
    /** Unused at the moment.
     *
     *  Tag dispatch and strong types.
     */
    namespace variables {

      /** Strong types.
       */
      template<typename Tag>
      using AVar = fluent::NamedType<long double, Tag,
            fluent::Multiplicative,
            fluent::Addable,
            fluent::Comparable,
            fluent::Printable,
            fluent::ImplicitlyConvertibleTo >;

      using Energy = AVar< struct Energy_tag >;
      
      Energy operator "" _GeV( long double e );
      Energy operator "" _MeV( long double e );
      Energy operator "" _keV( long double e );
      Energy operator "" _eV(  long double e );

      using Theta    = AVar< struct Theta_tag >;
      using Phi      = AVar< struct Phi_tag >;
      using Momentum = AVar< struct Momentum_tag >;
    }


    /**
     * @name PSVars Phase Space Variables
     * Phase space variables used for evaluating cross section.
     * @{
     */

    /** Lorentz Invariants.
     */
    enum class Invariant { x, Q2, y, z, t, s, W2, k_dot_P };

    /**  PS variable type.
     */
    enum class PSVarType { E, P, Px, Py, Pz, PT, Theta, Phi, Eta };



    /** Kinematic Function.
     *
     *  Computes scalar quantity using well defined kinematics for an argument
     *  (i.e. a fixed InitialState and independent variable values).
     *
     */
    template<class PFunc, typename = typename std::enable_if<
             !helper::is_std_function<PFunc>::value &&
             helper::returns_a_double         <PFunc>::value && 
             helper::is_first_arg_inital_state<PFunc>::value && 
             helper::is_second_arg_array      <PFunc>::value >::type>
    class KinematicRelationFunction : public PFunc {
    public:
      using VarArray         = typename helper::function_traits<PFunc>::template arg<1>::type;
      static constexpr int N = std::tuple_size<typename std::decay<VarArray>::type>::value;
    public: 
      KinematicRelationFunction(const KinematicRelationFunction& v) : PFunc(v) {}
      KinematicRelationFunction(const PFunc& v) : PFunc(v) {}
    };
    //template<class PFunc>
    //class KinematicRelationFunction<std::function<PFunc>> : public KinematicRelationFunction<PFunc> {};


    /** Kinematic Relation Function helper.
     *
     */
    template <class T0>
    auto make_kinematic_relation(T0&& f) {
      using f_type = std::decay_t<T0>;
      return KinematicRelationFunction<f_type>{ std::forward<T0>(f) };
    }

    /** Phase space variable implementation.
     *
     *  The variable \c x is transformed into the scaled variable \c y such that 
     *  \f[y(min) = 0 \quad \text{and} \quad y(max) = 1\f]
     *  \f[ norm = max - min \f]
     *  \f[ offset = min \f]
     *  \f[ y(x) = (x-offset)/(norm) \f]
     *  \f[ x(y) = y*norm + offset \f]
     *
     */
    class PSVBase : public TNamed {
    protected:
      std::array<double,2> fLimits{0.0,1.0};
      bool                 fUniform = false;
      bool                 fAzimuthal = false;  // azimuthal angle
      //bool                 fIsInvariant{false};
    public:
      enum { dependent = 0} ; 
      PSVBase(const char* n = "", const char* t="") : TNamed(n,t){ }
      PSVBase(const std::array<double,2>& l, const char* n = "", const char* t="") : TNamed(n,t), fLimits(l){ }
      double NormalizedScale() const { return(fLimits.at(1) - fLimits.at(0)); }
      double NormalizedOffset() const { return(fLimits.at(0)); }
      double NormalizedVar(double x) const { return (x-NormalizedOffset())/(NormalizedScale()); }
      double Var(double y) const { return (y*NormalizedScale() + NormalizedOffset()); }
      double Min() const { return fLimits.at(0); }
      double Max() const { if(fAzimuthal) { return fLimits.at(1)+2.0*insane::units::pi; } return fLimits.at(1); }
      bool   IsUniform() const { return fUniform; }
      void   SetUniform(bool v) { fUniform = v; }
      bool   IsContained(double v) const { return ( (v>fLimits.at(0)) && (v<fLimits.at(1)) ) ? true : false; }
      //bool   IsInvariant() const { return fIsInvariant; }
      //void   SetInvariant(bool v) const { fIsInvariant = v; }
      void Print(Option_t* opt = "") const {
        std::cout << TNamed::GetName() << " ["
                  << TNamed::GetName() << "] :  limits (" 
                  << fLimits.at(0) << " - " 
                  << fLimits.at(1) << "), norm ("
                  << NormalizedVar(fLimits.at(0)) << " - " 
                  << NormalizedVar(fLimits.at(1)) << ")\n";
      }
    };



    /** Phase space variable.
     *
     *  Independent variable.
     */
    template<class... F>
    class PSV : public PSVBase {
    public:
      enum { dependent = 0} ; 
    public:
      PSV(const char* n = "", const char* t="") : PSVBase(n,t){ }
      PSV(const std::array<double,2>& l, const char* n = "", const char* t="") : PSVBase(l,n,t){ }
    };



    /** PS Variable.
     *
     *  Specialization for using KinematicRelationFucntion.
     *  Note this will be a dependent variable. 
     */
    template<class F>
    class PSV<KinematicRelationFunction<F>> : public PSVBase {
    public:
      static constexpr std::size_t N = KinematicRelationFunction<F>::N;
      KinematicRelationFunction<F>   func;
      enum { dependent = 1}; 
    public:
      PSV(const F& f, const char* n = "", const char* t = "") : PSVBase(n,t), func(f) { }
      PSV(F&& f, const char* n = "", const char* t = "") : PSVBase(n,t),  func(std::forward<F>(f)) { }
      PSV(const std::array<double,2>& l, F&& f, const char* n = "", const char* t="") : PSVBase(l,n,t), func(f){ }

      double operator()(const InitialState& v, const std::array<double,N> args) { return func(v,args);}
      double operator()(const InitialState& v, const std::array<double,N> args) const { return func(v,args);}
    };
    template <class F>
    auto make_PSV(F&& f) {
      return PSV<KinematicRelationFunction<F>>{ std::forward<F>(f) };
    }
    template <class F>
    auto make_PSV(const std::array<double,2>& l,F&& f) {
      return PSV<KinematicRelationFunction<F>>{l, std::forward<F>(f) };
    }
    template <class F>
    auto make_PSV(const char* n, const std::array<double,2>& l,F&& f) {
      return PSV<KinematicRelationFunction<F>>{l, std::forward<F>(f), n, n};
    }



    /** PS Variable.
     *  Specialization for using KinematicRelationFucntion.
     *  Note this will be a dependent variable. 
     */
    template<class F>//, typename std::enable_if<std::is_enum<E>::value>::type>
    class PSV<KinematicRelationFunction<F>, Invariant> : public PSVBase {
    public:
      static constexpr std::size_t N = KinematicRelationFunction<F>::N;
      Invariant type;
      KinematicRelationFunction<F>   func;
      enum { dependent = 1}; 
    public:
      PSV(Invariant i, const F& f, const char* n = "", const char* t = "") : PSVBase(n,t), type(i), func(f){ }
      PSV(Invariant i, F&& f, const char* n = "", const char* t = "") : PSVBase(n,t),  type(i), func(std::forward<F>(f)) { }
      PSV(const std::array<double,2>& l, Invariant i, F&& f, const char* n = "", const char* t="") : PSVBase(l,n,t), type(i), func(f){ }

      double operator()(const InitialState& v, const std::array<double,N> args) { return func(v,args);}
      double operator()(const InitialState& v, const std::array<double,N> args) const { return func(v,args);}
    };



    /** PS Variable.
     *  This is for independent variables.
     */
    template<>//, typename std::enable_if<std::is_enum<E>::value>::type>
    class PSV<Invariant> : public PSVBase {
    public:
      Invariant type;
      enum { dependent = 0}; 
    public:
      PSV(Invariant i, const char* n = "", const char* t = "") : PSVBase(n,t), type(i) { }
      PSV(const std::array<double,2>& l, Invariant i, const char* n = "", const char* t="") : PSVBase(l,n,t), type(i){ }
    };


    /** PS Variable.
     *  This is for independent variables.
     */
    template<>
    class PSV<> : public PSVBase {
    public:
      enum { dependent = 0}; 
    public:
      PSV(const char* n = "", const char* t = "") : PSVBase(n,t){ }
      PSV(const std::array<double,2>& l, const char* n = "", const char* t="") : PSVBase(l,n,t){ }
    };

    using IPSV = PSV<>;

    /**@}*/   

   
    /** Make an array of IPSVs.
     *
     * Helper to build up PSVs with names 
     */
    template<size_t N,class...Vs>
    std::array<IPSV,N> make_PSV_array(Vs&&...vs){
      auto names      = std::array<std::string,N>(std::forward<Vs...>(vs...));
      auto dispatcher = helper::make_index_dispatcher<N>();
      return dispatcher([&](auto idx) { return IPSV(std::get<idx>(names).c_str()); } );
    }


  }
}

#endif
