#ifndef insane_physics_graphs_HH
#define insane_physics_graphs_HH

#include <tuple>
#include <vector>
#include <algorithm>
#include "TGraphErrors.h"

#include "Helpers.h"

namespace insane {

  /** Build a TGraph + error band with function returning tuple
   *  of values.
   */
  template <class F,
            typename =
                typename std::enable_if<helpers::is_tuple<helpers::result_t<F>>::value>::type>
  struct Graph_yErrors {

    F func;

    Graph_yErrors(const F& f) : func(f) {}

    // This function should return a tuple
    unsigned int N_points = 100;

    TGraphErrors* build_graph(double x_min, double x_max) const {
      double dx = (x_max-x_min)/double(N_points-1);
      auto gr = new TGraphErrors(N_points); 
      std::vector<double> v(N_points,0.0);
      std::generate(v.begin(), v.end(), 
                    [&x_min, &dx, n = 0] () mutable { double val =  x_min + double(n)*dx; n++; return val; });
      std::vector<int> ip(N_points);
      //std::iota(ip.begin(), ip.end(),0);
      std::for_each(v.begin(), v.end(),
                    [&, n = 0] (double x) mutable { 
                      auto vals = func(x);
                      gr->SetPoint(n, x, std::get<0>(vals));
                      gr->SetPointError(n, 0.0, std::get<1>(vals));
                      n++;
                    });
      return gr;
    }

  };
  template<class F>
  auto build_graph_error_func(const F& func, double x_min, double x_max){
     auto g = Graph_yErrors<F>(func);
     return g.build_graph(x_min,x_max);
  }

}


#endif
