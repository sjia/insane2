#ifndef InSANEDiffXSec_H
#define InSANEDiffXSec_H 1

#include <ostream>
#include <vector>
#include <array>
#include <tuple>

#include "TFoam.h"
#include "TFoamIntegrand.h"
#include "Math/IFunction.h"
#include "Math/Vector4D.h"
#include "Math/Vector4Dfwd.h"
#include "Math/VectorUtil.h"

#include "Math/Integrator.h"
#include "Math/IntegratorMultiDim.h"
#include "Math/AllIntegrationTypes.h"
#include "Math/Functor.h"
#include "Math/GaussIntegrator.h"

#include "Helpers.h"
#include "InitialState.h"
#include "NFoldDifferential.h"
#include "PhaseSpaceVariables.h"


namespace insane {
  namespace physics {

    namespace helper = insane::helpers;

    /** @brief Differential Cross section.
     */
    template<class P, class Func, class...Args >
    class DiffCrossSection :  std::false_type {};

    /** @brief Differential Cross section.
     */
    template<class P, class Func> 
    class DiffCrossSection<P,Func, typename std::enable_if<helper::is_std_function<Func>::value>::type>
      : public DiffCrossSection<P, typename helper::is_std_function_t<Func>>
    { };

    /** Differential Cross section.
     *
     *   - Needs N-fold differential variables.
     *   - Builds Final state particles from independent variables
     *   - Calculate independent variables
     *   - Computes differential cross section
     *  Should PS vars be constructed for independent variables? Is this a poor design?
     *   - Pro: can constrain the PS with "independent PS" variables's limits
     *   - Con: mixes independent and dependent variables
     *
     *  \f$ \frac{d^N\sigma}{dx_1 dx_2 ... dx_N}  \f$
     *
     *  Note, that the solid angle \f$ d\Omega = sin(\theta)d\theta d\phi \f$
     *  requires that the flagg fIncludeJacobian be set to true to include the
     *  \f$sin(\theta)\f$ factor in calculated result.
     *
     *  Units used:
     *    - GeV
     *    - radian
     *    - steradian
     *    - nanobarn
     *
     *  Methods to override:
     *    - EvaluateXSec
     *    - InitPhaseSpace
     *    - GetDependentVariables
     *
     *  See xsec_ids.md for XS ID numbering convetion.
     *
     *  Since we are often using natural units to do a calculation, i.e. h=1 and c=1,
     *  we need to multiply cross sections by 
     *  (hc)^2=(197.33 MeV (-fm))^2 = 0.038939129 GeV^2 fm^2 =
     *   0.00038939129 GeV^2 barn =  0.38939129 GeV^2 mbarn
     *
     */
    template<class P, class Func>
    class DiffCrossSection<P,Func> {
    public:
      static constexpr std::size_t N   = P::N_I;
      using VarArray                   = std::array<double,N>;
      using CompleteVars               = std::tuple<InitialState, VarArray>;
      using PhaseSpace_t               = typename is_PhaseSpace<P>::PS_type;
      using Func_t                     = typename helper::is_std_function_t<Func>;

      PhaseSpace_t                      fPS;  // holds indepdent and dependent variables
      KinematicRelationFunction<Func_t> func;

    public:

      template<class T, typename = typename std::enable_if<is_Differential<T>::value>::type>
      DiffCrossSection(const T& diff, const Func& f) : fPS(diff), func(f) { }

      explicit DiffCrossSection(const PhaseSpace_t& ps, const Func& f) : fPS(ps), func(f) { }
      explicit DiffCrossSection(PhaseSpace_t&& ps, Func&& f) : fPS(std::forward<PhaseSpace_t>(ps)), func(std::forward<Func>(f)) { }

      const auto&  ConstPhaseSpace() const { return fPS; }
      auto&        PhaseSpace() { return fPS; }
      const auto&  ConstDifferential() const { return ConstPhaseSpace().ConstDifferential(); }
      auto&        Differential() { return PhaseSpace().Differential(); }

      //@{
      /** Evaluate Cross Section.
       *
       * The cross section evaluation proceeds as follows:
       * 
       *  -# Checks the independent variables are all in the phase space. 
       *     Returns zero if outside of phase space.
       *  -# Checks if the dependent variables are within their defined limits.
       *  -# Evaluates the cross section function
       *  -# Checks that the result is positive, returns zero otherwise.
       *
       */
      double operator()(const InitialState& is, const VarArray& v){
        if( !fPS.IndVarsInPhaseSpace(v) ) {
          return 0.0;
        }
        if( !fPS.DepVarsInPhaseSpace(is,v) ) {
          return 0.0;
        }
        double result = func(is,v);
        if(result > 0) return result;
        return 0.0;
      }

      double operator()(const InitialState& is, const VarArray& v) const {
        if( !fPS.IndVarsInPhaseSpace(v) ) {
          return 0.0;
        }
        if( !fPS.DepVarsInPhaseSpace(is,v) ) {
          return 0.0;
        }
        double result = func(is,v);
        if(result > 0) return result;
        return 0.0;
      }
      double operator()(const CompleteVars& v){
        return operator()(std::get<0>(v), std::get<1>(v) );
      }

      /** Evaluate Cross Section without using phase space limits.
       *
       * The cross section evaluation proceeds as follows:
       * 
       *  -# Evaluates the cross section function
       *  -# Checks that the result is positive, returns zero otherwise.
       *
       */
      double Eval_NoPS(const InitialState& is, const VarArray& v) const {
        double result = func(is,v);
        if(result > 0) return result;
        return 0.0;
      }
      //@}

      /** Compute dependent variables.
       *
       * Returns a std::array<double,N_D>.
       */
      constexpr auto DependentVars(const InitialState& is, const VarArray& v){
        return fPS.DependentVars(is,v);
      }

      void Print(Option_t* opt = "") const {
        std::cout << "DiffCrossSection : N " << N << "\n";
        fPS.Print();
        //std::cout << __PRETTY_FUNCTION__ << "\n";
      }
    };
    template<class P, class Func>
    constexpr auto make_diff_cross_section(const P& ps, const Func& f) {
      return DiffCrossSection<P,Func>(ps, f);
    }





    /** Integrated Cross Section.
     *  
     *  Stores the initial state and total cross section (computed from a PSSampler or  
     *  some other numerical integration).
     *
     *  Passed to the phase space sampler as the function to sample.
     *
     */
    template<class XS>
    class IntegratedCrossSection : public TFoamIntegrand, public ROOT::Math::IBaseFunctionMultiDim {
    public:

      static constexpr std::size_t N   = XS::N;
      using VarArray                   = std::array<double,N>;
      using CompleteVars               = std::tuple<InitialState, VarArray>;

      InitialState  fInitState;
      XS            fXS;
      double        fTotalXS;

    public:
      IntegratedCrossSection(const InitialState& is, const XS& xs, double tot = 0.0 ) : fInitState(is), fXS(xs), fTotalXS(tot) { }
      IntegratedCrossSection(InitialState&& is, XS&& xs, double tot = 0.0 ) : 
        fInitState(std::forward<InitialState>(is)), fXS(std::forward<XS>(xs)), fTotalXS(tot){ }

      const InitialState& GetInitialState() const { return fInitState; }

      const auto&  ConstPhaseSpace() const { return fXS.ConstPhaseSpace(); }
      auto&        PhaseSpace() { return fXS.PhaseSpace(); }
      const auto&  ConstDifferential() const { return fXS.ConstPhaseSpace().ConstDifferential(); }
      auto&        Differential() { return fXS.PhaseSpace().Differential(); }

      /** Compute scaled variable jacobian.
       *  See \c NDiff for implementation.
       *
       *   \f[ \frac{\partial(y_1,...,y_I)}{\partial(x_1,...,x_{N_I})} = 
       *       \sum_{i=0}^{N_I} (x_{i}^{\text{max}}-x_{i}^{\text{min}}) \f]
       *   where y are normalized to the range [0,1] and x are the regular
       *   independent variables.
       *
       */
      constexpr auto ScaledVarJacobian() const { return fXS.ConstDifferential().ScaledVarJacobian(); }

      /** Compute re-scaled independent variables.
       *
       * The argument is an array of \b normalized \b variables which each span [0,1].
       * This function returns the rescaled (physical) values which are defined by the
       * PSV's min and max.
       *
       * Returns array of type std::array<double,N_I>.
       */
      constexpr auto RescaledVars(const VarArray& v) const { return fXS.ConstPhaseSpace().RescaledVars(v); }

      /** Density function needed by FOAM.
       *  The x arguments are the \b normalized \b variables which span [0,1].
       *  They are converted into regular variables before evaluating the cross section. 
       */
      virtual Double_t 	Density (Int_t ndim, Double_t * x) {
        auto x_arr = reinterpret_cast< std::array<double,N >* >( x );
        //auto vars = RescaledVars(*x_arr);
        //for(const auto& v: vars) {
        //  std::cout << v << ", ";
        //} std::cout << "\n";
        return fXS(fInitState, RescaledVars(*x_arr));
      }

      void   SetTotalXS(double xs) { fTotalXS = xs; }
      double TotalXS() const {return fTotalXS;}


      virtual IntegratedCrossSection*  Clone(const char * newname) const {
        auto * copy = new IntegratedCrossSection<XS>(fInitState,fXS,fTotalXS);
        //(*copy) = (*this);
        return copy;
      }
      virtual IntegratedCrossSection*  Clone() const { return( Clone("") ); }
      virtual unsigned int NDim() const { return N;}

      void Print(Option_t* opt = "") const {
        std::cout << "IntegratedCrossSection : \n";
        fInitState.Print();
        fXS.Print();
        std::cout << " total XS : " << fTotalXS << std::endl;
      }


      double CalculateTotalXS() {
        ROOT::Math::IntegratorMultiDim ig2(ROOT::Math::IntegrationMultiDim::kVEGAS,1.0e-8); 
        ig2.SetFunction(*this);

        auto  a = ConstPhaseSpace().ConstDifferential().GetIndVarsMin();
        auto  b = ConstPhaseSpace().ConstDifferential().GetIndVarsMax();

        //auto bval = std::begin(b);
        //for(auto val : a) {
        //  std::cout << "(" << val << ", " << *bval << ")\n";
        //  bval++;
        //}

        fTotalXS = ig2.Integral(&a[0], &b[0]);
        return fTotalXS;
      }


    private:
      virtual double DoEval(const double *x) const {
        const auto x_arr = reinterpret_cast< const std::array<double,N >* >( x );
        return fXS(fInitState, *x_arr);
      }
    };
    template<class XS>
    constexpr auto make_integrated_cross_section(const InitialState& is, const XS& xs, double tot = 0.0) {
      return IntegratedCrossSection<XS>(is, xs, tot);
    }

  }
}

#endif

