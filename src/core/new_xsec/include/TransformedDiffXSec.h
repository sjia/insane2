#ifndef insane_physics_TransformedDiffXSec_HH
#define insane_physics_TransformedDiffXSec_HH 1

#include "DiffCrossSection.h"
#include "Jacobians.h"
#include "PhaseSpaceVariables.h"
#include "Helpers.h"

#include <experimental/tuple>

namespace insane {
  namespace physics {

    namespace detail = helpers::detail;
    namespace notstd = helpers::notstd;

    //template <class...T>
    template <class XS, class JM, class P, typename T>
    class TransformedCrossSection ;

    template<typename...X>
    struct is_DiffCrossSection : std::false_type { };

    template<typename...Vs>
    struct is_DiffCrossSection<DiffCrossSection<Vs...>> : std::true_type { };

    template<typename...Vs>
    struct is_DiffCrossSection<TransformedCrossSection<Vs...>> : std::true_type { };

    //template<typename P, typename F>
    //struct is_DiffCrossSection<TransformedCrossSection<P,F>> : std::true_type { using PS_type = P; using F_type = F; };

    template<typename...X>
    struct is_IntegratedCrossSection : std::false_type { };

    template<typename P>
    struct is_IntegratedCrossSection<IntegratedCrossSection<P>> : std::true_type { using PS_type = P;};


    /** Transform a Cross Section with the Jacobian. 
     *
     *  Apply Jacobian to XS yielding another XS.
     *
     */
    template <class XS, class JM, class P,
             typename = typename std::enable_if<is_DiffCrossSection<XS>::value>::type>
    class TransformedCrossSection {
    public:
      static constexpr std::size_t N   = P::N_I;
      static constexpr std::size_t N_I = P::N_I;
      using VarArray                   = std::array<double,N>;
      using CompleteVars               = std::tuple<InitialState, VarArray>;
      using PhaseSpace_t               = typename is_PhaseSpace<P>::PS_type;

      PhaseSpace_t   fPS;
      InitialState   fInit; // initial state for evaluating the base XS
      XS             fXS;
      JM             fJM;

    public:
      TransformedCrossSection(const XS& xs, const InitialState& is,
                              const JM& jm, const PhaseSpace_t& ps ) :
        fXS(xs), fInit(is), fJM(jm), fPS(ps) { }

      template<class T, typename =
               typename std::enable_if<is_Differential<T>::value>::type>
      TransformedCrossSection(const XS& xs, const InitialState& is,
                              const JM& jm, const T& diff ) :
        fXS(xs), fInit(is), fJM(jm), fPS(diff) { }

      const auto&  ConstPhaseSpace() const { return fPS; }
      auto&        PhaseSpace() { return fPS; }
      const auto&  ConstDifferential() const { return ConstPhaseSpace().ConstDifferential(); }
      auto&        Differential() { return PhaseSpace().Differential(); }

      /** Evaluate transformed cross section.
       *
       */
      double operator()(const InitialState& is, const VarArray& v){
        if( !fPS.IndVarsInPhaseSpace(v) ) {
          return 0.0;
        }
        if( !fPS.DepVarsInPhaseSpace(is,v) ) {
          return 0.0;
        }
        auto old_is   = fInit;
        auto old_vars = fJM.Vars(is,v);
        return fXS.Eval_NoPS(old_is,old_vars)*(fJM.Det(is,v));
      }
      double operator()(const InitialState& is, const VarArray& v) const {
        if( !fPS.IndVarsInPhaseSpace(v) ) {
          return 0.0;
        }
        if( !fPS.DepVarsInPhaseSpace(is,v) ) {
          return 0.0;
        }
        auto old_is   = fInit;
        auto old_vars = fJM.Vars(is,v);
        return fXS.Eval_NoPS(old_is,old_vars)*(fJM.Det(is,v));
      }

      double operator()(const CompleteVars& v){
        return operator()(std::get<0>(v), std::get<1>(v) );
      }
      double Eval_NoPS(const InitialState& is, const VarArray& v) const {
        auto old_is   = fInit;
        auto old_vars = fJM.Vars(is,v);
        return fXS.Eval_NoPS(old_is,old_vars)*(fJM.Det(is,v));
      }

      /** Compute dependent variables.
       * Returns a std::array<double,N_D>.
       */
      constexpr auto DependentVars(const InitialState& is, const VarArray& v){
        return fPS.DependentVars(is,v);
      }

      /** Print.
       * Helper.
       */
      void Print() const {
        std::cout << "TransformedDiffXSec : N " << N << "\n";
        fPS.Print();
        //std::cout << __PRETTY_FUNCTION__ << "\n";
      }
    };



    /** Create a Jacobian-transformed cross section.
     *
     *  Helper to make a transformed XS from a user supplied Jacobian (JM) 
     *  and phase space (Ps).
     */
    template<class XS, class JM, class Ps>
    constexpr auto  make_jacobian_transformed_xs(const XS& xs, const InitialState& is,
                                                 const JM& jm, const Ps& ps){
      static_assert( std::is_same<decltype(xs.fPS.fDiff), decltype(jm.fDiff1)>::value, "XS and jacobian differentials do not match");
      assert( ((std::get<0>(xs.fPS.fDiff.fIndVars))).Compare(&(std::get<0>(jm.fDiff1.fIndVars)))==0 );
      using TXS = TransformedCrossSection<XS,JM,Ps>;
      return TXS(xs, is, jm, ps);
    }

    /** Create a Jacobian-transformed cross section.
     *
     *  Helper to make a transformed XS from a user supplied Jacobian (JM) 
     *  and NDiff (Df). Note a phase space is automatically constructed from
     *  the Df (see \c DiffCrossSection).
     */
    template <class XS, class JM, class Df, 
             typename = typename std::enable_if<is_Differential<Df>::value>::type>
    constexpr auto  make_jacobian_transformed_xs(const XS& xs, const InitialState& is,
                                            const JM& jm, const Df& d){
      //static_assert( std::is_same<decltype(xs.fPS.fDiff), decltype(jm.fDiff1)>::value, "XS and jacobian differentials do not match");
      //assert( ((std::get<0>(xs.fPS.fDiff.fIndVars))).Compare(&(std::get<0>(jm.fDiff1.fIndVars)))==0 );
      using TXS = TransformedCrossSection<XS,JM,Df>;
      return TXS(xs, is, jm, d);
    }

    /** Create new cross section.
     *
     *  Skip creating the Jacobian and build it from a new phase space and 
     *  tuple of functions that calculate the old variables as a function of the new variables .
     */
    template<class XS, class Ps, class...FS>
    constexpr auto  make_jacobian_transformed_xs(const XS& xs, const InitialState& is,
                                            const std::tuple<FS...>& fs,
                                            const Ps& ps){
      return make_jacobian_transformed_xs(xs, is,
                                          make_jacobian(xs.fPS.fDiff, ps.fDiff, fs),
                                          ps);
    }

  }
}

#endif

