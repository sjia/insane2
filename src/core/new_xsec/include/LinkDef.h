#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedef;


#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ namespace insane;
#pragma link C++ namespace insane::physics;
#pragma link C++ namespace insane::helpers;

#pragma link C++ class insane::physics::InitalState+;

#pragma link C++ class insane::physics::PSVBase+;
//#pragma link C++ class insane::physics::PhaseSpace+;
//#pragma link C++ class insane::physics::PhaseSpaceVariable+;
//#pragma link C++ class insane::physics::DiscretePhaseSpaceVariable+;
//#pragma link C++ class std::vector<insane::physics::PhaseSpaceVariable*>+;
//#pragma link C++ class insane::physics::PhaseSpaceSampler+;

#pragma link C++ class insane::physics::FinalState+;
//#pragma link C++ class insane::physics::DiffXSecKinematicKey+;
//#pragma link C++ class insane::physics::InclusiveDiffXSec+;
//#pragma link C++ class insane::physics::ExclusiveDiffXSec+;
//#pragma link C++ class insane::physics::InclusiveMottXSec+;
//#pragma link C++ class insane::physics::ExclusiveMottXSec+;
//#pragma link C++ class insane::physics::FlatInclusiveDiffXSec+;
//#pragma link C++ class insane::physics::FlatExclusiveDiffXSec+;

#endif

