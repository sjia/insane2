#include "Jacobians.h"

#include "PhaseSpace.h"


namespace insane {
  namespace physics {

    //void compile_test(){

    //  // Change of variables (x,Q2) -> (x,y)
    //  // Jacobian = Q2/y
    //  
    //  PhaseSpaceVariable x1("x","x");
    //  PhaseSpaceVariable Q2("Q2","Q2");
    //  Q2.SetMaximum(50);
    //  PhaseSpaceVariable y1("y","y");

    //  PhaseSpace   ps1;
    //  ps1.AddVariable(x1);
    //  ps1.AddVariable(Q2);

    //  InitialState init_state(12.0,0.938);

    //  double Q2_0 = 5.0;
    //  double x_0  = 0.4;
    //  double y_0  = Q2_0/(init_state.s()-(0.938*0.938))/x_0;
    //  std::cout << "     Q2_0 " << Q2_0 << "\n";
    //  std::cout << "      x_0 " << x_0  << "\n";
    //  std::cout << "      y_0 " << y_0  << "\n";
    //  std::cout << " Q2_0/y_0 " << Q2_0/y_0 << "\n";

    //  PhaseSpace   ps2;
    //  ps2.AddVariable(x1);
    //  ps2.AddVariable(y1);

    //  NFoldDifferential<2> diff1(init_state, {&x1,&Q2});
    //  NFoldDifferential<2> diff2(init_state, {&x1,&y1});


    //  // Q2 as a function of x and y
    //  auto Q2_func = [=](const InitialState& is, const std::array<double,2>& vars){
    //    double s = is.s();
    //    double m2= is.p2().M2();
    //    double xv = vars[0];
    //    double yv = vars[1];
    //    return (s-m2)*xv*yv; };

    //  // x as a function of x (trivial)
    //  auto x_func = [=](const InitialState& is, const std::array<double,2>& vars){
    //    double xv = vars[0];
    //    return xv; };

    //  // y as a function  of x and Q2
    //  auto y_func = [=](const InitialState& is, const std::array<double,2>& vars){
    //    double s = is.s();
    //    double m2= is.p2().M2();
    //    double xv = vars[0];
    //    double Q2v = vars[1];
    //    return Q2v/((s-m2)*xv); };


    //  auto vs = std::make_tuple(x_func, Q2_func);

    //  auto ds = derivatives< typename std::decay<decltype(vs)>::type >(vs);

    //  DerivativeMatrix_impl< decltype(ds),2> dm(ds);

    //  std::cout << dm.Det(init_state,{x_0,Q2_0}) << std::endl;

    //  auto dm2 = get_DM(x_func, Q2_func);


    //  for(int i=0;i<5;i++){
    //    x_0 += 0.02*i;
    //    std::cout << dm2.Det(init_state,{x_0,Q2_0}) << std::endl;
    //  }

    //  std::cout << " DM : \n" ;
    //  DM<decltype(x_func), decltype(Q2_func)>  amat(x_func, Q2_func);

    //  std::cout << "\n JacobianDeterminant : \n" ;

    //  JacobianDeterminant<decltype(x_func), decltype(Q2_func)> jm(diff1, diff2,
    //                                                             x_func, Q2_func);

    //  x_0 = 0.4;
    //  std::cout << jm.Det(init_state,{x_0,Q2_0}) << std::endl;

    //  jm.PrintDebug(init_state,{x_0,y_0});
    //  jm.PrintInfo();
    //}
  }
}

