#include "InitialState.h"

namespace insane {
  namespace physics {

    InitialState::InitialState(){ }

    InitialState::InitialState(int pdg1, const ROOT::Math::XYZTVector& p1, 
                               int pdg2, const ROOT::Math::XYZTVector& p2,
                               AcceleratorType   a,
                               FrameOfReference  f) :
      fPrimaryPdgCode(pdg1), fPrimaryMomentum(p1),
      fSecondaryPdgCode(pdg2), fSecondaryMomentum(p2),
      fAcceleratorType(a), fFrame(f)
    { }
    //______________________________________________________________________________

    InitialState::InitialState(const std::array<double,2>& p1, const std::array<double,2>& p2) 
    {
      fPrimaryMomentum   = ROOT::Math::PxPyPzMVector{0,0,-p1.at(0), p1.at(1)};
      fSecondaryMomentum = ROOT::Math::PxPyPzMVector{0,0,p2.at(0), p2.at(1)};
      if( p2.at(0) == 0.0 ) {
        fAcceleratorType = AcceleratorType::FixedTarget;
        fFrame           = FrameOfReference::TargetAtRest;
      } else {
        fAcceleratorType = AcceleratorType::Collider;
        fFrame           = FrameOfReference::Arbitrary;
      }
    }

    InitialState::InitialState(double Ee, double Ep, double Mp)
    {
      double Me = 0.000511;
      fPrimaryMomentum   = ROOT::Math::PxPyPzMVector{0,0,-Ee, Me};
      fSecondaryMomentum = ROOT::Math::PxPyPzMVector{0,0,Ep, Mp};
      if( Ep == 0.0 ) {
        fAcceleratorType = AcceleratorType::FixedTarget;
        fFrame           = FrameOfReference::TargetAtRest;
      } else {
        fAcceleratorType = AcceleratorType::Collider;
        fFrame           = FrameOfReference::Arbitrary;
      }
    }
    
    InitialState::InitialState(RefFrame<FrameOfReference::Lab> f, double P1, double P2, double M2) : InitialState(P1,P2,M2)
    {
      fAcceleratorType = AcceleratorType::FixedTarget;
      fFrame           = FrameOfReference::Lab;
    }
    
    void InitialState::Print() const
    {
      std::cout << " ------------------------------------\n";
      std::cout << "         s : " << s() << std::endl;
      std::cout << " Primary   : " << fPrimaryMomentum   <<  " [" << fPrimaryPdgCode   << "]" << std::endl;
      std::cout << " Secondary : " << fSecondaryMomentum <<  " [" << fSecondaryPdgCode << "]" << std::endl;
      std::cout << " Frame     : ";
      if(fFrame == FrameOfReference::Lab) {
        std::cout << "Lab\n";
      } else if(fFrame == FrameOfReference::RestFrame) {
        std::cout << "RestFrame\n";
      } else if(fFrame == FrameOfReference::Arbitrary) {
        std::cout << "Arbitrary\n";
      } else {
        std::cout << "Unknown\n";
      }
      
    }
    //______________________________________________________________________________

    InitialState InitialState::GetSecondaryRestFrame() const
    {
      // Get boosted initial stat where the secondary particle is at rest.
      InitialState f = GetNewFrame( ROOT::Math::LorentzRotation( ROOT::Math::Boost( GetBoostVectorToSecondaryRest() )) );
      f.SetFrame(FrameOfReference::TargetAtRest);
      f.SetAccelerator(AcceleratorType::FixedTarget);
      return f;
    }
    //______________________________________________________________________________

    InitialState InitialState::GetCMFrame() const
    {
      // Get initial state in CM frame
      InitialState f = GetNewFrame( ROOT::Math::LorentzRotation( ROOT::Math::Boost(GetBoostVectorToCM())) );
      f.SetFrame(FrameOfReference::CM);
      f.SetAccelerator(AcceleratorType::Collider);
      return f;
    }
    //______________________________________________________________________________

  }
}

