#include "PhaseSpaceVariables.h"

namespace insane {
  namespace physics {

    namespace variables {

      Energy operator "" _GeV( long double e )
      {
        return Energy{e};
      }

      Energy operator "" _MeV( long double e )
      {
        return Energy{e/(1.0e3)};
      }
      Energy operator "" _keV( long double e )
      {
        return Energy{e/(1.0e6)};
      }
      Energy operator "" _eV( long double e )
      {
        return Energy{e/(1.0e9)};
      }

    }
  }
}
