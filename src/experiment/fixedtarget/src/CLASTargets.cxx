#include "CLASTargets.h"

namespace insane {
   namespace physics {

      Helium4GasTarget::Helium4GasTarget(const char * name, const char * title) : 
         Target(name,title)
      { 
         DefineMaterials();
      }
      //______________________________________________________________________________

      Helium4GasTarget::~Helium4GasTarget()
      { }
      //______________________________________________________________________________

      void Helium4GasTarget::DefineMaterials()
      {
         using namespace insane::units;
         double       target_length = 35.0;
         auto * matHe4 = new TargetMaterial("He4","He4",2,4);

         double density_at_1atm_300K = 0.164*kg/m3; // kg/m3
         double a_noUnit     = 4.003;
         double pre_noUnit   = 3.0;
         double tempe_noUnit = 298.;
         double pressure     = 3.0*atmosphere;
         double temperature  = 298.*kelvin;
         double He4_density  = density_at_1atm_300K*pre_noUnit;
         // density is 0.178 at 0C, 0.169 at 15C, 0.165 at 25C (all in units of kg/m3)
         //
         //(a_noUnit*pre_noUnit)/(0.0821*tempe_noUnit)*kg/m3; //0.164*kg/m3 at 1 atm;

         matHe4->fLength          = target_length;    //cm
         matHe4->fDensity         = He4_density/(g/cm3); // g/cm3
         matHe4->fZposition       = 0.0;     //cm

         double window_thickness   =  15.0/10000.0 ;   //15 um
         auto * matAlEndcap1 = new TargetMaterial("Al-endcap1", "Aluminum target endcap upstream", 13, 27);
         matAlEndcap1->fLength               =  window_thickness ;   //15 um
         matAlEndcap1->fDensity              =  2.7;        // g/cm3
         matAlEndcap1->fZposition            = -(target_length/2.0+window_thickness/2.0);     //cm

         auto * matAlEndcap2 = new TargetMaterial("Al-endcap2", "Aluminum target endcap downstream", 13, 27);
         matAlEndcap2->fLength               =  window_thickness ;  //cm
         matAlEndcap2->fDensity              =  2.7;       // g/cm3
         matAlEndcap2->fZposition            =  target_length/2.0;     //cm
         matAlEndcap2->fZposition            = (target_length/2.0+window_thickness/2.0);     //cm

         this->AddMaterial(matHe4);
         this->AddMaterial(matAlEndcap1);
         this->AddMaterial(matAlEndcap2);
      }
      //______________________________________________________________________________
      


      DeuteriumGasTarget::DeuteriumGasTarget(const char * name, const char * title) : 
         Target(name,title)
      { 
         DefineMaterials();
      }
      //______________________________________________________________________________

      DeuteriumGasTarget::~DeuteriumGasTarget()
      { }
      //______________________________________________________________________________

      void DeuteriumGasTarget::DefineMaterials()
      {
         using namespace insane::units;
         double       target_length = 35.0;
         auto * matD  = new TargetMaterial("D","D",1,2);

         double density_at_1atm_300K = 0.165*kg/m3;
         // density is 0.179 at 0C, to 0.170 at 15C, to 0.165 at 25C.
         double a_noUnit     = 2.01410178;
         double pre_noUnit   = 3.0;
         double tempe_noUnit = 298.;
         double pressure     = 3.0*atmosphere;
         double temperature  = 298.*kelvin;
         double He4_density  = density_at_1atm_300K*pre_noUnit;//(a_noUnit*pre_noUnit)/(0.0821*tempe_noUnit)*kg/m3; //0.164*kg/m3 at 1 atm;

         matD->fLength          = target_length;    //cm
         matD->fDensity         = He4_density/(g/cm3); // g/cm3
         matD->fZposition       = 0.0;     //cm

         double window_thickness   =  15.0/10000.0 ;   //15 um
         auto * matAlEndcap1 = new TargetMaterial("Al-endcap1", "Aluminum target endcap upstream", 13, 27);
         matAlEndcap1->fLength               =  window_thickness ;   //15 um
         matAlEndcap1->fDensity              =  2.7;        // g/cm3
         matAlEndcap1->fZposition            = -(target_length/2.0+window_thickness/2.0);     //cm

         auto * matAlEndcap2 = new TargetMaterial("Al-endcap2", "Aluminum target endcap downstream", 13, 27);
         matAlEndcap2->fLength               =  window_thickness ;  //cm
         matAlEndcap2->fDensity              =  2.7;       // g/cm3
         matAlEndcap2->fZposition            =  target_length/2.0;     //cm
         matAlEndcap2->fZposition            = (target_length/2.0+window_thickness/2.0);     //cm

         this->AddMaterial(matD);
         this->AddMaterial(matAlEndcap1);
         this->AddMaterial(matAlEndcap2);
      }
      //______________________________________________________________________________
      


      HydrogenGasTarget::HydrogenGasTarget(const char * name, const char * title) : 
         Target(name,title)
      { 
         DefineMaterials();
      }
      //______________________________________________________________________________

      HydrogenGasTarget::~HydrogenGasTarget()
      { }
      //______________________________________________________________________________

      void HydrogenGasTarget::DefineMaterials()
      {
         using namespace insane::units;
         double       target_length = 35.0;
         auto * matD  = new TargetMaterial("H","H",1,1);

         double density_at_1atm_300K = 0.0823*kg/m3;
         // density is 0.0899 at 0C, to 0.0852 at 15C, to 0.0823 at 25C.
         double a_noUnit     = 2.01410178;
         double pre_noUnit   = 3.0;
         double tempe_noUnit = 298.;
         double pressure     = 3.0*atmosphere;
         double temperature  = 298.*kelvin;
         double He4_density  = density_at_1atm_300K*pre_noUnit;//(a_noUnit*pre_noUnit)/(0.0821*tempe_noUnit)*kg/m3; //0.164*kg/m3 at 1 atm;

         matD->fLength          = target_length;    //cm
         matD->fDensity         = He4_density/(g/cm3); // g/cm3
         matD->fZposition       = 0.0;     //cm

         double window_thickness   =  15.0/10000.0 ;   //15 um
         auto * matAlEndcap1 = new TargetMaterial("Al-endcap1", "Aluminum target endcap upstream", 13, 27);
         matAlEndcap1->fLength               =  window_thickness ;   //15 um
         matAlEndcap1->fDensity              =  2.7;        // g/cm3
         matAlEndcap1->fZposition            = -(target_length/2.0+window_thickness/2.0);     //cm

         auto * matAlEndcap2 = new TargetMaterial("Al-endcap2", "Aluminum target endcap downstream", 13, 27);
         matAlEndcap2->fLength               =  window_thickness ;  //cm
         matAlEndcap2->fDensity              =  2.7;       // g/cm3
         matAlEndcap2->fZposition            =  target_length/2.0;     //cm
         matAlEndcap2->fZposition            = (target_length/2.0+window_thickness/2.0);     //cm

         this->AddMaterial(matD);
         this->AddMaterial(matAlEndcap1);
         this->AddMaterial(matAlEndcap2);
      }
      //______________________________________________________________________________
      

      LH2Target::LH2Target(const char * name, const char * title) : 
         Target(name,title)
      { 
         DefineMaterials();
      }
      //______________________________________________________________________________

      LH2Target::~LH2Target()
      { }
      //______________________________________________________________________________

      void LH2Target::DefineMaterials()
      {
         Double_t       LH2_density   = 0.07085; //g/cm^3
         Double_t       target_length = 5.0;
         auto * matLH2 = new TargetMaterial("LH2","LH2",1,1);
         matLH2->fLength          = target_length;    //cm
         matLH2->fDensity         = LH2_density; // g/cm3
         matLH2->fZposition       = 0.0;     //cm

         auto * matAlEndcap1 = new TargetMaterial("Al-endcap1", "Aluminum target endcap upstream", 13, 27);
         matAlEndcap1->fLength               =  0.00381 ;   //cm
         matAlEndcap1->fDensity              =  2.7;        // g/cm3
         matAlEndcap1->fZposition            = -target_length/2.0;     //cm

         auto * matAlEndcap2 = new TargetMaterial("Al-endcap2", "Aluminum target endcap downstream", 13, 27);
         matAlEndcap2->fLength               =  0.00381 ;  //cm
         matAlEndcap2->fDensity              =  2.7;       // g/cm3
         matAlEndcap2->fZposition            =  target_length/2.0;     //cm

         this->AddMaterial(matLH2);
         this->AddMaterial(matAlEndcap1);
         this->AddMaterial(matAlEndcap2);

      }
      //______________________________________________________________________________

      TDISDeuteriumGasTarget::TDISDeuteriumGasTarget(const char * name, const char * title) : 
         Target(name,title)
      { 
         DefineMaterials();
      }
      //______________________________________________________________________________

      TDISDeuteriumGasTarget::~TDISDeuteriumGasTarget()
      { }
      //______________________________________________________________________________

      void TDISDeuteriumGasTarget::DefineMaterials()
      {
         using namespace insane::units;
         double       target_length = 40.0;
         auto * matD  = new TargetMaterial("D","D",1,2);

         double density_at_1atm_300K = 0.165*kg/m3;
         // density is 0.179 at 0C, to 0.170 at 15C, to 0.165 at 25C.
         double a_noUnit     = 2.01410178;
         double pre_noUnit   = 1.0;
         double tempe_noUnit = 298.;
         double pressure     = 1.0*atmosphere;
         double temperature  = 298.*kelvin;
         double He4_density  = density_at_1atm_300K*pre_noUnit;//(a_noUnit*pre_noUnit)/(0.0821*tempe_noUnit)*kg/m3; //0.164*kg/m3 at 1 atm;

         matD->fLength          = target_length;    //cm
         matD->fDensity         = He4_density/(g/cm3); // g/cm3
         matD->fZposition       = 0.0;     //cm

         double window_thickness   =  20.0/10000.0 ;   //20 um
         auto * matBeEndcap1 = new TargetMaterial("Be-endcap1", "Beryllium target endcap upstream", 4, 9);
         matBeEndcap1->fLength               =  window_thickness ;   //15 um
         matBeEndcap1->fDensity              =  1.85;        // g/cm3
         matBeEndcap1->fZposition            = -(target_length/2.0+window_thickness/2.0);     //cm

         auto * matBeEndcap2 = new TargetMaterial("Be-endcap2", "Beryllium target endcap downstream", 4, 9);
         matBeEndcap2->fLength               =  window_thickness ;  //cm
         matBeEndcap2->fDensity              =  1.85;       // g/cm3
         matBeEndcap2->fZposition            =  target_length/2.0;     //cm
         matBeEndcap2->fZposition            = (target_length/2.0+window_thickness/2.0);     //cm

         this->AddMaterial(matD);
         this->AddMaterial(matBeEndcap1);
         this->AddMaterial(matBeEndcap2);
      }
      //______________________________________________________________________________
      
      TDISHydrogenGasTarget::TDISHydrogenGasTarget(const char * name, const char * title) : 
         Target(name,title)
      { 
         DefineMaterials();
      }
      //______________________________________________________________________________

      TDISHydrogenGasTarget::~TDISHydrogenGasTarget()
      { }
      //______________________________________________________________________________

      void TDISHydrogenGasTarget::DefineMaterials()
      {
         using namespace insane::units;
         double       target_length = 40.0;
         auto * matD  = new TargetMaterial("H","H",1,1);

         double density_at_1atm_300K = 0.0823*kg/m3;
         // density is 0.0899 at 0C, to 0.0852 at 15C, to 0.0823 at 25C.
         double a_noUnit     = 2.01410178;
         double pre_noUnit   = 1.0;
         double tempe_noUnit = 298.;
         double pressure     = 1.0*atmosphere;
         double temperature  = 298.*kelvin;
         double He4_density  = density_at_1atm_300K*pre_noUnit;//(a_noUnit*pre_noUnit)/(0.0821*tempe_noUnit)*kg/m3; //0.164*kg/m3 at 1 atm;

         matD->fLength          = target_length;    //cm
         matD->fDensity         = He4_density/(g/cm3); // g/cm3
         matD->fZposition       = 0.0;     //cm

         double window_thickness   =  20.0/10000.0 ;   //20 um
         auto * matBeEndcap1 = new TargetMaterial("Be-endcap1", "Beryllium target endcap upstream", 4, 9);
         matBeEndcap1->fLength               =  window_thickness ;   //15 um
         matBeEndcap1->fDensity              =  1.85;        // g/cm3
         matBeEndcap1->fZposition            = -(target_length/2.0+window_thickness/2.0);     //cm

         auto * matBeEndcap2 = new TargetMaterial("Be-endcap2", "Beryllium target endcap downstream", 4, 9);
         matBeEndcap2->fLength               =  window_thickness ;  //cm
         matBeEndcap2->fDensity              =  1.85;       // g/cm3
         matBeEndcap2->fZposition            =  target_length/2.0;     //cm
         matBeEndcap2->fZposition            = (target_length/2.0+window_thickness/2.0);     //cm

         this->AddMaterial(matD);
         this->AddMaterial(matBeEndcap1);
         this->AddMaterial(matBeEndcap2);
      }
      //______________________________________________________________________________
      

   }
}
//______________________________________________________________________________

